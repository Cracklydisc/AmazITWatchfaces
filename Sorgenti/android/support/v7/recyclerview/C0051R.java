package android.support.v7.recyclerview;

import com.huami.watch.watchface.analogyellow.R;

public final class C0051R {

    public static final class styleable {
        public static final int[] BoxInsetLayout_Layout = new int[]{R.attr.hm_layout_box};
        public static final int[] PageListView = new int[]{R.attr.centerItemInPage, R.attr.itemCountPerPage};
        public static final int[] RecyclerView = new int[]{16842948, R.attr.layoutManager, R.attr.spanCount, R.attr.reverseLayout, R.attr.stackFromEnd};
        public static final int RecyclerView_layoutManager = 1;
    }
}
