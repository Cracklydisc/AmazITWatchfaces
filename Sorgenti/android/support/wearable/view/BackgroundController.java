package android.support.wearable.view;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;
import android.support.wearable.view.GridViewPager.OnAdapterChangeListener;
import android.support.wearable.view.GridViewPager.OnPageChangeListener;

@TargetApi(20)
class BackgroundController implements OnAdapterChangeListener, OnPageChangeListener {
    private GridPagerAdapter mAdapter;
    private final CrossfadeDrawable mBackground;
    private final ViewportDrawable mBaseLayer;
    private final Point mBaseSourcePage;
    private float mBaseXPos;
    private int mBaseXSteps;
    private float mBaseYPos;
    private int mBaseYSteps;
    private final ViewportDrawable mCrossfadeLayer;
    private float mCrossfadeXPos;
    private float mCrossfadeYPos;
    private final Point mCurrentPage;
    private Direction mDirection;
    private final Point mFadeSourcePage;
    private int mFadeXSteps;
    private int mFadeYSteps;
    private final Point mLastPageScrolled;
    private final Point mLastSelectedPage;
    private final LruCache<Integer, Drawable> mPageBackgrounds;
    private final LruCache<Integer, Drawable> mRowBackgrounds;
    private float mScrollRelativeX;
    private float mScrollRelativeY;
    private boolean mUsingCrossfadeLayer;

    private enum Direction {
        LEFT(-1, 0),
        UP(0, -1),
        RIGHT(1, 0),
        DOWN(0, 1),
        NONE(0, 0);
        
        private final int f0x;
        private final int f1y;

        private Direction(int x, int y) {
            this.f0x = x;
            this.f1y = y;
        }

        boolean isVertical() {
            return this.f1y != 0;
        }

        boolean isHorizontal() {
            return this.f0x != 0;
        }

        static Direction fromOffset(float x, float y) {
            if (y != 0.0f) {
                return y > 0.0f ? DOWN : UP;
            } else {
                if (x != 0.0f) {
                    return x > 0.0f ? RIGHT : LEFT;
                } else {
                    return NONE;
                }
            }
        }
    }

    private static int pack(int x, int y) {
        return (y << 16) | (65535 & x);
    }

    private static int pack(Point p) {
        return pack(p.x, p.y);
    }

    public void onPageScrollStateChanged(int state) {
        if (state == 0) {
            this.mDirection = Direction.NONE;
        }
    }

    public void onPageScrolled(int row, int column, float rowOffset, float colOffset, int rowOffsetPx, int colOffsetPx) {
        float relX;
        float relY;
        if (this.mDirection == Direction.NONE || !this.mCurrentPage.equals(this.mLastSelectedPage) || !this.mLastPageScrolled.equals(column, row)) {
            this.mLastPageScrolled.set(column, row);
            this.mCurrentPage.set(this.mLastSelectedPage.x, this.mLastSelectedPage.y);
            relX = 0.0f;
            relY = ((float) Func.clamp(row - this.mCurrentPage.y, -1, 0)) + rowOffset;
            if (relY == 0.0f) {
                relX = ((float) Func.clamp(column - this.mCurrentPage.x, -1, 0)) + colOffset;
            }
            this.mDirection = Direction.fromOffset(relX, relY);
            updateBackgrounds(this.mCurrentPage, this.mLastPageScrolled, this.mDirection, relX, relY);
        } else if (this.mDirection.isVertical()) {
            relX = 0.0f;
            relY = ((float) Func.clamp(row - this.mCurrentPage.y, -1, 0)) + rowOffset;
        } else {
            relX = ((float) Func.clamp(column - this.mCurrentPage.x, -1, 0)) + colOffset;
            relY = 0.0f;
        }
        this.mScrollRelativeX = relX;
        this.mScrollRelativeY = relY;
        this.mBaseLayer.setPosition(this.mBaseXPos + relX, this.mBaseYPos + relY);
        if (this.mUsingCrossfadeLayer) {
            this.mBackground.setProgress(this.mDirection.isVertical() ? Math.abs(relY) : Math.abs(relX));
            this.mCrossfadeLayer.setPosition(this.mCrossfadeXPos + relX, this.mCrossfadeYPos + relY);
        }
    }

    private void updateBackgrounds(Point current, Point scrolling, Direction dir, float relX, float relY) {
        if (this.mAdapter == null || this.mAdapter.getRowCount() <= 0) {
            this.mUsingCrossfadeLayer = false;
            this.mBaseLayer.setDrawable(null);
            this.mCrossfadeLayer.setDrawable(null);
            return;
        }
        Drawable base = updateBaseLayer(current, relX, relY);
        boolean overScrolling;
        if (((float) current.x) + relX < 0.0f || ((float) current.y) + relY < 0.0f || ((float) scrolling.x) + relX > ((float) (this.mAdapter.getColumnCount(current.y) - 1)) || ((float) scrolling.y) + relY > ((float) (this.mAdapter.getRowCount() - 1))) {
            overScrolling = true;
        } else {
            overScrolling = false;
        }
        if (this.mDirection == Direction.NONE || overScrolling) {
            this.mUsingCrossfadeLayer = false;
            this.mCrossfadeLayer.setDrawable(null);
            this.mBackground.setProgress(0.0f);
            return;
        }
        updateFadingLayer(current, scrolling, dir, relX, relY, base);
    }

    private Drawable updateBaseLayer(Point current, float relX, float relY) {
        Drawable base = (Drawable) this.mPageBackgrounds.get(Integer.valueOf(pack(current)));
        this.mBaseSourcePage.set(current.x, current.y);
        if (base == GridPagerAdapter.BACKGROUND_NONE) {
            base = (Drawable) this.mRowBackgrounds.get(Integer.valueOf(current.y));
            this.mBaseXSteps = this.mAdapter.getColumnCount(current.y) + 2;
            this.mBaseXPos = (float) (current.x + 1);
        } else {
            this.mBaseXSteps = 3;
            this.mBaseXPos = 1.0f;
        }
        this.mBaseYSteps = 3;
        this.mBaseYPos = 1.0f;
        this.mBaseLayer.setDrawable(base);
        this.mBaseLayer.setStops(this.mBaseXSteps, this.mBaseYSteps);
        this.mBaseLayer.setPosition(this.mBaseXPos + relX, this.mBaseYPos + relY);
        this.mBackground.setBase(this.mBaseLayer);
        return base;
    }

    private void updateFadingLayer(Point current, Point scrolling, Direction dir, float relX, float relY, Drawable base) {
        int crossfadeY = scrolling.y + (dir == Direction.DOWN ? 1 : 0);
        int crossfadeX = scrolling.x + (dir == Direction.RIGHT ? 1 : 0);
        if (crossfadeY != this.mCurrentPage.y) {
            crossfadeX = this.mAdapter.getCurrentColumnForRow(crossfadeY, current.x);
        }
        Drawable fade = (Drawable) this.mPageBackgrounds.get(Integer.valueOf(pack(crossfadeX, crossfadeY)));
        this.mFadeSourcePage.set(crossfadeX, crossfadeY);
        boolean fadeIsRowBg = false;
        if (fade == GridPagerAdapter.BACKGROUND_NONE) {
            fade = (Drawable) this.mRowBackgrounds.get(Integer.valueOf(crossfadeY));
            fadeIsRowBg = true;
        }
        if (base == fade) {
            this.mUsingCrossfadeLayer = false;
            this.mCrossfadeLayer.setDrawable(null);
            this.mBackground.setFading(null);
            this.mBackground.setProgress(0.0f);
            return;
        }
        if (fadeIsRowBg) {
            this.mFadeXSteps = this.mAdapter.getColumnCount(Func.clamp(crossfadeY, 0, this.mAdapter.getRowCount() - 1)) + 2;
            if (dir.isHorizontal()) {
                this.mCrossfadeXPos = (float) (current.x + 1);
            } else {
                this.mCrossfadeXPos = (float) (crossfadeX + 1);
            }
        } else {
            this.mFadeXSteps = 3;
            this.mCrossfadeXPos = (float) (1 - dir.f0x);
        }
        this.mFadeYSteps = 3;
        this.mCrossfadeYPos = (float) (1 - dir.f1y);
        this.mUsingCrossfadeLayer = true;
        this.mCrossfadeLayer.setDrawable(fade);
        this.mCrossfadeLayer.setStops(this.mFadeXSteps, this.mFadeYSteps);
        this.mCrossfadeLayer.setPosition(this.mCrossfadeXPos + relX, this.mCrossfadeYPos + relY);
        this.mBackground.setFading(this.mCrossfadeLayer);
    }

    public void onPageSelected(int row, int column) {
        this.mLastSelectedPage.set(column, row);
    }

    public void onAdapterChanged(GridPagerAdapter oldAdapter, GridPagerAdapter newAdapter) {
        reset();
        this.mLastSelectedPage.set(0, 0);
        this.mCurrentPage.set(0, 0);
        this.mAdapter = newAdapter;
    }

    public void onDataSetChanged() {
        reset();
    }

    private void reset() {
        this.mDirection = Direction.NONE;
        this.mPageBackgrounds.evictAll();
        this.mRowBackgrounds.evictAll();
        this.mCrossfadeLayer.setDrawable(null);
        this.mBaseLayer.setDrawable(null);
    }
}
