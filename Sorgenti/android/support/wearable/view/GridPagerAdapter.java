package android.support.wearable.view;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;

@TargetApi(20)
public abstract class GridPagerAdapter {
    public static final Drawable BACKGROUND_NONE = new NoOpDrawable();
    public static final Point POSITION_NONE = new Point(-1, -1);
    public static final Point POSITION_UNCHANGED = new Point(-2, -2);

    private static final class NoOpDrawable extends Drawable {
        private NoOpDrawable() {
        }

        public void draw(Canvas canvas) {
        }

        public void setAlpha(int alpha) {
        }

        public void setColorFilter(ColorFilter cf) {
        }

        public int getOpacity() {
            return 0;
        }
    }

    protected abstract void destroyItem(ViewGroup viewGroup, int i, int i2, Object obj);

    public abstract int getColumnCount(int i);

    public abstract int getRowCount();

    protected abstract Object instantiateItem(ViewGroup viewGroup, int i, int i2);

    public abstract boolean isViewFromObject(View view, Object obj);

    public int getCurrentColumnForRow(int row, int currentColumn) {
        return 0;
    }

    protected void setCurrentColumnForRow(int row, int currentColumn) {
    }

    protected void startUpdate(ViewGroup container) {
    }

    protected void finishUpdate(ViewGroup container) {
    }
}
