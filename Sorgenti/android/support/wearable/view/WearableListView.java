package android.support.wearable.view;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.PointF;
import android.os.Handler;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.Recycler;
import android.support.v7.widget.RecyclerView.State;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Property;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Scroller;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@TargetApi(20)
public class WearableListView extends RecyclerView {
    private boolean mCanClick;
    private ClickListener mClickListener;
    private boolean mGestureDirectionLocked;
    private boolean mGreedyTouchMode;
    private int mInitialOffset;
    private int mLastScrollChange;
    private final int[] mLocation;
    private final int mMaxFlingVelocity;
    private final int mMinFlingVelocity;
    private final AdapterDataObserver mObserver;
    private final Set<OnScrollListener> mOnScrollListeners;
    private OnOverScrollListener mOverScrollListener;
    private boolean mPossibleVerticalSwipe;
    private final Handler mPressedHandler;
    private final Runnable mPressedRunnable;
    private View mPressedView;
    private int mPreviousCentral;
    private final Runnable mReleasedRunnable;
    private Animator mScrollAnimator;
    private Scroller mScroller;
    private SetScrollVerticallyProperty mSetScrollVerticallyProperty;
    private float mStartFirstTop;
    private float mStartX;
    private float mStartY;
    private int mTapPositionX;
    private int mTapPositionY;
    private final float[] mTapRegions;
    private final int mTouchSlop;

    class C00771 implements Runnable {
        C00771() {
        }

        public void run() {
            WearableListView.this.mPressedView = WearableListView.this.getChildAt(WearableListView.this.findCenterViewIndex());
            WearableListView.this.mPressedView.setPressed(true);
        }
    }

    class C00782 implements Runnable {
        C00782() {
        }

        public void run() {
            WearableListView.this.releasePressedItem();
        }
    }

    class C00793 extends AdapterDataObserver {
        C00793() {
        }
    }

    class C00804 extends android.support.v7.widget.RecyclerView.OnScrollListener {
        C00804() {
        }

        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == 0 && WearableListView.this.getChildCount() > 0) {
                WearableListView.this.handleTouchUp(null, newState);
            }
            for (OnScrollListener listener : WearableListView.this.mOnScrollListeners) {
                listener.onScrollStateChanged(newState);
            }
        }
    }

    class C00815 extends SimpleAnimatorListener {
        C00815() {
        }

        public void onAnimationEnd(Animator animator) {
            if (!wasCanceled()) {
                WearableListView.this.mCanClick = true;
            }
        }
    }

    public interface ClickListener {
        void onClick(ViewHolder viewHolder);

        void onTopEmptyRegionClick();
    }

    private class LayoutManager extends android.support.v7.widget.RecyclerView.LayoutManager {
        private int mAbsoluteScroll;
        private int mFirstPosition;
        private boolean mPushFirstHigher;
        private boolean mUseOldViewTop;

        private LayoutManager() {
            this.mUseOldViewTop = true;
        }

        public void onLayoutChildren(Recycler recycler, State state) {
            int parentBottom = getHeight() - getPaddingBottom();
            View oldTopView = getChildCount() > 0 ? getChildAt(0) : null;
            boolean hasOldTopView = oldTopView != null && this.mUseOldViewTop;
            int oldTop = WearableListView.this.getCentralViewTop() + WearableListView.this.mInitialOffset;
            if (this.mPushFirstHigher) {
                oldTop -= WearableListView.this.getAdjustedHeight() / 3;
            }
            if (hasOldTopView) {
                oldTop = oldTopView.getTop();
            }
            this.mUseOldViewTop = true;
            detachAndScrapAttachedViews(recycler);
            int top = oldTop;
            int left = getPaddingLeft();
            int right = getWidth() - getPaddingRight();
            int count = state.getItemCount();
            int i = 0;
            while (getFirstPosition() + i < count && top < parentBottom) {
                View v = recycler.getViewForPosition(getFirstPosition() + i);
                addView(v, i);
                measureView(v);
                int bottom = top + WearableListView.this.getItemHeight();
                v.layout(left, top, right, bottom);
                i++;
                top = bottom;
            }
            if (getChildCount() > 0) {
                WearableListView.this.notifyChildrenAboutProximity(false);
            }
            if (!hasOldTopView) {
                setAbsoluteScroll(((this.mPushFirstHigher ? 1 : 0) + getFirstPosition()) * WearableListView.this.getItemHeight());
            }
        }

        private void setAbsoluteScroll(int absoluteScroll) {
            this.mAbsoluteScroll = absoluteScroll;
            for (OnScrollListener listener : WearableListView.this.mOnScrollListeners) {
                listener.onAbsoluteScrollChange(this.mAbsoluteScroll);
            }
        }

        private void measureView(View v) {
            LayoutParams lp = (LayoutParams) v.getLayoutParams();
            v.measure(android.support.v7.widget.RecyclerView.LayoutManager.getChildMeasureSpec(getWidth(), ((getPaddingLeft() + getPaddingRight()) + lp.leftMargin) + lp.rightMargin, lp.width, canScrollHorizontally()), android.support.v7.widget.RecyclerView.LayoutManager.getChildMeasureSpec(getHeight(), ((getPaddingTop() + getPaddingBottom()) + lp.topMargin) + lp.bottomMargin, getHeight() / 3, canScrollVertically()));
        }

        public LayoutParams generateDefaultLayoutParams() {
            return new LayoutParams(-1, -2);
        }

        public boolean canScrollVertically() {
            return true;
        }

        public int scrollVerticallyBy(int dy, Recycler recycler, State state) {
            if (getChildCount() == 0) {
                return 0;
            }
            int scrolled = 0;
            int left = getPaddingLeft();
            int right = getWidth() - getPaddingRight();
            int scrollBy;
            View v;
            if (dy < 0) {
                while (scrolled > dy) {
                    View topView = getChildAt(0);
                    if (getFirstPosition() > 0) {
                        scrollBy = Math.min(scrolled - dy, Math.max(-topView.getTop(), 0));
                        scrolled -= scrollBy;
                        offsetChildrenVertical(scrollBy);
                        if (getFirstPosition() <= 0 || scrolled <= dy) {
                            break;
                        }
                        this.mFirstPosition--;
                        v = recycler.getViewForPosition(getFirstPosition());
                        addView(v, 0);
                        measureView(v);
                        int bottom = topView.getTop();
                        v.layout(left, bottom - WearableListView.this.getItemHeight(), right, bottom);
                    } else {
                        this.mPushFirstHigher = false;
                        scrollBy = Math.min(-dy, (WearableListView.this.mOverScrollListener != null ? getHeight() : WearableListView.this.getTopViewMaxTop()) - topView.getTop());
                        scrolled -= scrollBy;
                        offsetChildrenVertical(scrollBy);
                    }
                }
            } else if (dy > 0) {
                int parentHeight = getHeight();
                while (scrolled < dy) {
                    View bottomView = getChildAt(getChildCount() - 1);
                    if (state.getItemCount() <= this.mFirstPosition + getChildCount()) {
                        scrollBy = Math.max(-dy, (getHeight() / 2) - bottomView.getBottom());
                        scrolled -= scrollBy;
                        offsetChildrenVertical(scrollBy);
                        break;
                    }
                    scrollBy = -Math.min(dy - scrolled, Math.max(bottomView.getBottom() - parentHeight, 0));
                    scrolled -= scrollBy;
                    offsetChildrenVertical(scrollBy);
                    if (scrolled >= dy) {
                        break;
                    }
                    v = recycler.getViewForPosition(this.mFirstPosition + getChildCount());
                    int top = getChildAt(getChildCount() - 1).getBottom();
                    addView(v);
                    measureView(v);
                    v.layout(left, top, right, top + WearableListView.this.getItemHeight());
                }
            }
            recycleViewsOutOfBounds(recycler);
            WearableListView.this.notifyChildrenAboutProximity(true);
            WearableListView.this.onScroll(scrolled);
            setAbsoluteScroll(this.mAbsoluteScroll + scrolled);
            return scrolled;
        }

        public void scrollToPosition(int position) {
            this.mUseOldViewTop = false;
            if (position > 0) {
                this.mFirstPosition = position - 1;
                this.mPushFirstHigher = true;
                return;
            }
            this.mFirstPosition = position;
            this.mPushFirstHigher = false;
        }

        public void smoothScrollToPosition(RecyclerView recyclerView, State state, int position) {
            LinearSmoothScroller linearSmoothScroller = new SmoothScroller(recyclerView.getContext(), this);
            linearSmoothScroller.setTargetPosition(position);
            startSmoothScroll(linearSmoothScroller);
        }

        private void recycleViewsOutOfBounds(Recycler recycler) {
            int i;
            int childCount = getChildCount();
            int parentWidth = getWidth();
            int parentHeight = getHeight();
            boolean foundFirst = false;
            int first = 0;
            int last = 0;
            for (i = 0; i < childCount; i++) {
                View v = getChildAt(i);
                if (v.hasFocus() || (v.getRight() >= 0 && v.getLeft() <= parentWidth && v.getBottom() >= 0 && v.getTop() <= parentHeight)) {
                    if (!foundFirst) {
                        first = i;
                        foundFirst = true;
                    }
                    last = i;
                }
            }
            for (i = childCount - 1; i > last; i--) {
                removeAndRecycleViewAt(i, recycler);
            }
            for (i = first - 1; i >= 0; i--) {
                removeAndRecycleViewAt(i, recycler);
            }
            if (getChildCount() == 0) {
                this.mFirstPosition = 0;
            } else if (first > 0) {
                this.mPushFirstHigher = true;
                this.mFirstPosition += first;
            }
        }

        public int getFirstPosition() {
            return this.mFirstPosition;
        }

        public void onAdapterChanged(Adapter oldAdapter, Adapter newAdapter) {
            removeAllViews();
        }
    }

    public interface OnCenterProximityListener {
        void onCenterPosition(boolean z);

        void onNonCenterPosition(boolean z);
    }

    public interface OnOverScrollListener {
        void onOverScroll();
    }

    public interface OnScrollListener {
        void onAbsoluteScrollChange(int i);

        void onCentralPositionChanged(int i);

        void onScroll(int i);

        void onScrollStateChanged(int i);
    }

    private class SetScrollVerticallyProperty extends Property<WearableListView, Integer> {
        public SetScrollVerticallyProperty() {
            super(Integer.class, "scrollVertically");
        }

        public Integer get(WearableListView wearableListView) {
            return Integer.valueOf(wearableListView.mLastScrollChange);
        }

        public void set(WearableListView wearableListView, Integer value) {
            wearableListView.setScrollVertically(value.intValue());
        }
    }

    private static class SmoothScroller extends LinearSmoothScroller {
        private final LayoutManager mLayoutManager;

        public SmoothScroller(Context context, LayoutManager manager) {
            super(context);
            this.mLayoutManager = manager;
        }

        protected void onStart() {
            super.onStart();
        }

        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }

        public int calculateDtToFit(int viewStart, int viewEnd, int boxStart, int boxEnd, int snapPreference) {
            return ((boxStart + boxEnd) / 2) - ((viewStart + viewEnd) / 2);
        }

        public PointF computeScrollVectorForPosition(int targetPosition) {
            if (targetPosition < this.mLayoutManager.getFirstPosition()) {
                return new PointF(0.0f, -1.0f);
            }
            return new PointF(0.0f, 1.0f);
        }
    }

    public static class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        protected void onCenterProximity(boolean isCentralItem, boolean animate) {
            if (this.itemView instanceof OnCenterProximityListener) {
                OnCenterProximityListener item = this.itemView;
                if (isCentralItem) {
                    item.onCenterPosition(animate);
                } else {
                    item.onNonCenterPosition(animate);
                }
            }
        }
    }

    public WearableListView(Context context) {
        this(context, null);
    }

    public WearableListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WearableListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCanClick = true;
        this.mSetScrollVerticallyProperty = new SetScrollVerticallyProperty();
        this.mOnScrollListeners = new HashSet();
        this.mInitialOffset = 0;
        this.mTapRegions = new float[2];
        this.mPreviousCentral = 0;
        this.mLocation = new int[2];
        this.mPressedHandler = new Handler();
        this.mPressedView = null;
        this.mPressedRunnable = new C00771();
        this.mReleasedRunnable = new C00782();
        this.mObserver = new C00793();
        setHasFixedSize(true);
        setOverScrollMode(2);
        setLayoutManager(new LayoutManager());
        setOnScrollListener(new C00804());
        ViewConfiguration vc = ViewConfiguration.get(context);
        this.mTouchSlop = vc.getScaledTouchSlop();
        this.mMinFlingVelocity = vc.getScaledMinimumFlingVelocity();
        this.mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
    }

    public void setAdapter(Adapter adapter) {
        Adapter currentAdapter = getAdapter();
        if (currentAdapter != null) {
            currentAdapter.unregisterAdapterDataObserver(this.mObserver);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(this.mObserver);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.mGreedyTouchMode && getChildCount() > 0) {
            int action = event.getActionMasked();
            if (action == 0) {
                this.mStartX = event.getX();
                this.mStartY = event.getY();
                this.mStartFirstTop = getChildCount() > 0 ? (float) getChildAt(0).getTop() : 0.0f;
                this.mPossibleVerticalSwipe = true;
                this.mGestureDirectionLocked = false;
            } else if (action == 2 && this.mPossibleVerticalSwipe) {
                handlePossibleVerticalSwipe(event);
            }
            getParent().requestDisallowInterceptTouchEvent(this.mPossibleVerticalSwipe);
        }
        return super.onInterceptTouchEvent(event);
    }

    private boolean handlePossibleVerticalSwipe(MotionEvent event) {
        if (this.mGestureDirectionLocked) {
            return this.mPossibleVerticalSwipe;
        }
        float deltaX = Math.abs(this.mStartX - event.getX());
        float deltaY = Math.abs(this.mStartY - event.getY());
        if ((deltaX * deltaX) + (deltaY * deltaY) > ((float) (this.mTouchSlop * this.mTouchSlop))) {
            if (deltaX > deltaY) {
                this.mPossibleVerticalSwipe = false;
            }
            this.mGestureDirectionLocked = true;
        }
        return this.mPossibleVerticalSwipe;
    }

    public boolean onTouchEvent(MotionEvent event) {
        int scrollState = getScrollState();
        boolean result = super.onTouchEvent(event);
        if (getChildCount() <= 0) {
            return result;
        }
        int action = event.getActionMasked();
        if (action == 0) {
            handleTouchDown(event);
            return result;
        } else if (action == 1) {
            handleTouchUp(event, scrollState);
            getParent().requestDisallowInterceptTouchEvent(false);
            return result;
        } else if (action == 2) {
            if (Math.abs(this.mTapPositionX - ((int) event.getX())) >= this.mTouchSlop || Math.abs(this.mTapPositionY - ((int) event.getY())) >= this.mTouchSlop) {
                releasePressedItem();
                this.mCanClick = false;
            }
            result |= handlePossibleVerticalSwipe(event);
            getParent().requestDisallowInterceptTouchEvent(this.mPossibleVerticalSwipe);
            return result;
        } else if (action != 3) {
            return result;
        } else {
            getParent().requestDisallowInterceptTouchEvent(false);
            return result;
        }
    }

    private void releasePressedItem() {
        if (this.mPressedView != null) {
            this.mPressedView.setPressed(false);
            this.mPressedView = null;
        }
        this.mPressedHandler.removeCallbacks(this.mPressedRunnable);
    }

    private void onScroll(int dy) {
        for (OnScrollListener listener : this.mOnScrollListeners) {
            listener.onScroll(dy);
        }
    }

    public void addOnScrollListener(OnScrollListener listener) {
        this.mOnScrollListeners.add(listener);
    }

    private boolean checkForTap(MotionEvent event) {
        float rawY = event.getRawY();
        int index = findCenterViewIndex();
        ViewHolder holder = getChildViewHolder(getChildAt(index));
        computeTapRegions(this.mTapRegions);
        if (rawY <= this.mTapRegions[0] || rawY >= this.mTapRegions[1]) {
            if (index > 0 && rawY <= this.mTapRegions[0]) {
                animateToMiddle(index - 1, index);
                return true;
            } else if (index < getChildCount() - 1 && rawY >= this.mTapRegions[1]) {
                animateToMiddle(index + 1, index);
                return true;
            } else if (index != 0 || rawY > this.mTapRegions[0] || this.mClickListener == null || !isEnabled()) {
                return false;
            } else {
                this.mClickListener.onTopEmptyRegionClick();
                return true;
            }
        } else if (this.mClickListener == null || !isEnabled()) {
            return true;
        } else {
            this.mClickListener.onClick(holder);
            return true;
        }
    }

    private void animateToMiddle(int newCenterIndex, int oldCenterIndex) {
        if (newCenterIndex == oldCenterIndex) {
            throw new IllegalArgumentException("newCenterIndex must be different from oldCenterIndex");
        }
        startScrollAnimation(new ArrayList(), getCentralViewTop() - getChildAt(newCenterIndex).getTop(), 150);
    }

    private void startScrollAnimation(List<Animator> animators, int scroll, long duration) {
        startScrollAnimation((List) animators, scroll, duration, 0);
    }

    private void startScrollAnimation(List<Animator> animators, int scroll, long duration, long delay) {
        startScrollAnimation(animators, scroll, duration, delay, null);
    }

    private void startScrollAnimation(int scroll, long duration, long delay, AnimatorListener listener) {
        startScrollAnimation(null, scroll, duration, delay, listener);
    }

    private void startScrollAnimation(List<Animator> animators, int scroll, long duration, long delay, AnimatorListener listener) {
        if (this.mScrollAnimator != null) {
            this.mScrollAnimator.cancel();
        }
        this.mLastScrollChange = 0;
        ObjectAnimator scrollAnimator = ObjectAnimator.ofInt(this, this.mSetScrollVerticallyProperty, new int[]{0, -scroll});
        if (animators != null) {
            animators.add(scrollAnimator);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(animators);
            this.mScrollAnimator = animatorSet;
        } else {
            this.mScrollAnimator = scrollAnimator;
        }
        this.mScrollAnimator.setDuration(duration);
        if (listener != null) {
            this.mScrollAnimator.addListener(listener);
        }
        if (delay > 0) {
            this.mScrollAnimator.setStartDelay(delay);
        }
        this.mScrollAnimator.start();
    }

    public boolean fling(int velocityX, int velocityY) {
        if (getChildCount() == 0) {
            return false;
        }
        int currentPosition = getChildPosition(getChildAt(findCenterViewIndex()));
        if ((currentPosition == 0 && velocityY < 0) || (currentPosition == getAdapter().getItemCount() - 1 && velocityY > 0)) {
            return super.fling(velocityX, velocityY);
        }
        if (Math.abs(velocityY) < this.mMinFlingVelocity) {
            return false;
        }
        velocityY = Math.max(Math.min(velocityY, this.mMaxFlingVelocity), -this.mMaxFlingVelocity);
        if (this.mScroller == null) {
            this.mScroller = new Scroller(getContext(), null, true);
        }
        this.mScroller.fling(0, 0, 0, velocityY, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
        int delta = this.mScroller.getFinalY() / (getPaddingTop() + (getAdjustedHeight() / 2));
        if (delta == 0) {
            delta = velocityY > 0 ? 1 : -1;
        }
        smoothScrollToPosition(Math.max(0, Math.min(getAdapter().getItemCount() - 1, currentPosition + delta)));
        return true;
    }

    public ViewHolder getChildViewHolder(View child) {
        return (ViewHolder) super.getChildViewHolder(child);
    }

    private int findCenterViewIndex() {
        int count = getChildCount();
        int index = -1;
        int closest = Integer.MAX_VALUE;
        int centerY = getCenterYPos(this);
        for (int i = 0; i < count; i++) {
            int distance = Math.abs(centerY - (getTop() + getCenterYPos(getChildAt(i))));
            if (distance < closest) {
                closest = distance;
                index = i;
            }
        }
        if (index != -1) {
            return index;
        }
        throw new IllegalStateException("Can't find central view.");
    }

    private static int getCenterYPos(View v) {
        return (v.getTop() + v.getPaddingTop()) + (getAdjustedHeight(v) / 2);
    }

    private void handleTouchUp(MotionEvent event, int scrollState) {
        if (this.mCanClick && event != null && checkForTap(event)) {
            this.mPressedHandler.postDelayed(this.mReleasedRunnable, (long) ViewConfiguration.getTapTimeout());
        } else if (scrollState != 0) {
        } else {
            if (isOverScrolling()) {
                this.mOverScrollListener.onOverScroll();
            } else {
                animateToCenter();
            }
        }
    }

    private boolean isOverScrolling() {
        return getChildCount() > 0 && this.mStartFirstTop <= ((float) getCentralViewTop()) && getChildAt(0).getTop() >= getTopViewMaxTop() && this.mOverScrollListener != null;
    }

    private int getTopViewMaxTop() {
        return getHeight() / 2;
    }

    private int getItemHeight() {
        return getAdjustedHeight() / 3;
    }

    private int getCentralViewTop() {
        return getPaddingTop() + getItemHeight();
    }

    public void animateToCenter() {
        startScrollAnimation(getCentralViewTop() - getChildAt(findCenterViewIndex()).getTop(), 150, 0, new C00815());
    }

    private void handleTouchDown(MotionEvent event) {
        if (this.mCanClick) {
            this.mTapPositionX = (int) event.getX();
            this.mTapPositionY = (int) event.getY();
            float rawY = event.getRawY();
            computeTapRegions(this.mTapRegions);
            if (rawY > this.mTapRegions[0] && rawY < this.mTapRegions[1] && (getChildAt(findCenterViewIndex()) instanceof OnCenterProximityListener)) {
                this.mPressedHandler.removeCallbacks(this.mReleasedRunnable);
                this.mPressedHandler.postDelayed(this.mPressedRunnable, (long) ViewConfiguration.getTapTimeout());
            }
        }
    }

    private void setScrollVertically(int scroll) {
        scrollBy(0, scroll - this.mLastScrollChange);
        this.mLastScrollChange = scroll;
    }

    private void notifyChildrenAboutProximity(boolean animate) {
        int count = getChildCount();
        int index = findCenterViewIndex();
        int i = 0;
        while (i < count) {
            getChildViewHolder(getChildAt(i)).onCenterProximity(i == index, animate);
            i++;
        }
        int position = getChildViewHolder(getChildAt(index)).getPosition();
        if (position != this.mPreviousCentral) {
            for (OnScrollListener listener : this.mOnScrollListeners) {
                listener.onCentralPositionChanged(position);
            }
            this.mPreviousCentral = position;
        }
    }

    private int getAdjustedHeight() {
        return getAdjustedHeight(this);
    }

    private static int getAdjustedHeight(View v) {
        return (v.getHeight() - v.getPaddingBottom()) - v.getPaddingTop();
    }

    private void computeTapRegions(float[] tapRegions) {
        int[] iArr = this.mLocation;
        this.mLocation[1] = 0;
        iArr[0] = 0;
        getLocationOnScreen(this.mLocation);
        int mScreenTop = this.mLocation[1];
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        tapRegions[0] = ((float) mScreenTop) + (((float) (metrics.heightPixels - mScreenTop)) * 0.33f);
        tapRegions[1] = ((float) mScreenTop) + (((float) (metrics.heightPixels - mScreenTop)) * 0.66999996f);
    }
}
