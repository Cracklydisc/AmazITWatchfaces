package android.support.wearable.watchface;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.v7.recyclerview.C0051R;

public interface IWatchFaceService extends IInterface {

    public static abstract class Stub extends Binder implements IWatchFaceService {

        private static class Proxy implements IWatchFaceService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void setStyle(WatchFaceStyle style) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("android.support.wearable.watchface.IWatchFaceService");
                    if (style != null) {
                        _data.writeInt(1);
                        style.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public static IWatchFaceService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("android.support.wearable.watchface.IWatchFaceService");
            if (iin == null || !(iin instanceof IWatchFaceService)) {
                return new Proxy(obj);
            }
            return (IWatchFaceService) iin;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    WatchFaceStyle _arg0;
                    data.enforceInterface("android.support.wearable.watchface.IWatchFaceService");
                    if (data.readInt() != 0) {
                        _arg0 = (WatchFaceStyle) WatchFaceStyle.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    setStyle(_arg0);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("android.support.wearable.watchface.IWatchFaceService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void setStyle(WatchFaceStyle watchFaceStyle) throws RemoteException;
}
