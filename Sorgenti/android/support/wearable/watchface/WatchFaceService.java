package android.support.wearable.watchface;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.service.wallpaper.WallpaperService;
import android.support.wearable.watchface.IWatchFaceService.Stub;
import android.support.wearable.watchface.WatchFaceStyle.Builder;
import android.util.Log;
import android.view.SurfaceHolder;

@TargetApi(21)
public abstract class WatchFaceService extends WallpaperService {

    public abstract class Engine extends android.service.wallpaper.WallpaperService.Engine {
        private boolean mInAmbientMode;
        private int mInterruptionFilter;
        private WatchFaceStyle mLastWatchFaceStyle;
        private final Rect mPeekCardPosition = new Rect(0, 0, 0, 0);
        private final IntentFilter mTimeTickFilter = new IntentFilter("android.intent.action.TIME_TICK");
        private final BroadcastReceiver mTimeTickReceiver = new C00841();
        private boolean mTimeTickRegistered = false;
        private int mUnreadCount;
        private WakeLock mWakeLock;
        private IWatchFaceService mWatchFaceService;
        private WatchFaceStyle mWatchFaceStyle;

        class C00841 extends BroadcastReceiver {
            C00841() {
            }

            public void onReceive(Context context, Intent intent) {
                if (Log.isLoggable("WatchFaceService", 3)) {
                    Log.d("WatchFaceService", "Received time tick.");
                }
                Engine.this.onTimeTick();
            }
        }

        public Engine() {
            super(WatchFaceService.this);
        }

        public Bundle onCommand(String action, int x, int y, int z, Bundle extras, boolean resultRequested) {
            if (Log.isLoggable("WatchFaceService", 3)) {
                Log.d("WatchFaceService", "received command: " + action);
            }
            if ("com.google.android.wearable.action.BACKGROUND_ACTION".equals(action)) {
                maybeUpdateAmbientMode(extras);
                maybeUpdateInterruptionFilter(extras);
                maybeUpdatePeekCardPosition(extras);
                maybeUpdateUnreadCount(extras);
            } else if ("com.google.android.wearable.action.AMBIENT_UPDATE".equals(action)) {
                if (this.mInAmbientMode) {
                    if (Log.isLoggable("WatchFaceService", 3)) {
                        Log.d("WatchFaceService", "ambient mode update");
                    }
                    this.mWakeLock.acquire();
                    onTimeTick();
                    this.mWakeLock.acquire(100);
                }
            } else if ("com.google.android.wearable.action.SET_PROPERTIES".equals(action)) {
                onPropertiesChanged(extras);
            } else if ("com.google.android.wearable.action.SET_BINDER".equals(action)) {
                onSetBinder(extras);
            } else if ("com.google.android.wearable.action.REQUEST_STYLE".equals(action)) {
                if (this.mLastWatchFaceStyle != null) {
                    setWatchFaceStyle(this.mLastWatchFaceStyle);
                } else if (Log.isLoggable("WatchFaceService", 3)) {
                    Log.d("WatchFaceService", "Last watch face style is null.");
                }
            }
            return null;
        }

        private void onSetBinder(Bundle extras) {
            IBinder binder = extras.getBinder("binder");
            if (binder != null) {
                this.mWatchFaceService = Stub.asInterface(binder);
                if (this.mWatchFaceStyle != null) {
                    try {
                        this.mWatchFaceService.setStyle(this.mWatchFaceStyle);
                        this.mWatchFaceStyle = null;
                        return;
                    } catch (RemoteException e) {
                        Log.w("WatchFaceService", "Failed to set WatchFaceStyle", e);
                        return;
                    }
                }
                return;
            }
            Log.w("WatchFaceService", "Binder is null.");
        }

        public void setWatchFaceStyle(WatchFaceStyle watchFaceStyle) {
            if (Log.isLoggable("WatchFaceService", 3)) {
                Log.d("WatchFaceService", "setWatchFaceStyle " + watchFaceStyle);
            }
            this.mWatchFaceStyle = watchFaceStyle;
            this.mLastWatchFaceStyle = watchFaceStyle;
            if (this.mWatchFaceService != null) {
                try {
                    this.mWatchFaceService.setStyle(watchFaceStyle);
                    this.mWatchFaceStyle = null;
                } catch (RemoteException e) {
                    Log.e("WatchFaceService", "Failed to set WatchFaceStyle: ", e);
                }
            }
        }

        public void onAmbientModeChanged(boolean inAmbientMode) {
        }

        public void onInterruptionFilterChanged(int interruptionFilter) {
        }

        public void onPeekCardPositionUpdate(Rect rect) {
        }

        public void onUnreadCountChanged(int count) {
        }

        public void onPropertiesChanged(Bundle properties) {
        }

        public void onTimeTick() {
        }

        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);
            this.mWatchFaceStyle = new Builder(WatchFaceService.this).build();
            this.mWakeLock = ((PowerManager) WatchFaceService.this.getSystemService("power")).newWakeLock(1, "WatchFaceService");
            this.mWakeLock.setReferenceCounted(false);
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (Log.isLoggable("WatchFaceService", 3)) {
                Log.d("WatchFaceService", "onVisibilityChanged: " + visible);
            }
            WatchFaceService.this.sendBroadcast(new Intent("com.google.android.wearable.watchfaces.action.REQUEST_STATE"));
        }

        public final boolean isInAmbientMode() {
            return this.mInAmbientMode;
        }

        private void maybeUpdateInterruptionFilter(Bundle bundle) {
            if (bundle.containsKey("interruption_filter")) {
                int interruptionFilter = bundle.getInt("interruption_filter", 1);
                if (interruptionFilter != this.mInterruptionFilter) {
                    this.mInterruptionFilter = interruptionFilter;
                    onInterruptionFilterChanged(interruptionFilter);
                }
            }
        }

        private void maybeUpdatePeekCardPosition(Bundle bundle) {
            if (bundle.containsKey("card_location")) {
                Rect rect = Rect.unflattenFromString(bundle.getString("card_location"));
                if (!rect.equals(this.mPeekCardPosition)) {
                    this.mPeekCardPosition.set(rect);
                    onPeekCardPositionUpdate(rect);
                }
            }
        }

        private void maybeUpdateAmbientMode(Bundle bundle) {
            if (bundle.containsKey("ambient_mode")) {
                boolean inAmbientMode = bundle.getBoolean("ambient_mode", false);
                if (this.mInAmbientMode != inAmbientMode) {
                    this.mInAmbientMode = inAmbientMode;
                    dispatchAmbientModeChanged();
                }
            }
        }

        private void dispatchAmbientModeChanged() {
            onAmbientModeChanged(this.mInAmbientMode);
            if (!this.mInAmbientMode) {
                if (!this.mTimeTickRegistered) {
                    this.mTimeTickRegistered = true;
                    WatchFaceService.this.registerReceiver(this.mTimeTickReceiver, this.mTimeTickFilter);
                }
                onTimeTick();
            } else if (this.mTimeTickRegistered) {
                this.mTimeTickRegistered = false;
                WatchFaceService.this.unregisterReceiver(this.mTimeTickReceiver);
            }
        }

        private void maybeUpdateUnreadCount(Bundle bundle) {
            if (bundle.containsKey("unread_count")) {
                int unreadCount = bundle.getInt("unread_count", 0);
                if (unreadCount != this.mUnreadCount) {
                    this.mUnreadCount = unreadCount;
                    onUnreadCountChanged(this.mUnreadCount);
                }
            }
        }
    }

    public abstract Engine onCreateEngine();
}
