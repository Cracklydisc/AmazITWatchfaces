package android.support.wearable.watchface;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v7.recyclerview.C0051R;
import java.util.Arrays;

@TargetApi(21)
public class WatchFaceStyle implements Parcelable {
    public static final Creator<WatchFaceStyle> CREATOR = new C00851();
    private final int ambientPeekMode;
    private final int backgroundVisibility;
    private final int cardPeekMode;
    private final int cardProgressMode;
    private final ComponentName component;
    private final int hotwordIndicatorGravity;
    private final int peekOpacityMode;
    private final boolean showSystemUiTime;
    private final boolean showUnreadCountIndicator;
    private final int statusBarGravity;
    private final int viewProtectionMode;

    static class C00851 implements Creator<WatchFaceStyle> {
        C00851() {
        }

        public WatchFaceStyle createFromParcel(Parcel p) {
            return new WatchFaceStyle(p.readBundle());
        }

        public WatchFaceStyle[] newArray(int size) {
            return new WatchFaceStyle[size];
        }
    }

    public static class Builder {
        private int mAmbientPeekMode;
        private int mBackgroundVisibility;
        private int mCardPeekMode;
        private int mCardProgressMode;
        private final ComponentName mComponent;
        private int mHotwordIndicatorGravity;
        private int mPeekOpacityMode;
        private boolean mShowSystemUiTime;
        private boolean mShowUnreadCountIndicator;
        private int mStatusBarGravity;
        private int mViewProtectionMode;

        public Builder(Service service) {
            this(new ComponentName(service, service.getClass()));
        }

        private Builder(ComponentName component) {
            this.mCardPeekMode = 0;
            this.mCardProgressMode = 0;
            this.mBackgroundVisibility = 0;
            this.mShowSystemUiTime = false;
            this.mAmbientPeekMode = 0;
            this.mPeekOpacityMode = 0;
            this.mViewProtectionMode = 0;
            this.mStatusBarGravity = 0;
            this.mHotwordIndicatorGravity = 0;
            this.mShowUnreadCountIndicator = false;
            this.mComponent = component;
        }

        public Builder setCardPeekMode(int peekMode) {
            switch (peekMode) {
                case 0:
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    this.mCardPeekMode = peekMode;
                    return this;
                default:
                    throw new IllegalArgumentException("peekMode must be PEEK_MODE_VARIABLE or PEEK_MODE_SHORT");
            }
        }

        public Builder setBackgroundVisibility(int backgroundVisibility) {
            switch (backgroundVisibility) {
                case 0:
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    this.mBackgroundVisibility = backgroundVisibility;
                    return this;
                default:
                    throw new IllegalArgumentException("backgroundVisibility must be BACKGROUND_VISIBILITY_INTERRUPTIVE or BACKGROUND_VISIBILITY_PERSISTENT");
            }
        }

        public Builder setShowSystemUiTime(boolean showSystemUiTime) {
            this.mShowSystemUiTime = showSystemUiTime;
            return this;
        }

        public WatchFaceStyle build() {
            return new WatchFaceStyle(this.mComponent, this.mCardPeekMode, this.mCardProgressMode, this.mBackgroundVisibility, this.mShowSystemUiTime, this.mAmbientPeekMode, this.mPeekOpacityMode, this.mViewProtectionMode, this.mStatusBarGravity, this.mHotwordIndicatorGravity, this.mShowUnreadCountIndicator);
        }
    }

    private WatchFaceStyle(ComponentName component, int cardPeekMode, int cardProgressMode, int backgroundVisibility, boolean showSystemUiTime, int ambientPeekMode, int peekOpacityMode, int viewProtectionMode, int statusBarGravity, int hotwordIndicatorGravity, boolean showUnreadCountIndicator) {
        this.component = component;
        this.ambientPeekMode = ambientPeekMode;
        this.backgroundVisibility = backgroundVisibility;
        this.cardPeekMode = cardPeekMode;
        this.cardProgressMode = cardProgressMode;
        this.hotwordIndicatorGravity = hotwordIndicatorGravity;
        this.peekOpacityMode = peekOpacityMode;
        this.showSystemUiTime = showSystemUiTime;
        this.showUnreadCountIndicator = showUnreadCountIndicator;
        this.statusBarGravity = statusBarGravity;
        this.viewProtectionMode = viewProtectionMode;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeBundle(toBundle());
    }

    public WatchFaceStyle(Bundle bundle) {
        this.component = (ComponentName) bundle.getParcelable("component");
        this.ambientPeekMode = bundle.getInt("ambientPeekMode", 0);
        this.backgroundVisibility = bundle.getInt("backgroundVisibility", 1);
        this.cardPeekMode = bundle.getInt("cardPeekMode", 0);
        this.cardProgressMode = bundle.getInt("cardProgressMode", 0);
        this.hotwordIndicatorGravity = bundle.getInt("hotwordIndicatorGravity");
        this.peekOpacityMode = bundle.getInt("peekOpacityMode", 0);
        this.showSystemUiTime = bundle.getBoolean("showSystemUiTime");
        this.showUnreadCountIndicator = bundle.getBoolean("showUnreadIndicator");
        this.statusBarGravity = bundle.getInt("statusBarGravity");
        this.viewProtectionMode = bundle.getInt("viewProtectionMode");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("component", this.component);
        bundle.putInt("ambientPeekMode", this.ambientPeekMode);
        bundle.putInt("backgroundVisibility", this.backgroundVisibility);
        bundle.putInt("cardPeekMode", this.cardPeekMode);
        bundle.putInt("cardProgressMode", this.cardProgressMode);
        bundle.putInt("hotwordIndicatorGravity", this.hotwordIndicatorGravity);
        bundle.putInt("peekOpacityMode", this.peekOpacityMode);
        bundle.putBoolean("showSystemUiTime", this.showSystemUiTime);
        bundle.putBoolean("showUnreadIndicator", this.showUnreadCountIndicator);
        bundle.putInt("statusBarGravity", this.statusBarGravity);
        bundle.putInt("viewProtectionMode", this.viewProtectionMode);
        return bundle;
    }

    public boolean equals(Object otherObj) {
        if (otherObj == null || !(otherObj instanceof WatchFaceStyle)) {
            return false;
        }
        WatchFaceStyle other = (WatchFaceStyle) otherObj;
        if (this.component.equals(other.component) && this.cardPeekMode == other.cardPeekMode && this.cardProgressMode == other.cardProgressMode && this.backgroundVisibility == other.backgroundVisibility && this.showSystemUiTime == other.showSystemUiTime && this.ambientPeekMode == other.ambientPeekMode && this.peekOpacityMode == other.peekOpacityMode && this.viewProtectionMode == other.viewProtectionMode && this.statusBarGravity == other.statusBarGravity && this.hotwordIndicatorGravity == other.hotwordIndicatorGravity && this.showUnreadCountIndicator == other.showUnreadCountIndicator) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i;
        int i2 = 1;
        int[] values = new int[11];
        values[0] = this.component.hashCode();
        values[1] = this.cardPeekMode;
        values[2] = this.cardProgressMode;
        values[3] = this.backgroundVisibility;
        if (this.showSystemUiTime) {
            i = 1;
        } else {
            i = 0;
        }
        values[4] = i;
        values[5] = this.ambientPeekMode;
        values[6] = this.peekOpacityMode;
        values[7] = this.viewProtectionMode;
        values[8] = this.statusBarGravity;
        values[9] = this.hotwordIndicatorGravity;
        if (!this.showUnreadCountIndicator) {
            i2 = 0;
        }
        values[10] = i2;
        return Arrays.hashCode(values);
    }

    public String toString() {
        String str = "watch face %s (card %d/%d bg %d time %s ambientPeek %d peekOpacityMode %d viewProtectionMode %d  statusBarGravity %d hotwordIndicatorGravity %d showUnreadCountIndicator %s)";
        Object[] objArr = new Object[11];
        objArr[0] = this.component == null ? "default" : this.component.getShortClassName();
        objArr[1] = Integer.valueOf(this.cardPeekMode);
        objArr[2] = Integer.valueOf(this.cardProgressMode);
        objArr[3] = Integer.valueOf(this.backgroundVisibility);
        objArr[4] = Boolean.valueOf(this.showSystemUiTime);
        objArr[5] = Integer.valueOf(this.ambientPeekMode);
        objArr[6] = Integer.valueOf(this.peekOpacityMode);
        objArr[7] = Integer.valueOf(this.viewProtectionMode);
        objArr[8] = Integer.valueOf(this.statusBarGravity);
        objArr[9] = Integer.valueOf(this.hotwordIndicatorGravity);
        objArr[10] = Boolean.valueOf(this.showUnreadCountIndicator);
        return String.format(str, objArr);
    }
}
