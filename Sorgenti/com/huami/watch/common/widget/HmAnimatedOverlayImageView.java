package com.huami.watch.common.widget;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import java.util.HashMap;

public class HmAnimatedOverlayImageView extends ImageView {
    private static final HashMap<Integer, Class<? extends HmAbstractOverlayAnimatorDrawable>> sRegisterAnimDrawableTypes = new HashMap();

    public static abstract class HmAbstractOverlayAnimatorDrawable extends Drawable {
    }
}
