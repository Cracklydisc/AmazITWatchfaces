package com.huami.watch.common.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewOverlay;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import java.util.ArrayList;

public class HmHaloButton extends Button {
    private static final Interpolator QUAD_OUT = new C00921();
    private float SCALE_FROM = 1.0f;
    private float SCALE_TO = 0.9f;
    private boolean clickFinished = true;
    private Runnable clickTask = new C00943();
    private HmOverlayClickHaloDrawble mAnimateDrawable = new HmOverlayClickHaloDrawble();
    private float mContentRatio = 0.78431374f;
    private OnClickListener mOnClickListener;
    private View mOverlayParent;
    private boolean withHalo = true;

    static class C00921 implements Interpolator {
        C00921() {
        }

        public float getInterpolation(float input) {
            return (-input) * (input - 2.0f);
        }
    }

    class C00943 implements Runnable {
        C00943() {
        }

        public void run() {
            if (HmHaloButton.this.mOnClickListener != null) {
                HmHaloButton.this.mOnClickListener.onClick(HmHaloButton.this);
            }
            HmHaloButton.this.clickFinished = true;
        }
    }

    class C00954 implements OnClickListener {
        C00954() {
        }

        public void onClick(View v) {
            if (HmHaloButton.this.clickFinished) {
                HmHaloButton.this.playClickAnimation(HmHaloButton.this.clickTask);
                HmHaloButton.this.clickFinished = false;
            }
        }
    }

    public HmHaloButton(Context context) {
        super(context);
        initClickTask();
    }

    public HmHaloButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initClickTask();
    }

    public HmHaloButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initClickTask();
    }

    public HmHaloButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initClickTask();
    }

    private View getVoverlayViewContainer() {
        if (this.mOverlayParent != null) {
            return this.mOverlayParent;
        }
        this.mOverlayParent = getRootView();
        return this.mOverlayParent;
    }

    protected void onDetachedFromWindow() {
        cancelOverlayDrawableAnimation();
    }

    private void playOverlayDrawableAnimation(final Runnable task) {
        if (this.mAnimateDrawable != null) {
            final ViewOverlay viewOverlay = getVoverlayViewContainer().getOverlay();
            ValueAnimator animator = this.mAnimateDrawable.getOverlayAnmiator();
            animator.cancel();
            if (task == null || animator.getRepeatCount() != -1) {
                if (this.withHalo) {
                    setOverlayDrawableBounds(this.mAnimateDrawable);
                }
                viewOverlay.add(this.mAnimateDrawable);
                animator.addListener(new AnimatorListener() {
                    boolean notCancel = true;

                    public void onAnimationEnd(Animator animator) {
                        animator.removeListener(this);
                        viewOverlay.remove(HmHaloButton.this.mAnimateDrawable);
                        if (task != null && this.notCancel) {
                            task.run();
                        }
                    }

                    public void onAnimationCancel(Animator animation) {
                        this.notCancel = false;
                    }

                    public void onAnimationStart(Animator animation) {
                        this.notCancel = true;
                    }

                    public void onAnimationRepeat(Animator animation) {
                    }
                });
                animator.start();
                return;
            }
            throw new IllegalArgumentException("Can not setup the task on the animator end, the animator will be run infinite.");
        }
    }

    private void cancelOverlayDrawableAnimation() {
        if (this.mAnimateDrawable != null) {
            this.mAnimateDrawable.cancelOverlayAnimator();
        }
    }

    private void setOverlayDrawableBounds(HmOverlayClickHaloDrawble d) {
        View overlayContainerView = getVoverlayViewContainer();
        Rect bounds = new Rect();
        getDescendantRectRelativeToParent(this, overlayContainerView, bounds);
        int width = bounds.width();
        float expand = ((this.mContentRatio / d.getContentRatio()) - 1.0f) * 0.5f;
        int hOffset = (int) Math.ceil((double) (((float) width) * expand));
        int vOffset = (int) Math.ceil((double) (((float) bounds.height()) * expand));
        bounds.left -= hOffset;
        bounds.top -= vOffset;
        bounds.right += hOffset;
        bounds.bottom += vOffset;
        d.setBounds(bounds);
    }

    private void initClickTask() {
        if (!isClickable()) {
            setClickable(true);
        }
        this.clickFinished = true;
        super.setOnClickListener(new C00954());
    }

    public void playClickAnimation(final Runnable callback) {
        clearAnimation();
        ScaleAnimation mScaleAnim = new ScaleAnimation(this.SCALE_FROM, this.SCALE_TO, this.SCALE_FROM, this.SCALE_TO, 1, 0.5f, 1, 0.5f);
        mScaleAnim.setInterpolator(QUAD_OUT);
        mScaleAnim.setDuration(125);
        mScaleAnim.setRepeatCount(1);
        mScaleAnim.setRepeatMode(2);
        mScaleAnim.setAnimationListener(new AnimationListener() {
            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
                HmHaloButton.this.playOverlayDrawableAnimation(callback);
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startAnimation(mScaleAnim);
    }

    public void setOnClickListener(OnClickListener l) {
        this.mOnClickListener = l;
    }

    private static float getDescendantRectRelativeToParent(View descendant, View root, Rect r) {
        int[] mTmpXY = new int[]{0, 0};
        float scale = getDescendantCoordRelativeToParent(descendant, root, mTmpXY, false);
        r.set(mTmpXY[0], mTmpXY[1], (int) (((float) mTmpXY[0]) + (((float) descendant.getMeasuredWidth()) * scale)), (int) (((float) mTmpXY[1]) + (((float) descendant.getMeasuredHeight()) * scale)));
        return scale;
    }

    private static float getDescendantCoordRelativeToParent(View descendant, View parentView, int[] coord, boolean includeRootScroll) {
        ArrayList<View> ancestorChain = new ArrayList();
        float[] pt = new float[]{(float) coord[0], (float) coord[1]};
        View v = descendant;
        while (v != parentView && v != null) {
            ancestorChain.add(v);
            v = (View) v.getParent();
        }
        float scale = 1.0f;
        int count = ancestorChain.size();
        for (int i = 0; i < count; i++) {
            View v0 = (View) ancestorChain.get(i);
            if (v0 != descendant || includeRootScroll) {
                pt[0] = pt[0] - ((float) v0.getScrollX());
                pt[1] = pt[1] - ((float) v0.getScrollY());
            }
            v0.getMatrix().mapPoints(pt);
            pt[0] = pt[0] + ((float) v0.getLeft());
            pt[1] = pt[1] + ((float) v0.getTop());
            scale *= v0.getScaleX();
        }
        coord[0] = Math.round(pt[0]);
        coord[1] = Math.round(pt[1]);
        return scale;
    }
}
