package com.huami.watch.common.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.huami.watch.utils.QuadInterpolator;

public class HmImageView extends ImageView {
    private static final QuadInterpolator QUAD_OUT = new QuadInterpolator((byte) 1);
    private AnimatorUpdateListener mAnimatorUpdateListener = new C00971();
    private boolean mClickAnimEnable = false;
    private float mScale = 1.0f;

    class C00971 implements AnimatorUpdateListener {
        C00971() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            HmImageView.this.mScale = ((Float) animation.getAnimatedValue()).floatValue();
            HmImageView.this.setScaleX(HmImageView.this.mScale);
            HmImageView.this.setScaleY(HmImageView.this.mScale);
        }
    }

    public HmImageView(Context context) {
        super(context);
    }

    public HmImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
