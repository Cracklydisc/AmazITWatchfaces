package com.huami.watch.common.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class HmLinearLayout extends LinearLayout {
    private boolean mEnableHalo = false;
    private float mHaloCenterX;
    private float mHaloCenterY;
    private float mHaloRadius;
    private Paint paint;

    public HmLinearLayout(Context context) {
        super(context);
    }

    public HmLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HmLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mEnableHalo) {
            canvas.drawCircle(this.mHaloCenterX, this.mHaloCenterY, this.mHaloRadius, this.paint);
        }
    }
}
