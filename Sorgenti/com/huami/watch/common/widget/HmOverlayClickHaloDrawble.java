package com.huami.watch.common.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import com.huami.watch.common.widget.HmAnimatedOverlayImageView.HmAbstractOverlayAnimatorDrawable;
import com.huami.watch.utils.QuadInterpolator;

public class HmOverlayClickHaloDrawble extends HmAbstractOverlayAnimatorDrawable {
    private int coordinateX;
    private int coordinateY;
    private Paint mPaint = new Paint(1);
    private float mRadius;
    private float mRadiusEnd;
    private float mRadiusStart;
    private ValueAnimator valueAnimator;

    class C00981 implements AnimatorUpdateListener {
        C00981() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            float input = ((Float) animation.getAnimatedValue()).floatValue();
            HmOverlayClickHaloDrawble.this.mRadius = HmOverlayClickHaloDrawble.this.mRadiusStart + ((HmOverlayClickHaloDrawble.this.mRadiusEnd - HmOverlayClickHaloDrawble.this.mRadiusStart) * input);
            HmOverlayClickHaloDrawble.this.mPaint.setAlpha((int) ((1.0f - input) * 180.0f));
            HmOverlayClickHaloDrawble.this.invalidateSelf();
        }
    }

    public HmOverlayClickHaloDrawble() {
        this.mPaint.setColor(-1);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeWidth(2.0f);
        this.mPaint.setAlpha(180);
        this.valueAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        this.valueAnimator.setInterpolator(new QuadInterpolator((byte) 1));
        this.valueAnimator.setDuration(330);
        this.valueAnimator.addUpdateListener(new C00981());
    }

    public float getContentRatio() {
        return 0.71428573f;
    }

    public void draw(Canvas canvas) {
        if (this.valueAnimator.isRunning()) {
            canvas.drawCircle((float) this.coordinateX, (float) this.coordinateY, this.mRadius, this.mPaint);
        }
    }

    public void setAlpha(int alpha) {
    }

    public void setColorFilter(ColorFilter cf) {
    }

    public int getOpacity() {
        return 0;
    }

    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        int r = (right - left) / 2;
        this.mRadiusStart = ((float) r) * 0.64285713f;
        this.mRadiusEnd = ((float) r) * 1.0f;
        this.coordinateX = left + r;
        this.coordinateY = top + r;
    }

    public ValueAnimator getOverlayAnmiator() {
        return this.valueAnimator;
    }

    public void cancelOverlayAnimator() {
        if (this.valueAnimator.isRunning()) {
            this.valueAnimator.cancel();
        }
    }
}
