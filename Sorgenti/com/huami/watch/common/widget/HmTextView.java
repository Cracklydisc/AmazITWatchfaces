package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class HmTextView extends TextView {
    public HmTextView(Context context) {
        super(context);
    }

    public HmTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HmTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HmTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
