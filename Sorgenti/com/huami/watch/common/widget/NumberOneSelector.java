package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.NumberView.OnValueChangeListener;
import com.huami.watch.ui.C0126R;

public class NumberOneSelector extends LinearLayout implements OnValueChangeListener {
    private boolean canAccept;
    private OnTouchListener clickListener;
    private SelectView currentView;
    private NumberView mHour;
    private int mHoureIndex;
    private String[] mHours;

    class C00992 implements OnTouchListener {
        C00992() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            int i = v.getId();
            if (i == C0126R.id.picker1) {
                NumberOneSelector.this.setCurrentView(SelectView.HOUR);
            } else if (i == C0126R.id.picker2) {
                NumberOneSelector.this.setCurrentView(SelectView.MINUTES);
            }
            return false;
        }
    }

    public enum SelectView {
        HOUR,
        MINUTES,
        AM_OR_PM
    }

    public NumberOneSelector(Context context) {
        this(context, null);
    }

    public NumberOneSelector(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberOneSelector(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mHoureIndex = 0;
        this.currentView = SelectView.HOUR;
        this.canAccept = true;
        this.clickListener = new C00992();
        initContent();
    }

    public void setCurrentView(SelectView currentView) {
        this.currentView = currentView;
        if (currentView == SelectView.HOUR) {
            this.mHour.setHasFocused(true);
        }
    }

    private void initContent() {
        this.mHours = new String[0];
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mHour = (NumberView) findViewById(C0126R.id.picker1);
        this.mHour.setOnValueChangedListener(this);
    }

    public void onValueChange(NumberView picker, int oldVal, int newVal) {
    }
}
