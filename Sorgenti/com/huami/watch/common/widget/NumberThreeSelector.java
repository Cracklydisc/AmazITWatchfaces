package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.NumberTwoSelector.SelectView;
import com.huami.watch.common.widget.NumberView.OnValueChangeListener;
import com.huami.watch.ui.C0126R;

public class NumberThreeSelector extends LinearLayout implements OnValueChangeListener {
    private boolean canAccept;
    private OnTouchListener clickListener;
    private SelectView currentView;
    private NumberView mAmPm;
    private int mAmPmIndex;
    private String[] mAmorPm;
    private NumberView mHour;
    private int mHoureIndex;
    private String[] mHours;
    private String[] mMintues;
    private NumberView mMinute;
    private int mMinuteIndex;

    class C01002 implements OnTouchListener {
        C01002() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            int i = v.getId();
            if (i == C0126R.id.picker1) {
                NumberThreeSelector.this.setCurrentView(SelectView.HOUR);
            } else if (i == C0126R.id.picker2) {
                NumberThreeSelector.this.setCurrentView(SelectView.MINUTES);
            } else if (i == C0126R.id.picker_ampm) {
                NumberThreeSelector.this.setCurrentView(SelectView.AM_OR_PM);
            }
            return false;
        }
    }

    public NumberThreeSelector(Context context) {
        this(context, null);
    }

    public NumberThreeSelector(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberThreeSelector(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mHoureIndex = 0;
        this.mMinuteIndex = 0;
        this.mAmPmIndex = 0;
        this.currentView = SelectView.AM_OR_PM;
        this.canAccept = true;
        this.clickListener = new C01002();
        initContent();
    }

    public void setCurrentView(SelectView currentView) {
        this.currentView = currentView;
        if (currentView == SelectView.HOUR) {
            this.mHour.setHasFocused(true);
            this.mMinute.setHasFocused(false);
            this.mAmPm.setHasFocused(false);
        } else if (currentView == SelectView.MINUTES) {
            this.mHour.setHasFocused(false);
            this.mMinute.setHasFocused(true);
            this.mAmPm.setHasFocused(false);
        } else if (currentView == SelectView.AM_OR_PM) {
            this.mHour.setHasFocused(false);
            this.mMinute.setHasFocused(false);
            this.mAmPm.setHasFocused(true);
        }
    }

    private void initContent() {
        this.mAmorPm = new String[0];
        this.mHours = new String[0];
        this.mMintues = new String[0];
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mAmPm = (NumberView) findViewById(C0126R.id.picker_ampm);
        this.mHour = (NumberView) findViewById(C0126R.id.picker1);
        this.mMinute = (NumberView) findViewById(C0126R.id.picker2);
    }

    public void onValueChange(NumberView picker, int oldVal, int newVal) {
    }
}
