package com.huami.watch.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import com.huami.watch.common.widget.NumberView.OnValueChangeListener;
import com.huami.watch.ui.C0126R;

public class NumberTwoSelector extends LinearLayout implements OnValueChangeListener {
    private boolean canAccept;
    private OnTouchListener clickListener;
    private SelectView currentView;
    private NumberView mHour;
    private int mHoureIndex;
    private String[] mHours;
    private String[] mMintues;
    private NumberView mMinute;
    private int mMinuteIndex;

    class C01012 implements OnTouchListener {
        C01012() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            int i = v.getId();
            if (i == C0126R.id.picker1) {
                NumberTwoSelector.this.setCurrentView(SelectView.HOUR);
            } else if (i == C0126R.id.picker2) {
                NumberTwoSelector.this.setCurrentView(SelectView.MINUTES);
            }
            return false;
        }
    }

    public enum SelectView {
        HOUR,
        MINUTES,
        AM_OR_PM
    }

    public NumberTwoSelector(Context context) {
        this(context, null);
    }

    public NumberTwoSelector(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberTwoSelector(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mHoureIndex = 0;
        this.mMinuteIndex = 0;
        this.currentView = SelectView.HOUR;
        this.canAccept = true;
        this.clickListener = new C01012();
        initContent();
    }

    public void setCurrentView(SelectView currentView) {
        this.currentView = currentView;
        if (currentView == SelectView.HOUR) {
            this.mHour.setHasFocused(true);
            this.mMinute.setHasFocused(false);
        } else if (currentView == SelectView.MINUTES) {
            this.mHour.setHasFocused(false);
            this.mMinute.setHasFocused(true);
        }
    }

    private void initContent() {
        this.mHours = new String[0];
        this.mMintues = new String[0];
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mHour = (NumberView) findViewById(C0126R.id.picker1);
        this.mMinute = (NumberView) findViewById(C0126R.id.picker2);
        this.mHour.setOnValueChangedListener(this);
        this.mMinute.setOnValueChangedListener(this);
    }

    public void onValueChange(NumberView picker, int oldVal, int newVal) {
    }
}
