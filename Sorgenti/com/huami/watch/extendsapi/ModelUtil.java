package com.huami.watch.extendsapi;

import android.content.Context;
import android.os.SystemProperties;

public class ModelUtil {
    private static final String[] everestModels = new String[]{"A1609", "A1619", "A1602", "A1612"};
    private static final String[] everestModels_real = new String[]{"A1609", "A1619"};
    private static final String[] huangheModels_2 = new String[]{"A1602", "A1612"};

    public static boolean isRealModelEverest(Context context) {
        String model = getSystemProperty(context, "ro.build.huami.model");
        for (String m : everestModels_real) {
            if (m.equalsIgnoreCase(model)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isModelHuanghe(Context context) {
        String model = getSystemProperty(context, "ro.build.huami.model");
        for (String m : huangheModels_2) {
            if (m.equalsIgnoreCase(model)) {
                return true;
            }
        }
        return false;
    }

    private static String getSystemProperty(Context context, String key) throws IllegalArgumentException {
        return SystemProperties.get(key, "");
    }
}
