package com.huami.watch.indicator;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.wearable.view.SimpleAnimatorListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import com.huami.watch.ui.C0126R;
import com.huami.watch.utils.Gloable;
import java.util.Map;

public class ViewPagerPageIndicator extends View implements OnPageChangeListener {
    public static final boolean DEBUG = Gloable.DEBUG;
    private final Rect dirtyRect;
    private boolean isScroll;
    private boolean isShow;
    public float mAllArc;
    private int mAniationYOffset;
    public float mArc_Angle;
    float mCenterX;
    float mCenterY;
    float mCircle_Radius;
    float[] mCircle_X;
    float[] mCirlce_Y;
    private int mColorFocused;
    private int mColorNormal;
    private Orientation mCurrentOrientation;
    private DataSetObserver mDataSetObserver;
    private int mDelta;
    private boolean mDirty;
    public float mEvery_ArcAngle;
    private ValueAnimator mIndicatorAnimator;
    private float mIndicatorCenterDistance;
    private float mIndicatorRadius;
    private float mIndicatorRadiusFocused;
    private Orientation mLastOrientation;
    private int mNumPages;
    private PagerAdapter mPagerAdapter;
    private Paint mPaint;
    private int mSelectedPage;
    public float mSetoff;
    public float mStartAngle;
    private ViewPager mViewPager;
    private Map<Integer, Bitmap> specialIndicatorMap;

    class C01081 extends SimpleAnimatorListener {
        C01081() {
        }

        public void onAnimationComplete(Animator animator) {
            super.onAnimationComplete(animator);
        }

        public void onAnimationEnd(Animator animator) {
            super.onAnimationEnd(animator);
        }
    }

    class C01092 implements AnimatorUpdateListener {
        C01092() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            ViewPagerPageIndicator.this.invalidate(ViewPagerPageIndicator.this.dirtyRect);
        }
    }

    public enum Orientation {
        LET(3),
        TOP(0),
        RIGHT(1),
        BOTTOM(2);
        
        private int mIndex;

        private Orientation(int index) {
            this.mIndex = index;
        }

        private int getIndex() {
            return this.mIndex;
        }
    }

    private class PagerObserver extends DataSetObserver {
        private PagerObserver() {
        }

        public void onChanged() {
            ViewPagerPageIndicator.this.dataSetChanged();
        }

        public void onInvalidated() {
            ViewPagerPageIndicator.this.dataSetChanged();
        }
    }

    public ViewPagerPageIndicator(Context context) {
        this(context, null);
    }

    public ViewPagerPageIndicator(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public ViewPagerPageIndicator(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        this.mCurrentOrientation = Orientation.TOP;
        this.mLastOrientation = this.mCurrentOrientation;
        this.mSetoff = 0.0f;
        this.mDirty = true;
        this.isShow = false;
        this.mAniationYOffset = 10;
        this.mDelta = 5;
        this.dirtyRect = new Rect();
        init();
        initAnimation();
    }

    public static float circleY(float screenCircleRadius, int mNum, float mIndicatorRadiusFocused, float startAngle, float centerY, float every_ArcAngle, Orientation orientation, float setoff) {
        float num_Ang = (((((float) orientation.getIndex()) * 90.0f) + startAngle) + (((float) mNum) * every_ArcAngle)) + (setoff * every_ArcAngle);
        if (DEBUG) {
            Log.i("scroll", "  num_Ang" + num_Ang);
            Log.i("scroll", "  setoff*every_ArcAngle" + (setoff * every_ArcAngle));
        }
        float coordinate_Y = (screenCircleRadius - mIndicatorRadiusFocused) * ((float) Math.sin((((double) num_Ang) * 3.141592653589793d) / 180.0d));
        if (orientation == Orientation.LET) {
            return centerY + coordinate_Y;
        }
        return centerY - coordinate_Y;
    }

    public static float circleX(float screenCircleRadius, int mNum, float mIndicatorRadiusFocused, float startAngle, float center_x, float every_ArcAngle, Orientation orientation, float setoff) {
        float coordinate_X = (screenCircleRadius - mIndicatorRadiusFocused) * ((float) Math.cos((((double) ((((((float) orientation.getIndex()) * 90.0f) + startAngle) + (((float) mNum) * every_ArcAngle)) + (setoff * every_ArcAngle))) * 3.141592653589793d) / 180.0d));
        if (startAngle >= 90.0f) {
            if (orientation == Orientation.BOTTOM) {
                return center_x - coordinate_X;
            }
            return center_x + coordinate_X;
        } else if (orientation == Orientation.BOTTOM) {
            return center_x + coordinate_X;
        } else {
            return center_x - coordinate_X;
        }
    }

    private void init() {
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(Style.FILL);
        this.mColorFocused = getResources().getColor(C0126R.color.cw_page_indicator_focused);
        this.mColorNormal = getResources().getColor(C0126R.color.cw_page_indicator);
        this.mPaint.setColor(this.mColorNormal);
        this.mIndicatorRadius = getResources().getDimension(C0126R.dimen.indicator_radius);
        this.mIndicatorRadiusFocused = getResources().getDimension(C0126R.dimen.indicator_radius_focused);
        this.mIndicatorCenterDistance = getResources().getDimension(C0126R.dimen.indicator_center_distance);
    }

    protected void onDraw(Canvas canvas) {
        if (this.isShow) {
            super.onDraw(canvas);
            if (this.mCurrentOrientation != this.mLastOrientation || this.mCircle_X == null || this.mCirlce_Y == null || this.mDirty) {
                this.mDirty = false;
                this.mLastOrientation = this.mCurrentOrientation;
                this.mCircle_X = arrayX(this.mCircle_Radius, this.mNumPages, this.mIndicatorRadius, this.mStartAngle, this.mEvery_ArcAngle);
                this.mCirlce_Y = arrayY(this.mCircle_Radius, this.mNumPages, this.mIndicatorRadius, this.mStartAngle, this.mEvery_ArcAngle);
            }
            for (int i = 0; i < this.mNumPages; i++) {
                if (i != this.mSelectedPage || this.isScroll) {
                    this.mPaint.setColor(this.mColorNormal);
                } else {
                    this.mPaint.setColor(this.mColorFocused);
                }
                float radius = this.mIndicatorRadius;
                if (this.mIndicatorAnimator.isRunning()) {
                    int offset = this.mAniationYOffset + (this.mNumPages * this.mDelta);
                    int multiple = 255 / offset;
                    int remainder = 255 % offset;
                    int currentOffset = ((Integer) this.mIndicatorAnimator.getAnimatedValue()).intValue();
                    int alpha = currentOffset;
                    if (i != this.mSelectedPage || this.isScroll) {
                        this.mPaint.setColor(this.mColorNormal);
                        this.mPaint.setAlpha(((alpha * multiple) / 3) + remainder);
                    } else {
                        this.mPaint.setColor(this.mColorFocused);
                        this.mPaint.setAlpha((alpha * multiple) + remainder);
                        if (DEBUG) {
                            Log.i("scroll", "alpha  is " + ((alpha * multiple) + remainder));
                        }
                    }
                    if (currentOffset - (this.mDelta * i) > 0) {
                        float temp = this.mCirlce_Y[i];
                        float[] fArr = this.mCirlce_Y;
                        fArr[i] = fArr[i] + ((float) Math.max(0, this.mAniationYOffset - (currentOffset - (this.mDelta * i))));
                        if (getSpecialIcon(i) != null) {
                            canvas.drawBitmap(getSpecialIcon(i), this.mCircle_X[i] - ((float) (getSpecialIcon(i).getWidth() / 2)), this.mCirlce_Y[i] - ((float) (getSpecialIcon(i).getHeight() / 2)), this.mPaint);
                        } else {
                            canvas.drawCircle(this.mCircle_X[i], this.mCirlce_Y[i], radius, this.mPaint);
                        }
                        this.mCirlce_Y[i] = temp;
                    }
                } else {
                    if (getSpecialIcon(i) != null) {
                        canvas.drawBitmap(getSpecialIcon(i), this.mCircle_X[i] - ((float) (getSpecialIcon(i).getWidth() / 2)), this.mCirlce_Y[i] - ((float) (getSpecialIcon(i).getHeight() / 2)), this.mPaint);
                    } else {
                        canvas.drawCircle(this.mCircle_X[i], this.mCirlce_Y[i], radius, this.mPaint);
                    }
                    if (DEBUG) {
                        Log.i("scroll", " mCircle_X[i]            :" + this.mCircle_X[i] + "     mCirlce_Y[i]  :" + this.mCirlce_Y[i]);
                    }
                }
            }
            if (this.isScroll) {
                this.mPaint.setColor(this.mColorFocused);
                float scrollX = circleX(this.mCircle_Radius, this.mSelectedPage, this.mIndicatorRadius, this.mStartAngle, this.mCenterX, this.mEvery_ArcAngle, this.mCurrentOrientation, this.mSetoff);
                float scrollY = circleY(this.mCircle_Radius, this.mSelectedPage, this.mIndicatorRadius, this.mStartAngle, this.mCenterY, this.mEvery_ArcAngle, this.mCurrentOrientation, this.mSetoff);
                if (DEBUG) {
                    Log.i("scroll", " scrollY             :" + scrollY);
                }
                if (getSpecialIcon(this.mSelectedPage) != null) {
                    canvas.drawBitmap(getSpecialIcon(this.mSelectedPage), scrollX - ((float) (getSpecialIcon(this.mSelectedPage).getWidth() / 2)), scrollY - ((float) (getSpecialIcon(this.mSelectedPage).getHeight() / 2)), this.mPaint);
                    return;
                }
                canvas.drawCircle(scrollX, scrollY, this.mIndicatorRadius, this.mPaint);
            }
        }
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        float half_width = (float) (((right - left) - (getPaddingLeft() + getPaddingRight())) / 2);
        float half_height = (float) (((bottom - top) - (getPaddingBottom() + getPaddingTop())) / 2);
        this.mCenterX = (float) ((right + left) / 2);
        this.mCenterY = (float) ((top + bottom) / 2);
        this.mCircle_Radius = Math.min(half_width, half_height);
        this.mAllArc = ((float) (this.mNumPages - 1)) * this.mIndicatorCenterDistance;
        this.mArc_Angle = (((360.0f * this.mAllArc) / 2.0f) / this.mCircle_Radius) / 3.1415927f;
        this.mStartAngle = 90.0f - (this.mArc_Angle / 2.0f);
        this.mEvery_ArcAngle = this.mArc_Angle / (((float) this.mNumPages) - 1.0f);
        if (DEBUG) {
            Log.d("scroll", "onLayout  mNumPages :" + this.mNumPages + "   mStartAngle :" + this.mStartAngle + "    mEvery_ArcAngle :" + this.mEvery_ArcAngle);
        }
        this.mDirty = true;
        this.dirtyRect.set(left, top, right, top + 18);
    }

    public void onPageScrollStateChanged(int i) {
        if (i == 1) {
            this.isScroll = true;
            if (DEBUG) {
                Log.d("scroll", "isScroll :" + this.isScroll);
            }
        }
    }

    public void onPageScrolled(int pos, float f, int j) {
        this.mSetoff = f;
        this.mSelectedPage = pos;
        invalidate(this.dirtyRect);
    }

    public void onPageSelected(int NumPages) {
        this.mSelectedPage = NumPages;
        invalidate(this.dirtyRect);
    }

    private void setPageCount(int nubmer) {
        this.mDirty = true;
        this.mNumPages = nubmer;
        if (DEBUG) {
            Log.d("scroll", "setPageCount page numbers :" + nubmer);
        }
        requestLayout();
        invalidate(this.dirtyRect);
    }

    public float[] arrayX(float screenCircleRadius, int allNum, float mIndicatorRadiusFocused, float startAngle, float every_ArcAngle) {
        float[] circle_X = new float[allNum];
        for (int i = 0; i < allNum; i++) {
            circle_X[i] = circleX(screenCircleRadius, i, mIndicatorRadiusFocused, startAngle, this.mCenterX, every_ArcAngle, this.mCurrentOrientation, 0.0f);
        }
        return circle_X;
    }

    public float[] arrayY(float screenCircleRadius, int mNumPages, float mIndicatorRadiusFocused, float startAngle, float every_ArcAngle) {
        float[] circle_Y = new float[mNumPages];
        for (int i = 0; i < mNumPages; i++) {
            circle_Y[i] = circleY(screenCircleRadius, i, mIndicatorRadiusFocused, startAngle, this.mCenterY, every_ArcAngle, this.mCurrentOrientation, 0.0f);
            if (DEBUG) {
                Log.i("scroll", "  circle_Y[i]            :" + circle_Y[i]);
            }
        }
        return circle_Y;
    }

    public void setViewPager(ViewPager viewPager) {
        if (viewPager == null) {
            this.mViewPager = null;
            if (this.mPagerAdapter != null) {
                this.mPagerAdapter.unregisterDataSetObserver(this.mDataSetObserver);
            }
            setPageCount(0);
        } else if (viewPager != this.mViewPager) {
            this.mViewPager = null;
            this.mViewPager = viewPager;
            this.mViewPager.addOnPageChangeListener(this);
            if (this.mPagerAdapter != null) {
                this.mPagerAdapter.unregisterDataSetObserver(this.mDataSetObserver);
            }
            if (this.mDataSetObserver == null) {
                this.mDataSetObserver = new PagerObserver();
            }
            this.mPagerAdapter = this.mViewPager.getAdapter();
            if (this.mPagerAdapter == null) {
                throw new RuntimeException("should call ViewPager.setAdpater before setViewPager");
            }
            this.mPagerAdapter.registerDataSetObserver(this.mDataSetObserver);
            setPageCount(this.mPagerAdapter.getCount());
            this.mSelectedPage = this.mViewPager.getCurrentItem();
        }
    }

    protected void dataSetChanged() {
        if (this.mPagerAdapter != null) {
            setPageCount(this.mPagerAdapter.getCount());
        }
    }

    public void showIndicator(boolean isShow) {
        if (this.isShow != isShow) {
            this.isShow = isShow;
            if (this.mNumPages != 0) {
                if (isShow) {
                    if (DEBUG) {
                        Log.i("scroll", "mNumPages :" + this.mNumPages);
                    }
                    this.mIndicatorAnimator.cancel();
                    int offset = this.mAniationYOffset + (this.mNumPages * this.mDelta);
                    this.mIndicatorAnimator.setIntValues(new int[]{0, offset});
                    this.mIndicatorAnimator.setDuration((long) (this.mNumPages * 300));
                    this.mIndicatorAnimator.setInterpolator(new DecelerateInterpolator(2.5f));
                    this.mIndicatorAnimator.start();
                }
                invalidate(this.dirtyRect);
            }
        }
    }

    private void initAnimation() {
        this.mIndicatorAnimator = new ValueAnimator();
        this.mIndicatorAnimator.addListener(new C01081());
        this.mIndicatorAnimator.addUpdateListener(new C01092());
    }

    private Bitmap getSpecialIcon(int position) {
        if (this.specialIndicatorMap == null) {
            return null;
        }
        Bitmap bitmap = (Bitmap) this.specialIndicatorMap.get(Integer.valueOf(position));
        if (bitmap == null) {
            return null;
        }
        return bitmap;
    }
}
