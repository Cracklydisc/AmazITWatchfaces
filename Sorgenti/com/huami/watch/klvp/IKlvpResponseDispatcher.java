package com.huami.watch.klvp;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.v7.recyclerview.C0051R;

public interface IKlvpResponseDispatcher extends IInterface {

    public static abstract class Stub extends Binder implements IKlvpResponseDispatcher {

        private static class Proxy implements IKlvpResponseDispatcher {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void dispatchResponse(KlvpResponse response) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.klvp.IKlvpResponseDispatcher");
                    if (response != null) {
                        _data.writeInt(1);
                        response.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(1, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.huami.watch.klvp.IKlvpResponseDispatcher");
        }

        public static IKlvpResponseDispatcher asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.klvp.IKlvpResponseDispatcher");
            if (iin == null || !(iin instanceof IKlvpResponseDispatcher)) {
                return new Proxy(obj);
            }
            return (IKlvpResponseDispatcher) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    KlvpResponse _arg0;
                    data.enforceInterface("com.huami.watch.klvp.IKlvpResponseDispatcher");
                    if (data.readInt() != 0) {
                        _arg0 = (KlvpResponse) KlvpResponse.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    dispatchResponse(_arg0);
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.klvp.IKlvpResponseDispatcher");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void dispatchResponse(KlvpResponse klvpResponse) throws RemoteException;
}
