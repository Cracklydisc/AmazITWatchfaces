package com.huami.watch.klvp;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class KlvpRequest implements Parcelable {
    public static final Creator<KlvpRequest> CREATOR = new C01101();
    public byte cmd;
    public byte[] configValue;
    public byte msgRemain;
    public short pairId;
    public short target;

    static class C01101 implements Creator<KlvpRequest> {
        C01101() {
        }

        public KlvpRequest createFromParcel(Parcel source) {
            return new KlvpRequest(source);
        }

        public KlvpRequest[] newArray(int size) {
            return new KlvpRequest[size];
        }
    }

    public KlvpRequest(Parcel source) {
        readFromParcel(source);
    }

    private void readFromParcel(Parcel source) {
        this.pairId = (short) source.readInt();
        this.msgRemain = source.readByte();
        this.cmd = source.readByte();
        this.target = (short) source.readInt();
        int len = source.readInt();
        if (len < 0) {
            this.configValue = null;
            return;
        }
        this.configValue = new byte[len];
        source.readByteArray(this.configValue);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.pairId);
        dest.writeByte(this.msgRemain);
        dest.writeByte(this.cmd);
        dest.writeInt(this.target);
        if (this.configValue == null) {
            dest.writeInt(-1);
            return;
        }
        dest.writeInt(this.configValue.length);
        dest.writeByteArray(this.configValue);
    }

    public String toString() {
        String request = "{ pairId = " + this.pairId + " msgRemain = " + this.msgRemain + " cmd = " + this.cmd + " responseCode = " + 0 + " target = " + this.target;
        if (this.configValue == null) {
            return request + " configValue (protobuf data) = null }";
        }
        request = request + " configValue (protobuf data) =";
        for (int i = 0; i < this.configValue.length; i++) {
            request = request + " byte[" + i + "] = " + this.configValue[i];
        }
        return request + " }";
    }
}
