package com.huami.watch.klvp;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class KlvpResponse implements Parcelable {
    public static final Creator<KlvpResponse> CREATOR = new C01111();
    public byte cmd;
    public byte msgRemain;
    public short pairId;
    public byte responseCode;
    public byte[] responseValues;
    public short target;

    static class C01111 implements Creator<KlvpResponse> {
        C01111() {
        }

        public KlvpResponse createFromParcel(Parcel source) {
            return new KlvpResponse(source);
        }

        public KlvpResponse[] newArray(int size) {
            return new KlvpResponse[size];
        }
    }

    public KlvpResponse(Parcel source) {
        readFromParcel(source);
    }

    private void readFromParcel(Parcel source) {
        this.pairId = (short) source.readInt();
        this.msgRemain = source.readByte();
        this.cmd = source.readByte();
        this.responseCode = source.readByte();
        this.target = (short) source.readInt();
        int len = source.readInt();
        if (len < 0) {
            this.responseValues = null;
            return;
        }
        this.responseValues = new byte[len];
        source.readByteArray(this.responseValues);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.pairId);
        dest.writeByte(this.msgRemain);
        dest.writeByte(this.cmd);
        dest.writeByte(this.responseCode);
        dest.writeInt(this.target);
        if (this.responseValues == null) {
            dest.writeInt(-1);
            return;
        }
        dest.writeInt(this.responseValues.length);
        dest.writeByteArray(this.responseValues);
    }

    public String toString() {
        String response = "{ pairId = " + this.pairId + " msgRemain = " + this.msgRemain + " cmd = " + this.cmd + " responseCode = " + this.responseCode + " target = " + this.target;
        if (this.responseValues == null) {
            return response + " responseValues (protobuf data) = null }";
        }
        response = response + " responseValues (protobuf data) =";
        for (int i = 0; i < this.responseValues.length; i++) {
            response = response + " byte[" + i + "] = " + this.responseValues[i];
        }
        return response + " }";
    }
}
