package com.huami.watch.scrollbar;

import android.util.Log;
import android.view.View;
import com.huami.watch.utils.Gloable;
import java.lang.reflect.Field;

public class ArcScrollbarHelper {
    public static final boolean DEGBU = Gloable.DEBUG;

    public static boolean setArcScrollBarDrawable(View scrollbarView) {
        if (DEGBU) {
            Log.d("ArcHelper", "ArcHelper setScrollBarDrawable begin ");
        }
        if (scrollbarView == null) {
            return false;
        }
        int i;
        ArcScrollbarDrawable arcScrollbarDrawable = new ArcScrollbarDrawable(scrollbarView);
        Class<?> scrollClass = scrollbarView.getClass();
        while (!scrollClass.getName().equals("android.view.View")) {
            scrollClass = scrollClass.getSuperclass();
            if (scrollClass == null) {
                return false;
            }
        }
        Field[] field = scrollClass.getDeclaredFields();
        Field scrollFied = null;
        for (i = 0; i < field.length; i++) {
            if (field[i].getName().equals("mScrollCache")) {
                scrollFied = field[i];
                break;
            }
        }
        if (scrollFied == null) {
            return false;
        }
        try {
            scrollFied.setAccessible(true);
            Object obj = scrollFied.get(scrollbarView);
            if (obj == null) {
                return false;
            }
            Field[] field2 = obj.getClass().getDeclaredFields();
            for (i = 0; i < field2.length; i++) {
                if (field2[i].getName().equals("scrollBar")) {
                    field2[i].set(obj, arcScrollbarDrawable);
                    if (DEGBU) {
                        Log.d("ArcHelper", "ArcHelper setScrollBarDrawable end ");
                    }
                    return true;
                }
            }
            return false;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
