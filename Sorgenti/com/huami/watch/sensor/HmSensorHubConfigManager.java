package com.huami.watch.sensor;

import android.content.Context;
import android.hardware.HealthSensorHistoryData;
import android.hardware.IConfigFinishDispatcher;
import android.hardware.IDataDispatcher;
import android.hardware.IDataDispatcher.Stub;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import android.util.SparseArray;
import com.google.protobuf.nano.InvalidProtocolBufferNanoException;
import com.huami.watch.klvp.IKlvpResponseDispatcher;
import com.huami.watch.klvp.KlvpResponse;
import com.huami.watch.sensor.HmSensorManager.HealthSensorHistory;
import com.huami.watch.sensorhub.SensorHubProtos.SportStatistics;
import java.util.HashMap;

public class HmSensorHubConfigManager {
    private static final boolean DEBUG = Log.isLoggable("HmSensorHubManager", 3);
    private static volatile HmSensorHubConfigManager sInstance = null;
    private static IHmSensorHubService sService = null;
    private IBinder mCallback = new Binder();
    private final IConfigFinishDispatcher mConfigFinishDispatcher = new C01143();
    private HashMap<String, OnConfigFinishListener> mConfigFinishIdListenerMap = new HashMap();
    private final Object mConfigFinishListenerLock = new Object();
    private Context mContext;
    private HashMap<Long, OnDataReadyListener> mDataReadyListenerMap = new HashMap();
    private EventHandlerDelegate mFinishEventHandlerDelegate;
    private final IDataDispatcher mHealthDataDispatcher = new C01121();
    private final IKlvpResponseDispatcher mKlvpResponseDispatcher = new C01132();
    private SparseArray<Integer> mPairIdTypeMap = new SparseArray();
    private SparseArray<OnKlvpDataListener> mRealTimeDataListenerMap = new SparseArray();

    class C01121 extends Stub {
        C01121() {
        }

        public void dispatchData(long sessionId, HealthSensorHistoryData data) {
            HealthSensorHistory localData = null;
            if (data != null) {
                localData = new HealthSensorHistory(data.id, data.data);
            }
            if (!HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().sendMessage(HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().obtainMessage(1, new DataInfo(sessionId, localData)))) {
                OnDataReadyListener dataListener = HmSensorHubConfigManager.this.findDataReadyListener(sessionId);
                Log.w("HmSensorHubManager", "callback thread has exit when dispatch mHealthData");
                if (dataListener != null) {
                    dataListener.onDataReady(-2, null);
                    HmSensorHubConfigManager.this.unregisterDataReadyListener(sessionId);
                    return;
                }
                Log.w("HmSensorHubManager", "null listener!!");
            }
        }
    }

    class C01132 extends IKlvpResponseDispatcher.Stub {
        C01132() {
        }

        public void dispatchResponse(KlvpResponse response) {
            if (!HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().sendMessage(HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().obtainMessage(2, response))) {
                Log.w("HmSensorHubManager", "send message failed when dispatch klvp response data");
            }
        }
    }

    class C01143 extends IConfigFinishDispatcher.Stub {
        C01143() {
        }

        public void dispatchConfigFinish(int resultCode, String msg, String clientId) {
            HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().sendMessage(HmSensorHubConfigManager.this.mFinishEventHandlerDelegate.getHandler().obtainMessage(0, new ConfigFinishInfo(resultCode, msg, clientId)));
        }
    }

    class ConfigFinishInfo {
        public String clientId;
        public String msg;
        public int resultCode;

        public ConfigFinishInfo(int resultCode, String msg, String clientId) {
            this.resultCode = resultCode;
            this.msg = msg;
            this.clientId = clientId;
        }
    }

    class DataInfo {
        public HealthSensorHistory data;
        public long sessionId;

        public DataInfo(long sessionId, HealthSensorHistory data) {
            this.sessionId = sessionId;
            this.data = data;
        }
    }

    private class EventHandlerDelegate {
        private final Handler mHandler;

        EventHandlerDelegate() {
            Looper looper;
            if (Looper.myLooper() != null) {
                looper = Looper.myLooper();
            } else {
                looper = Looper.getMainLooper();
            }
            Log.i("HmSensorHubManager", "HmSensorHubManager use the thread:" + looper.getThread() + "to handle callback" + " owner process:" + Process.myPid() + " threads id:" + Process.myTid());
            if (looper != null) {
                this.mHandler = new Handler(looper, HmSensorHubConfigManager.this) {
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case 0:
                                OnConfigFinishListener listener;
                                ConfigFinishInfo info = msg.obj;
                                String clientId = info.clientId;
                                String r_msg = info.msg;
                                int resultCode = info.resultCode;
                                synchronized (HmSensorHubConfigManager.this.mConfigFinishListenerLock) {
                                    listener = HmSensorHubConfigManager.this.findConfigFinishListener(clientId);
                                }
                                if (listener != null) {
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "HmSensorHubConfigManager dispatching onFinish(" + resultCode + "," + r_msg + ") ");
                                    }
                                    listener.onFinish(resultCode, r_msg);
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "dispathing done, unregister callback listener~");
                                    }
                                    HmSensorHubConfigManager.this.unregisterConfigFinishListener(clientId);
                                    return;
                                }
                                Log.w("HmSensorHubManager", "can't find listener for client:" + clientId);
                                return;
                            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                                DataInfo dataInfo = msg.obj;
                                OnDataReadyListener dataListener = HmSensorHubConfigManager.this.findDataReadyListener(dataInfo.sessionId);
                                if (dataListener != null) {
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "HmSensorHubConfigManager dispatching onDataReady(" + dataInfo.sessionId + "," + dataInfo.data + ") ");
                                    }
                                    dataListener.onDataReady(dataInfo.sessionId, dataInfo.data);
                                    HmSensorHubConfigManager.this.unregisterDataReadyListener(dataInfo.sessionId);
                                    return;
                                }
                                Log.w("HmSensorHubManager", "null listener!!");
                                return;
                            case 2:
                                KlvpResponse response = msg.obj;
                                if (response == null) {
                                    Log.w("HmSensorHubManager", " response is null !!!");
                                    return;
                                }
                                OnKlvpDataListener callback = HmSensorHubConfigManager.this.findRealtimeCallback(response.pairId);
                                if (callback != null) {
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "response target:" + response.target);
                                    }
                                    switch (response.target) {
                                        case (short) 2:
                                            SportStatistics sportStatistics = null;
                                            try {
                                                sportStatistics = SportStatistics.parseFrom(response.responseValues);
                                            } catch (InvalidProtocolBufferNanoException e) {
                                                e.printStackTrace();
                                            }
                                            Integer typeInt = (Integer) HmSensorHubConfigManager.this.mPairIdTypeMap.get(response.pairId);
                                            int type = -1;
                                            if (typeInt != null) {
                                                type = typeInt.intValue();
                                            }
                                            if (sportStatistics == null) {
                                                Log.w("HmSensorHubManager", "probuf decode wrong of SportStatistics!!!");
                                                break;
                                            }
                                            switch (type) {
                                                case 0:
                                                    callback.onHealthDataReady(0, -1.0f);
                                                    break;
                                                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                                                    float distance = 0.0f;
                                                    if (sportStatistics.mTodayAcm != null) {
                                                        distance = sportStatistics.mTodayAcm.getMDistance();
                                                    }
                                                    callback.onHealthDataReady(1, distance);
                                                    break;
                                                case 2:
                                                    callback.onSportDataReady(sportStatistics);
                                                    break;
                                                default:
                                                    Log.w("HmSensorHubManager", "unknown type " + type + "of pairid:" + response.pairId);
                                                    break;
                                            }
                                        default:
                                            Log.e("HmSensorHubManager", "undefined target");
                                            break;
                                    }
                                    if (HmSensorHubConfigManager.DEBUG) {
                                        Log.d("HmSensorHubManager", "callback called !!!!");
                                    }
                                    HmSensorHubConfigManager.this.unregisterKlvpCallback(response.pairId);
                                    return;
                                }
                                Log.d("HmSensorHubManager", "callback is null of pairId :" + response.pairId + "maybe it's config cmd");
                                return;
                            default:
                                return;
                        }
                    }
                };
            } else {
                this.mHandler = null;
            }
        }

        Handler getHandler() {
            return this.mHandler;
        }
    }

    public interface OnConfigFinishListener {
        void onFinish(int i, String str);
    }

    public interface OnDataReadyListener {
        void onDataReady(long j, HealthSensorHistory healthSensorHistory);
    }

    public interface OnKlvpDataListener {
        void onHealthDataReady(int i, float f);

        void onSportDataReady(SportStatistics sportStatistics);
    }

    public static class SwimInfo implements Parcelable {
        public static final Creator<SwimInfo> CREATOR = new C01161();
        public float mAvgDistancePerStroke = 0.0f;
        public float mAvgStrokeSpeed = 0.0f;
        public float mLapStrokeSpeed = 0.0f;
        public int mLapStrokes = 0;
        public int mLapSwolf = 0;
        public float mMaxStrokeSpeed = 0.0f;
        public float mRtDistancePerStroke = 0.0f;
        public float mRtStrokeSpeed = 0.0f;
        public int mSwolfPerFixedMeters = 0;
        public int mTotalStrokes = 0;
        public int mTotalTrips = 0;

        static class C01161 implements Creator<SwimInfo> {
            C01161() {
            }

            public SwimInfo createFromParcel(Parcel in) {
                return new SwimInfo(in);
            }

            public SwimInfo[] newArray(int size) {
                return new SwimInfo[size];
            }
        }

        protected SwimInfo(Parcel in) {
            this.mSwolfPerFixedMeters = in.readInt();
            this.mTotalStrokes = in.readInt();
            this.mTotalTrips = in.readInt();
            this.mRtDistancePerStroke = in.readFloat();
            this.mRtStrokeSpeed = in.readFloat();
            this.mAvgStrokeSpeed = in.readFloat();
            this.mMaxStrokeSpeed = in.readFloat();
            this.mAvgDistancePerStroke = in.readFloat();
            this.mLapStrokes = in.readInt();
            this.mLapStrokeSpeed = in.readFloat();
            this.mLapSwolf = in.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.mSwolfPerFixedMeters);
            dest.writeInt(this.mTotalStrokes);
            dest.writeInt(this.mTotalTrips);
            dest.writeFloat(this.mRtDistancePerStroke);
            dest.writeFloat(this.mRtStrokeSpeed);
            dest.writeFloat(this.mAvgStrokeSpeed);
            dest.writeFloat(this.mMaxStrokeSpeed);
            dest.writeFloat(this.mAvgDistancePerStroke);
            dest.writeInt(this.mLapStrokes);
            dest.writeFloat(this.mLapStrokeSpeed);
            dest.writeInt(this.mLapSwolf);
        }

        public String toString() {
            return "SwimInfo{mSwolfPerFixedMeters=" + this.mSwolfPerFixedMeters + ", mTotalStrokes=" + this.mTotalStrokes + ", mTotalTrips=" + this.mTotalTrips + ", mRtDistancePerStroke=" + this.mRtDistancePerStroke + ", mRtStrokeSpeed=" + this.mRtStrokeSpeed + ", mAvgStrokeSpeed=" + this.mAvgStrokeSpeed + ", mMaxStrokeSpeed=" + this.mMaxStrokeSpeed + ", mAvgDistancePerStroke=" + this.mAvgDistancePerStroke + ", mLapStrokes=" + this.mLapStrokes + ", mLapStrokeSpeed=" + this.mLapStrokeSpeed + ", mLapSwolf=" + this.mLapSwolf + '}';
        }
    }

    private OnConfigFinishListener findConfigFinishListener(String clientId) {
        return (OnConfigFinishListener) this.mConfigFinishIdListenerMap.get(clientId);
    }

    private OnDataReadyListener findDataReadyListener(long sessionId) {
        return (OnDataReadyListener) this.mDataReadyListenerMap.get(Long.valueOf(sessionId));
    }

    private OnKlvpDataListener findRealtimeCallback(int id) {
        return (OnKlvpDataListener) this.mRealTimeDataListenerMap.get(id);
    }

    private void unregisterDataReadyListener(long sesionId) {
        if (DEBUG) {
            Log.d("HmSensorHubManager", "unregisterDataReadyListener of session:" + sesionId);
        }
        synchronized (this.mDataReadyListenerMap) {
            this.mDataReadyListenerMap.remove(Long.valueOf(sesionId));
        }
    }

    private void unregisterKlvpCallback(int id) {
        synchronized (this.mRealTimeDataListenerMap) {
            if (DEBUG) {
                Log.d("HmSensorHubManager", "remove callback of pairid:" + id);
            }
            this.mRealTimeDataListenerMap.remove(id);
        }
    }

    private void unregisterConfigFinishListener(String clientId) {
        if (DEBUG) {
            Log.d("HmSensorHubManager", "unregisterConfigFinishListener of client:" + clientId);
        }
        synchronized (this.mConfigFinishListenerLock) {
            this.mConfigFinishIdListenerMap.remove(clientId);
        }
    }

    public HmSensorHubConfigManager(Context context) {
        this.mContext = context;
        this.mFinishEventHandlerDelegate = new EventHandlerDelegate();
    }

    private static IHmSensorHubService getService() throws RemoteException {
        if (sService != null) {
            return sService;
        }
        sService = IHmSensorHubService.Stub.asInterface(ServiceManager.getService("hm_sensor_hub_service"));
        if (sService != null) {
            return sService;
        }
        throw new RemoteException("Can't get Service:hm_sensor_hub_service");
    }

    public SensorDataInfo getSensorDatainfo() {
        try {
            return getService().getSensorDataInfo();
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getHeartRate() {
        SensorDataInfo info = getSensorDatainfo();
        if (info != null) {
            return info.mHeartRate;
        }
        Log.w("HmSensorHubManager", "get null sensor data info");
        return 0;
    }

    public int getFloorCount() {
        SensorDataInfo info = getSensorDatainfo();
        if (info != null) {
            return info.mFloorCount;
        }
        Log.w("HmSensorHubManager", "get null sensor data info");
        return 0;
    }

    public float getSportCalories() {
        SensorDataInfo info = getSensorDatainfo();
        if (info != null) {
            return info.mSportCalories;
        }
        Log.w("HmSensorHubManager", "get null sensor data info");
        return 0.0f;
    }
}
