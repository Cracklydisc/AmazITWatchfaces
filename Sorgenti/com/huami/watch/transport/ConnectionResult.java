package com.huami.watch.transport;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v7.recyclerview.C0051R;

public class ConnectionResult implements Parcelable {
    public static final Creator<ConnectionResult> CREATOR = new C01191();
    private int mResultCode;

    static class C01191 implements Creator<ConnectionResult> {
        C01191() {
        }

        public ConnectionResult createFromParcel(Parcel in) {
            return new ConnectionResult(in);
        }

        public ConnectionResult[] newArray(int size) {
            return new ConnectionResult[size];
        }
    }

    public String toString() {
        switch (this.mResultCode) {
            case 0:
                return "success";
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                return "service is unavailable";
            case 2:
                return "service authentication failure";
            default:
                return "Assert";
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mResultCode);
    }

    private ConnectionResult(Parcel in) {
        this.mResultCode = in.readInt();
    }
}
