package com.huami.watch.transport;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.SparseArray;
import com.huami.watch.util.Config;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class DataBundle implements Parcelable {
    public static final Creator<DataBundle> CREATOR = new C01201();
    public static final boolean DEBUG = Config.isDebug();
    public static final DataBundle EMPTY = new DataBundle();
    private boolean mAllowFds;
    private ClassLoader mClassLoader;
    private boolean mFdsKnown;
    private boolean mHasFds;
    Map<String, Object> mMap;
    Parcel mParcelledData;

    static class C01201 implements Creator<DataBundle> {
        C01201() {
        }

        public DataBundle createFromParcel(Parcel in) {
            int length = in.readInt();
            if (length < 0) {
                return null;
            }
            return new DataBundle(in, length);
        }

        public DataBundle[] newArray(int size) {
            return new DataBundle[size];
        }
    }

    static {
        EMPTY.mMap = Collections.unmodifiableMap(new HashMap());
    }

    public DataBundle() {
        this.mMap = null;
        this.mParcelledData = null;
        this.mHasFds = false;
        this.mFdsKnown = true;
        this.mAllowFds = true;
        this.mMap = new HashMap();
        this.mClassLoader = getClass().getClassLoader();
    }

    public DataBundle(Parcel parcelledData, int length) {
        this.mMap = null;
        this.mParcelledData = null;
        this.mHasFds = false;
        this.mFdsKnown = true;
        this.mAllowFds = true;
        setClassLoader(Bitmap.Config.class.getClassLoader());
        readFromParcelInner(parcelledData, length);
    }

    public DataBundle(DataBundle b) {
        this.mMap = null;
        this.mParcelledData = null;
        this.mHasFds = false;
        this.mFdsKnown = true;
        this.mAllowFds = true;
        if (b.mParcelledData != null) {
            this.mParcelledData = Parcel.obtain();
            this.mParcelledData.appendFrom(b.mParcelledData, 0, b.mParcelledData.dataSize());
            this.mParcelledData.setDataPosition(0);
        } else {
            this.mParcelledData = null;
        }
        if (b.mMap != null) {
            this.mMap = new HashMap(b.mMap);
        } else {
            this.mMap = null;
        }
        this.mHasFds = b.mHasFds;
        this.mFdsKnown = b.mFdsKnown;
        this.mClassLoader = b.mClassLoader;
    }

    public void setClassLoader(ClassLoader loader) {
        this.mClassLoader = loader;
    }

    public Object clone() {
        return new DataBundle(this);
    }

    public boolean hasFileDescriptors() {
        if (!this.mFdsKnown) {
            boolean fdFound = false;
            if (this.mParcelledData == null) {
                Iterator<Entry<String, Object>> iter = this.mMap.entrySet().iterator();
                while (!fdFound && iter.hasNext()) {
                    SparseArray<? extends Parcelable> obj = ((Entry) iter.next()).getValue();
                    if (obj instanceof Parcelable) {
                        if ((((Parcelable) obj).describeContents() & 1) != 0) {
                            fdFound = true;
                            break;
                        }
                    } else if (obj instanceof Parcelable[]) {
                        Parcelable[] array = (Parcelable[]) obj;
                        for (n = array.length - 1; n >= 0; n--) {
                            if ((array[n].describeContents() & 1) != 0) {
                                fdFound = true;
                                break;
                            }
                        }
                    } else if (obj instanceof SparseArray) {
                        SparseArray<? extends Parcelable> array2 = obj;
                        for (n = array2.size() - 1; n >= 0; n--) {
                            if ((((Parcelable) array2.get(n)).describeContents() & 1) != 0) {
                                fdFound = true;
                                break;
                            }
                        }
                    } else if (obj instanceof ArrayList) {
                        ArrayList array3 = (ArrayList) obj;
                        if (array3.size() > 0 && (array3.get(0) instanceof Parcelable)) {
                            for (n = array3.size() - 1; n >= 0; n--) {
                                Parcelable p = (Parcelable) array3.get(n);
                                if (p != null && (p.describeContents() & 1) != 0) {
                                    fdFound = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            } else if (this.mParcelledData.hasFileDescriptors()) {
                fdFound = true;
            }
            this.mHasFds = fdFound;
            this.mFdsKnown = true;
        }
        return this.mHasFds;
    }

    public void writeToParcel(Parcel parcel, int flags) {
        if (this.mParcelledData != null) {
            int length = this.mParcelledData.dataSize();
            parcel.writeInt(length);
            parcel.appendFrom(this.mParcelledData, 0, length);
            return;
        }
        parcel.writeInt(-1);
        int oldPos = parcel.dataPosition();
        if (this.mMap == null || this.mMap != null) {
            Set<Entry<String, Object>> entries = this.mMap.entrySet();
            parcel.writeInt(entries.size());
            for (Entry<String, Object> e : entries) {
                parcel.writeValue(e.getKey());
                parcel.writeValue(e.getValue());
            }
            int newPos = parcel.dataPosition();
            length = newPos - oldPos;
            parcel.setDataPosition(oldPos - 4);
            parcel.writeInt(length);
            parcel.setDataPosition(newPos);
            return;
        }
        parcel.writeInt(-1);
    }

    void readFromParcelInner(Parcel parcel, int length) {
        int offset = parcel.dataPosition();
        parcel.setDataPosition(offset + length);
        Parcel p = Parcel.obtain();
        p.setDataPosition(0);
        p.appendFrom(parcel, offset, length);
        p.setDataPosition(0);
        this.mParcelledData = p;
    }

    public int describeContents() {
        if (hasFileDescriptors()) {
            return 0 | 1;
        }
        return 0;
    }

    public synchronized String toString() {
        String str;
        if (this.mParcelledData != null) {
            str = "Bundle[mParcelledData.dataSize=" + this.mParcelledData.dataSize() + "]";
        } else {
            str = "Bundle[" + this.mMap.toString() + "]";
        }
        return str;
    }
}
