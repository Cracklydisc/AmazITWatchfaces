package com.huami.watch.transport;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v7.recyclerview.C0051R;

public class DataTransportResult implements Parcelable {
    public static final Creator<DataTransportResult> CREATOR = new C01211();
    private int resultCode;
    private TransportDataItem transportItem;

    static class C01211 implements Creator<DataTransportResult> {
        C01211() {
        }

        public DataTransportResult createFromParcel(Parcel in) {
            return new DataTransportResult(in);
        }

        public DataTransportResult[] newArray(int size) {
            return new DataTransportResult[size];
        }
    }

    public DataTransportResult(Parcel in) {
        this.transportItem = (TransportDataItem) in.readParcelable(TransportDataItem.class.getClassLoader());
        this.resultCode = in.readInt();
    }

    public String toString() {
        switch (this.resultCode) {
            case 0:
                return "OK";
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                return "FAILED_CHANNEL_UNAVAILABLE";
            case 2:
                return "FAILED_LINK_DISCONNECTED";
            case 3:
                return "FAILED_IWDS_CRASH";
            case 4:
                return "FAILED_TRANSPORT_SERVICE_UNCONNECTED";
            default:
                return "NONE";
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.transportItem, 0);
        dest.writeInt(this.resultCode);
    }
}
