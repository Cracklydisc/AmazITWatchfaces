package com.huami.watch.transport;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v7.recyclerview.C0051R;

public class Status implements Parcelable {
    public static final Creator<Status> CREATOR = new C01221();
    private int mCode;

    static class C01221 implements Creator<Status> {
        C01221() {
        }

        public Status createFromParcel(Parcel in) {
            return new Status(in);
        }

        public Status[] newArray(int size) {
            return new Status[size];
        }
    }

    public Status(Parcel in) {
        this.mCode = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int arg1) {
        dest.writeInt(this.mCode);
    }

    public String toString() {
        switch (this.mCode) {
            case 0:
                return "cancel";
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                return "success";
            default:
                return " ";
        }
    }
}
