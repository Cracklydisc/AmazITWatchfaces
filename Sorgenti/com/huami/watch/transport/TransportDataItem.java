package com.huami.watch.transport;

import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class TransportDataItem implements Parcelable {
    public static final Creator<TransportDataItem> CREATOR = new C01231();
    private DataBundle data;
    private long time;
    private Uri uri;
    private Builder uriBuild = new Builder();

    static class C01231 implements Creator<TransportDataItem> {
        C01231() {
        }

        public TransportDataItem createFromParcel(Parcel source) {
            return new TransportDataItem(source);
        }

        public TransportDataItem[] newArray(int size) {
            return new TransportDataItem[size];
        }
    }

    public TransportDataItem(Parcel source) {
        this.uri = Uri.parse(source.readString());
        this.time = source.readLong();
        this.data = new DataBundle(source, source.readInt());
        this.data.setClassLoader(getClass().getClassLoader());
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uri.toString());
        dest.writeLong(this.time);
        this.data.writeToParcel(dest, 0);
    }

    public String toString() {
        return "TransportDataItem [action=" + this.uri + "] [map=" + this.data.toString() + "] [time=" + this.time + "]";
    }

    public int describeContents() {
        return 0;
    }
}
