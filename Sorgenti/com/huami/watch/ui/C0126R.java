package com.huami.watch.ui;

public final class C0126R {

    public static final class color {
        public static final int cw_page_indicator = 2131230849;
        public static final int cw_page_indicator_focused = 2131230850;
    }

    public static final class dimen {
        public static final int indicator_center_distance = 2131296546;
        public static final int indicator_radius = 2131296544;
        public static final int indicator_radius_focused = 2131296545;
    }

    public static final class id {
        public static final int picker1 = 2131558441;
        public static final int picker2 = 2131558442;
        public static final int picker_ampm = 2131558443;
    }
}
