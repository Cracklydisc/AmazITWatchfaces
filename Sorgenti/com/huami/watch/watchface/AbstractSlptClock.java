package com.huami.watch.watchface;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v7.recyclerview.C0051R;
import android.text.format.DateFormat;
import android.util.Log;
import com.huami.watch.watchface.slpt.Lock.LowPowerClock;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.huami.watch.watchface.widget.slpt.SlptCalorieWidget;
import com.huami.watch.watchface.widget.slpt.SlptDateWidget;
import com.huami.watch.watchface.widget.slpt.SlptDefaultWidget;
import com.huami.watch.watchface.widget.slpt.SlptHeartRateWidget;
import com.huami.watch.watchface.widget.slpt.SlptPowerWidget;
import com.huami.watch.watchface.widget.slpt.SlptStepWidget;
import com.huami.watch.watchface.widget.slpt.SlptTodayDistanceWidget;
import com.huami.watch.watchface.widget.slpt.SlptTodayFloorWidget;
import com.huami.watch.watchface.widget.slpt.SlptTotalDistanceWidget;
import com.huami.watch.watchface.widget.slpt.SlptWeatherWidget;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.utils.SlptWatchFaceConst;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class AbstractSlptClock extends Service {
    Callback callback = new C01292();
    private int hourForamt = 1;
    private Context mContext;
    private ArrayList<PreDrawedPictureGroup> mListDrawedPictureGroup26WS = new ArrayList();
    private ArrayList<PreDrawedPictureGroup> mListDrawedPictureGroup8S = new ArrayList();
    private Object mLock = Util.mLock;
    private SlptClockClient mSlptClockClient = new SlptClockClient();
    private boolean periodSecond = false;
    private String uuid = "AbstractSlptClock";

    class C01271 implements Runnable {
        C01271() {
        }

        public void run() {
            try {
                synchronized (AbstractSlptClock.this.mLock) {
                    AbstractSlptClock.this.enableSlptClock();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C01292 implements Callback {

        class C01281 implements Runnable {
            C01281() {
            }

            public void run() {
                try {
                    synchronized (AbstractSlptClock.this.mLock) {
                        AbstractSlptClock.this.enableSlptClock();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        C01292() {
        }

        public void onServiceDisconnected() {
            Log.d(AbstractSlptClock.this.uuid, "slpt clock service has crashed");
            try {
                synchronized (AbstractSlptClock.this.mLock) {
                    AbstractSlptClock.this.mSlptClockClient.bindService(AbstractSlptClock.this.mContext, AbstractSlptClock.this.uuid, AbstractSlptClock.this.callback);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceConnected() {
            new Thread(new C01281()).start();
        }
    }

    protected abstract SlptLayout createClockLayout26WC();

    protected abstract SlptLayout createClockLayout8C();

    protected abstract void initWatchFaceConfig();

    public void onCreate() {
        super.onCreate();
        this.mContext = getApplicationContext();
        Log.i("AbstractSlptClock", "onCreate!");
        this.uuid = super.getClass().getSimpleName();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(this.uuid, "onStartCommand!");
        if (this.mSlptClockClient.serviceIsConnected()) {
            new Thread(new C01271()).start();
        } else {
            try {
                synchronized (this.mLock) {
                    this.mSlptClockClient.bindService(this.mContext, this.uuid, this.callback);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 1;
    }

    public void onDestroy() {
        Log.d(this.uuid, "onDestroy!");
        try {
            synchronized (this.mLock) {
                this.mSlptClockClient.unbindService(this.mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private int enableSlptClock() {
        if (Util.getSportfaceValue() != 1) {
            if (!Util.checkWatchFace(this.mContext)) {
                Log.i(this.uuid, "check need watchface false");
            } else if (this.mSlptClockClient.lockService()) {
                setClockPeriodSecond(false);
                initWatchFaceConfig();
                this.mSlptClockClient.clearAllClock();
                SlptLayout rootLayout8S = createClockLayout8C();
                if (rootLayout8S != null) {
                    rootLayout8S.add(SportItemInfoWrapper.createWakeUpLockView(this.mContext));
                    SlptClock clock8S = new SlptClock(rootLayout8S);
                    Iterator i$ = this.mListDrawedPictureGroup8S.iterator();
                    while (i$.hasNext()) {
                        clock8S.addPreDrawedPicture((PreDrawedPictureGroup) i$.next());
                    }
                    this.mSlptClockClient.selectClockIndex(0);
                    this.mSlptClockClient.enableOneClock(clock8S);
                    SlptLayout rootLayout26WS = createClockLayout26WC();
                    if (rootLayout26WS != null) {
                        rootLayout26WS.add(SportItemInfoWrapper.createWakeUpLockView(this.mContext));
                        SlptClock clock26WS = new SlptClock(rootLayout26WS);
                        i$ = this.mListDrawedPictureGroup26WS.iterator();
                        while (i$.hasNext()) {
                            clock26WS.addPreDrawedPicture((PreDrawedPictureGroup) i$.next());
                        }
                        this.mSlptClockClient.selectClockIndex(1);
                        this.mSlptClockClient.enableOneClock(clock26WS);
                    }
                    SlptClock clockLowPower = new LowPowerClock(this.mContext);
                    if (clockLowPower.getRootView() != null) {
                        this.mSlptClockClient.selectClockIndex(2);
                        this.mSlptClockClient.enableOneClock(clockLowPower);
                    }
                    clearDrawdPictureGroup();
                    this.mSlptClockClient.selectClockIndex(0);
                    if (DateFormat.is24HourFormat(this.mContext)) {
                        this.mSlptClockClient.setHourFormat(1);
                    } else {
                        this.mSlptClockClient.setHourFormat(0);
                    }
                    if (isClockPeriodSecond()) {
                        this.mSlptClockClient.setClockPeriod(1);
                    } else {
                        this.mSlptClockClient.setClockPeriod(60);
                    }
                    this.mSlptClockClient.disableSportMode();
                    this.mSlptClockClient.enableSlpt();
                    this.mSlptClockClient.unlockService();
                    this.mSlptClockClient.setKeyWakeupStatus(SlptWatchFaceConst.KEY_WAEUP_STATUS_UP);
                    Log.i(this.uuid, "enable clock success");
                } else {
                    this.mSlptClockClient.unlockService();
                }
            } else {
                Log.i(this.uuid, "lock service error!");
            }
        }
        return 0;
    }

    public void addDrawedPictureGroup8C(PreDrawedPictureGroup group) {
        this.mListDrawedPictureGroup8S.add(group);
    }

    public void addDrawedPictureGroup26WC(PreDrawedPictureGroup group) {
        this.mListDrawedPictureGroup26WS.add(group);
    }

    public void clearDrawdPictureGroup() {
        this.mListDrawedPictureGroup8S.clear();
        this.mListDrawedPictureGroup26WS.clear();
    }

    public void setHourFormat(int key) {
        if (key == 0 || key == 1) {
            this.hourForamt = key;
        }
    }

    public void setClockPeriodSecond(boolean tag) {
        this.periodSecond = tag;
    }

    public boolean isClockPeriodSecond() {
        return this.periodSecond;
    }

    private SlptViewComponent getDataWidget(DataWidgetConfig widgetconfig, boolean need26wc) {
        int width;
        int height;
        SlptDefaultWidget widget;
        int dataType = widgetconfig.getDataType();
        int x = widgetconfig.getX();
        int y = widgetconfig.getY();
        int model = widgetconfig.getModel();
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                width = 120;
                height = 21;
                break;
            case 3:
                width = 100;
                height = 100;
                break;
            case 4:
                width = 100;
                height = 24;
                break;
            default:
                width = 84;
                height = 84;
                break;
        }
        switch (dataType) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                widget = new SlptStepWidget(this, x, y, width, height, model, need26wc);
                break;
            case 2:
                widget = new SlptTodayDistanceWidget(this, x, y, width, height, model, need26wc);
                break;
            case 3:
                widget = new SlptTotalDistanceWidget(this, x, y, width, height, model, need26wc);
                break;
            case 4:
                widget = new SlptCalorieWidget(this, x, y, width, height, model, need26wc);
                break;
            case 5:
                widget = new SlptHeartRateWidget(this, x, y, width, height, model, need26wc);
                break;
            case 6:
                if (!Util.needEnAssets() || model != 3) {
                    SlptDefaultWidget slptDateWidget;
                    if (model != 5) {
                        if (model != 6 && model != 7) {
                            if (model != 8 && model != 9 && model != 10 && model != 11) {
                                slptDateWidget = new SlptDateWidget(this, x, y, 120, 21, model, need26wc);
                                break;
                            }
                            slptDateWidget = new SlptDateWidget(this, x, y, 180, 18, model, need26wc);
                            break;
                        }
                        slptDateWidget = new SlptDateWidget(this, x, y, 160, 30, model, need26wc);
                        break;
                    }
                    slptDateWidget = new SlptDateWidget(this, x, y, 32, 32, model, need26wc);
                    break;
                }
                return null;
                break;
            case 8:
                widget = new SlptWeatherWidget(this, x, y, width, height, model, need26wc);
                break;
            case 10:
                widget = new SlptPowerWidget(this, x, y, width, height, model, need26wc);
                break;
            case 12:
                widget = new SlptTodayFloorWidget(this, x, y, width, height, model, need26wc);
                break;
            default:
                return null;
        }
        return widget.getWidgetView();
    }

    protected SlptViewComponent get8cDataWidget(DataWidgetConfig widgetconfig) {
        return getDataWidget(widgetconfig, false);
    }

    protected SlptViewComponent get26WCDataWidget(DataWidgetConfig widgetconfig) {
        return getDataWidget(widgetconfig, true);
    }
}
