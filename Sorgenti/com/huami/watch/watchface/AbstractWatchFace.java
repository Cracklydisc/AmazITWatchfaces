package com.huami.watch.watchface;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.support.v7.recyclerview.C0051R;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.CanvasWatchFaceService.Engine;
import android.support.wearable.watchface.WatchFaceStyle.Builder;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.animation.Interpolator;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.util.SingletonWrapper;
import com.huami.watch.watchface.util.SlptOtherScreenManager;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.huami.watch.watchface.widget.AbsWatchFaceDataWidget;
import com.huami.watch.watchface.widget.BatteryRemindDateWidget;
import com.huami.watch.watchface.widget.DigitDateWidget;
import com.huami.watch.watchface.widget.FatBurnDataWidget;
import com.huami.watch.watchface.widget.HeartRateDateWidget;
import com.huami.watch.watchface.widget.MileageDateWidget;
import com.huami.watch.watchface.widget.TodayDistanceDateWidget;
import com.huami.watch.watchface.widget.TodayFloorDateWidget;
import com.huami.watch.watchface.widget.WalkDateWidget;
import com.huami.watch.watchface.widget.WeatherDateWidget;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class AbstractWatchFace extends CanvasWatchFaceService {
    protected static final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1);
    private static final Interpolator QUAD = new C01312();
    private static final Interpolator QUART = new C01301();
    private Context mContext;
    private Intent mSlptService;
    private SlptWatchFaceReceiver mSlptWatchFaceReceiver;
    private SlptOtherScreenManager slptScreenManager;

    static class C01301 implements Interpolator {
        C01301() {
        }

        public float getInterpolation(float input) {
            input -= 1.0f;
            return -((((input * input) * input) * input) - 1.0f);
        }
    }

    static class C01312 implements Interpolator {
        C01312() {
        }

        public float getInterpolation(float input) {
            return (-input) * (input - 2.0f);
        }
    }

    public abstract class BaseEngine extends Engine {
        private Runnable mAtWatchFaceCallback = new C01363();
        private ContentObserver mAtWatchFaceObserver = new ContentObserver(null) {
            public void onChange(boolean selfChange) {
                BaseEngine.this.mUpdateTimeHandler.removeCallbacks(BaseEngine.this.mAtWatchFaceCallback);
                BaseEngine.this.mUpdateTimeHandler.post(BaseEngine.this.mAtWatchFaceCallback);
            }
        };
        Calendar mCalendar;
        private Path mClip = new Path();
        private SparseArray<LinkedList<WatchDataListener>> mDataListenersMap;
        private SparseArray<WatchDataProvider> mDataProvider;
        private boolean mIsAtWatchFace = false;
        private Object mLock;
        boolean mLowBitAmbient;
        private int mMeasurement;
        private Runnable mMeasurementChangedCallback = new C01385();
        private ContentObserver mMeasurementObserver = new ContentObserver(null) {
            public void onChange(boolean selfChange) {
                BaseEngine.this.mUpdateTimeHandler.removeCallbacks(BaseEngine.this.mMeasurementChangedCallback);
                BaseEngine.this.mUpdateTimeHandler.post(BaseEngine.this.mMeasurementChangedCallback);
            }
        };
        boolean mMute;
        boolean mRegisteredTimeZoneReceiver = false;
        private int mTargetStep;
        private ContentObserver mTargetStepObserver;
        private int mTimeFormat;
        private Runnable mTimeFormatChangedCallback;
        private ContentObserver mTimeFormatObserver;
        final BroadcastReceiver mTimeZoneReceiver = new C01352();
        final Handler mUpdateTimeHandler = new C01341();
        private ArrayList<AbsWatchFaceDataWidget> mWidgets;
        final /* synthetic */ AbstractWatchFace this$0;

        class C01341 extends Handler {
            C01341() {
            }

            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                        if (Log.isLoggable("HmWatchFace", 2)) {
                            Log.v("HmWatchFace", "updating time");
                        }
                        BaseEngine.this.invalidate();
                        if (BaseEngine.this.shouldTimerBeRunning()) {
                            BaseEngine.this.mUpdateTimeHandler.sendEmptyMessageDelayed(0, AbstractWatchFace.INTERACTIVE_UPDATE_RATE_MS - (System.currentTimeMillis() % AbstractWatchFace.INTERACTIVE_UPDATE_RATE_MS));
                            BaseEngine.this.onTimeUpdate();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        class C01352 extends BroadcastReceiver {
            C01352() {
            }

            public void onReceive(Context context, Intent intent) {
                BaseEngine.this.mCalendar.setTimeZone(TimeZone.getDefault());
                BaseEngine.this.invalidate();
            }
        }

        class C01363 implements Runnable {
            C01363() {
            }

            public void run() {
                BaseEngine.this.updateAtWatchFaceValue();
            }
        }

        class C01385 implements Runnable {
            C01385() {
            }

            public void run() {
                int measurement = Util.getMeasurement(BaseEngine.this.this$0.mContext);
                if (measurement != BaseEngine.this.mMeasurement) {
                    Log.d("HmWatchFace", "Measurement changed " + measurement);
                    BaseEngine.this.mMeasurement = measurement;
                    BaseEngine.this.onMeasurementChanged(BaseEngine.this.mMeasurement);
                    WatchDataProvider dataProvider = (WatchDataProvider) BaseEngine.this.mDataProvider.get(2);
                    if (dataProvider != null) {
                        dataProvider.unregister();
                        dataProvider.register();
                    }
                    dataProvider = (WatchDataProvider) BaseEngine.this.mDataProvider.get(3);
                    if (dataProvider != null) {
                        dataProvider.unregister();
                        dataProvider.register();
                    }
                    dataProvider = (WatchDataProvider) BaseEngine.this.mDataProvider.get(7);
                    if (dataProvider != null) {
                        dataProvider.unregister();
                        dataProvider.register();
                    }
                    if (BaseEngine.this.this$0.mSlptService != null) {
                        BaseEngine.this.this$0.mContext.startService(BaseEngine.this.this$0.mSlptService);
                    }
                }
            }
        }

        class C01407 implements Runnable {
            C01407() {
            }

            public void run() {
                int timeFormat = DateFormat.is24HourFormat(BaseEngine.this.this$0.mContext) ? 0 : 1;
                if (timeFormat != BaseEngine.this.mTimeFormat) {
                    Log.d("HmWatchFace", "TimeFormat changed " + timeFormat);
                    BaseEngine.this.mTimeFormat = timeFormat;
                    BaseEngine.this.onHourFormatChanged(BaseEngine.this.mTimeFormat);
                    if (BaseEngine.this.this$0.mSlptService != null) {
                        BaseEngine.this.this$0.mContext.stopService(BaseEngine.this.this$0.mSlptService);
                        BaseEngine.this.this$0.mContext.startService(BaseEngine.this.this$0.mSlptService);
                    }
                }
            }
        }

        private abstract class WatchDataProvider {
            abstract void onDestroy();

            abstract void register();

            abstract void requestData();

            abstract void requestData(WatchDataListener watchDataListener);

            abstract void unregister();

            private WatchDataProvider() {
            }
        }

        private class BatteryDataProvider extends WatchDataProvider {
            private int mBatteryLevel;
            private int mBatteryMax;
            private BroadcastReceiver mBatteryStatusReceiver;

            class C01431 extends BroadcastReceiver {
                C01431() {
                }

                public void onReceive(Context context, Intent intent) {
                    BatteryDataProvider.this.updateBatteryLevel(intent);
                }
            }

            private BatteryDataProvider() {
                super();
                this.mBatteryStatusReceiver = new C01431();
            }

            void register() {
                IntentFilter filter = new IntentFilter();
                filter.addAction("android.intent.action.BATTERY_CHANGED");
                updateBatteryLevel(BaseEngine.this.this$0.mContext.registerReceiver(this.mBatteryStatusReceiver, filter));
            }

            void unregister() {
                BaseEngine.this.this$0.mContext.unregisterReceiver(this.mBatteryStatusReceiver);
            }

            void onDestroy() {
                unregister();
            }

            private void updateBatteryLevel(Intent intent) {
                if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                    this.mBatteryLevel = intent.getIntExtra("level", 0);
                    this.mBatteryMax = intent.getIntExtra("scale", 0);
                    requestData();
                }
            }

            void requestData() {
                BaseEngine.this.onDataUpdate(10, Integer.valueOf(this.mBatteryLevel), Integer.valueOf(this.mBatteryMax));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null) {
                    listener.onDataUpdate(10, Integer.valueOf(this.mBatteryLevel), Integer.valueOf(this.mBatteryMax));
                }
            }
        }

        private class CaloriesDataProvider extends WatchDataProvider {
            private CaloriesDataProvider() {
                super();
            }

            void register() {
            }

            void unregister() {
            }

            void onDestroy() {
            }

            void requestData() {
                float calories = SingletonWrapper.getInstance(BaseEngine.this.this$0.mContext).getSportCalories();
                BaseEngine.this.onDataUpdate(4, Float.valueOf(calories));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null) {
                    listener.onDataUpdate(4, Float.valueOf(SingletonWrapper.getInstance(BaseEngine.this.this$0.mContext).getSportCalories()));
                }
            }
        }

        private class DateDataProvider extends WatchDataProvider {
            private int day;
            private long lastTime;
            private DateChangeReceiver mDateReceiver;
            private int month;
            private int week;
            private int year;

            private class DateChangeReceiver extends BroadcastReceiver {
                private DateChangeReceiver() {
                }

                public void onReceive(Context context, Intent intent) {
                    String wakeupSource = intent.getStringExtra("WAKEUP_SOURCE");
                    Log.i("HmWatchFace", "receive date change action, wakup source: " + wakeupSource);
                    if ("sensorhub.reach.day.end".equals(wakeupSource) && BaseEngine.this.this$0.mSlptService != null) {
                        BaseEngine.this.this$0.mContext.startService(BaseEngine.this.this$0.mSlptService);
                    }
                }
            }

            public DateDataProvider() {
                super();
                this.year = 2015;
                this.month = 12;
                this.day = 12;
                this.week = 7;
                this.mDateReceiver = null;
                this.mDateReceiver = new DateChangeReceiver();
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("com.huami.watch.action.SENSOR_WAKEUP");
                BaseEngine.this.this$0.mContext.registerReceiver(this.mDateReceiver, intentFilter);
            }

            void register() {
            }

            void unregister() {
            }

            void onDestroy() {
                if (this.mDateReceiver != null) {
                    try {
                        BaseEngine.this.this$0.mContext.unregisterReceiver(this.mDateReceiver);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            private void updateData() {
                if (this.lastTime != BaseEngine.this.mCalendar.getTimeInMillis()) {
                    this.lastTime = BaseEngine.this.mCalendar.getTimeInMillis();
                    int year = BaseEngine.this.mCalendar.get(1);
                    int month = BaseEngine.this.mCalendar.get(2) + 1;
                    int day = BaseEngine.this.mCalendar.get(5);
                    if (!(this.year == year && this.month == month && this.day == day)) {
                        this.year = year;
                        this.month = month;
                        this.day = day;
                    }
                    this.week = BaseEngine.this.mCalendar.get(7);
                }
            }

            void requestData() {
                updateData();
                BaseEngine.this.onDataUpdate(6, Integer.valueOf(this.year), Integer.valueOf(this.month), Integer.valueOf(this.day), Integer.valueOf(this.week));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null) {
                    updateData();
                    listener.onDataUpdate(6, Integer.valueOf(this.year), Integer.valueOf(this.month), Integer.valueOf(this.day), Integer.valueOf(this.week));
                }
            }
        }

        private class FloorDataProvider extends WatchDataProvider {
            private int mFloorData;

            private FloorDataProvider() {
                super();
                this.mFloorData = 0;
            }

            void register() {
            }

            void unregister() {
            }

            void onDestroy() {
            }

            private void updateFloorCount() {
                this.mFloorData = Math.max(0, SingletonWrapper.getInstance(BaseEngine.this.this$0.mContext).getFloorCount());
            }

            void requestData() {
                updateFloorCount();
                BaseEngine.this.onDataUpdate(12, Integer.valueOf(this.mFloorData));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null) {
                    updateFloorCount();
                    BaseEngine.this.onDataUpdate(12, Integer.valueOf(this.mFloorData));
                }
            }
        }

        private class HeartRateDataProvider extends WatchDataProvider {
            private ContentObserver mContentObserver;
            private int mHeartRato;
            private int mLastHeartRato;
            private Runnable mUpdateHeartTask;

            class C01441 implements Runnable {
                C01441() {
                }

                public void run() {
                    HeartRateDataProvider.this.updateLastHeartInfo();
                }
            }

            private HeartRateDataProvider() {
                super();
                this.mLastHeartRato = 0;
                this.mHeartRato = 0;
                this.mUpdateHeartTask = new C01441();
                this.mContentObserver = new ContentObserver(null) {
                    public void onChange(boolean selfChange) {
                        if (!BaseEngine.this.mUpdateTimeHandler.hasCallbacks(HeartRateDataProvider.this.mUpdateHeartTask)) {
                            BaseEngine.this.mUpdateTimeHandler.post(HeartRateDataProvider.this.mUpdateHeartTask);
                        }
                    }
                };
            }

            void register() {
                BaseEngine.this.this$0.mContext.getContentResolver().registerContentObserver(SingletonWrapper.CONTENT_HEART_URI, false, this.mContentObserver);
                updateLastHeartInfo();
            }

            void unregister() {
                BaseEngine.this.this$0.mContext.getContentResolver().unregisterContentObserver(this.mContentObserver);
                BaseEngine.this.mUpdateTimeHandler.removeCallbacks(this.mUpdateHeartTask);
            }

            void onDestroy() {
                unregister();
            }

            private void updateLastHeartInfo() {
                this.mLastHeartRato = SingletonWrapper.getLastHeartRato(BaseEngine.this.this$0.mContext);
                Log.d("HmWatchFace", "updateLastHeartInfo " + this.mLastHeartRato);
                requestData();
            }

            private void updateHeartInfo() {
                this.mHeartRato = SingletonWrapper.getInstance(BaseEngine.this.this$0.mContext).getHeartRato();
                if (this.mHeartRato <= 0) {
                    this.mHeartRato = this.mLastHeartRato;
                }
            }

            void requestData() {
                updateHeartInfo();
                BaseEngine.this.onDataUpdate(5, Integer.valueOf(this.mHeartRato));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null) {
                    updateHeartInfo();
                    listener.onDataUpdate(5, Integer.valueOf(this.mHeartRato));
                }
            }
        }

        private class StepDataProvider extends WatchDataProvider {
            private int mCurStepCount = 0;
            private DateChangeReceiver mDateReceiver = null;
            private SensorEventListener mListener = null;
            private SensorManager mManager = null;
            private Sensor mStepSensor = null;
            private int mTotalStepTarget = 8000;

            private class DateChangeReceiver extends BroadcastReceiver {
                private DateChangeReceiver() {
                }

                public void onReceive(Context context, Intent intent) {
                    String wakeupSource = intent.getStringExtra("WAKEUP_SOURCE");
                    Log.i("HmWatchFace", "receive date change action, wakup source: " + wakeupSource);
                    if ("sensorhub.reach.day.end".equals(wakeupSource)) {
                        StepDataProvider.this.mCurStepCount = 0;
                        BaseEngine.this.onDataUpdate(1, Integer.valueOf(StepDataProvider.this.mCurStepCount), Integer.valueOf(StepDataProvider.this.mTotalStepTarget));
                        BaseEngine.this.postInvalidate();
                    }
                }
            }

            public StepDataProvider() {
                super();
                this.mManager = (SensorManager) BaseEngine.this.this$0.mContext.getSystemService("sensor");
                this.mStepSensor = this.mManager.getDefaultSensor(19);
                this.mListener = new SensorEventListener(BaseEngine.this) {
                    public void onSensorChanged(SensorEvent event) {
                        float[] values = event.values;
                        if (values != null && values.length >= 1) {
                            int stepCount = (int) values[0];
                            if (stepCount < 0) {
                                Log.i("HmWatchFace", "count is below zero");
                                return;
                            }
                            StepDataProvider.this.mCurStepCount = stepCount;
                            BaseEngine.this.onDataUpdate(1, Integer.valueOf(StepDataProvider.this.mCurStepCount), Integer.valueOf(StepDataProvider.this.mTotalStepTarget));
                        }
                    }

                    public void onAccuracyChanged(Sensor sensor, int accuracy) {
                    }
                };
                this.mDateReceiver = new DateChangeReceiver();
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("com.huami.watch.action.SENSOR_WAKEUP");
                BaseEngine.this.this$0.mContext.registerReceiver(this.mDateReceiver, intentFilter);
            }

            private void updateTotalStepCount() {
                String str = WatchSettings.get(BaseEngine.this.this$0.mContext.getContentResolver(), "huami.watch.health.config");
                if (str != null) {
                    try {
                        this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            void register() {
                if (this.mManager == null || this.mListener == null || this.mStepSensor == null) {
                    Log.i("HmWatchFace", "registerStepListener error, " + this.mManager + ", " + this.mListener + ", " + this.mStepSensor);
                    return;
                }
                updateTotalStepCount();
                this.mManager.registerListener(this.mListener, this.mStepSensor, 0);
                Log.i("HmWatchFace", "registerStepListener step sensor");
            }

            void unregister() {
                if (this.mManager == null || this.mListener == null) {
                    Log.i("HmWatchFace", "unRegisterStepListener error, " + this.mManager + ", " + this.mListener);
                    return;
                }
                this.mManager.unregisterListener(this.mListener);
                Log.i("HmWatchFace", "unregisterStepListener step sensor");
            }

            void onDestroy() {
                unregister();
                if (this.mDateReceiver != null) {
                    try {
                        BaseEngine.this.this$0.mContext.unregisterReceiver(this.mDateReceiver);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            void requestData() {
                BaseEngine.this.onDataUpdate(1, Integer.valueOf(this.mCurStepCount), Integer.valueOf(this.mTotalStepTarget));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null && listener.getDataType() == 1) {
                    listener.onDataUpdate(1, Integer.valueOf(this.mCurStepCount), Integer.valueOf(this.mTotalStepTarget));
                }
            }
        }

        private class TimeDataProvider extends WatchDataProvider {
            private int ampm;
            private int hours;
            private long lastTime;
            private int minutes;
            private int seconds;

            private TimeDataProvider() {
                super();
                this.seconds = 30;
                this.minutes = 9;
                this.hours = 10;
                this.ampm = -1;
            }

            void register() {
            }

            void unregister() {
            }

            void onDestroy() {
            }

            private void updateData() {
                boolean hourFormat12 = true;
                if (this.lastTime != BaseEngine.this.mCalendar.getTimeInMillis()) {
                    this.lastTime = BaseEngine.this.mCalendar.getTimeInMillis();
                    this.seconds = BaseEngine.this.mCalendar.get(13);
                    this.minutes = BaseEngine.this.mCalendar.get(12);
                    if (BaseEngine.this.getHourFormat() != 1) {
                        hourFormat12 = false;
                    }
                    if (hourFormat12) {
                        this.hours = BaseEngine.this.mCalendar.get(10);
                        if (this.hours == 0) {
                            this.hours = 12;
                        }
                        this.ampm = BaseEngine.this.mCalendar.get(9);
                        return;
                    }
                    this.hours = BaseEngine.this.mCalendar.get(11);
                    this.ampm = -1;
                }
            }

            void requestData() {
                updateData();
                BaseEngine.this.onDataUpdate(7, Integer.valueOf(this.seconds), Integer.valueOf(this.minutes), Integer.valueOf(this.hours), Integer.valueOf(this.ampm));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null) {
                    updateData();
                    listener.onDataUpdate(7, Integer.valueOf(this.seconds), Integer.valueOf(this.minutes), Integer.valueOf(this.hours), Integer.valueOf(this.ampm));
                }
            }
        }

        private class TodayDistanceDataProvider extends WatchDataProvider {
            private double distance;
            private ContentObserver mTodayDistanceObserver;
            private Runnable mUpdateTodayDistanceTask;

            class C01471 implements Runnable {
                C01471() {
                }

                public void run() {
                    Log.d("HmWatchFace", "TodayDistanceDataProvider onChange.");
                    TodayDistanceDataProvider.this.updateSportTodayDistance();
                }
            }

            private TodayDistanceDataProvider() {
                super();
                this.mUpdateTodayDistanceTask = new C01471();
                this.mTodayDistanceObserver = new ContentObserver(null) {
                    public void onChange(boolean selfChange) {
                        BaseEngine.this.mUpdateTimeHandler.removeCallbacks(TodayDistanceDataProvider.this.mUpdateTodayDistanceTask);
                        BaseEngine.this.mUpdateTimeHandler.post(TodayDistanceDataProvider.this.mUpdateTodayDistanceTask);
                    }
                };
            }

            void register() {
                BaseEngine.this.this$0.mContext.getContentResolver().registerContentObserver(Secure.getUriFor("sport_today_distance"), false, this.mTodayDistanceObserver);
                updateSportTodayDistance();
            }

            void unregister() {
                BaseEngine.this.this$0.mContext.getContentResolver().unregisterContentObserver(this.mTodayDistanceObserver);
                BaseEngine.this.mUpdateTimeHandler.removeCallbacks(this.mUpdateTodayDistanceTask);
            }

            void onDestroy() {
                unregister();
            }

            void requestData() {
                BaseEngine.this.onDataUpdate(2, Double.valueOf(this.distance), Integer.valueOf(BaseEngine.this.mMeasurement));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null && listener.getDataType() == 2) {
                    listener.onDataUpdate(2, Double.valueOf(this.distance), Integer.valueOf(BaseEngine.this.mMeasurement));
                }
            }

            private void updateSportTodayDistance() {
                this.distance = BaseEngine.this.convertKm(SingletonWrapper.getInstance(BaseEngine.this.this$0.mContext).getTodayDistance());
                BaseEngine.this.onDataUpdate(2, Double.valueOf(this.distance), Integer.valueOf(BaseEngine.this.mMeasurement));
            }
        }

        private class TotalDistanceDataProvider extends WatchDataProvider {
            private double distance;
            private ContentObserver mTotalDistanceObserver;
            private Runnable mUpdateTotalDistanceTask;

            class C01491 implements Runnable {
                C01491() {
                }

                public void run() {
                    Log.d("HmWatchFace", "TotalDistanceDataProvider onChange.");
                    TotalDistanceDataProvider.this.updateSportTotalDistance();
                }
            }

            private TotalDistanceDataProvider() {
                super();
                this.mUpdateTotalDistanceTask = new C01491();
                this.mTotalDistanceObserver = new ContentObserver(null) {
                    public void onChange(boolean selfChange) {
                        BaseEngine.this.mUpdateTimeHandler.removeCallbacks(TotalDistanceDataProvider.this.mUpdateTotalDistanceTask);
                        BaseEngine.this.mUpdateTimeHandler.post(TotalDistanceDataProvider.this.mUpdateTotalDistanceTask);
                    }
                };
            }

            void register() {
                BaseEngine.this.this$0.mContext.getContentResolver().registerContentObserver(Secure.getUriFor("sport_total_distance"), false, this.mTotalDistanceObserver);
                updateSportTotalDistance();
            }

            void unregister() {
                BaseEngine.this.this$0.mContext.getContentResolver().unregisterContentObserver(this.mTotalDistanceObserver);
                BaseEngine.this.mUpdateTimeHandler.removeCallbacks(this.mUpdateTotalDistanceTask);
            }

            void onDestroy() {
                unregister();
            }

            void requestData() {
                BaseEngine.this.onDataUpdate(3, Double.valueOf(this.distance), Integer.valueOf(BaseEngine.this.mMeasurement));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null && listener.getDataType() == 3) {
                    listener.onDataUpdate(3, Double.valueOf(this.distance), Integer.valueOf(BaseEngine.this.mMeasurement));
                }
            }

            private void updateSportTotalDistance() {
                this.distance = BaseEngine.this.convertKm(SingletonWrapper.getInstance(BaseEngine.this.this$0.mContext).getTotalDistance());
                BaseEngine.this.onDataUpdate(3, Double.valueOf(this.distance), Integer.valueOf(BaseEngine.this.mMeasurement));
            }
        }

        private class WeatherDataProvider extends WatchDataProvider {
            private Runnable mUpdateWeatherTask;
            private ContentObserver mWeatherDataObserver;
            private String tempFlag;
            private String tempString;
            private int weatherType;

            class C01511 implements Runnable {
                C01511() {
                }

                public void run() {
                    Log.d("HmWatchFace", "WeatherDataProvider onChange.");
                    WeatherDataProvider.this.updateWeatherData();
                    if (BaseEngine.this.this$0.mSlptService != null) {
                        BaseEngine.this.this$0.mContext.stopService(BaseEngine.this.this$0.mSlptService);
                        BaseEngine.this.this$0.mContext.startService(BaseEngine.this.this$0.mSlptService);
                    }
                }
            }

            private WeatherDataProvider() {
                super();
                this.weatherType = -1;
                this.mUpdateWeatherTask = new C01511();
                this.mWeatherDataObserver = new ContentObserver(null) {
                    public void onChange(boolean selfChange) {
                        BaseEngine.this.mUpdateTimeHandler.removeCallbacks(WeatherDataProvider.this.mUpdateWeatherTask);
                        BaseEngine.this.mUpdateTimeHandler.post(WeatherDataProvider.this.mUpdateWeatherTask);
                    }
                };
            }

            void register() {
                BaseEngine.this.this$0.mContext.getContentResolver().registerContentObserver(System.getUriFor("WeatherCheckedSummary"), false, this.mWeatherDataObserver);
                updateWeatherData();
            }

            void unregister() {
                BaseEngine.this.this$0.mContext.getContentResolver().unregisterContentObserver(this.mWeatherDataObserver);
                BaseEngine.this.mUpdateTimeHandler.removeCallbacks(this.mUpdateWeatherTask);
            }

            void onDestroy() {
                unregister();
            }

            void requestData() {
                BaseEngine.this.onDataUpdate(8, this.tempFlag, this.tempString, Integer.valueOf(this.weatherType));
            }

            void requestData(WatchDataListener listener) {
                if (listener != null && listener.getDataType() == 8) {
                    listener.onDataUpdate(8, this.tempFlag, this.tempString, Integer.valueOf(this.weatherType));
                }
            }

            private void updateWeatherData() {
                String weatherData = System.getString(BaseEngine.this.this$0.mContext.getContentResolver(), "WeatherCheckedSummary");
                if (weatherData != null) {
                    try {
                        JSONObject weatherJson = new JSONObject(weatherData);
                        String tempFlag = weatherJson.getString("tempUnit");
                        String tempString = weatherJson.getString("temp");
                        int weatherType = weatherJson.getInt("weatherCodeFrom");
                        if (!tempFlag.equals(this.tempFlag) || !tempString.equals(this.tempString) || weatherType != this.weatherType) {
                            Log.d("HmWatchFace", "  handle weather change.");
                            this.tempFlag = tempFlag;
                            this.tempString = tempString;
                            this.weatherType = weatherType;
                            BaseEngine.this.onDataUpdate(8, tempFlag, tempString, Integer.valueOf(weatherType));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        protected abstract void onPrepareResources(Resources resources);

        public BaseEngine(AbstractWatchFace abstractWatchFace) {
            int i = 0;
            this.this$0 = abstractWatchFace;
            super();
            if (Util.IS_OVERSEA) {
                i = 1;
            }
            this.mTimeFormat = i;
            this.mTimeFormatChangedCallback = new C01407();
            this.mTimeFormatObserver = new ContentObserver(null) {
                public void onChange(boolean selfChange) {
                    BaseEngine.this.mUpdateTimeHandler.removeCallbacks(BaseEngine.this.mTimeFormatChangedCallback);
                    BaseEngine.this.mUpdateTimeHandler.post(BaseEngine.this.mTimeFormatChangedCallback);
                }
            };
            this.mTargetStep = 8000;
            this.mTargetStepObserver = new ContentObserver(null) {
                public void onChange(boolean selfChange) {
                    super.onChange(selfChange);
                    String str = WatchSettings.get(BaseEngine.this.this$0.mContext.getContentResolver(), "huami.watch.health.config");
                    if (str != null) {
                        try {
                            int step = new JSONObject(str).optInt("step_target", 8000);
                            if (BaseEngine.this.mTargetStep != step) {
                                BaseEngine.this.mTargetStep = step;
                                if (BaseEngine.this.this$0.mSlptService != null) {
                                    BaseEngine.this.this$0.mContext.stopService(BaseEngine.this.this$0.mSlptService);
                                    BaseEngine.this.this$0.mContext.startService(BaseEngine.this.this$0.mSlptService);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            this.mWidgets = new ArrayList();
            this.mLock = new Object();
            this.mDataListenersMap = new SparseArray();
            this.mDataProvider = new SparseArray();
        }

        protected void onTimeUpdate() {
        }

        public void onCreate(SurfaceHolder holder) {
            if (Log.isLoggable("HmWatchFace", 3)) {
                Log.d("HmWatchFace", "onCreate");
            }
            super.onCreate(holder);
            setWatchFaceStyle(new Builder(this.this$0).setCardPeekMode(1).setBackgroundVisibility(0).setShowSystemUiTime(false).build());
            this.mCalendar = Calendar.getInstance();
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            registerMeasurementObserverObserver();
            registerTimeFormatObserverObserver();
            registerTargetStepObserver();
            onPrepareResources(this.this$0.getResources());
            registermAtWatchFaceObserverObserver();
        }

        public void onDestroy() {
            this.mUpdateTimeHandler.removeMessages(0);
            unRegistermAtWatchFaceObserverObserver();
            unRegisterMeasurementObserverObserver();
            unRegisterTimeFormatObserverObserver();
            unRegisterTargetStepObserver();
            destroyWatchDataListeners();
            onDestroyWidgets();
            super.onDestroy();
        }

        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            this.mLowBitAmbient = properties.getBoolean("low_bit_ambient", false);
            if (Log.isLoggable("HmWatchFace", 3)) {
                Log.d("HmWatchFace", "onPropertiesChanged: low-bit ambient = " + this.mLowBitAmbient);
            }
        }

        public void onTimeTick() {
            if (Log.isLoggable("HmWatchFace", 3)) {
                Log.d("HmWatchFace", "onTimeTick: ambient = " + isInAmbientMode());
            }
            invalidate();
        }

        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            if (Log.isLoggable("HmWatchFace", 3)) {
                Log.d("HmWatchFace", "onAmbientModeChanged: " + inAmbientMode);
            }
            if (this.mLowBitAmbient && inAmbientMode) {
                invalidate();
                updateTimer();
            } else {
                invalidate();
                updateTimer();
            }
        }

        public void onInterruptionFilterChanged(int interruptionFilter) {
            super.onInterruptionFilterChanged(interruptionFilter);
            boolean inMuteMode = interruptionFilter == 3;
            if (this.mMute != inMuteMode) {
                this.mMute = inMuteMode;
                invalidate();
            }
        }

        public void onDraw(Canvas canvas, Rect bounds) {
            this.mCalendar.setTimeInMillis(System.currentTimeMillis());
            int width = bounds.width();
            int height = width;
            float centerX = ((float) width) / 2.0f;
            float centerY = ((float) height) / 2.0f;
            updateDataProviderOnDraw();
            canvas.save(2);
            canvas.clipPath(this.mClip);
            onDrawWatchFace(canvas, (float) width, (float) height, centerX, centerY, this.mCalendar);
            canvas.restore();
        }

        protected void onDrawWatchFace(Canvas canvas, float width, float height, float centerX, float centerY, Calendar calendar) {
            onDrawWidgets(canvas);
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (Log.isLoggable("HmWatchFace", 3)) {
                Log.d("HmWatchFace", "onVisibilityChanged: " + visible);
            }
            if (visible) {
                registerReceiver();
                this.mCalendar.setTimeZone(TimeZone.getDefault());
            } else {
                unregisterReceiver();
            }
            updateTimer();
        }

        private void registerReceiver() {
            if (!this.mRegisteredTimeZoneReceiver) {
                this.mRegisteredTimeZoneReceiver = true;
                this.this$0.mContext.registerReceiver(this.mTimeZoneReceiver, new IntentFilter("android.intent.action.TIMEZONE_CHANGED"));
            }
        }

        private void unregisterReceiver() {
            if (this.mRegisteredTimeZoneReceiver) {
                this.mRegisteredTimeZoneReceiver = false;
                this.this$0.mContext.unregisterReceiver(this.mTimeZoneReceiver);
            }
        }

        private void updateTimer() {
            if (Log.isLoggable("HmWatchFace", 3)) {
                Log.d("HmWatchFace", "updateTimer");
            }
            this.mUpdateTimeHandler.removeMessages(0);
            if (shouldTimerBeRunning()) {
                this.mUpdateTimeHandler.sendEmptyMessage(0);
            }
        }

        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        private void registermAtWatchFaceObserverObserver() {
            this.this$0.mContext.getContentResolver().registerContentObserver(Secure.getUriFor("prop.launcher.at_watchface"), false, this.mAtWatchFaceObserver);
            updateAtWatchFaceValue();
        }

        private void unRegistermAtWatchFaceObserverObserver() {
            this.this$0.mContext.getContentResolver().unregisterContentObserver(this.mAtWatchFaceObserver);
            this.mUpdateTimeHandler.post(this.mAtWatchFaceCallback);
        }

        private void updateAtWatchFaceValue() {
            boolean isAtWatchFace = this.mIsAtWatchFace;
            try {
                isAtWatchFace = ((int) Secure.getFloat(this.this$0.mContext.getContentResolver(), "prop.launcher.at_watchface")) == 1;
            } catch (SettingNotFoundException e) {
            }
            if (isAtWatchFace != this.mIsAtWatchFace) {
                this.mIsAtWatchFace = isAtWatchFace;
                onAtWatchFaceChange(this.mIsAtWatchFace);
                onAtWatchFaceChangeInternal(this.mIsAtWatchFace);
            }
        }

        protected void onAtWatchFaceChange(boolean isAtWatchFace) {
        }

        private double convertKm(double distance) {
            switch (this.mMeasurement) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    return Util.convertKmToMi(distance);
                default:
                    return distance;
            }
        }

        protected final int getMeasurement() {
            return this.mMeasurement;
        }

        protected void onMeasurementChanged(int measurement) {
        }

        private void registerMeasurementObserverObserver() {
            this.mMeasurement = Util.getMeasurement(this.this$0.mContext);
            this.this$0.mContext.getContentResolver().registerContentObserver(Secure.getUriFor("measurement"), false, this.mMeasurementObserver);
        }

        private void unRegisterMeasurementObserverObserver() {
            this.this$0.mContext.getContentResolver().unregisterContentObserver(this.mMeasurementObserver);
            this.mUpdateTimeHandler.removeCallbacks(this.mMeasurementChangedCallback);
        }

        private void registerTimeFormatObserverObserver() {
            this.mTimeFormat = DateFormat.is24HourFormat(this.this$0.mContext) ? 0 : 1;
            Log.d("HmWatchFace", "registerTimeFormatObserverObserver TimeFormat = " + this.mTimeFormat);
            this.this$0.mContext.getContentResolver().registerContentObserver(System.getUriFor("time_12_24"), false, this.mTimeFormatObserver);
        }

        private void unRegisterTimeFormatObserverObserver() {
            this.this$0.mContext.getContentResolver().unregisterContentObserver(this.mTimeFormatObserver);
            this.mUpdateTimeHandler.removeCallbacks(this.mTimeFormatChangedCallback);
        }

        private void registerTargetStepObserver() {
            this.this$0.mContext.getContentResolver().registerContentObserver(Uri.parse("content://com.huami.watch.companion.settings"), false, this.mTargetStepObserver);
        }

        private void unRegisterTargetStepObserver() {
            this.this$0.mContext.getContentResolver().unregisterContentObserver(this.mTargetStepObserver);
        }

        protected final int getHourFormat() {
            return this.mTimeFormat;
        }

        protected void onHourFormatChanged(int hourFormat) {
        }

        protected void addWidget(AbsWatchFaceDataWidget widget) {
            if (widget == null) {
                throw new IllegalArgumentException("Cannot add a null widget to a WatchFace");
            } else if (!this.mWidgets.contains(widget)) {
                this.mWidgets.add(widget);
                registerWatchDataListener(widget);
            }
        }

        protected final void onDrawWidgets(Canvas canvas) {
            Iterator i$ = this.mWidgets.iterator();
            while (i$.hasNext()) {
                AbsWatchFaceDataWidget widget = (AbsWatchFaceDataWidget) i$.next();
                if (widget != null) {
                    canvas.translate((float) widget.getX(), (float) widget.getY());
                    widget.onDraw(canvas);
                    canvas.translate((float) (-widget.getX()), (float) (-widget.getY()));
                }
            }
        }

        private final void onDestroyWidgets() {
            Iterator i$ = this.mWidgets.iterator();
            while (i$.hasNext()) {
                AbsWatchFaceDataWidget widget = (AbsWatchFaceDataWidget) i$.next();
                widget.onDestroy();
                unregisterWatchDataListener(widget);
            }
            this.mWidgets.clear();
        }

        protected void registerWatchDataListener(WatchDataListener listener) {
            if (listener != null) {
                int dataType = listener.getDataType();
                if (SingletonWrapper.isSupportDataType(dataType)) {
                    registerDataProvider(dataType);
                    synchronized (this.mLock) {
                        LinkedList<WatchDataListener> listeners = (LinkedList) this.mDataListenersMap.get(dataType);
                        if (listeners == null) {
                            listeners = new LinkedList();
                            this.mDataListenersMap.put(dataType, listeners);
                        }
                        listeners.addLast(listener);
                    }
                    requestWatchData(listener);
                }
            }
        }

        protected void unregisterWatchDataListener(WatchDataListener listener) {
            if (listener != null) {
                int dataType = listener.getDataType();
                synchronized (this.mLock) {
                    LinkedList<WatchDataListener> listeners = (LinkedList) this.mDataListenersMap.get(dataType);
                    if (listeners != null) {
                        listeners.remove(listener);
                        if (listeners.size() < 1) {
                            this.mDataListenersMap.remove(dataType);
                            unregisterDataProvider(dataType);
                        }
                    }
                }
            }
        }

        private void destroyWatchDataListeners() {
            synchronized (this.mLock) {
                if (this.mDataListenersMap.size() > 0) {
                    for (int i = 0; i < this.mDataListenersMap.size(); i++) {
                        unregisterDataProvider(this.mDataListenersMap.keyAt(i));
                    }
                    this.mDataListenersMap.clear();
                }
            }
        }

        private boolean containsDataListener(int dataType) {
            boolean z;
            synchronized (this.mLock) {
                z = this.mDataListenersMap.indexOfKey(dataType) >= 0;
            }
            return z;
        }

        private void onDataUpdate(int dataType, Object... values) {
            synchronized (this.mLock) {
                LinkedList<WatchDataListener> listeners = (LinkedList) this.mDataListenersMap.get(dataType);
                if (listeners != null) {
                    Iterator i$ = listeners.iterator();
                    while (i$.hasNext()) {
                        ((WatchDataListener) i$.next()).onDataUpdate(dataType, values);
                    }
                }
            }
        }

        private void requestWatchData(WatchDataListener listener) {
            if (listener != null) {
                WatchDataProvider dataProvider = getWatchDataProvider(listener.getDataType());
                if (dataProvider != null) {
                    dataProvider.requestData(listener);
                }
            }
        }

        private void onAtWatchFaceChangeInternal(boolean isAtWatchFace) {
            WatchDataProvider dataProvider;
            if (isAtWatchFace) {
                dataProvider = (WatchDataProvider) this.mDataProvider.get(1);
                if (dataProvider != null) {
                    dataProvider.register();
                }
                dataProvider = (WatchDataProvider) this.mDataProvider.get(2);
                if (dataProvider != null) {
                    dataProvider.requestData();
                }
                dataProvider = (WatchDataProvider) this.mDataProvider.get(12);
                if (dataProvider != null) {
                    dataProvider.requestData();
                    return;
                }
                return;
            }
            dataProvider = (WatchDataProvider) this.mDataProvider.get(1);
            if (dataProvider != null) {
                dataProvider.unregister();
            }
        }

        private void updateDataProviderOnDraw() {
            WatchDataProvider dataProvider;
            if (containsDataListener(7)) {
                dataProvider = getWatchDataProvider(7);
                if (dataProvider != null) {
                    dataProvider.requestData();
                }
            }
            if (containsDataListener(6)) {
                dataProvider = getWatchDataProvider(6);
                if (dataProvider != null) {
                    dataProvider.requestData();
                }
            }
            if (containsDataListener(5)) {
                dataProvider = getWatchDataProvider(5);
                if (dataProvider != null) {
                    dataProvider.requestData();
                }
            }
            if (containsDataListener(4)) {
                dataProvider = getWatchDataProvider(4);
                if (dataProvider != null) {
                    dataProvider.requestData();
                }
            }
        }

        private void registerDataProvider(int dataType) {
            Log.d("HmWatchFace", "registerDataProvider " + dataType);
            if (this.mDataProvider.get(dataType) == null) {
                switch (dataType) {
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        this.mDataProvider.put(1, new StepDataProvider());
                        return;
                    case 2:
                        TodayDistanceDataProvider todayDistanceDataProvider = new TodayDistanceDataProvider();
                        todayDistanceDataProvider.register();
                        this.mDataProvider.put(2, todayDistanceDataProvider);
                        return;
                    case 3:
                        TotalDistanceDataProvider totalDistanceDataProvider = new TotalDistanceDataProvider();
                        totalDistanceDataProvider.register();
                        this.mDataProvider.put(3, totalDistanceDataProvider);
                        return;
                    case 4:
                        this.mDataProvider.put(4, new CaloriesDataProvider());
                        return;
                    case 5:
                        HeartRateDataProvider heartRateDataProvider = new HeartRateDataProvider();
                        heartRateDataProvider.register();
                        this.mDataProvider.put(5, heartRateDataProvider);
                        return;
                    case 6:
                        DateDataProvider dateDataProvider = new DateDataProvider();
                        dateDataProvider.register();
                        this.mDataProvider.put(6, dateDataProvider);
                        return;
                    case 7:
                        TimeDataProvider timeDataProvider = new TimeDataProvider();
                        timeDataProvider.register();
                        this.mDataProvider.put(7, timeDataProvider);
                        return;
                    case 8:
                        WeatherDataProvider weatherDataProvider = new WeatherDataProvider();
                        weatherDataProvider.register();
                        this.mDataProvider.put(8, weatherDataProvider);
                        return;
                    case 10:
                        BatteryDataProvider batteryDataProvider = new BatteryDataProvider();
                        batteryDataProvider.register();
                        this.mDataProvider.put(10, batteryDataProvider);
                        return;
                    case 12:
                        FloorDataProvider floorDataProvider = new FloorDataProvider();
                        floorDataProvider.register();
                        this.mDataProvider.put(12, floorDataProvider);
                        return;
                    default:
                        return;
                }
            }
        }

        private void unregisterDataProvider(int dataType) {
            Log.d("HmWatchFace", "unregisterDataProvider " + dataType);
            WatchDataProvider dataProvider = (WatchDataProvider) this.mDataProvider.get(dataType);
            if (dataProvider != null) {
                dataProvider.onDestroy();
                this.mDataProvider.remove(dataType);
            }
        }

        private WatchDataProvider getWatchDataProvider(int dataType) {
            return (WatchDataProvider) this.mDataProvider.get(dataType);
        }

        protected AbsWatchFaceDataWidget getDataWidget(Resources resources, DataWidgetConfig config) {
            int dataType = config.getDataType();
            int x = config.getX();
            int y = config.getY();
            int model = config.getModel();
            switch (dataType) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    return new WalkDateWidget(resources, x, y, model);
                case 2:
                    return new TodayDistanceDateWidget(resources, x, y, model);
                case 3:
                    return new MileageDateWidget(resources, x, y, model);
                case 4:
                    return new FatBurnDataWidget(resources, x, y, model);
                case 5:
                    return new HeartRateDateWidget(resources, x, y, model);
                case 6:
                    if (Util.needEnAssets() && model == 3) {
                        return null;
                    }
                    return new DigitDateWidget(resources, x, y, model);
                case 8:
                    return new WeatherDateWidget(resources, x, y, model);
                case 10:
                    return new BatteryRemindDateWidget(resources, x, y, model);
                case 12:
                    return new TodayFloorDateWidget(resources, x, y, model);
                default:
                    return null;
            }
        }
    }

    public abstract class AnalogEngine extends BaseEngine {
        private float animHorRot = 300.0f;
        private float animMinRot = 54.0f;
        private float animSecRot = 180.0f;
        private boolean init = false;
        private boolean initAnim = false;

        class C01321 extends AnimatorListenerAdapter {
            C01321() {
            }

            public void onAnimationEnd(Animator animation) {
                AnalogEngine.this.init = true;
                AnalogEngine.this.initAnim = false;
            }
        }

        public AnalogEngine() {
            super(AbstractWatchFace.this);
        }

        public void onDrawWatchFace(Canvas canvas, float width, float height, float centerX, float centerY, Calendar calendar) {
            int seconds = calendar.get(13);
            int minutes = calendar.get(12);
            float secRot = (float) (seconds * 6);
            float minRot = (float) (minutes * 6);
            float hrRot = (float) (((calendar.get(10) * 5) * 6) + ((minutes / 12) * 6));
            if (this.init) {
                onDrawAnalog(canvas, width, height, centerX, centerY, secRot, minRot, hrRot);
            } else if (this.initAnim) {
                onDrawAnalog(canvas, width, height, centerX, centerY, this.animSecRot, this.animMinRot, this.animHorRot);
            } else {
                float tSecRot = secRot + 6.0f;
                float tMinRot = minRot;
                float tHorRot = hrRot;
                ValueAnimator anim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
                anim.setDuration(500);
                anim.setStartDelay(800);
                anim.setInterpolator(AbstractWatchFace.QUART);
                anim.addListener(new C01321());
                final float f = tSecRot;
                final float f2 = tMinRot;
                final float f3 = tHorRot;
                anim.addUpdateListener(new AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float input = ((Float) animation.getAnimatedValue()).floatValue();
                        AnalogEngine.this.animSecRot = ((f - 180.0f) * input) + 180.0f;
                        AnalogEngine.this.animMinRot = ((f2 - 54.0f) * input) + 54.0f;
                        AnalogEngine.this.animHorRot = ((f3 - 300.0f) * input) + 300.0f;
                        AnalogEngine.this.invalidate();
                    }
                });
                anim.start();
                this.initAnim = true;
                invalidate();
            }
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            onDrawWidgets(canvas);
        }
    }

    public abstract class DigitalEngine extends BaseEngine {
        private int animHor = 10;
        private int animMin = 9;
        private int animSec = 30;
        private boolean init = false;
        private boolean initAnim = false;
        boolean mDrawTimeIndicator = true;

        class C01531 extends AnimatorListenerAdapter {
            C01531() {
            }

            public void onAnimationEnd(Animator animation) {
                DigitalEngine.this.init = true;
                DigitalEngine.this.initAnim = false;
            }
        }

        public DigitalEngine() {
            super(AbstractWatchFace.this);
        }

        protected void onTimeUpdate() {
            this.mDrawTimeIndicator = !this.mDrawTimeIndicator;
        }

        public void onDrawWatchFace(Canvas canvas, float width, float height, float centerX, float centerY, Calendar calendar) {
            int hours;
            int ampm;
            boolean hourFormat12 = getHourFormat() == 1;
            int seconds = calendar.get(13);
            int minutes = calendar.get(12);
            if (hourFormat12) {
                hours = calendar.get(10);
                if (hours == 0) {
                    hours = 12;
                }
                ampm = calendar.get(9);
            } else {
                hours = calendar.get(11);
                ampm = 11;
            }
            int year = calendar.get(1);
            int month = calendar.get(2) + 1;
            int day = calendar.get(5);
            int week = calendar.get(7);
            if (this.init) {
                onDrawDigital(canvas, width, height, centerX, centerY, seconds, minutes, hours, year, month, day, week, ampm);
            } else if (this.initAnim) {
                onDrawDigital(canvas, width, height, centerX, centerY, this.animSec, this.animMin, this.animHor, year, month, day, week, ampm);
            } else {
                int tSec = seconds + 1;
                int tMin = minutes;
                int tHor = hours;
                ValueAnimator anim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
                anim.setDuration((long) getInitAnimDuration());
                anim.setStartDelay(800);
                anim.setInterpolator(AbstractWatchFace.QUAD);
                anim.addListener(new C01531());
                final int i = tSec;
                final int i2 = tMin;
                final int i3 = tHor;
                anim.addUpdateListener(new AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float input = ((Float) animation.getAnimatedValue()).floatValue();
                        DigitalEngine.this.animSec = (int) (30.0f + (((float) (i - 30)) * input));
                        DigitalEngine.this.animMin = (int) (9.0f + (((float) (i2 - 9)) * input));
                        DigitalEngine.this.animHor = (int) (10.0f + (((float) (i3 - 10)) * input));
                        DigitalEngine.this.invalidate();
                    }
                });
                anim.start();
                this.initAnim = true;
                invalidate();
            }
        }

        protected int getInitAnimDuration() {
            return 500;
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            onDrawWidgets(canvas);
        }
    }

    public class SlptWatchFaceReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.huami.watchface.SlptWatchService.disable.sportView")) {
                if (Util.getSportfaceValue() == 1) {
                    Log.i("HmWatchFace", "in sport mode do not need to enable clock");
                    return;
                }
                Util.cleanSportItemOrder();
                Log.i("HmWatchFace", "enable clock slpt view " + Util.getSportfaceValue());
                Util.sHasWatchFaceStarted++;
                if (AbstractWatchFace.this.mSlptService != null) {
                    AbstractWatchFace.this.mContext.startService(AbstractWatchFace.this.mSlptService);
                }
            } else if (intent.getAction().equals("android.intent.action.LOCALE_CHANGED")) {
                Log.i("HmWatchFace", "change local new slpt watchface");
                if (AbstractWatchFace.this.mSlptService != null) {
                    AbstractWatchFace.this.mContext.stopService(AbstractWatchFace.this.mSlptService);
                    AbstractWatchFace.this.mContext.startService(AbstractWatchFace.this.mSlptService);
                }
            } else if (intent.getAction().equals("com.huami.watchface.SlptWatchService.enable.sportView")) {
                Log.i("HmWatchFace", "disenable clock slpt view");
            } else if ("com.huami.intent.action.WATCHFACE_CONFIG_CHANGED".equals(intent.getAction())) {
                Log.i("HmWatchFace", "change watchface config");
                if (AbstractWatchFace.this.mSlptService != null) {
                    AbstractWatchFace.this.mContext.stopService(AbstractWatchFace.this.mSlptService);
                    AbstractWatchFace.this.mContext.startService(AbstractWatchFace.this.mSlptService);
                }
            } else if (intent.getAction().equals("com.huami.watchface.SlptWatchService.leave_gps")) {
                if (Util.getSportfaceValue() == 1) {
                    Log.e("HmWatchFace", "leave GPS page enter sport mode");
                } else if (intent.getBooleanExtra("is_start_sport", false)) {
                    Log.i("HmWatchFace", "leave GPS page enter sport mode");
                } else {
                    Util.showWatchFace(AbstractWatchFace.this.mContext);
                    Log.i("HmWatchFace", "leave GPS page start clock page ");
                    if (AbstractWatchFace.this.mSlptService != null) {
                        AbstractWatchFace.this.mContext.startService(AbstractWatchFace.this.mSlptService);
                    }
                }
            } else if (intent.getAction().equals("com.huami.watchface.SlptWatchService.enable.otherScreen")) {
                Log.v("HmWatchFace", "start other screen");
                if (AbstractWatchFace.this.slptScreenManager.createScreen(intent) == 0) {
                    AbstractWatchFace.this.slptScreenManager.showScreen();
                }
            } else if (intent.getAction().equals("com.huami.watchface.SlptWatchService.disable.otherScreen")) {
                Log.v("HmWatchFace", "stop other screen");
                AbstractWatchFace.this.slptScreenManager.hideScreen();
                if (AbstractWatchFace.this.mSlptService != null) {
                    AbstractWatchFace.this.startService(AbstractWatchFace.this.mSlptService);
                }
            }
        }
    }

    protected abstract Class<? extends AbstractSlptClock> slptClockClass();

    public void onCreate() {
        super.onCreate();
        this.mContext = getApplicationContext();
        this.mSlptWatchFaceReceiver = new SlptWatchFaceReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.huami.watchface.SlptWatchService.disable.sportView");
        filter.addAction("com.huami.watchface.SlptWatchService.enable.sportView");
        filter.addAction("com.huami.watchface.SlptWatchService.leave_gps");
        filter.addAction("com.huami.watchface.SlptWatchService.enable.otherScreen");
        filter.addAction("com.huami.watchface.SlptWatchService.disable.otherScreen");
        filter.addAction("android.intent.action.LOCALE_CHANGED");
        filter.addAction("com.huami.intent.action.WATCHFACE_CONFIG_CHANGED");
        filter.setPriority(1);
        this.mContext.registerReceiver(this.mSlptWatchFaceReceiver, filter);
        this.slptScreenManager = SlptOtherScreenManager.getInstance(this.mContext);
        Util.sHasWatchFaceStarted++;
        this.mSlptService = new Intent(this.mContext, slptClockClass());
        this.mContext.startService(this.mSlptService);
        Util.setCurrentSlpt(this.mContext, slptClockClass().getSimpleName());
        Log.i(super.getClass().getSimpleName(), "start SLPT Service [ " + slptClockClass() + " ] : ");
    }

    public boolean onUnbind(Intent intent) {
        Util.sHasWatchFaceStarted--;
        if (!(this.mSlptService == null || slptClockClass().getSimpleName().equals(Util.getCurrentSlpt(this.mContext)))) {
            Log.i(super.getClass().getSimpleName(), "Stop SLPT Service [ " + slptClockClass() + " ] : " + stopService(this.mSlptService));
        }
        notifyStatusBarPosition(-1.0f);
        this.mContext.unregisterReceiver(this.mSlptWatchFaceReceiver);
        return super.onUnbind(intent);
    }

    protected void notifyStatusBarPosition(float posX, float posY) {
        Util.notifyStatusBarPositionUpdated(this, posX, posY);
    }

    protected void notifyStatusBarPosition(float posY) {
        Util.notifyStatusBarPositionUpdated(this, posY);
    }
}
