package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.WatchFaceConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.huami.watch.watchface.widget.AbsWatchFaceDataWidget;
import com.huami.watch.watchface.widget.BatteryCircleWidget;
import com.huami.watch.watchface.widget.StepTextWidget;
import com.huami.watch.watchface.widget.TodayDistanceProgressWidget;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class AnalogWatchFaceEleven extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private Bitmap big_circle;
        private WatchFaceConfig config;
        private Drawable mBgDrawable;
        private Paint mBitmapPaint;
        private RectF mBound;
        private Path mClip;
        private Path mClipPathBig;
        private Path mClipPathIn;
        private int mCurStepCount;
        private Paint mGPaint;
        private Bitmap mGraduation;
        private Bitmap mHourBitmap;
        private Bitmap mMinBitmap;
        private int mProgressDegreeStep;
        private Bitmap mSecBitmap;
        private WatchDataListener mStepDataListener;
        private int mTotalStepTarget;

        class C01561 implements WatchDataListener {
            C01561() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 1) {
                    Engine.this.mCurStepCount = ((Integer) values[0]).intValue();
                    Engine.this.updateStepInfo();
                }
            }

            public int getDataType() {
                return 1;
            }
        }

        private Engine() {
            super();
            this.mClip = new Path();
            this.mCurStepCount = 0;
            this.mTotalStepTarget = 8000;
            this.mBound = new RectF(0.0f, 0.0f, 320.0f, 320.0f);
            this.mClipPathBig = new Path();
            this.mClipPathIn = new Path();
            this.mStepDataListener = new C01561();
        }

        protected void onPrepareResources(Resources resources) {
            int i;
            this.mBitmapPaint = new Paint(7);
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.config = WatchFaceConfig.getWatchFaceConfig(AnalogWatchFaceEleven.this.getApplicationContext(), AnalogWatchFaceEleven.class.getName());
            if (this.config != null) {
                switch (this.config.getBgType()) {
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        this.mBgDrawable = this.config.getBgDrawable();
                        break;
                    case 4:
                        this.mBgDrawable = this.config.getBgDrawable();
                        break;
                }
            }
            if (this.mBgDrawable == null) {
                this.mBgDrawable = resources.getDrawable(R.drawable.watchface_default_eleven_bg);
            }
            this.mBgDrawable.setBounds(0, 0, 320, 320);
            this.mHourBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_eleven_hour);
            this.mMinBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_eleven_minute);
            this.mSecBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_eleven_seconds);
            this.mGraduation = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_eleven_graduation);
            this.big_circle = decodeImage(resources, "guard/shiguanglicheng/watchface_slpt_big_circle.png");
            this.mGPaint = new Paint(3);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setStrokeWidth(17.0f);
            this.mClipPathIn.addCircle(160.0f, 160.0f, 142.0f, Direction.CCW);
            Bitmap[] batterys = new Bitmap[18];
            for (i = 0; i < 18; i++) {
                batterys[i] = decodeImage(resources, String.format("guard/chengshijinying/battery_%d.png", new Object[]{Integer.valueOf(i)}));
            }
            BatteryCircleWidget batteryWidget = new BatteryCircleWidget(resources, 227, 160, batterys, decodeImage(resources, "guard/chengshijinying/watchface_default_oval.png"), -90.0f, 360.0f);
            Resources resources2 = resources;
            TodayDistanceProgressWidget todayDistanceWidget = new TodayDistanceProgressWidget(resources2, 93, 160, decodeImage(resources, "guard/chengshijinying/watchface_default_oval.png"), -90.0f, 360.0f);
            Bitmap[] NUMS = new Bitmap[10];
            for (i = 0; i < 10; i++) {
                NUMS[i] = decodeImage(resources, String.format("guard/chengshijinying/step_%d.png", new Object[]{Integer.valueOf(i)}));
            }
            AbsWatchFaceDataWidget stepTextWidget = new StepTextWidget(160, 251, NUMS);
            addWidget(batteryWidget);
            addWidget(todayDistanceWidget);
            addWidget(stepTextWidget);
            registerWatchDataListener(this.mStepDataListener);
            ArrayList<DataWidgetConfig> mWidgets = null;
            if (this.config != null) {
                mWidgets = this.config.getDataWidgets();
            }
            if (mWidgets.size() < 1) {
                mWidgets.add(new DataWidgetConfig(3, 6, 110, 94, 0, null, null));
            }
            for (int j = 0; j < mWidgets.size(); j++) {
                AbsWatchFaceDataWidget widget = getDataWidget(resources, (DataWidgetConfig) mWidgets.get(j));
                if (widget != null) {
                    addWidget(widget);
                }
            }
        }

        private Bitmap decodeImage(Resources resources, String fileName) {
            Bitmap image = null;
            try {
                InputStream is = AnalogWatchFaceEleven.this.getResources().getAssets().open(fileName);
                image = BitmapFactory.decodeStream(is);
                is.close();
                return image;
            } catch (IOException e) {
                e.printStackTrace();
                return image;
            }
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.clipPath(this.mClip);
            if (this.mBgDrawable != null) {
                this.mBgDrawable.draw(canvas);
            }
            canvas.drawBitmap(this.mGraduation, 0.0f, 0.0f, this.mBitmapPaint);
            canvas.save(2);
            canvas.clipPath(this.mClipPathBig, Op.INTERSECT);
            canvas.clipPath(this.mClipPathIn, Op.DIFFERENCE);
            canvas.drawBitmap(this.big_circle, 0.0f, 0.0f, this.mGPaint);
            canvas.restore();
            super.onDrawAnalog(canvas, width, height, centerX, centerY, secRot, minRot, hrRot);
            canvas.save(1);
            canvas.rotate(hrRot, centerX, centerY);
            canvas.translate(centerX - ((float) (this.mHourBitmap.getWidth() / 2)), centerY - ((float) (this.mHourBitmap.getHeight() / 2)));
            canvas.drawBitmap(this.mHourBitmap, 0.0f, 0.0f, this.mBitmapPaint);
            canvas.restore();
            canvas.save(1);
            canvas.rotate(minRot, centerX, centerY);
            canvas.translate(centerX - ((float) (this.mMinBitmap.getWidth() / 2)), centerY - ((float) (this.mMinBitmap.getHeight() / 2)));
            canvas.drawBitmap(this.mMinBitmap, 0.0f, 0.0f, this.mBitmapPaint);
            canvas.restore();
            canvas.save(1);
            canvas.rotate(secRot, centerX, centerY);
            canvas.translate(centerX - ((float) (this.mSecBitmap.getWidth() / 2)), centerY - ((float) (this.mSecBitmap.getHeight() / 2)));
            canvas.drawBitmap(this.mSecBitmap, 0.0f, 0.0f, this.mBitmapPaint);
            canvas.restore();
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(AnalogWatchFaceEleven.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                    updateStepInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void updateStepInfo() {
            if (this.mTotalStepTarget > 0) {
                this.mProgressDegreeStep = (int) (Math.min((((float) this.mCurStepCount) * 1.0f) / ((float) this.mTotalStepTarget), 1.0f) * 304.0f);
            } else {
                this.mProgressDegreeStep = 0;
            }
            this.mClipPathBig.rewind();
            this.mClipPathBig.addArc(this.mBound, 118.0f, (float) this.mProgressDegreeStep);
            this.mClipPathBig.lineTo(160.0f, 160.0f);
            this.mClipPathBig.close();
        }

        public void onDestroy() {
            if (!(this.mHourBitmap == null || this.mHourBitmap.isRecycled())) {
                this.mHourBitmap.recycle();
                this.mHourBitmap = null;
            }
            if (!(this.mMinBitmap == null || this.mMinBitmap.isRecycled())) {
                this.mMinBitmap.recycle();
                this.mMinBitmap = null;
            }
            if (!(this.mSecBitmap == null || this.mSecBitmap.isRecycled())) {
                this.mSecBitmap.recycle();
                this.mSecBitmap = null;
            }
            super.onDestroy();
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportElevenSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(35.0f);
        return new Engine();
    }
}
