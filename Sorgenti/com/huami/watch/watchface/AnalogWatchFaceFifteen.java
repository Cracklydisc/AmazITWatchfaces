package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.util.WatchFaceConfig;
import com.huami.watch.watchface.util.WatchFaceConfig.TimeHand;

public class AnalogWatchFaceFifteen extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private WatchFaceConfig config;
        private boolean drawBgFog;
        private Drawable mBgDrawable;
        private Path mClip;
        private Drawable mGraduation;
        private Drawable mHour;
        private Drawable mMinute;
        private Drawable mSeconds;

        private Engine() {
            super();
            this.mClip = new Path();
            this.drawBgFog = false;
        }

        protected void onPrepareResources(Resources resources) {
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.config = WatchFaceConfig.getWatchFaceConfig(AnalogWatchFaceFifteen.this.getApplicationContext(), AnalogWatchFaceFifteen.class.getName());
            if (this.config != null) {
                switch (this.config.getBgType()) {
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        this.mBgDrawable = this.config.getBgDrawable();
                        break;
                    case 4:
                        this.mBgDrawable = this.config.getBgDrawable();
                        this.drawBgFog = true;
                        break;
                }
            }
            if (this.mBgDrawable == null) {
                this.mBgDrawable = resources.getDrawable(R.drawable.watchface_bg_analog_fifteen_0);
            }
            this.mBgDrawable.setBounds(0, 0, 320, 320);
            if (this.config != null) {
                this.mGraduation = this.config.getGraduation();
            }
            if (this.mGraduation == null) {
                this.mGraduation = Util.decodeBitmapDrawableFromAssets(resources, "graduation/06/graduation.png");
            }
            this.mGraduation.setBounds(0, 0, 320, 320);
            TimeHand timeHand = null;
            if (this.config != null) {
                timeHand = this.config.getTimeHand();
                if (timeHand != null) {
                    this.mHour = timeHand.getHour();
                    this.mMinute = timeHand.getMinute();
                    this.mSeconds = timeHand.getSeconds();
                }
            }
            if (timeHand == null) {
                this.mHour = Util.decodeBitmapDrawableFromAssets(resources, "timehand/04/hour.png");
                this.mMinute = Util.decodeBitmapDrawableFromAssets(resources, "timehand/04/minute.png");
                this.mSeconds = Util.decodeBitmapDrawableFromAssets(resources, "timehand/04/seconds.png");
            }
            if (this.mHour != null) {
                int left = 160 - (this.mHour.getIntrinsicWidth() / 2);
                int top = 160 - (this.mHour.getIntrinsicHeight() / 2);
                this.mHour.setBounds(left, top, this.mHour.getIntrinsicWidth() + left, this.mHour.getIntrinsicHeight() + top);
            }
            if (this.mMinute != null) {
                left = 160 - (this.mMinute.getIntrinsicWidth() / 2);
                top = 160 - (this.mMinute.getIntrinsicHeight() / 2);
                this.mMinute.setBounds(left, top, this.mMinute.getIntrinsicWidth() + left, this.mMinute.getIntrinsicHeight() + top);
            }
            if (this.mSeconds != null) {
                left = 160 - (this.mSeconds.getIntrinsicWidth() / 2);
                top = 160 - (this.mSeconds.getIntrinsicHeight() / 2);
                this.mSeconds.setBounds(left, top, this.mSeconds.getIntrinsicWidth() + left, this.mSeconds.getIntrinsicHeight() + top);
            }
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.clipPath(this.mClip);
            if (this.mBgDrawable != null) {
                this.mBgDrawable.draw(canvas);
            }
            if (this.mGraduation != null) {
                this.mGraduation.draw(canvas);
            }
            if (this.mHour != null) {
                canvas.save();
                canvas.rotate(hrRot, centerX, centerY);
                this.mHour.draw(canvas);
                canvas.restore();
            }
            if (this.mMinute != null) {
                canvas.save();
                canvas.rotate(minRot, centerX, centerY);
                this.mMinute.draw(canvas);
                canvas.restore();
            }
            if (this.mSeconds != null) {
                canvas.save();
                canvas.rotate(secRot, centerX, centerY);
                this.mSeconds.draw(canvas);
                canvas.restore();
            }
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return AnalogWatchFaceFifteenSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(35.0f);
        return new Engine();
    }
}
