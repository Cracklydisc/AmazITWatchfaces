package com.huami.watch.watchface;

import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.util.WatchFaceConfig;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedHourWithMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class AnalogWatchFaceFifteenSlpt extends AbstractSlptClock {
    protected void initWatchFaceConfig() {
    }

    protected SlptLayout createClockLayout8C() {
        int i;
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptPredrawedHourWithMinuteView hourView = new SlptPredrawedHourWithMinuteView();
        SlptPictureView lowBatteryView = new SlptPictureView();
        SlptPictureView bgView2 = new SlptPictureView();
        byte[] bgMem = null;
        WatchFaceConfig config = WatchFaceConfig.getWatchFaceConfig(getApplicationContext(), AnalogWatchFaceFifteen.class.getName());
        if (config != null) {
            switch (config.getBgType()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    bgMem = SimpleFile.readFileFromAssets(this, config.getBgPathSlpt());
                    break;
                case 4:
                    bgMem = SimpleFile.readFile(config.getBgPathSlpt());
                    break;
            }
        }
        if (bgMem == null) {
            bgMem = SimpleFile.readFileFromAssets(this, "guard/com.huami.watch.watchface.AnalogWatchFaceFifteen/watchface_bg_analog_fifteen_0.png");
        }
        SlptPictureView bgView = new SlptPictureView();
        bgView.setImagePicture(bgMem);
        frameLayout.background.color = -16777216;
        super.clearDrawdPictureGroup();
        String assetsMinutePathFormat = null;
        String assetsHourPathFormat = null;
        if (config != null) {
            switch (config.getTimeHandType()) {
                case 2:
                    assetsMinutePathFormat = config.getMinutePathSlpt();
                    assetsHourPathFormat = config.getHourPathSlpt();
                    break;
            }
        }
        if (assetsMinutePathFormat == null) {
            assetsMinutePathFormat = "timehand/04/8c/minute/%d.png.color_map";
        }
        String[] assetsMinutePath = new String[16];
        for (i = 0; i < assetsMinutePath.length; i++) {
            assetsMinutePath[i] = String.format(assetsMinutePathFormat, new Object[]{Integer.valueOf(i)});
        }
        if (assetsHourPathFormat == null) {
            assetsHourPathFormat = "timehand/04/8c/hour/%d.png.color_map";
        }
        String[] assetsHourPath = new String[16];
        for (i = 0; i < assetsHourPath.length; i++) {
            assetsHourPath[i] = String.format(assetsHourPathFormat, new Object[]{Integer.valueOf(i)});
        }
        PreDrawedPictureGroup group = new PreDrawedPictureGroup(this, assetsMinutePath);
        super.addDrawedPictureGroup8C(group);
        PreDrawedPictureGroup group1 = new PreDrawedPictureGroup(this, assetsHourPath);
        super.addDrawedPictureGroup8C(group1);
        minuteView.setPreDrawedPicture(group);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(group1);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        byte[] graduationMem = null;
        if (config != null) {
            switch (config.getGraduationType()) {
                case 2:
                    graduationMem = SimpleFile.readFileFromAssets(this, config.getGraduationPathSlpt());
                    break;
            }
        }
        if (graduationMem == null) {
            graduationMem = SimpleFile.readFileFromAssets(this, "graduation/06/8c/graduation.png");
        }
        bgView2.setImagePicture(graduationMem);
        frameLayout.add(bgView);
        frameLayout.add(bgView2);
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        return frameLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
