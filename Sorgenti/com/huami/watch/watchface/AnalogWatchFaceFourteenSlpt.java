package com.huami.watch.watchface;

import android.content.Context;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.util.WatchFaceConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedHourWithMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedSecondView;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.util.ArrayList;

public class AnalogWatchFaceFourteenSlpt extends AbstractSlptClock {
    int f2i = 0;
    private Context mContext;
    private boolean needRefreshSecond;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        this.needRefreshSecond = Util.needSlptRefreshSecond(this.mContext).booleanValue();
        if (this.needRefreshSecond) {
            setClockPeriodSecond(true);
        }
    }

    protected SlptLayout createClockLayout8C() {
        int i;
        byte[] lowBatteryViewMem;
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent hourView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent secondView = new SlptPredrawedSecondView();
        SlptViewComponent lowBatteryView = new SlptPictureView();
        SlptViewComponent bgView2 = new SlptPictureView();
        byte[] bgMem = null;
        WatchFaceConfig config = WatchFaceConfig.getWatchFaceConfig(getApplicationContext(), AnalogWatchFaceFourteen.class.getName());
        if (config != null) {
            switch (config.getBgType()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    bgMem = SimpleFile.readFileFromAssets(this.mContext, config.getBgPathSlpt());
                    break;
                case 4:
                    bgMem = SimpleFile.readFile(config.getBgPathSlpt());
                    break;
            }
        }
        if (bgMem == null) {
            bgMem = SimpleFile.readFileFromAssets(this.mContext, "guard/com.huami.watch.watchface.AnalogWatchFaceFourteen/watchface_default_fourteen_bg.png");
        }
        SlptViewComponent bgView = new SlptPictureView();
        bgView.setImagePicture(bgMem);
        frameLayout.background.color = -16777216;
        super.clearDrawdPictureGroup();
        String assetsMinutePathFormat = null;
        String assetsHourPathFormat = null;
        String assetsSecondPathFormat = null;
        if (config != null) {
            switch (config.getTimeHandType()) {
                case 2:
                    assetsMinutePathFormat = config.getMinutePathSlpt();
                    assetsHourPathFormat = config.getHourPathSlpt();
                    assetsSecondPathFormat = config.getSecondsPathSlpt();
                    break;
            }
        }
        if (assetsMinutePathFormat == null) {
            assetsMinutePathFormat = "timehand/01/8c/minute/%d.png.color_map";
        }
        String[] assetsMinutePath = new String[16];
        for (i = 0; i < assetsMinutePath.length; i++) {
            assetsMinutePath[i] = String.format(assetsMinutePathFormat, new Object[]{Integer.valueOf(i)});
        }
        if (assetsHourPathFormat == null) {
            assetsHourPathFormat = "timehand/01/8c/hour/%d.png.color_map";
        }
        String[] assetsHourPath = new String[16];
        for (i = 0; i < assetsHourPath.length; i++) {
            assetsHourPath[i] = String.format(assetsHourPathFormat, new Object[]{Integer.valueOf(i)});
        }
        PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsMinutePath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsHourPath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        minuteView.setPreDrawedPicture(preDrawedPictureGroup);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(preDrawedPictureGroup);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        if (this.needRefreshSecond) {
            if (assetsSecondPathFormat == null) {
                assetsSecondPathFormat = "timehand/01/8c/seconds/%d.png.color_map";
            }
            String[] assetsSecondPath = new String[16];
            for (i = 0; i < assetsSecondPath.length; i++) {
                assetsSecondPath[i] = String.format(assetsSecondPathFormat, new Object[]{Integer.valueOf(i)});
            }
            preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsSecondPath);
            super.addDrawedPictureGroup8C(preDrawedPictureGroup);
            secondView.setPreDrawedPicture(preDrawedPictureGroup);
            secondView.setStart(0, 0);
            secondView.setRect(320, 320);
        }
        byte[] graduationMem = null;
        if (config != null) {
            switch (config.getGraduationType()) {
                case 2:
                    graduationMem = SimpleFile.readFileFromAssets(this.mContext, config.getGraduationPathSlpt());
                    break;
            }
        }
        if (graduationMem == null) {
            graduationMem = SimpleFile.readFileFromAssets(this.mContext, "graduation/01/8c/graduation.png");
        }
        bgView2.setImagePicture(graduationMem);
        if (Util.needEnAssets()) {
            lowBatteryViewMem = SimpleFile.readFileFromAssets(this.mContext, "guard/xiaohuli/en/watchface_low_power.png");
        } else {
            lowBatteryViewMem = SimpleFile.readFileFromAssets(this.mContext, "guard/xiaohuli/watchface_low_power.png");
        }
        lowBatteryView.setImagePicture(lowBatteryViewMem);
        SlptSportUtil.setLowBatteryIconView(lowBatteryView);
        lowBatteryView.setStart(0, 52);
        lowBatteryView.setRect(320, 24);
        lowBatteryView.show = false;
        lowBatteryView.alignX = (byte) 2;
        frameLayout.add(bgView);
        frameLayout.add(bgView2);
        ArrayList<DataWidgetConfig> mWidgets = null;
        if (config != null) {
            mWidgets = config.getDataWidgets();
        }
        if (mWidgets.size() == 0) {
            mWidgets.add(new DataWidgetConfig(0, 10, 52, 118, 0, null, null));
            mWidgets.add(new DataWidgetConfig(1, 1, 119, 187, 0, null, null));
            mWidgets.add(new DataWidgetConfig(2, 2, 184, 118, 0, null, null));
            mWidgets.add(new DataWidgetConfig(3, 6, 110, 94, 0, null, null));
        } else if (mWidgets.size() < 4) {
            mWidgets.add(new DataWidgetConfig(3, 6, 110, 94, 0, null, null));
        }
        for (int j = 0; j < mWidgets.size(); j++) {
            SlptViewComponent widgetView = get8cDataWidget((DataWidgetConfig) mWidgets.get(j));
            if (widgetView != null) {
                frameLayout.add(widgetView);
            }
        }
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        frameLayout.add(lowBatteryView);
        return frameLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
