package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.analogyellow.R;

public class AnalogWatchFaceNine extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private Bitmap mBgBitmap;
        private Paint mBitmapPaint;
        private Bitmap mHourBitmap;
        private Bitmap mMinBitmap;
        private Bitmap mSecBitmap;

        private Engine() {
            super();
        }

        protected void onPrepareResources(Resources resources) {
            this.mBitmapPaint = new Paint(7);
            this.mBgBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_nine_bg);
            this.mHourBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_nine_hour);
            this.mMinBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_nine_minute);
            this.mSecBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_nine_seconds);
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.save();
            canvas.drawBitmap(this.mBgBitmap, 0.0f, 0.0f, this.mBitmapPaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(hrRot, centerX, centerY);
            canvas.drawBitmap(this.mHourBitmap, centerX - ((float) (this.mHourBitmap.getWidth() / 2)), centerY - ((float) (this.mHourBitmap.getHeight() / 2)), this.mBitmapPaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(minRot, centerX, centerY);
            canvas.drawBitmap(this.mMinBitmap, centerX - ((float) (this.mMinBitmap.getWidth() / 2)), centerY - ((float) (this.mMinBitmap.getHeight() / 2)), this.mBitmapPaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(secRot, centerX, centerY);
            canvas.drawBitmap(this.mSecBitmap, centerX - ((float) (this.mSecBitmap.getWidth() / 2)), centerY - ((float) (this.mSecBitmap.getHeight() / 2)), this.mBitmapPaint);
            canvas.restore();
        }

        public void onDestroy() {
            if (!(this.mBgBitmap == null || this.mBgBitmap.isRecycled())) {
                this.mBgBitmap.recycle();
                this.mBgBitmap = null;
            }
            if (!(this.mHourBitmap == null || this.mHourBitmap.isRecycled())) {
                this.mHourBitmap.recycle();
                this.mHourBitmap = null;
            }
            if (!(this.mMinBitmap == null || this.mMinBitmap.isRecycled())) {
                this.mMinBitmap.recycle();
                this.mMinBitmap = null;
            }
            if (!(this.mSecBitmap == null || this.mSecBitmap.isRecycled())) {
                this.mSecBitmap.recycle();
                this.mSecBitmap = null;
            }
            super.onDestroy();
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return AnalogWatchFaceNineSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(42.3f);
        return new Engine();
    }
}
