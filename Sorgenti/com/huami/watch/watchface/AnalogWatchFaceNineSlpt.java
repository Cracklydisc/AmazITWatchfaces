package com.huami.watch.watchface;

import android.content.Context;
import android.util.Log;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogHourView;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogSecondView;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class AnalogWatchFaceNineSlpt extends AbstractSlptClock {
    int f3i = 0;
    private Context mContext;
    private boolean needRefreshSecond;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        this.needRefreshSecond = Util.needSlptRefreshSecond(this.mContext).booleanValue();
        if (this.needRefreshSecond) {
            setClockPeriodSecond(true);
        }
    }

    protected SlptLayout createClockLayout8C() {
        byte[] lowBatteryViewMem;
        Log.i("jingdian", "createClockLayout8C: ");
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptPredrawedMinuteView minuteView = new SlptPredrawedMinuteView();
        SlptAnalogHourView hourView = new SlptAnalogHourView();
        SlptPictureView lowBatteryView = new SlptPictureView();
        SlptAnalogSecondView secondView = new SlptAnalogSecondView();
        SlptPictureView bgView = new SlptPictureView();
        String[] assetsMinutePath = new String[]{"guard/jingdian/minute_00.png.color_map", "guard/jingdian/minute_01.png.color_map", "guard/jingdian/minute_02.png.color_map", "guard/jingdian/minute_03.png.color_map", "guard/jingdian/minute_04.png.color_map", "guard/jingdian/minute_05.png.color_map", "guard/jingdian/minute_06.png.color_map", "guard/jingdian/minute_07.png.color_map", "guard/jingdian/minute_08.png.color_map", "guard/jingdian/minute_09.png.color_map", "guard/jingdian/minute_10.png.color_map", "guard/jingdian/minute_11.png.color_map", "guard/jingdian/minute_12.png.color_map", "guard/jingdian/minute_13.png.color_map", "guard/jingdian/minute_14.png.color_map", "guard/jingdian/minute_15.png.color_map"};
        bgView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jingdian/watchface_default_bg.png"));
        hourView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jingdian/watchface_default_hour.png"));
        secondView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/jingdian/watchface_default_second.png"));
        frameLayout.background.color = -16777216;
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup group = new PreDrawedPictureGroup(this, assetsMinutePath);
        super.addDrawedPictureGroup8C(group);
        minuteView.setPreDrawedPicture(group);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        if (Util.needEnAssets()) {
            lowBatteryViewMem = SimpleFile.readFileFromAssets(this, "guard/jingdian/en/watchface_low_power.png");
        } else {
            lowBatteryViewMem = SimpleFile.readFileFromAssets(this, "guard/jingdian/watchface_low_power.png");
        }
        lowBatteryView.setImagePicture(lowBatteryViewMem);
        lowBatteryView.id = (short) 17;
        lowBatteryView.setStart(0, 78);
        lowBatteryView.setRect(320, 24);
        lowBatteryView.show = false;
        lowBatteryView.alignX = (byte) 2;
        frameLayout.add(bgView);
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        frameLayout.add(lowBatteryView);
        return frameLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
