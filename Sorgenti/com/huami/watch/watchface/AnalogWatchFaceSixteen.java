package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.widget.EverestDateWidget;
import com.huami.watch.watchface.widget.PointerBatteryWidget;
import com.huami.watch.watchface.widget.PointerTodayDistanceWidget;
import com.huami.watch.watchface.widget.PointerTodayStepWidget;

public class AnalogWatchFaceSixteen extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private Drawable mBgDrawable;
        private Path mClip;
        private Drawable mHour;
        private Drawable mMinute;
        private Drawable mSeconds;

        private Engine() {
            super();
            this.mClip = new Path();
        }

        protected void onPrepareResources(Resources resources) {
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.mBgDrawable = resources.getDrawable(R.drawable.watchface_bg_analog_sixteen_0, null);
            this.mBgDrawable.setBounds(0, 0, 320, 320);
            this.mHour = Util.decodeBitmapDrawableFromAssets(resources, "timehand/05/hour.png");
            this.mMinute = Util.decodeBitmapDrawableFromAssets(resources, "timehand/05/minute.png");
            this.mSeconds = Util.decodeBitmapDrawableFromAssets(resources, "timehand/05/seconds.png");
            if (this.mHour != null) {
                int left = 160 - (this.mHour.getIntrinsicWidth() / 2);
                int top = 160 - (this.mHour.getIntrinsicHeight() / 2);
                this.mHour.setBounds(left, top, this.mHour.getIntrinsicWidth() + left, this.mHour.getIntrinsicHeight() + top);
            }
            if (this.mMinute != null) {
                left = 160 - (this.mMinute.getIntrinsicWidth() / 2);
                top = 160 - (this.mMinute.getIntrinsicHeight() / 2);
                this.mMinute.setBounds(left, top, this.mMinute.getIntrinsicWidth() + left, this.mMinute.getIntrinsicHeight() + top);
            }
            if (this.mSeconds != null) {
                left = 160 - (this.mSeconds.getIntrinsicWidth() / 2);
                top = 160 - (this.mSeconds.getIntrinsicHeight() / 2);
                this.mSeconds.setBounds(left, top, this.mSeconds.getIntrinsicWidth() + left, this.mSeconds.getIntrinsicHeight() + top);
            }
            Drawable pointer = resources.getDrawable(R.drawable.pointer_small, null);
            PointerTodayDistanceWidget distanceWidget = new PointerTodayDistanceWidget(resources, 106, 25, 108, 108);
            distanceWidget.setPointerDrawable(pointer);
            addWidget(distanceWidget);
            PointerBatteryWidget batteryWidget = new PointerBatteryWidget(resources, 11, 107, 108, 108);
            batteryWidget.setPointerDrawable(pointer);
            addWidget(batteryWidget);
            PointerTodayStepWidget stepWidget = new PointerTodayStepWidget(resources, 106, 186, 108, 108);
            stepWidget.setPointerDrawable(pointer);
            addWidget(stepWidget);
            addWidget(new EverestDateWidget(resources, 214, 156));
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.clipPath(this.mClip);
            if (this.mBgDrawable != null) {
                this.mBgDrawable.draw(canvas);
            }
            onDrawWidgets(canvas);
            if (this.mHour != null) {
                canvas.save();
                canvas.rotate(hrRot, centerX, centerY);
                this.mHour.draw(canvas);
                canvas.restore();
            }
            if (this.mMinute != null) {
                canvas.save();
                canvas.rotate(minRot, centerX, centerY);
                this.mMinute.draw(canvas);
                canvas.restore();
            }
            if (this.mSeconds != null) {
                canvas.save();
                canvas.rotate(secRot, centerX, centerY);
                this.mSeconds.draw(canvas);
                canvas.restore();
            }
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return AnalogWatchFaceSixteenSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(170.0f, 79.0f);
        return new Engine();
    }
}
