package com.huami.watch.watchface;

import android.content.Context;
import android.util.Log;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.slpt.slptFonts;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.arc.SlptPowerArcAnglePicView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayDistanceArcAnglePicView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedHourWithMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedPower;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedSecondView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedTodayDistance;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedTodayStep;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import org.json.JSONException;
import org.json.JSONObject;

public class AnalogWatchFaceSixteenSlpt extends AbstractSlptClock {
    private SlptViewComponent lowBatteryView;
    private Context mContext;
    private boolean needRefreshSecond;

    private void getStepTips(SlptLinearLayout[] stepTips, slptFonts fonts) {
        int step = 8000;
        String str = WatchSettings.get(getBaseContext().getContentResolver(), "huami.watch.health.config");
        if (str != null) {
            try {
                step = new JSONObject(str).optInt("step_target", 8000);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.d("everest", "get tartget step " + step + "str " + str);
        int delta = step / 4;
        for (int i = 0; i < 3; i++) {
            int k = delta * (i + 1);
            if (k < 1000) {
                stepTips[i] = (SlptLinearLayout) fonts.getStringView(String.valueOf(k));
            } else if (k % 1000 == 0) {
                stepTips[i] = (SlptLinearLayout) fonts.getStringView((k / 1000) + "k");
            } else {
                stepTips[i] = (SlptLinearLayout) fonts.getStringView((((float) ((k + 50) / 100)) / 10.0f) + "k");
            }
        }
    }

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        this.needRefreshSecond = Util.needSlptRefreshSecond(this.mContext).booleanValue();
        if (this.needRefreshSecond) {
            setClockPeriodSecond(true);
        }
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this, -65536, 0);
        this.lowBatteryView.setStart(208, 119);
    }

    protected SlptLayout createClockLayout8C() {
        int i;
        String str;
        Log.i("everest", "createClockLayout8C: ");
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent hourView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent secondView = new SlptPredrawedSecondView();
        SlptViewComponent predrawedTodayDistance = new SlptPredrawedTodayDistance();
        SlptViewComponent predrawedTodayStep = new SlptPredrawedTodayStep();
        SlptViewComponent predrawedPower = new SlptPredrawedPower();
        SlptViewComponent stepArcAnglePicView = new SlptTodayStepArcAnglePicView();
        SlptTodayDistanceArcAnglePicView distanceArcAnglePicView = new SlptTodayDistanceArcAnglePicView();
        SlptViewComponent powerArcAnglePicView = new SlptPowerArcAnglePicView();
        SlptPictureView bgView = new SlptPictureView();
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptLinearLayout[] stepTips = new SlptLinearLayout[3];
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        String[] assetsMinutePath = new String[]{"guard/everest/minute/00.png.color_map", "guard/everest/minute/01.png.color_map", "guard/everest/minute/02.png.color_map", "guard/everest/minute/03.png.color_map", "guard/everest/minute/04.png.color_map", "guard/everest/minute/05.png.color_map", "guard/everest/minute/06.png.color_map", "guard/everest/minute/07.png.color_map", "guard/everest/minute/08.png.color_map", "guard/everest/minute/09.png.color_map", "guard/everest/minute/10.png.color_map", "guard/everest/minute/11.png.color_map", "guard/everest/minute/12.png.color_map", "guard/everest/minute/13.png.color_map", "guard/everest/minute/14.png.color_map", "guard/everest/minute/15.png.color_map"};
        String[] assetsHourPath = new String[]{"guard/everest/hour/00.png.color_map", "guard/everest/hour/01.png.color_map", "guard/everest/hour/02.png.color_map", "guard/everest/hour/03.png.color_map", "guard/everest/hour/04.png.color_map", "guard/everest/hour/05.png.color_map", "guard/everest/hour/06.png.color_map", "guard/everest/hour/07.png.color_map", "guard/everest/hour/08.png.color_map", "guard/everest/hour/09.png.color_map", "guard/everest/hour/10.png.color_map", "guard/everest/hour/11.png.color_map", "guard/everest/hour/12.png.color_map", "guard/everest/hour/13.png.color_map", "guard/everest/hour/14.png.color_map", "guard/everest/hour/15.png.color_map"};
        String[] assetsSecondPath = new String[]{"guard/everest/seconds/00.png.color_map", "guard/everest/seconds/01.png.color_map", "guard/everest/seconds/02.png.color_map", "guard/everest/seconds/03.png.color_map", "guard/everest/seconds/04.png.color_map", "guard/everest/seconds/05.png.color_map", "guard/everest/seconds/06.png.color_map", "guard/everest/seconds/07.png.color_map", "guard/everest/seconds/08.png.color_map", "guard/everest/seconds/09.png.color_map", "guard/everest/seconds/10.png.color_map", "guard/everest/seconds/11.png.color_map", "guard/everest/seconds/12.png.color_map", "guard/everest/seconds/13.png.color_map", "guard/everest/seconds/14.png.color_map", "guard/everest/seconds/15.png.color_map"};
        String[] assetsSmallArrawPath = new String[]{"guard/everest/small/00.png.color_map", "guard/everest/small/01.png.color_map", "guard/everest/small/02.png.color_map", "guard/everest/small/03.png.color_map", "guard/everest/small/04.png.color_map", "guard/everest/small/05.png.color_map", "guard/everest/small/06.png.color_map", "guard/everest/small/07.png.color_map", "guard/everest/small/08.png.color_map", "guard/everest/small/09.png.color_map", "guard/everest/small/10.png.color_map", "guard/everest/small/11.png.color_map", "guard/everest/small/12.png.color_map", "guard/everest/small/13.png.color_map", "guard/everest/small/14.png.color_map", "guard/everest/small/15.png.color_map"};
        slptFonts fonts = new slptFonts();
        byte[][] date_num = new byte[10][];
        byte[][] small_num = new byte[10][];
        byte[][] week_num = new byte[7][];
        for (i = 0; i < 10; i++) {
            date_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("datawidget/date_everest/%d_8c.png", new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            str = new String("datawidget/date_everest/w%d_8c.png");
        } else {
            str = new String("datawidget/date_everest/w%d_8c.png");
        }
        for (i = 0; i < 7; i++) {
            week_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            str = new String("datawidget/step_everest/%d.png");
        } else {
            str = new String("datawidget/step_everest/%d.png");
        }
        for (i = 0; i < 10; i++) {
            small_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        fonts.setNumFonts(small_num);
        fonts.addChar('k', SimpleFile.readFileFromAssets(this.mContext, "datawidget/step_everest/k.png"));
        fonts.addChar('.', SimpleFile.readFileFromAssets(this.mContext, "datawidget/step_everest/point.png"));
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup group = new PreDrawedPictureGroup(this.mContext, assetsMinutePath);
        super.addDrawedPictureGroup8C(group);
        PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsHourPath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        if (this.needRefreshSecond) {
            preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsSecondPath);
            super.addDrawedPictureGroup8C(preDrawedPictureGroup);
            secondView.setPreDrawedPicture(preDrawedPictureGroup);
            secondView.setStart(10, 10);
            secondView.setRect(320, 320);
        }
        preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsSmallArrawPath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        minuteView.setPreDrawedPicture(group);
        minuteView.setStart(10, 10);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(preDrawedPictureGroup);
        hourView.setStart(10, 10);
        hourView.setRect(320, 320);
        predrawedTodayDistance.setPreDrawedPicture(preDrawedPictureGroup);
        predrawedTodayDistance.setStart(106, 25);
        predrawedTodayDistance.setRect(108, 108);
        predrawedTodayStep.setPreDrawedPicture(preDrawedPictureGroup);
        predrawedTodayStep.setStart(106, 186);
        predrawedTodayStep.setRect(108, 108);
        predrawedPower.setPreDrawedPicture(preDrawedPictureGroup);
        predrawedPower.setStart(11, 107);
        predrawedPower.setRect(108, 108);
        bgView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/everest/watchface_default_bg_8c.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "datawidget/date_everest/point_8c.png"));
        monthLayout.setImagePictureArrayForAll(date_num);
        weekView.setImagePictureArray(week_num);
        monthLayout.setStart(212, 156);
        weekView.setStart(256, 155);
        byte[] smallArraw = SimpleFile.readFileFromAssets(this.mContext, "guard/everest/watchface_rate_bg_8c.png");
        stepArcAnglePicView.setImagePicture(smallArraw);
        distanceArcAnglePicView.setImagePicture(smallArraw);
        powerArcAnglePicView.setImagePicture(smallArraw);
        stepArcAnglePicView.setRect(108, 108);
        stepArcAnglePicView.setStart(106, 186);
        stepArcAnglePicView.start_angle = 0;
        stepArcAnglePicView.len_angle = 360;
        stepArcAnglePicView.full_angle = 360;
        stepArcAnglePicView.draw_clockwise = 1;
        distanceArcAnglePicView.setRect(108, 108);
        distanceArcAnglePicView.setStart(106, 25);
        distanceArcAnglePicView.start_angle = 0;
        distanceArcAnglePicView.len_angle = 360;
        distanceArcAnglePicView.full_angle = 360;
        distanceArcAnglePicView.draw_clockwise = 1;
        powerArcAnglePicView.setRect(108, 108);
        powerArcAnglePicView.setStart(11, 107);
        powerArcAnglePicView.start_angle = 0;
        powerArcAnglePicView.len_angle = 360;
        powerArcAnglePicView.full_angle = 360;
        powerArcAnglePicView.alignParentX = (byte) 2;
        powerArcAnglePicView.draw_clockwise = 1;
        getStepTips(stepTips, fonts);
        stepTips[0].setStart(178, 236);
        stepTips[0].setRect(22, 7);
        stepTips[0].alignX = (byte) 1;
        stepTips[1].setStart(149, 273);
        stepTips[1].setRect(22, 7);
        stepTips[1].alignX = (byte) 2;
        stepTips[2].setStart(120, 236);
        stepTips[2].setRect(22, 7);
        stepTips[2].alignX = (byte) 0;
        frameLayout.add(bgView);
        frameLayout.add(stepTips[0]);
        frameLayout.add(stepTips[1]);
        frameLayout.add(stepTips[2]);
        frameLayout.add(predrawedTodayDistance);
        frameLayout.add(predrawedTodayStep);
        frameLayout.add(predrawedPower);
        frameLayout.add(stepArcAnglePicView);
        frameLayout.add(distanceArcAnglePicView);
        frameLayout.add(powerArcAnglePicView);
        frameLayout.add(monthLayout);
        frameLayout.add(weekView);
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        frameLayout.add(this.lowBatteryView);
        return frameLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        int i;
        String str;
        Log.i("everest", "createClockLayout8C: ");
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent hourView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent secondView = new SlptPredrawedSecondView();
        SlptViewComponent predrawedTodayDistance = new SlptPredrawedTodayDistance();
        SlptViewComponent predrawedTodayStep = new SlptPredrawedTodayStep();
        SlptViewComponent predrawedPower = new SlptPredrawedPower();
        SlptViewComponent stepArcAnglePicView = new SlptTodayStepArcAnglePicView();
        SlptTodayDistanceArcAnglePicView distanceArcAnglePicView = new SlptTodayDistanceArcAnglePicView();
        SlptViewComponent powerArcAnglePicView = new SlptPowerArcAnglePicView();
        SlptPictureView bgView = new SlptPictureView();
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptLinearLayout[] stepTips = new SlptLinearLayout[3];
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        String[] assetsMinutePath = new String[]{"guard/everest/26WC/minute/00.png.color_map", "guard/everest/26WC/minute/01.png.color_map", "guard/everest/26WC/minute/02.png.color_map", "guard/everest/26WC/minute/03.png.color_map", "guard/everest/26WC/minute/04.png.color_map", "guard/everest/26WC/minute/05.png.color_map", "guard/everest/26WC/minute/06.png.color_map", "guard/everest/26WC/minute/07.png.color_map", "guard/everest/26WC/minute/08.png.color_map", "guard/everest/26WC/minute/09.png.color_map", "guard/everest/26WC/minute/10.png.color_map", "guard/everest/26WC/minute/11.png.color_map", "guard/everest/26WC/minute/12.png.color_map", "guard/everest/26WC/minute/13.png.color_map", "guard/everest/26WC/minute/14.png.color_map", "guard/everest/26WC/minute/15.png.color_map"};
        String[] assetsHourPath = new String[]{"guard/everest/26WC/hour/00.png.color_map", "guard/everest/26WC/hour/01.png.color_map", "guard/everest/26WC/hour/02.png.color_map", "guard/everest/26WC/hour/03.png.color_map", "guard/everest/26WC/hour/04.png.color_map", "guard/everest/26WC/hour/05.png.color_map", "guard/everest/26WC/hour/06.png.color_map", "guard/everest/26WC/hour/07.png.color_map", "guard/everest/26WC/hour/08.png.color_map", "guard/everest/26WC/hour/09.png.color_map", "guard/everest/26WC/hour/10.png.color_map", "guard/everest/26WC/hour/11.png.color_map", "guard/everest/26WC/hour/12.png.color_map", "guard/everest/26WC/hour/13.png.color_map", "guard/everest/26WC/hour/14.png.color_map", "guard/everest/26WC/hour/15.png.color_map"};
        String[] assetsSecondPath = new String[]{"guard/everest/26WC/seconds/00.png.color_map", "guard/everest/26WC/seconds/01.png.color_map", "guard/everest/26WC/seconds/02.png.color_map", "guard/everest/26WC/seconds/03.png.color_map", "guard/everest/26WC/seconds/04.png.color_map", "guard/everest/26WC/seconds/05.png.color_map", "guard/everest/26WC/seconds/06.png.color_map", "guard/everest/26WC/seconds/07.png.color_map", "guard/everest/26WC/seconds/08.png.color_map", "guard/everest/26WC/seconds/09.png.color_map", "guard/everest/26WC/seconds/10.png.color_map", "guard/everest/26WC/seconds/11.png.color_map", "guard/everest/26WC/seconds/12.png.color_map", "guard/everest/26WC/seconds/13.png.color_map", "guard/everest/26WC/seconds/14.png.color_map", "guard/everest/26WC/seconds/15.png.color_map"};
        String[] assetsSmallArrawPath = new String[]{"guard/everest/26WC/small/00.png.color_map", "guard/everest/26WC/small/01.png.color_map", "guard/everest/26WC/small/02.png.color_map", "guard/everest/26WC/small/03.png.color_map", "guard/everest/26WC/small/04.png.color_map", "guard/everest/26WC/small/05.png.color_map", "guard/everest/26WC/small/06.png.color_map", "guard/everest/26WC/small/07.png.color_map", "guard/everest/26WC/small/08.png.color_map", "guard/everest/26WC/small/09.png.color_map", "guard/everest/26WC/small/10.png.color_map", "guard/everest/26WC/small/11.png.color_map", "guard/everest/26WC/small/12.png.color_map", "guard/everest/26WC/small/13.png.color_map", "guard/everest/26WC/small/14.png.color_map", "guard/everest/26WC/small/15.png.color_map"};
        slptFonts fonts = new slptFonts();
        byte[][] date_num = new byte[10][];
        byte[][] small_num = new byte[10][];
        byte[][] week_num = new byte[7][];
        for (i = 0; i < 10; i++) {
            date_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("datawidget/date_everest/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            str = new String("datawidget/date_everest/w%d.png");
        } else {
            str = new String("datawidget/date_everest/w%d.png");
        }
        for (i = 0; i < 7; i++) {
            week_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            str = new String("datawidget/step_everest/%d.png");
        } else {
            str = new String("datawidget/step_everest/%d.png");
        }
        for (i = 0; i < 10; i++) {
            small_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        fonts.setNumFonts(small_num);
        fonts.addChar('k', SimpleFile.readFileFromAssets(this.mContext, "datawidget/step_everest/k.png"));
        fonts.addChar('.', SimpleFile.readFileFromAssets(this.mContext, "datawidget/step_everest/point.png"));
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup group = new PreDrawedPictureGroup(this.mContext, assetsMinutePath);
        super.addDrawedPictureGroup26WC(group);
        PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsHourPath);
        super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
        if (this.needRefreshSecond) {
            preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsSecondPath);
            super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
            secondView.setPreDrawedPicture(preDrawedPictureGroup);
            secondView.setStart(0, 0);
            secondView.setRect(320, 320);
        }
        preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsSmallArrawPath);
        super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
        minuteView.setPreDrawedPicture(group);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(preDrawedPictureGroup);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        predrawedTodayDistance.setPreDrawedPicture(preDrawedPictureGroup);
        predrawedTodayDistance.setStart(106, 25);
        predrawedTodayDistance.setRect(108, 108);
        predrawedTodayStep.setPreDrawedPicture(preDrawedPictureGroup);
        predrawedTodayStep.setStart(106, 186);
        predrawedTodayStep.setRect(108, 108);
        predrawedPower.setPreDrawedPicture(preDrawedPictureGroup);
        predrawedPower.setStart(11, 107);
        predrawedPower.setRect(108, 108);
        bgView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/everest/26WC/watchface_default_bg.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "datawidget/date_everest/point_8c.png"));
        monthLayout.setImagePictureArrayForAll(date_num);
        weekView.setImagePictureArray(week_num);
        monthLayout.setStart(212, 156);
        weekView.setStart(256, 155);
        byte[] smallArraw = SimpleFile.readFileFromAssets(this.mContext, "guard/everest/watchface_rate_bg_8c.png");
        stepArcAnglePicView.setImagePicture(smallArraw);
        distanceArcAnglePicView.setImagePicture(smallArraw);
        powerArcAnglePicView.setImagePicture(smallArraw);
        stepArcAnglePicView.setRect(108, 108);
        stepArcAnglePicView.setStart(106, 186);
        stepArcAnglePicView.start_angle = 0;
        stepArcAnglePicView.len_angle = 360;
        stepArcAnglePicView.full_angle = 360;
        stepArcAnglePicView.draw_clockwise = 1;
        distanceArcAnglePicView.setRect(108, 108);
        distanceArcAnglePicView.setStart(106, 25);
        distanceArcAnglePicView.start_angle = 0;
        distanceArcAnglePicView.len_angle = 360;
        distanceArcAnglePicView.full_angle = 360;
        distanceArcAnglePicView.draw_clockwise = 1;
        powerArcAnglePicView.setRect(108, 108);
        powerArcAnglePicView.setStart(11, 107);
        powerArcAnglePicView.start_angle = 0;
        powerArcAnglePicView.len_angle = 360;
        powerArcAnglePicView.full_angle = 360;
        powerArcAnglePicView.alignParentX = (byte) 2;
        powerArcAnglePicView.draw_clockwise = 1;
        getStepTips(stepTips, fonts);
        stepTips[0].setStart(178, 236);
        stepTips[0].setRect(22, 7);
        stepTips[0].alignX = (byte) 1;
        stepTips[1].setStart(149, 273);
        stepTips[1].setRect(22, 7);
        stepTips[1].alignX = (byte) 2;
        stepTips[2].setStart(120, 236);
        stepTips[2].setRect(22, 7);
        stepTips[2].alignX = (byte) 0;
        frameLayout.add(bgView);
        frameLayout.add(stepTips[0]);
        frameLayout.add(stepTips[1]);
        frameLayout.add(stepTips[2]);
        frameLayout.add(predrawedTodayDistance);
        frameLayout.add(predrawedTodayStep);
        frameLayout.add(predrawedPower);
        frameLayout.add(stepArcAnglePicView);
        frameLayout.add(distanceArcAnglePicView);
        frameLayout.add(powerArcAnglePicView);
        frameLayout.add(monthLayout);
        frameLayout.add(weekView);
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        frameLayout.add(this.lowBatteryView);
        return frameLayout;
    }
}
