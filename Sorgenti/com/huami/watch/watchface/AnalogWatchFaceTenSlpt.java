package com.huami.watch.watchface;

import android.content.Context;
import android.util.Log;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedHourWithMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class AnalogWatchFaceTenSlpt extends AbstractSlptClock {
    int f4i = 0;
    private Context mContext;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
    }

    protected SlptLayout createClockLayout8C() {
        byte[] lowBatteryViewMem;
        Log.i("xiaohuli", "createClockLayout8C: ");
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptPredrawedMinuteView minuteView = new SlptPredrawedMinuteView();
        SlptPredrawedHourWithMinuteView hourView = new SlptPredrawedHourWithMinuteView();
        SlptPictureView lowBatteryView = new SlptPictureView();
        SlptPictureView bgView = new SlptPictureView();
        String[] assetsMinutePath = new String[]{"guard/xiaohuli/minute_00.png.color_map", "guard/xiaohuli/minute_01.png.color_map", "guard/xiaohuli/minute_02.png.color_map", "guard/xiaohuli/minute_03.png.color_map", "guard/xiaohuli/minute_04.png.color_map", "guard/xiaohuli/minute_05.png.color_map", "guard/xiaohuli/minute_06.png.color_map", "guard/xiaohuli/minute_07.png.color_map", "guard/xiaohuli/minute_08.png.color_map", "guard/xiaohuli/minute_09.png.color_map", "guard/xiaohuli/minute_10.png.color_map", "guard/xiaohuli/minute_11.png.color_map", "guard/xiaohuli/minute_12.png.color_map", "guard/xiaohuli/minute_13.png.color_map", "guard/xiaohuli/minute_14.png.color_map", "guard/xiaohuli/minute_15.png.color_map"};
        String[] assetsHourPath = new String[]{"guard/xiaohuli/hour_00.png.color_map", "guard/xiaohuli/hour_01.png.color_map", "guard/xiaohuli/hour_02.png.color_map", "guard/xiaohuli/hour_03.png.color_map", "guard/xiaohuli/hour_04.png.color_map", "guard/xiaohuli/hour_05.png.color_map", "guard/xiaohuli/hour_06.png.color_map", "guard/xiaohuli/hour_07.png.color_map", "guard/xiaohuli/hour_08.png.color_map", "guard/xiaohuli/hour_09.png.color_map", "guard/xiaohuli/hour_10.png.color_map", "guard/xiaohuli/hour_11.png.color_map", "guard/xiaohuli/hour_12.png.color_map", "guard/xiaohuli/hour_13.png.color_map", "guard/xiaohuli/hour_14.png.color_map", "guard/xiaohuli/hour_15.png.color_map"};
        bgView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/xiaohuli/watchface_default_bg.png"));
        frameLayout.background.color = -16777216;
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup group = new PreDrawedPictureGroup(this.mContext, assetsMinutePath);
        super.addDrawedPictureGroup8C(group);
        PreDrawedPictureGroup group1 = new PreDrawedPictureGroup(this.mContext, assetsHourPath);
        super.addDrawedPictureGroup8C(group1);
        minuteView.setPreDrawedPicture(group);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(group1);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        if (Util.needEnAssets()) {
            lowBatteryViewMem = SimpleFile.readFileFromAssets(this.mContext, "guard/xiaohuli/en/watchface_low_power.png");
        } else {
            lowBatteryViewMem = SimpleFile.readFileFromAssets(this.mContext, "guard/xiaohuli/watchface_low_power.png");
        }
        lowBatteryView.setImagePicture(lowBatteryViewMem);
        lowBatteryView.id = (short) 17;
        lowBatteryView.setStart(0, 78);
        lowBatteryView.setRect(320, 24);
        lowBatteryView.show = false;
        lowBatteryView.alignX = (byte) 2;
        frameLayout.add(bgView);
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        frameLayout.add(lowBatteryView);
        return frameLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
