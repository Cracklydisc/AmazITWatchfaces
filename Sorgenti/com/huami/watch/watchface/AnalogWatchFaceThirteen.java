package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.WatchFaceConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.huami.watch.watchface.widget.AbsWatchFaceDataWidget;
import java.util.ArrayList;

public class AnalogWatchFaceThirteen extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private WatchFaceConfig config;
        private boolean drawBgFog;
        private Drawable mBgDrawable;
        private Paint mBitmapPaint;
        private Path mClip;
        private Bitmap mGraduation;
        private Bitmap mHourBitmap;
        private Bitmap mMinBitmap;
        private Bitmap mSecBitmap;

        private Engine() {
            super();
            this.mClip = new Path();
            this.drawBgFog = false;
        }

        protected void onPrepareResources(Resources resources) {
            this.mBitmapPaint = new Paint(7);
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.config = WatchFaceConfig.getWatchFaceConfig(AnalogWatchFaceThirteen.this.getApplicationContext(), AnalogWatchFaceThirteen.class.getName());
            if (this.config != null) {
                switch (this.config.getBgType()) {
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        this.mBgDrawable = this.config.getBgDrawable();
                        break;
                    case 4:
                        this.mBgDrawable = this.config.getBgDrawable();
                        this.drawBgFog = true;
                        break;
                }
            }
            if (this.mBgDrawable == null) {
                this.mBgDrawable = resources.getDrawable(R.drawable.watchface_bg_analog_thirteen_0);
            }
            this.mBgDrawable.setBounds(0, 0, 320, 320);
            this.mGraduation = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_thirteen_graduation);
            this.mHourBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_thirteen_hour);
            this.mMinBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_thirteen_minute);
            this.mSecBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_thirteen_seconds);
            ArrayList<DataWidgetConfig> mWidgets = null;
            if (this.config != null) {
                mWidgets = this.config.getDataWidgets();
            }
            if (mWidgets.size() < 1) {
                mWidgets.add(new DataWidgetConfig(3, 6, 110, 94, 0, null, null));
            }
            for (int j = 0; j < mWidgets.size(); j++) {
                AbsWatchFaceDataWidget widget = getDataWidget(resources, (DataWidgetConfig) mWidgets.get(j));
                if (widget != null) {
                    addWidget(widget);
                }
            }
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.clipPath(this.mClip);
            if (this.mBgDrawable != null) {
                this.mBgDrawable.draw(canvas);
            }
            canvas.drawBitmap(this.mGraduation, 0.0f, 0.0f, this.mBitmapPaint);
            super.onDrawAnalog(canvas, width, height, centerX, centerY, secRot, minRot, hrRot);
            canvas.save();
            canvas.rotate(hrRot, centerX, centerY);
            canvas.drawBitmap(this.mHourBitmap, centerX - ((float) (this.mHourBitmap.getWidth() / 2)), centerY - ((float) (this.mHourBitmap.getHeight() / 2)), this.mBitmapPaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(minRot, centerX, centerY);
            canvas.drawBitmap(this.mMinBitmap, centerX - ((float) (this.mMinBitmap.getWidth() / 2)), centerY - ((float) (this.mMinBitmap.getHeight() / 2)), this.mBitmapPaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(secRot, centerX, centerY);
            canvas.drawBitmap(this.mSecBitmap, centerX - ((float) (this.mSecBitmap.getWidth() / 2)), centerY - ((float) (this.mSecBitmap.getHeight() / 2)), this.mBitmapPaint);
            canvas.restore();
        }

        public void onDestroy() {
            if (!(this.mHourBitmap == null || this.mHourBitmap.isRecycled())) {
                this.mHourBitmap.recycle();
                this.mHourBitmap = null;
            }
            if (!(this.mMinBitmap == null || this.mMinBitmap.isRecycled())) {
                this.mMinBitmap.recycle();
                this.mMinBitmap = null;
            }
            if (!(this.mSecBitmap == null || this.mSecBitmap.isRecycled())) {
                this.mSecBitmap.recycle();
                this.mSecBitmap = null;
            }
            super.onDestroy();
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return AnalogWatchFaceThirteenSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(35.0f);
        return new Engine();
    }
}
