package com.huami.watch.watchface;

import android.content.Context;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.util.WatchFaceConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedHourWithMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedSecondView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.util.ArrayList;

public class AnalogWatchFaceThirteenSlpt extends AbstractSlptClock {
    int f5i = 0;
    private Context mContext;
    private boolean needRefreshSecond;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        this.needRefreshSecond = Util.needSlptRefreshSecond(this.mContext).booleanValue();
        if (this.needRefreshSecond) {
            setClockPeriodSecond(true);
        }
    }

    protected SlptLayout createClockLayout8C() {
        Log.i("2017NewYear", "createClockLayout8C: ");
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent hourView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent secondView = new SlptPredrawedSecondView();
        SlptPictureView lowBatteryView = new SlptPictureView();
        SlptPictureView bgView2 = new SlptPictureView();
        String[] assetsMinutePath = new String[]{"guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/0.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/1.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/2.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/3.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/4.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/5.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/6.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/7.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/8.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/9.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/10.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/11.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/12.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/13.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/14.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/minute/15.png.color_map"};
        String[] assetsHourPath = new String[]{"guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/0.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/1.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/2.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/3.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/4.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/5.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/6.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/7.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/8.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/9.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/10.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/11.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/12.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/13.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/14.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/hour/15.png.color_map"};
        String[] assetsSecondPath = new String[]{"guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/0.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/1.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/2.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/3.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/4.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/5.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/6.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/7.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/8.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/9.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/10.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/11.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/12.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/13.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/14.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/8c/second/15.png.color_map"};
        byte[] bgMem = null;
        WatchFaceConfig config = WatchFaceConfig.getWatchFaceConfig(getApplicationContext(), AnalogWatchFaceThirteen.class.getName());
        if (config != null) {
            switch (config.getBgType()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    bgMem = SimpleFile.readFileFromAssets(this, config.getBgPathSlpt());
                    break;
                case 4:
                    bgMem = SimpleFile.readFile(config.getBgPathSlpt());
                    break;
            }
        }
        if (bgMem == null) {
            bgMem = SimpleFile.readFileFromAssets(this, "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/watchface_bg_analog_thirteen_0.png");
        }
        SlptPictureView bgView = new SlptPictureView();
        bgView.setImagePicture(bgMem);
        frameLayout.background.color = -16777216;
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsMinutePath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsHourPath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsSecondPath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        minuteView.setPreDrawedPicture(preDrawedPictureGroup);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(preDrawedPictureGroup);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        secondView.setPreDrawedPicture(preDrawedPictureGroup);
        secondView.setStart(0, 0);
        secondView.setRect(320, 320);
        bgView2.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/watchface_default_graduation.png"));
        if (bgView != null) {
            frameLayout.add(bgView);
        }
        frameLayout.add(bgView2);
        ArrayList<DataWidgetConfig> mWidgets = null;
        if (config != null) {
            mWidgets = config.getDataWidgets();
        }
        if (mWidgets.size() < 1) {
            mWidgets.add(new DataWidgetConfig(3, 6, 110, 94, 0, null, null));
        }
        for (int j = 0; j < mWidgets.size(); j++) {
            SlptViewComponent widgetView = get8cDataWidget((DataWidgetConfig) mWidgets.get(j));
            if (widgetView != null) {
                frameLayout.add(widgetView);
            }
        }
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        return frameLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        Log.i("2017NewYear", "createClockLayout26WC: ");
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent hourView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent secondView = new SlptPredrawedSecondView();
        SlptPictureView lowBatteryView = new SlptPictureView();
        SlptPictureView bgView = new SlptPictureView();
        SlptPictureView bgView2 = new SlptPictureView();
        String[] assetsMinutePath = new String[]{"guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/0.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/1.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/2.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/3.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/4.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/5.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/6.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/7.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/8.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/9.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/10.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/11.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/12.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/13.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/14.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/minute/15.png.color_map"};
        String[] assetsHourPath = new String[]{"guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/0.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/1.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/2.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/3.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/4.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/5.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/6.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/7.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/8.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/9.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/10.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/11.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/12.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/13.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/14.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/hour/15.png.color_map"};
        String[] assetsSecondPath = new String[]{"guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/0.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/1.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/2.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/3.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/4.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/5.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/6.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/7.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/8.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/9.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/10.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/11.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/12.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/13.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/14.png.color_map", "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/26WC/second/15.png.color_map"};
        byte[] bgMem = null;
        WatchFaceConfig config = WatchFaceConfig.getWatchFaceConfig(getApplicationContext(), AnalogWatchFaceThirteen.class.getName());
        if (config != null) {
            switch (config.getBgType()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    bgMem = SimpleFile.readFileFromAssets(this, config.getBgPathSlpt());
                    break;
                case 4:
                    bgMem = SimpleFile.readFile(config.getBgPathSlpt());
                    break;
            }
        }
        if (bgMem == null) {
            bgMem = SimpleFile.readFileFromAssets(this, "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/watchface_bg_analog_thirteen_0.png");
        }
        bgView.setImagePicture(bgMem);
        frameLayout.background.color = -16777216;
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsMinutePath);
        super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
        preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsHourPath);
        super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
        preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsSecondPath);
        super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
        minuteView.setPreDrawedPicture(preDrawedPictureGroup);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(preDrawedPictureGroup);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        secondView.setPreDrawedPicture(preDrawedPictureGroup);
        secondView.setStart(10, 10);
        secondView.setRect(300, 300);
        bgView2.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/watchface_default_graduation.png"));
        if (bgView != null) {
            frameLayout.add(bgView);
        }
        frameLayout.add(bgView2);
        ArrayList<DataWidgetConfig> mWidgets = null;
        if (config != null) {
            mWidgets = config.getDataWidgets();
        }
        if (mWidgets.size() < 1) {
            mWidgets.add(new DataWidgetConfig(3, 6, 110, 94, 0, null, null));
        }
        for (int j = 0; j < mWidgets.size(); j++) {
            SlptViewComponent widgetView = get8cDataWidget((DataWidgetConfig) mWidgets.get(j));
            if (widgetView != null) {
                frameLayout.add(widgetView);
            }
        }
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        return frameLayout;
    }
}
