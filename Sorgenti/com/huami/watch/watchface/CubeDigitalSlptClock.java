package com.huami.watch.watchface;

import android.content.Context;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class CubeDigitalSlptClock extends AbstractSlptClock {
    String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    byte[][] images = new byte[10][];
    private SlptViewComponent lowBatteryView = null;
    private Context mContext;
    String[] weekNums = DigitalWatchFace.WEEKDAYS;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        initImageBytes();
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -65536, 0);
        this.lowBatteryView.setStart(0, 70);
        this.lowBatteryView.setRect(320, this.mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
    }

    protected SlptLayout createClockLayout8C() {
        SlptAbsoluteLayout rootLayout = new SlptAbsoluteLayout();
        SlptLinearLayout dateLinearLayout = new SlptLinearLayout();
        SlptPictureView dateSepView1 = new SlptPictureView();
        SlptMonthHView monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptPictureView dateSepView2 = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptLinearLayout timeLinearLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptPictureView timeSepView = new SlptPictureView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        SlptPictureView bgView = new SlptPictureView();
        dateSepView1.textSize = (float) 18;
        monthHView.textSize = (float) 18;
        monthLView.textSize = (float) 18;
        dateSepView2.textSize = (float) 18;
        dayHView.textSize = (float) 18;
        dayLView.textSize = (float) 18;
        weekView.textSize = (float) 18;
        dateSepView1.textColor = -1;
        monthHView.textColor = -1;
        monthLView.textColor = -1;
        dateSepView2.textColor = -1;
        dayHView.textColor = -1;
        dayLView.textColor = -1;
        weekView.textColor = -1;
        dateSepView1.setStringPicture(getResources().getString(R.string.cube_month));
        monthHView.setStringPictureArray(this.digitalNums);
        monthLView.setStringPictureArray(this.digitalNums);
        dateSepView2.setStringPicture(getResources().getString(R.string.cube_day));
        dayHView.setStringPictureArray(this.digitalNums);
        dayLView.setStringPictureArray(this.digitalNums);
        weekView.setStringPictureArray(getApplicationContext().getResources().getStringArray(R.array.weekdays));
        weekView.padding.left = (short) 2;
        dateLinearLayout.add(monthHView);
        dateLinearLayout.add(monthLView);
        dateLinearLayout.add(dateSepView1);
        dateLinearLayout.add(dayHView);
        dateLinearLayout.add(dayLView);
        dateLinearLayout.add(dateSepView2);
        dateLinearLayout.add(weekView);
        dateLinearLayout.setStart(0, 193);
        dateLinearLayout.setRect(320, 2147483646);
        dateLinearLayout.alignX = (byte) 2;
        hourHView.setImagePictureArray(this.images);
        hourLView.setImagePictureArray(this.images);
        minuteHView.setImagePictureArray(this.images);
        minuteLView.setImagePictureArray(this.images);
        timeSepView.setImagePicture(SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_x.png"));
        hourLView.padding.left = (short) 10;
        timeSepView.padding.left = (short) 13;
        minuteHView.padding.left = (short) 13;
        minuteLView.padding.left = (short) 10;
        timeLinearLayout.add(hourHView);
        timeLinearLayout.add(hourLView);
        timeLinearLayout.add(timeSepView);
        timeLinearLayout.add(minuteHView);
        timeLinearLayout.add(minuteLView);
        timeLinearLayout.setStart(0, 108);
        timeLinearLayout.setRect(320, 2147483646);
        timeLinearLayout.alignX = (byte) 2;
        byte[] bgImg = SimpleFile.readFileFromAssets(this, "nothing.png");
        if (bgImg != null) {
            bgView.setImagePicture(bgImg);
        }
        rootLayout.add(bgView);
        rootLayout.add(timeLinearLayout);
        rootLayout.add(dateLinearLayout);
        rootLayout.add(this.lowBatteryView);
        return rootLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }

    private void initImageBytes() {
        this.images[0] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_0.png");
        this.images[1] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_1.png");
        this.images[2] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_2.png");
        this.images[3] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_3.png");
        this.images[4] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_4.png");
        this.images[5] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_5.png");
        this.images[6] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_6.png");
        this.images[7] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_7.png");
        this.images[8] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_8.png");
        this.images[9] = SimpleFile.readFileFromAssets(this, "slpt-digital-cube/cube_9.png");
    }
}
