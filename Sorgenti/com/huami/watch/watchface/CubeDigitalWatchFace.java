package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.cubedigital.WholeNumber;
import com.huami.watch.watchface.cubedigital.WholeNumber.IWholeNumber;
import com.huami.watch.watchface.util.Util;

public class CubeDigitalWatchFace extends AbstractWatchFace {

    private class Engine extends DigitalEngine implements IWholeNumber {
        private String mDayStr;
        private WholeNumber mHourH;
        private WholeNumber mHourL;
        private boolean mIsFirstLoaded;
        private float mMarginLeft;
        private WholeNumber mMinH;
        private WholeNumber mMinL;
        private String mMonthStr;
        private float mRadius;
        private float mRectWidth;
        private float mSecMarginLeft;
        private float mSecMarginTop;
        private Paint mSecPaint;
        private float mStartLeft;
        private float mStartTop;
        private float mTextMarginBottom;
        private Paint mTextPaint;
        private float mTextSize;

        private Engine() {
            super();
            this.mIsFirstLoaded = true;
        }

        protected void onPrepareResources(Resources resources) {
            this.mTextPaint = new Paint(5);
            this.mSecPaint = new Paint(5);
            this.mSecPaint.setColor(-16776961);
            this.mStartLeft = resources.getDimension(R.dimen.cube_start_left);
            this.mStartTop = resources.getDimension(R.dimen.cube_start_top);
            this.mRectWidth = resources.getDimension(R.dimen.cube_rect_width);
            this.mRadius = resources.getDimension(R.dimen.cube_sec_radius);
            this.mMarginLeft = resources.getDimension(R.dimen.cube_magin_left);
            this.mSecMarginTop = resources.getDimension(R.dimen.cube_sec_magin_top);
            this.mSecMarginLeft = resources.getDimension(R.dimen.cube_sec_margin_left);
            this.mTextSize = resources.getDimension(R.dimen.cube_text_size);
            this.mTextMarginBottom = resources.getDimension(R.dimen.cube_txt_margin_bottom);
            this.mMonthStr = resources.getString(R.string.cube_month);
            this.mDayStr = resources.getString(R.string.cube_day);
            this.mTextPaint.setTextSize(this.mTextSize);
            this.mTextPaint.setColor(-1);
            this.mTextPaint.setTextAlign(Align.CENTER);
            this.mHourH = new WholeNumber(this.mStartLeft, this.mStartTop, this.mRectWidth, 0);
            this.mHourL = new WholeNumber((this.mStartLeft + (this.mRectWidth * 2.0f)) + this.mMarginLeft, this.mStartTop, this.mRectWidth, 1);
            this.mMinH = new WholeNumber(((this.mStartLeft + (4.0f * this.mRectWidth)) + this.mMarginLeft) + (this.mRadius * 6.0f), this.mStartTop, this.mRectWidth, 2);
            this.mMinL = new WholeNumber(((this.mStartLeft + (this.mRectWidth * 6.0f)) + (this.mMarginLeft * 2.0f)) + (this.mRadius * 6.0f), this.mStartTop, this.mRectWidth, 3);
            this.mHourH.setListener(this);
            this.mHourL.setListener(this);
            this.mMinH.setListener(this);
            this.mMinL.setListener(this);
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            canvas.drawColor(0, Mode.CLEAR);
            updateCurrentTime(hours, minutes);
            if (!this.mDrawTimeIndicator) {
                canvas.drawCircle(this.mSecMarginLeft + this.mRadius, this.mSecMarginTop + this.mRadius, this.mRadius, this.mSecPaint);
                canvas.drawCircle(this.mSecMarginLeft + this.mRadius, this.mSecMarginTop + (5.0f * this.mRadius), this.mRadius, this.mSecPaint);
            }
            this.mHourH.drawSelf(canvas);
            this.mHourL.drawSelf(canvas);
            this.mMinH.drawSelf(canvas);
            this.mMinL.drawSelf(canvas);
            canvas.drawText(Util.formatTime(month) + this.mMonthStr + Util.formatTime(day) + this.mDayStr + " " + CubeDigitalWatchFace.this.getApplicationContext().getResources().getStringArray(R.array.weekdays)[week - 1], centerX, 300.0f - this.mTextMarginBottom, this.mTextPaint);
        }

        public void updateCurrentTime(int hours, int mins) {
            int h1 = hours / 10;
            int h2 = hours % 10;
            int m1 = mins / 10;
            int m2 = mins % 10;
            if (this.mIsFirstLoaded) {
                this.mHourH.setCurNumber(h1, false, 0);
                this.mHourL.setCurNumber(h2, false, 0);
                this.mMinH.setCurNumber(m1, false, 0);
                this.mMinL.setCurNumber(m2, false, 0);
                this.mIsFirstLoaded = false;
                return;
            }
            int delay = 0;
            if (this.mHourH.setCurNumber(h1)) {
                delay = 0 + 50;
            }
            if (this.mHourL.setCurNumber(h2, delay)) {
                delay += 50;
            }
            if (this.mMinH.setCurNumber(m1, delay)) {
                delay += 50;
            }
            this.mMinL.setCurNumber(m2, delay);
        }

        public void refreshView() {
            postInvalidate();
        }

        protected int getInitAnimDuration() {
            return 1;
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return CubeDigitalSlptClock.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(31.0f);
        return new Engine();
    }
}
