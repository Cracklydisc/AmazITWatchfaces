package com.huami.watch.watchface;

import android.graphics.Typeface;
import com.huami.watch.watchface.util.TypefaceManager;
import com.ingenic.iwds.slpt.view.core.SlptFrameLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.digital.SlptYear0View;
import com.ingenic.iwds.slpt.view.digital.SlptYear1View;
import com.ingenic.iwds.slpt.view.digital.SlptYear2View;
import com.ingenic.iwds.slpt.view.digital.SlptYear3View;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalSlptClock extends AbstractSlptClock {
    String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    String[] weekNums = DigitalWatchFace.WEEKDAYS;

    protected void initWatchFaceConfig() {
    }

    protected SlptLayout createClockLayout8C() {
        SlptLayout rootLayout = new SlptFrameLayout();
        SlptLinearLayout dateLinearLayout = new SlptLinearLayout();
        SlptViewComponent year0View = new SlptYear0View();
        SlptViewComponent year1View = new SlptYear1View();
        SlptViewComponent year2View = new SlptYear2View();
        SlptViewComponent year3View = new SlptYear3View();
        SlptPictureView dateSepView1 = new SlptPictureView();
        SlptMonthHView monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptPictureView dateSepView2 = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptViewComponent timeLinearLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptPictureView timeSepView = new SlptPictureView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        SlptPictureView bgView = new SlptPictureView();
        year0View.textSize = (float) 19;
        year1View.textSize = (float) 19;
        year2View.textSize = (float) 19;
        year3View.textSize = (float) 19;
        dateSepView1.textSize = (float) 19;
        monthHView.textSize = (float) 19;
        monthLView.textSize = (float) 19;
        dateSepView2.textSize = (float) 19;
        dayHView.textSize = (float) 19;
        dayLView.textSize = (float) 19;
        weekView.textSize = (float) 24;
        year0View.textColor = -1;
        year1View.textColor = -1;
        year2View.textColor = -1;
        year3View.textColor = -1;
        dateSepView1.textColor = -1;
        monthHView.textColor = -1;
        monthLView.textColor = -1;
        dateSepView2.textColor = -1;
        dayHView.textColor = -1;
        dayLView.textColor = -1;
        weekView.textColor = -1;
        year0View.setStringPictureArray(this.digitalNums);
        year1View.setStringPictureArray(this.digitalNums);
        year2View.setStringPictureArray(this.digitalNums);
        year3View.setStringPictureArray(this.digitalNums);
        dateSepView1.setStringPicture('/');
        monthHView.setStringPictureArray(this.digitalNums);
        monthLView.setStringPictureArray(this.digitalNums);
        dateSepView2.setStringPicture('/');
        dayHView.setStringPictureArray(this.digitalNums);
        dayLView.setStringPictureArray(this.digitalNums);
        weekView.setStringPictureArray(this.weekNums);
        weekView.padding.left = (short) 71;
        dateLinearLayout.add(year3View);
        dateLinearLayout.add(year2View);
        dateLinearLayout.add(year1View);
        dateLinearLayout.add(year0View);
        dateLinearLayout.add(dateSepView1);
        dateLinearLayout.add(monthHView);
        dateLinearLayout.add(monthLView);
        dateLinearLayout.add(dateSepView2);
        dateLinearLayout.add(dayHView);
        dateLinearLayout.add(dayLView);
        dateLinearLayout.orientation = (byte) 0;
        dateLinearLayout.padding.left = (short) 71;
        dateLinearLayout.padding.top = (short) 6;
        Typeface timeTypeface = TypefaceManager.get().createFromAsset("typeface/DINPro-Normal.ttf");
        if (timeTypeface == null) {
            timeTypeface = Typeface.DEFAULT;
        }
        hourHView.textColor = -1;
        hourLView.textColor = -1;
        timeSepView.textColor = -1;
        minuteHView.textColor = -1;
        minuteLView.textColor = -1;
        hourHView.textSize = (float) 68;
        hourLView.textSize = (float) 68;
        timeSepView.textSize = (float) 68;
        minuteHView.textSize = (float) 68;
        minuteLView.textSize = (float) 68;
        hourHView.typeface = timeTypeface;
        hourLView.typeface = timeTypeface;
        timeSepView.typeface = timeTypeface;
        minuteHView.typeface = timeTypeface;
        minuteLView.typeface = timeTypeface;
        hourHView.setStringPictureArray(this.digitalNums);
        hourLView.setStringPictureArray(this.digitalNums);
        timeSepView.setStringPicture(":");
        minuteHView.setStringPictureArray(this.digitalNums);
        minuteLView.setStringPictureArray(this.digitalNums);
        timeLinearLayout.add(hourHView);
        timeLinearLayout.add(hourLView);
        timeLinearLayout.add(timeSepView);
        timeLinearLayout.add(minuteHView);
        timeLinearLayout.add(minuteLView);
        timeLinearLayout.orientation = (byte) 0;
        timeLinearLayout.padding.left = (short) 71;
        timeLinearLayout.padding.top = (short) 85;
        SlptLinearLayout linearLayout = new SlptLinearLayout();
        linearLayout.add(timeLinearLayout);
        linearLayout.add(weekView);
        linearLayout.add(dateLinearLayout);
        linearLayout.orientation = (byte) 1;
        byte[] bgImg = SimpleFile.readFileFromAssets(this, "slpt-digital/bg.png");
        if (bgImg != null) {
            bgView.setImagePicture(bgImg);
        }
        bgView.descHeight = (byte) 2;
        bgView.descWidth = (byte) 2;
        bgView.rect.width = 320;
        bgView.rect.height = 300;
        rootLayout.add(bgView);
        rootLayout.add(linearLayout);
        return rootLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
