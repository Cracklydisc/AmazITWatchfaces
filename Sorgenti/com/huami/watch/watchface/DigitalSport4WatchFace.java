package com.huami.watch.watchface;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.recyclerview.C0051R;
import android.support.wearable.view.SimpleAnimatorListener;
import android.text.TextPaint;
import android.util.Log;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class DigitalSport4WatchFace extends AbstractWatchFace {
    private int step_distance = 3;

    private class Engine extends DigitalEngine {
        private Paint cureBackPaint;
        private TextPaint datePaint;
        private TextPaint hourPaint;
        private Bitmap icon_step;
        private Bitmap km_icon;
        private float km_left;
        private float km_top;
        private float left_hour;
        private float left_km_text;
        private float left_minute;
        private float left_month;
        private float left_step_img;
        private float left_step_text;
        private float left_weekly;
        Bitmap mBackgroundBitmap;
        private int mCurStepCount;
        private RectF mDest;
        private int mOldCurStepCount;
        private Paint mPaint;
        private float mRadius;
        private ValueAnimator mStepAnimator;
        private int mStepCompose;
        private WatchDataListener mStepDataListener;
        private int mStepDiff;
        private Paint mStepPaint;
        private WatchDataListener mTotalDistanceDataListener;
        private int mTotalStepTarget;
        private TextPaint minutePaint;
        private TextPaint monthPaint;
        private TextPaint stepNumberPaint;
        private float top_hour;
        private float top_km_text;
        private float top_minute;
        private float top_month;
        private float top_step_img;
        private float top_step_text;
        private float top_weekly;
        private double totalKMNumber;
        private String totalKmString;

        class C01651 implements AnimatorUpdateListener {
            C01651() {
            }

            public void onAnimationUpdate(ValueAnimator animation) {
                Engine.this.mStepCompose = (int) (((float) Engine.this.mStepDiff) * ((Float) animation.getAnimatedValue()).floatValue());
                Engine.this.postInvalidate();
            }
        }

        class C01662 extends SimpleAnimatorListener {
            C01662() {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                Engine.this.mStepCompose = 0;
                Engine.this.mOldCurStepCount = Engine.this.mCurStepCount;
            }

            public void onAnimationCancel(Animator animator) {
                Engine.this.mStepCompose = 0;
                Engine.this.mOldCurStepCount = Engine.this.mCurStepCount;
            }
        }

        class C01673 implements WatchDataListener {
            C01673() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 3) {
                    Engine.this.totalKMNumber = ((Double) values[0]).doubleValue();
                    Log.d("HmWatchFace", "onDataUpdate totalKMNumber = " + Engine.this.totalKMNumber);
                    Engine.this.updateSportTotalDistance();
                }
            }

            public int getDataType() {
                return 3;
            }
        }

        class C01684 implements WatchDataListener {
            C01684() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 1) {
                    Engine.this.mCurStepCount = ((Integer) values[0]).intValue();
                    if (Engine.this.mOldCurStepCount != Engine.this.mCurStepCount) {
                        if (Engine.this.mOldCurStepCount > Engine.this.mCurStepCount) {
                            Engine.this.mOldCurStepCount = 0;
                        }
                        Engine.this.mStepAnimator.cancel();
                        Engine.this.mStepDiff = Engine.this.mCurStepCount - Engine.this.mOldCurStepCount;
                        Engine.this.mStepAnimator.start();
                    }
                }
            }

            public int getDataType() {
                return 1;
            }
        }

        private Engine() {
            super();
            this.km_icon = null;
            this.icon_step = null;
            this.mCurStepCount = 0;
            this.mOldCurStepCount = 0;
            this.mStepDiff = 0;
            this.mStepCompose = 0;
            this.mDest = new RectF();
            this.totalKMNumber = -1.0d;
            this.totalKmString = "--" + getDistanceUnit();
            this.mTotalDistanceDataListener = new C01673();
            this.mTotalStepTarget = 8000;
            this.mStepDataListener = new C01684();
        }

        private String getDistanceUnit() {
            switch (getMeasurement()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    return "mi";
                default:
                    return "km";
            }
        }

        protected void onPrepareResources(Resources resources) {
            this.mPaint = new Paint(1);
            this.mPaint.setColor(resources.getColor(R.color.digital_border_color));
            this.mPaint.setStrokeWidth(resources.getDimension(R.dimen.digital_circle_border));
            this.mPaint.setStyle(Style.STROKE);
            this.km_icon = ((BitmapDrawable) resources.getDrawable(R.drawable.digital_sport_4_icon_km, null)).getBitmap();
            this.icon_step = ((BitmapDrawable) resources.getDrawable(R.drawable.digital_sport_4_icon_step, null)).getBitmap();
            this.mBackgroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.nothing, null)).getBitmap();
            this.cureBackPaint = new Paint(1);
            this.cureBackPaint.setColor(resources.getColor(R.color.digital_sport_arc_gray));
            this.cureBackPaint.setStrokeWidth(resources.getDimension(R.dimen.digital_sport_cure_width));
            this.mRadius = resources.getDimension(R.dimen.digital_circle_radius);
            this.hourPaint = new TextPaint();
            this.hourPaint.setColor(-256);
            this.hourPaint.setTextSize(resources.getDimension(R.dimen.digital_sport_hour_text));
            this.hourPaint.setTextAlign(Align.RIGHT);
            this.hourPaint.setAntiAlias(true);
            this.hourPaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/HUAMI8CSPORT.ttf"));
            this.minutePaint = new TextPaint();
            this.minutePaint.setColor(resources.getColor(R.color.digital_sport_font_white));
            this.minutePaint.setTextSize(resources.getDimension(R.dimen.digital_sport_minuts_text));
            this.minutePaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/huamifont.ttf"));
            this.minutePaint.setTextAlign(Align.LEFT);
            this.minutePaint.setAntiAlias(true);
            this.mStepPaint = new Paint(1);
            this.mStepPaint.setColor(resources.getColor(R.color.digital_sport_arc_lightblue));
            this.mStepPaint.setStrokeCap(Cap.ROUND);
            this.mStepPaint.setStrokeWidth(resources.getDimension(R.dimen.digital_circle_border));
            this.mStepPaint.setStyle(Style.STROKE);
            this.mStepPaint.setAntiAlias(true);
            this.stepNumberPaint = new TextPaint();
            this.stepNumberPaint.setColor(-1);
            this.stepNumberPaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/huamifont.ttf"));
            this.stepNumberPaint.setTextSize(resources.getDimension(R.dimen.digital_sport_step_text));
            this.stepNumberPaint.setAntiAlias(true);
            this.datePaint = new TextPaint();
            this.datePaint.setColor(-1);
            this.datePaint.setAlpha(153);
            this.datePaint.setTextSize(resources.getDimension(R.dimen.digital_sport_date_size));
            this.datePaint.setTextAlign(Align.CENTER);
            this.datePaint.setAntiAlias(true);
            this.monthPaint = new TextPaint();
            this.monthPaint.setColor(-1);
            this.monthPaint.setAlpha(153);
            this.monthPaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/AvenirLTStd-Book.otf"));
            this.monthPaint.setTextSize(resources.getDimension(R.dimen.digital_sport_month_size));
            this.monthPaint.setTextAlign(Align.CENTER);
            this.monthPaint.setAntiAlias(true);
            this.km_left = resources.getDimension(R.dimen.digital_sport_km_left);
            this.km_top = resources.getDimension(R.dimen.digital_sport_km_top);
            this.left_km_text = resources.getDimension(R.dimen.digital_sport_left_km_text);
            this.top_km_text = resources.getDimension(R.dimen.digital_sport_top_km_text);
            this.left_hour = resources.getDimension(R.dimen.digital_sport_left_hour);
            this.top_hour = resources.getDimension(R.dimen.digital_sport_top_hour);
            this.left_minute = resources.getDimension(R.dimen.digital_sport_left_minute);
            this.top_minute = resources.getDimension(R.dimen.digital_sport_top_minute);
            this.left_weekly = resources.getDimension(R.dimen.digital_sport_left_weekly);
            this.top_weekly = resources.getDimension(R.dimen.digital_sport_top_weekly);
            this.left_step_img = resources.getDimension(R.dimen.digital_sport_left_step_imag);
            this.top_step_img = resources.getDimension(R.dimen.digital_sport_top_step_imag);
            this.left_step_text = resources.getDimension(R.dimen.digital_sport_left_step_text);
            this.top_step_text = resources.getDimension(R.dimen.digital_sport_top_step_text);
            this.left_month = resources.getDimension(R.dimen.digital_sport_left_month);
            this.top_month = resources.getDimension(R.dimen.digital_sport_top_month);
            this.mStepAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mStepAnimator.addUpdateListener(new C01651());
            this.mStepAnimator.addListener(new C01662());
            this.mStepAnimator.setDuration(300);
            registerWatchDataListener(this.mTotalDistanceDataListener);
            registerWatchDataListener(this.mStepDataListener);
        }

        private void updateSportTotalDistance() {
            Log.i("HmWatchFace", "updateSportTotalDistance: totalNumbers :" + this.totalKMNumber);
            this.totalKmString = Util.getFormatDistance(this.totalKMNumber) + getDistanceUnit();
        }

        protected void onMeasurementChanged(int measurement) {
            updateSportTotalDistance();
            invalidate();
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            canvas.drawColor(-16777216);
            drawBackgroundArc(canvas, centerX, centerY);
            drawStepContent(canvas, centerX, centerY);
            canvas.save();
            this.km_left = (float) DigitalSport4WatchFace.this.getStepImgCenterXPos(this.totalKmString, (float) this.km_icon.getWidth(), centerX, this.stepNumberPaint);
            canvas.drawBitmap(this.km_icon, this.km_left, this.km_top, null);
            canvas.restore();
            this.left_km_text = (float) DigitalSport4WatchFace.this.getStepTextPosX(this.totalKmString, (float) this.km_icon.getWidth(), centerX, this.stepNumberPaint);
            canvas.drawText(this.totalKmString, this.left_km_text, this.top_km_text, this.stepNumberPaint);
            canvas.drawText("" + (hours < 10 ? "0" + hours : Integer.valueOf(hours)), this.left_hour, this.top_hour, this.hourPaint);
            canvas.drawText("" + (minutes < 10 ? "0" + minutes : Integer.valueOf(minutes)), this.left_minute, this.top_minute, this.minutePaint);
            drawMonthDay(canvas, month, day, this.monthPaint);
            canvas.drawText("" + DigitalSport4WatchFace.this.getApplicationContext().getResources().getStringArray(R.array.weekdays)[week - 1], this.left_weekly, this.top_weekly, this.datePaint);
            canvas.drawBitmap(this.icon_step, (float) DigitalSport4WatchFace.this.getStepImgCenterXPos((this.mOldCurStepCount + this.mStepCompose) + "/" + this.mTotalStepTarget, (float) this.icon_step.getWidth(), centerX, this.stepNumberPaint), this.top_step_img, null);
            this.left_step_text = (float) DigitalSport4WatchFace.this.getStepTextPosX((this.mOldCurStepCount + this.mStepCompose) + "/" + this.mTotalStepTarget, (float) this.icon_step.getWidth(), centerX, this.stepNumberPaint);
            canvas.drawText("" + (this.mOldCurStepCount + this.mStepCompose) + "/" + this.mTotalStepTarget, this.left_step_text, this.top_step_text, this.stepNumberPaint);
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void drawStepContent(Canvas canvas, float centerX, float centerY) {
            float degree = (((float) (this.mOldCurStepCount + this.mStepCompose)) / ((float) this.mTotalStepTarget)) * 360.0f;
            canvas.save();
            canvas.drawArc(centerX - this.mRadius, centerY - this.mRadius, centerX + this.mRadius, centerY + this.mRadius, -90.0f, degree, false, this.mStepPaint);
            canvas.restore();
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(DigitalSport4WatchFace.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void drawMonthDay(Canvas canvas, int month, int day, Paint dataPaint) {
            String date = "";
            if (month < 10) {
                date = date + "0";
            }
            date = date + month + "-";
            if (day < 10) {
                date = date + "0";
            }
            canvas.drawText(date + day, this.left_month, this.top_month, dataPaint);
        }

        private void drawBackgroundArc(Canvas canvas, float centerX, float centerY) {
            canvas.save();
            canvas.drawCircle(centerX, centerY, this.mRadius, this.mPaint);
            canvas.restore();
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalSport4WatchFaceSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(21.0f);
        return new Engine();
    }

    private int getStepImgCenterXPos(String str, float imageWidth, float centerX, Paint paint) {
        return (int) (((centerX - ((float) (computerDataTextLength(str, paint) / 2))) - (imageWidth / 2.0f)) - ((float) this.step_distance));
    }

    private int getStepTextPosX(String str, float imageWidth, float centerX, Paint paint) {
        return (int) (((centerX - ((float) (computerDataTextLength(str, paint) / 2))) + (imageWidth / 2.0f)) + ((float) this.step_distance));
    }

    private int computerDataTextLength(String str, Paint paint) {
        return getTextWidth(paint, str);
    }

    public int getTextWidth(Paint paint, String str) {
        int iRet = 0;
        if (str != null && str.length() > 0) {
            int len = str.length();
            float[] widths = new float[len];
            paint.getTextWidths(str, widths);
            for (int j = 0; j < len; j++) {
                iRet += (int) Math.ceil((double) widths[j]);
            }
        }
        return iRet;
    }
}
