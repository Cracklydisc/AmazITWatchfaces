package com.huami.watch.watchface;

import android.content.Context;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepTargetView;
import com.ingenic.iwds.slpt.view.sport.SlptTotalDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptTotalDistanceLView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalSport4WatchFaceSlpt extends AbstractSlptClock {
    private byte[][] date_num = new byte[10][];
    private String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private byte[][] hour_num = new byte[10][];
    private SlptViewComponent lowBatteryView = null;
    private Context mContext;
    private byte[][] minute_num = new byte[10][];
    private byte[][] small_num = new byte[10][];
    private byte[][] week_num = new byte[7][];

    private void init_num_mem() {
        int i;
        String path;
        for (i = 0; i < 10; i++) {
            this.date_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/manbuqingchun/date_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            path = new String("guard/manbuqingchun/en/week_%d.png");
        } else {
            path = new String("guard/manbuqingchun/week_%d.png");
        }
        for (i = 0; i < 7; i++) {
            this.week_num[i] = SimpleFile.readFileFromAssets(this, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.hour_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/manbuqingchun/hour_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.minute_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/manbuqingchun/minute_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.small_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/manbuqingchun/small_num_%d.png", new Object[]{Integer.valueOf(i)}));
        }
    }

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        init_num_mem();
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -65536, 0);
        this.lowBatteryView.setStart(0, 36);
        this.lowBatteryView.setRect(320, this.mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
    }

    protected SlptLayout createClockLayout8C() {
        byte[] kmTextMem;
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptLinearLayout hourLayout = new SlptLinearLayout();
        SlptViewComponent minuteLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptViewComponent stepArcAnagleview = new SlptTodayStepArcAnglePicView();
        SlptViewComponent totalLayout = new SlptLinearLayout();
        SlptLinearLayout totalValueLayout = new SlptLinearLayout();
        SlptPictureView kmIconView = new SlptPictureView();
        SlptViewComponent totalDistanceFview = new SlptTotalDistanceFView();
        SlptViewComponent totalDisSeqView = new SlptPictureView();
        SlptViewComponent totalDistanceLView = new SlptTotalDistanceLView();
        SlptPictureView kmTextView = new SlptPictureView();
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptViewComponent stepLayout = new SlptLinearLayout();
        SlptTodayStepNumView stepNumView = new SlptTodayStepNumView();
        SlptViewComponent stepSeqView = new SlptPictureView();
        SlptViewComponent stepTargetView = new SlptTodayStepTargetView();
        SlptViewComponent stepIconView = new SlptPictureView();
        hourLayout.add(hourHView);
        hourLayout.add(hourLView);
        minuteLayout.add(minuteHView);
        minuteLayout.add(minuteLView);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        totalValueLayout.add(totalDistanceFview);
        totalValueLayout.add(totalDisSeqView);
        totalValueLayout.add(totalDistanceLView);
        totalValueLayout.add(kmTextView);
        totalLayout.add(kmIconView);
        totalLayout.add(totalValueLayout);
        stepLayout.add(stepIconView);
        stepLayout.add(stepNumView);
        stepLayout.add(stepSeqView);
        stepLayout.add(stepTargetView);
        linearLayout.add(totalLayout);
        linearLayout.add(hourLayout);
        linearLayout.add(minuteLayout);
        linearLayout.add(monthLayout);
        linearLayout.add(weekView);
        linearLayout.add(stepLayout);
        linearLayout.add(stepArcAnagleview);
        linearLayout.add(this.lowBatteryView);
        linearLayout.background.color = -16777216;
        stepArcAnagleview.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/manbuqingchun/watchface_default_status_8c.png"));
        kmIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/manbuqingchun/watchface_icon_km.png"));
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/manbuqingchun/watchface_icon_step_8c.png"));
        stepSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/manbuqingchun/small_step_seq.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/manbuqingchun/date_seq.png"));
        if (Util.getMeasurement(this) == 1) {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/manbuqingchun/en/small_num_km.png");
        } else {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/manbuqingchun/small_num_km.png");
        }
        kmTextView.setImagePicture(kmTextMem);
        totalDisSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/manbuqingchun/distance_point.png"));
        totalDisSeqView.id = (short) 11;
        hourLayout.setImagePictureArrayForAll(this.hour_num);
        minuteLayout.setImagePictureArrayForAll(this.minute_num);
        totalDistanceFview.setImagePictureArray(this.small_num);
        totalDistanceLView.setImagePictureArray(this.small_num);
        kmIconView.setPadding(0, 5, 0, 0);
        totalValueLayout.setPadding(0, 0, 2, 0);
        totalLayout.setStart(0, 62);
        totalLayout.setRect(320, 28);
        totalLayout.alignX = (byte) 2;
        kmTextView.alignParentY = (byte) 0;
        monthLayout.setStart(186, 170);
        monthLayout.setImagePictureArrayForAll(this.date_num);
        weekView.setStart(189, 200);
        weekView.setImagePictureArray(this.week_num);
        stepIconView.setPadding(0, 6, 0, 0);
        stepLayout.setStart(0, 234);
        stepLayout.setRect(320, 24);
        stepNumView.setImagePictureArray(this.small_num);
        stepTargetView.setImagePictureArray(this.small_num);
        stepLayout.alignX = (byte) 2;
        stepArcAnagleview.start_angle = 0;
        stepArcAnagleview.full_angle = 360;
        hourLayout.setStart(60, 93);
        minuteLayout.setStart(188, 96);
        return linearLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
