package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextPaint;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;

public class DigitalWatchFace extends AbstractWatchFace {
    static final String[] WEEKDAYS = new String[]{"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};

    private class Engine extends DigitalEngine {
        Bitmap mBackgroundBitmap;
        TextPaint mDatePaint;
        TextPaint mTimePaint;

        private Engine() {
            super();
        }

        protected void onPrepareResources(Resources resources) {
            this.mBackgroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_bg_digital, null)).getBitmap();
            this.mTimePaint = new TextPaint();
            this.mTimePaint.setAntiAlias(true);
            this.mTimePaint.setColor(-1);
            this.mTimePaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/DINPro-Normal.ttf"));
            this.mDatePaint = new TextPaint();
            this.mDatePaint.setAntiAlias(true);
            this.mDatePaint.setColor(-1);
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            canvas.drawBitmap(this.mBackgroundBitmap, 0.0f, 0.0f, null);
            String time = "";
            if (hours < 10) {
                time = time + "0";
            }
            time = time + hours;
            this.mTimePaint.setTextSize(68.0f);
            canvas.drawText(time, 70.0f, 136.0f, this.mTimePaint);
            if (!this.mDrawTimeIndicator) {
                canvas.drawText(":", 142.0f, 136.0f, this.mTimePaint);
            }
            time = "";
            if (minutes < 10) {
                time = "0";
            }
            canvas.drawText(time + minutes, 163.0f, 136.0f, this.mTimePaint);
            String weekStr = DigitalWatchFace.WEEKDAYS[week - 1];
            this.mDatePaint.setTextSize(24.0f);
            canvas.drawText(weekStr, 71.0f, 173.0f, this.mDatePaint);
            String date = year + "/";
            if (month < 10) {
                date = date + "0";
            }
            date = date + month + "/";
            if (day < 10) {
                date = date + "0";
            }
            date = date + day;
            this.mDatePaint.setTextSize(19.0f);
            canvas.drawText(date, 71.0f, 201.0f, this.mDatePaint);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalSlptClock.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(33.0f);
        return new Engine();
    }
}
