package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.recyclerview.C0051R;
import android.text.TextPaint;
import android.util.Log;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import java.io.IOException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

public class DigitalWatchFaceSportEight extends AbstractWatchFace {

    private class Engine extends DigitalEngine {
        private Bitmap[] NUMS;
        private Bitmap[] WEEKS;
        private Bitmap big_circle;
        private boolean hourFormat12;
        private Bitmap mAmBitmap;
        private RectF mBound;
        private Path mClipPathBig;
        private Path mClipPathSmall;
        private int mColorRed;
        private int mColorWhite;
        private int mCurStepCount;
        private Paint mGPaint;
        private float mLeftAm;
        private float mLeftDate;
        private float mLeftWeek;
        private TextPaint mPaintHuamiFont;
        private Bitmap mPmBitmap;
        private int mProgressDegreeStep;
        private double mSportTodayDistance;
        private int mSportTotalDistance;
        private double mSportTotalDistanceKm;
        private WatchDataListener mStepDataListener;
        private WatchDataListener mTodayDistanceDataListener;
        private float mTopAm;
        private float mTopDate;
        private float mTopWeek;
        private WatchDataListener mTotalDistanceDataListener;
        private Bitmap mTotalKmIcon;
        private int mTotalStepTarget;
        private Bitmap minus;
        private Bitmap small_circle;
        private Bitmap unit;

        class C01711 implements WatchDataListener {
            C01711() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 1) {
                    Engine.this.mCurStepCount = ((Integer) values[0]).intValue();
                    Engine.this.updateStepInfo();
                }
            }

            public int getDataType() {
                return 1;
            }
        }

        class C01722 implements WatchDataListener {
            C01722() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 2) {
                    Engine.this.mSportTodayDistance = ((Double) values[0]).doubleValue();
                    Log.d("SportEight", "onDataUpdate mSportTodayDistance = " + Engine.this.mSportTodayDistance);
                    Engine.this.updateSportTodayDistance();
                }
            }

            public int getDataType() {
                return 2;
            }
        }

        class C01733 implements WatchDataListener {
            C01733() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 3) {
                    Engine.this.mSportTotalDistanceKm = ((Double) values[0]).doubleValue();
                    Log.d("SportEight", "onDataUpdate mSportTotalDistance = " + Engine.this.mSportTotalDistance);
                    Engine.this.updateSportTotalDistance();
                }
            }

            public int getDataType() {
                return 3;
            }
        }

        private Engine() {
            super();
            this.NUMS = new Bitmap[10];
            this.WEEKS = new Bitmap[7];
            this.mCurStepCount = 0;
            this.mTotalStepTarget = 8000;
            this.mBound = new RectF(0.0f, 0.0f, 320.0f, 320.0f);
            this.mClipPathBig = new Path();
            this.mClipPathSmall = new Path();
            this.hourFormat12 = false;
            this.mStepDataListener = new C01711();
            this.mTodayDistanceDataListener = new C01722();
            this.mTotalDistanceDataListener = new C01733();
        }

        protected void onPrepareResources(Resources resources) {
            boolean z;
            int i;
            if (getHourFormat() == 1) {
                z = true;
            } else {
                z = false;
            }
            this.hourFormat12 = z;
            this.mTotalKmIcon = decodeImage(resources, "guard/shiguanglicheng/watchface_slpt_icon_km.png");
            for (i = 0; i < 10; i++) {
                this.NUMS[i] = decodeImage(resources, String.format("guard/shiguanglicheng/watchface_slpt_number_%d.png", new Object[]{Integer.valueOf(i)}));
            }
            String weekPath = resources.getString(R.string.digital_sport_eight_week_path);
            for (i = 0; i < 7; i++) {
                this.WEEKS[i] = decodeImage(resources, String.format(weekPath, new Object[]{Integer.valueOf(i)}));
            }
            this.minus = decodeImage(resources, "guard/shiguanglicheng/watchface_slpt_number_minus.png");
            switch (getMeasurement()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    this.unit = decodeImage(resources, resources.getString(R.string.digital_sport_eight_distance_mi_unit_path));
                    break;
                default:
                    this.unit = decodeImage(resources, resources.getString(R.string.digital_sport_eight_distance_km_unit_path));
                    break;
            }
            this.big_circle = decodeImage(resources, "guard/shiguanglicheng/watchface_slpt_big_circle.png");
            this.small_circle = decodeImage(resources, "guard/shiguanglicheng/watchface_slpt_small_circle.png");
            this.mLeftDate = resources.getDimension(R.dimen.digital_sport_eight_date_left);
            this.mLeftWeek = resources.getDimension(R.dimen.digital_sport_eight_week_left);
            if (this.hourFormat12) {
                this.mTopDate = resources.getDimension(R.dimen.digital_sport_eight_date_top_with_am);
                this.mTopWeek = resources.getDimension(R.dimen.digital_sport_eight_week_top_with_am);
            } else {
                this.mTopDate = resources.getDimension(R.dimen.digital_sport_eight_date_top);
                this.mTopWeek = resources.getDimension(R.dimen.digital_sport_eight_week_top);
            }
            this.mAmBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_eight_icon_am, null)).getBitmap();
            this.mPmBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_eight_icon_pm, null)).getBitmap();
            this.mLeftAm = resources.getDimension(R.dimen.digital_sport_eight_am_left);
            this.mTopAm = resources.getDimension(R.dimen.digital_sport_eight_am_top);
            this.mColorRed = -65536;
            this.mColorWhite = -1;
            this.mPaintHuamiFont = new TextPaint(1);
            this.mPaintHuamiFont.setTypeface(TypefaceManager.get().createFromAsset("typeface/HUAMI8CSPORT.ttf"));
            this.mPaintHuamiFont.setTextSize(163.0f);
            this.mPaintHuamiFont.setTextAlign(Align.CENTER);
            this.mGPaint = new Paint(3);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setStrokeWidth(17.0f);
            registerWatchDataListener(this.mStepDataListener);
            registerWatchDataListener(this.mTodayDistanceDataListener);
            registerWatchDataListener(this.mTotalDistanceDataListener);
        }

        private Bitmap decodeImage(Resources resources, String fileName) {
            Bitmap image = null;
            try {
                InputStream is = DigitalWatchFaceSportEight.this.getResources().getAssets().open(fileName);
                image = BitmapFactory.decodeStream(is);
                is.close();
                return image;
            } catch (IOException e) {
                e.printStackTrace();
                return image;
            }
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(DigitalWatchFaceSportEight.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                    updateStepInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void updateStepInfo() {
            if (this.mTotalStepTarget > 0) {
                this.mProgressDegreeStep = (int) (Math.min((((float) this.mCurStepCount) * 1.0f) / ((float) this.mTotalStepTarget), 1.0f) * 304.0f);
            } else {
                this.mProgressDegreeStep = 0;
            }
            this.mClipPathBig.rewind();
            this.mClipPathBig.addArc(this.mBound, 118.0f, (float) this.mProgressDegreeStep);
            this.mClipPathBig.lineTo(160.0f, 160.0f);
            this.mClipPathBig.close();
        }

        private void updateSportTodayDistance() {
            int mProgressDegreeDistance = (int) (Math.min(this.mSportTodayDistance / 3.0d, 1.0d) * 304.0d);
            this.mClipPathSmall.rewind();
            this.mClipPathSmall.addArc(this.mBound, 118.0f, (float) mProgressDegreeDistance);
            this.mClipPathSmall.lineTo(160.0f, 160.0f);
            this.mClipPathSmall.close();
        }

        protected void onMeasurementChanged(int measurement) {
            if (!(this.unit == null || this.unit.isRecycled())) {
                this.unit.recycle();
            }
            Resources resources = DigitalWatchFaceSportEight.this.getResources();
            if (resources != null) {
                switch (getMeasurement()) {
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        this.unit = decodeImage(resources, resources.getString(R.string.digital_sport_eight_distance_mi_unit_path));
                        break;
                    default:
                        this.unit = decodeImage(resources, resources.getString(R.string.digital_sport_eight_distance_km_unit_path));
                        break;
                }
            }
            updateSportTodayDistance();
            updateSportTotalDistance();
            invalidate();
        }

        private void updateSportTotalDistance() {
            this.mSportTotalDistance = (int) this.mSportTotalDistanceKm;
        }

        protected void onHourFormatChanged(int hourFormat) {
            boolean z = true;
            if (getHourFormat() != 1) {
                z = false;
            }
            this.hourFormat12 = z;
            Resources resources = DigitalWatchFaceSportEight.this.getResources();
            if (resources != null) {
                if (this.hourFormat12) {
                    this.mTopDate = resources.getDimension(R.dimen.digital_sport_eight_date_top_with_am);
                    this.mTopWeek = resources.getDimension(R.dimen.digital_sport_eight_week_top_with_am);
                } else {
                    this.mTopDate = resources.getDimension(R.dimen.digital_sport_eight_date_top);
                    this.mTopWeek = resources.getDimension(R.dimen.digital_sport_eight_week_top);
                }
            }
            invalidate();
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            canvas.drawColor(-16777216);
            this.mPaintHuamiFont.setColor(this.mColorWhite);
            canvas.drawText(Util.formatTime(hours), 160.0f, 158.0f, this.mPaintHuamiFont);
            this.mPaintHuamiFont.setColor(this.mColorRed);
            canvas.drawText(Util.formatTime(minutes), 160.0f, 280.0f, this.mPaintHuamiFont);
            if (this.hourFormat12) {
                switch (ampm) {
                    case 0:
                        canvas.drawBitmap(this.mAmBitmap, this.mLeftAm, this.mTopAm, this.mGPaint);
                        break;
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        canvas.drawBitmap(this.mPmBitmap, this.mLeftAm, this.mTopAm, this.mGPaint);
                        break;
                }
            }
            int m0 = month / 10;
            int m1 = month - (m0 * 10);
            canvas.drawBitmap(this.NUMS[m0], this.mLeftDate, this.mTopDate, this.mGPaint);
            canvas.drawBitmap(this.NUMS[m1], this.mLeftDate + 10.0f, this.mTopDate, this.mGPaint);
            canvas.drawBitmap(this.minus, this.mLeftDate + 20.0f, this.mTopDate, this.mGPaint);
            int d0 = day / 10;
            int d1 = day - (d0 * 10);
            canvas.drawBitmap(this.NUMS[d0], this.mLeftDate + 30.0f, this.mTopDate, this.mGPaint);
            canvas.drawBitmap(this.NUMS[d1], this.mLeftDate + 40.0f, this.mTopDate, this.mGPaint);
            canvas.drawBitmap(this.WEEKS[week - 1], this.mLeftWeek, this.mTopWeek, this.mGPaint);
            canvas.drawBitmap(this.mTotalKmIcon, 55.0f, 127.0f, this.mGPaint);
            String kmString = String.valueOf(this.mSportTotalDistance);
            int kmLength = kmString.length();
            int kmX = 70 - (((kmLength * 10) + 32) / 2);
            for (int i = 0; i < kmLength; i++) {
                canvas.drawBitmap(this.NUMS[kmString.charAt(i) - 48], (float) ((i * 10) + kmX), (float) 166, this.mGPaint);
            }
            if (!(this.unit == null || this.unit.isRecycled())) {
                canvas.drawBitmap(this.unit, (float) ((kmLength * 10) + kmX), (float) 166, this.mGPaint);
            }
            canvas.save(2);
            canvas.clipPath(this.mClipPathBig);
            canvas.drawBitmap(this.big_circle, 0.0f, 0.0f, this.mGPaint);
            canvas.restore();
            canvas.save(2);
            canvas.clipPath(this.mClipPathSmall);
            canvas.drawBitmap(this.small_circle, 0.0f, 0.0f, this.mGPaint);
            canvas.restore();
        }

        public void onDestroy() {
            super.onDestroy();
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportEightSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(12.0f);
        return new Engine();
    }
}
