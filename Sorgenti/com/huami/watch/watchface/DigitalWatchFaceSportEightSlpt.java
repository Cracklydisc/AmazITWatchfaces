package com.huami.watch.watchface;

import android.graphics.Typeface;
import android.text.format.DateFormat;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.arc.SlptTodayDistanceArcAnglePicView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.sport.SlptTotalDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptTotalDistanceLView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalWatchFaceSportEightSlpt extends AbstractSlptClock {
    private String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    protected void initWatchFaceConfig() {
    }

    protected SlptLayout createClockLayout8C() {
        int i;
        byte[] km_mem;
        byte[] lowBatteryText_mem;
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptLinearLayout dateLinearLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent mounthLayout = new SlptLinearLayout();
        SlptLinearLayout dayLayout = new SlptLinearLayout();
        SlptPictureView dateSepView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptLinearLayout hourLayout = new SlptLinearLayout();
        SlptViewComponent minuteLayout = new SlptLinearLayout();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptViewComponent totalDistanceFView = new SlptTotalDistanceFView();
        SlptTotalDistanceLView totalDistanceLView = new SlptTotalDistanceLView();
        SlptPictureView totalDistanceSeqView = new SlptPictureView();
        SlptViewComponent totalDistanceLayout = new SlptLinearLayout();
        SlptViewComponent kmView = new SlptPictureView();
        SlptViewComponent kmIconView = new SlptPictureView();
        SlptPictureView lowBatteryIcon = new SlptPictureView();
        SlptViewComponent lowBatteryText = new SlptPictureView();
        SlptViewComponent stepArcPicView = new SlptTodayStepArcAnglePicView();
        SlptTodayDistanceArcAnglePicView distanceArcPicView = new SlptTodayDistanceArcAnglePicView();
        byte[][] small_num = new byte[10][];
        byte[][] week_num = new byte[7][];
        Typeface timeTypeface = TypefaceManager.get().createFromAsset("typeface/HUAMI8CSPORT.ttf");
        for (i = 0; i < 10; i++) {
            small_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/shiguanglicheng/watchface_slpt_number_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        boolean needAssets = Util.needEnAssets();
        for (i = 0; i < 7; i++) {
            String path;
            if (needAssets) {
                path = String.format("guard/shiguanglicheng/en/watchface_slpt_number_week_%d.png", new Object[]{Integer.valueOf(i)});
            } else {
                path = String.format("guard/shiguanglicheng/watchface_slpt_number_week_%d.png", new Object[]{Integer.valueOf(i)});
            }
            week_num[i] = SimpleFile.readFileFromAssets(this, path);
        }
        if (Util.getMeasurement(this) == 1) {
            km_mem = SimpleFile.readFileFromAssets(this, "guard/shiguanglicheng/en/watchface_slpt_number_km.png");
        } else {
            km_mem = SimpleFile.readFileFromAssets(this, "guard/shiguanglicheng/watchface_slpt_number_km.png");
        }
        byte[] km_icon_mem = SimpleFile.readFileFromAssets(this, "guard/shiguanglicheng/watchface_slpt_icon_km.png");
        byte[] dateSeq_mem = SimpleFile.readFileFromAssets(this, "guard/shiguanglicheng/watchface_slpt_number_minus.png");
        byte[] lowBatteryIcon_mem = SimpleFile.readFileFromAssets(this, "guard/watchface_slpt_icon_low_power.png");
        if (Util.needEnAssets()) {
            lowBatteryText_mem = SimpleFile.readFileFromAssets(this, "guard/en/watchface_slpt_text_low_power.png");
        } else {
            lowBatteryText_mem = SimpleFile.readFileFromAssets(this, "guard/watchface_slpt_text_low_power.png");
        }
        byte[] stepArcPicView_mem = SimpleFile.readFileFromAssets(this, "guard/shiguanglicheng/watchface_slpt_big_circle.png");
        byte[] distanceArcPicView_mem = SimpleFile.readFileFromAssets(this, "guard/shiguanglicheng/watchface_slpt_small_circle.png");
        kmView.setImagePicture(km_mem);
        kmIconView.setImagePicture(km_icon_mem);
        dateSepView.setImagePicture(dateSeq_mem);
        lowBatteryIcon.setImagePicture(lowBatteryIcon_mem);
        lowBatteryText.setImagePicture(lowBatteryText_mem);
        stepArcPicView.setImagePicture(stepArcPicView_mem);
        distanceArcPicView.setImagePicture(distanceArcPicView_mem);
        byte[] memAM = SimpleFile.readFileFromAssets(this, "guard/malasong/AM.png");
        SlptPictureView amView = new SlptPictureView();
        amView.setImagePicture(memAM);
        SlptSportUtil.setAmBgView(amView);
        byte[] memPM = SimpleFile.readFileFromAssets(this, "guard/malasong/PM.png");
        SlptViewComponent pmView = new SlptPictureView();
        pmView.setImagePicture(memPM);
        SlptSportUtil.setPmBgView(pmView);
        totalDistanceLayout.add(totalDistanceFView);
        totalDistanceLayout.add(kmView);
        linearLayout.add(stepArcPicView);
        linearLayout.add(distanceArcPicView);
        linearLayout.add(totalDistanceLayout);
        linearLayout.add(kmIconView);
        hourLayout.add(hourHView);
        hourLayout.add(hourLView);
        minuteLayout.add(minuteHView);
        minuteLayout.add(minuteLView);
        linearLayout.add(hourLayout);
        linearLayout.add(minuteLayout);
        mounthLayout.add(monthHView);
        mounthLayout.add(monthLView);
        dayLayout.add(dayHView);
        dayLayout.add(dayLView);
        dateLinearLayout.add(mounthLayout);
        dateLinearLayout.add(dateSepView);
        dateLinearLayout.add(dayLayout);
        linearLayout.add(dateLinearLayout);
        linearLayout.add(weekView);
        linearLayout.add(lowBatteryIcon);
        linearLayout.add(lowBatteryText);
        linearLayout.background.color = -16777216;
        totalDistanceSeqView.setStringPicture(".");
        totalDistanceSeqView.id = (short) 11;
        totalDistanceSeqView.centerHorizontal = (byte) 1;
        totalDistanceSeqView.centerVertical = (byte) 1;
        totalDistanceSeqView.setTextAttr(20.0f, -1, Typeface.DEFAULT);
        totalDistanceFView.setImagePictureArray(small_num);
        totalDistanceLView.setImagePictureArray(small_num);
        totalDistanceLayout.setStart(29, 166);
        totalDistanceLayout.id = (short) 13;
        totalDistanceLayout.setRect(82, 2147483646);
        totalDistanceLayout.alignX = (byte) 2;
        kmIconView.setStart(55, 127);
        kmIconView.id = (short) 14;
        hourLayout.setTextAttrForAll(163.0f, -1, timeTypeface);
        minuteLayout.setTextAttrForAll(163.0f, -65536, timeTypeface);
        hourLayout.setStringPictureArrayForAll(this.digitalNums);
        minuteLayout.setStringPictureArrayForAll(this.digitalNums);
        hourLayout.setStart(112, 36);
        minuteLayout.setStart(112, 158);
        mounthLayout.setImagePictureArrayForAll(small_num);
        dayLayout.setImagePictureArrayForAll(small_num);
        weekView.setImagePictureArray(week_num);
        dateLinearLayout.id = (short) 15;
        weekView.setStart(224, 166);
        weekView.id = (short) 16;
        if (DateFormat.is24HourFormat(this)) {
            dateLinearLayout.setStart(224, 132);
            weekView.setStart(227, 166);
        } else {
            linearLayout.add(amView);
            linearLayout.add(pmView);
            amView.setStart(228, 119);
            pmView.setStart(228, 119);
            dateLinearLayout.setStart(226, 150);
            weekView.setStart(229, 183);
        }
        stepArcPicView.start_angle = 208;
        stepArcPicView.full_angle = 304;
        distanceArcPicView.start_angle = 208;
        distanceArcPicView.full_angle = 304;
        lowBatteryText.setStart(35, 166);
        lowBatteryText.id = (short) 17;
        lowBatteryText.show = false;
        lowBatteryIcon.setStart(46, 132);
        lowBatteryIcon.id = (short) 18;
        lowBatteryIcon.show = false;
        return linearLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
