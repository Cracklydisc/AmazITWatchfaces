package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.widget.AbsWatchFaceDataWidget;
import com.huami.watch.watchface.widget.BatteryLevelWidget;
import com.huami.watch.watchface.widget.DefaultWatchFaceDataWidget;
import com.huami.watch.watchface.widget.ImageFont;
import com.huami.watch.watchface.widget.TextDateWidget;
import com.huami.watch.watchface.widget.TimeAnalogWidget;

public class DigitalWatchFaceSportEighteen extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private Drawable mBgDrawable;
        private Path mClip;

        private Engine() {
            super();
            this.mClip = new Path();
        }

        protected void onPrepareResources(Resources resources) {
            int i;
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.mBgDrawable = resources.getDrawable(R.drawable.watchface_bg_digital_sport_eighteen, null);
            this.mBgDrawable.setBounds(0, 0, 320, 320);
            ImageFont imageFont = new ImageFont("18");
            for (i = 0; i < 10; i++) {
                String num_res = String.format("datawidget/font/18/%d.png", new Object[]{Integer.valueOf(i)});
                imageFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, num_res));
            }
            imageFont.addChar('.', Util.decodeImage(resources, "datawidget/font/18/point.png"));
            ImageFont dateFont = new ImageFont("18_1");
            for (i = 0; i < 10; i++) {
                dateFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/font/18_1/%d.png", new Object[]{Integer.valueOf(i)})));
            }
            Bitmap[] weeks = new Bitmap[7];
            for (i = 0; i < weeks.length; i++) {
                Resources resources2 = resources;
                weeks[i] = Util.decodeImage(resources2, String.format("datawidget/date_18/en/%d.png", new Object[]{Integer.valueOf(i)}));
            }
            TextDateWidget dateWidget = new TextDateWidget(resources, 210, 152, 60, 15, dateFont, weeks);
            dateWidget.setWeekPosition(0, 0);
            dateWidget.setDatePosition(43, 0);
            addWidget(dateWidget);
            final Drawable todayDistanceIcon = resources.getDrawable(R.drawable.watchface_bg_digital_sport_eighteen_run, null);
            DefaultWatchFaceDataWidget todayDistanceWidget = new DefaultWatchFaceDataWidget(resources, 127, 59, 66, 66, 2, 0, imageFont) {
                private float mProgress = 0.0f;
                private String mTodayDistance = "0";

                public void onDataUpdate(int dataType, Object... values) {
                    if (2 == dataType && values != null && values[0] != null) {
                        double todayDistance = ((Double) values[0]).doubleValue();
                        this.mTodayDistance = Util.getFormatDistance(todayDistance);
                        if (todayDistance > 0.0d) {
                            this.mProgress = (float) Math.min(todayDistance / 3.0d, 1.0d);
                        } else {
                            this.mProgress = 0.0f;
                        }
                    }
                }

                protected boolean isSupportProgress() {
                    return true;
                }

                protected String getText() {
                    return this.mTodayDistance;
                }

                protected float getProgress() {
                    return this.mProgress;
                }

                protected Drawable getIconDrawable(Resources resources) {
                    return todayDistanceIcon;
                }
            };
            BitmapDrawable progressDrawable = (BitmapDrawable) resources.getDrawable(R.drawable.watchface_bg_digital_sport_eighteen_oval, null);
            todayDistanceWidget.setProgressBitmap(progressDrawable.getBitmap());
            todayDistanceWidget.setProgressBackground(null);
            todayDistanceWidget.setIconTop(10);
            todayDistanceWidget.setPaddingToDrawable(4);
            addWidget(todayDistanceWidget);
            final Drawable stepIcon = resources.getDrawable(R.drawable.watchface_bg_digital_sport_eighteen_step, null);
            AbsWatchFaceDataWidget stepWidget = new DefaultWatchFaceDataWidget(resources, 127, 201, 66, 66, 1, 0, imageFont) {
                private float mProgress = 0.0f;
                private String mWalk = "0";

                public void onDataUpdate(int dataType, Object... values) {
                    if (1 == dataType && values != null && values[0] != null) {
                        int walk = ((Integer) values[0]).intValue();
                        int target = ((Integer) values[1]).intValue();
                        this.mWalk = String.valueOf(walk);
                        if (target > 0) {
                            this.mProgress = (((float) walk) * 1.0f) / ((float) target);
                        }
                    }
                }

                protected boolean isSupportProgress() {
                    return true;
                }

                protected String getText() {
                    return this.mWalk;
                }

                protected float getProgress() {
                    return this.mProgress;
                }

                protected Drawable getIconDrawable(Resources resources) {
                    return stepIcon;
                }
            };
            stepWidget.setProgressBitmap(progressDrawable.getBitmap());
            stepWidget.setProgressBackground(null);
            stepWidget.setIconTop(10);
            stepWidget.setPaddingToDrawable(4);
            addWidget(stepWidget);
            Bitmap[] batterys = new Bitmap[11];
            for (i = 0; i < batterys.length; i++) {
                resources2 = resources;
                batterys[i] = Util.decodeImage(resources2, String.format("guard/anyekupao/8C/power/android/%d.png", new Object[]{Integer.valueOf(i)}));
            }
            addWidget(new BatteryLevelWidget(45, 107, batterys));
            addWidget(new TimeAnalogWidget(160, 160, new Paint(3), BitmapFactory.decodeResource(resources, R.drawable.watchface_default_eighteen_hour), BitmapFactory.decodeResource(resources, R.drawable.watchface_default_eighteen_minute), BitmapFactory.decodeResource(resources, R.drawable.watchface_default_eighteen_seconds)));
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.clipPath(this.mClip);
            if (this.mBgDrawable != null) {
                this.mBgDrawable.draw(canvas);
            }
            onDrawWidgets(canvas);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportEighteenSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(160.0f, 79.0f);
        return new Engine();
    }
}
