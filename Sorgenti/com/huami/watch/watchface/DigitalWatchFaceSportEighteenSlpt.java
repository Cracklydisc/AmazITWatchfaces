package com.huami.watch.watchface;

import android.content.Context;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogSecondView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayDistanceArcAnglePicView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptBatteryView;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedHourWithMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedSecondView;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.sport.SlptTodaySportDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptTodaySportDistanceLView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalWatchFaceSportEighteenSlpt extends AbstractSlptClock {
    private int batterySize = 11;
    private SlptViewComponent lowBatteryView;
    private Context mContext;
    private boolean needRefreshSecond;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        this.needRefreshSecond = Util.needSlptRefreshSecond(this.mContext).booleanValue();
        if (this.needRefreshSecond) {
            setClockPeriodSecond(true);
        }
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this, -65536, 0);
        this.lowBatteryView.setStart(204, 120);
    }

    protected SlptLayout createClockLayout8C() {
        int i;
        byte[][] batteryMem = new byte[this.batterySize][];
        byte[][] defaultNumMem = new byte[10][];
        byte[][] dateNumMem = new byte[10][];
        byte[][] weekMem = new byte[7][];
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptLinearLayout dayLayout = new SlptLinearLayout();
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent hourWithMinuteView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent secondView = new SlptPredrawedSecondView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptBatteryView batteryView = new SlptBatteryView(this.batterySize);
        SlptViewComponent stepNumView = new SlptTodayStepNumView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptViewComponent stepArcAnglePicView = new SlptTodayStepArcAnglePicView();
        SlptViewComponent runIconView = new SlptPictureView();
        SlptViewComponent todaySportDistanceFView = new SlptTodaySportDistanceFView();
        SlptPictureView todayDistanceSeq = new SlptPictureView();
        SlptViewComponent todaySportDistanceLView = new SlptTodaySportDistanceLView();
        SlptViewComponent todayDistanceArcAnglePicView = new SlptTodayDistanceArcAnglePicView();
        SlptViewComponent todayDistanceLayout = new SlptLinearLayout();
        byte[] point = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/8C/number/point.png");
        byte[] bgMem = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/8C/watchface_bg_8c.png");
        byte[] circleMem = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/8C/step/Oval.png");
        byte[] stepIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/8C/step/step.png");
        byte[] runIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/8C/step/run.png");
        for (i = 0; i < 10; i++) {
            defaultNumMem[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/anyekupao/8C/number/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            dateNumMem[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/anyekupao/8C/date/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 7; i++) {
            weekMem[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/anyekupao/8C/week/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < this.batterySize; i++) {
            batteryMem[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/anyekupao/8C/power/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        String[] assetsMinutePath = new String[]{"guard/anyekupao/8C/timehand/minute/0.png.color_map", "guard/anyekupao/8C/timehand/minute/1.png.color_map", "guard/anyekupao/8C/timehand/minute/2.png.color_map", "guard/anyekupao/8C/timehand/minute/3.png.color_map", "guard/anyekupao/8C/timehand/minute/4.png.color_map", "guard/anyekupao/8C/timehand/minute/5.png.color_map", "guard/anyekupao/8C/timehand/minute/6.png.color_map", "guard/anyekupao/8C/timehand/minute/7.png.color_map", "guard/anyekupao/8C/timehand/minute/8.png.color_map", "guard/anyekupao/8C/timehand/minute/9.png.color_map", "guard/anyekupao/8C/timehand/minute/10.png.color_map", "guard/anyekupao/8C/timehand/minute/11.png.color_map", "guard/anyekupao/8C/timehand/minute/12.png.color_map", "guard/anyekupao/8C/timehand/minute/13.png.color_map", "guard/anyekupao/8C/timehand/minute/14.png.color_map", "guard/anyekupao/8C/timehand/minute/15.png.color_map"};
        String[] assetsHourPath = new String[]{"guard/anyekupao/8C/timehand/hour/0.png.color_map", "guard/anyekupao/8C/timehand/hour/1.png.color_map", "guard/anyekupao/8C/timehand/hour/2.png.color_map", "guard/anyekupao/8C/timehand/hour/3.png.color_map", "guard/anyekupao/8C/timehand/hour/4.png.color_map", "guard/anyekupao/8C/timehand/hour/5.png.color_map", "guard/anyekupao/8C/timehand/hour/6.png.color_map", "guard/anyekupao/8C/timehand/hour/7.png.color_map", "guard/anyekupao/8C/timehand/hour/8.png.color_map", "guard/anyekupao/8C/timehand/hour/9.png.color_map", "guard/anyekupao/8C/timehand/hour/10.png.color_map", "guard/anyekupao/8C/timehand/hour/11.png.color_map", "guard/anyekupao/8C/timehand/hour/12.png.color_map", "guard/anyekupao/8C/timehand/hour/13.png.color_map", "guard/anyekupao/8C/timehand/hour/14.png.color_map", "guard/anyekupao/8C/timehand/hour/15.png.color_map"};
        String[] assetsSecondPath = new String[]{"guard/anyekupao/8C/timehand/seconds/0.png.color_map", "guard/anyekupao/8C/timehand/seconds/1.png.color_map", "guard/anyekupao/8C/timehand/seconds/2.png.color_map", "guard/anyekupao/8C/timehand/seconds/3.png.color_map", "guard/anyekupao/8C/timehand/seconds/4.png.color_map", "guard/anyekupao/8C/timehand/seconds/5.png.color_map", "guard/anyekupao/8C/timehand/seconds/6.png.color_map", "guard/anyekupao/8C/timehand/seconds/7.png.color_map", "guard/anyekupao/8C/timehand/seconds/8.png.color_map", "guard/anyekupao/8C/timehand/seconds/9.png.color_map", "guard/anyekupao/8C/timehand/seconds/10.png.color_map", "guard/anyekupao/8C/timehand/seconds/11.png.color_map", "guard/anyekupao/8C/timehand/seconds/12.png.color_map", "guard/anyekupao/8C/timehand/seconds/13.png.color_map", "guard/anyekupao/8C/timehand/seconds/14.png.color_map", "guard/anyekupao/8C/timehand/seconds/15.png.color_map"};
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsMinutePath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsHourPath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        minuteView.setPreDrawedPicture(preDrawedPictureGroup);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourWithMinuteView.setPreDrawedPicture(preDrawedPictureGroup);
        hourWithMinuteView.setStart(0, 0);
        hourWithMinuteView.setRect(320, 320);
        if (this.needRefreshSecond) {
            preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsSecondPath);
            super.addDrawedPictureGroup8C(preDrawedPictureGroup);
            secondView.setPreDrawedPicture(preDrawedPictureGroup);
            secondView.setStart(0, 0);
            secondView.setRect(320, 320);
        }
        SlptPictureView bgView = new SlptPictureView();
        bgView.setImagePicture(bgMem);
        batteryView.setImagePictureArray(batteryMem);
        batteryView.setStart(45, 107);
        dayLayout.add(dayHView);
        dayLayout.add(dayLView);
        dayHView.padding.right = (short) 1;
        dayLayout.setImagePictureArrayForAll(dateNumMem);
        dayLayout.setStart(252, 152);
        weekView.setImagePictureArray(weekMem);
        weekView.setStart(210, 152);
        runIconView.setImagePicture(runIconMem);
        runIconView.setStart(150, 69);
        todayDistanceArcAnglePicView.setImagePicture(circleMem);
        todayDistanceArcAnglePicView.setStart(127, 59);
        todayDistanceArcAnglePicView.len_angle = 360;
        todayDistanceArcAnglePicView.start_angle = 0;
        todayDistanceArcAnglePicView.full_angle = 360;
        todayDistanceSeq.setImagePicture(point);
        SlptSportUtil.setTodayDistanceDotView(todayDistanceSeq);
        todayDistanceLayout.add(todaySportDistanceFView);
        todayDistanceLayout.add(todayDistanceSeq);
        todayDistanceLayout.add(todaySportDistanceLView);
        todayDistanceLayout.setImagePictureArrayForAll(defaultNumMem);
        todayDistanceLayout.setStart(127, 94);
        todayDistanceLayout.setRect(66, 18);
        todayDistanceLayout.alignX = (byte) 2;
        stepIconView.setImagePicture(stepIconMem);
        stepIconView.setStart(150, 211);
        stepNumView.setImagePictureArray(defaultNumMem);
        stepNumView.setStart(127, 234);
        stepNumView.setRect(66, 18);
        stepNumView.alignX = (byte) 2;
        stepArcAnglePicView.setImagePicture(circleMem);
        stepArcAnglePicView.setStart(127, 201);
        stepArcAnglePicView.len_angle = 360;
        stepArcAnglePicView.start_angle = 0;
        stepArcAnglePicView.full_angle = 360;
        frameLayout.add(bgView);
        frameLayout.add(runIconView);
        frameLayout.add(stepIconView);
        frameLayout.add(todayDistanceArcAnglePicView);
        frameLayout.add(stepArcAnglePicView);
        frameLayout.add(batteryView);
        frameLayout.add(dayLayout);
        frameLayout.add(todayDistanceLayout);
        frameLayout.add(stepNumView);
        frameLayout.add(weekView);
        frameLayout.add(hourWithMinuteView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        frameLayout.add(this.lowBatteryView);
        return frameLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        int i;
        byte[][] batteryMem = new byte[this.batterySize][];
        byte[][] defaultNumMem = new byte[10][];
        byte[][] dateNumMem = new byte[10][];
        byte[][] weekMem = new byte[7][];
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptLinearLayout dayLayout = new SlptLinearLayout();
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent hourWithMinuteView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent secondView = new SlptAnalogSecondView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptBatteryView batteryView = new SlptBatteryView(this.batterySize);
        SlptViewComponent stepNumView = new SlptTodayStepNumView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptViewComponent stepArcAnglePicView = new SlptTodayStepArcAnglePicView();
        SlptViewComponent runIconView = new SlptPictureView();
        SlptViewComponent todaySportDistanceFView = new SlptTodaySportDistanceFView();
        SlptPictureView todayDistanceSeq = new SlptPictureView();
        SlptViewComponent todaySportDistanceLView = new SlptTodaySportDistanceLView();
        SlptViewComponent todayDistanceArcAnglePicView = new SlptTodayDistanceArcAnglePicView();
        SlptViewComponent todayDistanceLayout = new SlptLinearLayout();
        for (i = 0; i < 10; i++) {
            defaultNumMem[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/anyekupao/8C/number/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            dateNumMem[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/anyekupao/8C/date/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 7; i++) {
            weekMem[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/anyekupao/8C/week/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < this.batterySize; i++) {
            batteryMem[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/anyekupao/26WC/power/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        byte[] point = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/8C/number/point.png");
        byte[] bgMem = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/26WC/watchface_bg.png");
        byte[] circleMem = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/26WC/step/Oval.png");
        byte[] stepIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/8C/step/step.png");
        byte[] runIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/8C/step/run.png");
        byte[] secondMem = SimpleFile.readFileFromAssets(this.mContext, "guard/anyekupao/26WC/seconds.png");
        String[] assetsMinutePath = new String[]{"guard/anyekupao/26WC/timehand/minute/0.png.color_map", "guard/anyekupao/26WC/timehand/minute/1.png.color_map", "guard/anyekupao/26WC/timehand/minute/2.png.color_map", "guard/anyekupao/26WC/timehand/minute/3.png.color_map", "guard/anyekupao/26WC/timehand/minute/4.png.color_map", "guard/anyekupao/26WC/timehand/minute/5.png.color_map", "guard/anyekupao/26WC/timehand/minute/6.png.color_map", "guard/anyekupao/26WC/timehand/minute/7.png.color_map", "guard/anyekupao/26WC/timehand/minute/8.png.color_map", "guard/anyekupao/26WC/timehand/minute/9.png.color_map", "guard/anyekupao/26WC/timehand/minute/10.png.color_map", "guard/anyekupao/26WC/timehand/minute/11.png.color_map", "guard/anyekupao/26WC/timehand/minute/12.png.color_map", "guard/anyekupao/26WC/timehand/minute/13.png.color_map", "guard/anyekupao/26WC/timehand/minute/14.png.color_map", "guard/anyekupao/26WC/timehand/minute/15.png.color_map"};
        String[] assetsHourPath = new String[]{"guard/anyekupao/26WC/timehand/hour/0.png.color_map", "guard/anyekupao/26WC/timehand/hour/1.png.color_map", "guard/anyekupao/26WC/timehand/hour/2.png.color_map", "guard/anyekupao/26WC/timehand/hour/3.png.color_map", "guard/anyekupao/26WC/timehand/hour/4.png.color_map", "guard/anyekupao/26WC/timehand/hour/5.png.color_map", "guard/anyekupao/26WC/timehand/hour/6.png.color_map", "guard/anyekupao/26WC/timehand/hour/7.png.color_map", "guard/anyekupao/26WC/timehand/hour/8.png.color_map", "guard/anyekupao/26WC/timehand/hour/9.png.color_map", "guard/anyekupao/26WC/timehand/hour/10.png.color_map", "guard/anyekupao/26WC/timehand/hour/11.png.color_map", "guard/anyekupao/26WC/timehand/hour/12.png.color_map", "guard/anyekupao/26WC/timehand/hour/13.png.color_map", "guard/anyekupao/26WC/timehand/hour/14.png.color_map", "guard/anyekupao/26WC/timehand/hour/15.png.color_map"};
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsMinutePath);
        super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
        preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsHourPath);
        super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
        minuteView.setPreDrawedPicture(preDrawedPictureGroup);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourWithMinuteView.setPreDrawedPicture(preDrawedPictureGroup);
        hourWithMinuteView.setStart(0, 0);
        hourWithMinuteView.setRect(320, 320);
        secondView.setImagePicture(secondMem);
        SlptPictureView bgView = new SlptPictureView();
        bgView.setImagePicture(bgMem);
        batteryView.setImagePictureArray(batteryMem);
        batteryView.setStart(45, 107);
        dayLayout.add(dayHView);
        dayLayout.add(dayLView);
        dayHView.padding.right = (short) 1;
        dayLayout.setImagePictureArrayForAll(dateNumMem);
        dayLayout.setStart(252, 152);
        weekView.setImagePictureArray(weekMem);
        weekView.setStart(210, 152);
        runIconView.setImagePicture(runIconMem);
        runIconView.setStart(150, 69);
        todayDistanceArcAnglePicView.setImagePicture(circleMem);
        todayDistanceArcAnglePicView.setStart(127, 59);
        todayDistanceArcAnglePicView.len_angle = 360;
        todayDistanceArcAnglePicView.start_angle = 0;
        todayDistanceArcAnglePicView.full_angle = 360;
        todayDistanceSeq.setImagePicture(point);
        SlptSportUtil.setTodayDistanceDotView(todayDistanceSeq);
        todayDistanceLayout.add(todaySportDistanceFView);
        todayDistanceLayout.add(todayDistanceSeq);
        todayDistanceLayout.add(todaySportDistanceLView);
        todayDistanceLayout.setImagePictureArrayForAll(defaultNumMem);
        todayDistanceLayout.setStart(127, 94);
        todayDistanceLayout.setRect(66, 18);
        todayDistanceLayout.alignX = (byte) 2;
        stepIconView.setImagePicture(stepIconMem);
        stepIconView.setStart(150, 211);
        stepNumView.setImagePictureArray(defaultNumMem);
        stepNumView.setStart(127, 234);
        stepNumView.setRect(66, 18);
        stepNumView.alignX = (byte) 2;
        stepArcAnglePicView.setImagePicture(circleMem);
        stepArcAnglePicView.setStart(127, 201);
        stepArcAnglePicView.len_angle = 360;
        stepArcAnglePicView.start_angle = 0;
        stepArcAnglePicView.full_angle = 360;
        secondView.alignX = (byte) 2;
        secondView.alignY = (byte) 2;
        secondView.rect.height = 320;
        secondView.rect.width = 320;
        secondView.descHeight = (byte) 2;
        secondView.descWidth = (byte) 2;
        frameLayout.add(bgView);
        frameLayout.add(runIconView);
        frameLayout.add(stepIconView);
        frameLayout.add(todayDistanceArcAnglePicView);
        frameLayout.add(stepArcAnglePicView);
        frameLayout.add(batteryView);
        frameLayout.add(dayLayout);
        frameLayout.add(todayDistanceLayout);
        frameLayout.add(stepNumView);
        frameLayout.add(weekView);
        frameLayout.add(hourWithMinuteView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        frameLayout.add(this.lowBatteryView);
        return frameLayout;
    }
}
