package com.huami.watch.watchface;

import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.util.WatchFaceConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.ingenic.iwds.slpt.view.arc.SlptPowerArcAnglePicView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayDistanceArcAnglePicView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptBatteryView;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedHourWithMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.util.ArrayList;

public class DigitalWatchFaceSportElevenSlpt extends AbstractSlptClock {
    private String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private byte[][] small_num = new byte[10][];
    private byte[][] small_step_num = new byte[10][];
    private byte[][] week_num = new byte[7][];

    private void init_num_mem() {
        int i;
        String path;
        for (i = 0; i < 10; i++) {
            this.small_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/chengshijinying/small_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            path = new String("guard/chengshijinying/en/week_%d.png");
        } else {
            path = new String("guard/chengshijinying/week_%d.png");
        }
        for (i = 0; i < 7; i++) {
            this.week_num[i] = SimpleFile.readFileFromAssets(this, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.small_step_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/chengshijinying/step_%d.png", new Object[]{Integer.valueOf(i)}));
        }
    }

    protected void initWatchFaceConfig() {
    }

    protected SlptLayout createClockLayout8C() {
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent slptBatteryView = new SlptBatteryView(17);
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent hourView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptViewComponent slptBgView2 = new SlptPictureView();
        SlptPowerArcAnglePicView arcPicPower = new SlptPowerArcAnglePicView();
        SlptTodayStepArcAnglePicView arcPicStep = new SlptTodayStepArcAnglePicView();
        SlptTodayDistanceArcAnglePicView arcPicDistance = new SlptTodayDistanceArcAnglePicView();
        SlptViewComponent stepNumView = new SlptTodayStepNumView();
        byte[][] battery_num = new byte[17][];
        byte[] slptBgView1Mem = null;
        linearLayout.add(slptBgView1);
        linearLayout.add(slptBgView2);
        ArrayList<DataWidgetConfig> mWidgets = null;
        WatchFaceConfig config = WatchFaceConfig.getWatchFaceConfig(getApplicationContext(), AnalogWatchFaceEleven.class.getName());
        if (config != null) {
            mWidgets = config.getDataWidgets();
        }
        if (mWidgets.size() < 1) {
            mWidgets.add(new DataWidgetConfig(3, 6, 110, 94, 0, null, null));
        }
        for (int j = 0; j < mWidgets.size(); j++) {
            SlptViewComponent widgetView = get8cDataWidget((DataWidgetConfig) mWidgets.get(j));
            if (widgetView != null) {
                linearLayout.add(widgetView);
            }
        }
        linearLayout.add(arcPicPower);
        linearLayout.add(arcPicStep);
        linearLayout.add(stepNumView);
        linearLayout.add(arcPicDistance);
        linearLayout.add(slptBatteryView);
        linearLayout.add(hourView);
        linearLayout.add(minuteView);
        init_num_mem();
        linearLayout.background.color = -16777216;
        if (config != null) {
            switch (config.getBgType()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    slptBgView1Mem = SimpleFile.readFileFromAssets(this, config.getBgPathSlpt());
                    break;
                case 4:
                    slptBgView1Mem = SimpleFile.readFile(config.getBgPathSlpt());
                    break;
            }
        }
        if (slptBgView1Mem == null) {
            slptBgView1Mem = SimpleFile.readFileFromAssets(this, "guard/com.huami.watch.watchface.AnalogWatchFaceEleven/watchface_default_eleven_bg.png");
        }
        slptBgView1.setImagePicture(slptBgView1Mem);
        slptBgView2.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/com.huami.watch.watchface.AnalogWatchFaceEleven/watchface_default_eleven_graduation.png"));
        arcPicPower.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/chengshijinying/watchface_default_oval_8c.png"));
        arcPicDistance.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/chengshijinying/watchface_default_oval_8c.png"));
        arcPicStep.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/chengshijinying/watchface_slpt_big_circle.png"));
        String[] assetsMinutePath = new String[]{"guard/chengshijinying/minute_0.png.color_map", "guard/chengshijinying/minute_1.png.color_map", "guard/chengshijinying/minute_2.png.color_map", "guard/chengshijinying/minute_3.png.color_map", "guard/chengshijinying/minute_4.png.color_map", "guard/chengshijinying/minute_5.png.color_map", "guard/chengshijinying/minute_6.png.color_map", "guard/chengshijinying/minute_7.png.color_map", "guard/chengshijinying/minute_8.png.color_map", "guard/chengshijinying/minute_9.png.color_map", "guard/chengshijinying/minute_10.png.color_map", "guard/chengshijinying/minute_11.png.color_map", "guard/chengshijinying/minute_12.png.color_map", "guard/chengshijinying/minute_13.png.color_map", "guard/chengshijinying/minute_14.png.color_map", "guard/chengshijinying/minute_15.png.color_map"};
        String[] assetsHourPath = new String[]{"guard/chengshijinying/hour_0.png.color_map", "guard/chengshijinying/hour_1.png.color_map", "guard/chengshijinying/hour_2.png.color_map", "guard/chengshijinying/hour_3.png.color_map", "guard/chengshijinying/hour_4.png.color_map", "guard/chengshijinying/hour_5.png.color_map", "guard/chengshijinying/hour_6.png.color_map", "guard/chengshijinying/hour_7.png.color_map", "guard/chengshijinying/hour_8.png.color_map", "guard/chengshijinying/hour_9.png.color_map", "guard/chengshijinying/hour_10.png.color_map", "guard/chengshijinying/hour_11.png.color_map", "guard/chengshijinying/hour_12.png.color_map", "guard/chengshijinying/hour_13.png.color_map", "guard/chengshijinying/hour_14.png.color_map", "guard/chengshijinying/hour_15.png.color_map"};
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsMinutePath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsHourPath);
        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
        minuteView.setPreDrawedPicture(preDrawedPictureGroup);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(preDrawedPictureGroup);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        for (int i = 0; i < 17; i++) {
            battery_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/chengshijinying/battery_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        slptBatteryView.setImagePictureArray(battery_num);
        stepNumView.setImagePictureArray(this.small_step_num);
        stepNumView.setStart(0, 251);
        stepNumView.setRect(320, 20);
        stepNumView.alignX = (byte) 2;
        slptBatteryView.setStart(214, 152);
        arcPicDistance.setRect(80, 80);
        arcPicDistance.setStart(53, 120);
        arcPicDistance.start_angle = 0;
        arcPicDistance.len_angle = 0;
        arcPicDistance.full_angle = 360;
        arcPicPower.setStart(187, 120);
        arcPicPower.setRect(80, 80);
        arcPicPower.start_angle = 0;
        arcPicPower.len_angle = 0;
        arcPicPower.full_angle = 360;
        arcPicStep.start_angle = 208;
        arcPicStep.full_angle = 304;
        return linearLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
