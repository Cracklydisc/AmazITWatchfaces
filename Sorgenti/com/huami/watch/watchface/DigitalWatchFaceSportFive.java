package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class DigitalWatchFaceSportFive extends AbstractWatchFace {

    private class Engine extends DigitalEngine {
        private String[] WEEKDAYS;
        private WatchDataListener mBatteryDataListener;
        private int mBatteryLevel;
        private Drawable mBatteryProgress;
        private String mBatteryString;
        private RectF mBound;
        private int mColorGreen;
        private int mColorWhiteCC;
        private int mCurStepCount;
        private float mFontSizeBattery;
        private float mFontSizeDate;
        private float mFontSizeStep;
        private float mFontSizeTime;
        private Paint mGPaint;
        private TextPaint mPaintHuamiTime2Font;
        private TextPaint mPaintHuamiTimeFont;
        private TextPaint mPaintWeek;
        private Path mPathStepBarBg;
        private Path mPathStepBarFg;
        private int mProgressDegreeStep;
        private Bitmap mStepBarBg;
        private Bitmap mStepBarFg;
        private WatchDataListener mStepDataListener;
        private Bitmap mStepIcon;
        private String mStepString;
        private int mTotalStepTarget;
        private float mXBatteryString;
        private float mXDate;
        private float mXMiddle;
        private float mXMinute;
        private float mXStepIcon;
        private float mXStepString;
        private float mXWeek;
        private float mXtHour;
        private float mYBatteryString;
        private float mYDate;
        private float mYHour;
        private float mYMiddle;
        private float mYMinute;
        private float mYStepIcon;
        private float mYStepString;
        private float mYWeek;

        class C01781 implements WatchDataListener {
            C01781() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 1) {
                    Engine.this.mCurStepCount = ((Integer) values[0]).intValue();
                    Engine.this.updateStepInfo();
                }
            }

            public int getDataType() {
                return 1;
            }
        }

        class C01792 implements WatchDataListener {
            C01792() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 10) {
                    Engine.this.updateBatteryLevel(((Integer) values[0]).intValue(), ((Integer) values[1]).intValue());
                }
            }

            public int getDataType() {
                return 10;
            }
        }

        private Engine() {
            super();
            this.mCurStepCount = -1;
            this.mTotalStepTarget = 8000;
            this.mStepString = "--";
            this.mBound = new RectF(0.0f, 0.0f, 320.0f, 320.0f);
            this.mStepDataListener = new C01781();
            this.mBatteryDataListener = new C01792();
        }

        protected void onPrepareResources(Resources resources) {
            this.mStepIcon = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_five_icon_step, null)).getBitmap();
            this.mStepBarFg = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_five_step_bar, null)).getBitmap();
            this.mStepBarBg = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_five_step_bar_bg, null)).getBitmap();
            this.mBatteryProgress = resources.getDrawable(R.drawable.digital_sport_five_battery_level);
            this.mPathStepBarFg = new Path();
            this.mPathStepBarBg = new Path();
            this.mXtHour = resources.getDimension(R.dimen.digital_sport_five_hour_x);
            this.mYHour = resources.getDimension(R.dimen.digital_sport_five_hour_y);
            this.mXMinute = resources.getDimension(R.dimen.digital_sport_five_minute_x);
            this.mYMinute = resources.getDimension(R.dimen.digital_sport_five_minute_y);
            this.mXMiddle = resources.getDimension(R.dimen.digital_sport_five_middle_x);
            this.mYMiddle = resources.getDimension(R.dimen.digital_sport_five_middle_y);
            this.mXBatteryString = resources.getDimension(R.dimen.digital_sport_five_battery_x);
            this.mYBatteryString = resources.getDimension(R.dimen.digital_sport_five_battery_y);
            this.mXDate = resources.getDimension(R.dimen.digital_sport_five_date_x);
            this.mYDate = resources.getDimension(R.dimen.digital_sport_five_date_y);
            this.mXWeek = resources.getDimension(R.dimen.digital_sport_five_week_x);
            this.mYWeek = resources.getDimension(R.dimen.digital_sport_five_week_y);
            this.mYStepIcon = resources.getDimension(R.dimen.digital_sport_five_step_icon_y);
            this.mYStepString = resources.getDimension(R.dimen.digital_sport_five_step_text_y);
            int xBatteryProgress = resources.getDimensionPixelOffset(R.dimen.digital_sport_five_battery_img_x);
            int yBatteryProgress = resources.getDimensionPixelOffset(R.dimen.digital_sport_five_battery_img_y);
            this.mBatteryProgress.setBounds(xBatteryProgress, yBatteryProgress, xBatteryProgress + resources.getDimensionPixelOffset(R.dimen.digital_sport_five_battery_img_width), yBatteryProgress + resources.getDimensionPixelOffset(R.dimen.digital_sport_five_battery_img_height));
            this.mFontSizeTime = resources.getDimension(R.dimen.digital_sport_five_time_font);
            this.mFontSizeStep = resources.getDimension(R.dimen.digital_sport_five_step_font);
            this.mFontSizeBattery = resources.getDimension(R.dimen.digital_sport_five_battery_font);
            this.mFontSizeDate = resources.getDimension(R.dimen.digital_sport_five_date_font);
            this.mColorWhiteCC = resources.getColor(R.color.digital_sport_five_white_cc);
            this.mColorGreen = resources.getColor(R.color.digital_sport_five_green);
            this.mPaintHuamiTimeFont = new TextPaint(1);
            this.mPaintHuamiTimeFont.setColor(this.mColorGreen);
            this.mPaintHuamiTimeFont.setTextSize(this.mFontSizeTime);
            this.mPaintHuamiTimeFont.setTypeface(TypefaceManager.get().createFromAsset("typeface/huamitime.ttf"));
            this.mPaintHuamiTime2Font = new TextPaint(1);
            this.mPaintHuamiTime2Font.setTypeface(TypefaceManager.get().createFromAsset("typeface/huamitime2.ttf"));
            this.mPaintWeek = new TextPaint(1);
            this.mPaintWeek.setTextSize(resources.getDimension(R.dimen.digital_sport_five_week_font));
            this.mPaintWeek.setColor(this.mColorWhiteCC);
            this.mPaintWeek.setTextAlign(Align.LEFT);
            this.mGPaint = new Paint(3);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setStrokeWidth(17.0f);
            this.WEEKDAYS = resources.getStringArray(R.array.weekdays);
            registerWatchDataListener(this.mBatteryDataListener);
            registerWatchDataListener(this.mStepDataListener);
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(DigitalWatchFaceSportFive.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                    updateStepInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void updateStepInfo() {
            if (this.mCurStepCount >= 0) {
                this.mStepString = String.valueOf(this.mCurStepCount);
            }
            float widthStepIcon = (float) this.mStepIcon.getWidth();
            this.mPaintHuamiTime2Font.setTextSize(this.mFontSizeStep);
            this.mXStepIcon = ((widthStepIcon + 4.0f) + getFontlength(this.mPaintHuamiTime2Font, this.mStepString)) * -0.5f;
            this.mXStepString = (this.mXStepIcon + 4.0f) + widthStepIcon;
            if (this.mTotalStepTarget <= 0 || this.mCurStepCount < 0) {
                this.mProgressDegreeStep = 0;
            } else {
                this.mProgressDegreeStep = ((int) ((Math.min((((float) this.mCurStepCount) * 1.0f) / ((float) this.mTotalStepTarget), 1.0f) * 316.0f) / 4.0f)) * 4;
            }
            this.mPathStepBarFg.rewind();
            this.mPathStepBarFg.addArc(this.mBound, -247.0f, (float) this.mProgressDegreeStep);
            this.mPathStepBarFg.lineTo(160.0f, 160.0f);
            this.mPathStepBarFg.close();
            this.mPathStepBarBg.rewind();
            this.mPathStepBarBg.addArc(this.mBound, 68.0f, -316.0f + ((float) this.mProgressDegreeStep));
            this.mPathStepBarBg.lineTo(160.0f, 160.0f);
            this.mPathStepBarBg.close();
        }

        private void updateBatteryLevel(int batteryLevel, int batteryMax) {
            if (batteryMax > 0) {
                float scale = Math.min((((float) batteryLevel) * 1.0f) / ((float) batteryMax), 1.0f);
                this.mBatteryString = String.valueOf((int) (100.0f * scale)) + "%";
                this.mBatteryLevel = (int) (17.0f * scale);
            } else {
                this.mBatteryString = "--%";
                this.mBatteryLevel = 0;
            }
            this.mBatteryProgress.setLevel(this.mBatteryLevel);
        }

        private float getFontlength(Paint paint, String str) {
            return paint.measureText(str);
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            canvas.drawColor(-16777216, Mode.CLEAR);
            this.mPaintHuamiTimeFont.setTextAlign(Align.RIGHT);
            canvas.drawText(Util.formatTime(hours), this.mXtHour, this.mYHour, this.mPaintHuamiTimeFont);
            this.mPaintHuamiTimeFont.setTextAlign(Align.LEFT);
            canvas.drawText(Util.formatTime(minutes), this.mXMinute, this.mYMinute, this.mPaintHuamiTimeFont);
            if (!this.mDrawTimeIndicator) {
                this.mPaintHuamiTimeFont.setTextAlign(Align.CENTER);
                canvas.drawText(":", this.mXMiddle, this.mYMiddle, this.mPaintHuamiTimeFont);
            }
            this.mPaintHuamiTime2Font.setTextAlign(Align.RIGHT);
            this.mPaintHuamiTime2Font.setTextSize(this.mFontSizeDate);
            this.mPaintHuamiTime2Font.setColor(this.mColorWhiteCC);
            canvas.drawText(Util.formatTime(month) + "-" + Util.formatTime(day), this.mXDate, this.mYDate, this.mPaintHuamiTime2Font);
            canvas.drawText(this.WEEKDAYS[week - 1], this.mXWeek, this.mYWeek, this.mPaintWeek);
            this.mBatteryProgress.draw(canvas);
            this.mPaintHuamiTime2Font.setTextAlign(Align.LEFT);
            this.mPaintHuamiTime2Font.setTextSize(this.mFontSizeBattery);
            canvas.drawText(this.mBatteryString, this.mXBatteryString, this.mYBatteryString, this.mPaintHuamiTime2Font);
            canvas.drawBitmap(this.mStepIcon, this.mXStepIcon + centerX, this.mYStepIcon, this.mGPaint);
            this.mPaintHuamiTime2Font.setColor(this.mColorGreen);
            this.mPaintHuamiTime2Font.setTextSize(this.mFontSizeStep);
            canvas.drawText(this.mStepString, this.mXStepString + centerX, this.mYStepString, this.mPaintHuamiTime2Font);
            canvas.save(2);
            canvas.clipPath(this.mPathStepBarBg, Op.INTERSECT);
            canvas.drawBitmap(this.mStepBarBg, 0.0f, 0.0f, this.mGPaint);
            canvas.restore();
            canvas.save(2);
            canvas.clipPath(this.mPathStepBarFg, Op.INTERSECT);
            canvas.drawBitmap(this.mStepBarFg, 0.0f, 0.0f, this.mGPaint);
            canvas.restore();
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportFiveSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(33.3f);
        return new Engine();
    }
}
