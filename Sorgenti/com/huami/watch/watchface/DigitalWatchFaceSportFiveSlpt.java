package com.huami.watch.watchface;

import android.content.Context;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptBatteryView;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.sport.SlptPowerNumView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalWatchFaceSportFiveSlpt extends AbstractSlptClock {
    private String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private SlptViewComponent lowBatteryView = null;
    private Context mContext;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -65536, 0);
        this.lowBatteryView.setStart(0, 54);
        this.lowBatteryView.setRect(320, this.mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
    }

    protected SlptLayout createClockLayout8C() {
        int i;
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptLinearLayout hourLayout = new SlptLinearLayout();
        SlptViewComponent minuteLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        SlptViewComponent powerLayout = new SlptLinearLayout();
        SlptPowerNumView powerNumView = new SlptPowerNumView();
        SlptViewComponent powerPercentView = new SlptPictureView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptBatteryView batteryView = new SlptBatteryView(17);
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptViewComponent stepLayout = new SlptLinearLayout();
        SlptTodayStepNumView stepNumView = new SlptTodayStepNumView();
        SlptViewComponent stepArcAnaleView = new SlptTodayStepArcAnglePicView();
        SlptViewComponent timeSeqView = new SlptPictureView();
        SlptViewComponent stepIconView = new SlptPictureView();
        byte[][] week_num = new byte[7][];
        byte[][] step_num = new byte[10][];
        byte[][] time_num = new byte[10][];
        byte[][] small_num = new byte[10][];
        byte[][] battery_num = new byte[17][];
        for (i = 0; i < 10; i++) {
            small_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/qiangjiepai/small_num_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        String str;
        if (Util.needEnAssets()) {
            str = new String("guard/qiangjiepai/en/week_%d.png");
        } else {
            str = new String("guard/qiangjiepai/week_%d.png");
        }
        for (i = 0; i < 7; i++) {
            week_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            step_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/qiangjiepai/step_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            time_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/qiangjiepai/time_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 17; i++) {
            battery_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/qiangjiepai/power_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        hourLayout.add(hourHView);
        hourLayout.add(hourLView);
        minuteLayout.add(minuteHView);
        minuteLayout.add(minuteLView);
        timeLayout.add(hourLayout);
        timeLayout.add(timeSeqView);
        timeLayout.add(minuteLayout);
        powerLayout.add(batteryView);
        powerLayout.add(powerNumView);
        powerLayout.add(powerPercentView);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        stepLayout.add(stepIconView);
        stepLayout.add(stepNumView);
        linearLayout.add(slptBgView1);
        linearLayout.add(timeLayout);
        linearLayout.add(monthLayout);
        linearLayout.add(weekView);
        linearLayout.add(stepLayout);
        linearLayout.add(stepArcAnaleView);
        linearLayout.add(powerLayout);
        linearLayout.add(this.lowBatteryView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/qiangjiepai/watchface_step_bar_bg.png"));
        stepArcAnaleView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/qiangjiepai/watchface_step_bar.png"));
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/qiangjiepai/watchface_icon_step.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/qiangjiepai/small_num_seq.png"));
        timeSeqView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/qiangjiepai/time_seq.png"));
        powerPercentView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/qiangjiepai/small_num_percent.png"));
        hourLayout.setImagePictureArrayForAll(time_num);
        minuteLayout.setImagePictureArrayForAll(time_num);
        monthLayout.setStart(98, 214);
        monthLayout.setImagePictureArrayForAll(small_num);
        powerNumView.setImagePictureArray(small_num);
        batteryView.setImagePictureArray(battery_num);
        powerNumView.setPadding(7, 0, 0, 0);
        powerLayout.setStart(0, 94);
        powerLayout.setRect(320, 20);
        powerLayout.alignX = (byte) 2;
        powerLayout.alignY = (byte) 2;
        weekView.setStart(166, 214);
        weekView.setImagePictureArray(week_num);
        stepIconView.setPadding(0, 3, 0, 0);
        stepNumView.setPadding(0, 0, 2, 0);
        stepLayout.setStart(0, 267);
        stepLayout.setRect(320, 24);
        stepNumView.setImagePictureArray(step_num);
        stepLayout.alignX = (byte) 2;
        stepArcAnaleView.start_angle = 202;
        stepArcAnaleView.full_angle = 316;
        timeLayout.setStart(0, 125);
        timeLayout.setRect(320, 78);
        timeLayout.alignX = (byte) 2;
        return linearLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
