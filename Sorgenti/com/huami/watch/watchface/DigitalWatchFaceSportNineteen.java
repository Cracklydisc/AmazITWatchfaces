package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.widget.AbsWatchFaceDataWidget;
import com.huami.watch.watchface.widget.BatteryRemindDateWidget;
import com.huami.watch.watchface.widget.ImageFont;
import com.huami.watch.watchface.widget.TextDateWidget;
import com.huami.watch.watchface.widget.TimeDigitalWidget;
import com.huami.watch.watchface.widget.WeatherDateWidget;

public class DigitalWatchFaceSportNineteen extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private Drawable mBgDrawable;
        private Path mClip;
        private Drawable totalDistanceUnit;

        private Engine() {
            super();
            this.mClip = new Path();
        }

        protected void onPrepareResources(Resources resources) {
            int i;
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.mBgDrawable = resources.getDrawable(R.drawable.watchface_bg_digital_sport_nineteen, null);
            this.mBgDrawable.setBounds(0, 0, 320, 320);
            ImageFont imageFont = new ImageFont("19");
            for (i = 0; i < 10; i++) {
                String num_res = String.format("datawidget/font/19/%d.png", new Object[]{Integer.valueOf(i)});
                imageFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, num_res));
            }
            imageFont.addChar('.', Util.decodeImage(resources, "datawidget/font/19/point.png"));
            imageFont.addChar('%', Util.decodeImage(resources, "datawidget/font/19/percent.png"));
            imageFont = new ImageFont("19_1");
            for (i = 0; i < 10; i++) {
                num_res = String.format("datawidget/font/19_1/%d.png", new Object[]{Integer.valueOf(i)});
                imageFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, num_res));
            }
            imageFont.addChar('-', Util.decodeImage(resources, "datawidget/font/19_1/minus.png"));
            imageFont.addChar('.', Util.decodeImage(resources, "datawidget/font/19_1/dot.png"));
            imageFont.addChar('/', Util.decodeImage(resources, "datawidget/font/19_1/separator.png"));
            imageFont.addChar('℃', Util.decodeImage(resources, "datawidget/font/19_1/C.png"));
            imageFont.addChar('℉', Util.decodeImage(resources, "datawidget/font/19_1/F.png"));
            ImageFont timeFont = new ImageFont("time19");
            for (i = 0; i < 10; i++) {
                timeFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/font/time19/%d.png", new Object[]{Integer.valueOf(i)})));
            }
            timeFont.addChar(':', Util.decodeImage(resources, "datawidget/font/time19/colon.png"));
            timeFont.addChar('A', Util.decodeImage(resources, "datawidget/font/time19/A.png"));
            timeFont.addChar('P', Util.decodeImage(resources, "datawidget/font/time19/P.png"));
            timeFont.addChar('M', Util.decodeImage(resources, "datawidget/font/time19/M.png"));
            TimeDigitalWidget timeWidget = new TimeDigitalWidget(0, 62, 59, 196, 72, timeFont, 0, 0, 199, 49);
            timeWidget.setMinuteColor(-337634);
            addWidget(timeWidget);
            Bitmap[] weeks = new Bitmap[7];
            for (i = 0; i < weeks.length; i++) {
                Resources resources2 = resources;
                weeks[i] = Util.decodeImage(resources2, String.format("datawidget/date_19/en/%d.png", new Object[]{Integer.valueOf(i)}));
            }
            TextDateWidget dateWidget = new TextDateWidget(resources, 118, 131, 84, 24, imageFont, weeks);
            dateWidget.setDatePosition(0, 0);
            dateWidget.setWeekPosition(48, 0);
            dateWidget.setDateFormat("MM.dd");
            addWidget(dateWidget);
            Drawable todayCalIcon = resources.getDrawable(R.drawable.watchface_bg_digital_sport_nineteen_calories, null);
            final ImageFont imageFont2 = imageFont;
            final Drawable drawable = resources.getDrawable(R.drawable.watchface_bg_digital_sport_nineteen_unit_kcal, null);
            final Drawable drawable2 = todayCalIcon;
            addWidget(new AbsWatchFaceDataWidget() {
                private String mTodayCal = "0";
                private Paint f6p = new Paint(7);
                private int textX;
                private int unitX;

                public void onDataUpdate(int dataType, Object... values) {
                    if (4 == dataType && values != null && values[0] != null) {
                        this.mTodayCal = String.valueOf((int) ((Float) values[0]).floatValue());
                        int w = imageFont2.getFontWidth(this.mTodayCal);
                        this.textX = ((getWidth() - w) - drawable.getIntrinsicWidth()) / 2;
                        this.unitX = this.textX + w;
                        drawable.setBounds(this.unitX, 18, this.unitX + drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight() + 18);
                    }
                }

                public int getDataType() {
                    return 4;
                }

                public int getX() {
                    return 54;
                }

                public int getY() {
                    return 222;
                }

                public int getWidth() {
                    return 106;
                }

                public void onDraw(Canvas canvas) {
                    if (drawable2 != null) {
                        int w = drawable2.getIntrinsicWidth();
                        int x = (getWidth() - w) / 2;
                        drawable2.setBounds(x, 45, x + w, drawable2.getIntrinsicHeight() + 45);
                        drawable2.draw(canvas);
                    }
                    imageFont2.drawText(canvas, this.mTodayCal, this.textX, 18, this.f6p);
                    drawable.draw(canvas);
                }
            });
            Drawable totalDistanceIcon = resources.getDrawable(R.drawable.watchface_bg_digital_sport_nineteen_mileage, null);
            if (getMeasurement() == 1) {
                this.totalDistanceUnit = Util.decodeBitmapDrawableFromAssets(resources, "guard/superAvante/26WC/numBlack/mi.png");
            } else {
                this.totalDistanceUnit = Util.decodeBitmapDrawableFromAssets(resources, "guard/superAvante/26WC/numBlack/km.png");
            }
            imageFont2 = imageFont;
            drawable = totalDistanceIcon;
            addWidget(new AbsWatchFaceDataWidget() {
                private String mTotalDistance = "0";
                private Paint f7p = new Paint(7);
                private int textX;
                private int unitX;

                public void onDataUpdate(int dataType, Object... values) {
                    if (3 == dataType && values != null && values[0] != null) {
                        this.mTotalDistance = Util.getFormatDistance(((Double) values[0]).doubleValue());
                        int w = imageFont2.getFontWidth(this.mTotalDistance);
                        this.textX = ((getWidth() - w) - Engine.this.totalDistanceUnit.getIntrinsicWidth()) / 2;
                        this.unitX = this.textX + w;
                        Engine.this.totalDistanceUnit.setBounds(this.unitX, 18, this.unitX + Engine.this.totalDistanceUnit.getIntrinsicWidth(), Engine.this.totalDistanceUnit.getIntrinsicHeight() + 18);
                    }
                }

                public int getDataType() {
                    return 3;
                }

                public int getX() {
                    return 160;
                }

                public int getY() {
                    return 222;
                }

                public int getWidth() {
                    return 106;
                }

                public void onDraw(Canvas canvas) {
                    if (drawable != null) {
                        int w = drawable.getIntrinsicWidth();
                        int x = (getWidth() - w) / 2;
                        drawable.setBounds(x, 45, x + w, drawable.getIntrinsicHeight() + 45);
                        drawable.draw(canvas);
                    }
                    imageFont2.drawText(canvas, this.mTotalDistance, this.textX, 18, this.f7p);
                    Engine.this.totalDistanceUnit.draw(canvas);
                }
            });
            imageFont = new ImageFont("19_2");
            for (i = 0; i < 10; i++) {
                num_res = String.format("datawidget/font/19_2/%d.png", new Object[]{Integer.valueOf(i)});
                imageFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, num_res));
            }
            imageFont.addChar('|', Util.decodeImage(resources, "datawidget/font/19_2/line.png"));
            Drawable stepIcon = resources.getDrawable(R.drawable.watchface_bg_digital_sport_nineteen_step, null);
            stepIcon.setBounds(19, 18, stepIcon.getIntrinsicWidth() + 19, stepIcon.getIntrinsicHeight() + 18);
            imageFont2 = imageFont;
            drawable = stepIcon;
            addWidget(new AbsWatchFaceDataWidget() {
                private PorterDuffColorFilter cf = new PorterDuffColorFilter(-9437268, Mode.SRC_ATOP);
                private int lineRight;
                private Paint f8p = new Paint(7);
                private String step = "0";
                private int stepRight;
                private String target = "2000";
                private int targetRight = 180;
                private int f9y = 18;

                public void onDataUpdate(int dataType, Object... values) {
                    if (1 == dataType && values != null && values[0] != null) {
                        this.step = String.valueOf(((Integer) values[0]).intValue());
                        this.target = String.valueOf(((Integer) values[1]).intValue());
                        this.lineRight = (this.targetRight - imageFont2.getFontWidth(this.target)) - 3;
                        this.stepRight = (this.lineRight - imageFont2.getCharWidth('|')) - 3;
                    }
                }

                public int getDataType() {
                    return 1;
                }

                public int getX() {
                    return 20;
                }

                public int getY() {
                    return 160;
                }

                public void onDraw(Canvas canvas) {
                    if (drawable != null) {
                        drawable.draw(canvas);
                    }
                    this.f8p.setTextAlign(Align.RIGHT);
                    this.f8p.setColorFilter(this.cf);
                    imageFont2.drawText(canvas, this.target, this.targetRight, this.f9y, this.f8p);
                    imageFont2.drawText(canvas, String.valueOf('|'), this.lineRight, this.f9y, this.f8p);
                    this.f8p.setColorFilter(null);
                    imageFont2.drawText(canvas, this.step, this.stepRight, this.f9y, this.f8p);
                }
            });
            AbsWatchFaceDataWidget weatherWidget = new WeatherDateWidget(resources, 224, 162, 19, imageFont);
            weatherWidget.setIconTop(0);
            weatherWidget.setPaddingToDrawable(6);
            weatherWidget.setProgressBackground(null);
            addWidget(weatherWidget);
            AbsWatchFaceDataWidget batteryWidget = new BatteryRemindDateWidget(DigitalWatchFaceSportNineteen.this.getResources(), 99, 14, 1, imageFont) {
                protected boolean isSupportProgress() {
                    return false;
                }

                protected Drawable[] loadLevelDrawables(int model) {
                    String ICON_PATH = "guard/superAvante/power/%d.png";
                    Drawable[] levels = new Drawable[11];
                    for (int i = 0; i < 11; i++) {
                        levels[i] = Util.decodeBitmapDrawableFromAssets(this.resources, String.format(ICON_PATH, new Object[]{Integer.valueOf(i)}));
                    }
                    return levels;
                }
            };
            batteryWidget.setProgressBackground(null);
            batteryWidget.setPaddingToDrawable(1);
            addWidget(batteryWidget);
        }

        protected void onMeasurementChanged(int measurement) {
            if (measurement == 1) {
                this.totalDistanceUnit = Util.decodeBitmapDrawableFromAssets(DigitalWatchFaceSportNineteen.this.getResources(), "guard/superAvante/26WC/numBlack/mi.png");
            } else {
                this.totalDistanceUnit = Util.decodeBitmapDrawableFromAssets(DigitalWatchFaceSportNineteen.this.getResources(), "guard/superAvante/26WC/numBlack/km.png");
            }
            invalidate();
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.clipPath(this.mClip);
            if (this.mBgDrawable != null) {
                this.mBgDrawable.draw(canvas);
            }
            onDrawWidgets(canvas);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportNineteenSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(23.0f);
        return new Engine();
    }
}
