package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.recyclerview.C0051R;
import android.text.TextPaint;
import android.util.Log;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class DigitalWatchFaceSportOne extends AbstractWatchFace {

    private class Engine extends DigitalEngine {
        private Bitmap KM;
        private Bitmap MI;
        private Bitmap[] WEEKS;
        private boolean hourFormat12;
        private Bitmap mAmBitmap;
        private Bitmap mBackgroundBitmap;
        private WatchDataListener mBatteryDataListener;
        private RectF mBound;
        private float mCenterXBatteryLevel;
        private float mCenterXDate;
        private float mCenterXTodayAnchor;
        private float mCenterXTotalAnchor;
        private float mCenterYBatteryLevel;
        private float mCenterYDate;
        private float mCenterYKMAnchor;
        private float mCenterYStepCount;
        private int mColorBlue;
        private int mColorProgressBg;
        private int mColorProgressFg;
        private int mColorRed;
        private int mColorWhite;
        private int mCurStepCount;
        private float mFontSizeBattery;
        private float mFontSizeDate;
        private float mFontSizeHour;
        private float mFontSizeKm;
        private float mFontSizeMinute;
        private float mFontSizeSecond;
        private float mFontSizeStep;
        private Paint mGPaint;
        private float mLeftAm;
        private float mLeftMinute;
        private float mLeftSecond;
        private float mLeftTodayIcon;
        private float mLeftTodayText;
        private float mLeftTodayUnit;
        private float mLeftTotalIcon;
        private float mLeftTotalText;
        private float mLeftTotalUnit;
        private float mLeftWeek;
        private float mOffsetYKMAnchor;
        private float mOffsetYUnit;
        private TextPaint mPaintHuamiFont;
        private Bitmap mPmBitmap;
        private int mProgressBattery;
        private float mProgressDegreeBattery;
        private float mProgressDegreeStep;
        private float mRightHour;
        private double mSportTodayDistance;
        private String mSportTodayString;
        private double mSportTotalDistance;
        private String mSportTotalString;
        private WatchDataListener mStepDataListener;
        private String mStepString;
        private WatchDataListener mTodayDistanceDataListener;
        private Bitmap mTodayKmIcon;
        private float mTopAm;
        private float mTopHour;
        private float mTopMinute;
        private float mTopSecond;
        private float mTopTodayIcon;
        private float mTopTotalIcon;
        private float mTopWeek;
        private WatchDataListener mTotalDistanceDataListener;
        private Bitmap mTotalKmIcon;
        private int mTotalStepTarget;
        private float mUnitWidth;

        class C01861 implements WatchDataListener {
            C01861() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 1) {
                    Engine.this.mCurStepCount = ((Integer) values[0]).intValue();
                    Engine.this.updateStepInfo();
                }
            }

            public int getDataType() {
                return 1;
            }
        }

        class C01872 implements WatchDataListener {
            C01872() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 10) {
                    Engine.this.updateBatteryLevel(((Integer) values[0]).intValue(), ((Integer) values[1]).intValue());
                }
            }

            public int getDataType() {
                return 10;
            }
        }

        class C01883 implements WatchDataListener {
            C01883() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 2) {
                    Engine.this.mSportTodayDistance = ((Double) values[0]).doubleValue();
                    Log.d("SportOne", "onDataUpdate mSportTodayDistance = " + Engine.this.mSportTodayDistance);
                    Engine.this.updateSportTodayDistance();
                }
            }

            public int getDataType() {
                return 2;
            }
        }

        class C01894 implements WatchDataListener {
            C01894() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 3) {
                    Engine.this.mSportTotalDistance = ((Double) values[0]).doubleValue();
                    Log.d("SportOne", "onDataUpdate mSportTotalDistance = " + Engine.this.mSportTotalDistance);
                    Engine.this.updateSportTotalDistance();
                }
            }

            public int getDataType() {
                return 3;
            }
        }

        private Engine() {
            super();
            this.mCurStepCount = 0;
            this.mTotalStepTarget = 8000;
            this.mStepString = "";
            this.mBound = new RectF();
            this.WEEKS = new Bitmap[7];
            this.mSportTotalDistance = -1.0d;
            this.mSportTodayDistance = -1.0d;
            this.mSportTotalString = "--" + getDistanceUnit();
            this.mSportTodayString = "--";
            this.hourFormat12 = false;
            this.mStepDataListener = new C01861();
            this.mBatteryDataListener = new C01872();
            this.mTodayDistanceDataListener = new C01883();
            this.mTotalDistanceDataListener = new C01894();
        }

        private Bitmap getDistanceUnit() {
            switch (getMeasurement()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    return this.MI;
                default:
                    return this.KM;
            }
        }

        protected void onPrepareResources(Resources resources) {
            boolean z;
            if (getHourFormat() == 1) {
                z = true;
            } else {
                z = false;
            }
            this.hourFormat12 = z;
            this.mBackgroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_bg_digital_sport_one, null)).getBitmap();
            this.mTotalKmIcon = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_icon_km, null)).getBitmap();
            this.mTodayKmIcon = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_icon_run, null)).getBitmap();
            this.mAmBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_one_icon_am, null)).getBitmap();
            this.mPmBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_one_icon_pm, null)).getBitmap();
            this.KM = Util.decodeImage(resources, "guard/malasong/small_km.png");
            this.MI = Util.decodeImage(resources, "guard/malasong/en/small_km.png");
            if (this.KM != null) {
                this.mUnitWidth = (float) this.KM.getWidth();
            }
            this.mRightHour = resources.getDimension(R.dimen.digital_sport_one_hour_right);
            this.mTopHour = resources.getDimension(R.dimen.digital_sport_one_hour_top);
            this.mLeftMinute = resources.getDimension(R.dimen.digital_sport_one_minute_left);
            this.mTopMinute = resources.getDimension(R.dimen.digital_sport_one_minute_top);
            this.mLeftSecond = resources.getDimension(R.dimen.digital_sport_one_second_left);
            if (this.hourFormat12) {
                this.mTopSecond = resources.getDimension(R.dimen.digital_sport_one_second_with_am_top);
            } else {
                this.mTopSecond = resources.getDimension(R.dimen.digital_sport_one_second_top);
            }
            this.mCenterYStepCount = resources.getDimension(R.dimen.digital_sport_one_step_centery);
            this.mCenterXBatteryLevel = resources.getDimension(R.dimen.digital_sport_one_battery_centerx);
            this.mCenterYBatteryLevel = resources.getDimension(R.dimen.digital_sport_one_battery_centery);
            this.mCenterXDate = resources.getDimension(R.dimen.digital_sport_one_date_centerx);
            this.mCenterYDate = resources.getDimension(R.dimen.digital_sport_one_date_centery);
            this.mLeftWeek = resources.getDimension(R.dimen.digital_sport_one_week_x);
            this.mTopWeek = resources.getDimension(R.dimen.digital_sport_one_week_y);
            this.mCenterXTotalAnchor = resources.getDimension(R.dimen.digital_sport_one_total_km_centerx);
            this.mCenterXTodayAnchor = resources.getDimension(R.dimen.digital_sport_one_today_km_centerx);
            this.mCenterYKMAnchor = resources.getDimension(R.dimen.digital_sport_one_km_centery);
            this.mOffsetYKMAnchor = resources.getDimension(R.dimen.digital_sport_one_km_text_offsety);
            this.mOffsetYUnit = resources.getDimension(R.dimen.digital_sport_one_unit_text_offsety);
            this.mLeftAm = resources.getDimension(R.dimen.digital_sport_one_am_left);
            this.mTopAm = resources.getDimension(R.dimen.digital_sport_one_am_top);
            this.mFontSizeHour = resources.getDimension(R.dimen.digital_sport_font_hour_size);
            this.mFontSizeMinute = resources.getDimension(R.dimen.digital_sport_font_minute_size);
            this.mFontSizeSecond = resources.getDimension(R.dimen.digital_sport_font_second_size);
            this.mFontSizeStep = resources.getDimension(R.dimen.digital_sport_font_step_size);
            this.mFontSizeBattery = resources.getDimension(R.dimen.digital_sport_font_battery_size);
            this.mFontSizeDate = resources.getDimension(R.dimen.digital_sport_font_date_size);
            this.mFontSizeKm = resources.getDimension(R.dimen.digital_sport_font_km_size);
            this.mColorRed = resources.getColor(R.color.digital_sport_one_red);
            this.mColorBlue = resources.getColor(R.color.digital_sport_one_blue);
            this.mColorWhite = resources.getColor(R.color.digital_sport_one_white);
            this.mColorProgressFg = resources.getColor(R.color.digital_sport_one_progress_fg);
            this.mColorProgressBg = resources.getColor(R.color.digital_sport_one_progress_bg);
            this.mPaintHuamiFont = new TextPaint(1);
            this.mPaintHuamiFont.setTypeface(TypefaceManager.get().createFromAsset("typeface/huamifont.ttf"));
            this.mGPaint = new Paint(3);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setStrokeWidth(17.0f);
            String weekPath = resources.getString(R.string.digital_sport_one_week_path);
            for (int i = 0; i < 7; i++) {
                this.WEEKS[i] = Util.decodeImage(resources, String.format(weekPath, new Object[]{Integer.valueOf(i)}));
            }
            registerWatchDataListener(this.mBatteryDataListener);
            registerWatchDataListener(this.mStepDataListener);
            registerWatchDataListener(this.mTodayDistanceDataListener);
            registerWatchDataListener(this.mTotalDistanceDataListener);
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(DigitalWatchFaceSportOne.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                    updateStepInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void updateStepInfo() {
            this.mStepString = this.mCurStepCount + " / " + this.mTotalStepTarget;
            if (this.mTotalStepTarget > 0) {
                this.mProgressDegreeStep = Math.min((((float) this.mCurStepCount) * 1.0f) / ((float) this.mTotalStepTarget), 1.0f) * -70.0f;
            } else {
                this.mProgressDegreeStep = 0.0f;
            }
        }

        private void updateBatteryLevel(int batteryLevel, int batteryMax) {
            if (batteryMax > 0) {
                float scale = Math.min((((float) batteryLevel) * 1.0f) / ((float) batteryMax), 1.0f);
                this.mProgressBattery = (int) (100.0f * scale);
                this.mProgressDegreeBattery = 70.0f * scale;
                return;
            }
            this.mProgressBattery = 0;
            this.mProgressDegreeBattery = 0.0f;
        }

        private void updateSportTodayDistance() {
            this.mPaintHuamiFont.setTextSize(this.mFontSizeKm);
            this.mSportTodayString = Util.getFormatDistance(this.mSportTodayDistance);
            float textWidth = getFontlength(this.mPaintHuamiFont, this.mSportTodayString);
            this.mLeftTodayIcon = this.mCenterXTodayAnchor - (((((float) this.mTodayKmIcon.getWidth()) + textWidth) + this.mUnitWidth) * 0.5f);
            this.mTopTodayIcon = this.mCenterYKMAnchor - (((float) this.mTodayKmIcon.getHeight()) * 0.5f);
            this.mLeftTodayText = this.mLeftTodayIcon + ((float) this.mTodayKmIcon.getWidth());
            this.mLeftTodayUnit = this.mLeftTodayText + textWidth;
        }

        private void updateSportTotalDistance() {
            this.mSportTotalString = Util.getFormatDistance(this.mSportTotalDistance);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeKm);
            float textWidth = getFontlength(this.mPaintHuamiFont, this.mSportTotalString);
            this.mLeftTotalIcon = this.mCenterXTotalAnchor - (((((float) this.mTotalKmIcon.getWidth()) + textWidth) + this.mUnitWidth) * 0.5f);
            this.mTopTotalIcon = this.mCenterYKMAnchor - (((float) this.mTotalKmIcon.getHeight()) * 0.5f);
            this.mLeftTotalText = this.mLeftTotalIcon + ((float) this.mTotalKmIcon.getWidth());
            this.mLeftTotalUnit = this.mLeftTotalText + textWidth;
        }

        protected void onMeasurementChanged(int measurement) {
            updateSportTodayDistance();
            updateSportTotalDistance();
            invalidate();
        }

        protected void onHourFormatChanged(int hourFormat) {
            boolean z = true;
            if (getHourFormat() != 1) {
                z = false;
            }
            this.hourFormat12 = z;
            Resources resources = DigitalWatchFaceSportOne.this.getResources();
            if (resources != null) {
                if (this.hourFormat12) {
                    this.mTopSecond = resources.getDimension(R.dimen.digital_sport_one_second_with_am_top);
                } else {
                    this.mTopSecond = resources.getDimension(R.dimen.digital_sport_one_second_top);
                }
            }
            invalidate();
        }

        private float getFontlength(Paint paint, String str) {
            return paint.measureText(str);
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            this.mBound.set(0.0f, 0.0f, width, height);
            canvas.drawBitmap(this.mBackgroundBitmap, null, this.mBound, this.mGPaint);
            this.mPaintHuamiFont.setColor(this.mColorWhite);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeHour);
            this.mPaintHuamiFont.setTextAlign(Align.RIGHT);
            canvas.drawText(Util.formatTime(hours), this.mRightHour, this.mTopHour, this.mPaintHuamiFont);
            this.mPaintHuamiFont.setColor(this.mColorRed);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeMinute);
            this.mPaintHuamiFont.setTextAlign(Align.LEFT);
            canvas.drawText(Util.formatTime(minutes), this.mLeftMinute, this.mTopMinute, this.mPaintHuamiFont);
            this.mPaintHuamiFont.setColor(this.mColorWhite);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeSecond);
            canvas.drawText(Util.formatTime(seconds), this.mLeftSecond, this.mTopSecond, this.mPaintHuamiFont);
            if (this.hourFormat12) {
                switch (ampm) {
                    case 0:
                        canvas.drawBitmap(this.mAmBitmap, this.mLeftAm, this.mTopAm, this.mGPaint);
                        break;
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        canvas.drawBitmap(this.mPmBitmap, this.mLeftAm, this.mTopAm, this.mGPaint);
                        break;
                }
            }
            this.mPaintHuamiFont.setTextAlign(Align.CENTER);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeDate);
            canvas.drawText(Util.formatTime(month) + "-" + Util.formatTime(day), this.mCenterXDate, this.mCenterYDate, this.mPaintHuamiFont);
            if (!(this.WEEKS[week - 1] == null || this.WEEKS[week - 1].isRecycled())) {
                canvas.drawBitmap(this.WEEKS[week - 1], this.mLeftWeek, this.mTopWeek, this.mGPaint);
            }
            this.mPaintHuamiFont.setTextAlign(Align.LEFT);
            Bitmap unitBmp = getDistanceUnit();
            this.mPaintHuamiFont.setTextSize(this.mFontSizeKm);
            canvas.drawBitmap(this.mTotalKmIcon, this.mLeftTotalIcon, this.mTopTotalIcon, this.mGPaint);
            canvas.drawText(this.mSportTotalString, this.mLeftTotalText, this.mCenterYKMAnchor + this.mOffsetYKMAnchor, this.mPaintHuamiFont);
            if (unitBmp != null) {
                canvas.drawBitmap(unitBmp, this.mLeftTotalUnit, this.mCenterYKMAnchor + this.mOffsetYUnit, this.mGPaint);
            }
            canvas.drawBitmap(this.mTodayKmIcon, this.mLeftTodayIcon, this.mTopTodayIcon, this.mGPaint);
            canvas.drawText(this.mSportTodayString, this.mLeftTodayText, this.mCenterYKMAnchor + this.mOffsetYKMAnchor, this.mPaintHuamiFont);
            if (unitBmp != null) {
                canvas.drawBitmap(unitBmp, this.mLeftTodayUnit, this.mCenterYKMAnchor + this.mOffsetYUnit, this.mGPaint);
            }
            this.mPaintHuamiFont.setTextAlign(Align.CENTER);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeStep);
            canvas.drawText(this.mStepString, 0.5f * width, this.mCenterYStepCount, this.mPaintHuamiFont);
            this.mBound.set(centerX - 119.0f, centerY - 119.0f, 119.0f + centerX, 119.0f + centerY);
            this.mGPaint.setColor(this.mColorProgressBg);
            canvas.drawArc(this.mBound, 125.0f, -70.0f, false, this.mGPaint);
            canvas.drawArc(this.mBound, -125.0f, 70.0f, false, this.mGPaint);
            this.mGPaint.setColor(this.mColorProgressFg);
            canvas.drawArc(this.mBound, 125.0f, this.mProgressDegreeStep, false, this.mGPaint);
            canvas.drawArc(this.mBound, -125.0f, this.mProgressDegreeBattery, false, this.mGPaint);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeBattery);
            this.mPaintHuamiFont.setColor(this.mColorBlue);
            canvas.drawText(String.valueOf(this.mProgressBattery), this.mCenterXBatteryLevel, this.mCenterYBatteryLevel, this.mPaintHuamiFont);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportOneSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(37.3f);
        return new Engine();
    }
}
