package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import android.text.TextPaint;
import android.util.Log;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class DigitalWatchFaceSportSeven extends AbstractWatchFace {

    private class Engine extends DigitalEngine {
        private String[] WEEKDAYS;
        private Bitmap mBackgroundBitmap;
        private RectF mBound;
        private float mCenterXDate;
        private float mCenterXMiddle;
        private float mCenterYDate;
        private float mCenterYMiddle;
        private final Path mClipPath;
        private int mColorBlack;
        private int mColorBlack7A;
        private int mColorBlue;
        private int mColorWhite;
        private int mColorWhite4D;
        private int mColorWhite7A;
        private int mCurStepCount;
        private String mCurrentStepString;
        private float mFontSizeDate;
        private float mFontSizeHour;
        private float mFontSizeKm;
        private float mFontSizeMiddle;
        private float mFontSizeMinute;
        private float mFontSizeSecond;
        private float mFontSizeStep;
        private Paint mGPaint;
        private float mLeftSecond;
        private TextPaint mPaintMantekaFont;
        private TextPaint mPaintTekoFont;
        private TextPaint mPaintWeek;
        private int mProgressStepLevel;
        private double mSportTotalDistance;
        private String mSportTotalString;
        private WatchDataListener mStepDataListener;
        private Bitmap mStepIcon;
        private Drawable mStepProgress;
        private float mTopSecond;
        private WatchDataListener mTotalDistanceDataListener;
        private Bitmap mTotalKmIcon;
        private String mTotalStepString;
        private int mTotalStepTarget;
        private float mXCurrentStepCount;
        private float mXHour;
        private float mXKmIcon;
        private float mXMinute;
        private float mXStepIcon;
        private float mXTotalKm;
        private float mXTotalStepCount;
        private float mYHour;
        private float mYKmIcon;
        private float mYMinute;
        private float mYStepCount;
        private float mYStepIcon;
        private float mYTotalKm;
        private float mYWeek;

        class C01911 implements WatchDataListener {
            C01911() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 1) {
                    Engine.this.mCurStepCount = ((Integer) values[0]).intValue();
                    Engine.this.updateStepInfo();
                }
            }

            public int getDataType() {
                return 1;
            }
        }

        class C01922 implements WatchDataListener {
            C01922() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 3) {
                    Engine.this.mSportTotalDistance = ((Double) values[0]).doubleValue();
                    Log.d("SportSeven", "onDataUpdate mSportTotalDistance = " + Engine.this.mSportTotalDistance);
                    Engine.this.updateSportTotalDistance();
                }
            }

            public int getDataType() {
                return 3;
            }
        }

        private Engine() {
            super();
            this.mCurStepCount = 0;
            this.mTotalStepTarget = 8000;
            this.mCurrentStepString = "--";
            this.mTotalStepString = "8000";
            this.mBound = new RectF();
            this.mClipPath = new Path();
            this.mSportTotalString = "--" + getDistanceUnit();
            this.mStepDataListener = new C01911();
            this.mTotalDistanceDataListener = new C01922();
        }

        private String getDistanceUnit() {
            switch (getMeasurement()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    return "mi";
                default:
                    return "km";
            }
        }

        protected void onPrepareResources(Resources resources) {
            this.mBackgroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_bg_digital_sport_seven, null)).getBitmap();
            this.mStepProgress = resources.getDrawable(R.drawable.digital_sport_seven_step_level);
            this.mStepIcon = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_seven_icon_step, null)).getBitmap();
            this.mTotalKmIcon = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_seven_icon_km, null)).getBitmap();
            this.mXHour = resources.getDimension(R.dimen.digital_sport_seven_hour_x);
            this.mYHour = resources.getDimension(R.dimen.digital_sport_seven_hour_y);
            this.mXMinute = resources.getDimension(R.dimen.digital_sport_seven_minute_x);
            this.mYMinute = resources.getDimension(R.dimen.digital_sport_seven_minute_y);
            this.mCenterXMiddle = resources.getDimension(R.dimen.digital_sport_seven_middle_x);
            this.mCenterYMiddle = resources.getDimension(R.dimen.digital_sport_seven_middle_y);
            this.mLeftSecond = resources.getDimension(R.dimen.digital_sport_seven_second_x);
            this.mTopSecond = resources.getDimension(R.dimen.digital_sport_seven_second_y);
            this.mYStepIcon = resources.getDimension(R.dimen.digital_sport_seven_step_icon_y);
            this.mYStepCount = resources.getDimension(R.dimen.digital_sport_seven_step_centery);
            this.mYKmIcon = resources.getDimension(R.dimen.digital_sport_seven_km_icon_y);
            this.mYTotalKm = resources.getDimension(R.dimen.digital_sport_seven_total_km_y);
            this.mCenterXDate = resources.getDimension(R.dimen.digital_sport_seven_date_centerx);
            this.mCenterYDate = resources.getDimension(R.dimen.digital_sport_seven_date_centery);
            this.mYWeek = resources.getDimension(R.dimen.digital_sport_seven_week_y);
            this.mClipPath.reset();
            this.mClipPath.addCircle(160.0f, 160.0f, 150.0f, Direction.CCW);
            int xStepProgress = resources.getDimensionPixelOffset(R.dimen.digital_sport_seven_step_progress_x);
            int yStepProgress = resources.getDimensionPixelOffset(R.dimen.digital_sport_seven_step_progress_y);
            this.mStepProgress.setBounds(xStepProgress, yStepProgress, xStepProgress + resources.getDimensionPixelOffset(R.dimen.digital_sport_seven_step_progress_width), yStepProgress + resources.getDimensionPixelOffset(R.dimen.digital_sport_seven_step_progress_height));
            this.mFontSizeHour = resources.getDimension(R.dimen.digital_sport_seven_time_font);
            this.mFontSizeMinute = resources.getDimension(R.dimen.digital_sport_seven_time_font);
            this.mFontSizeMiddle = resources.getDimension(R.dimen.digital_sport_seven_time_font);
            this.mFontSizeSecond = resources.getDimension(R.dimen.digital_sport_seven_second_font);
            this.mFontSizeDate = resources.getDimension(R.dimen.digital_sport_seven_date_font);
            this.mFontSizeStep = resources.getDimension(R.dimen.digital_sport_seven_step_font);
            this.mFontSizeKm = resources.getDimension(R.dimen.digital_sport_seven_km_font);
            this.mColorBlack = resources.getColor(R.color.digital_sport_seven_black);
            this.mColorBlue = resources.getColor(R.color.digital_sport_seven_blue);
            this.mColorWhite = resources.getColor(R.color.digital_sport_seven_white);
            this.mColorWhite7A = resources.getColor(R.color.digital_sport_seven_white_7a);
            this.mColorWhite4D = resources.getColor(R.color.digital_sport_seven_white_4d);
            this.mColorBlack7A = resources.getColor(R.color.digital_sport_seven_black_7a);
            this.mPaintMantekaFont = new TextPaint(1);
            this.mPaintMantekaFont.setTypeface(TypefaceManager.get().createFromAsset("typeface/manteka.ttf"));
            this.mPaintTekoFont = new TextPaint(1);
            this.mPaintTekoFont.setTypeface(TypefaceManager.get().createFromAsset("typeface/Teko-Regular.ttf"));
            this.mPaintWeek = new TextPaint(1);
            this.mPaintWeek.setColor(this.mColorWhite);
            this.mPaintWeek.setTypeface(Typeface.SANS_SERIF);
            this.mPaintWeek.setTextSize(resources.getDimension(R.dimen.digital_sport_seven_weekly_font));
            this.mPaintWeek.setTextAlign(Align.CENTER);
            this.mGPaint = new Paint(3);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setStrokeWidth(17.0f);
            this.WEEKDAYS = resources.getStringArray(R.array.weekdays);
            registerWatchDataListener(this.mStepDataListener);
            registerWatchDataListener(this.mTotalDistanceDataListener);
            updateStepInfo();
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(DigitalWatchFaceSportSeven.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                    this.mTotalStepString = String.valueOf(this.mTotalStepTarget);
                    updateStepInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void updateStepInfo() {
            this.mCurrentStepString = this.mCurStepCount + " / ";
            this.mPaintTekoFont.setTextSize(this.mFontSizeStep);
            float widthStepIcon = (float) this.mStepIcon.getWidth();
            float widthCurrentStepString = getFontlength(this.mPaintTekoFont, this.mCurrentStepString);
            this.mXStepIcon = (((widthStepIcon + 6.0f) + widthCurrentStepString) + getFontlength(this.mPaintTekoFont, this.mTotalStepString)) * -0.5f;
            this.mXCurrentStepCount = (this.mXStepIcon + 6.0f) + widthStepIcon;
            this.mXTotalStepCount = this.mXCurrentStepCount + widthCurrentStepString;
            if (this.mTotalStepTarget > 0) {
                this.mProgressStepLevel = (int) (Math.min((((float) this.mCurStepCount) * 1.0f) / ((float) this.mTotalStepTarget), 1.0f) * 17.0f);
            } else {
                this.mProgressStepLevel = 0;
            }
            this.mStepProgress.setLevel(this.mProgressStepLevel);
        }

        private void updateSportTotalDistance() {
            this.mSportTotalString = Util.getFormatDistance(this.mSportTotalDistance) + getDistanceUnit();
            float iconWidth = (float) this.mTotalKmIcon.getWidth();
            this.mPaintTekoFont.setTextSize(this.mFontSizeKm);
            this.mXKmIcon = -0.5f * ((iconWidth + 14.0f) + getFontlength(this.mPaintTekoFont, this.mSportTotalString));
            this.mXTotalKm = (this.mXKmIcon + 14.0f) + iconWidth;
        }

        protected void onMeasurementChanged(int measurement) {
            updateSportTotalDistance();
            invalidate();
        }

        private float getFontlength(Paint paint, String str) {
            return paint.measureText(str);
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            this.mBound.set(0.0f, 0.0f, width, height);
            canvas.drawBitmap(this.mBackgroundBitmap, null, this.mBound, this.mGPaint);
            this.mPaintMantekaFont.setColor(this.mColorBlack);
            this.mPaintMantekaFont.setTextSize(this.mFontSizeHour);
            this.mPaintMantekaFont.setTextAlign(Align.RIGHT);
            canvas.drawText(Util.formatTime(hours), this.mXHour, this.mYHour, this.mPaintMantekaFont);
            this.mPaintMantekaFont.setTextSize(this.mFontSizeMinute);
            this.mPaintMantekaFont.setTextAlign(Align.LEFT);
            canvas.drawText(Util.formatTime(minutes), this.mXMinute, this.mYMinute, this.mPaintMantekaFont);
            this.mPaintMantekaFont.setTextSize(this.mFontSizeSecond);
            canvas.drawText(Util.formatTime(seconds), this.mLeftSecond, this.mTopSecond, this.mPaintMantekaFont);
            if (!this.mDrawTimeIndicator) {
                this.mPaintMantekaFont.setTextSize(this.mFontSizeMiddle);
                this.mPaintMantekaFont.setTextAlign(Align.CENTER);
                canvas.drawText(":", this.mCenterXMiddle, this.mCenterYMiddle, this.mPaintMantekaFont);
            }
            this.mPaintTekoFont.setTextAlign(Align.CENTER);
            this.mPaintTekoFont.setTextSize(this.mFontSizeDate);
            this.mPaintTekoFont.setColor(this.mColorBlack7A);
            canvas.drawText(Util.formatTime(month) + "-" + Util.formatTime(day), this.mCenterXDate, this.mCenterYDate, this.mPaintTekoFont);
            canvas.drawBitmap(this.mStepIcon, this.mXStepIcon + centerX, this.mYStepIcon, this.mGPaint);
            this.mPaintTekoFont.setTextAlign(Align.LEFT);
            this.mPaintTekoFont.setTextSize(this.mFontSizeStep);
            this.mPaintTekoFont.setColor(this.mColorWhite);
            canvas.drawText(this.mCurrentStepString, this.mXCurrentStepCount + centerX, this.mYStepCount, this.mPaintTekoFont);
            this.mPaintTekoFont.setColor(this.mColorWhite7A);
            canvas.drawText(this.mTotalStepString, this.mXTotalStepCount + centerX, this.mYStepCount, this.mPaintTekoFont);
            this.mStepProgress.draw(canvas);
            canvas.drawBitmap(this.mTotalKmIcon, this.mXKmIcon + centerX, this.mYKmIcon, this.mGPaint);
            this.mPaintTekoFont.setTextSize(this.mFontSizeKm);
            this.mPaintTekoFont.setColor(this.mColorBlue);
            canvas.drawText(this.mSportTotalString, this.mXTotalKm + centerX, this.mYTotalKm, this.mPaintTekoFont);
            this.mPaintWeek.setTextAlign(Align.CENTER);
            this.mPaintWeek.setColor(this.mColorWhite);
            canvas.drawText(this.WEEKDAYS[week - 1], centerX, this.mYWeek, this.mPaintWeek);
            canvas.save(2);
            canvas.clipPath(this.mClipPath);
            this.mPaintWeek.setColor(this.mColorWhite4D);
            this.mPaintWeek.setTextAlign(Align.RIGHT);
            canvas.drawText(this.WEEKDAYS[week + -2 < 0 ? 6 : week - 2], 93.0f, this.mYWeek, this.mPaintWeek);
            this.mPaintWeek.setTextAlign(Align.LEFT);
            canvas.drawText(this.WEEKDAYS[week % 7], 232.0f, this.mYWeek, this.mPaintWeek);
            canvas.restore();
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportSevenSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(6.7f);
        return new Engine();
    }
}
