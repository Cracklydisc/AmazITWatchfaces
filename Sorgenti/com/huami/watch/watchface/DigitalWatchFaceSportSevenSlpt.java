package com.huami.watch.watchface;

import android.content.Context;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepPicGroupView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepTargetView;
import com.ingenic.iwds.slpt.view.sport.SlptTotalDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptTotalDistanceLView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalWatchFaceSportSevenSlpt extends AbstractSlptClock {
    private byte[][] date_num = new byte[10][];
    private String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private byte[][] km_num = new byte[10][];
    private SlptViewComponent lowBatteryView = null;
    private Context mContext;
    private byte[][] step_num = new byte[10][];
    private byte[][] step_process_num = new byte[18][];
    private byte[][] time_num = new byte[10][];
    private byte[][] week_num = new byte[7][];

    private void init_num_mem() {
        int i;
        String path;
        for (i = 0; i < 10; i++) {
            this.date_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/50road/date_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 18; i++) {
            this.step_process_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/50road/step_process_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            path = new String("guard/50road/en/week_%d.png");
        } else {
            path = new String("guard/50road/week_%d.png");
        }
        for (i = 0; i < 7; i++) {
            this.week_num[i] = SimpleFile.readFileFromAssets(this, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.step_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/50road/step_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.time_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/50road/time_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.km_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/50road/km_%d.png", new Object[]{Integer.valueOf(i)}));
        }
    }

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        init_num_mem();
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -65536, 0);
        this.lowBatteryView.setStart(0, 15);
        this.lowBatteryView.setRect(320, this.mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
    }

    protected SlptLayout createClockLayout8C() {
        byte[] kmTextMem;
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptLinearLayout hourLayout = new SlptLinearLayout();
        SlptLinearLayout minuteLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptViewComponent totalLayout = new SlptLinearLayout();
        SlptPictureView kmIconView = new SlptPictureView();
        SlptViewComponent totalDistanceFview = new SlptTotalDistanceFView();
        SlptViewComponent totalDisSeqView = new SlptPictureView();
        SlptViewComponent totalDistanceLView = new SlptTotalDistanceLView();
        SlptPictureView kmTextView = new SlptPictureView();
        SlptLinearLayout monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptViewComponent stepLayout = new SlptLinearLayout();
        SlptTodayStepNumView stepNumView = new SlptTodayStepNumView();
        SlptViewComponent stepSeqView = new SlptPictureView();
        SlptViewComponent stepTargetView = new SlptTodayStepTargetView();
        SlptViewComponent timeSeqView = new SlptPictureView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptViewComponent slptTodayStepPicGroupView = new SlptTodayStepPicGroupView(18);
        hourLayout.add(hourHView);
        hourLayout.add(hourLView);
        minuteLayout.add(minuteHView);
        minuteLayout.add(minuteLView);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        timeLayout.add(hourHView);
        timeLayout.add(hourLView);
        timeLayout.add(timeSeqView);
        timeLayout.add(minuteHView);
        timeLayout.add(minuteLView);
        timeLayout.add(monthHView);
        timeLayout.add(monthLView);
        timeLayout.add(monthSeqView);
        timeLayout.add(dayHView);
        timeLayout.add(dayLView);
        totalLayout.add(kmIconView);
        totalLayout.add(totalDistanceFview);
        totalLayout.add(totalDisSeqView);
        totalLayout.add(totalDistanceLView);
        totalLayout.add(kmTextView);
        stepLayout.add(stepIconView);
        stepLayout.add(stepNumView);
        stepLayout.add(stepSeqView);
        stepLayout.add(stepTargetView);
        linearLayout.add(slptBgView1);
        linearLayout.add(totalLayout);
        linearLayout.add(timeLayout);
        linearLayout.add(weekView);
        linearLayout.add(stepLayout);
        linearLayout.add(slptTodayStepPicGroupView);
        linearLayout.add(this.lowBatteryView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/watchface_bg.png"));
        kmIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/watchface_icon_km.png"));
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/watchface_icon_step.png"));
        stepSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/step_seq.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/date_seq.png"));
        timeSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/time_seq.png"));
        if (Util.getMeasurement(this) == 1) {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/50road/en/km_text.png");
        } else {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/50road/km_text.png");
        }
        kmTextView.setImagePicture(kmTextMem);
        slptTodayStepPicGroupView.setImagePictureArray(this.step_process_num);
        slptTodayStepPicGroupView.setStart(0, 205);
        totalDisSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/distance_point.png"));
        totalDisSeqView.id = (short) 11;
        hourLayout.setImagePictureArrayForAll(this.time_num);
        minuteLayout.setImagePictureArrayForAll(this.time_num);
        monthLayout.setImagePictureArrayForAll(this.date_num);
        totalDistanceFview.setImagePictureArray(this.km_num);
        totalDistanceLView.setImagePictureArray(this.km_num);
        totalLayout.setStart(47, 82);
        kmIconView.setPadding(0, 14, 0, 0);
        totalLayout.setStart(0, 266);
        totalLayout.setRect(320, 2147483646);
        totalLayout.alignX = (byte) 2;
        weekView.setStart(124, 61);
        weekView.setImagePictureArray(this.week_num);
        stepIconView.setPadding(0, 4, 0, 0);
        stepLayout.setStart(0, 228);
        stepLayout.setRect(320, 24);
        stepNumView.setImagePictureArray(this.step_num);
        stepTargetView.setImagePictureArray(this.step_num);
        stepLayout.alignX = (byte) 2;
        timeLayout.setStart(0, 117);
        timeLayout.setRect(320, 67);
        minuteLView.setPadding(0, 4, 0, 0);
        timeLayout.alignX = (byte) 2;
        timeLayout.alignY = (byte) 1;
        return linearLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        byte[] kmTextMem;
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptLinearLayout hourLayout = new SlptLinearLayout();
        SlptLinearLayout minuteLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptViewComponent totalLayout = new SlptLinearLayout();
        SlptPictureView kmIconView = new SlptPictureView();
        SlptViewComponent totalDistanceFview = new SlptTotalDistanceFView();
        SlptViewComponent totalDisSeqView = new SlptPictureView();
        SlptViewComponent totalDistanceLView = new SlptTotalDistanceLView();
        SlptPictureView kmTextView = new SlptPictureView();
        SlptLinearLayout monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptViewComponent stepLayout = new SlptLinearLayout();
        SlptTodayStepNumView stepNumView = new SlptTodayStepNumView();
        SlptViewComponent stepSeqView = new SlptPictureView();
        SlptViewComponent stepTargetView = new SlptTodayStepTargetView();
        SlptViewComponent timeSeqView = new SlptPictureView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptViewComponent slptTodayStepPicGroupView = new SlptTodayStepPicGroupView(18);
        hourLayout.add(hourHView);
        hourLayout.add(hourLView);
        minuteLayout.add(minuteHView);
        minuteLayout.add(minuteLView);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        timeLayout.add(hourHView);
        timeLayout.add(hourLView);
        timeLayout.add(timeSeqView);
        timeLayout.add(minuteHView);
        timeLayout.add(minuteLView);
        timeLayout.add(monthHView);
        timeLayout.add(monthLView);
        timeLayout.add(monthSeqView);
        timeLayout.add(dayHView);
        timeLayout.add(dayLView);
        totalLayout.add(kmIconView);
        totalLayout.add(totalDistanceFview);
        totalLayout.add(totalDisSeqView);
        totalLayout.add(totalDistanceLView);
        totalLayout.add(kmTextView);
        stepLayout.add(stepIconView);
        stepLayout.add(stepNumView);
        stepLayout.add(stepSeqView);
        stepLayout.add(stepTargetView);
        linearLayout.add(slptBgView1);
        linearLayout.add(totalLayout);
        linearLayout.add(timeLayout);
        linearLayout.add(weekView);
        linearLayout.add(stepLayout);
        linearLayout.add(slptTodayStepPicGroupView);
        linearLayout.add(this.lowBatteryView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/watchface_bg_26wc.png"));
        kmIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/watchface_icon_km.png"));
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/watchface_icon_step.png"));
        stepSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/step_seq.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/date_seq.png"));
        timeSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/time_seq.png"));
        if (Util.getMeasurement(this) == 1) {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/50road/en/km_text.png");
        } else {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/50road/km_text.png");
        }
        kmTextView.setImagePicture(kmTextMem);
        slptTodayStepPicGroupView.setImagePictureArray(this.step_process_num);
        slptTodayStepPicGroupView.setStart(0, 205);
        totalDisSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/50road/distance_point.png"));
        totalDisSeqView.id = (short) 11;
        hourLayout.setImagePictureArrayForAll(this.time_num);
        minuteLayout.setImagePictureArrayForAll(this.time_num);
        monthLayout.setImagePictureArrayForAll(this.date_num);
        totalDistanceFview.setImagePictureArray(this.km_num);
        totalDistanceLView.setImagePictureArray(this.km_num);
        totalLayout.setStart(47, 82);
        kmIconView.setPadding(0, 14, 0, 0);
        totalLayout.setStart(0, 266);
        totalLayout.setRect(320, 2147483646);
        totalLayout.alignX = (byte) 2;
        weekView.setStart(124, 61);
        weekView.setImagePictureArray(this.week_num);
        stepIconView.setPadding(0, 4, 0, 0);
        stepLayout.setStart(0, 228);
        stepLayout.setRect(320, 24);
        stepNumView.setImagePictureArray(this.step_num);
        stepTargetView.setImagePictureArray(this.step_num);
        stepLayout.alignX = (byte) 2;
        timeLayout.setStart(0, 117);
        timeLayout.setRect(320, 67);
        minuteLView.setPadding(0, 4, 0, 0);
        timeLayout.alignX = (byte) 2;
        timeLayout.alignY = (byte) 1;
        return linearLayout;
    }
}
