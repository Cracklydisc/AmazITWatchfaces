package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.widget.AbsWatchFaceDataWidget;
import com.huami.watch.watchface.widget.BatteryRemindDateWidget;
import com.huami.watch.watchface.widget.DigitDateWidget;
import com.huami.watch.watchface.widget.FatBurnTextWidget;
import com.huami.watch.watchface.widget.HeartRateTextWidget;
import com.huami.watch.watchface.widget.ImageFont;
import com.huami.watch.watchface.widget.TimeDigitalWidget;
import com.huami.watch.watchface.widget.TodayDistanceDateWidget;
import com.huami.watch.watchface.widget.TodayFloorTextWidget;
import com.huami.watch.watchface.widget.TotalDistanceTextWidget;
import com.huami.watch.watchface.widget.WalkDateWidget;
import com.huami.watch.watchface.widget.WeatherDateWidget;

public class DigitalWatchFaceSportSeventeen extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private Drawable mBgDrawable;
        private Path mClip;

        private Engine() {
            super();
            this.mClip = new Path();
        }

        protected void onPrepareResources(Resources resources) {
            int i;
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.mBgDrawable = resources.getDrawable(R.drawable.watchface_bg_digital_sport_seventeen, null);
            this.mBgDrawable.setBounds(0, 0, 320, 320);
            ImageFont timeFont = new ImageFont("17");
            for (i = 0; i < 10; i++) {
                timeFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/font/17/%d.png", new Object[]{Integer.valueOf(i)})));
            }
            timeFont.addChar(':', Util.decodeImage(resources, "datawidget/font/17/colon.png"));
            timeFont.addChar('A', Util.decodeImage(resources, "datawidget/font/17/A.png"));
            timeFont.addChar('P', Util.decodeImage(resources, "datawidget/font/17/P.png"));
            addWidget(new TimeDigitalWidget(0, 37, 117, 246, 90, timeFont, 0, 0, 246, 2));
            ImageFont imageFont = new ImageFont("17_1");
            for (i = 0; i < 10; i++) {
                String num_res = String.format("datawidget/font/17_1/%d.png", new Object[]{Integer.valueOf(i)});
                imageFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, num_res));
            }
            imageFont.addChar('/', Util.decodeImage(resources, "datawidget/font/17_1/separator.png"));
            imageFont.addChar('-', Util.decodeImage(resources, "datawidget/font/17_1/minus.png"));
            imageFont.addChar('.', Util.decodeImage(resources, "datawidget/font/17_1/point.png"));
            imageFont.addChar('%', Util.decodeImage(resources, "datawidget/font/17_1/percent.png"));
            imageFont.addChar(' ', Util.decodeImage(resources, "datawidget/font/17_1/space.png"));
            imageFont.addChar('℃', Util.decodeImage(resources, "datawidget/font/17_1/celsius.png"));
            imageFont.addChar('℉', Util.decodeImage(resources, "datawidget/font/17_1/fahrenheit.png"));
            addWidget(new DigitDateWidget(resources, 100, 99, 0, 0, 2, R.string.custom_data_widget_date_3, imageFont));
            addWidget(new DigitDateWidget(resources, 100, 60, 0, 0, 4, -1, imageFont));
            addWidget(new TodayDistanceDateWidget(DigitalWatchFaceSportSeventeen.this.getResources(), 15, 69, 17, imageFont));
            addWidget(new WalkDateWidget(DigitalWatchFaceSportSeventeen.this.getResources(), 204, 69, 17, imageFont));
            addWidget(new FatBurnTextWidget(DigitalWatchFaceSportSeventeen.this.getResources(), 64, 210, imageFont));
            addWidget(new TodayFloorTextWidget(DigitalWatchFaceSportSeventeen.this.getResources(), 256, 210, imageFont));
            addWidget(new TotalDistanceTextWidget(DigitalWatchFaceSportSeventeen.this.getResources(), 128, 268, imageFont));
            addWidget(new HeartRateTextWidget(DigitalWatchFaceSportSeventeen.this.getResources(), 177, 268, imageFont));
            WeatherDateWidget weatherWidget = new WeatherDateWidget(DigitalWatchFaceSportSeventeen.this.getResources(), 118, 188, 0, imageFont) {
                protected boolean isAlwaysShowUnit() {
                    return true;
                }
            };
            weatherWidget.setProgressBackground(null);
            weatherWidget.setPaddingToDrawable(1);
            addWidget(weatherWidget);
            AbsWatchFaceDataWidget batteryWidget = new BatteryRemindDateWidget(DigitalWatchFaceSportSeventeen.this.getResources(), 99, 9, 1, imageFont) {
                protected boolean isSupportProgress() {
                    return false;
                }

                protected Drawable[] loadLevelDrawables(int model) {
                    String ICON_PATH = "guard/xinjiyuan/battery/%d.png";
                    Drawable[] levels = new Drawable[11];
                    for (int i = 0; i < 11; i++) {
                        levels[i] = Util.decodeBitmapDrawableFromAssets(this.resources, String.format(ICON_PATH, new Object[]{Integer.valueOf(i)}));
                    }
                    return levels;
                }
            };
            batteryWidget.setProgressBackground(null);
            addWidget(batteryWidget);
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.clipPath(this.mClip);
            if (this.mBgDrawable != null) {
                this.mBgDrawable.draw(canvas);
            }
            onDrawWidgets(canvas);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportSeventeenSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(23.6f);
        return new Engine();
    }
}
