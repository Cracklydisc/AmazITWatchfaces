package com.huami.watch.watchface;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import android.support.wearable.view.SimpleAnimatorListener;
import android.text.TextPaint;
import android.util.Log;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class DigitalWatchFaceSportSix extends AbstractWatchFace {

    private class Engine extends DigitalEngine {
        private String[] WEEKDAYS;
        private Drawable batteryDrawable;
        private Bitmap mBackgroundBitmap;
        private WatchDataListener mBatteryDataListener;
        private float mBatteryTop;
        private RectF mBound;
        private float mCalCircleRadius;
        private float mCalCircleThickness;
        private Paint mCalPaint;
        private Typeface mCalStepTypeface;
        private float mCenterXBatteryLevel;
        private float mCenterXDate;
        private float mCenterXWeek;
        private float mCenterYBatteryLevel;
        private float mCenterYDate;
        private float mCenterYWeek;
        private int mColorBattery;
        private int mColorBgStepCirlce;
        private int mColorCal;
        private int mColorKmOne;
        private int mColorKmThree;
        private int mColorKmTwo;
        private int mColorStep;
        private int mColorStepCirlce;
        private int mColorTime;
        private int mColorWhite;
        private int mCurStepCount;
        private Typeface mDateBatteryTypeface;
        private float mFontSizeBattery;
        private float mFontSizeDate;
        private float mFontSizeHour;
        private float mFontSizeMinute;
        private float mFontSizeStep;
        private Paint mGPaint;
        private Bitmap mKmBitmap;
        private float mLeftMinute;
        private float mLeftSecond;
        private float mLeftTodayCalorieIcon;
        private float mLeftTodayStepIcon;
        private int mOldCurStepCount;
        private TextPaint mPaintHuamiFont;
        private TextPaint mPaintWeek;
        private int mProgressBattery;
        private float mProgressDegreeBattery;
        private float mRightHour;
        private Bitmap mSecondIcon;
        private double mSportTodayDistance;
        private String mSportTodayString;
        private ValueAnimator mStepAnimator;
        private Bitmap mStepCircleBitmap;
        private float mStepCircleRadius;
        private float mStepCircleThickness;
        private int mStepCompose;
        private WatchDataListener mStepDataListener;
        private int mStepDiff;
        private Paint mStepPaint;
        private String mStepString;
        private Typeface mTimeTypeface;
        private WatchDataListener mTodayDistanceDataListener;
        private Bitmap mTodayStepIcon;
        private float mTopCalorie;
        private float mTopHour;
        private float mTopMinute;
        private float mTopSecond;
        private float mTopStep;
        private float mTopTodayCalorieIcon;
        private float mTopTodayStepIcon;
        private Bitmap mTotalKmIcon;
        private int mTotalStepTarget;
        private Typeface mWeekTypeface;

        class C01971 implements AnimatorUpdateListener {
            C01971() {
            }

            public void onAnimationUpdate(ValueAnimator animation) {
                Engine.this.mStepCompose = (int) (((float) Engine.this.mStepDiff) * ((Float) animation.getAnimatedValue()).floatValue());
                Engine.this.postInvalidate();
            }
        }

        class C01982 extends SimpleAnimatorListener {
            C01982() {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                Engine.this.mStepCompose = 0;
                Engine.this.mOldCurStepCount = Engine.this.mCurStepCount;
            }

            public void onAnimationCancel(Animator animator) {
                Engine.this.mStepCompose = 0;
                Engine.this.mOldCurStepCount = Engine.this.mCurStepCount;
            }
        }

        class C01993 implements WatchDataListener {
            C01993() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 1) {
                    Engine.this.mCurStepCount = ((Integer) values[0]).intValue();
                    if (Engine.this.mOldCurStepCount != Engine.this.mCurStepCount) {
                        if (Engine.this.mOldCurStepCount > Engine.this.mCurStepCount) {
                            Engine.this.mOldCurStepCount = 0;
                        }
                        Engine.this.mStepAnimator.cancel();
                        Engine.this.mStepDiff = Engine.this.mCurStepCount - Engine.this.mOldCurStepCount;
                        Engine.this.mStepAnimator.start();
                    }
                    Engine.this.updateStepInfo();
                }
            }

            public int getDataType() {
                return 1;
            }
        }

        class C02004 implements WatchDataListener {
            C02004() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 2) {
                    Engine.this.mSportTodayDistance = ((Double) values[0]).doubleValue();
                    Log.d("SportSix", "onDataUpdate mSportTodayDistance = " + Engine.this.mSportTodayDistance);
                    Engine.this.updateSportTodayDistance();
                }
            }

            public int getDataType() {
                return 2;
            }
        }

        class C02015 implements WatchDataListener {
            C02015() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 10) {
                    Engine.this.updateBatteryLevel(((Integer) values[0]).intValue(), ((Integer) values[1]).intValue());
                }
            }

            public int getDataType() {
                return 10;
            }
        }

        private Engine() {
            super();
            this.mCurStepCount = -1;
            this.mOldCurStepCount = 0;
            this.mStepDiff = 0;
            this.mStepCompose = 0;
            this.mTotalStepTarget = 8000;
            this.mStepString = "--";
            this.mBound = new RectF();
            this.mSportTodayDistance = -1.0d;
            this.mSportTodayString = "--" + getDistanceUnit();
            this.mStepDataListener = new C01993();
            this.mTodayDistanceDataListener = new C02004();
            this.mBatteryDataListener = new C02015();
        }

        private String getDistanceUnit() {
            switch (getMeasurement()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    return "mi";
                default:
                    return "km";
            }
        }

        protected void onPrepareResources(Resources resources) {
            this.mBackgroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_bg_digital_sport_six, null)).getBitmap();
            this.mTotalKmIcon = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_six_icon_km, null)).getBitmap();
            this.mTodayStepIcon = ((BitmapDrawable) resources.getDrawable(R.drawable.digital_sport_6_icon_step, null)).getBitmap();
            this.mSecondIcon = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_six_icon_second, null)).getBitmap();
            this.mStepCircleBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_six_step_bar, null)).getBitmap();
            this.mKmBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_six_calorie_bar, null)).getBitmap();
            this.batteryDrawable = resources.getDrawable(R.drawable.digital_sport_six_battery_level);
            this.mRightHour = resources.getDimension(R.dimen.digital_sport_six_hour_right);
            this.mTopHour = resources.getDimension(R.dimen.digital_sport_six_hour_top);
            this.mLeftMinute = resources.getDimension(R.dimen.digital_sport_six_minute_left);
            this.mTopMinute = resources.getDimension(R.dimen.digital_sport_six_minute_top);
            this.mLeftSecond = resources.getDimension(R.dimen.digital_sport_six_second_left);
            this.mTopSecond = resources.getDimension(R.dimen.digital_sport_six_second_top);
            this.mLeftTodayCalorieIcon = resources.getDimension(R.dimen.digital_sport_six_cal_icon_left);
            this.mTopTodayCalorieIcon = resources.getDimension(R.dimen.digital_sport_six_cal_icon_top);
            this.mTopCalorie = resources.getDimension(R.dimen.digital_sport_six_cal_text_top);
            this.mLeftTodayStepIcon = resources.getDimension(R.dimen.digital_sport_six_step_icon_left);
            this.mTopTodayStepIcon = resources.getDimension(R.dimen.digital_sport_six_step_icon_top);
            this.mTopStep = resources.getDimension(R.dimen.digital_sport_six_step_text_top);
            this.mCenterXBatteryLevel = resources.getDimension(R.dimen.digital_sport_six_battery_leftx);
            this.mCenterYBatteryLevel = resources.getDimension(R.dimen.digital_sport_six_battery_topy);
            this.mBatteryTop = resources.getDimension(R.dimen.digital_sport_six_battery_font_topy);
            this.mCenterXDate = resources.getDimension(R.dimen.digital_sport_six_date_rightx);
            this.mCenterYDate = resources.getDimension(R.dimen.digital_sport_six_date_topy);
            this.mCenterXWeek = resources.getDimension(R.dimen.digital_sport_six_week_rightx);
            this.mCenterYWeek = resources.getDimension(R.dimen.digital_sport_six_week_topy);
            this.mStepCircleThickness = resources.getDimension(R.dimen.digital_sport_six_step_thickness);
            this.mStepCircleRadius = resources.getDimension(R.dimen.digital_sport_six_step_radius);
            this.mCalCircleThickness = resources.getDimension(R.dimen.digital_sport_six_cal_thickness);
            this.mCalCircleRadius = resources.getDimension(R.dimen.digital_sport_six_cal_radius);
            this.mFontSizeHour = resources.getDimension(R.dimen.digital_sport_six_font_hour_size);
            this.mFontSizeMinute = resources.getDimension(R.dimen.digital_sport_six_font_minute_size);
            this.mFontSizeStep = resources.getDimension(R.dimen.digital_sport_six_font_step_size);
            this.mFontSizeBattery = resources.getDimension(R.dimen.digital_sport_six_font_battery_size);
            this.mFontSizeDate = resources.getDimension(R.dimen.digital_sport_six_font_date_size);
            this.mColorWhite = resources.getColor(R.color.digital_sport_six_white);
            this.mColorKmOne = resources.getColor(R.color.digital_sport_six_cal_circle_color_one);
            this.mColorKmTwo = resources.getColor(R.color.digital_sport_six_cal_circle_color_two);
            this.mColorKmThree = resources.getColor(R.color.digital_sport_six_cal_circle_color_three);
            this.mColorTime = resources.getColor(R.color.digital_sport_six_time_font_color);
            this.mColorCal = resources.getColor(R.color.digital_sport_six_cal_font_color);
            this.mColorStep = resources.getColor(R.color.digital_sport_six_step_font_color);
            this.mColorBattery = resources.getColor(R.color.digital_sport_six_battery_font_color);
            this.mColorStepCirlce = resources.getColor(R.color.digital_sport_six_step_circle_color);
            this.mColorBgStepCirlce = resources.getColor(R.color.digital_sport_six_step_circle_bg_color);
            this.mPaintHuamiFont = new TextPaint(1);
            this.mTimeTypeface = TypefaceManager.get().createFromAsset("typeface/AkzidenzGroteskStd-MdCn.otf");
            this.mCalStepTypeface = TypefaceManager.get().createFromAsset("typeface/Teko-Medium.ttf");
            this.mDateBatteryTypeface = TypefaceManager.get().createFromAsset("typeface/Teko-Regular.ttf");
            this.mWeekTypeface = Typeface.create("SourceHanSans-Regular", 0);
            this.mPaintWeek = new TextPaint(1);
            this.mPaintWeek.setColor(this.mColorWhite);
            this.mPaintWeek.setTextSize(resources.getDimension(R.dimen.digital_sport_six_font_week_size));
            this.mPaintWeek.setTextAlign(Align.CENTER);
            this.mGPaint = new Paint(3);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setStrokeWidth(17.0f);
            this.mStepPaint = new Paint(7);
            this.mStepPaint.setStyle(Style.STROKE);
            this.mStepPaint.setStrokeCap(Cap.ROUND);
            this.mStepPaint.setStrokeWidth(this.mStepCircleThickness);
            this.mCalPaint = new Paint(7);
            this.mCalPaint.setStyle(Style.STROKE);
            this.mCalPaint.setStrokeCap(Cap.ROUND);
            this.mCalPaint.setStrokeWidth(this.mCalCircleThickness);
            this.mCalPaint.setShader(new SweepGradient(160.0f, 160.0f, new int[]{this.mColorKmTwo, this.mColorKmThree, this.mColorKmTwo, this.mColorKmOne, this.mColorKmTwo}, null));
            this.WEEKDAYS = resources.getStringArray(R.array.weekdays);
            this.mStepAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mStepAnimator.addUpdateListener(new C01971());
            this.mStepAnimator.addListener(new C01982());
            this.mStepAnimator.setDuration(300);
            registerWatchDataListener(this.mBatteryDataListener);
            registerWatchDataListener(this.mStepDataListener);
            registerWatchDataListener(this.mTodayDistanceDataListener);
            updateStepInfo();
        }

        private void drawStepContent(Canvas canvas, float centerX, float centerY) {
            float ratio = 0.0f;
            if (this.mCurStepCount >= 0) {
                ratio = Math.min(1.0f, (((float) this.mCurStepCount) * 1.0f) / ((float) this.mTotalStepTarget));
            }
            float degree = ratio * 360.0f;
            canvas.save();
            this.mStepPaint.setColor(this.mColorBgStepCirlce);
            canvas.drawCircle(centerX, centerY, this.mStepCircleRadius, this.mStepPaint);
            this.mStepPaint.setColor(this.mColorStepCirlce);
            canvas.drawArc(centerX - this.mStepCircleRadius, centerY - this.mStepCircleRadius, centerX + this.mStepCircleRadius, centerY + this.mStepCircleRadius, -180.0f, degree, false, this.mStepPaint);
            double angle = 3.141592653589793d - (((double) (2.0f * ratio)) * 3.141592653589793d);
            float left = (float) ((((double) centerX) + (((double) this.mStepCircleRadius) * Math.cos(angle))) - ((double) (this.mStepCircleBitmap.getWidth() / 2)));
            float top = (float) ((((double) centerY) - (((double) this.mStepCircleRadius) * Math.sin(angle))) - ((double) (this.mStepCircleBitmap.getHeight() / 2)));
            canvas.drawBitmap(this.mStepCircleBitmap, left, top, this.mGPaint);
            canvas.restore();
        }

        private void drawCalorieContent(Canvas canvas, float centerX, float centerY) {
            float degree = (float) ((this.mSportTodayDistance / 3.0d) * 360.0d);
            if (Float.compare(degree, 0.0f) != 0) {
                if (degree >= 360.0f) {
                    degree = 360.0f;
                }
                canvas.save();
                canvas.drawArc(centerX - this.mCalCircleRadius, centerY - this.mCalCircleRadius, centerX + this.mCalCircleRadius, centerY + this.mCalCircleRadius, -90.0f, degree, false, this.mCalPaint);
                double angle = Math.toRadians((double) (90.0f - degree));
                Canvas canvas2 = canvas;
                canvas2.drawBitmap(this.mKmBitmap, (float) ((((double) centerX) + (((double) this.mCalCircleRadius) * Math.cos(angle))) - ((double) (this.mKmBitmap.getWidth() / 2))), (float) ((((double) centerY) - (((double) this.mCalCircleRadius) * Math.sin(angle))) - ((double) (this.mKmBitmap.getHeight() / 2))), this.mGPaint);
                canvas.restore();
            }
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(DigitalWatchFaceSportSix.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                    updateStepInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void updateStepInfo() {
            if (this.mCurStepCount >= 0) {
                this.mStepString = String.valueOf(this.mCurStepCount);
            }
        }

        private void updateSportTodayDistance() {
            this.mSportTodayString = Util.getFormatDistance(this.mSportTodayDistance) + getDistanceUnit();
        }

        protected void onMeasurementChanged(int measurement) {
            updateSportTodayDistance();
            invalidate();
        }

        private void updateBatteryLevel(int batteryLevel, int batteryMax) {
            if (batteryMax > 0) {
                float scale = Math.min((((float) batteryLevel) * 1.0f) / ((float) batteryMax), 1.0f);
                this.mProgressBattery = (int) (100.0f * scale);
                this.mProgressDegreeBattery = 70.0f * scale;
                return;
            }
            this.mProgressBattery = 0;
            this.mProgressDegreeBattery = 0.0f;
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            this.mBound.set(0.0f, 0.0f, width, height);
            canvas.drawBitmap(this.mBackgroundBitmap, null, this.mBound, this.mGPaint);
            drawStepContent(canvas, centerX, centerY);
            this.mPaintHuamiFont.setTypeface(this.mTimeTypeface);
            this.mPaintHuamiFont.setColor(this.mColorTime);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeHour);
            this.mPaintHuamiFont.setTextAlign(Align.RIGHT);
            canvas.drawText(Util.formatTime(hours), this.mRightHour, this.mTopHour, this.mPaintHuamiFont);
            if (!this.mDrawTimeIndicator) {
                canvas.drawBitmap(this.mSecondIcon, this.mLeftSecond, this.mTopSecond, this.mGPaint);
            }
            this.mPaintHuamiFont.setColor(this.mColorTime);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeMinute);
            this.mPaintHuamiFont.setTextAlign(Align.LEFT);
            canvas.drawText(Util.formatTime(minutes), this.mLeftMinute, this.mTopMinute, this.mPaintHuamiFont);
            this.mPaintHuamiFont.setTypeface(this.mDateBatteryTypeface);
            this.mPaintHuamiFont.setTextAlign(Align.RIGHT);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeDate);
            canvas.drawText(Util.formatTime(month) + "-" + Util.formatTime(day), this.mCenterXDate, this.mCenterYDate, this.mPaintHuamiFont);
            this.mPaintHuamiFont.setTypeface(this.mWeekTypeface);
            canvas.drawText(this.WEEKDAYS[week - 1], this.mCenterXWeek, this.mCenterYWeek, this.mPaintWeek);
            this.mPaintHuamiFont.setTypeface(this.mCalStepTypeface);
            this.mPaintHuamiFont.setTextAlign(Align.LEFT);
            this.mPaintHuamiFont.setColor(this.mColorCal);
            canvas.drawBitmap(this.mTotalKmIcon, this.mLeftTodayCalorieIcon, this.mTopTodayCalorieIcon, this.mGPaint);
            canvas.drawText(this.mSportTodayString, (this.mLeftTodayCalorieIcon + ((float) this.mTotalKmIcon.getWidth())) + 3.0f, this.mTopCalorie, this.mPaintHuamiFont);
            this.mPaintHuamiFont.setColor(this.mColorStep);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeStep);
            canvas.drawBitmap(this.mTodayStepIcon, this.mLeftTodayStepIcon, this.mTopTodayStepIcon, this.mGPaint);
            canvas.drawText(this.mStepString, (this.mLeftTodayStepIcon + ((float) this.mTodayStepIcon.getWidth())) + 3.0f, this.mTopStep, this.mPaintHuamiFont);
            drawCalorieContent(canvas, centerX, centerY);
            this.batteryDrawable.setLevel(this.mProgressBattery);
            Bitmap bitmap = Util.drawableToBitmap(this.batteryDrawable);
            canvas.drawBitmap(bitmap, this.mCenterXBatteryLevel, this.mCenterYBatteryLevel, null);
            this.mPaintHuamiFont.setTypeface(this.mDateBatteryTypeface);
            this.mPaintHuamiFont.setTextSize(this.mFontSizeBattery);
            this.mPaintHuamiFont.setColor(this.mColorBattery);
            canvas.drawText(String.valueOf(this.mProgressBattery), this.mCenterXBatteryLevel + ((float) bitmap.getWidth()), this.mBatteryTop, this.mPaintHuamiFont);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportSixSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(156.0f);
        return new Engine();
    }
}
