package com.huami.watch.watchface;

import android.content.Context;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.analog.SlptRotateTodayDistanceView;
import com.ingenic.iwds.slpt.view.analog.SlptRotateTodayStepView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayDistanceArcAnglePicView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptBatteryView;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.sport.SlptPowerNumView;
import com.ingenic.iwds.slpt.view.sport.SlptTodaySportDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptTodaySportDistanceLView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalWatchFaceSportSixSlpt extends AbstractSlptClock {
    private byte[][] battery_num = new byte[14][];
    private byte[][] date_num = new byte[10][];
    private String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private byte[][] km_num = new byte[10][];
    private SlptViewComponent lowBatteryView = null;
    private Context mContext;
    private byte[][] power_num = new byte[10][];
    private byte[][] step_num = new byte[10][];
    private byte[][] time_num = new byte[10][];
    private byte[][] week_num = new byte[7][];

    private void init_num_mem() {
        int i;
        String path;
        for (i = 0; i < 10; i++) {
            this.date_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/lonelytrack/date_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.power_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/lonelytrack/power_num_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 14; i++) {
            this.battery_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/lonelytrack/power_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            path = new String("guard/lonelytrack/en/week_%d.png");
        } else {
            path = new String("guard/lonelytrack/week_%d.png");
        }
        for (i = 0; i < 7; i++) {
            this.week_num[i] = SimpleFile.readFileFromAssets(this, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.step_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/lonelytrack/step_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.time_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/lonelytrack/time_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.km_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/lonelytrack/km_%d.png", new Object[]{Integer.valueOf(i)}));
        }
    }

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        init_num_mem();
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -65536, 0);
        this.lowBatteryView.setStart(0, 234);
        this.lowBatteryView.setRect(320, this.mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
    }

    protected SlptLayout createClockLayout8C() {
        byte[] kmTextMem;
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptLinearLayout hourLayout = new SlptLinearLayout();
        SlptViewComponent minuteLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptPictureView slptBgView1 = new SlptPictureView();
        SlptViewComponent kmIconView = new SlptPictureView();
        SlptViewComponent todayDistanceFview = new SlptTodaySportDistanceFView();
        SlptViewComponent todayDisSeqView = new SlptPictureView();
        SlptViewComponent todayDistanceLview = new SlptTodaySportDistanceLView();
        SlptViewComponent kmTextView = new SlptPictureView();
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptViewComponent stepNumView = new SlptTodayStepNumView();
        SlptViewComponent timeSeqView = new SlptPictureView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptBatteryView batteryView = new SlptBatteryView(14);
        SlptViewComponent powerNumView = new SlptPowerNumView();
        SlptViewComponent stepArcAnglePicView = new SlptTodayStepArcAnglePicView();
        SlptTodayDistanceArcAnglePicView distanceArcAnglePicView = new SlptTodayDistanceArcAnglePicView();
        SlptViewComponent rotateStepView = new SlptRotateTodayStepView();
        SlptViewComponent rotateDistanceView = new SlptRotateTodayDistanceView();
        SlptPictureView distanceStartPoint = new SlptPictureView();
        hourLayout.add(hourHView);
        hourLayout.add(hourLView);
        minuteLayout.add(minuteHView);
        minuteLayout.add(minuteLView);
        timeLayout.add(hourLayout);
        timeLayout.add(timeSeqView);
        timeLayout.add(minuteLayout);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        SlptViewComponent topLayout = new SlptLinearLayout();
        topLayout.add(kmIconView);
        topLayout.add(todayDistanceFview);
        topLayout.add(todayDisSeqView);
        topLayout.add(todayDistanceLview);
        topLayout.add(kmTextView);
        topLayout.add(stepIconView);
        topLayout.add(stepNumView);
        SlptLinearLayout bottomLayout = new SlptLinearLayout();
        bottomLayout.add(monthLayout);
        bottomLayout.add(weekView);
        bottomLayout.add(batteryView);
        bottomLayout.add(powerNumView);
        linearLayout.add(topLayout);
        linearLayout.add(timeLayout);
        linearLayout.add(bottomLayout);
        linearLayout.add(distanceArcAnglePicView);
        linearLayout.add(stepArcAnglePicView);
        linearLayout.add(this.lowBatteryView);
        linearLayout.add(distanceStartPoint);
        linearLayout.add(rotateStepView);
        linearLayout.add(rotateDistanceView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/bg.png"));
        kmIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_icon_run.png"));
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_icon_step.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/date_seq.png"));
        timeSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/time_seq.png"));
        if (Util.getMeasurement(this) == 1) {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/lonelytrack/en/km_text.png");
        } else {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/lonelytrack/km_text.png");
        }
        kmTextView.setImagePicture(kmTextMem);
        stepArcAnglePicView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/runner_step.png"));
        distanceArcAnglePicView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/runner_distance.png"));
        todayDisSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/km_point.png"));
        todayDisSeqView.id = (short) 20;
        rotateStepView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_step_bar_climax.png"));
        rotateDistanceView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_distance_bar_climax.png"));
        distanceStartPoint.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_distance_point.png"));
        distanceStartPoint.id = (short) 22;
        distanceStartPoint.show = false;
        distanceStartPoint.setStart(0, 10);
        distanceStartPoint.setRect(320, 300);
        distanceStartPoint.alignX = (byte) 2;
        batteryView.setImagePictureArray(this.battery_num);
        powerNumView.setImagePictureArray(this.power_num);
        todayDistanceFview.setImagePictureArray(this.km_num);
        todayDistanceLview.setImagePictureArray(this.km_num);
        stepNumView.setImagePictureArray(this.step_num);
        stepIconView.setPadding(11, 3, 0, 0);
        topLayout.setStart(0, 81);
        topLayout.setRect(320, 24);
        topLayout.alignX = (byte) 2;
        topLayout.alignY = (byte) 2;
        hourLayout.setImagePictureArrayForAll(this.time_num);
        minuteLayout.setImagePictureArrayForAll(this.time_num);
        timeLayout.setStart(0, 117);
        timeLayout.setRect(320, 83);
        timeLayout.alignX = (byte) 2;
        monthHView.setImagePictureArray(this.date_num);
        monthLView.setImagePictureArray(this.date_num);
        dayHView.setImagePictureArray(this.date_num);
        dayLView.setImagePictureArray(this.date_num);
        weekView.setImagePictureArray(this.week_num);
        weekView.setPadding(8, 5, 0, 0);
        powerNumView.setImagePictureArray(this.power_num);
        batteryView.setImagePictureArray(this.battery_num);
        bottomLayout.setStart(0, 207);
        bottomLayout.setRect(320, 32);
        bottomLayout.alignX = (byte) 2;
        bottomLayout.alignY = (byte) 2;
        stepArcAnglePicView.start_angle = 270;
        stepArcAnglePicView.full_angle = 360;
        stepArcAnglePicView.setStart(10, 10);
        stepArcAnglePicView.setRect(300, 300);
        distanceArcAnglePicView.start_angle = 0;
        distanceArcAnglePicView.full_angle = 360;
        distanceArcAnglePicView.setStart(10, 10);
        distanceArcAnglePicView.setRect(300, 300);
        rotateStepView.start_angle = 270;
        rotateStepView.full_angle = 360;
        rotateStepView.setStart(10, 10);
        rotateStepView.setRect(300, 300);
        rotateDistanceView.start_angle = 0;
        rotateDistanceView.full_angle = 360;
        rotateDistanceView.setStart(10, 10);
        rotateDistanceView.setRect(300, 300);
        return linearLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        byte[] kmTextMem;
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptLinearLayout hourLayout = new SlptLinearLayout();
        SlptViewComponent minuteLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptPictureView slptBgView1 = new SlptPictureView();
        SlptViewComponent kmIconView = new SlptPictureView();
        SlptViewComponent todayDistanceFview = new SlptTodaySportDistanceFView();
        SlptViewComponent todayDisSeqView = new SlptPictureView();
        SlptViewComponent todayDistanceLview = new SlptTodaySportDistanceLView();
        SlptViewComponent kmTextView = new SlptPictureView();
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptViewComponent stepNumView = new SlptTodayStepNumView();
        SlptViewComponent timeSeqView = new SlptPictureView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptBatteryView batteryView = new SlptBatteryView(14);
        SlptViewComponent powerNumView = new SlptPowerNumView();
        SlptViewComponent stepArcAnglePicView = new SlptTodayStepArcAnglePicView();
        SlptTodayDistanceArcAnglePicView distanceArcAnglePicView = new SlptTodayDistanceArcAnglePicView();
        SlptViewComponent rotateStepView = new SlptRotateTodayStepView();
        SlptViewComponent rotateDistanceView = new SlptRotateTodayDistanceView();
        SlptPictureView distanceStartPoint = new SlptPictureView();
        hourLayout.add(hourHView);
        hourLayout.add(hourLView);
        minuteLayout.add(minuteHView);
        minuteLayout.add(minuteLView);
        timeLayout.add(hourLayout);
        timeLayout.add(timeSeqView);
        timeLayout.add(minuteLayout);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        SlptViewComponent topLayout = new SlptLinearLayout();
        topLayout.add(kmIconView);
        topLayout.add(todayDistanceFview);
        topLayout.add(todayDisSeqView);
        topLayout.add(todayDistanceLview);
        topLayout.add(kmTextView);
        topLayout.add(stepIconView);
        topLayout.add(stepNumView);
        SlptLinearLayout bottomLayout = new SlptLinearLayout();
        bottomLayout.add(monthLayout);
        bottomLayout.add(weekView);
        bottomLayout.add(batteryView);
        bottomLayout.add(powerNumView);
        linearLayout.add(topLayout);
        linearLayout.add(timeLayout);
        linearLayout.add(bottomLayout);
        linearLayout.add(distanceArcAnglePicView);
        linearLayout.add(stepArcAnglePicView);
        linearLayout.add(this.lowBatteryView);
        linearLayout.add(distanceStartPoint);
        linearLayout.add(rotateStepView);
        linearLayout.add(rotateDistanceView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/bg_26wc.png"));
        kmIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_icon_run.png"));
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_icon_step.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/date_seq.png"));
        timeSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/time_seq.png"));
        if (Util.getMeasurement(this) == 1) {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/lonelytrack/en/km_text.png");
        } else {
            kmTextMem = SimpleFile.readFileFromAssets(this, "guard/lonelytrack/km_text.png");
        }
        kmTextView.setImagePicture(kmTextMem);
        stepArcAnglePicView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/runner_step.png"));
        distanceArcAnglePicView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/runner_distance.png"));
        todayDisSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/km_point.png"));
        todayDisSeqView.id = (short) 20;
        rotateStepView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_step_bar_climax.png"));
        rotateDistanceView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_distance_bar_climax.png"));
        distanceStartPoint.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/lonelytrack/watchface_distance_point.png"));
        distanceStartPoint.id = (short) 22;
        distanceStartPoint.show = false;
        distanceStartPoint.setStart(0, 10);
        distanceStartPoint.setRect(320, 300);
        distanceStartPoint.alignX = (byte) 2;
        batteryView.setImagePictureArray(this.battery_num);
        powerNumView.setImagePictureArray(this.power_num);
        todayDistanceFview.setImagePictureArray(this.km_num);
        todayDistanceLview.setImagePictureArray(this.km_num);
        stepNumView.setImagePictureArray(this.step_num);
        stepIconView.setPadding(11, 3, 0, 0);
        topLayout.setStart(0, 81);
        topLayout.setRect(320, 24);
        topLayout.alignX = (byte) 2;
        topLayout.alignY = (byte) 2;
        hourLayout.setImagePictureArrayForAll(this.time_num);
        minuteLayout.setImagePictureArrayForAll(this.time_num);
        timeLayout.setStart(0, 117);
        timeLayout.setRect(320, 83);
        timeLayout.alignX = (byte) 2;
        monthHView.setImagePictureArray(this.date_num);
        monthLView.setImagePictureArray(this.date_num);
        dayHView.setImagePictureArray(this.date_num);
        dayLView.setImagePictureArray(this.date_num);
        weekView.setImagePictureArray(this.week_num);
        weekView.setPadding(8, 5, 0, 0);
        powerNumView.setImagePictureArray(this.power_num);
        batteryView.setImagePictureArray(this.battery_num);
        bottomLayout.setStart(0, 207);
        bottomLayout.setRect(320, 32);
        bottomLayout.alignX = (byte) 2;
        bottomLayout.alignY = (byte) 2;
        stepArcAnglePicView.start_angle = 270;
        stepArcAnglePicView.full_angle = 360;
        stepArcAnglePicView.setStart(10, 10);
        stepArcAnglePicView.setRect(300, 300);
        distanceArcAnglePicView.start_angle = 0;
        distanceArcAnglePicView.full_angle = 360;
        distanceArcAnglePicView.setStart(10, 10);
        distanceArcAnglePicView.setRect(300, 300);
        rotateStepView.start_angle = 270;
        rotateStepView.full_angle = 360;
        rotateStepView.setStart(10, 10);
        rotateStepView.setRect(300, 300);
        rotateDistanceView.start_angle = 0;
        rotateDistanceView.full_angle = 360;
        rotateDistanceView.setStart(10, 10);
        rotateDistanceView.setRect(300, 300);
        return linearLayout;
    }
}
