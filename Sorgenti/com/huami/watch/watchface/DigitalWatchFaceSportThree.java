package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextPaint;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class DigitalWatchFaceSportThree extends AbstractWatchFace {

    private class Engine extends DigitalEngine {
        private String HEART_UNIT;
        private String STEP_UNIT;
        private String[] WEEKDAYS;
        private TextPaint kcalPaintFont;
        private Bitmap mBackgroundBitmap;
        private RectF mBound;
        private float mCalories;
        private WatchDataListener mCaloriesDataListener;
        private String mCaloriesText;
        private float mCenterXCal;
        private float mCenterXDate;
        private float mCenterXHeartAnchor;
        private float mCenterXHeartText;
        private float mCenterXMiddle;
        private float mCenterXStepAnchor;
        private float mCenterXStepText;
        private float mCenterXWeek;
        private float mCenterYCal;
        private float mCenterYDate;
        private float mCenterYStepAnchor;
        private float mCenterYWeek;
        private int mColorDateWhite;
        private int mColorProgressFg;
        private int mColorWhite;
        private int mCurStepCount;
        private float mFontSizeCal;
        private float mFontSizeDate;
        private float mFontSizeHour;
        private float mFontSizeMiddle;
        private float mFontSizeMinute;
        private float mFontSizeStep;
        private Paint mGPaint;
        private WatchDataListener mHeartRateDataListener;
        private int mHeartRato;
        private String mHeartString;
        private Bitmap mIconHeart;
        private Bitmap mIconStep;
        private float mLeftHeartIcon;
        private float mLeftHeartUnitText;
        private float mLeftMinute;
        private float mLeftStepIcon;
        private float mLeftStepUnitText;
        private float mOffsetYStepAnchor;
        private TextPaint mPaintTekoFont;
        private TextPaint mPaintTekoFontWithShadow;
        private TextPaint mPaintUnit;
        private TextPaint mPaintWeek;
        private float mProgressDegreeStep;
        private float mRightHour;
        private WatchDataListener mStepDataListener;
        private String mStepString;
        private float mTopHeartIcon;
        private float mTopHeartUnitText;
        private float mTopHour;
        private float mTopMiddle;
        private float mTopMinute;
        private float mTopStepIcon;
        private float mTopStepUnitText;
        private int mTotalStepTarget;
        private boolean needUnit;

        class C02031 implements WatchDataListener {
            C02031() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 1) {
                    Engine.this.mCurStepCount = ((Integer) values[0]).intValue();
                    Engine.this.updateStepInfo();
                }
            }

            public int getDataType() {
                return 1;
            }
        }

        class C02042 implements WatchDataListener {
            C02042() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 4) {
                    Engine.this.mCalories = ((Float) values[0]).floatValue();
                    Engine.this.updateCalories();
                }
            }

            public int getDataType() {
                return 4;
            }
        }

        class C02053 implements WatchDataListener {
            C02053() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 5) {
                    Engine.this.mHeartRato = ((Integer) values[0]).intValue();
                    Engine.this.updateHeartInfo();
                }
            }

            public int getDataType() {
                return 5;
            }
        }

        private Engine() {
            super();
            this.mCurStepCount = -1;
            this.mTotalStepTarget = 8000;
            this.mHeartRato = 0;
            this.mStepString = "--";
            this.mHeartString = "--";
            this.mBound = new RectF();
            this.needUnit = true;
            this.mStepDataListener = new C02031();
            this.mCalories = 0.0f;
            this.mCaloriesText = "--kCal";
            this.mCaloriesDataListener = new C02042();
            this.mHeartRateDataListener = new C02053();
        }

        protected void onPrepareResources(Resources resources) {
            this.mBackgroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_bg_digital_sport_three, null)).getBitmap();
            this.mIconStep = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_three_icon_step, null)).getBitmap();
            this.mIconHeart = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_three_icon_heart, null)).getBitmap();
            this.needUnit = resources.getBoolean(R.bool.digital_sport_three_need_unit);
            this.STEP_UNIT = " " + resources.getString(R.string.sport_step);
            this.HEART_UNIT = " " + resources.getString(R.string.everyminute_times);
            this.mRightHour = resources.getDimension(R.dimen.digital_sport_three_hour_right);
            this.mTopHour = resources.getDimension(R.dimen.digital_sport_three_hour_top);
            this.mLeftMinute = resources.getDimension(R.dimen.digital_sport_three_minute_left);
            this.mTopMinute = resources.getDimension(R.dimen.digital_sport_three_minute_top);
            this.mCenterXMiddle = resources.getDimension(R.dimen.digital_sport_three_middle_centerx);
            this.mTopMiddle = resources.getDimension(R.dimen.digital_sport_three_middle_top);
            this.mCenterXCal = resources.getDimension(R.dimen.digital_sport_three_cal_centerx);
            this.mCenterYCal = resources.getDimension(R.dimen.digital_sport_three_cal_centery);
            this.mCenterXDate = resources.getDimension(R.dimen.digital_sport_three_date_centerx);
            this.mCenterYDate = resources.getDimension(R.dimen.digital_sport_three_date_centery);
            this.mCenterXWeek = resources.getDimension(R.dimen.digital_sport_three_week_centerx);
            this.mCenterYWeek = resources.getDimension(R.dimen.digital_sport_three_week_centery);
            this.mCenterXStepAnchor = resources.getDimension(R.dimen.digital_sport_three_step_anchor_centerx);
            this.mCenterXHeartAnchor = resources.getDimension(R.dimen.digital_sport_three_heart_anchor_centerx);
            this.mCenterYStepAnchor = resources.getDimension(R.dimen.digital_sport_three_step_anchor_centery);
            this.mOffsetYStepAnchor = resources.getDimension(R.dimen.digital_sport_three_step_text_offsety);
            this.mTopStepUnitText = resources.getDimension(R.dimen.digital_sport_three_step_unit_top);
            this.mTopHeartUnitText = resources.getDimension(R.dimen.digital_sport_three_step_unit_top);
            this.mFontSizeHour = resources.getDimension(R.dimen.digital_sport_three_font_hour_size);
            this.mFontSizeMinute = resources.getDimension(R.dimen.digital_sport_three_font_minute_size);
            this.mFontSizeMiddle = resources.getDimension(R.dimen.digital_sport_three_font_middle_size);
            this.mFontSizeStep = resources.getDimension(R.dimen.digital_sport_three_font_step_size);
            this.mFontSizeCal = resources.getDimension(R.dimen.digital_sport_three_font_cal_size);
            this.mFontSizeDate = resources.getDimension(R.dimen.digital_sport_three_font_date_size);
            this.mColorWhite = resources.getColor(R.color.digital_sport_three_white);
            this.mColorDateWhite = resources.getColor(R.color.digital_sport_three_date_white);
            this.mColorProgressFg = resources.getColor(R.color.digital_sport_three_progress_fg);
            this.mPaintTekoFontWithShadow = new TextPaint(1);
            this.mPaintTekoFontWithShadow.setTypeface(TypefaceManager.get().createFromAsset("typeface/huamufontspeed.ttf"));
            this.mPaintTekoFontWithShadow.setShadowLayer(resources.getDimension(R.dimen.digital_sport_three_font_shadow_radius), resources.getDimension(R.dimen.digital_sport_three_font_shadow_x), resources.getDimension(R.dimen.digital_sport_three_font_shadow_y), resources.getColor(R.color.digital_sport_three_font_shadow));
            this.mPaintTekoFont = new TextPaint(1);
            this.mPaintTekoFont.setTypeface(TypefaceManager.get().createFromAsset("typeface/huamufontspeed.ttf"));
            this.kcalPaintFont = new TextPaint(1);
            this.kcalPaintFont.setTypeface(TypefaceManager.get().createFromAsset("typeface/huamufontspeedkcal.ttf"));
            this.kcalPaintFont.setTextAlign(Align.CENTER);
            this.kcalPaintFont.setColor(this.mColorWhite);
            this.mPaintWeek = new TextPaint(1);
            this.mPaintWeek.setTypeface(Typeface.SANS_SERIF);
            this.mPaintWeek.setColor(this.mColorDateWhite);
            this.mPaintWeek.setTextSize(resources.getDimension(R.dimen.digital_sport_three_font_week_size));
            this.mPaintWeek.setTextAlign(Align.CENTER);
            this.mPaintUnit = new TextPaint(1);
            this.mPaintUnit.setTypeface(Typeface.SANS_SERIF);
            this.mPaintUnit.setColor(this.mColorDateWhite);
            this.mPaintUnit.setTextSize(resources.getDimension(R.dimen.digital_sport_three_font_unit_size));
            this.mPaintUnit.setTextAlign(Align.LEFT);
            this.mGPaint = new Paint(3);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setStrokeWidth(26.0f);
            this.WEEKDAYS = resources.getStringArray(R.array.weekdays);
            registerWatchDataListener(this.mStepDataListener);
            registerWatchDataListener(this.mCaloriesDataListener);
            registerWatchDataListener(this.mHeartRateDataListener);
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(DigitalWatchFaceSportThree.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                    updateStepInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void updateStepInfo() {
            if (this.mTotalStepTarget <= 0 || this.mCurStepCount < 0) {
                this.mProgressDegreeStep = 0.0f;
            } else {
                this.mProgressDegreeStep = Math.min((((float) this.mCurStepCount) * 1.0f) / ((float) this.mTotalStepTarget), 1.0f) * 300.0f;
            }
            if (this.mCurStepCount >= 0) {
                this.mStepString = String.valueOf(this.mCurStepCount);
            }
            this.mPaintTekoFont.setTextSize(this.mFontSizeStep);
            float textWidth = getFontlength(this.mPaintTekoFont, this.mStepString);
            if (this.needUnit) {
                this.mLeftStepIcon = this.mCenterXStepAnchor - (((((float) this.mIconStep.getWidth()) + textWidth) + getFontlength(this.mPaintUnit, this.STEP_UNIT)) * 0.5f);
            } else {
                this.mLeftStepIcon = this.mCenterXStepAnchor - ((((float) this.mIconStep.getWidth()) + textWidth) * 0.5f);
            }
            this.mTopStepIcon = this.mCenterYStepAnchor - (((float) this.mIconStep.getHeight()) * 0.5f);
            this.mCenterXStepText = (this.mLeftStepIcon + ((float) this.mIconStep.getWidth())) + (textWidth * 0.5f);
            this.mLeftStepUnitText = (this.mLeftStepIcon + ((float) this.mIconStep.getWidth())) + textWidth;
        }

        private void updateCalories() {
            this.mCaloriesText = String.valueOf((int) this.mCalories) + "kCal";
        }

        private void updateHeartInfo() {
            if (this.mHeartRato > 0) {
                this.mHeartString = String.valueOf(this.mHeartRato);
            } else {
                this.mHeartString = "--";
            }
            this.mPaintTekoFont.setTextSize(this.mFontSizeStep);
            float textWidth = getFontlength(this.mPaintTekoFont, this.mHeartString);
            if (this.needUnit) {
                this.mLeftHeartIcon = this.mCenterXHeartAnchor - (((((float) this.mIconHeart.getWidth()) + textWidth) + getFontlength(this.mPaintUnit, this.HEART_UNIT)) * 0.5f);
            } else {
                this.mLeftHeartIcon = this.mCenterXHeartAnchor - ((((float) this.mIconHeart.getWidth()) + textWidth) * 0.5f);
            }
            this.mTopHeartIcon = this.mCenterYStepAnchor - (((float) this.mIconHeart.getHeight()) * 0.5f);
            this.mCenterXHeartText = (this.mLeftHeartIcon + ((float) this.mIconHeart.getWidth())) + (textWidth * 0.5f);
            this.mLeftHeartUnitText = (this.mLeftHeartIcon + ((float) this.mIconHeart.getWidth())) + textWidth;
        }

        private float getFontlength(Paint paint, String str) {
            return paint.measureText(str);
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            this.mBound.set(centerX - 140.0f, centerY - 140.0f, 140.0f + centerX, 140.0f + centerY);
            this.mGPaint.setColor(-16777216);
            canvas.drawCircle(centerX, centerY, 140.0f, this.mGPaint);
            this.mGPaint.setColor(this.mColorProgressFg);
            canvas.drawArc(this.mBound, 120.0f, this.mProgressDegreeStep, false, this.mGPaint);
            this.mBound.set(0.0f, 0.0f, width, height);
            canvas.drawBitmap(this.mBackgroundBitmap, null, this.mBound, this.mGPaint);
            this.mPaintTekoFontWithShadow.setColor(this.mColorWhite);
            this.mPaintTekoFontWithShadow.setTextSize(this.mFontSizeHour);
            this.mPaintTekoFontWithShadow.setTextAlign(Align.RIGHT);
            canvas.drawText(Util.formatTime(hours), this.mRightHour, this.mTopHour, this.mPaintTekoFontWithShadow);
            this.mPaintTekoFontWithShadow.setTextSize(this.mFontSizeMinute);
            this.mPaintTekoFontWithShadow.setTextAlign(Align.LEFT);
            canvas.drawText(Util.formatTime(minutes), this.mLeftMinute, this.mTopMinute, this.mPaintTekoFontWithShadow);
            if (!this.mDrawTimeIndicator) {
                this.mPaintTekoFontWithShadow.setTextSize(this.mFontSizeMiddle);
                this.mPaintTekoFontWithShadow.setTextAlign(Align.CENTER);
                canvas.drawText(":", this.mCenterXMiddle, this.mTopMiddle, this.mPaintTekoFontWithShadow);
            }
            this.mPaintTekoFont.setTextAlign(Align.CENTER);
            this.mPaintTekoFont.setTextSize(this.mFontSizeDate);
            this.mPaintTekoFont.setColor(this.mColorDateWhite);
            canvas.drawText(Util.formatTime(month) + "-" + Util.formatTime(day), this.mCenterXDate, this.mCenterYDate, this.mPaintTekoFont);
            canvas.drawText(this.WEEKDAYS[week - 1], this.mCenterXWeek, this.mCenterYWeek, this.mPaintWeek);
            this.mPaintTekoFont.setTextSize(this.mFontSizeStep);
            canvas.drawBitmap(this.mIconStep, this.mLeftStepIcon, this.mTopStepIcon, this.mGPaint);
            canvas.drawText(this.mStepString, this.mCenterXStepText, this.mCenterYStepAnchor + this.mOffsetYStepAnchor, this.mPaintTekoFont);
            if (this.needUnit) {
                canvas.drawText(this.STEP_UNIT, this.mLeftStepUnitText, this.mTopStepUnitText, this.mPaintUnit);
            }
            canvas.drawBitmap(this.mIconHeart, this.mLeftHeartIcon, this.mTopHeartIcon, this.mGPaint);
            canvas.drawText(this.mHeartString, this.mCenterXHeartText, this.mCenterYStepAnchor + this.mOffsetYStepAnchor, this.mPaintTekoFont);
            if (this.needUnit) {
                canvas.drawText(this.HEART_UNIT, this.mLeftHeartUnitText, this.mTopHeartUnitText, this.mPaintUnit);
            }
            this.kcalPaintFont.setTextSize(this.mFontSizeCal);
            canvas.drawText(this.mCaloriesText, this.mCenterXCal, this.mCenterYCal, this.kcalPaintFont);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportThreeSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(35.0f);
        return new Engine();
    }
}
