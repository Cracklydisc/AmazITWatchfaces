package com.huami.watch.watchface;

import android.content.Context;
import android.graphics.Typeface;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.sport.SlptLastHeartRateView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayCaloriesView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalWatchFaceSportThreeSlpt extends AbstractSlptClock {
    private byte[][] cal_num = new byte[10][];
    private byte[][] date_num = new byte[10][];
    private String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private SlptViewComponent lowBatteryView = null;
    private Context mContext;
    private byte[][] small_num = new byte[10][];
    private byte[][] time_num = new byte[10][];
    private byte[][] week_num = new byte[7][];

    private void init_num_mem() {
        int i;
        String path;
        for (i = 0; i < 10; i++) {
            this.time_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/jisudidai/time_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            path = new String("guard/jisudidai/en/week_%d.png");
        } else {
            path = new String("guard/jisudidai/week_%d.png");
        }
        for (i = 0; i < 7; i++) {
            this.week_num[i] = SimpleFile.readFileFromAssets(this, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.small_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/jisudidai/small_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.date_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/jisudidai/date_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.cal_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/jisudidai/cal_%d.png", new Object[]{Integer.valueOf(i)}));
        }
    }

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        init_num_mem();
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -65536, 0);
        this.lowBatteryView.setStart(0, 50);
        this.lowBatteryView.setRect(320, this.mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
    }

    protected SlptLayout createClockLayout8C() {
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptViewComponent hourHView = new SlptHourHView();
        SlptViewComponent hourLView = new SlptHourLView();
        SlptPictureView timeSeqView = new SlptPictureView();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptPictureView heartIconView = new SlptPictureView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptTodayStepArcAnglePicView arcPicStep = new SlptTodayStepArcAnglePicView();
        SlptViewComponent stepNumLayout = new SlptLinearLayout();
        SlptViewComponent mStepNum = new SlptTodayStepNumView();
        SlptViewComponent mStepTitle = new SlptPictureView();
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptPictureView monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptLinearLayout calorsLayout = new SlptLinearLayout();
        SlptViewComponent todayCaloriesView = new SlptTodayCaloriesView();
        SlptPictureView caloresTips = new SlptPictureView();
        SlptLinearLayout heartrateLayout = new SlptLinearLayout();
        SlptViewComponent lastHeartRateView = new SlptLastHeartRateView();
        SlptPictureView heartrateTips = new SlptPictureView();
        SlptPictureView heartRateInvisableView = new SlptPictureView();
        timeLayout.add(hourHView);
        timeLayout.add(hourLView);
        timeLayout.add(timeSeqView);
        timeLayout.add(minuteHView);
        timeLayout.add(minuteLView);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        calorsLayout.add(todayCaloriesView);
        calorsLayout.add(caloresTips);
        heartrateLayout.add(heartIconView);
        heartrateLayout.add(lastHeartRateView);
        heartrateLayout.add(heartRateInvisableView);
        if (!Util.needEnAssets()) {
            heartrateLayout.add(heartrateTips);
        }
        stepNumLayout.add(stepIconView);
        stepNumLayout.add(mStepNum);
        if (!Util.needEnAssets()) {
            stepNumLayout.add(mStepTitle);
        }
        linearLayout.add(arcPicStep);
        linearLayout.add(slptBgView1);
        linearLayout.add(timeLayout);
        linearLayout.add(stepNumLayout);
        linearLayout.add(monthLayout);
        linearLayout.add(calorsLayout);
        linearLayout.add(heartrateLayout);
        linearLayout.add(weekView);
        linearLayout.add(this.lowBatteryView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/watchface_bg_8c.png"));
        arcPicStep.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/watchface_bg_status_8c.png"));
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/watchface_icon_step_8c.png"));
        heartIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/watchface_icon_heart_8c.png"));
        timeSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/time_x.png"));
        mStepTitle.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/small_step.png"));
        heartrateTips.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/small_rate.png"));
        caloresTips.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/kcal.png"));
        monthSeqView.setStringPicture("-");
        heartRateInvisableView.setStringPicture("--");
        heartRateInvisableView.setTextAttr(24.0f, -1, Typeface.DEFAULT);
        heartRateInvisableView.id = (short) 21;
        heartRateInvisableView.show = false;
        Typeface numTypeface = TypefaceManager.get().createFromAsset("typeface/Teko-Regular.ttf");
        Typeface timeTypeface = TypefaceManager.get().createFromAsset("typeface/huamufontspeed.ttf");
        minuteHView.setPadding(3, 0, 0, 0);
        timeLayout.setTextAttrForAll(126.0f, -1, timeTypeface);
        timeLayout.setImagePictureArrayForAll(this.time_num);
        timeLayout.setStart(63, 112);
        mStepNum.setTextAttr(24.0f, -1, numTypeface);
        mStepNum.setImagePictureArray(this.small_num);
        mStepNum.setPadding(2, 0, 3, 0);
        mStepTitle.setTextAttr(15.0f, -1, Typeface.DEFAULT);
        mStepTitle.setPadding(2, 0, 1, 0);
        stepNumLayout.setStart(65, 81);
        stepNumLayout.setRect(95, 24);
        stepNumLayout.alignX = (byte) 2;
        mStepTitle.alignParentY = (byte) 0;
        lastHeartRateView.setTextAttr(24.0f, -1, numTypeface);
        lastHeartRateView.setImagePictureArray(this.small_num);
        lastHeartRateView.setPadding(0, 0, 2, 0);
        heartrateTips.setTextAttr(15.0f, -1, Typeface.DEFAULT);
        heartrateTips.setPadding(2, 0, 3, 0);
        heartIconView.setPadding(0, 0, 2, 0);
        lastHeartRateView.setPadding(2, 0, 4, 0);
        heartrateLayout.setStart(160, 79);
        heartrateLayout.setRect(95, 24);
        heartrateLayout.alignX = (byte) 2;
        heartrateLayout.alignY = (byte) 0;
        todayCaloriesView.setImagePictureArray(this.cal_num);
        caloresTips.setPadding(2, 0, 10, 0);
        calorsLayout.setTextAttrForAll(32.0f, -1, numTypeface);
        calorsLayout.setStart(85, 257);
        calorsLayout.setRect(150, 48);
        calorsLayout.alignX = (byte) 2;
        monthLayout.setTextAttrForAll(24.0f, -1, numTypeface);
        monthLayout.setImagePictureArrayForAll(this.date_num);
        monthLayout.setStringPictureArrayForAll(this.digitalNums);
        monthLayout.setStart(72, 208);
        monthLayout.alignX = (byte) 2;
        monthLayout.alignY = (byte) 1;
        weekView.setTextAttr(18.0f, -1, numTypeface);
        weekView.setImagePictureArray(this.week_num);
        weekView.setStart(198, 212);
        arcPicStep.start_angle = 210;
        arcPicStep.len_angle = 0;
        arcPicStep.full_angle = 300;
        return linearLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptViewComponent hourHView = new SlptHourHView();
        SlptViewComponent hourLView = new SlptHourLView();
        SlptPictureView timeSeqView = new SlptPictureView();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptPictureView heartIconView = new SlptPictureView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptTodayStepArcAnglePicView arcPicStep = new SlptTodayStepArcAnglePicView();
        SlptViewComponent stepNumLayout = new SlptLinearLayout();
        SlptViewComponent mStepNum = new SlptTodayStepNumView();
        SlptViewComponent mStepTitle = new SlptPictureView();
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptPictureView monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptLinearLayout calorsLayout = new SlptLinearLayout();
        SlptViewComponent todayCaloriesView = new SlptTodayCaloriesView();
        SlptPictureView caloresTips = new SlptPictureView();
        SlptLinearLayout heartrateLayout = new SlptLinearLayout();
        SlptViewComponent lastHeartRateView = new SlptLastHeartRateView();
        SlptPictureView heartrateTips = new SlptPictureView();
        SlptPictureView heartRateInvisableView = new SlptPictureView();
        timeLayout.add(hourHView);
        timeLayout.add(hourLView);
        timeLayout.add(timeSeqView);
        timeLayout.add(minuteHView);
        timeLayout.add(minuteLView);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        calorsLayout.add(todayCaloriesView);
        calorsLayout.add(caloresTips);
        heartrateLayout.add(heartIconView);
        heartrateLayout.add(lastHeartRateView);
        heartrateLayout.add(heartRateInvisableView);
        if (!Util.needEnAssets()) {
            heartrateLayout.add(heartrateTips);
        }
        stepNumLayout.add(stepIconView);
        stepNumLayout.add(mStepNum);
        if (!Util.needEnAssets()) {
            stepNumLayout.add(mStepTitle);
        }
        linearLayout.add(arcPicStep);
        linearLayout.add(slptBgView1);
        linearLayout.add(timeLayout);
        linearLayout.add(stepNumLayout);
        linearLayout.add(monthLayout);
        linearLayout.add(calorsLayout);
        linearLayout.add(heartrateLayout);
        linearLayout.add(weekView);
        linearLayout.add(this.lowBatteryView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/watchface_bg_26wc.png"));
        arcPicStep.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/watchface_bg_status_8c.png"));
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/watchface_icon_step_8c.png"));
        heartIconView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/watchface_icon_heart_8c.png"));
        timeSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/time_x.png"));
        mStepTitle.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/small_step.png"));
        heartrateTips.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/small_rate.png"));
        caloresTips.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/jisudidai/kcal.png"));
        monthSeqView.setStringPicture("-");
        heartRateInvisableView.setStringPicture("--");
        heartRateInvisableView.setTextAttr(24.0f, -1, Typeface.DEFAULT);
        heartRateInvisableView.id = (short) 21;
        heartRateInvisableView.show = false;
        Typeface numTypeface = TypefaceManager.get().createFromAsset("typeface/Teko-Regular.ttf");
        Typeface timeTypeface = TypefaceManager.get().createFromAsset("typeface/huamufontspeed.ttf");
        minuteHView.setPadding(3, 0, 0, 0);
        timeLayout.setTextAttrForAll(126.0f, -1, timeTypeface);
        timeLayout.setImagePictureArrayForAll(this.time_num);
        timeLayout.setStart(63, 112);
        mStepNum.setTextAttr(24.0f, -1, numTypeface);
        mStepNum.setImagePictureArray(this.small_num);
        mStepNum.setPadding(2, 0, 3, 0);
        mStepTitle.setTextAttr(15.0f, -1, Typeface.DEFAULT);
        mStepTitle.setPadding(2, 0, 1, 0);
        stepNumLayout.setStart(65, 81);
        stepNumLayout.setRect(95, 24);
        stepNumLayout.alignX = (byte) 2;
        mStepTitle.alignParentY = (byte) 0;
        lastHeartRateView.setTextAttr(24.0f, -1, numTypeface);
        lastHeartRateView.setImagePictureArray(this.small_num);
        lastHeartRateView.setPadding(0, 0, 2, 0);
        heartrateTips.setTextAttr(15.0f, -1, Typeface.DEFAULT);
        heartrateTips.setPadding(2, 0, 3, 0);
        heartIconView.setPadding(0, 0, 2, 0);
        lastHeartRateView.setPadding(2, 0, 4, 0);
        heartrateLayout.setStart(160, 79);
        heartrateLayout.setRect(95, 24);
        heartrateLayout.alignX = (byte) 2;
        heartrateLayout.alignY = (byte) 0;
        todayCaloriesView.setImagePictureArray(this.cal_num);
        caloresTips.setPadding(2, 0, 10, 0);
        calorsLayout.setTextAttrForAll(32.0f, -1, numTypeface);
        calorsLayout.setStart(85, 257);
        calorsLayout.setRect(150, 48);
        calorsLayout.alignX = (byte) 2;
        monthLayout.setTextAttrForAll(24.0f, -1, numTypeface);
        monthLayout.setImagePictureArrayForAll(this.date_num);
        monthLayout.setStringPictureArrayForAll(this.digitalNums);
        monthLayout.setStart(72, 208);
        monthLayout.alignX = (byte) 2;
        monthLayout.alignY = (byte) 1;
        weekView.setTextAttr(18.0f, -1, numTypeface);
        weekView.setImagePictureArray(this.week_num);
        weekView.setStart(198, 212);
        arcPicStep.start_angle = 210;
        arcPicStep.len_angle = 0;
        arcPicStep.full_angle = 300;
        return linearLayout;
    }
}
