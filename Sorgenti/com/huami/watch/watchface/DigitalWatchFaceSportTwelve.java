package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.text.TextPaint;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.widget.BatteryLevelWidget;
import com.huami.watch.watchface.widget.StepProgressTextWidget;
import java.io.IOException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

public class DigitalWatchFaceSportTwelve extends AbstractWatchFace {

    private class Engine extends DigitalEngine {
        private Bitmap[] NUMS;
        private Bitmap[] NUMS_STEP;
        private Bitmap[] WEEKS;
        private Bitmap mBgBitmap;
        private Paint mBitmapPaint;
        private Paint mGPaint;
        private float mLeftDate;
        private float mLeftWeek;
        private TextPaint mPaintHuamiFont;
        private StepProgressTextWidget mStepWidget;
        private float mTopDate;
        private int mTotalStepTarget;
        private Bitmap minus;
        private Bitmap stepProgress;

        private Engine() {
            super();
            this.NUMS = new Bitmap[10];
            this.NUMS_STEP = new Bitmap[10];
            this.WEEKS = new Bitmap[7];
            this.mTotalStepTarget = 8000;
        }

        protected void onPrepareResources(Resources resources) {
            int i;
            this.mBitmapPaint = new Paint(7);
            this.mBgBitmap = BitmapFactory.decodeResource(resources, R.drawable.watchface_default_twelve_bg);
            for (i = 0; i < 10; i++) {
                this.NUMS[i] = decodeImage(resources, String.format("guard/huanyingMC/date_%d.png", new Object[]{Integer.valueOf(i)}));
            }
            String weekPath = resources.getString(R.string.digital_sport_twelve_week_path);
            for (i = 0; i < 7; i++) {
                this.WEEKS[i] = decodeImage(resources, String.format(weekPath, new Object[]{Integer.valueOf(i)}));
            }
            this.minus = decodeImage(resources, "guard/huanyingMC/date_minus.png");
            this.mLeftDate = resources.getDimension(R.dimen.digital_sport_twelve_date_left);
            this.mLeftWeek = resources.getDimension(R.dimen.digital_sport_twelve_week_left);
            this.mTopDate = resources.getDimension(R.dimen.digital_sport_twelve_date_top);
            this.mPaintHuamiFont = new TextPaint(1);
            this.mPaintHuamiFont.setColor(-16777216);
            this.mPaintHuamiFont.setTypeface(TypefaceManager.get().createFromAsset("typeface/huamidigital.ttf"));
            this.mGPaint = new Paint(3);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setStrokeWidth(17.0f);
            for (i = 0; i < 10; i++) {
                this.NUMS_STEP[i] = decodeImage(resources, String.format("guard/huanyingMC/step_num_%d.png", new Object[]{Integer.valueOf(i)}));
            }
            this.stepProgress = BitmapFactory.decodeResource(resources, R.drawable.digital_sport_twelve_step);
            this.mStepWidget = new StepProgressTextWidget(88, 107, 0, 5, this.NUMS_STEP, this.stepProgress, -90.0f, 360.0f, 12);
            Bitmap[] levels = new Bitmap[6];
            for (i = 0; i < 6; i++) {
                Resources resources2 = resources;
                levels[i] = decodeImage(resources2, String.format("guard/huanyingMC/android/battery_%d.png", new Object[]{Integer.valueOf(i)}));
            }
            BatteryLevelWidget mBatteryWidget = new BatteryLevelWidget(208, 38, levels);
            addWidget(this.mStepWidget);
            addWidget(mBatteryWidget);
        }

        private Bitmap decodeImage(Resources resources, String fileName) {
            Bitmap image = null;
            try {
                InputStream is = DigitalWatchFaceSportTwelve.this.getResources().getAssets().open(fileName);
                image = BitmapFactory.decodeStream(is);
                is.close();
                return image;
            } catch (IOException e) {
                e.printStackTrace();
                return image;
            }
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(DigitalWatchFaceSportTwelve.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                    if (this.mStepWidget != null) {
                        this.mStepWidget.setTotalStepTarget(this.mTotalStepTarget);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            canvas.drawBitmap(this.mBgBitmap, 0.0f, 0.0f, this.mBitmapPaint);
            this.mPaintHuamiFont.setTextSize(68.0f);
            this.mPaintHuamiFont.setTextAlign(Align.RIGHT);
            canvas.drawText(Util.formatTime(hours), 131.0f, 220.0f, this.mPaintHuamiFont);
            this.mPaintHuamiFont.setTextAlign(Align.LEFT);
            canvas.drawText(Util.formatTime(minutes), 147.0f, 220.0f, this.mPaintHuamiFont);
            if (!this.mDrawTimeIndicator) {
                canvas.drawText(":", 131.0f, 220.0f, this.mPaintHuamiFont);
            }
            this.mPaintHuamiFont.setTextSize(48.0f);
            canvas.drawText(Util.formatTime(seconds), 216.0f, 208.0f, this.mPaintHuamiFont);
            int m0 = month / 10;
            int m1 = month - (m0 * 10);
            canvas.drawBitmap(this.NUMS[m0], this.mLeftDate, this.mTopDate, this.mGPaint);
            canvas.drawBitmap(this.NUMS[m1], this.mLeftDate + 14.0f, this.mTopDate, this.mGPaint);
            canvas.drawBitmap(this.minus, this.mLeftDate + 28.0f, this.mTopDate, this.mGPaint);
            int d0 = day / 10;
            int d1 = day - (d0 * 10);
            canvas.drawBitmap(this.NUMS[d0], this.mLeftDate + 42.0f, this.mTopDate, this.mGPaint);
            canvas.drawBitmap(this.NUMS[d1], this.mLeftDate + 56.0f, this.mTopDate, this.mGPaint);
            canvas.drawBitmap(this.WEEKS[week - 1], this.mLeftWeek, this.mTopDate, this.mGPaint);
            super.onDrawDigital(canvas, width, height, centerX, centerY, seconds, minutes, hours, year, month, day, week, ampm);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSporttwelveSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(23.3f);
        return new Engine();
    }
}
