package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.widget.AbsWatchFaceDataWidget;
import com.huami.watch.watchface.widget.BatteryLevelWidget;
import com.huami.watch.watchface.widget.FatBurnDataWidget;
import com.huami.watch.watchface.widget.HeartRateDateWidget;
import com.huami.watch.watchface.widget.ImageFont;
import com.huami.watch.watchface.widget.MileageDateWidget;
import com.huami.watch.watchface.widget.TextDateWidget;
import com.huami.watch.watchface.widget.TimeDigitalWidget;
import com.huami.watch.watchface.widget.TodayDistanceDateWidget;
import com.huami.watch.watchface.widget.TodayDistanceLevelWidget;
import com.huami.watch.watchface.widget.WalkDateWidget;
import com.huami.watch.watchface.widget.WeatherDateWidget;

public class DigitalWatchFaceSportTwenty extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private Drawable mBgDrawable;
        private Path mClip;

        private Engine() {
            super();
            this.mClip = new Path();
        }

        protected void onPrepareResources(Resources resources) {
            int i;
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.mBgDrawable = Util.decodeBitmapDrawableFromAssets(resources, "guard/weilaijijia/background/watchface_bg.png");
            this.mBgDrawable.setBounds(0, 0, 320, 320);
            ImageFont imageFont = new ImageFont("20_light");
            for (i = 0; i < 10; i++) {
                String num_res = String.format("guard/weilaijijia/font_widget_white/%d.png", new Object[]{Integer.valueOf(i)});
                imageFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, num_res));
            }
            imageFont.addChar('.', Util.decodeImage(resources, "guard/weilaijijia/font_widget_white/point.png"));
            imageFont = new ImageFont("20_black");
            for (i = 0; i < 10; i++) {
                num_res = String.format("guard/weilaijijia/font_widget_black/%d.png", new Object[]{Integer.valueOf(i)});
                imageFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, num_res));
            }
            imageFont.addChar('-', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/minus.png"));
            imageFont.addChar('.', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/dot.png"));
            imageFont.addChar('/', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/separator.png"));
            imageFont.addChar('℃', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/celsius.png"));
            imageFont.addChar('℉', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/fahrenheit.png"));
            imageFont.addChar('A', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/A.png"));
            imageFont.addChar('B', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/B.png"));
            imageFont.addChar('C', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/C.png"));
            imageFont.addChar('K', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/K.png"));
            imageFont.addChar('L', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/L.png"));
            imageFont.addChar('M', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/M.png"));
            imageFont.addChar('P', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/P.png"));
            imageFont.addChar('I', Util.decodeImage(resources, "guard/weilaijijia/font_widget_black/I.png"));
            imageFont = new ImageFont("20_step");
            for (i = 0; i < 10; i++) {
                num_res = String.format("guard/weilaijijia/font_step/%d.png", new Object[]{Integer.valueOf(i)});
                imageFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, num_res));
            }
            final ImageFont datePowerFont = new ImageFont("20_date_power");
            for (i = 0; i < 10; i++) {
                datePowerFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("guard/weilaijijia/font_date_power/%d.png", new Object[]{Integer.valueOf(i)})));
            }
            datePowerFont.addChar('.', Util.decodeImage(resources, "guard/weilaijijia/font_date_power/point.png"));
            datePowerFont.addChar('%', Util.decodeImage(resources, "guard/weilaijijia/font_date_power/percent.png"));
            ImageFont timeFont = new ImageFont("weilaijijia-time");
            for (i = 0; i < 10; i++) {
                timeFont.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("guard/weilaijijia/time/%d.png", new Object[]{Integer.valueOf(i)})));
            }
            timeFont.addChar(':', Util.decodeImage(resources, "guard/weilaijijia/time/colon.png"));
            timeFont.addChar('A', Util.decodeImage(resources, "guard/weilaijijia/time/A.png"));
            timeFont.addChar('P', Util.decodeImage(resources, "guard/weilaijijia/time/P.png"));
            TimeDigitalWidget timeWidget = new TimeDigitalWidget(0, 50, 189, 220, 46, timeFont, 0, 0, 212, 27);
            timeWidget.setPaddingToIndicator(3);
            addWidget(timeWidget);
            Bitmap[] weeks = new Bitmap[7];
            for (i = 0; i < weeks.length; i++) {
                Resources resources2 = resources;
                weeks[i] = Util.decodeImage(resources2, String.format("guard/weilaijijia/week/%d.png", new Object[]{Integer.valueOf(i)}));
            }
            TextDateWidget dateWidget = new TextDateWidget(resources, 0, 57, 320, 24, datePowerFont, weeks);
            dateWidget.setTextAlign(Align.CENTER);
            dateWidget.setDatePosition(160, 0);
            dateWidget.setWeekPosition(214, 0);
            dateWidget.setDateFormat("MM.dd");
            addWidget(dateWidget);
            Bitmap[] batterys = new Bitmap[11];
            for (i = 0; i < batterys.length; i++) {
                resources2 = resources;
                batterys[i] = Util.decodeImage(resources2, String.format("guard/weilaijijia/power/%d.png", new Object[]{Integer.valueOf(i)}));
            }
            addWidget(new BatteryLevelWidget(116, 114, batterys));
            addWidget(new AbsWatchFaceDataWidget() {
                private String mBattery = "0%";
                private Paint f10p = new Paint(3);

                public int getDataType() {
                    return 10;
                }

                public void onDataUpdate(int dataType, Object... values) {
                    if (10 == dataType && values != null && values[0] != null) {
                        this.mBattery = String.valueOf(((Integer) values[0]).intValue()) + "%";
                        this.f10p.setTextAlign(Align.RIGHT);
                    }
                }

                public int getX() {
                    return 203;
                }

                public int getY() {
                    return 107;
                }

                public int getWidth() {
                    return 88;
                }

                public void onDraw(Canvas canvas) {
                    datePowerFont.drawText(canvas, this.mBattery, getWidth(), 0, this.f10p);
                }
            });
            final Drawable stepIcon = Util.decodeBitmapDrawableFromAssets(resources, "guard/weilaijijia/icons/step.png");
            AbsWatchFaceDataWidget stepWidget = new WalkDateWidget(resources, 16, 94, 0, imageFont) {
                protected Drawable getIconDrawable(Resources resources) {
                    return stepIcon;
                }
            };
            stepWidget.setPaddingToDrawable(5);
            addWidget(stepWidget);
            final Drawable calIcon = Util.decodeBitmapDrawableFromAssets(resources, "guard/weilaijijia/icons/heat.png");
            AbsWatchFaceDataWidget fatWidget = new FatBurnDataWidget(DigitalWatchFaceSportTwenty.this.getResources(), 120, 149, 1, imageFont) {
                protected Drawable getIconDrawable(Resources resources) {
                    return calIcon;
                }

                protected boolean isWithUnit() {
                    return true;
                }
            };
            fatWidget.setGravity(3);
            fatWidget.setPaddingToDrawable(3);
            addWidget(fatWidget);
            final Drawable mileageIcon = Util.decodeBitmapDrawableFromAssets(resources, "guard/weilaijijia/icons/mileage.png");
            AbsWatchFaceDataWidget mileageWidget = new MileageDateWidget(DigitalWatchFaceSportTwenty.this.getResources(), 218, 149, 1, imageFont) {
                protected Drawable getIconDrawable(Resources resources) {
                    return mileageIcon;
                }

                protected boolean isWithUnit() {
                    return true;
                }
            };
            mileageWidget.setGravity(3);
            mileageWidget.setPaddingToDrawable(3);
            addWidget(mileageWidget);
            AbsWatchFaceDataWidget weatherWidget = new WeatherDateWidget(resources, 120, 170, 160, 18, 1, imageFont);
            weatherWidget.setWeatherIconPath("guard/weilaijijia/weather/");
            weatherWidget.setGravity(3);
            weatherWidget.setPaddingToDrawable(3);
            addWidget(weatherWidget);
            final Drawable heartIcon = Util.decodeBitmapDrawableFromAssets(resources, "guard/weilaijijia/icons/heart.png");
            AbsWatchFaceDataWidget heartWidget = new HeartRateDateWidget(resources, 218, 170, 1, imageFont) {
                protected Drawable getIconDrawable(Resources resources) {
                    return heartIcon;
                }

                protected boolean isWithUnit() {
                    return true;
                }
            };
            heartWidget.setGravity(3);
            heartWidget.setPaddingToDrawable(3);
            addWidget(heartWidget);
            Bitmap[] levels = new Bitmap[11];
            for (i = 0; i < levels.length; i++) {
                resources2 = resources;
                levels[i] = Util.decodeImage(resources2, String.format("guard/weilaijijia/run/%d.png", new Object[]{Integer.valueOf(i)}));
            }
            addWidget(new TodayDistanceLevelWidget(51, 247, levels));
            final Drawable runIcon = Util.decodeBitmapDrawableFromAssets(resources, "guard/weilaijijia/icons/run.png");
            addWidget(new TodayDistanceDateWidget(DigitalWatchFaceSportTwenty.this.getResources(), 100, 257, 1, imageFont) {
                protected Drawable getIconDrawable(Resources resources) {
                    return runIcon;
                }
            });
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.clipPath(this.mClip);
            if (this.mBgDrawable != null) {
                this.mBgDrawable.draw(canvas);
            }
            onDrawWidgets(canvas);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportTwentySlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(10.0f);
        return new Engine();
    }
}
