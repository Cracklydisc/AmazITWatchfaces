package com.huami.watch.watchface;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import android.support.wearable.view.SimpleAnimatorListener;
import android.text.TextPaint;
import android.util.Log;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class DigitalWatchFaceSportTwo extends AbstractWatchFace {

    private class Engine extends DigitalEngine {
        private Drawable batteryDrawable;
        private Paint hourPaint;
        private Bitmap image_background;
        private Bitmap image_battery;
        private Bitmap image_kaluli;
        private Bitmap image_step;
        private float img_step_centerX;
        private float img_step_total_centerX;
        private float left_battery_img;
        private float left_battery_text;
        private float left_battery_unit_text;
        private float left_hour;
        private float left_kaluli_img;
        private float left_kaluli_number;
        private float left_kaluli_unit;
        private float left_minute;
        private float left_month;
        private float left_step_img;
        private float left_step_number;
        private float left_step_unit;
        private float left_weekly;
        private Bitmap mAmBitmap;
        private WatchDataListener mBatteryDataListener;
        private RectF mBound;
        private int mCurStepCount;
        private float mLeftAm;
        private int mOldCurStepCount;
        private Bitmap mPmBitmap;
        private int mProgressBattery;
        private int mProgressDegreeBattery;
        private int mProgressDegreeStep;
        private float mRadius;
        private double mSportTodayDistance;
        private String mSportTodayString;
        private ValueAnimator mStepAnimator;
        private int mStepCompose;
        private WatchDataListener mStepDataListener;
        private int mStepDiff;
        private WatchDataListener mTodayDistanceDataListener;
        private float mTopAm;
        private int mTotalStepTarget;
        private Paint minutePaint;
        private boolean needUnit;
        private Paint numberPaint;
        private Paint paintBatterUnit;
        private Paint paintKaluliUnit;
        private Paint paint_arc_battery;
        private Paint paint_arc_kaluli;
        private Paint paint_arc_step;
        private Paint paint_battery;
        private Paint paint_bg;
        private String step;
        private float top_battery_img;
        private float top_battery_text;
        private float top_hour;
        private float top_kaluli_img;
        private float top_kaluli_number;
        private float top_minute;
        private float top_month;
        private float top_step_img;
        private float top_step_number;
        private float top_weekly;
        private Paint weeklyPaint;

        class C02151 implements AnimatorUpdateListener {
            C02151() {
            }

            public void onAnimationUpdate(ValueAnimator animation) {
                Engine.this.mStepCompose = (int) (((float) Engine.this.mStepDiff) * ((Float) animation.getAnimatedValue()).floatValue());
                Engine.this.postInvalidate();
            }
        }

        class C02162 extends SimpleAnimatorListener {
            C02162() {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                Engine.this.mStepCompose = 0;
                Engine.this.mOldCurStepCount = Engine.this.mCurStepCount;
            }

            public void onAnimationCancel(Animator animator) {
                Engine.this.mStepCompose = 0;
                Engine.this.mOldCurStepCount = Engine.this.mCurStepCount;
            }
        }

        class C02173 implements WatchDataListener {
            C02173() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 10) {
                    Engine.this.updateBatteryLevel(((Integer) values[0]).intValue(), ((Integer) values[1]).intValue());
                }
            }

            public int getDataType() {
                return 10;
            }
        }

        class C02184 implements WatchDataListener {
            C02184() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 2) {
                    Engine.this.mSportTodayDistance = ((Double) values[0]).doubleValue();
                    Log.d("HmWatchFace", "onDataUpdate mSportTodayDistance = " + Engine.this.mSportTodayDistance);
                    Engine.this.updateSportTodayDistance();
                }
            }

            public int getDataType() {
                return 2;
            }
        }

        class C02195 implements WatchDataListener {
            C02195() {
            }

            public void onDataUpdate(int dataType, Object... values) {
                if (dataType == 1) {
                    Engine.this.mCurStepCount = ((Integer) values[0]).intValue();
                    if (Engine.this.mOldCurStepCount != Engine.this.mCurStepCount) {
                        if (Engine.this.mOldCurStepCount > Engine.this.mCurStepCount) {
                            Engine.this.mOldCurStepCount = 0;
                        }
                        Engine.this.mStepAnimator.cancel();
                        Engine.this.mStepDiff = Engine.this.mCurStepCount - Engine.this.mOldCurStepCount;
                        Engine.this.mStepAnimator.start();
                    }
                }
            }

            public int getDataType() {
                return 1;
            }
        }

        private Engine() {
            super();
            this.mBound = new RectF();
            this.mSportTodayDistance = -1.0d;
            this.mSportTodayString = "--";
            this.mCurStepCount = 0;
            this.mOldCurStepCount = 0;
            this.mStepDiff = 0;
            this.mStepCompose = 0;
            this.needUnit = true;
            this.mBatteryDataListener = new C02173();
            this.mTotalStepTarget = 8000;
            this.mTodayDistanceDataListener = new C02184();
            this.mStepDataListener = new C02195();
        }

        private String getDistanceUnit() {
            switch (getMeasurement()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    return "mi";
                default:
                    return "km";
            }
        }

        protected void onPrepareResources(Resources resources) {
            this.needUnit = resources.getBoolean(R.bool.digital_sport_three_need_unit);
            this.step = resources.getString(R.string.sport_step);
            this.image_background = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_bg_digital_sport_two, null)).getBitmap();
            this.image_battery = ((BitmapDrawable) resources.getDrawable(R.drawable.battery_00)).getBitmap();
            this.batteryDrawable = resources.getDrawable(R.drawable.digital_sport_two_battery_level);
            this.image_step = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_bg_digital_sport_two_step)).getBitmap();
            this.image_kaluli = ((BitmapDrawable) resources.getDrawable(R.drawable.sport_watch_two_step_num)).getBitmap();
            this.mAmBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_two_icon_am, null)).getBitmap();
            this.mPmBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_sport_two_icon_pm, null)).getBitmap();
            this.mRadius = resources.getDimension(R.dimen.digital_sport_two_circle_radius);
            this.paint_bg = new Paint(3);
            this.paint_bg.setStyle(Style.STROKE);
            this.paint_bg.setStrokeWidth(17.0f);
            this.paint_battery = new TextPaint();
            this.paint_battery.setColor(resources.getColor(R.color.digital_sport_font_white));
            this.paint_battery.setTextSize(resources.getDimension(R.dimen.digital_sport_two_battery_font));
            this.paint_battery.setTypeface(TypefaceManager.get().createFromAsset("typeface/DINPro-Medium.otf"));
            this.paint_battery.setTextAlign(Align.LEFT);
            this.paint_battery.setAntiAlias(true);
            this.hourPaint = new TextPaint();
            this.hourPaint.setColor(-1);
            this.hourPaint.setTextSize(resources.getDimension(R.dimen.digital_sport_two_hour_font));
            this.hourPaint.setTextAlign(Align.CENTER);
            this.hourPaint.setAntiAlias(true);
            this.hourPaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/DINPro-Medium.otf"));
            this.minutePaint = new TextPaint();
            this.minutePaint.setColor(resources.getColor(R.color.digital_sport_font_white));
            this.minutePaint.setTextSize(resources.getDimension(R.dimen.digital_sport_two_minute_font));
            this.minutePaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/DINPro-Light.otf"));
            this.minutePaint.setTextAlign(Align.CENTER);
            this.minutePaint.setAntiAlias(true);
            this.weeklyPaint = new TextPaint();
            this.weeklyPaint.setColor(-1);
            this.weeklyPaint.setTextSize(resources.getDimension(R.dimen.digital_sport_two_weekly_font));
            this.weeklyPaint.setAntiAlias(true);
            this.numberPaint = new TextPaint();
            this.numberPaint.setAntiAlias(true);
            this.numberPaint.setColor(-1);
            this.numberPaint.setTextSize(resources.getDimension(R.dimen.digital_sport_two_battery_font));
            this.numberPaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/DINPro-Medium.otf"));
            this.numberPaint.setTextAlign(Align.RIGHT);
            this.paintBatterUnit = new TextPaint();
            this.paintBatterUnit.setTextAlign(Align.LEFT);
            this.paintBatterUnit.setAntiAlias(true);
            this.paintBatterUnit.setColor(-1);
            this.paintBatterUnit.setTextSize(resources.getDimension(R.dimen.digital_sport_two_battery_unit));
            this.paintKaluliUnit = new TextPaint();
            this.paintKaluliUnit.setTextAlign(Align.LEFT);
            this.paintKaluliUnit.setAntiAlias(true);
            this.paintKaluliUnit.setColor(-1);
            this.paintKaluliUnit.setTextSize(resources.getDimension(R.dimen.digital_sport_two_kaluli_unit));
            this.paint_arc_battery = new Paint(1);
            this.paint_arc_battery.setColor(-16711936);
            this.paint_arc_battery.setStrokeWidth(resources.getDimension(R.dimen.digital_sport_two_circle_border));
            this.paint_arc_battery.setStyle(Style.STROKE);
            this.paint_arc_kaluli = new Paint(1);
            this.paint_arc_kaluli.setColor(-65536);
            this.paint_arc_kaluli.setStrokeWidth(resources.getDimension(R.dimen.digital_sport_two_circle_border));
            this.paint_arc_kaluli.setStyle(Style.STROKE);
            this.paint_arc_kaluli.setAntiAlias(true);
            this.paint_arc_step = new Paint(1);
            this.paint_arc_step.setColor(-16776961);
            this.paint_arc_step.setStrokeWidth(resources.getDimension(R.dimen.digital_sport_two_circle_border));
            this.paint_arc_step.setStyle(Style.STROKE);
            this.paint_arc_step.setAntiAlias(true);
            this.left_battery_img = resources.getDimension(R.dimen.digital_sport_two_battery_img_centerx);
            this.top_battery_img = resources.getDimension(R.dimen.digital_sport_two_battery_img__centery);
            this.left_battery_text = resources.getDimension(R.dimen.digital_sport_two_left_battery_text);
            this.top_battery_text = resources.getDimension(R.dimen.digital_sport_two_top_battery_text);
            this.left_hour = resources.getDimension(R.dimen.digital_sport_two_hour_left);
            this.top_hour = resources.getDimension(R.dimen.digital_sport_two_hour_top);
            this.left_minute = resources.getDimension(R.dimen.digital_sport_two_minute_left);
            this.top_minute = resources.getDimension(R.dimen.digital_sport_two_minute_top);
            this.left_kaluli_img = resources.getDimension(R.dimen.digital_sport_two_kaluli_img_left);
            this.top_kaluli_img = resources.getDimension(R.dimen.digital_sport_two_kaluli_img_top);
            this.left_step_img = resources.getDimension(R.dimen.digital_sport_two_step_img_left);
            this.top_step_img = resources.getDimension(R.dimen.digital_sport_two_step_img_top);
            this.left_month = resources.getDimension(R.dimen.digital_sport_two_month_left);
            this.top_month = resources.getDimension(R.dimen.digital_sport_two_month_top);
            this.left_weekly = resources.getDimension(R.dimen.digital_sport_two_weekly_left);
            this.top_weekly = resources.getDimension(R.dimen.digital_sport_two_weekly_top);
            this.left_kaluli_number = resources.getDimension(R.dimen.digital_sport_two_kaluli_number_left);
            this.top_kaluli_number = resources.getDimension(R.dimen.digital_sport_two_kaluli_number_top);
            this.left_kaluli_unit = resources.getDimension(R.dimen.digital_sport_two_kaluli_unit_left);
            this.left_step_number = resources.getDimension(R.dimen.digital_sport_two_step_number_left);
            this.top_step_number = resources.getDimension(R.dimen.digital_sport_two_step_number_top);
            this.left_battery_unit_text = resources.getDimension(R.dimen.digital_sport_two_battery_unit_left);
            this.left_step_unit = resources.getDimension(R.dimen.digital_sport_two_step_unit_left);
            this.img_step_centerX = resources.getDimension(R.dimen.img_step_centerX);
            this.img_step_total_centerX = resources.getDimension(R.dimen.img_step_total_centerX);
            this.mLeftAm = resources.getDimension(R.dimen.digital_sport_two_am_left);
            this.mTopAm = resources.getDimension(R.dimen.digital_sport_two_am_top);
            this.mStepAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mStepAnimator.addUpdateListener(new C02151());
            this.mStepAnimator.addListener(new C02162());
            this.mStepAnimator.setDuration(300);
            registerWatchDataListener(this.mBatteryDataListener);
            registerWatchDataListener(this.mStepDataListener);
            registerWatchDataListener(this.mTodayDistanceDataListener);
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            this.mBound.set(0.0f, 0.0f, width, height);
            canvas.drawBitmap(this.image_background, null, this.mBound, this.paint_bg);
            this.batteryDrawable.setLevel(this.mProgressBattery);
            canvas.drawBitmap(Util.drawableToBitmap(this.batteryDrawable), this.left_battery_img, this.top_battery_img, null);
            canvas.drawText("" + this.mProgressBattery, this.left_battery_text, this.top_battery_text, this.paint_battery);
            this.left_battery_unit_text = ((float) getTextWidth(this.paint_battery, this.mProgressBattery + "")) + centerX;
            canvas.drawText("%", this.left_battery_unit_text, this.top_battery_text, this.paintBatterUnit);
            drawBatteryArc(canvas, centerX, centerY);
            canvas.drawText(Util.formatTime(hours), this.left_hour, this.top_hour, this.hourPaint);
            canvas.drawText(Util.formatTime(minutes), this.left_minute, this.top_minute, this.minutePaint);
            switch (ampm) {
                case 0:
                    canvas.drawBitmap(this.mAmBitmap, this.mLeftAm, this.mTopAm, this.paint_bg);
                    break;
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    canvas.drawBitmap(this.mPmBitmap, this.mLeftAm, this.mTopAm, this.paint_bg);
                    break;
            }
            drawMonthDay(canvas, month, day, this.numberPaint);
            canvas.drawText("" + DigitalWatchFaceSportTwo.this.getApplicationContext().getResources().getStringArray(R.array.weekdays)[week - 1], this.left_weekly, this.top_weekly, this.weeklyPaint);
            canvas.drawBitmap(this.image_kaluli, this.left_kaluli_img, this.top_kaluli_img, null);
            if (this.needUnit) {
                this.left_kaluli_unit = getUnitLeftPosX(this.numberPaint, "" + (this.mOldCurStepCount + this.mStepCompose), this.paintKaluliUnit, this.step, this.img_step_centerX);
                canvas.drawText("" + (this.mOldCurStepCount + this.mStepCompose), this.left_kaluli_unit, this.top_kaluli_number, this.numberPaint);
                canvas.drawText(this.step, this.left_kaluli_unit + 3.0f, this.top_kaluli_number, this.paintKaluliUnit);
            } else {
                this.left_kaluli_unit = getUnitLeftPosX(this.numberPaint, "" + (this.mOldCurStepCount + this.mStepCompose), this.paintKaluliUnit, "", this.img_step_centerX);
                canvas.drawText("" + (this.mOldCurStepCount + this.mStepCompose), this.left_kaluli_unit, this.top_kaluli_number, this.numberPaint);
            }
            drawKaluLiArc(canvas, centerX, centerY);
            canvas.drawBitmap(this.image_step, this.left_step_img, this.top_step_img, null);
            if (this.needUnit) {
                this.left_step_unit = getUnitLeftPosX(this.numberPaint, this.mSportTodayString, this.paintKaluliUnit, getDistanceUnit(), this.img_step_total_centerX);
                canvas.drawText(this.mSportTodayString, this.left_step_unit, this.top_step_number, this.numberPaint);
                canvas.drawText(getDistanceUnit(), this.left_step_unit + 3.0f, this.top_step_number, this.paintKaluliUnit);
            } else {
                this.left_step_unit = getUnitLeftPosX(this.numberPaint, this.mSportTodayString, this.paintKaluliUnit, "", this.img_step_total_centerX);
                canvas.drawText(this.mSportTodayString, this.left_step_unit, this.top_step_number, this.numberPaint);
            }
            drawStepArc(canvas, centerX, centerY);
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                updateTotalStepCount();
            }
        }

        private void updateBatteryLevel(int batteryLevel, int batteryMax) {
            if (batteryMax > 0) {
                float scale = Math.min((((float) batteryLevel) * 1.0f) / ((float) batteryMax), 1.0f);
                this.mProgressBattery = (int) (100.0f * scale);
                this.mProgressDegreeBattery = (int) (87.0f * scale);
                return;
            }
            this.mProgressBattery = 0;
            this.mProgressDegreeBattery = 0;
        }

        private void drawMonthDay(Canvas canvas, int month, int day, Paint dataPaint) {
            canvas.drawText(Util.formatTime(month) + "-" + Util.formatTime(day), this.left_month, this.top_month, dataPaint);
        }

        private void updateTotalStepCount() {
            String str = WatchSettings.get(DigitalWatchFaceSportTwo.this.getBaseContext().getContentResolver(), "huami.watch.health.config");
            if (str != null) {
                try {
                    this.mTotalStepTarget = new JSONObject(str).optInt("step_target", 8000);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void drawBatteryArc(Canvas canvas, float centerX, float centerY) {
            float degree = (float) this.mProgressDegreeBattery;
            canvas.save();
            canvas.drawArc(centerX - this.mRadius, centerY - this.mRadius, centerX + this.mRadius, centerY + this.mRadius, -134.0f, degree, false, this.paint_arc_battery);
            canvas.restore();
        }

        private void drawKaluLiArc(Canvas canvas, float centerX, float centerY) {
            int degree = (int) (77.0f * Math.min((((float) (this.mOldCurStepCount + this.mStepCompose)) * 1.0f) / ((float) this.mTotalStepTarget), 1.0f));
            canvas.save();
            canvas.drawArc(centerX - this.mRadius, centerY - this.mRadius, centerX + this.mRadius, centerY + this.mRadius, 129.0f, (float) degree, false, this.paint_arc_kaluli);
            canvas.restore();
        }

        private void drawStepArc(Canvas canvas, float centerX, float centerY) {
            int degree = this.mProgressDegreeStep;
            canvas.save();
            canvas.drawArc(centerX - this.mRadius, centerY - this.mRadius, centerX + this.mRadius, centerY + this.mRadius, 51.0f, (float) (-degree), false, this.paint_arc_step);
            canvas.restore();
        }

        private void updateSportTodayDistance() {
            if (this.mSportTodayDistance > 0.0d) {
                this.mProgressDegreeStep = (int) (Math.min(this.mSportTodayDistance / 3.0d, 1.0d) * 77.0d);
            } else {
                this.mProgressDegreeStep = 0;
            }
            this.mSportTodayString = Util.getFormatDistance(this.mSportTodayDistance);
        }

        protected void onMeasurementChanged(int measurement) {
            updateSportTodayDistance();
            invalidate();
        }

        public int getTextWidth(Paint paint, String str) {
            int iRet = 0;
            if (str != null && str.length() > 0) {
                int len = str.length();
                float[] widths = new float[len];
                paint.getTextWidths(str, widths);
                for (int j = 0; j < len; j++) {
                    iRet += (int) Math.ceil((double) widths[j]);
                }
            }
            return iRet;
        }

        private float getUnitLeftPosX(Paint numberPaint, String numberText, Paint unitPaint, String unitString, float upImgCenter) {
            return ((float) ((getTextWidth(numberPaint, numberText) - getTextWidth(unitPaint, unitString)) / 2)) + upImgCenter;
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return DigitalWatchFaceSportTwoSlpt.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(39.0f);
        return new Engine();
    }
}
