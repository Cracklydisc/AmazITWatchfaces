package com.huami.watch.watchface;

import android.content.Context;
import android.graphics.Typeface;
import android.text.format.DateFormat;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.arc.SlptPowerArcAnglePicView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayDistanceArcAnglePicView;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptBatteryView;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.sport.SlptPowerNumView;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.sport.SlptTodaySportDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptTodaySportDistanceLView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalWatchFaceSportTwoSlpt extends AbstractSlptClock {
    private SlptViewComponent lowBatteryView;
    private Context mContext;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -16711936, -16777216);
        this.lowBatteryView.setStart(10, 40);
        this.lowBatteryView.setRect(320, getResources().getInteger(R.integer.low_battery_tips_hight));
    }

    protected SlptLayout createClockLayout8C() {
        int i;
        byte[] kmIconMem;
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent hourLayout = new SlptLinearLayout();
        SlptViewComponent minuteLayout = new SlptLinearLayout();
        SlptBatteryView batteryView = new SlptBatteryView(14);
        SlptViewComponent hourHView = new SlptHourHView();
        SlptViewComponent hourLView = new SlptHourLView();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptViewComponent runIconView = new SlptPictureView();
        SlptViewComponent powerNumLayout = new SlptLinearLayout();
        SlptPowerNumView powerNumView = new SlptPowerNumView();
        SlptViewComponent powerTitle = new SlptPictureView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptPowerArcAnglePicView arcPicPower = new SlptPowerArcAnglePicView();
        SlptTodayStepArcAnglePicView arcPicStep = new SlptTodayStepArcAnglePicView();
        SlptTodayDistanceArcAnglePicView arcPicDistance = new SlptTodayDistanceArcAnglePicView();
        SlptViewComponent stepNumLayout = new SlptLinearLayout();
        SlptViewComponent mStepNum = new SlptTodayStepNumView();
        SlptViewComponent mStepTitle = new SlptPictureView();
        SlptViewComponent todaySportLayout = new SlptLinearLayout();
        SlptViewComponent todaySDFView = new SlptTodaySportDistanceFView();
        SlptViewComponent todaySDFSeqView = new SlptPictureView();
        SlptViewComponent todaySDLView = new SlptTodaySportDistanceLView();
        SlptViewComponent kmText2View = new SlptPictureView();
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        byte[][] small_num = new byte[10][];
        byte[][] week_num = new byte[7][];
        for (i = 0; i < 10; i++) {
            small_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/IamRunner/small_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        String str;
        if (Util.needEnAssets()) {
            str = new String("guard/IamRunner/en/week_%d.png");
        } else {
            str = new String("guard/IamRunner/week_%d.png");
        }
        for (i = 0; i < 7; i++) {
            week_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        byte[][] battery_num = new byte[14][];
        hourLayout.add(hourHView);
        hourLayout.add(hourLView);
        minuteLayout.add(minuteHView);
        minuteLayout.add(minuteLView);
        powerNumLayout.add(batteryView);
        powerNumLayout.add(powerNumView);
        powerNumLayout.add(powerTitle);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        todaySportLayout.add(todaySDFView);
        todaySportLayout.add(todaySDFSeqView);
        todaySportLayout.add(todaySDLView);
        if (!Util.needEnAssets()) {
            todaySportLayout.add(kmText2View);
        }
        stepNumLayout.add(mStepNum);
        if (!Util.needEnAssets()) {
            stepNumLayout.add(mStepTitle);
        }
        linearLayout.add(slptBgView1);
        linearLayout.add(stepIconView);
        linearLayout.add(runIconView);
        linearLayout.add(arcPicPower);
        linearLayout.add(arcPicDistance);
        linearLayout.add(arcPicStep);
        linearLayout.add(todaySportLayout);
        linearLayout.add(stepNumLayout);
        linearLayout.add(hourLayout);
        linearLayout.add(minuteLayout);
        linearLayout.add(monthLayout);
        linearLayout.add(weekView);
        linearLayout.add(powerNumLayout);
        linearLayout.add(this.lowBatteryView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/watchface_runner_bg.png"));
        arcPicPower.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/runner_power.png"));
        arcPicStep.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/runner_step.png"));
        arcPicDistance.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/runner_km.png"));
        if (Util.getMeasurement(this.mContext) == 1) {
            kmIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/en/mi.png");
        } else {
            kmIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/km.png");
        }
        byte[] stepIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/step.png");
        byte[] percentIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/percentage.png");
        byte[] pointMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/point.png");
        byte[] monthSeqMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/small_seq.png");
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/watchface_icon_step.png"));
        stepIconView.setStart(63, 127);
        runIconView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/watchface_icon_run.png"));
        runIconView.setStart(231, 128);
        if (!DateFormat.is24HourFormat(this.mContext)) {
            byte[] memAM = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/am.png");
            SlptPictureView amView = new SlptPictureView();
            amView.setImagePicture(memAM);
            SlptSportUtil.setAmBgView(amView);
            amView.setStart(210, 90);
            byte[] memPM = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/pm.png");
            SlptViewComponent pmView = new SlptPictureView();
            pmView.setImagePicture(memPM);
            SlptSportUtil.setPmBgView(pmView);
            pmView.setStart(210, 90);
            linearLayout.add(amView);
            linearLayout.add(pmView);
        }
        for (i = 0; i < 14; i++) {
            battery_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/IamRunner/battery_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        batteryView.setImagePictureArray(battery_num);
        kmText2View.setImagePicture(kmIconMem);
        todaySDFSeqView.setImagePicture(pointMem);
        todaySDFSeqView.id = (short) 20;
        monthSeqView.setImagePicture(monthSeqMem);
        mStepTitle.setImagePicture(stepIconMem);
        powerTitle.setImagePicture(percentIconMem);
        Typeface hourTypeface = TypefaceManager.get().createFromAsset("typeface/DINPro-Medium.otf");
        Typeface minuteTypeface = TypefaceManager.get().createFromAsset("typeface/DINPro-Light.otf");
        hourHView.setTextAttr(96.0f, -1, hourTypeface);
        hourLView.setTextAttr(96.0f, -1, hourTypeface);
        minuteHView.setTextAttr(96.0f, -1, minuteTypeface);
        minuteLView.setTextAttr(96.0f, -1, minuteTypeface);
        hourLayout.setStart(107, 56);
        hourLayout.setRect(103, 102);
        minuteLayout.setStart(110, 134);
        minuteLayout.setRect(103, 102);
        powerNumView.setImagePictureArray(small_num);
        powerNumLayout.setStart(128, 42);
        powerNumLayout.setRect(2147483646, 23);
        powerNumLayout.alignX = (byte) 0;
        powerNumLayout.alignY = (byte) 2;
        mStepNum.setImagePictureArray(small_num);
        mStepTitle.setPadding(2, 0, 0, 0);
        stepNumLayout.setStart(41, 164);
        stepNumLayout.setRect(73, 23);
        stepNumLayout.alignX = (byte) 2;
        todaySDFView.setImagePictureArray(small_num);
        todaySDLView.setImagePictureArray(small_num);
        kmText2View.setPadding(3, 0, 0, 3);
        todaySportLayout.setStart(206, 166);
        todaySportLayout.setRect(73, 23);
        todaySportLayout.alignX = (byte) 2;
        todaySportLayout.alignY = (byte) 0;
        monthLayout.setStart(103, 254);
        monthLayout.setRect(57, 28);
        monthLayout.setImagePictureArrayForAll(small_num);
        monthLayout.alignX = (byte) 2;
        monthLayout.alignY = (byte) 1;
        weekView.setStart(163, 264);
        weekView.setImagePictureArray(week_num);
        arcPicPower.start_angle = 316;
        arcPicPower.len_angle = 0;
        arcPicPower.full_angle = 87;
        arcPicStep.start_angle = 219;
        arcPicStep.len_angle = 0;
        arcPicStep.full_angle = 77;
        arcPicDistance.start_angle = 64;
        arcPicDistance.len_angle = 0;
        arcPicDistance.full_angle = 77;
        arcPicDistance.draw_clockwise = 0;
        return linearLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        int i;
        byte[] kmIconMem;
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptViewComponent hourLayout = new SlptLinearLayout();
        SlptViewComponent minuteLayout = new SlptLinearLayout();
        SlptBatteryView batteryView = new SlptBatteryView(14);
        SlptViewComponent hourHView = new SlptHourHView();
        SlptViewComponent hourLView = new SlptHourLView();
        SlptViewComponent minuteHView = new SlptMinuteHView();
        SlptViewComponent minuteLView = new SlptMinuteLView();
        SlptViewComponent stepIconView = new SlptPictureView();
        SlptViewComponent runIconView = new SlptPictureView();
        SlptViewComponent powerNumLayout = new SlptLinearLayout();
        SlptPowerNumView powerNumView = new SlptPowerNumView();
        SlptViewComponent powerTitle = new SlptPictureView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptPowerArcAnglePicView arcPicPower = new SlptPowerArcAnglePicView();
        SlptTodayStepArcAnglePicView arcPicStep = new SlptTodayStepArcAnglePicView();
        SlptTodayDistanceArcAnglePicView arcPicDistance = new SlptTodayDistanceArcAnglePicView();
        SlptViewComponent stepNumLayout = new SlptLinearLayout();
        SlptViewComponent mStepNum = new SlptTodayStepNumView();
        SlptViewComponent mStepTitle = new SlptPictureView();
        SlptViewComponent todaySportLayout = new SlptLinearLayout();
        SlptViewComponent todaySDFView = new SlptTodaySportDistanceFView();
        SlptViewComponent todaySDFSeqView = new SlptPictureView();
        SlptViewComponent todaySDLView = new SlptTodaySportDistanceLView();
        SlptViewComponent kmText2View = new SlptPictureView();
        SlptViewComponent monthLayout = new SlptLinearLayout();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        byte[][] small_num = new byte[10][];
        byte[][] week_num = new byte[7][];
        for (i = 0; i < 10; i++) {
            small_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/IamRunner/small_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        String str;
        if (Util.needEnAssets()) {
            str = new String("guard/IamRunner/en/week_%d.png");
        } else {
            str = new String("guard/IamRunner/week_%d.png");
        }
        for (i = 0; i < 7; i++) {
            week_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        byte[][] battery_num = new byte[14][];
        hourLayout.add(hourHView);
        hourLayout.add(hourLView);
        minuteLayout.add(minuteHView);
        minuteLayout.add(minuteLView);
        powerNumLayout.add(batteryView);
        powerNumLayout.add(powerNumView);
        powerNumLayout.add(powerTitle);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        todaySportLayout.add(todaySDFView);
        todaySportLayout.add(todaySDFSeqView);
        todaySportLayout.add(todaySDLView);
        if (!Util.needEnAssets()) {
            todaySportLayout.add(kmText2View);
        }
        stepNumLayout.add(mStepNum);
        if (!Util.needEnAssets()) {
            stepNumLayout.add(mStepTitle);
        }
        linearLayout.add(slptBgView1);
        linearLayout.add(stepIconView);
        linearLayout.add(runIconView);
        linearLayout.add(arcPicPower);
        linearLayout.add(arcPicDistance);
        linearLayout.add(arcPicStep);
        linearLayout.add(todaySportLayout);
        linearLayout.add(stepNumLayout);
        linearLayout.add(hourLayout);
        linearLayout.add(minuteLayout);
        linearLayout.add(monthLayout);
        linearLayout.add(weekView);
        linearLayout.add(powerNumLayout);
        linearLayout.add(this.lowBatteryView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/26WC/watchface_runner_bg.png"));
        arcPicPower.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/26WC/runner_power.png"));
        arcPicStep.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/26WC/runner_step.png"));
        arcPicDistance.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/26WC/runner_km.png"));
        if (Util.getMeasurement(this.mContext) == 1) {
            kmText2View.setStringPicture("mi");
        } else {
            kmText2View.setStringPicture("km");
        }
        stepIconView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/26WC/watchface_icon_step.png"));
        stepIconView.setStart(63, 127);
        runIconView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/26WC/watchface_icon_run.png"));
        runIconView.setStart(231, 128);
        if (!DateFormat.is24HourFormat(this.mContext)) {
            byte[] memAM = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/26WC/am.png");
            SlptPictureView amView = new SlptPictureView();
            amView.setImagePicture(memAM);
            SlptSportUtil.setAmBgView(amView);
            amView.setStart(210, 90);
            byte[] memPM = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/26WC/pm.png");
            SlptViewComponent pmView = new SlptPictureView();
            pmView.setImagePicture(memPM);
            SlptSportUtil.setPmBgView(pmView);
            pmView.setStart(210, 90);
            linearLayout.add(amView);
            linearLayout.add(pmView);
        }
        for (i = 0; i < 14; i++) {
            battery_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("guard/IamRunner/battery_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        batteryView.setImagePictureArray(battery_num);
        if (Util.getMeasurement(this.mContext) == 1) {
            kmIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/en/mi.png");
        } else {
            kmIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/km.png");
        }
        byte[] stepIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/step.png");
        byte[] percentIconMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/percentage.png");
        byte[] pointMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/point.png");
        byte[] monthSeqMem = SimpleFile.readFileFromAssets(this.mContext, "guard/IamRunner/small_seq.png");
        kmText2View.setImagePicture(kmIconMem);
        todaySDFSeqView.setImagePicture(pointMem);
        todaySDFSeqView.id = (short) 20;
        monthSeqView.setImagePicture(monthSeqMem);
        mStepTitle.setImagePicture(stepIconMem);
        powerTitle.setImagePicture(percentIconMem);
        Typeface hourTypeface = TypefaceManager.get().createFromAsset("typeface/DINPro-Medium.otf");
        Typeface minuteTypeface = TypefaceManager.get().createFromAsset("typeface/DINPro-Light.otf");
        hourHView.setTextAttr(this.mContext.getResources().getDimension(R.dimen.digital_sport_two_hour_font), -1, hourTypeface);
        hourLView.setTextAttr(this.mContext.getResources().getDimension(R.dimen.digital_sport_two_hour_font), -1, hourTypeface);
        minuteHView.setTextAttr(this.mContext.getResources().getDimension(R.dimen.digital_sport_two_minute_font), -1, minuteTypeface);
        minuteLView.setTextAttr(this.mContext.getResources().getDimension(R.dimen.digital_sport_two_minute_font), -1, minuteTypeface);
        hourLayout.setStart(107, 56);
        hourLayout.setRect(103, 102);
        minuteLayout.setStart(110, 134);
        minuteLayout.setRect(103, 102);
        powerNumView.setImagePictureArray(small_num);
        powerNumLayout.setStart(128, 42);
        powerNumLayout.setRect(2147483646, 23);
        powerNumLayout.alignX = (byte) 0;
        powerNumLayout.alignY = (byte) 2;
        mStepNum.setImagePictureArray(small_num);
        mStepTitle.setPadding(2, 0, 0, 0);
        stepNumLayout.setStart(41, 164);
        stepNumLayout.setRect(73, 23);
        stepNumLayout.alignX = (byte) 2;
        todaySDFView.setImagePictureArray(small_num);
        todaySDLView.setImagePictureArray(small_num);
        kmText2View.setPadding(3, 0, 0, 3);
        todaySportLayout.setStart(206, 166);
        todaySportLayout.setRect(73, 23);
        todaySportLayout.alignX = (byte) 2;
        todaySportLayout.alignY = (byte) 0;
        monthLayout.setStart(103, 254);
        monthLayout.setRect(57, 28);
        monthLayout.setImagePictureArrayForAll(small_num);
        monthLayout.alignX = (byte) 2;
        monthLayout.alignY = (byte) 1;
        weekView.setStart(163, 264);
        weekView.setImagePictureArray(week_num);
        arcPicPower.start_angle = 316;
        arcPicPower.len_angle = 0;
        arcPicPower.full_angle = 87;
        arcPicStep.start_angle = 219;
        arcPicStep.len_angle = 0;
        arcPicStep.full_angle = 77;
        arcPicDistance.start_angle = 64;
        arcPicDistance.len_angle = 0;
        arcPicDistance.full_angle = 77;
        arcPicDistance.draw_clockwise = 0;
        return linearLayout;
    }
}
