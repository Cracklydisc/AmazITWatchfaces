package com.huami.watch.watchface;

import android.content.Context;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptBatteryView;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepPicGroupView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class DigitalWatchFaceSporttwelveSlpt extends AbstractSlptClock {
    private byte[][] date_num = new byte[10][];
    private String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private SlptViewComponent lowBatteryView = null;
    private Context mContext;
    private byte[][] small_step_num = new byte[10][];
    private byte[][] step_process_num = new byte[13][];
    private byte[][] timer_num = new byte[10][];
    private byte[][] week_num = new byte[7][];

    private void init_num_mem() {
        int i;
        String path;
        for (i = 0; i < 13; i++) {
            this.step_process_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/huanyingMC/step_pic_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.timer_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/huanyingMC/time_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.date_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/huanyingMC/date_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        if (Util.needEnAssets()) {
            path = new String("guard/huanyingMC/en/week_%d.png");
        } else {
            path = new String("guard/huanyingMC/week_%d.png");
        }
        for (i = 0; i < 7; i++) {
            this.week_num[i] = SimpleFile.readFileFromAssets(this, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
        for (i = 0; i < 10; i++) {
            this.small_step_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/huanyingMC/step_num_%d.png", new Object[]{Integer.valueOf(i)}));
        }
    }

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        init_num_mem();
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -65536, 0);
        this.lowBatteryView.setStart(0, 50);
        this.lowBatteryView.setRect(320, this.mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
    }

    protected SlptLayout createClockLayout8C() {
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptBatteryView batteryView = new SlptBatteryView(6);
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptViewComponent timeSeqView = new SlptPictureView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptLinearLayout monthLayout = new SlptLinearLayout();
        SlptMonthHView monthHView = new SlptMonthHView();
        SlptMonthLView monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptViewComponent stepNumView = new SlptTodayStepNumView();
        SlptViewComponent slptTodayStepPicGroupView = new SlptTodayStepPicGroupView(13);
        byte[][] battery_num = new byte[6][];
        timeLayout.add(hourHView);
        timeLayout.add(hourLView);
        timeLayout.add(timeSeqView);
        timeLayout.add(minuteHView);
        timeLayout.add(minuteLView);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        monthLayout.add(weekView);
        linearLayout.add(slptBgView1);
        linearLayout.add(monthLayout);
        linearLayout.add(timeLayout);
        linearLayout.add(stepNumView);
        linearLayout.add(batteryView);
        linearLayout.add(slptTodayStepPicGroupView);
        linearLayout.add(this.lowBatteryView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/huanyingMC/watchface_default_bg_8c.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/huanyingMC/date_minus.png"));
        timeSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/huanyingMC/colon.png"));
        for (int i = 0; i < 6; i++) {
            battery_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/huanyingMC/battery_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        batteryView.setImagePictureArray(battery_num);
        hourHView.setImagePictureArray(this.timer_num);
        hourLView.setImagePictureArray(this.timer_num);
        minuteHView.setImagePictureArray(this.timer_num);
        minuteLView.setImagePictureArray(this.timer_num);
        monthHView.setImagePictureArray(this.date_num);
        monthLView.setImagePictureArray(this.date_num);
        dayHView.setImagePictureArray(this.date_num);
        dayLView.setImagePictureArray(this.date_num);
        stepNumView.setImagePictureArray(this.small_step_num);
        weekView.setImagePictureArray(this.week_num);
        timeLayout.setStart(61, 167);
        monthLayout.setStart(0, 238);
        monthLayout.setRect(320, 26);
        weekView.setPadding(13, 0, 0, 0);
        monthLayout.alignX = (byte) 2;
        monthLayout.alignY = (byte) 0;
        slptTodayStepPicGroupView.setImagePictureArray(this.step_process_num);
        slptTodayStepPicGroupView.setStart(0, 205);
        stepNumView.setStart(31, 112);
        stepNumView.setRect(114, 18);
        stepNumView.alignX = (byte) 2;
        slptTodayStepPicGroupView.setStart(31, 50);
        slptTodayStepPicGroupView.setRect(114, 114);
        batteryView.setStart(208, 38);
        return linearLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
        SlptBatteryView batteryView = new SlptBatteryView(6);
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptViewComponent timeSeqView = new SlptPictureView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        SlptViewComponent slptBgView1 = new SlptPictureView();
        SlptLinearLayout monthLayout = new SlptLinearLayout();
        SlptMonthHView monthHView = new SlptMonthHView();
        SlptMonthLView monthLView = new SlptMonthLView();
        SlptViewComponent monthSeqView = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptViewComponent stepNumView = new SlptTodayStepNumView();
        SlptViewComponent slptTodayStepPicGroupView = new SlptTodayStepPicGroupView(13);
        byte[][] battery_num = new byte[6][];
        timeLayout.add(hourHView);
        timeLayout.add(hourLView);
        timeLayout.add(timeSeqView);
        timeLayout.add(minuteHView);
        timeLayout.add(minuteLView);
        monthLayout.add(monthHView);
        monthLayout.add(monthLView);
        monthLayout.add(monthSeqView);
        monthLayout.add(dayHView);
        monthLayout.add(dayLView);
        monthLayout.add(weekView);
        linearLayout.add(this.lowBatteryView);
        linearLayout.add(slptBgView1);
        linearLayout.add(monthLayout);
        linearLayout.add(timeLayout);
        linearLayout.add(stepNumView);
        linearLayout.add(batteryView);
        linearLayout.add(slptTodayStepPicGroupView);
        linearLayout.background.color = -16777216;
        slptBgView1.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/huanyingMC/watchface_default_bg_26wc.png"));
        monthSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/huanyingMC/date_minus.png"));
        timeSeqView.setImagePicture(SimpleFile.readFileFromAssets(this, "guard/huanyingMC/colon.png"));
        for (int i = 0; i < 6; i++) {
            battery_num[i] = SimpleFile.readFileFromAssets(this, String.format("guard/huanyingMC/battery_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        batteryView.setImagePictureArray(battery_num);
        hourHView.setImagePictureArray(this.timer_num);
        hourLView.setImagePictureArray(this.timer_num);
        minuteHView.setImagePictureArray(this.timer_num);
        minuteLView.setImagePictureArray(this.timer_num);
        monthHView.setImagePictureArray(this.date_num);
        monthLView.setImagePictureArray(this.date_num);
        dayHView.setImagePictureArray(this.date_num);
        dayLView.setImagePictureArray(this.date_num);
        stepNumView.setImagePictureArray(this.small_step_num);
        weekView.setImagePictureArray(this.week_num);
        timeLayout.setStart(61, 167);
        monthLayout.setStart(0, 238);
        monthLayout.setRect(320, 26);
        weekView.setPadding(13, 0, 0, 0);
        monthLayout.alignX = (byte) 2;
        monthLayout.alignY = (byte) 0;
        slptTodayStepPicGroupView.setImagePictureArray(this.step_process_num);
        slptTodayStepPicGroupView.setStart(0, 205);
        stepNumView.setStart(31, 112);
        stepNumView.setRect(114, 18);
        stepNumView.alignX = (byte) 2;
        slptTodayStepPicGroupView.setStart(31, 50);
        slptTodayStepPicGroupView.setRect(114, 114);
        batteryView.setStart(208, 38);
        return linearLayout;
    }
}
