package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.util.WatchFaceConfig;
import com.huami.watch.watchface.util.WatchFaceConfig.ImageFontInfo;
import com.huami.watch.watchface.util.WatchFaceConfig.TimeDigital;
import com.huami.watch.watchface.util.WatchFaceConfig.TimeHand;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.huami.watch.watchface.widget.AbsWatchFaceDataWidget;
import com.huami.watch.watchface.widget.TimeDigitalWidget;
import java.util.ArrayList;

public class ExternalWatchFace extends AbstractWatchFace {

    private class Engine extends AnalogEngine {
        private WatchFaceConfig config;
        private Drawable mBgDrawable;
        private Path mClip;
        private Drawable mGraduation;
        private Drawable mHour;
        private Drawable mMinute;
        private Drawable mSeconds;

        private Engine() {
            super();
            this.mClip = new Path();
        }

        protected void onPrepareResources(Resources resources) {
            this.mClip.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.config = WatchFaceConfig.getWatchFaceConfig(ExternalWatchFace.this.getApplicationContext(), "external_watchface");
            if (this.config != null) {
                float density = resources.getDisplayMetrics().density;
                float x = (float) this.config.getStatusBarPosX();
                float y = (float) this.config.getStatusBarPosY();
                if (x <= -2.14748365E9f) {
                    x = -1.0f;
                } else if (density != 0.0f) {
                    x /= resources.getDisplayMetrics().density;
                } else {
                    x /= 1.5f;
                }
                if (y <= -2.14748365E9f) {
                    y = -1.0f;
                } else if (density != 0.0f) {
                    y /= resources.getDisplayMetrics().density;
                } else {
                    y /= 1.5f;
                }
                ExternalWatchFace.this.notifyStatusBarPosition(x, y);
            }
            if (this.config != null) {
                switch (this.config.getBgType()) {
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    case 4:
                    case 6:
                        this.mBgDrawable = this.config.getBgDrawable();
                        break;
                }
            }
            if (this.mBgDrawable != null) {
                this.mBgDrawable.setBounds(0, 0, 320, 320);
            }
            if (this.config != null) {
                this.mGraduation = this.config.getGraduation();
            }
            if (this.mGraduation != null) {
                this.mGraduation.setBounds(0, 0, 320, 320);
            }
            if (this.config != null) {
                TimeHand timeHand = this.config.getTimeHand();
                if (timeHand != null) {
                    this.mHour = timeHand.getHour();
                    this.mMinute = timeHand.getMinute();
                    this.mSeconds = timeHand.getSeconds();
                }
            }
            if (this.mHour != null) {
                int left = 160 - (this.mHour.getIntrinsicWidth() / 2);
                int top = 160 - (this.mHour.getIntrinsicHeight() / 2);
                this.mHour.setBounds(left, top, this.mHour.getIntrinsicWidth() + left, this.mHour.getIntrinsicHeight() + top);
            }
            if (this.mMinute != null) {
                left = 160 - (this.mMinute.getIntrinsicWidth() / 2);
                top = 160 - (this.mMinute.getIntrinsicHeight() / 2);
                this.mMinute.setBounds(left, top, this.mMinute.getIntrinsicWidth() + left, this.mMinute.getIntrinsicHeight() + top);
            }
            if (this.mSeconds != null) {
                left = 160 - (this.mSeconds.getIntrinsicWidth() / 2);
                top = 160 - (this.mSeconds.getIntrinsicHeight() / 2);
                this.mSeconds.setBounds(left, top, this.mSeconds.getIntrinsicWidth() + left, this.mSeconds.getIntrinsicHeight() + top);
            }
            if (this.config != null) {
                TimeDigital timeDigital = this.config.getTimeDigital();
                if (timeDigital != null) {
                    Log.d("ExternalWatchFace", timeDigital.toString());
                    ImageFontInfo fontInfo = timeDigital.getImageFontInfo();
                    if (fontInfo != null) {
                        addWidget(new TimeDigitalWidget(timeDigital.getModel(), timeDigital.getX(), timeDigital.getY(), timeDigital.getWidth(), timeDigital.getHeight(), fontInfo.parseImageFont(), timeDigital.getColor(), timeDigital.getGap()));
                    }
                }
            }
            if (this.config != null) {
                ArrayList<DataWidgetConfig> mWidgets = this.config.getDataWidgets();
                for (int j = 0; j < mWidgets.size(); j++) {
                    AbsWatchFaceDataWidget widget = getDataWidget(resources, (DataWidgetConfig) mWidgets.get(j));
                    if (widget != null) {
                        addWidget(widget);
                    }
                }
            }
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.clipPath(this.mClip);
            if (this.mBgDrawable != null) {
                this.mBgDrawable.draw(canvas);
            }
            if (this.mGraduation != null) {
                this.mGraduation.draw(canvas);
            }
            super.onDrawAnalog(canvas, width, height, centerX, centerY, secRot, minRot, hrRot);
            if (this.mHour != null) {
                canvas.save();
                canvas.rotate(hrRot, centerX, centerY);
                this.mHour.draw(canvas);
                canvas.restore();
            }
            if (this.mMinute != null) {
                canvas.save();
                canvas.rotate(minRot, centerX, centerY);
                this.mMinute.draw(canvas);
                canvas.restore();
            }
            if (this.mSeconds != null) {
                canvas.save();
                canvas.rotate(secRot, centerX, centerY);
                this.mSeconds.draw(canvas);
                canvas.restore();
            }
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return ExternalWatchFaceSlpt.class;
    }

    public Engine onCreateEngine() {
        return new Engine();
    }
}
