package com.huami.watch.watchface;

import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.util.WatchFaceConfig;
import com.huami.watch.watchface.util.WatchFaceConfig.ImageFontInfo;
import com.huami.watch.watchface.util.WatchFaceConfig.TimeDigital;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.huami.watch.watchface.widget.slpt.SlptDigitalTimeWidget;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogHourWithMinuteView;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogMinuteView;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogSecondView;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedHourWithMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedSecondView;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.util.ArrayList;

public class ExternalWatchFaceSlpt extends AbstractSlptClock {
    private WatchFaceConfig config = null;
    private SlptLinearLayout lowBatteryLayout = new SlptLinearLayout();
    private boolean needRefreshSecond = false;

    protected void initWatchFaceConfig() {
        SlptPictureView lowBatteryPic = new SlptPictureView();
        clearDrawdPictureGroup();
        this.lowBatteryLayout.clear();
        this.needRefreshSecond = Util.needSlptRefreshSecond(this).booleanValue();
        if (this.needRefreshSecond) {
            setClockPeriodSecond(true);
        }
        this.config = WatchFaceConfig.getWatchFaceConfig(getApplicationContext(), "external_watchface");
        if (this.config != null && this.config.getLowPowerY() != Integer.MIN_VALUE) {
            byte[] lowBatteryViewMem = null;
            switch (this.config.getLowPowerIconType()) {
                case 2:
                    lowBatteryViewMem = SimpleFile.readFileFromAssets(this, this.config.getLowPowerIconPath());
                    break;
                case 6:
                    lowBatteryViewMem = this.config.parseFile(6, this.config.getLowPowerIconPath());
                    break;
            }
            if (lowBatteryViewMem == null) {
                this.lowBatteryLayout.add(SportItemInfoWrapper.createLowBatteryView(this, -65536, 0));
            } else {
                lowBatteryPic.setImagePicture(lowBatteryViewMem);
                SlptSportUtil.setLowBatteryIconView(lowBatteryPic);
                this.lowBatteryLayout.add(lowBatteryPic);
            }
            if (this.config.getLowPowerX() == Integer.MIN_VALUE) {
                this.lowBatteryLayout.setStart(0, this.config.getLowPowerY());
                this.lowBatteryLayout.setRect(320, 2147483646);
                this.lowBatteryLayout.alignX = (byte) 2;
                return;
            }
            this.lowBatteryLayout.setStart(this.config.getLowPowerX(), this.config.getLowPowerY());
        }
    }

    protected SlptLayout createClockLayout8C() {
        if (this.config == null) {
            Log.i("ExternalWatchFaceSlpt", "custome config is null");
            return null;
        }
        int i;
        SlptLayout frameLayout = new SlptAbsoluteLayout();
        frameLayout.background.color = -16777216;
        SlptViewComponent bgView = new SlptPictureView();
        SlptViewComponent bgView2 = new SlptPictureView();
        byte[] bgMem = null;
        switch (this.config.getBgType()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                bgMem = SimpleFile.readFileFromAssets(this, this.config.getBgPathSlpt());
                break;
            case 4:
                bgMem = SimpleFile.readFile(this.config.getBgPathSlpt());
                break;
            case 6:
                bgMem = this.config.parseFile(6, this.config.getBgPathSlpt());
                break;
        }
        if (bgMem != null) {
            bgView.setImagePicture(bgMem);
            frameLayout.add(bgView);
        }
        byte[] graduationMem = null;
        switch (this.config.getGraduationType()) {
            case 2:
                graduationMem = SimpleFile.readFileFromAssets(this, this.config.getGraduationPathSlpt());
                break;
            case 6:
                graduationMem = this.config.parseFile(6, this.config.getGraduationPathSlpt());
                break;
        }
        if (graduationMem != null) {
            bgView2.setImagePicture(graduationMem);
            frameLayout.add(bgView2);
        }
        TimeDigital timeDigital = this.config.getTimeDigital();
        if (timeDigital != null) {
            Log.d("ExternalWatchFaceSlpt", timeDigital.toString());
            ImageFontInfo fontInfo = timeDigital.getImageFontInfo();
            if (fontInfo != null) {
                byte[][] time_num = new byte[10][];
                for (i = 0; i < 10; i++) {
                    time_num[i] = fontInfo.parseCharData(Character.forDigit(i, 10), timeDigital.getColor());
                }
                byte[] maohao = fontInfo.parseCharData(':', timeDigital.getColor());
                byte[] a = fontInfo.parseCharData('A', timeDigital.getColor());
                byte[] p = fontInfo.parseCharData('P', timeDigital.getColor());
                byte[] m = fontInfo.parseCharData('M', timeDigital.getColor());
                SlptDigitalTimeWidget timeWidget = new SlptDigitalTimeWidget(this, timeDigital.getX(), timeDigital.getY(), timeDigital.getWidth(), timeDigital.getHeight(), 0);
                timeWidget.setTimeView(time_num);
                timeWidget.setColonView(maohao);
                timeWidget.setAPMview(a, p, m);
                frameLayout.add(timeWidget.getWidgetView());
            }
        }
        ArrayList<DataWidgetConfig> mWidgets = this.config.getDataWidgets();
        for (int j = 0; j < mWidgets.size(); j++) {
            SlptViewComponent widgetView = get8cDataWidget((DataWidgetConfig) mWidgets.get(j));
            if (widgetView != null) {
                frameLayout.add(widgetView);
            }
        }
        String assetsMinutePathFormat = this.config.getMinutePathSlpt();
        String assetsHourPathFormat = this.config.getHourPathSlpt();
        String assetsSecondPathFormat = this.config.getSecondsPathSlpt();
        PreDrawedPictureGroup preDrawedPictureGroup;
        SlptViewComponent hourView;
        SlptViewComponent minuteView;
        SlptViewComponent secondView;
        switch (this.config.getTimeHandType()) {
            case 2:
                if (assetsHourPathFormat != null) {
                    String[] assetsHourPath = new String[16];
                    for (i = 0; i < assetsHourPath.length; i++) {
                        assetsHourPath[i] = String.format(assetsHourPathFormat, new Object[]{Integer.valueOf(i)});
                    }
                    preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsHourPath);
                    super.addDrawedPictureGroup8C(preDrawedPictureGroup);
                    hourView = new SlptPredrawedHourWithMinuteView();
                    hourView.setPreDrawedPicture(preDrawedPictureGroup);
                    hourView.setStart(0, 0);
                    hourView.setRect(320, 320);
                    frameLayout.add(hourView);
                }
                if (assetsMinutePathFormat != null) {
                    String[] assetsMinutePath = new String[16];
                    for (i = 0; i < assetsMinutePath.length; i++) {
                        assetsMinutePath[i] = String.format(assetsMinutePathFormat, new Object[]{Integer.valueOf(i)});
                    }
                    preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsMinutePath);
                    super.addDrawedPictureGroup8C(preDrawedPictureGroup);
                    minuteView = new SlptPredrawedMinuteView();
                    minuteView.setPreDrawedPicture(preDrawedPictureGroup);
                    minuteView.setStart(0, 0);
                    minuteView.setRect(320, 320);
                    frameLayout.add(minuteView);
                }
                if (this.needRefreshSecond && assetsSecondPathFormat != null) {
                    String[] assetsSecondsPath = new String[16];
                    for (i = 0; i < assetsSecondsPath.length; i++) {
                        assetsSecondsPath[i] = String.format(assetsSecondPathFormat, new Object[]{Integer.valueOf(i)});
                    }
                    preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsSecondsPath);
                    super.addDrawedPictureGroup8C(preDrawedPictureGroup);
                    secondView = new SlptPredrawedSecondView();
                    secondView.setPreDrawedPicture(preDrawedPictureGroup);
                    secondView.setStart(0, 0);
                    secondView.setRect(320, 320);
                    frameLayout.add(secondView);
                    break;
                }
            case 6:
                boolean loadSuccess;
                if (assetsHourPathFormat != null) {
                    if (assetsHourPathFormat.endsWith(".color_map")) {
                        byte[][] assetsHourPath2 = new byte[16][];
                        loadSuccess = true;
                        i = 0;
                        while (i < assetsHourPath2.length) {
                            assetsHourPath2[i] = this.config.parseFile(6, String.format(assetsHourPathFormat, new Object[]{Integer.valueOf(i)}));
                            if (assetsHourPath2[i] == null) {
                                loadSuccess = false;
                                if (loadSuccess) {
                                    preDrawedPictureGroup = new PreDrawedPictureGroup(assetsHourPath2);
                                    super.addDrawedPictureGroup8C(preDrawedPictureGroup);
                                    hourView = new SlptPredrawedHourWithMinuteView();
                                    hourView.setPreDrawedPicture(preDrawedPictureGroup);
                                    hourView.setStart(0, 0);
                                    hourView.setRect(320, 320);
                                    frameLayout.add(hourView);
                                }
                            } else {
                                i++;
                            }
                        }
                        if (loadSuccess) {
                            preDrawedPictureGroup = new PreDrawedPictureGroup(assetsHourPath2);
                            super.addDrawedPictureGroup8C(preDrawedPictureGroup);
                            hourView = new SlptPredrawedHourWithMinuteView();
                            hourView.setPreDrawedPicture(preDrawedPictureGroup);
                            hourView.setStart(0, 0);
                            hourView.setRect(320, 320);
                            frameLayout.add(hourView);
                        }
                    } else if (assetsHourPathFormat.endsWith(".png")) {
                        byte[] hourImg = this.config.parseFile(6, assetsHourPathFormat);
                        if (hourImg != null) {
                            SlptViewComponent pngHourView = new SlptAnalogHourWithMinuteView();
                            pngHourView.setImagePicture(hourImg);
                            pngHourView.alignX = (byte) 2;
                            pngHourView.alignY = (byte) 2;
                            pngHourView.rect.height = 320;
                            pngHourView.rect.width = 320;
                            pngHourView.descHeight = (byte) 2;
                            pngHourView.descWidth = (byte) 2;
                            frameLayout.add(pngHourView);
                        }
                    }
                }
                if (assetsMinutePathFormat != null) {
                    if (assetsMinutePathFormat.endsWith(".color_map")) {
                        byte[][] assetsMinutePath2 = new byte[16][];
                        loadSuccess = true;
                        i = 0;
                        while (i < assetsMinutePath2.length) {
                            assetsMinutePath2[i] = this.config.parseFile(6, String.format(assetsMinutePathFormat, new Object[]{Integer.valueOf(i)}));
                            if (assetsMinutePath2[i] == null) {
                                loadSuccess = false;
                                if (loadSuccess) {
                                    preDrawedPictureGroup = new PreDrawedPictureGroup(assetsMinutePath2);
                                    super.addDrawedPictureGroup8C(preDrawedPictureGroup);
                                    minuteView = new SlptPredrawedMinuteView();
                                    minuteView.setPreDrawedPicture(preDrawedPictureGroup);
                                    minuteView.setStart(0, 0);
                                    minuteView.setRect(320, 320);
                                    frameLayout.add(minuteView);
                                }
                            } else {
                                i++;
                            }
                        }
                        if (loadSuccess) {
                            preDrawedPictureGroup = new PreDrawedPictureGroup(assetsMinutePath2);
                            super.addDrawedPictureGroup8C(preDrawedPictureGroup);
                            minuteView = new SlptPredrawedMinuteView();
                            minuteView.setPreDrawedPicture(preDrawedPictureGroup);
                            minuteView.setStart(0, 0);
                            minuteView.setRect(320, 320);
                            frameLayout.add(minuteView);
                        }
                    } else if (assetsMinutePathFormat.endsWith(".png")) {
                        byte[] minuteImg = this.config.parseFile(6, assetsMinutePathFormat);
                        if (minuteImg != null) {
                            SlptViewComponent pngMinuteView = new SlptAnalogMinuteView();
                            pngMinuteView.setImagePicture(minuteImg);
                            pngMinuteView.alignX = (byte) 2;
                            pngMinuteView.alignY = (byte) 2;
                            pngMinuteView.rect.height = 320;
                            pngMinuteView.rect.width = 320;
                            pngMinuteView.descHeight = (byte) 2;
                            pngMinuteView.descWidth = (byte) 2;
                            frameLayout.add(pngMinuteView);
                        }
                    }
                }
                if (this.needRefreshSecond && assetsSecondPathFormat != null) {
                    if (!assetsSecondPathFormat.endsWith(".color_map")) {
                        if (assetsSecondPathFormat.endsWith(".png")) {
                            byte[] secondImg = this.config.parseFile(6, assetsSecondPathFormat);
                            if (secondImg != null) {
                                SlptViewComponent pngSecondView = new SlptAnalogSecondView();
                                pngSecondView.setImagePicture(secondImg);
                                pngSecondView.alignX = (byte) 2;
                                pngSecondView.alignY = (byte) 2;
                                pngSecondView.rect.height = 320;
                                pngSecondView.rect.width = 320;
                                pngSecondView.descHeight = (byte) 2;
                                pngSecondView.descWidth = (byte) 2;
                                frameLayout.add(pngSecondView);
                                break;
                            }
                        }
                    }
                    byte[][] assetsSecondPath = new byte[16][];
                    loadSuccess = true;
                    for (i = 0; i < assetsSecondPath.length; i++) {
                        assetsSecondPath[i] = this.config.parseFile(6, String.format(assetsSecondPathFormat, new Object[]{Integer.valueOf(i)}));
                        if (assetsSecondPath[i] == null) {
                            loadSuccess = false;
                            if (loadSuccess) {
                                preDrawedPictureGroup = new PreDrawedPictureGroup(assetsSecondPath);
                                super.addDrawedPictureGroup8C(preDrawedPictureGroup);
                                secondView = new SlptPredrawedSecondView();
                                secondView.setPreDrawedPicture(preDrawedPictureGroup);
                                secondView.setStart(0, 0);
                                secondView.setRect(320, 320);
                                frameLayout.add(secondView);
                                break;
                            }
                        }
                    }
                    if (loadSuccess) {
                        preDrawedPictureGroup = new PreDrawedPictureGroup(assetsSecondPath);
                        super.addDrawedPictureGroup8C(preDrawedPictureGroup);
                        secondView = new SlptPredrawedSecondView();
                        secondView.setPreDrawedPicture(preDrawedPictureGroup);
                        secondView.setStart(0, 0);
                        secondView.setRect(320, 320);
                        frameLayout.add(secondView);
                    }
                }
                break;
        }
        if (this.lowBatteryLayout.size() == 0) {
            return frameLayout;
        }
        frameLayout.add(this.lowBatteryLayout);
        return frameLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        if (this.config == null || !this.config.isSupport26W()) {
            Log.i("ExternalWatchFaceSlpt", "custome config is null or not support 26W");
            return null;
        }
        int i;
        SlptLayout frameLayout = new SlptAbsoluteLayout();
        frameLayout.background.color = -16777216;
        SlptViewComponent bgView = new SlptPictureView();
        SlptViewComponent bgView2 = new SlptPictureView();
        byte[] bgMem = null;
        switch (this.config.getBgType()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                bgMem = SimpleFile.readFileFromAssets(this, this.config.getBgPathSlpt26W());
                break;
            case 4:
                bgMem = SimpleFile.readFile(this.config.getBgPathSlpt26W());
                break;
            case 6:
                bgMem = this.config.parseFile(6, this.config.getBgPathSlpt26W());
                break;
        }
        if (bgMem != null) {
            bgView.setImagePicture(bgMem);
            frameLayout.add(bgView);
        }
        byte[] graduationMem = null;
        switch (this.config.getGraduationType()) {
            case 2:
                graduationMem = SimpleFile.readFileFromAssets(this, this.config.getGraduationPathSlpt26W());
                break;
            case 6:
                graduationMem = this.config.parseFile(6, this.config.getGraduationPathSlpt26W());
                break;
        }
        if (graduationMem != null) {
            bgView2.setImagePicture(graduationMem);
            frameLayout.add(bgView2);
        }
        TimeDigital timeDigital = this.config.getTimeDigital();
        if (timeDigital != null) {
            Log.d("ExternalWatchFaceSlpt", timeDigital.toString());
            ImageFontInfo fontInfo = timeDigital.getImageFontInfo();
            if (fontInfo != null) {
                byte[][] time_num = new byte[10][];
                for (i = 0; i < 10; i++) {
                    time_num[i] = fontInfo.parseCharData(Character.forDigit(i, 10), timeDigital.getColor());
                }
                byte[] maohao = fontInfo.parseCharData(':', timeDigital.getColor());
                byte[] a = fontInfo.parseCharData('A', timeDigital.getColor());
                byte[] p = fontInfo.parseCharData('P', timeDigital.getColor());
                byte[] m = fontInfo.parseCharData('M', timeDigital.getColor());
                SlptDigitalTimeWidget timeWidget = new SlptDigitalTimeWidget(this, timeDigital.getX(), timeDigital.getY(), timeDigital.getWidth(), timeDigital.getHeight(), 0);
                timeWidget.setTimeView(time_num);
                timeWidget.setColonView(maohao);
                timeWidget.setAPMview(a, p, m);
                frameLayout.add(timeWidget.getWidgetView());
            }
        }
        ArrayList<DataWidgetConfig> mWidgets = this.config.getDataWidgets();
        for (int j = 0; j < mWidgets.size(); j++) {
            SlptViewComponent widgetView = get26WCDataWidget((DataWidgetConfig) mWidgets.get(j));
            if (widgetView != null) {
                frameLayout.add(widgetView);
            }
        }
        String assetsMinutePathFormat = this.config.getMinutePathSlpt26W();
        String assetsHourPathFormat = this.config.getHourPathSlpt26W();
        String assetsSecondPathFormat = this.config.getSecondsPathSlpt26W();
        PreDrawedPictureGroup preDrawedPictureGroup;
        SlptViewComponent hourView;
        SlptViewComponent minuteView;
        SlptViewComponent secondView;
        switch (this.config.getTimeHandType()) {
            case 2:
                if (assetsHourPathFormat != null) {
                    String[] assetsHourPath = new String[16];
                    for (i = 0; i < assetsHourPath.length; i++) {
                        assetsHourPath[i] = String.format(assetsHourPathFormat, new Object[]{Integer.valueOf(i)});
                    }
                    preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsHourPath);
                    super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
                    hourView = new SlptPredrawedHourWithMinuteView();
                    hourView.setPreDrawedPicture(preDrawedPictureGroup);
                    hourView.setStart(0, 0);
                    hourView.setRect(320, 320);
                    frameLayout.add(hourView);
                }
                if (assetsMinutePathFormat != null) {
                    String[] assetsMinutePath = new String[16];
                    for (i = 0; i < assetsMinutePath.length; i++) {
                        assetsMinutePath[i] = String.format(assetsMinutePathFormat, new Object[]{Integer.valueOf(i)});
                    }
                    preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsMinutePath);
                    super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
                    minuteView = new SlptPredrawedMinuteView();
                    minuteView.setPreDrawedPicture(preDrawedPictureGroup);
                    minuteView.setStart(0, 0);
                    minuteView.setRect(320, 320);
                    frameLayout.add(minuteView);
                }
                if (this.needRefreshSecond && assetsSecondPathFormat != null) {
                    String[] assetsSecondsPath = new String[16];
                    for (i = 0; i < assetsSecondsPath.length; i++) {
                        assetsSecondsPath[i] = String.format(assetsSecondPathFormat, new Object[]{Integer.valueOf(i)});
                    }
                    preDrawedPictureGroup = new PreDrawedPictureGroup(this, assetsSecondsPath);
                    super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
                    secondView = new SlptPredrawedSecondView();
                    secondView.setPreDrawedPicture(preDrawedPictureGroup);
                    secondView.setStart(0, 0);
                    secondView.setRect(320, 320);
                    frameLayout.add(secondView);
                    break;
                }
            case 6:
                boolean loadSuccess;
                if (assetsHourPathFormat != null) {
                    if (assetsHourPathFormat.endsWith(".color_map")) {
                        byte[][] assetsHourPath2 = new byte[16][];
                        loadSuccess = true;
                        i = 0;
                        while (i < assetsHourPath2.length) {
                            assetsHourPath2[i] = this.config.parseFile(6, String.format(assetsHourPathFormat, new Object[]{Integer.valueOf(i)}));
                            if (assetsHourPath2[i] == null) {
                                loadSuccess = false;
                                if (loadSuccess) {
                                    preDrawedPictureGroup = new PreDrawedPictureGroup(assetsHourPath2);
                                    super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
                                    hourView = new SlptPredrawedHourWithMinuteView();
                                    hourView.setPreDrawedPicture(preDrawedPictureGroup);
                                    hourView.setStart(0, 0);
                                    hourView.setRect(320, 320);
                                    frameLayout.add(hourView);
                                }
                            } else {
                                i++;
                            }
                        }
                        if (loadSuccess) {
                            preDrawedPictureGroup = new PreDrawedPictureGroup(assetsHourPath2);
                            super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
                            hourView = new SlptPredrawedHourWithMinuteView();
                            hourView.setPreDrawedPicture(preDrawedPictureGroup);
                            hourView.setStart(0, 0);
                            hourView.setRect(320, 320);
                            frameLayout.add(hourView);
                        }
                    } else if (assetsHourPathFormat.endsWith(".png")) {
                        byte[] hourImg = this.config.parseFile(6, assetsHourPathFormat);
                        if (hourImg != null) {
                            SlptViewComponent pngHourView = new SlptAnalogHourWithMinuteView();
                            pngHourView.setImagePicture(hourImg);
                            pngHourView.alignX = (byte) 2;
                            pngHourView.alignY = (byte) 2;
                            pngHourView.rect.height = 320;
                            pngHourView.rect.width = 320;
                            pngHourView.descHeight = (byte) 2;
                            pngHourView.descWidth = (byte) 2;
                            frameLayout.add(pngHourView);
                        }
                    }
                }
                if (assetsMinutePathFormat != null) {
                    if (assetsMinutePathFormat.endsWith(".color_map")) {
                        byte[][] assetsMinutePath2 = new byte[16][];
                        loadSuccess = true;
                        i = 0;
                        while (i < assetsMinutePath2.length) {
                            assetsMinutePath2[i] = this.config.parseFile(6, String.format(assetsMinutePathFormat, new Object[]{Integer.valueOf(i)}));
                            if (assetsMinutePath2[i] == null) {
                                loadSuccess = false;
                                if (loadSuccess) {
                                    preDrawedPictureGroup = new PreDrawedPictureGroup(assetsMinutePath2);
                                    super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
                                    minuteView = new SlptPredrawedMinuteView();
                                    minuteView.setPreDrawedPicture(preDrawedPictureGroup);
                                    minuteView.setStart(0, 0);
                                    minuteView.setRect(320, 320);
                                    frameLayout.add(minuteView);
                                }
                            } else {
                                i++;
                            }
                        }
                        if (loadSuccess) {
                            preDrawedPictureGroup = new PreDrawedPictureGroup(assetsMinutePath2);
                            super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
                            minuteView = new SlptPredrawedMinuteView();
                            minuteView.setPreDrawedPicture(preDrawedPictureGroup);
                            minuteView.setStart(0, 0);
                            minuteView.setRect(320, 320);
                            frameLayout.add(minuteView);
                        }
                    } else if (assetsMinutePathFormat.endsWith(".png")) {
                        byte[] minuteImg = this.config.parseFile(6, assetsMinutePathFormat);
                        if (minuteImg != null) {
                            SlptViewComponent pngMinuteView = new SlptAnalogMinuteView();
                            pngMinuteView.setImagePicture(minuteImg);
                            pngMinuteView.alignX = (byte) 2;
                            pngMinuteView.alignY = (byte) 2;
                            pngMinuteView.rect.height = 320;
                            pngMinuteView.rect.width = 320;
                            pngMinuteView.descHeight = (byte) 2;
                            pngMinuteView.descWidth = (byte) 2;
                            frameLayout.add(pngMinuteView);
                        }
                    }
                }
                if (this.needRefreshSecond && assetsSecondPathFormat != null) {
                    if (!assetsSecondPathFormat.endsWith(".color_map")) {
                        if (assetsSecondPathFormat.endsWith(".png")) {
                            byte[] secondImg = this.config.parseFile(6, assetsSecondPathFormat);
                            if (secondImg != null) {
                                SlptViewComponent pngSecondView = new SlptAnalogSecondView();
                                pngSecondView.setImagePicture(secondImg);
                                pngSecondView.alignX = (byte) 2;
                                pngSecondView.alignY = (byte) 2;
                                pngSecondView.rect.height = 320;
                                pngSecondView.rect.width = 320;
                                pngSecondView.descHeight = (byte) 2;
                                pngSecondView.descWidth = (byte) 2;
                                frameLayout.add(pngSecondView);
                                break;
                            }
                        }
                    }
                    byte[][] assetsSecondPath = new byte[16][];
                    loadSuccess = true;
                    for (i = 0; i < assetsSecondPath.length; i++) {
                        assetsSecondPath[i] = this.config.parseFile(6, String.format(assetsSecondPathFormat, new Object[]{Integer.valueOf(i)}));
                        if (assetsSecondPath[i] == null) {
                            loadSuccess = false;
                            if (loadSuccess) {
                                preDrawedPictureGroup = new PreDrawedPictureGroup(assetsSecondPath);
                                super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
                                secondView = new SlptPredrawedSecondView();
                                secondView.setPreDrawedPicture(preDrawedPictureGroup);
                                secondView.setStart(0, 0);
                                secondView.setRect(320, 320);
                                frameLayout.add(secondView);
                                break;
                            }
                        }
                    }
                    if (loadSuccess) {
                        preDrawedPictureGroup = new PreDrawedPictureGroup(assetsSecondPath);
                        super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
                        secondView = new SlptPredrawedSecondView();
                        secondView.setPreDrawedPicture(preDrawedPictureGroup);
                        secondView.setStart(0, 0);
                        secondView.setRect(320, 320);
                        frameLayout.add(secondView);
                    }
                }
                break;
        }
        if (this.lowBatteryLayout.size() == 0) {
            return frameLayout;
        }
        frameLayout.add(this.lowBatteryLayout);
        return frameLayout;
    }
}
