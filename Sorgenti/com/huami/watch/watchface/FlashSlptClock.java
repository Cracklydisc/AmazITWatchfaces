package com.huami.watch.watchface;

import com.ingenic.iwds.slpt.view.analog.SlptAnalogHourView;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogMinuteView;
import com.ingenic.iwds.slpt.view.core.SlptFrameLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class FlashSlptClock extends AbstractSlptClock {
    protected void initWatchFaceConfig() {
    }

    protected SlptLayout createClockLayout8C() {
        SlptLayout rootLayout = new SlptFrameLayout();
        SlptFrameLayout frameLayout = new SlptFrameLayout();
        SlptAnalogHourView hourView = new SlptAnalogHourView();
        SlptAnalogMinuteView minuteView = new SlptAnalogMinuteView();
        SlptPictureView bgView = new SlptPictureView();
        byte[] hourImg = SimpleFile.readFileFromAssets(this, "slpt-analog-flash/hour_8c.png");
        if (hourImg != null) {
            hourView.setImagePicture(hourImg);
            byte[] minuteImg = SimpleFile.readFileFromAssets(this, "slpt-analog-flash/min_8c.png");
            if (minuteImg != null) {
                minuteView.setImagePicture(minuteImg);
                byte[] bgImg = SimpleFile.readFileFromAssets(this, "slpt-analog-flash/bg_8c.png");
                if (bgImg != null) {
                    bgView.setImagePicture(bgImg);
                    frameLayout.add(bgView);
                    frameLayout.add(hourView);
                    frameLayout.add(minuteView);
                    frameLayout.alignX = (byte) 2;
                    frameLayout.alignY = (byte) 2;
                    frameLayout.descHeight = (byte) 2;
                    frameLayout.descWidth = (byte) 2;
                    frameLayout.rect.height = 320;
                    frameLayout.rect.width = 320;
                    rootLayout.add(frameLayout);
                }
            }
        }
        return rootLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
