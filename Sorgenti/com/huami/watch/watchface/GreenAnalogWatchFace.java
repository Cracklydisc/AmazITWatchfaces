package com.huami.watch.watchface;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import com.huami.watch.watchface.AbstractWatchFace.AnalogEngine;
import com.huami.watch.watchface.analogyellow.R;

public class GreenAnalogWatchFace extends AbstractWatchFace {
    public static final String[] WEEKDAYS = new String[]{"Sun.", "Mon.", "Tue.", "Wed.", "Thu.", "Fri.", "Sat."};

    private class Engine extends AnalogEngine {
        Bitmap mBackgroundBitmap;
        private float mDayMarginLeft;
        Bitmap mHourBitmap;
        Bitmap mMinuteBitmap;
        private float mMonthMarginLeft;
        private float mMonthMarginTop;
        Paint mPaint;
        Bitmap mPointerBitmap;
        Bitmap mSecondBitmap;
        private float mWeekMarginLeft;
        private float mWeekMarginTop;

        private Engine() {
            super();
        }

        protected void onPrepareResources(Resources resources) {
            this.mBackgroundBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_default_green_bg, null)).getBitmap();
            this.mHourBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_default_green_hour, null)).getBitmap();
            this.mMinuteBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_default_green_minute, null)).getBitmap();
            this.mSecondBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_default_green_seconds, null)).getBitmap();
            this.mPointerBitmap = ((BitmapDrawable) resources.getDrawable(R.drawable.watchface_default_green_small_pointer, null)).getBitmap();
            this.mPaint = new Paint();
            this.mPaint.setAntiAlias(true);
            this.mPaint.setFilterBitmap(true);
            this.mPaint.setDither(true);
            this.mMonthMarginLeft = resources.getDimension(R.dimen.green_month_margin_left);
            this.mMonthMarginTop = resources.getDimension(R.dimen.green_month_margin_top);
            this.mDayMarginLeft = resources.getDimension(R.dimen.green_day_margin_left);
            this.mWeekMarginLeft = resources.getDimension(R.dimen.green_week_margin_left);
            this.mWeekMarginTop = resources.getDimension(R.dimen.green_week_margin_top);
        }

        private float getMonthDegree() {
            return (((float) (this.mCalendar.get(2) + 1)) / 12.0f) * 360.0f;
        }

        private float getDayDegree() {
            return (((float) this.mCalendar.get(5)) / 31.0f) * 360.0f;
        }

        private float getWeekDegree() {
            return (((float) (this.mCalendar.get(7) - 1)) / 7.0f) * 360.0f;
        }

        protected void onDrawAnalog(Canvas canvas, float width, float height, float centerX, float centerY, float secRot, float minRot, float hrRot) {
            canvas.drawBitmap(this.mBackgroundBitmap, null, new RectF(0.0f, 0.0f, width, height), null);
            canvas.save();
            canvas.rotate(getMonthDegree(), this.mMonthMarginLeft, this.mMonthMarginTop);
            canvas.drawBitmap(this.mPointerBitmap, this.mMonthMarginLeft - (((float) this.mPointerBitmap.getWidth()) * 0.5f), this.mMonthMarginTop - (((float) this.mPointerBitmap.getHeight()) * 0.5f), this.mPaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(getDayDegree(), this.mDayMarginLeft, this.mMonthMarginTop);
            canvas.drawBitmap(this.mPointerBitmap, this.mDayMarginLeft - (((float) this.mPointerBitmap.getWidth()) * 0.5f), this.mMonthMarginTop - (((float) this.mPointerBitmap.getHeight()) * 0.5f), this.mPaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(getWeekDegree(), this.mWeekMarginLeft, this.mWeekMarginTop);
            canvas.drawBitmap(this.mPointerBitmap, this.mWeekMarginLeft - (((float) this.mPointerBitmap.getWidth()) * 0.5f), this.mWeekMarginTop - (((float) this.mPointerBitmap.getHeight()) * 0.5f), this.mPaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(hrRot, centerX, centerY);
            canvas.drawBitmap(this.mHourBitmap, centerX - (((float) this.mHourBitmap.getWidth()) / 2.0f), centerY - (((float) this.mHourBitmap.getHeight()) / 2.0f), this.mPaint);
            canvas.restore();
            canvas.save();
            canvas.rotate(minRot, centerX, centerY);
            canvas.drawBitmap(this.mMinuteBitmap, centerX - (((float) this.mMinuteBitmap.getWidth()) / 2.0f), centerY - (((float) this.mMinuteBitmap.getHeight()) / 2.0f), this.mPaint);
            canvas.restore();
            if (!isInAmbientMode()) {
                canvas.save();
                canvas.rotate(secRot, centerX, centerY);
                canvas.drawBitmap(this.mSecondBitmap, centerX - (((float) this.mSecondBitmap.getWidth()) / 2.0f), centerY - (((float) this.mSecondBitmap.getHeight()) / 2.0f), this.mPaint);
                canvas.restore();
            }
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return GreenSlptClock.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(51.0f);
        return new Engine();
    }
}
