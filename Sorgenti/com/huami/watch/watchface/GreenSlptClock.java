package com.huami.watch.watchface;

import android.content.Context;
import android.util.Log;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedDayView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedHourWithMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMinuteView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedMonthView;
import com.ingenic.iwds.slpt.view.predrawed.SlptPredrawedSecondView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class GreenSlptClock extends AbstractSlptClock {
    int f11i = 0;
    private SlptViewComponent lowBatteryView = null;
    private Context mContext;
    private boolean needRefreshSecond;
    String num_res;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        this.needRefreshSecond = Util.needSlptRefreshSecond(this.mContext).booleanValue();
        if (this.needRefreshSecond) {
            setClockPeriodSecond(true);
        }
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -65536, 0);
        this.lowBatteryView.setStart(0, 68);
        this.lowBatteryView.setRect(320, this.mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
    }

    protected SlptLayout createClockLayout8C() {
        Log.i("jixiexianfengSlpt", "createClockLayout8C: ");
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent hourView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent monthView = new SlptPredrawedMonthView();
        SlptViewComponent secondView = new SlptPredrawedSecondView();
        SlptPredrawedDayView dayView = new SlptPredrawedDayView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptPictureView bgView = new SlptPictureView();
        String[] assetsMinutePath = new String[]{"slpt-analog-green/8c/minute/minute_00.png.color_map", "slpt-analog-green/8c/minute/minute_01.png.color_map", "slpt-analog-green/8c/minute/minute_02.png.color_map", "slpt-analog-green/8c/minute/minute_03.png.color_map", "slpt-analog-green/8c/minute/minute_04.png.color_map", "slpt-analog-green/8c/minute/minute_05.png.color_map", "slpt-analog-green/8c/minute/minute_06.png.color_map", "slpt-analog-green/8c/minute/minute_07.png.color_map", "slpt-analog-green/8c/minute/minute_08.png.color_map", "slpt-analog-green/8c/minute/minute_09.png.color_map", "slpt-analog-green/8c/minute/minute_10.png.color_map", "slpt-analog-green/8c/minute/minute_11.png.color_map", "slpt-analog-green/8c/minute/minute_12.png.color_map", "slpt-analog-green/8c/minute/minute_13.png.color_map", "slpt-analog-green/8c/minute/minute_14.png.color_map", "slpt-analog-green/8c/minute/minute_15.png.color_map"};
        String[] assetsHourPath = new String[]{"slpt-analog-green/8c/hour/hour_00.png.color_map", "slpt-analog-green/8c/hour/hour_01.png.color_map", "slpt-analog-green/8c/hour/hour_02.png.color_map", "slpt-analog-green/8c/hour/hour_03.png.color_map", "slpt-analog-green/8c/hour/hour_04.png.color_map", "slpt-analog-green/8c/hour/hour_05.png.color_map", "slpt-analog-green/8c/hour/hour_06.png.color_map", "slpt-analog-green/8c/hour/hour_07.png.color_map", "slpt-analog-green/8c/hour/hour_08.png.color_map", "slpt-analog-green/8c/hour/hour_09.png.color_map", "slpt-analog-green/8c/hour/hour_10.png.color_map", "slpt-analog-green/8c/hour/hour_11.png.color_map", "slpt-analog-green/8c/hour/hour_12.png.color_map", "slpt-analog-green/8c/hour/hour_13.png.color_map", "slpt-analog-green/8c/hour/hour_14.png.color_map", "slpt-analog-green/8c/hour/hour_15.png.color_map"};
        String[] assetsSecondPath = new String[]{"slpt-analog-green/8c/second/second_00.png.color_map", "slpt-analog-green/8c/second/second_01.png.color_map", "slpt-analog-green/8c/second/second_02.png.color_map", "slpt-analog-green/8c/second/second_03.png.color_map", "slpt-analog-green/8c/second/second_04.png.color_map", "slpt-analog-green/8c/second/second_05.png.color_map", "slpt-analog-green/8c/second/second_06.png.color_map", "slpt-analog-green/8c/second/second_07.png.color_map", "slpt-analog-green/8c/second/second_08.png.color_map", "slpt-analog-green/8c/second/second_09.png.color_map", "slpt-analog-green/8c/second/second_10.png.color_map", "slpt-analog-green/8c/second/second_11.png.color_map", "slpt-analog-green/8c/second/second_12.png.color_map", "slpt-analog-green/8c/second/second_13.png.color_map", "slpt-analog-green/8c/second/second_14.png.color_map", "slpt-analog-green/8c/second/second_15.png.color_map"};
        String[] assetsMonthPath = new String[]{"slpt-analog-green/month_1.png.color_map", "slpt-analog-green/month_2.png.color_map", "slpt-analog-green/month_3.png.color_map", "slpt-analog-green/month_4.png.color_map", "slpt-analog-green/month_5.png.color_map", "slpt-analog-green/month_6.png.color_map", "slpt-analog-green/month_7.png.color_map", "slpt-analog-green/month_8.png.color_map", "slpt-analog-green/month_9.png.color_map", "slpt-analog-green/month_10.png.color_map", "slpt-analog-green/month_11.png.color_map", "slpt-analog-green/month_12.png.color_map"};
        String[] assetsDayPath = new String[]{"slpt-analog-green/month_1.png.color_map", "slpt-analog-green/day_1.png.color_map", "slpt-analog-green/day_2.png.color_map", "slpt-analog-green/day_3.png.color_map", "slpt-analog-green/day_4.png.color_map", "slpt-analog-green/day_5.png.color_map", "slpt-analog-green/day_6.png.color_map", "slpt-analog-green/day_7.png.color_map", "slpt-analog-green/day_8.png.color_map", "slpt-analog-green/day_9.png.color_map", "slpt-analog-green/day_10.png.color_map", "slpt-analog-green/day_11.png.color_map", "slpt-analog-green/day_12.png.color_map", "slpt-analog-green/day_13.png.color_map", "slpt-analog-green/day_14.png.color_map", "slpt-analog-green/day_15.png.color_map", "slpt-analog-green/day_16.png.color_map", "slpt-analog-green/day_17.png.color_map", "slpt-analog-green/day_18.png.color_map", "slpt-analog-green/day_19.png.color_map", "slpt-analog-green/day_20.png.color_map", "slpt-analog-green/day_21.png.color_map", "slpt-analog-green/day_22.png.color_map", "slpt-analog-green/day_23.png.color_map", "slpt-analog-green/day_24.png.color_map", "slpt-analog-green/day_25.png.color_map", "slpt-analog-green/day_26.png.color_map", "slpt-analog-green/day_27.png.color_map", "slpt-analog-green/day_28.png.color_map", "slpt-analog-green/day_29.png.color_map", "slpt-analog-green/day_30.png.color_map", "slpt-analog-green/day_31.png.color_map"};
        byte[][] week_num = new byte[10][];
        this.f11i = 0;
        while (this.f11i < 7) {
            this.num_res = String.format("slpt-analog-green/week_%d.png", new Object[]{Integer.valueOf(this.f11i)});
            week_num[this.f11i] = SimpleFile.readFileFromAssets(this.mContext, this.num_res);
            this.f11i++;
        }
        bgView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "slpt-analog-green/watchface_default_bg_8c.png"));
        frameLayout.background.color = -16777216;
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup group = new PreDrawedPictureGroup(this.mContext, assetsMinutePath);
        super.addDrawedPictureGroup8C(group);
        PreDrawedPictureGroup group2 = new PreDrawedPictureGroup(this.mContext, assetsHourPath);
        super.addDrawedPictureGroup8C(group2);
        if (this.needRefreshSecond) {
            PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsSecondPath);
            super.addDrawedPictureGroup8C(preDrawedPictureGroup);
            secondView.setPreDrawedPicture(preDrawedPictureGroup);
            secondView.setStart(0, 0);
            secondView.setRect(320, 320);
        }
        PreDrawedPictureGroup group3 = new PreDrawedPictureGroup(this.mContext, assetsMonthPath);
        super.addDrawedPictureGroup8C(group3);
        PreDrawedPictureGroup group4 = new PreDrawedPictureGroup(this.mContext, assetsDayPath);
        super.addDrawedPictureGroup8C(group4);
        weekView.setImagePictureArray(week_num);
        weekView.setStart(122, 185);
        minuteView.setPreDrawedPicture(group);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(group2);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        monthView.setPreDrawedPicture(group3);
        monthView.setStart(70, 102);
        monthView.setRect(72, 72);
        dayView.setPreDrawedPicture(group4);
        dayView.setStart(180, 101);
        dayView.setRect(72, 72);
        frameLayout.add(bgView);
        frameLayout.add(weekView);
        frameLayout.add(monthView);
        frameLayout.add(dayView);
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        frameLayout.add(this.lowBatteryView);
        return frameLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        Log.i("jixiexianfengSlpt", "createClockLayout26WC: ");
        SlptAbsoluteLayout frameLayout = new SlptAbsoluteLayout();
        SlptViewComponent minuteView = new SlptPredrawedMinuteView();
        SlptViewComponent hourView = new SlptPredrawedHourWithMinuteView();
        SlptViewComponent monthView = new SlptPredrawedMonthView();
        SlptViewComponent secondView = new SlptPredrawedSecondView();
        SlptPredrawedDayView dayView = new SlptPredrawedDayView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptPictureView bgView = new SlptPictureView();
        String[] assetsMinutePath = new String[]{"slpt-analog-green/26wc/minute/0.png.color_map", "slpt-analog-green/26wc/minute/1.png.color_map", "slpt-analog-green/26wc/minute/2.png.color_map", "slpt-analog-green/26wc/minute/3.png.color_map", "slpt-analog-green/26wc/minute/4.png.color_map", "slpt-analog-green/26wc/minute/5.png.color_map", "slpt-analog-green/26wc/minute/6.png.color_map", "slpt-analog-green/26wc/minute/7.png.color_map", "slpt-analog-green/26wc/minute/8.png.color_map", "slpt-analog-green/26wc/minute/9.png.color_map", "slpt-analog-green/26wc/minute/10.png.color_map", "slpt-analog-green/26wc/minute/11.png.color_map", "slpt-analog-green/26wc/minute/12.png.color_map", "slpt-analog-green/26wc/minute/13.png.color_map", "slpt-analog-green/26wc/minute/14.png.color_map", "slpt-analog-green/26wc/minute/15.png.color_map"};
        String[] assetsHourPath = new String[]{"slpt-analog-green/26wc/hour/0.png.color_map", "slpt-analog-green/26wc/hour/1.png.color_map", "slpt-analog-green/26wc/hour/2.png.color_map", "slpt-analog-green/26wc/hour/3.png.color_map", "slpt-analog-green/26wc/hour/4.png.color_map", "slpt-analog-green/26wc/hour/5.png.color_map", "slpt-analog-green/26wc/hour/6.png.color_map", "slpt-analog-green/26wc/hour/7.png.color_map", "slpt-analog-green/26wc/hour/8.png.color_map", "slpt-analog-green/26wc/hour/9.png.color_map", "slpt-analog-green/26wc/hour/10.png.color_map", "slpt-analog-green/26wc/hour/11.png.color_map", "slpt-analog-green/26wc/hour/12.png.color_map", "slpt-analog-green/26wc/hour/13.png.color_map", "slpt-analog-green/26wc/hour/14.png.color_map", "slpt-analog-green/26wc/hour/15.png.color_map"};
        String[] assetsSecondPath = new String[]{"slpt-analog-green/26wc/second/0.png.color_map", "slpt-analog-green/26wc/second/1.png.color_map", "slpt-analog-green/26wc/second/2.png.color_map", "slpt-analog-green/26wc/second/3.png.color_map", "slpt-analog-green/26wc/second/4.png.color_map", "slpt-analog-green/26wc/second/5.png.color_map", "slpt-analog-green/26wc/second/6.png.color_map", "slpt-analog-green/26wc/second/7.png.color_map", "slpt-analog-green/26wc/second/8.png.color_map", "slpt-analog-green/26wc/second/9.png.color_map", "slpt-analog-green/26wc/second/10.png.color_map", "slpt-analog-green/26wc/second/11.png.color_map", "slpt-analog-green/26wc/second/12.png.color_map", "slpt-analog-green/26wc/second/13.png.color_map", "slpt-analog-green/26wc/second/14.png.color_map", "slpt-analog-green/26wc/second/15.png.color_map"};
        String[] assetsMonthPath = new String[]{"slpt-analog-green/month_1.png.color_map", "slpt-analog-green/month_2.png.color_map", "slpt-analog-green/month_3.png.color_map", "slpt-analog-green/month_4.png.color_map", "slpt-analog-green/month_5.png.color_map", "slpt-analog-green/month_6.png.color_map", "slpt-analog-green/month_7.png.color_map", "slpt-analog-green/month_8.png.color_map", "slpt-analog-green/month_9.png.color_map", "slpt-analog-green/month_10.png.color_map", "slpt-analog-green/month_11.png.color_map", "slpt-analog-green/month_12.png.color_map"};
        String[] assetsDayPath = new String[]{"slpt-analog-green/month_1.png.color_map", "slpt-analog-green/day_1.png.color_map", "slpt-analog-green/day_2.png.color_map", "slpt-analog-green/day_3.png.color_map", "slpt-analog-green/day_4.png.color_map", "slpt-analog-green/day_5.png.color_map", "slpt-analog-green/day_6.png.color_map", "slpt-analog-green/day_7.png.color_map", "slpt-analog-green/day_8.png.color_map", "slpt-analog-green/day_9.png.color_map", "slpt-analog-green/day_10.png.color_map", "slpt-analog-green/day_11.png.color_map", "slpt-analog-green/day_12.png.color_map", "slpt-analog-green/day_13.png.color_map", "slpt-analog-green/day_14.png.color_map", "slpt-analog-green/day_15.png.color_map", "slpt-analog-green/day_16.png.color_map", "slpt-analog-green/day_17.png.color_map", "slpt-analog-green/day_18.png.color_map", "slpt-analog-green/day_19.png.color_map", "slpt-analog-green/day_20.png.color_map", "slpt-analog-green/day_21.png.color_map", "slpt-analog-green/day_22.png.color_map", "slpt-analog-green/day_23.png.color_map", "slpt-analog-green/day_24.png.color_map", "slpt-analog-green/day_25.png.color_map", "slpt-analog-green/day_26.png.color_map", "slpt-analog-green/day_27.png.color_map", "slpt-analog-green/day_28.png.color_map", "slpt-analog-green/day_29.png.color_map", "slpt-analog-green/day_30.png.color_map", "slpt-analog-green/day_31.png.color_map"};
        byte[][] week_num = new byte[10][];
        this.f11i = 0;
        while (this.f11i < 7) {
            this.num_res = String.format("slpt-analog-green/week_%d.png", new Object[]{Integer.valueOf(this.f11i)});
            week_num[this.f11i] = SimpleFile.readFileFromAssets(this.mContext, this.num_res);
            this.f11i++;
        }
        bgView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "slpt-analog-green/watchface_default_bg_26wc.png"));
        frameLayout.background.color = -16777216;
        super.clearDrawdPictureGroup();
        PreDrawedPictureGroup group = new PreDrawedPictureGroup(this.mContext, assetsMinutePath);
        super.addDrawedPictureGroup26WC(group);
        PreDrawedPictureGroup group2 = new PreDrawedPictureGroup(this.mContext, assetsHourPath);
        super.addDrawedPictureGroup26WC(group2);
        if (this.needRefreshSecond) {
            PreDrawedPictureGroup preDrawedPictureGroup = new PreDrawedPictureGroup(this.mContext, assetsSecondPath);
            super.addDrawedPictureGroup26WC(preDrawedPictureGroup);
            secondView.setPreDrawedPicture(preDrawedPictureGroup);
            secondView.setStart(0, 0);
            secondView.setRect(320, 320);
        }
        PreDrawedPictureGroup group3 = new PreDrawedPictureGroup(this.mContext, assetsMonthPath);
        super.addDrawedPictureGroup26WC(group3);
        PreDrawedPictureGroup group4 = new PreDrawedPictureGroup(this.mContext, assetsDayPath);
        super.addDrawedPictureGroup26WC(group4);
        weekView.setImagePictureArray(week_num);
        weekView.setStart(122, 185);
        minuteView.setPreDrawedPicture(group);
        minuteView.setStart(0, 0);
        minuteView.setRect(320, 320);
        hourView.setPreDrawedPicture(group2);
        hourView.setStart(0, 0);
        hourView.setRect(320, 320);
        monthView.setPreDrawedPicture(group3);
        monthView.setStart(70, 102);
        monthView.setRect(72, 72);
        dayView.setPreDrawedPicture(group4);
        dayView.setStart(180, 101);
        dayView.setRect(72, 72);
        frameLayout.add(bgView);
        frameLayout.add(weekView);
        frameLayout.add(monthView);
        frameLayout.add(dayView);
        frameLayout.add(hourView);
        frameLayout.add(minuteView);
        if (this.needRefreshSecond) {
            frameLayout.add(secondView);
        }
        frameLayout.add(this.lowBatteryView);
        return frameLayout;
    }
}
