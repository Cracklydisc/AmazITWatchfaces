package com.huami.watch.watchface;

import android.content.Context;
import android.graphics.Typeface;
import com.huami.watch.watchface.util.TypefaceManager;
import com.ingenic.iwds.slpt.view.core.SlptFrameLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class LifeDigitalSlptClock extends AbstractSlptClock {
    String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private Context mContext;
    private TypefaceManager typefaceManager;

    protected void initWatchFaceConfig() {
        this.typefaceManager = TypefaceManager.get();
        this.mContext = getApplicationContext();
    }

    protected SlptLayout createClockLayout8C() {
        SlptLayout rootLayout = new SlptFrameLayout();
        SlptLinearLayout hourLinearLayout = new SlptLinearLayout();
        SlptLinearLayout minLinearLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        SlptPictureView bgView = new SlptPictureView();
        byte[] bgImg = SimpleFile.readFileFromAssets(this.mContext, "nothing.png");
        if (bgImg != null) {
            bgView.setImagePicture(bgImg);
            Typeface hourTypeface = this.typefaceManager.createFromAsset("typeface/DINPro-Black.otf");
            Typeface minTypeface = this.typefaceManager.createFromAsset("typeface/DINPro-Light.otf");
            if (hourTypeface == null) {
                hourTypeface = Typeface.DEFAULT;
            }
            if (minTypeface == null) {
                minTypeface = Typeface.DEFAULT;
            }
            hourHView.textColor = -1;
            hourLView.textColor = -1;
            minuteHView.textColor = -1;
            minuteLView.textColor = -1;
            hourHView.textSize = 100.05f;
            hourLView.textSize = 100.05f;
            minuteHView.textSize = 100.05f;
            minuteLView.textSize = 100.05f;
            hourHView.typeface = hourTypeface;
            hourLView.typeface = hourTypeface;
            minuteHView.typeface = minTypeface;
            minuteLView.typeface = minTypeface;
            hourHView.setStringPictureArray(this.digitalNums);
            hourLView.setStringPictureArray(this.digitalNums);
            minuteHView.setStringPictureArray(this.digitalNums);
            minuteLView.setStringPictureArray(this.digitalNums);
            hourLinearLayout.add(hourHView);
            hourLinearLayout.add(hourLView);
            minLinearLayout.add(minuteHView);
            minLinearLayout.add(minuteLView);
            hourLinearLayout.orientation = (byte) 0;
            minLinearLayout.orientation = (byte) 0;
            hourLinearLayout.padding.left = (short) 105;
            hourLinearLayout.padding.top = (short) 44;
            minLinearLayout.padding.left = (short) 110;
            minLinearLayout.padding.top = (short) 136;
            rootLayout.add(bgView);
            rootLayout.add(hourLinearLayout);
            rootLayout.add(minLinearLayout);
        }
        return rootLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
