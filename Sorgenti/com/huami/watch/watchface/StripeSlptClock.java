package com.huami.watch.watchface;

import android.util.Log;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogHourView;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogMinuteView;
import com.ingenic.iwds.slpt.view.core.SlptFrameLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.digital.SlptYear0View;
import com.ingenic.iwds.slpt.view.digital.SlptYear1View;
import com.ingenic.iwds.slpt.view.digital.SlptYear2View;
import com.ingenic.iwds.slpt.view.digital.SlptYear3View;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class StripeSlptClock extends AbstractSlptClock {
    String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    String[] weekNums = DigitalWatchFace.WEEKDAYS;

    protected void initWatchFaceConfig() {
    }

    protected SlptLayout createClockLayout8C() {
        Log.i("AbstractSlptClock-AnalogClassic", "createClockLayout8C: ");
        SlptLayout rootLayout = new SlptFrameLayout();
        SlptLinearLayout dateLinearLayout = new SlptLinearLayout();
        SlptViewComponent year0View = new SlptYear0View();
        SlptViewComponent year1View = new SlptYear1View();
        SlptViewComponent year2View = new SlptYear2View();
        SlptViewComponent year3View = new SlptYear3View();
        SlptPictureView dateSepView1 = new SlptPictureView();
        SlptViewComponent monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptPictureView dateSepView2 = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        year0View.textSize = (float) 19;
        year1View.textSize = (float) 19;
        year2View.textSize = (float) 19;
        year3View.textSize = (float) 19;
        dateSepView1.textSize = (float) 19;
        monthHView.textSize = (float) 19;
        monthLView.textSize = (float) 19;
        dateSepView2.textSize = (float) 19;
        dayHView.textSize = (float) 19;
        dayLView.textSize = (float) 19;
        weekView.textSize = (float) 19;
        year0View.textColor = -1;
        year1View.textColor = -1;
        year2View.textColor = -1;
        year3View.textColor = -1;
        dateSepView1.textColor = -1;
        monthHView.textColor = -1;
        monthLView.textColor = -1;
        dateSepView2.textColor = -1;
        dayHView.textColor = -1;
        dayLView.textColor = -1;
        weekView.textColor = -1;
        year0View.setStringPictureArray(this.digitalNums);
        year1View.setStringPictureArray(this.digitalNums);
        year2View.setStringPictureArray(this.digitalNums);
        year3View.setStringPictureArray(this.digitalNums);
        dateSepView1.setStringPicture('/');
        monthHView.setStringPictureArray(this.digitalNums);
        monthLView.setStringPictureArray(this.digitalNums);
        dateSepView2.setStringPicture('/');
        dayHView.setStringPictureArray(this.digitalNums);
        dayLView.setStringPictureArray(this.digitalNums);
        weekView.setStringPictureArray(this.weekNums);
        weekView.padding.left = (short) 131;
        weekView.padding.top = (short) 3;
        dateLinearLayout.add(year3View);
        dateLinearLayout.add(year2View);
        dateLinearLayout.add(year1View);
        dateLinearLayout.add(year0View);
        dateLinearLayout.add(dateSepView1);
        dateLinearLayout.add(monthHView);
        dateLinearLayout.add(monthLView);
        dateLinearLayout.add(dateSepView2);
        dateLinearLayout.add(dayHView);
        dateLinearLayout.add(dayLView);
        dateLinearLayout.orientation = (byte) 0;
        dateLinearLayout.padding.left = (short) 110;
        dateLinearLayout.padding.top = (short) 73;
        SlptLinearLayout linearLayout = new SlptLinearLayout();
        linearLayout.add(dateLinearLayout);
        linearLayout.add(weekView);
        linearLayout.orientation = (byte) 1;
        SlptFrameLayout frameLayout = new SlptFrameLayout();
        SlptAnalogHourView hourView = new SlptAnalogHourView();
        SlptAnalogMinuteView minuteView = new SlptAnalogMinuteView();
        SlptPictureView bgView = new SlptPictureView();
        byte[] hourImg = SimpleFile.readFileFromAssets(this, "slpt-analog-stripe/hour_8c.png");
        if (hourImg != null) {
            hourView.setImagePicture(hourImg);
            byte[] minuteImg = SimpleFile.readFileFromAssets(this, "slpt-analog-stripe/minute_8c.png");
            if (minuteImg != null) {
                minuteView.setImagePicture(minuteImg);
                byte[] bgImg = SimpleFile.readFileFromAssets(this, "slpt-analog-stripe/bg_8c.png");
                if (bgImg != null) {
                    bgView.setImagePicture(bgImg);
                    frameLayout.add(bgView);
                    frameLayout.add(hourView);
                    frameLayout.add(minuteView);
                    frameLayout.alignX = (byte) 2;
                    frameLayout.alignY = (byte) 2;
                    frameLayout.descHeight = (byte) 2;
                    frameLayout.descWidth = (byte) 2;
                    frameLayout.rect.height = 320;
                    frameLayout.rect.width = 320;
                    rootLayout.add(frameLayout);
                    rootLayout.add(linearLayout);
                }
            }
        }
        return rootLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
