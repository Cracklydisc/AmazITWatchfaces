package com.huami.watch.watchface;

import android.content.Context;
import android.graphics.Typeface;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.TypefaceManager;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.digital.SlptYear0View;
import com.ingenic.iwds.slpt.view.digital.SlptYear1View;
import com.ingenic.iwds.slpt.view.digital.SlptYear2View;
import com.ingenic.iwds.slpt.view.digital.SlptYear3View;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class SunriseDigitalSlptClock extends AbstractSlptClock {
    String[] digitalNums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private SlptViewComponent lowBatteryView;
    private Context mContext;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
        this.lowBatteryView = SportItemInfoWrapper.createLowBatteryView(this.mContext, -65536, 0);
        this.lowBatteryView.setStart(117, 262);
    }

    protected SlptLayout createClockLayout8C() {
        SlptAbsoluteLayout rootLayout = new SlptAbsoluteLayout();
        SlptLinearLayout dateLinearLayout = new SlptLinearLayout();
        SlptViewComponent year0View = new SlptYear0View();
        SlptViewComponent year1View = new SlptYear1View();
        SlptViewComponent year2View = new SlptYear2View();
        SlptViewComponent year3View = new SlptYear3View();
        SlptPictureView dateSepView1 = new SlptPictureView();
        SlptMonthHView monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptPictureView dateSepView2 = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptLinearLayout timeLinearLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptPictureView timeSepView = new SlptPictureView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        SlptPictureView bgView = new SlptPictureView();
        year0View.textSize = 21.0f;
        year1View.textSize = 21.0f;
        year2View.textSize = 21.0f;
        year3View.textSize = 21.0f;
        dateSepView1.textSize = 21.0f;
        monthHView.textSize = 21.0f;
        monthLView.textSize = 21.0f;
        dateSepView2.textSize = 21.0f;
        dayHView.textSize = 21.0f;
        dayLView.textSize = 21.0f;
        weekView.textSize = 21.0f;
        year0View.textColor = -1;
        year1View.textColor = -1;
        year2View.textColor = -1;
        year3View.textColor = -1;
        dateSepView1.textColor = -1;
        monthHView.textColor = -1;
        monthLView.textColor = -1;
        dateSepView2.textColor = -1;
        dayHView.textColor = -1;
        dayLView.textColor = -1;
        weekView.textColor = -1;
        year0View.setStringPictureArray(this.digitalNums);
        year1View.setStringPictureArray(this.digitalNums);
        year2View.setStringPictureArray(this.digitalNums);
        year3View.setStringPictureArray(this.digitalNums);
        dateSepView1.setStringPicture('-');
        monthHView.setStringPictureArray(this.digitalNums);
        monthLView.setStringPictureArray(this.digitalNums);
        dateSepView2.setStringPicture('-');
        dayHView.setStringPictureArray(this.digitalNums);
        dayLView.setStringPictureArray(this.digitalNums);
        weekView.setStringPictureArray(getApplicationContext().getResources().getStringArray(R.array.weekdays));
        weekView.padding.left = (short) 8;
        dateLinearLayout.add(year3View);
        dateLinearLayout.add(year2View);
        dateLinearLayout.add(year1View);
        dateLinearLayout.add(year0View);
        dateLinearLayout.add(dateSepView1);
        dateLinearLayout.add(monthHView);
        dateLinearLayout.add(monthLView);
        dateLinearLayout.add(dateSepView2);
        dateLinearLayout.add(dayHView);
        dateLinearLayout.add(dayLView);
        dateLinearLayout.add(weekView);
        dateLinearLayout.setStart(0, 120);
        dateLinearLayout.setRect(320, 99);
        dateLinearLayout.alignX = (byte) 2;
        Typeface timeTypeface = TypefaceManager.get().createFromAsset("typeface/AvenirLTStd-Medium.otf");
        if (timeTypeface == null) {
            timeTypeface = Typeface.DEFAULT;
        }
        hourHView.textColor = -1;
        hourLView.textColor = -1;
        timeSepView.textColor = -1;
        minuteHView.textColor = -1;
        minuteLView.textColor = -1;
        hourHView.textSize = 71.5f;
        hourLView.textSize = 71.5f;
        timeSepView.textSize = 50.0f;
        minuteHView.textSize = 71.5f;
        minuteLView.textSize = 71.5f;
        hourHView.typeface = timeTypeface;
        hourLView.typeface = timeTypeface;
        timeSepView.typeface = timeTypeface;
        minuteHView.typeface = timeTypeface;
        minuteLView.typeface = timeTypeface;
        hourHView.setStringPictureArray(this.digitalNums);
        hourLView.setStringPictureArray(this.digitalNums);
        timeSepView.setStringPicture(":");
        minuteHView.setStringPictureArray(this.digitalNums);
        minuteLView.setStringPictureArray(this.digitalNums);
        timeSepView.padding.top = (short) 10;
        timeLinearLayout.add(hourHView);
        timeLinearLayout.add(hourLView);
        timeLinearLayout.add(timeSepView);
        timeLinearLayout.add(minuteHView);
        timeLinearLayout.add(minuteLView);
        timeLinearLayout.setStart(0, 43);
        timeLinearLayout.setRect(320, 99);
        timeLinearLayout.alignX = (byte) 2;
        byte[] bgImg = SimpleFile.readFileFromAssets(this, "slpt-digital-sunrise/bg.png");
        if (bgImg != null) {
            bgView.setImagePicture(bgImg);
        }
        rootLayout.add(bgView);
        rootLayout.add(timeLinearLayout);
        rootLayout.add(dateLinearLayout);
        rootLayout.add(this.lowBatteryView);
        return rootLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        SlptAbsoluteLayout rootLayout = new SlptAbsoluteLayout();
        SlptLinearLayout dateLinearLayout = new SlptLinearLayout();
        SlptViewComponent year0View = new SlptYear0View();
        SlptViewComponent year1View = new SlptYear1View();
        SlptViewComponent year2View = new SlptYear2View();
        SlptViewComponent year3View = new SlptYear3View();
        SlptPictureView dateSepView1 = new SlptPictureView();
        SlptMonthHView monthHView = new SlptMonthHView();
        SlptViewComponent monthLView = new SlptMonthLView();
        SlptPictureView dateSepView2 = new SlptPictureView();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        SlptViewComponent weekView = new SlptWeekView();
        SlptLinearLayout timeLinearLayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptPictureView timeSepView = new SlptPictureView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        SlptPictureView bgView = new SlptPictureView();
        year0View.textSize = 21.0f;
        year1View.textSize = 21.0f;
        year2View.textSize = 21.0f;
        year3View.textSize = 21.0f;
        dateSepView1.textSize = 21.0f;
        monthHView.textSize = 21.0f;
        monthLView.textSize = 21.0f;
        dateSepView2.textSize = 21.0f;
        dayHView.textSize = 21.0f;
        dayLView.textSize = 21.0f;
        weekView.textSize = 21.0f;
        year0View.textColor = -1;
        year1View.textColor = -1;
        year2View.textColor = -1;
        year3View.textColor = -1;
        dateSepView1.textColor = -1;
        monthHView.textColor = -1;
        monthLView.textColor = -1;
        dateSepView2.textColor = -1;
        dayHView.textColor = -1;
        dayLView.textColor = -1;
        weekView.textColor = -1;
        year0View.setStringPictureArray(this.digitalNums);
        year1View.setStringPictureArray(this.digitalNums);
        year2View.setStringPictureArray(this.digitalNums);
        year3View.setStringPictureArray(this.digitalNums);
        dateSepView1.setStringPicture('-');
        monthHView.setStringPictureArray(this.digitalNums);
        monthLView.setStringPictureArray(this.digitalNums);
        dateSepView2.setStringPicture('-');
        dayHView.setStringPictureArray(this.digitalNums);
        dayLView.setStringPictureArray(this.digitalNums);
        weekView.setStringPictureArray(getApplicationContext().getResources().getStringArray(R.array.weekdays));
        weekView.padding.left = (short) 8;
        dateLinearLayout.add(year3View);
        dateLinearLayout.add(year2View);
        dateLinearLayout.add(year1View);
        dateLinearLayout.add(year0View);
        dateLinearLayout.add(dateSepView1);
        dateLinearLayout.add(monthHView);
        dateLinearLayout.add(monthLView);
        dateLinearLayout.add(dateSepView2);
        dateLinearLayout.add(dayHView);
        dateLinearLayout.add(dayLView);
        dateLinearLayout.add(weekView);
        dateLinearLayout.setStart(0, 120);
        dateLinearLayout.setRect(320, 99);
        dateLinearLayout.alignX = (byte) 2;
        Typeface timeTypeface = TypefaceManager.get().createFromAsset("typeface/AvenirLTStd-Medium.otf");
        if (timeTypeface == null) {
            timeTypeface = Typeface.DEFAULT;
        }
        hourHView.textColor = -1;
        hourLView.textColor = -1;
        timeSepView.textColor = -1;
        minuteHView.textColor = -1;
        minuteLView.textColor = -1;
        hourHView.textSize = 71.5f;
        hourLView.textSize = 71.5f;
        timeSepView.textSize = 50.0f;
        minuteHView.textSize = 71.5f;
        minuteLView.textSize = 71.5f;
        hourHView.typeface = timeTypeface;
        hourLView.typeface = timeTypeface;
        timeSepView.typeface = timeTypeface;
        minuteHView.typeface = timeTypeface;
        minuteLView.typeface = timeTypeface;
        hourHView.setStringPictureArray(this.digitalNums);
        hourLView.setStringPictureArray(this.digitalNums);
        timeSepView.setStringPicture(":");
        minuteHView.setStringPictureArray(this.digitalNums);
        minuteLView.setStringPictureArray(this.digitalNums);
        timeSepView.padding.top = (short) 10;
        timeLinearLayout.add(hourHView);
        timeLinearLayout.add(hourLView);
        timeLinearLayout.add(timeSepView);
        timeLinearLayout.add(minuteHView);
        timeLinearLayout.add(minuteLView);
        timeLinearLayout.setStart(0, 43);
        timeLinearLayout.setRect(320, 99);
        timeLinearLayout.alignX = (byte) 2;
        byte[] bgImg = SimpleFile.readFileFromAssets(this, "slpt-digital-sunrise/bg_26wc.png");
        if (bgImg != null) {
            bgView.setImagePicture(bgImg);
        }
        rootLayout.add(bgView);
        rootLayout.add(timeLinearLayout);
        rootLayout.add(dateLinearLayout);
        rootLayout.add(this.lowBatteryView);
        return rootLayout;
    }
}
