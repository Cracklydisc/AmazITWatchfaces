package com.huami.watch.watchface;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Region.Op;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.TextPaint;
import android.view.animation.LinearInterpolator;
import com.huami.watch.watchface.AbstractWatchFace.DigitalEngine;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;

public class SunriseDigitalWatchFace extends AbstractWatchFace {

    private class Engine extends DigitalEngine {
        private final LinearInterpolator LINEAR;
        private boolean isSlowAnimStart;
        private ValueAnimator mAnimCloud;
        private Handler mAnimHandler;
        private ValueAnimator mAnimSun;
        private Bitmap mBgBmp;
        private Paint mBmpPaint;
        private Path mClipPath;
        private Bitmap mCloud0Bmp;
        private Bitmap mCloud1Bmp;
        private long mCloudStartTime;
        private long mCurrentAnimTime;
        private TextPaint mDatePaint;
        private Bitmap mFgBmp;
        private ValueAnimator mInitAnim;
        private int mLight;
        private Bitmap mLightBmp;
        private Paint mLightPaint;
        private float mPosXCloud0;
        private float mPosXCloud1;
        private float mPosXSun;
        private float mPosYSun;
        private Runnable mSlowAnimRunnable;
        private Bitmap mSunBmp;
        private long mSunStartTime;
        private TextPaint mTimePaint;
        private TextPaint mWeekPaint;

        class C02231 implements AnimatorUpdateListener {
            C02231() {
            }

            public void onAnimationUpdate(ValueAnimator animation) {
                float input = ((Float) animation.getAnimatedValue()).floatValue();
                float degree = 2.5307274f + (-1.3962634f * input);
                Engine.this.mPosXSun = Engine.this.getSunX(degree);
                Engine.this.mPosYSun = Engine.this.getSunY(degree);
                if (input < 0.125f) {
                    Engine.this.setLight((int) ((input * 255.0f) / 0.125f));
                } else if (input > 0.875f) {
                    Engine.this.setLight((int) (((1.0f - input) * 255.0f) / 0.125f));
                } else {
                    Engine.this.setLight(255);
                }
            }
        }

        class C02242 implements AnimatorUpdateListener {
            C02242() {
            }

            public void onAnimationUpdate(ValueAnimator animation) {
                float input = ((Float) animation.getAnimatedValue()).floatValue();
                Engine.this.mPosXCloud0 = 320.0f + (-398.0f * input);
                Engine.this.mPosXCloud1 = 140.0f + (180.0f * input);
                Engine.this.invalidate();
            }
        }

        class C02253 implements AnimatorUpdateListener {
            C02253() {
            }

            public void onAnimationUpdate(ValueAnimator animation) {
                float input = ((Float) animation.getAnimatedValue()).floatValue();
                float degree = 2.5307274f + (-0.71244335f * input);
                Engine.this.mPosXSun = Engine.this.getSunX(degree);
                Engine.this.mPosYSun = Engine.this.getSunY(degree);
                Engine.this.setLight((int) (255.0f * input));
                Engine.this.invalidate();
            }
        }

        class C02264 extends AnimatorListenerAdapter {
            C02264() {
            }

            public void onAnimationEnd(Animator animation) {
                Engine.this.mCurrentAnimTime = System.currentTimeMillis();
                Engine.this.mSunStartTime = Engine.this.mCurrentAnimTime - 61230;
                Engine.this.mCloudStartTime = Engine.this.mCurrentAnimTime;
                Engine.this.mAnimSun.setCurrentPlayTime(61230);
                Engine.this.startSlowAnim();
            }
        }

        class C02275 implements Runnable {
            C02275() {
            }

            public void run() {
                Engine.this.mCurrentAnimTime = System.currentTimeMillis();
                if (Engine.this.mCurrentAnimTime < Engine.this.mCloudStartTime) {
                    Engine.this.mSunStartTime = Engine.this.mCurrentAnimTime;
                    Engine.this.mCloudStartTime = Engine.this.mCurrentAnimTime;
                }
                Engine.this.mAnimSun.setCurrentPlayTime((Engine.this.mCurrentAnimTime - Engine.this.mSunStartTime) % 120000);
                Engine.this.mAnimCloud.setCurrentPlayTime((Engine.this.mCurrentAnimTime - Engine.this.mCloudStartTime) % 60000);
                if (Engine.this.isSlowAnimStart) {
                    Engine.this.mAnimHandler.postDelayed(this, 333);
                }
            }
        }

        private Engine() {
            super();
            this.mLight = 0;
            this.LINEAR = new LinearInterpolator();
            this.mAnimHandler = new Handler();
            this.isSlowAnimStart = false;
            this.mSlowAnimRunnable = new C02275();
        }

        protected void onPrepareResources(Resources resources) {
            this.mTimePaint = new TextPaint(1);
            this.mTimePaint.setColor(-1);
            this.mTimePaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/AvenirLTStd-Medium.otf"));
            this.mTimePaint.setTextSize(resources.getDimension(R.dimen.digital_sunrise_time_font));
            this.mTimePaint.setShadowLayer(2.0f, 0.0f, 3.0f, 939524096);
            this.mDatePaint = new TextPaint(1);
            this.mDatePaint.setColor(-1);
            this.mDatePaint.setFakeBoldText(true);
            this.mDatePaint.setTypeface(TypefaceManager.get().createFromAsset("typeface/Century-Gothic.ttf"));
            this.mDatePaint.setTextSize(resources.getDimension(R.dimen.digital_sunrise_date_font));
            this.mWeekPaint = new TextPaint(1);
            this.mWeekPaint.setTypeface(Typeface.SANS_SERIF);
            this.mWeekPaint.setColor(-1);
            this.mWeekPaint.setTextSize(resources.getDimension(R.dimen.digital_sunrise_week_font));
            this.mWeekPaint.setShadowLayer(2.0f, 0.0f, 3.0f, 939524096);
            this.mClipPath = new Path();
            this.mClipPath.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
            this.mBmpPaint = new Paint(7);
            this.mLightPaint = new Paint(7);
            Resources res = SunriseDigitalWatchFace.this.getResources();
            this.mBgBmp = BitmapFactory.decodeResource(res, R.drawable.watchface_bg_digital_sunrise);
            this.mFgBmp = BitmapFactory.decodeResource(res, R.drawable.watchface_fg_digital_sunrise);
            this.mLightBmp = BitmapFactory.decodeResource(res, R.drawable.watchface_light_digital_sunrise);
            this.mSunBmp = BitmapFactory.decodeResource(res, R.drawable.watchface_sun_digital_sunrise);
            this.mCloud0Bmp = BitmapFactory.decodeResource(res, R.drawable.watchface_cloud_0_digital_sunrise);
            this.mCloud1Bmp = BitmapFactory.decodeResource(res, R.drawable.watchface_cloud_1_digital_sunrise);
            this.mPosXCloud0 = 320.0f;
            this.mPosXCloud1 = 140.0f;
            this.mPosXSun = getSunX(2.5307274f);
            this.mPosYSun = getSunY(2.5307274f);
            this.mLightPaint.setAlpha(this.mLight);
            this.mAnimSun = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mAnimSun.setDuration(120000);
            this.mAnimSun.setInterpolator(this.LINEAR);
            this.mAnimSun.addUpdateListener(new C02231());
            this.mAnimCloud = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mAnimCloud.setDuration(60000);
            this.mAnimCloud.setInterpolator(this.LINEAR);
            this.mAnimCloud.addUpdateListener(new C02242());
            this.mInitAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
            this.mInitAnim.setDuration(1000);
            this.mInitAnim.setStartDelay(500);
            this.mInitAnim.setInterpolator(new LinearInterpolator());
            this.mInitAnim.addUpdateListener(new C02253());
            this.mInitAnim.addListener(new C02264());
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            if (visible) {
                this.mPosXCloud0 = 320.0f;
                this.mPosXCloud1 = 140.0f;
                this.mPosXSun = getSunX(2.5307274f);
                this.mPosYSun = getSunY(2.5307274f);
                this.mLightPaint.setAlpha(0);
                this.mInitAnim.start();
                return;
            }
            if (this.mInitAnim.isStarted()) {
                this.mInitAnim.cancel();
            }
            cancelSlowAnim();
            this.mPosXCloud0 = 320.0f;
            this.mPosXCloud1 = 140.0f;
            this.mPosXSun = getSunX(2.5307274f);
            this.mPosYSun = getSunY(2.5307274f);
            this.mLightPaint.setAlpha(0);
        }

        private void setLight(int alpha) {
            if (this.mLight != alpha) {
                this.mLight = alpha;
                this.mLightPaint.setAlpha(this.mLight);
            }
        }

        private float getSunX(float degree) {
            return 27.8f + (((float) Math.cos((double) degree)) * 76.35f);
        }

        private float getSunY(float degree) {
            return 202.0f - (((float) Math.sin((double) degree)) * 76.35f);
        }

        protected void onDrawDigital(Canvas canvas, float width, float height, float centerX, float centerY, int seconds, int minutes, int hours, int year, int month, int day, int week, int ampm) {
            canvas.save(2);
            canvas.clipPath(this.mClipPath, Op.INTERSECT);
            canvas.drawBitmap(this.mBgBmp, 0.0f, 0.0f, this.mBmpPaint);
            canvas.drawBitmap(this.mSunBmp, this.mPosXSun, this.mPosYSun, this.mBmpPaint);
            canvas.drawBitmap(this.mCloud0Bmp, this.mPosXCloud0, 0.0f, this.mBmpPaint);
            canvas.drawBitmap(this.mCloud1Bmp, this.mPosXCloud1, 0.0f, this.mBmpPaint);
            canvas.drawBitmap(this.mFgBmp, 0.0f, 0.0f, this.mBmpPaint);
            canvas.drawBitmap(this.mLightBmp, 0.0f, 0.0f, this.mLightPaint);
            this.mTimePaint.setTextAlign(Align.RIGHT);
            canvas.drawText(Util.formatTime(hours), 138.0f, 97.0f, this.mTimePaint);
            this.mTimePaint.setTextAlign(Align.LEFT);
            canvas.drawText(Util.formatTime(minutes), 166.0f, 97.0f, this.mTimePaint);
            if (!this.mDrawTimeIndicator) {
                this.mTimePaint.setTextAlign(Align.CENTER);
                canvas.drawText(":", 153.0f, 89.0f, this.mTimePaint);
            }
            canvas.drawText(year + "-" + month + "-" + day + " " + SunriseDigitalWatchFace.this.getApplicationContext().getResources().getStringArray(R.array.weekdays)[week - 1], 83.0f, 133.0f, this.mWeekPaint);
            canvas.restore();
        }

        private void startSlowAnim() {
            this.isSlowAnimStart = true;
            this.mSlowAnimRunnable.run();
        }

        private void cancelSlowAnim() {
            this.isSlowAnimStart = false;
            this.mAnimHandler.removeCallbacks(this.mSlowAnimRunnable);
            this.mAnimSun.setCurrentPlayTime(0);
            this.mAnimCloud.setCurrentPlayTime(0);
        }
    }

    protected Class<? extends AbstractSlptClock> slptClockClass() {
        return SunriseDigitalSlptClock.class;
    }

    public Engine onCreateEngine() {
        notifyStatusBarPosition(172.0f);
        return new Engine();
    }
}
