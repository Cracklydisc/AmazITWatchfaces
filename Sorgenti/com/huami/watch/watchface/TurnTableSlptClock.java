package com.huami.watch.watchface;

import android.content.Context;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogHourView;
import com.ingenic.iwds.slpt.view.analog.SlptAnalogMinuteView;
import com.ingenic.iwds.slpt.view.core.SlptFrameLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class TurnTableSlptClock extends AbstractSlptClock {
    private Context mContext;

    protected void initWatchFaceConfig() {
        this.mContext = getApplicationContext();
    }

    protected SlptLayout createClockLayout8C() {
        SlptLayout rootLayout = new SlptFrameLayout();
        SlptFrameLayout frameLayout = new SlptFrameLayout();
        SlptAnalogHourView hourView = new SlptAnalogHourView();
        SlptAnalogMinuteView minuteView = new SlptAnalogMinuteView();
        SlptPictureView blackbgView = new SlptPictureView();
        SlptPictureView bgView = new SlptPictureView();
        byte[] blackbgImg = SimpleFile.readFileFromAssets(this.mContext, "nothing.png");
        if (blackbgImg != null) {
            blackbgView.setImagePicture(blackbgImg);
            byte[] hourImg = SimpleFile.readFileFromAssets(this.mContext, "slpt-analog-turntable/hour_8c.png");
            if (hourImg != null) {
                hourView.setImagePicture(hourImg);
                byte[] minuteImg = SimpleFile.readFileFromAssets(this.mContext, "slpt-analog-turntable/min_8c.png");
                if (minuteImg != null) {
                    minuteView.setImagePicture(minuteImg);
                    byte[] bgImg = SimpleFile.readFileFromAssets(this.mContext, "slpt-analog-turntable/bg_8c.png");
                    if (bgImg != null) {
                        bgView.setImagePicture(bgImg);
                        frameLayout.add(blackbgView);
                        frameLayout.add(bgView);
                        frameLayout.add(minuteView);
                        frameLayout.add(hourView);
                        frameLayout.alignX = (byte) 2;
                        frameLayout.alignY = (byte) 2;
                        frameLayout.descHeight = (byte) 2;
                        frameLayout.descWidth = (byte) 2;
                        frameLayout.rect.height = 320;
                        frameLayout.rect.width = 320;
                        rootLayout.add(frameLayout);
                    }
                }
            }
        }
        return rootLayout;
    }

    protected SlptLayout createClockLayout26WC() {
        return null;
    }
}
