package com.huami.watch.watchface;

public interface WatchDataListener {
    int getDataType();

    void onDataUpdate(int i, Object... objArr);
}
