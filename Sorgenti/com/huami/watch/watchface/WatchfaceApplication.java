package com.huami.watch.watchface;

import android.app.Application;
import android.content.Intent;
import com.huami.watch.watchface.slpt.Lock.SlptLockScreenService;
import com.huami.watch.watchface.util.PointUtils;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.wearubc.UbcInterface;

public class WatchfaceApplication extends Application {
    private boolean needWatchFace = true;
    private int slptThemes = 0;

    public void onCreate() {
        super.onCreate();
        UbcInterface.initialize(this, "17", PointUtils.getApplicationVersion(this));
        TypefaceManager.init(getApplicationContext());
        CrashHandler.getInstance().init(getApplicationContext());
        Intent servicesIntent = new Intent(this, SlptLockScreenService.class);
        servicesIntent.setPackage(getPackageName());
        startService(servicesIntent);
        Util.cleanSportItemOrder();
    }

    public void setSlptThemes(int themes) {
        this.slptThemes = themes;
    }

    public int getSlptThems() {
        return this.slptThemes;
    }

    public String getSlptThemesDir(boolean needEnRes) {
        String language = "zh/";
        if (needEnRes) {
            language = "en/";
        }
        if (this.slptThemes == 1) {
            return "sport/white/" + language;
        }
        return "sport/black/" + language;
    }

    public void setNeedWatchFace(boolean flag) {
        this.needWatchFace = flag;
    }

    public boolean getNeedWatchFace() {
        return this.needWatchFace;
    }
}
