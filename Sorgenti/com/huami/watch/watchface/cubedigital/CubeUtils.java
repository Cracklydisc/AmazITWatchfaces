package com.huami.watch.watchface.cubedigital;

public class CubeUtils {
    public static final int[][][] FONTS;

    static {
        r0 = new int[10][][];
        r0[0] = new int[][]{new int[]{2, 5, 0}, new int[]{2, 6, 1}, new int[]{2, 4, 2}, new int[]{0, 7, 4}, new int[]{1, 8, 5}, new int[]{1, 4, 3}};
        r0[1] = new int[][]{new int[]{0, 3, 0}, new int[]{1, 4, 1}, new int[]{2, 2, 5}, new int[]{1, 4, 3}};
        r0[2] = new int[][]{new int[]{2, 5, 0}, new int[]{2, 6, 1}, new int[]{0, 3, 2}, new int[]{2, 8, 3}, new int[]{1, 0, 5}, new int[]{1, 4, 4}};
        r0[3] = new int[][]{new int[]{2, 1, 0}, new int[]{2, 4, 1}, new int[]{1, 4, 3}, new int[]{0, 3, 4}, new int[]{2, 4, 5}, new int[]{1, 4, 2}};
        r0[4] = new int[][]{new int[]{1, 3, 0}, new int[]{0, 3, 1}, new int[]{2, 4, 3}, new int[]{2, 4, 5}, new int[]{1, 4, 2}};
        r0[5] = new int[][]{new int[]{1, 4, 0}, new int[]{0, 2, 1}, new int[]{2, 6, 3}, new int[]{2, 3, 4}, new int[]{2, 8, 5}, new int[]{1, 4, 2}};
        r0[6] = new int[][]{new int[]{1, 5, 0}, new int[]{0, 2, 1}, new int[]{2, 6, 3}, new int[]{2, 7, 4}, new int[]{2, 8, 5}, new int[]{1, 4, 2}};
        r0[7] = new int[][]{new int[]{0, 1, 0}, new int[]{2, 3, 2}, new int[]{1, 2, 3}, new int[]{0, 2, 4}, new int[]{1, 4, 1}};
        r0[8] = new int[][]{new int[]{1, 9, 0}, new int[]{1, 10, 1}, new int[]{2, 5, 2}, new int[]{0, 6, 3}, new int[]{2, 7, 4}, new int[]{2, 8, 5}};
        r0[9] = new int[][]{new int[]{2, 5, 0}, new int[]{2, 6, 1}, new int[]{2, 7, 2}, new int[]{0, 3, 4}, new int[]{1, 8, 5}, new int[]{1, 4, 3}};
        FONTS = r0;
    }
}
