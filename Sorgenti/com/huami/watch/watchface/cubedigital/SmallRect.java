package com.huami.watch.watchface.cubedigital;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v7.recyclerview.C0051R;

public class SmallRect {
    private int mColor;
    private Paint mPaint = new Paint(5);
    private Path mPath = new Path();
    private int mPosition;
    private float mRectWidth;
    private int mShape;
    private float mStartX;
    private float mStartY;

    public SmallRect(int color, int shape, int position, float rectWidth) {
        this.mColor = color;
        this.mShape = shape;
        this.mPosition = position;
        this.mRectWidth = rectWidth;
        this.mPaint.setStyle(Style.FILL);
        Paint paint = this.mPaint;
        int i = color == 0 ? -256 : color == 1 ? -16776961 : -65536;
        paint.setColor(i);
    }

    private void initPosition(float input) {
        switch (this.mPosition) {
            case 0:
                this.mStartX = 0.0f;
                this.mStartY = this.mRectWidth * (1.0f - input);
                return;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.mStartX = this.mRectWidth - (this.mRectWidth * (1.0f - input));
                this.mStartY = this.mRectWidth * (1.0f - input);
                return;
            case 2:
                this.mStartX = 0.0f;
                this.mStartY = this.mRectWidth;
                return;
            case 3:
                this.mStartX = this.mRectWidth - (this.mRectWidth * (1.0f - input));
                this.mStartY = this.mRectWidth;
                return;
            case 4:
                this.mStartX = 0.0f;
                this.mStartY = (this.mRectWidth * 2.0f) - (this.mRectWidth * (1.0f - input));
                return;
            case 5:
                this.mStartX = this.mRectWidth - (this.mRectWidth * (1.0f - input));
                this.mStartY = (this.mRectWidth * 2.0f) - (this.mRectWidth * (1.0f - input));
                return;
            default:
                return;
        }
    }

    public void drawSmallRect(Canvas canvas, float input) {
        initPosition(input);
        switch (this.mShape) {
            case 0:
                this.mPath.reset();
                this.mPath.moveTo(this.mStartX, this.mStartY);
                this.mPath.lineTo(this.mStartX, this.mStartY + this.mRectWidth);
                this.mPath.lineTo(this.mStartX + this.mRectWidth, this.mStartY + this.mRectWidth);
                this.mPath.close();
                drawTriangle(canvas, this.mPath);
                return;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.mPath.reset();
                this.mPath.moveTo(this.mStartX, this.mStartY);
                this.mPath.lineTo(this.mStartX + this.mRectWidth, this.mStartY);
                this.mPath.lineTo(this.mStartX + this.mRectWidth, this.mStartY + this.mRectWidth);
                this.mPath.close();
                drawTriangle(canvas, this.mPath);
                return;
            case 2:
                this.mPath.reset();
                this.mPath.moveTo(this.mStartX, this.mStartY);
                this.mPath.lineTo(this.mStartX, this.mStartY + this.mRectWidth);
                this.mPath.lineTo(this.mStartX + this.mRectWidth, this.mStartY);
                this.mPath.close();
                drawTriangle(canvas, this.mPath);
                return;
            case 3:
                this.mPath.reset();
                this.mPath.moveTo(this.mStartX + this.mRectWidth, this.mStartY);
                this.mPath.lineTo(this.mStartX + this.mRectWidth, this.mStartY + this.mRectWidth);
                this.mPath.lineTo(this.mStartX, this.mStartY + this.mRectWidth);
                this.mPath.close();
                drawTriangle(canvas, this.mPath);
                return;
            case 4:
                drawRect(canvas, new RectF(this.mStartX, this.mStartY, this.mStartX + this.mRectWidth, this.mStartY + this.mRectWidth));
                return;
            case 5:
                drawArc(canvas, new RectF(this.mStartX, this.mStartY, this.mStartX + (this.mRectWidth * 2.0f), this.mStartY + (this.mRectWidth * 2.0f)), -90.0f, -90.0f);
                return;
            case 6:
                drawArc(canvas, new RectF(this.mStartX - this.mRectWidth, this.mStartY, this.mStartX + this.mRectWidth, this.mStartY + (this.mRectWidth * 2.0f)), -90.0f, 90.0f);
                return;
            case 7:
                drawArc(canvas, new RectF(this.mStartX, this.mStartY - this.mRectWidth, this.mStartX + (this.mRectWidth * 2.0f), this.mStartY + this.mRectWidth), 90.0f, 90.0f);
                return;
            case 8:
                drawArc(canvas, new RectF(this.mStartX - this.mRectWidth, this.mStartY - this.mRectWidth, this.mStartX + this.mRectWidth, this.mStartY + this.mRectWidth), 90.0f, -90.0f);
                return;
            case 9:
                drawArc(canvas, new RectF(this.mStartX + (this.mRectWidth / 2.0f), this.mStartY, this.mStartX + ((this.mRectWidth * 3.0f) / 2.0f), this.mStartY + this.mRectWidth), -90.0f, -180.0f);
                return;
            case 10:
                drawArc(canvas, new RectF(this.mStartX - (this.mRectWidth / 2.0f), this.mStartY, this.mStartX + (this.mRectWidth / 2.0f), this.mStartY + this.mRectWidth), -90.0f, 180.0f);
                return;
            default:
                return;
        }
    }

    private void drawTriangle(Canvas canvas, Path path) {
        canvas.drawPath(path, this.mPaint);
    }

    private void drawRect(Canvas canvas, RectF rect) {
        canvas.drawRect(rect, this.mPaint);
    }

    private void drawArc(Canvas canvas, RectF rect, float startAngle, float sweepAngle) {
        canvas.drawArc(rect, startAngle, sweepAngle, true, this.mPaint);
    }
}
