package com.huami.watch.watchface.cubedigital;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Canvas;
import android.support.v7.recyclerview.C0051R;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.List;

public class WholeNumber {
    private static final Interpolator CubicInOut = new C02325();
    public static boolean mIsHourHRun = false;
    public static boolean mIsHourLRun = false;
    public static boolean mIsMinHRun = false;
    public static boolean mIsMinLRun = false;
    private int mCurNumber;
    private ValueAnimator mInAnimator;
    private float mInput = 1.0f;
    private boolean mIsAnimationStarted = false;
    private boolean mIsNeedUpdated = false;
    private IWholeNumber mListener = null;
    private int mOldNumber;
    private ValueAnimator mOutAnimator;
    private int mPostion = -1;
    private float mRectWidth;
    private List<SmallRect> mSmallRect;
    private float mStartX;
    private float mStartY;

    public interface IWholeNumber {
        void refreshView();
    }

    class C02281 implements AnimatorUpdateListener {
        C02281() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            WholeNumber.this.mInput = 1.0f - ((Float) animation.getAnimatedValue()).floatValue();
            if (WholeNumber.this.mListener != null) {
                WholeNumber.this.mListener.refreshView();
            }
        }
    }

    class C02292 extends AnimatorListenerAdapter {
        private boolean mIsCanceled = false;

        C02292() {
        }

        public void onAnimationCancel(Animator animation) {
            this.mIsCanceled = true;
            WholeNumber.this.mOldNumber = WholeNumber.this.mCurNumber;
            WholeNumber.this.mInput = 1.0f;
            WholeNumber.this.mIsAnimationStarted = false;
        }

        public void onAnimationEnd(Animator animation) {
            WholeNumber.this.mOldNumber = WholeNumber.this.mCurNumber;
            if (this.mIsCanceled || !WholeNumber.this.mIsNeedUpdated) {
                WholeNumber.this.mIsAnimationStarted = false;
                return;
            }
            WholeNumber.this.mIsNeedUpdated = false;
            WholeNumber.this.updateSmallRectInfo(WholeNumber.this.mOldNumber, true, 0);
        }

        public void onAnimationStart(Animator animation) {
            this.mIsCanceled = false;
            WholeNumber.this.mIsAnimationStarted = true;
        }
    }

    class C02303 implements AnimatorUpdateListener {
        C02303() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            WholeNumber.this.mInput = ((Float) animation.getAnimatedValue()).floatValue();
            if (WholeNumber.this.mListener != null) {
                WholeNumber.this.mListener.refreshView();
            }
        }
    }

    class C02314 extends AnimatorListenerAdapter {
        C02314() {
        }

        public void onAnimationCancel(Animator animation) {
            WholeNumber.this.mOldNumber = WholeNumber.this.mCurNumber;
            WholeNumber.this.mInput = 1.0f;
            WholeNumber.this.mIsAnimationStarted = false;
        }

        public void onAnimationEnd(Animator animation) {
            WholeNumber.this.mOldNumber = WholeNumber.this.mCurNumber;
            WholeNumber.this.mIsAnimationStarted = false;
            WholeNumber.this.updateAnimationStatus(false);
        }

        public void onAnimationStart(Animator animation) {
            WholeNumber.this.mIsAnimationStarted = true;
        }
    }

    static class C02325 implements Interpolator {
        C02325() {
        }

        public float getInterpolation(float input) {
            input *= 2.0f;
            if (input < 1.0f) {
                return ((0.5f * input) * input) * input;
            }
            input -= 2.0f;
            return (((input * input) * input) + 2.0f) * 0.5f;
        }
    }

    public WholeNumber(float startX, float startY, float rectWidth, int position) {
        this.mRectWidth = rectWidth;
        this.mStartX = startX;
        this.mStartY = startY;
        this.mPostion = position;
        initAnimator();
    }

    public void setListener(IWholeNumber listener) {
        this.mListener = listener;
    }

    private void initAnimator() {
        this.mInAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        this.mInAnimator.addUpdateListener(new C02281());
        this.mInAnimator.addListener(new C02292());
        this.mInAnimator.setDuration(500);
        this.mInAnimator.setInterpolator(CubicInOut);
        this.mOutAnimator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        this.mOutAnimator.addUpdateListener(new C02303());
        this.mOutAnimator.addListener(new C02314());
        this.mOutAnimator.setDuration(500);
        this.mOutAnimator.setInterpolator(CubicInOut);
    }

    public boolean setCurNumber(int curNumber) {
        return setCurNumber(curNumber, 0);
    }

    public boolean setCurNumber(int curNumber, int delay) {
        if (this.mCurNumber == curNumber) {
            return false;
        }
        setCurNumber(curNumber, true, delay);
        return true;
    }

    public void setCurNumber(int curNumber, boolean isNeedUpdated, int delay) {
        if (!this.mIsAnimationStarted) {
            if (this.mPostion != 0 || (!mIsHourLRun && !mIsMinHRun && !mIsMinLRun)) {
                if (this.mPostion != 1 || (!mIsMinHRun && !mIsMinLRun)) {
                    if (this.mPostion != 2 || !mIsMinLRun) {
                        if (isNeedUpdated) {
                            this.mIsAnimationStarted = true;
                            updateAnimationStatus(true);
                        }
                        this.mCurNumber = curNumber;
                        this.mIsNeedUpdated = isNeedUpdated;
                        updateSmallRectInfo(curNumber, isNeedUpdated, delay);
                    }
                }
            }
        }
    }

    private void updateSmallRectInfo(int number, boolean isNeedUpdated, int delay) {
        int i;
        if (this.mSmallRect == null) {
            this.mSmallRect = new ArrayList();
        } else {
            this.mSmallRect.clear();
        }
        int[][][] iArr = CubeUtils.FONTS;
        if (isNeedUpdated) {
            i = this.mOldNumber;
        } else {
            i = number;
        }
        int[][] font = iArr[i];
        for (int[] rect : font) {
            this.mSmallRect.add(new SmallRect(rect[0], rect[1], rect[2], this.mRectWidth));
        }
        if (!isNeedUpdated) {
            this.mOldNumber = number;
            this.mCurNumber = number;
        } else if (this.mIsNeedUpdated) {
            this.mInAnimator.setStartDelay((long) delay);
            this.mInAnimator.start();
        } else {
            this.mOutAnimator.start();
        }
    }

    private void updateAnimationStatus(boolean isAnimated) {
        switch (this.mPostion) {
            case 0:
                mIsHourHRun = isAnimated;
                return;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                mIsHourLRun = isAnimated;
                return;
            case 2:
                mIsMinHRun = isAnimated;
                return;
            case 3:
                mIsMinLRun = isAnimated;
                return;
            default:
                return;
        }
    }

    public void drawSelf(Canvas canvas) {
        if (this.mSmallRect != null) {
            canvas.save();
            canvas.translate(this.mStartX, this.mStartY);
            for (int i = 0; i < this.mSmallRect.size(); i++) {
                ((SmallRect) this.mSmallRect.get(i)).drawSmallRect(canvas, this.mInput);
            }
            canvas.restore();
        }
    }
}
