package com.huami.watch.watchface.custom.listener;

import android.graphics.drawable.Drawable;
import com.huami.watch.common.widget.VerticalViewPager.OnPageChangeListener;

public interface ICustomChangeListener extends OnPageChangeListener {

    public enum ScrollDirection {
        UP,
        DOWN
    }

    void onBackgroundChanged(Drawable drawable, boolean z, int i);

    void onScrollDirectionChange(ScrollDirection scrollDirection);
}
