package com.huami.watch.watchface.custom.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

public abstract class AbsCustomViewHolder {
    protected Bitmap bitmap;
    protected Context context;
    protected Drawable drawable;
    protected PathInfo pathInfo;
    protected int position;
    protected View view;

    public abstract Drawable getDrawable();

    public abstract PathInfo getPathInfo();

    public AbsCustomViewHolder(Context context, View view, int position, PathInfo pathInfo, Drawable drawable) {
        this.position = -1;
        this.view = null;
        this.context = context;
        this.position = position;
        this.view = view;
        this.pathInfo = pathInfo;
        this.drawable = drawable;
    }

    public AbsCustomViewHolder(Context context, View view, int position, PathInfo pathInfo) {
        this(context, view, position, pathInfo, null);
        this.bitmap = BitmapFactory.decodeFile(pathInfo.path);
        if (this.bitmap != null) {
            this.drawable = new BitmapDrawable(context.getResources(), this.bitmap);
        }
    }

    public void destroy() {
        if (!(this.bitmap == null || this.bitmap.isRecycled())) {
            this.bitmap.recycle();
        }
        this.bitmap = null;
    }

    public String toString() {
        return "AbsCustomViewHolder{context=" + this.context + ", position=" + this.position + ", bitmap=" + this.bitmap + ", view=" + this.view + ", pathInfo=" + this.pathInfo + ", drawable=" + this.drawable + '}';
    }
}
