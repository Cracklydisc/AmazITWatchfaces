package com.huami.watch.watchface.custom.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ConfigInfo implements Parcelable {
    public static final Creator<ConfigInfo> CREATOR = new C02331();
    public int configType;
    public int position;

    static class C02331 implements Creator<ConfigInfo> {
        C02331() {
        }

        public ConfigInfo createFromParcel(Parcel in) {
            return new ConfigInfo(in);
        }

        public ConfigInfo[] newArray(int size) {
            return new ConfigInfo[size];
        }
    }

    public ConfigInfo(int configType) {
        this(configType, 0);
    }

    public ConfigInfo(int configType, int position) {
        this.configType = configType;
        this.position = position;
    }

    protected ConfigInfo(Parcel in) {
        this.configType = in.readInt();
        this.position = in.readInt();
    }

    public int getConfigType() {
        return this.configType;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.configType);
        dest.writeInt(this.position);
    }
}
