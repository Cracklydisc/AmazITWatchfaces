package com.huami.watch.watchface.custom.model;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

public class CustomViewHolder extends AbsCustomViewHolder {
    public CustomViewHolder(Context context, View view, int position, PathInfo pathInfo) {
        super(context, view, position, pathInfo);
    }

    public PathInfo getPathInfo() {
        return this.pathInfo;
    }

    public Drawable getDrawable() {
        return this.drawable;
    }
}
