package com.huami.watch.watchface.custom.model;

public class PathInfo {
    public boolean isSystemRes;
    public String path;

    public PathInfo(String path, boolean isSystemRes) {
        this.path = path;
        this.isSystemRes = isSystemRes;
    }
}
