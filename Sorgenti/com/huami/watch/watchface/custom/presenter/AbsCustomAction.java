package com.huami.watch.watchface.custom.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import com.huami.watch.watchface.custom.model.AbsCustomViewHolder;
import com.huami.watch.watchface.custom.model.PathInfo;
import com.huami.watch.watchface.custom.model.WatchFaceResType;
import com.huami.watch.watchface.custom.synctask.CustomTask.ICustomListener;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

public abstract class AbsCustomAction implements ICustomListener {
    private static final String TAG = CustomBackgroundAction.class.getName();
    protected Context mContext;
    private List<PathInfo> mFilePaths;
    private LayoutInflater mInflater;
    private int mResType = WatchFaceResType.NORMAL;
    private List<AbsCustomViewHolder> mViewHolders = null;
    private String mWatchFaceName = null;

    public abstract String getDestResPath();

    public abstract String getDestSlptPath();

    public abstract String getFolderName();

    public abstract String getSrcSlptPathName();

    public AbsCustomAction(Context context, String watchFaceName, int resType) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mWatchFaceName = watchFaceName;
        this.mResType = resType;
        init();
    }

    private void init() {
        this.mViewHolders = new LinkedList();
        this.mFilePaths = new LinkedList();
    }

    public int getCount() {
        if (this.mFilePaths == null) {
            return 0;
        }
        return this.mViewHolders.size();
    }

    public AbsCustomViewHolder getAdapterViewHolder(int position) {
        if (this.mViewHolders == null || position >= this.mViewHolders.size()) {
            return null;
        }
        return (AbsCustomViewHolder) this.mViewHolders.get(position);
    }

    public void destroyView() {
        if (this.mViewHolders != null) {
            for (int i = 0; i < this.mViewHolders.size(); i++) {
                ((AbsCustomViewHolder) this.mViewHolders.get(i)).destroy();
            }
        }
        this.mViewHolders.clear();
        this.mViewHolders = null;
        if (this.mFilePaths != null) {
            this.mFilePaths.clear();
        }
        this.mFilePaths = null;
    }

    public void saveCustomResult(int position) {
        String path = ((AbsCustomViewHolder) this.mViewHolders.get(position)).getPathInfo().path;
        if (path != null) {
            DataManager.getInstance(this.mContext).copyCustomRes(path, this.mContext.getFilesDir().getAbsolutePath() + getDestResPath());
            DataManager.getInstance(this.mContext).copyCustomRes(path.replaceFirst(getFolderName(), getSrcSlptPathName()), this.mContext.getFilesDir().getAbsolutePath() + getDestSlptPath());
            DataManager.getInstance(this.mContext).saveCustomReslut(this.mWatchFaceName, "Background", "@customBg/" + path.substring(path.lastIndexOf(File.separator) + 1), this.mResType);
        }
    }

    public void removeView(int position) {
        if (this.mViewHolders != null && this.mViewHolders.size() > position) {
            AbsCustomViewHolder viewHolder = (AbsCustomViewHolder) this.mViewHolders.remove(position);
            viewHolder.destroy();
            DataManager.getInstance(this.mContext).deleteRes(viewHolder.getPathInfo().path.substring(viewHolder.getPathInfo().path.lastIndexOf(File.separator)));
        }
    }

    public void onFileListFound(List<String> filePaths) {
        this.mViewHolders = LoadResManager.getInstance(this.mContext).loadWatchFaceCustomSDcardRes(filePaths);
    }
}
