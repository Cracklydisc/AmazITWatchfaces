package com.huami.watch.watchface.custom.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.custom.model.WatchFaceResType;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.BasePreviewConfig;
import java.util.List;

public abstract class AbsCustomSelectedItem {
    protected static final String TAG = AbsCustomSelectedItem.class.getName();
    protected Context mContext;
    protected int mResType = WatchFaceResType.NORMAL;
    protected WatchFaceConfigTemplate mTempLate;
    protected String mWatchFaceName;

    public abstract List<? extends BasePreviewConfig> getBackground();

    public abstract Drawable getBgDrawable();

    public abstract Drawable getFgDrawable();

    public abstract String getKey();

    public abstract int getSavedIndex();

    public AbsCustomSelectedItem(Context context, String watchFaceName, int resType) {
        this.mContext = context;
        this.mWatchFaceName = watchFaceName;
        this.mResType = resType;
    }

    public void setTemplate(WatchFaceConfigTemplate template) {
        this.mTempLate = template;
    }

    public void saveSelectedItem(int position) {
        DataManager.getInstance(this.mContext).saveCustomReslut(this.mWatchFaceName, getKey(), ((BasePreviewConfig) getBackground().get(position)).getConfig(), this.mResType);
    }
}
