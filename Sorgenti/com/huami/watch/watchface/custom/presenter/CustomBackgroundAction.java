package com.huami.watch.watchface.custom.presenter;

import android.content.Context;

public class CustomBackgroundAction extends AbsCustomAction {
    public CustomBackgroundAction(Context context, String watchFaceName, int resType) {
        super(context, watchFaceName, resType);
    }

    public String getFolderName() {
        return "customBg";
    }

    public String getDestResPath() {
        return "/watchface/customBg/";
    }

    public String getSrcSlptPathName() {
        return "customSlptBg";
    }

    public String getDestSlptPath() {
        return "/watchface/customSlptBg/";
    }
}
