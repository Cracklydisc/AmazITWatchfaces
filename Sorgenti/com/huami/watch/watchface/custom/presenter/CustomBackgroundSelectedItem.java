package com.huami.watch.watchface.custom.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.BasePreviewConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.SelectMask;
import com.huami.watch.wearubc.UbcInterface;
import java.util.List;

public class CustomBackgroundSelectedItem extends AbsCustomSelectedItem {
    public CustomBackgroundSelectedItem(Context context, String watchFaceName, int resType) {
        super(context, watchFaceName, resType);
    }

    public List<? extends BasePreviewConfig> getBackground() {
        if (this.mTempLate == null) {
            return null;
        }
        this.mTempLate.refresh();
        return this.mTempLate.getBackgrounds();
    }

    public String getKey() {
        return "Background";
    }

    public Drawable getFgDrawable() {
        if (this.mTempLate == null) {
            return null;
        }
        SelectMask mask = this.mTempLate.getBackgroundMask();
        if (mask == null) {
            return null;
        }
        return mask.getFgDrawable();
    }

    public Drawable getBgDrawable() {
        if (this.mTempLate == null) {
            return null;
        }
        SelectMask mask = this.mTempLate.getBackgroundMask();
        if (mask == null) {
            return null;
        }
        return mask.getBgDrawable();
    }

    public int getSavedIndex() {
        if (this.mTempLate != null) {
            return this.mTempLate.getSavedBackgroundIndex();
        }
        return 0;
    }

    public void saveSelectedItem(int position) {
        List<? extends BasePreviewConfig> backgroundList = getBackground();
        BasePreviewConfig background = (BasePreviewConfig) backgroundList.get(position);
        String lastConfig = DataManager.getInstance(this.mContext).getCustomReslut(this.mWatchFaceName, getKey(), this.mResType);
        DataManager.getInstance(this.mContext).saveCustomReslut(this.mWatchFaceName, getKey(), background.getConfig(), this.mResType);
        if (!(lastConfig == null || lastConfig.equals(background.getConfig())) || lastConfig == null) {
            UbcInterface.recordPropertyEvent("0001", background.getConfig());
            Log.i(TAG, "save not same background: " + background.getConfig());
        }
        if (position == backgroundList.size() - 1 && background.getConfig() != null && background.getConfig().contains("@customBg/")) {
            UbcInterface.recordPropertyEvent("0002", background.getConfig());
            Log.i(TAG, "save custom background: " + background.getConfig());
        }
    }
}
