package com.huami.watch.watchface.custom.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.BasePreviewConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetPositionConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.SelectMask;
import java.util.List;

public class CustomDataWidgetSelectedItem extends AbsCustomSelectedItem {
    private DataWidgetPositionConfig mDataConfig;

    public CustomDataWidgetSelectedItem(Context context, String watchFaceName, int resType) {
        super(context, watchFaceName, resType);
    }

    public CustomDataWidgetSelectedItem(Context context, String watchFaceName, DataWidgetPositionConfig dataWidgetPositionConfig, int resType) {
        this(context, watchFaceName, resType);
        this.mDataConfig = dataWidgetPositionConfig;
    }

    public List<? extends BasePreviewConfig> getBackground() {
        if (this.mDataConfig == null) {
            return null;
        }
        return this.mDataConfig.getDataWidgetConfigs();
    }

    public String getKey() {
        return this.mDataConfig.KEY;
    }

    public Drawable getFgDrawable() {
        if (this.mDataConfig == null) {
            return null;
        }
        SelectMask mask = this.mDataConfig.getDataWidgetMask();
        if (mask != null) {
            return mask.getFgDrawable();
        }
        return null;
    }

    public Drawable getBgDrawable() {
        if (this.mDataConfig == null) {
            return null;
        }
        SelectMask mask = this.mDataConfig.getDataWidgetMask();
        if (mask != null) {
            return mask.getBgDrawable();
        }
        return null;
    }

    public int getSavedIndex() {
        if (this.mDataConfig != null) {
            return this.mDataConfig.getSavedDataWidgetIndex();
        }
        return 0;
    }
}
