package com.huami.watch.watchface.custom.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.BasePreviewConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.SelectMask;
import java.util.List;

public class CustomGradutionSelectedItem extends AbsCustomSelectedItem {
    public CustomGradutionSelectedItem(Context context, String watchFaceName, int resType) {
        super(context, watchFaceName, resType);
    }

    public List<? extends BasePreviewConfig> getBackground() {
        if (this.mTempLate == null) {
            return null;
        }
        this.mTempLate.refresh();
        return this.mTempLate.getGraduations();
    }

    public String getKey() {
        return "Graduation";
    }

    public Drawable getFgDrawable() {
        if (this.mTempLate == null) {
            return null;
        }
        SelectMask mask = this.mTempLate.getGraduationMask();
        if (mask == null) {
            return null;
        }
        return mask.getFgDrawable();
    }

    public Drawable getBgDrawable() {
        if (this.mTempLate == null) {
            return null;
        }
        SelectMask mask = this.mTempLate.getGraduationMask();
        if (mask == null) {
            return null;
        }
        return mask.getBgDrawable();
    }

    public int getSavedIndex() {
        if (this.mTempLate != null) {
            return this.mTempLate.getSavedGraduationIndex();
        }
        return 0;
    }
}
