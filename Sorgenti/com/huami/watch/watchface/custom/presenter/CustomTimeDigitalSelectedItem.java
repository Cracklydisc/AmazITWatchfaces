package com.huami.watch.watchface.custom.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.BasePreviewConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.SelectMask;
import java.util.List;

public class CustomTimeDigitalSelectedItem extends AbsCustomSelectedItem {
    public CustomTimeDigitalSelectedItem(Context context, String watchFaceName, int resType) {
        super(context, watchFaceName, resType);
    }

    public List<? extends BasePreviewConfig> getBackground() {
        if (this.mTempLate == null) {
            return null;
        }
        this.mTempLate.refresh();
        return this.mTempLate.getTimeDigitals();
    }

    public String getKey() {
        return "TimeDigital";
    }

    public Drawable getFgDrawable() {
        if (this.mTempLate == null) {
            return null;
        }
        SelectMask mask = this.mTempLate.getTimeDigitalMask();
        if (mask == null) {
            return null;
        }
        return mask.getFgDrawable();
    }

    public Drawable getBgDrawable() {
        if (this.mTempLate == null) {
            return null;
        }
        SelectMask mask = this.mTempLate.getTimeDigitalMask();
        if (mask == null) {
            return null;
        }
        return mask.getBgDrawable();
    }

    public int getSavedIndex() {
        if (this.mTempLate != null) {
            return this.mTempLate.getSavedTimeDigitalIndex();
        }
        return 0;
    }
}
