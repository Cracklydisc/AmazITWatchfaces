package com.huami.watch.watchface.custom.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.BasePreviewConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.SelectMask;
import java.util.List;

public class CustomTimeHanderSelectedItem extends AbsCustomSelectedItem {
    public CustomTimeHanderSelectedItem(Context context, String watchFaceName, int resType) {
        super(context, watchFaceName, resType);
    }

    public List<? extends BasePreviewConfig> getBackground() {
        if (this.mTempLate == null) {
            return null;
        }
        this.mTempLate.refresh();
        return this.mTempLate.getTimeHands();
    }

    public String getKey() {
        return "TimeHand";
    }

    public Drawable getFgDrawable() {
        if (this.mTempLate == null) {
            return null;
        }
        SelectMask mask = this.mTempLate.getTimeHandMask();
        if (mask == null) {
            return null;
        }
        return mask.getFgDrawable();
    }

    public Drawable getBgDrawable() {
        if (this.mTempLate == null) {
            return null;
        }
        SelectMask mask = this.mTempLate.getTimeHandMask();
        if (mask == null) {
            return null;
        }
        return mask.getBgDrawable();
    }

    public int getSavedIndex() {
        if (this.mTempLate != null) {
            return this.mTempLate.getSavedTimeHandIndex();
        }
        return 0;
    }
}
