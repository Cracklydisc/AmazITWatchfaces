package com.huami.watch.watchface.custom.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.huami.watch.watchface.custom.model.WatchFaceResType;
import com.huami.watch.watchface.util.WatchFaceSave;
import java.io.File;
import java.io.IOException;

public class DataManager {
    private static final String TAG = DataManager.class.getName();
    private static DataManager sInstance = null;
    private Context mContext;

    public static synchronized DataManager getInstance(Context context) {
        DataManager dataManager;
        synchronized (DataManager.class) {
            synchronized (DataManager.class) {
                if (sInstance == null) {
                    sInstance = new DataManager(context.getApplicationContext());
                }
            }
            dataManager = sInstance;
        }
        return dataManager;
    }

    private DataManager(Context context) {
        this.mContext = context;
    }

    public void saveCustomReslut(String spName, String key, String value, int resType) {
        if (resType == WatchFaceResType.NORMAL) {
            Editor editor = getPreference(spName).edit();
            editor.putString(key, value);
            editor.commit();
            return;
        }
        WatchFaceSave preferences = WatchFaceSave.getWatchFaceSave("/sdcard/.watchface/current/" + spName);
        preferences.putString(key, value);
        preferences.commit();
    }

    public String getCustomReslut(String spName, String key, int resType) {
        if (resType == WatchFaceResType.NORMAL) {
            return getPreference(spName).getString(key, "");
        }
        return WatchFaceSave.getWatchFaceSave("/sdcard/.watchface/current/" + spName).getString(key, "");
    }

    public boolean isShowFirstTips() {
        return getPreference("first_tips").getBoolean("enable_first_tips", true);
    }

    public void hideFirstTips() {
        Editor editor = getPreference("first_tips").edit();
        editor.putBoolean("enable_first_tips", false);
        editor.commit();
    }

    public void copyCustomRes(String srcPath, String destPah) {
        Log.i(TAG, "srcPath:" + srcPath + ", destPath:" + destPah);
        File destFile = new File(destPah);
        if (!(destFile.exists() && destFile.isDirectory())) {
            destFile.mkdirs();
        }
        try {
            Runtime.getRuntime().exec("cp " + srcPath + " " + destPah);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private SharedPreferences getPreference(String spName) {
        return this.mContext.getSharedPreferences(spName, 0);
    }

    public void deleteRes(String fileName) {
        if (fileName != null) {
            Log.i(TAG, "deleteRes, fileName:" + fileName);
            String dataPath = "/data/data/com.huami.watch.watchface.analogyellow/files/watchface/customBg/" + fileName;
            String dataSlptPath = "/data/data/com.huami.watch.watchface.analogyellow/files/watchface/customSlptBg/" + fileName;
            String sdcardPath = "/sdcard/.watchface/customBg/" + fileName;
            String sdcardSlptPath = "/sdcard/.watchface/customSlptBg/" + fileName;
            File destFile = new File(dataPath);
            if (destFile != null && destFile.exists()) {
                Log.i(TAG, "deleteRes dataPath, iSuccess:" + destFile.delete());
            }
            destFile = new File(dataSlptPath);
            if (destFile != null && destFile.exists()) {
                Log.i(TAG, "deleteRes dataSlptPath, iSuccess:" + destFile.delete());
            }
            destFile = new File(sdcardPath);
            if (destFile != null && destFile.exists()) {
                Log.i(TAG, "deleteRes sdcardPath, iSuccess:" + destFile.delete());
            }
            destFile = new File(sdcardSlptPath);
            if (destFile != null && destFile.exists()) {
                Log.i(TAG, "deleteRes sdcardSlptPath, iSuccess:" + destFile.delete());
            }
        }
    }
}
