package com.huami.watch.watchface.custom.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import android.view.LayoutInflater;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.custom.model.AbsCustomViewHolder;
import com.huami.watch.watchface.custom.model.ConfigInfo;
import com.huami.watch.watchface.custom.model.CustomViewHolder;
import com.huami.watch.watchface.custom.model.PathInfo;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.BasePreviewConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetPositionConfig;
import java.util.LinkedList;
import java.util.List;

public class LoadResManager {
    private static final String TAG = LoadResManager.class.getName();
    private static LoadResManager sInstance = null;
    private Context mContext;

    public static LoadResManager getInstance(Context context) {
        synchronized (LoadResManager.class) {
            if (sInstance == null) {
                sInstance = new LoadResManager(context.getApplicationContext());
            }
        }
        return sInstance;
    }

    private LoadResManager(Context context) {
        this.mContext = context;
    }

    public List<AbsCustomViewHolder> loadWatchFaceCustomSDcardRes(List<String> backgroundPaths) {
        List<AbsCustomViewHolder> viewHolders = new LinkedList();
        Log.i(TAG, "-------------loadWatchFaceCustomSDcardRes, loadSdcardRes---------");
        loadWatchFaceBackgroundSDcardRes(viewHolders, backgroundPaths);
        return viewHolders;
    }

    private void loadWatchFaceBackgroundSDcardRes(List<AbsCustomViewHolder> viewHolders, List<String> backgroundPaths) {
        if (backgroundPaths != null) {
            int startPos = viewHolders.size();
            for (String path : backgroundPaths) {
                AbsCustomViewHolder viewHolder = new CustomViewHolder(this.mContext, LayoutInflater.from(this.mContext).inflate(R.layout.watch_face_custom_item, null), startPos, new PathInfo(path, false));
                Log.i(TAG, "viewhoudler:" + startPos + ":" + viewHolder.toString());
                if (viewHolder.getDrawable() != null) {
                    viewHolders.add(viewHolder);
                    startPos++;
                }
            }
        }
    }

    public Drawable getDefaultDrawable(WatchFaceConfigTemplate template, String watchName, ConfigInfo info) {
        if (template == null || info == null) {
            return null;
        }
        int backgroundType = info.configType;
        int position = info.position;
        List<? extends BasePreviewConfig> previewConfig;
        switch (backgroundType) {
            case 0:
                previewConfig = template.getBackgrounds();
                if (previewConfig == null || previewConfig.isEmpty()) {
                    return null;
                }
                return ((BasePreviewConfig) previewConfig.get(template.getSavedBackgroundIndex())).getPreviewDrawable();
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                previewConfig = template.getTimeHands();
                if (previewConfig == null || previewConfig.isEmpty()) {
                    return null;
                }
                return ((BasePreviewConfig) previewConfig.get(template.getSavedTimeHandIndex())).getPreviewDrawable();
            case 2:
                previewConfig = template.getGraduations();
                if (previewConfig == null || previewConfig.isEmpty()) {
                    return null;
                }
                return ((BasePreviewConfig) previewConfig.get(template.getSavedGraduationIndex())).getPreviewDrawable();
            case 3:
                previewConfig = ((DataWidgetPositionConfig) template.getDataWidgetPositionConfigs().get(position)).getDataWidgetConfigs();
                if (previewConfig == null || previewConfig.isEmpty()) {
                    return null;
                }
                return ((BasePreviewConfig) previewConfig.get(((DataWidgetPositionConfig) template.getDataWidgetPositionConfigs().get(position)).getSavedDataWidgetIndex())).getPreviewDrawable();
            case 4:
                previewConfig = template.getTimeDigitals();
                if (previewConfig == null || previewConfig.isEmpty()) {
                    return null;
                }
                return ((BasePreviewConfig) previewConfig.get(template.getSavedTimeDigitalIndex())).getPreviewDrawable();
            default:
                return null;
        }
    }
}
