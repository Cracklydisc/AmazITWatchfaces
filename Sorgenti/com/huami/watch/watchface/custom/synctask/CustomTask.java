package com.huami.watch.watchface.custom.synctask;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class CustomTask extends AsyncTask<String, Void, List<String>> {
    private static final String TAG = CustomTask.class.getName();
    private Context mContext;
    private ICustomListener mListener;

    public interface ICustomListener {
        void onFileListFound(List<String> list);
    }

    public CustomTask(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public void setCustomListener(ICustomListener listener) {
        this.mListener = listener;
    }

    protected List<String> doInBackground(String... params) {
        if (params == null || params.length < 1) {
            Log.i(TAG, "param is err");
            return null;
        } else if (!Environment.getExternalStorageState().equals("mounted")) {
            return null;
        } else {
            File dir = new File("/sdcard/.watchface/" + params[0]);
            if (dir == null || !dir.exists()) {
                dir.mkdirs();
                return null;
            }
            List<String> resources = new LinkedList();
            File[] files = dir.listFiles();
            if (files == null || files.length < 1) {
                return null;
            }
            for (File file : files) {
                if (file.isFile()) {
                    resources.add(file.getAbsolutePath());
                    Log.i(TAG, "doInBackground, file path:" + file.getAbsolutePath());
                }
            }
            return resources;
        }
    }

    protected void onPostExecute(List<String> filePaths) {
        super.onPostExecute(filePaths);
        if (this.mListener != null) {
            this.mListener.onFileListFound(filePaths);
        }
    }
}
