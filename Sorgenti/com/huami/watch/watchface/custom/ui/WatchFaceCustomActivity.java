package com.huami.watch.watchface.custom.ui;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.custom.model.WatchFaceResType;

public class WatchFaceCustomActivity extends FragmentActivity {
    private int mBackgroundType = -1;
    private int mResType = WatchFaceResType.NORMAL;
    private String mWatchName;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mWatchName = getIntent().getStringExtra("huami.watchface.servicename");
        this.mBackgroundType = getIntent().getIntExtra("huami.watchface.customtype", -1);
        this.mResType = getIntent().getIntExtra("watch_face_res_type", WatchFaceResType.NORMAL);
        setContentView(R.layout.custom_container);
        init();
    }

    private void init() {
        Fragment fragment = WatchFaceCustomFragment.newInstance(this.mWatchName, this.mBackgroundType, this.mResType);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commitAllowingStateLoss();
    }
}
