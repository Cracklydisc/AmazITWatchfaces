package com.huami.watch.watchface.custom.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.recyclerview.C0051R;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.GridLayoutManager.SpanSizeLookup;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.custom.model.WatchFaceResType;
import com.huami.watch.watchface.custom.presenter.AbsCustomAction;
import com.huami.watch.watchface.custom.presenter.CustomBackgroundAction;
import com.huami.watch.watchface.custom.synctask.CustomTask;
import com.huami.watch.watchface.custom.synctask.CustomTask.ICustomListener;
import java.util.List;

public class WatchFaceCustomFragment extends Fragment implements OnClickListener, ICustomListener {
    private int itemHeight = 104;
    private CustomAdapter mAdapter = null;
    private Button mControlBtn = null;
    private View mControlContainer = null;
    private AbsCustomAction mCustomAction = null;
    private int mCustomType = -1;
    private View mNoContentContainer = null;
    private RecyclerView mRecycleView = null;
    private int mResType = WatchFaceResType.NORMAL;
    private int mSeletedPos = -1;
    private CustomState mState = CustomState.NORMAL;
    private SparseArray<ViewHolder> mViewHolders;
    private String mWatchName = null;

    class C02341 extends SpanSizeLookup {
        C02341() {
        }

        public int getSpanSize(int position) {
            if (WatchFaceCustomFragment.this.mAdapter.getItemViewType(position) == 0 || WatchFaceCustomFragment.this.mAdapter.getItemViewType(position) == 3) {
                return 2;
            }
            return 1;
        }
    }

    public interface OnItemClickLitener {
        void onItemClick(ViewHolder viewHolder, int i);

        void onItemDeleteClick(ViewHolder viewHolder, int i);

        void onItemLongClick(ViewHolder viewHolder, int i);
    }

    class C02352 implements OnItemClickLitener {
        C02352() {
        }

        public void onItemClick(ViewHolder viewHolder, int position) {
            if (WatchFaceCustomFragment.this.mViewHolders == null) {
                return;
            }
            if (WatchFaceCustomFragment.this.mState == CustomState.NORMAL) {
                WatchFaceCustomFragment.this.mSeletedPos = position - 1;
                WatchFaceCustomFragment.this.mControlBtn.setText(WatchFaceCustomFragment.this.getActivity().getString(R.string.custom_selected_confirm));
                for (int i = 0; i < WatchFaceCustomFragment.this.mViewHolders.size(); i++) {
                    CustomContentViewHolder holder = (CustomContentViewHolder) WatchFaceCustomFragment.this.mViewHolders.get(WatchFaceCustomFragment.this.mViewHolders.keyAt(i));
                    int adpterPos = holder.getAdapterPosition();
                    if (adpterPos >= 0) {
                        if (adpterPos == position && WatchFaceCustomFragment.this.mState == CustomState.NORMAL) {
                            holder.showSelectedView(true);
                        } else {
                            holder.showSelectedView(false);
                        }
                    }
                }
                return;
            }
            WatchFaceCustomFragment.this.mSeletedPos = -1;
        }

        public void onItemLongClick(ViewHolder viewHolder, int position) {
            if (WatchFaceCustomFragment.this.mState == CustomState.NORMAL) {
                if (WatchFaceCustomFragment.this.mViewHolders != null) {
                    for (int i = 0; i < WatchFaceCustomFragment.this.mViewHolders.size(); i++) {
                        CustomContentViewHolder holder = (CustomContentViewHolder) WatchFaceCustomFragment.this.mViewHolders.get(WatchFaceCustomFragment.this.mViewHolders.keyAt(i));
                        holder.showSelectedView(false);
                        holder.showDeletedView(true);
                    }
                }
                WatchFaceCustomFragment.this.mState = CustomState.EDIT;
                WatchFaceCustomFragment.this.mControlBtn.setText(WatchFaceCustomFragment.this.getActivity().getString(R.string.custom_exit_delete));
            }
        }

        public void onItemDeleteClick(ViewHolder viewHolder, int position) {
            int holderPosition = viewHolder.getAdapterPosition();
            if (WatchFaceCustomFragment.this.mSeletedPos == holderPosition - 1) {
                WatchFaceCustomFragment.this.mSeletedPos = -1;
            } else if (WatchFaceCustomFragment.this.mSeletedPos > holderPosition - 1) {
                WatchFaceCustomFragment.access$620(WatchFaceCustomFragment.this, 1);
            }
            WatchFaceCustomFragment.this.mCustomAction.removeView(holderPosition - 1);
            WatchFaceCustomFragment.this.mViewHolders.remove(holderPosition);
            if (WatchFaceCustomFragment.this.mViewHolders.size() <= 0) {
                WatchFaceCustomFragment.this.mNoContentContainer.setVisibility(0);
            }
            WatchFaceCustomFragment.this.mAdapter.notifyItemRemoved(holderPosition);
        }
    }

    static /* synthetic */ class C02363 {
        static final /* synthetic */ int[] f12xe11d0728 = new int[CustomState.values().length];

        static {
            try {
                f12xe11d0728[CustomState.NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f12xe11d0728[CustomState.EDIT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    private class CustomAdapter extends Adapter {
        private OnItemClickLitener mOnItemClickLitener;

        private CustomAdapter() {
        }

        public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
            this.mOnItemClickLitener = mOnItemClickLitener;
        }

        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 0) {
                return new CustomHeaderViewHolder(LayoutInflater.from(WatchFaceCustomFragment.this.getActivity()).inflate(R.layout.custom_header_item, parent, false));
            } else if (viewType == 3) {
                return new CustomHeaderViewHolder(LayoutInflater.from(WatchFaceCustomFragment.this.getActivity()).inflate(R.layout.custom_footer_item, parent, false));
            } else if (viewType == 1) {
                return new CustomContentViewHolder(LayoutInflater.from(WatchFaceCustomFragment.this.getActivity()).inflate(R.layout.custom_content_item_left, parent, false));
            } else {
                return new CustomContentViewHolder(LayoutInflater.from(WatchFaceCustomFragment.this.getActivity()).inflate(R.layout.custom_content_item_right, parent, false));
            }
        }

        public void onBindViewHolder(final ViewHolder holder, final int position) {
            if (this.mOnItemClickLitener != null && getItemViewType(position) != 3 && getItemViewType(position) != 0) {
                int index = WatchFaceCustomFragment.this.mViewHolders.indexOfValue(holder);
                if (index >= 0) {
                    WatchFaceCustomFragment.this.mViewHolders.removeAt(index);
                }
                WatchFaceCustomFragment.this.mViewHolders.put(holder.getAdapterPosition(), holder);
                ((CustomContentViewHolder) holder).contentView.setBackground(WatchFaceCustomFragment.this.mCustomAction.getAdapterViewHolder(position - 1).getDrawable());
                ((CustomContentViewHolder) holder).showDeletedView(WatchFaceCustomFragment.this.mState == CustomState.EDIT);
                if (position == WatchFaceCustomFragment.this.mSeletedPos + 1) {
                    ((CustomContentViewHolder) holder).showSelectedView(true);
                } else {
                    ((CustomContentViewHolder) holder).showSelectedView(false);
                }
                ((CustomContentViewHolder) holder).contentView.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        CustomAdapter.this.mOnItemClickLitener.onItemClick(holder, position);
                    }
                });
                ((CustomContentViewHolder) holder).contentView.setOnLongClickListener(new OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        CustomAdapter.this.mOnItemClickLitener.onItemLongClick(holder, position);
                        return false;
                    }
                });
                ((CustomContentViewHolder) holder).mDeleteView.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        CustomAdapter.this.mOnItemClickLitener.onItemDeleteClick(holder, position);
                    }
                });
            }
        }

        public int getItemCount() {
            return WatchFaceCustomFragment.this.mCustomAction != null ? WatchFaceCustomFragment.this.mCustomAction.getCount() + 2 : 2;
        }

        public int getItemViewType(int position) {
            if (position == 0) {
                return 0;
            }
            if (position == getItemCount() - 1) {
                return 3;
            }
            if (position % 2 == 0) {
                return 2;
            }
            return 1;
        }
    }

    private class CustomContentViewHolder extends ViewHolder {
        private ImageView contentView;
        private View mDeleteView = null;
        private ImageView selectedView;

        public CustomContentViewHolder(View itemView) {
            super(itemView);
            itemView.setLayoutParams(new LayoutParams(-2, WatchFaceCustomFragment.this.itemHeight));
            this.contentView = (ImageView) itemView.findViewById(R.id.item_content);
            this.selectedView = (ImageView) itemView.findViewById(R.id.item_content_selected);
            this.mDeleteView = itemView.findViewById(R.id.item_deleted);
        }

        private void showSelectedView(boolean isShow) {
            this.selectedView.setVisibility(isShow ? 0 : 4);
        }

        private void showDeletedView(boolean isShow) {
            this.mDeleteView.setVisibility(isShow ? 0 : 4);
        }
    }

    private class CustomHeaderViewHolder extends ViewHolder {
        public CustomHeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public enum CustomState {
        NORMAL,
        EDIT
    }

    static /* synthetic */ int access$620(WatchFaceCustomFragment x0, int x1) {
        int i = x0.mSeletedPos - x1;
        x0.mSeletedPos = i;
        return i;
    }

    public static WatchFaceCustomFragment newInstance(String watchName, int type, int resType) {
        WatchFaceCustomFragment fragment = new WatchFaceCustomFragment();
        Bundle bundle = new Bundle();
        bundle.putString("huami.watchface.servicename", watchName);
        bundle.putInt("huami.watchface.customtype", type);
        bundle.putInt("watch_face_res_type", resType);
        fragment.setArguments(bundle);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            this.mWatchName = getArguments().getString("huami.watchface.servicename");
            this.mCustomType = getArguments().getInt("huami.watchface.customtype");
            this.mResType = getArguments().getInt("watch_face_res_type");
        }
        if (this.mCustomType == -1) {
            throw new IllegalArgumentException("err custom type");
        }
        if (this.mCustomType == 0) {
            this.mCustomAction = new CustomBackgroundAction(getActivity(), this.mWatchName, this.mResType);
        }
        View view = inflater.inflate(R.layout.custom_watchface_grid, null, false);
        this.mRecycleView = (RecyclerView) view.findViewById(R.id.recycler_view);
        this.mControlBtn = (Button) view.findViewById(R.id.control_btn);
        this.mControlContainer = view.findViewById(R.id.control_btn_container);
        this.mNoContentContainer = view.findViewById(R.id.no_content_container);
        this.mControlContainer.setOnClickListener(this);
        this.mControlBtn.setText(getActivity().getString(R.string.custom_none_selected));
        init();
        return view;
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.mViewHolders != null) {
            this.mViewHolders.clear();
        }
        if (this.mCustomAction != null) {
            this.mCustomAction.destroyView();
        }
    }

    private void init() {
        CustomTask task = new CustomTask(getActivity());
        task.setCustomListener(this);
        task.execute(new String[]{this.mCustomAction.getFolderName()});
    }

    public void onClick(View v) {
        if (v.getId() == R.id.control_btn_container) {
            switch (C02363.f12xe11d0728[this.mState.ordinal()]) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    if (this.mCustomAction != null && this.mSeletedPos != -1) {
                        this.mCustomAction.saveCustomResult(this.mSeletedPos);
                        Intent selectedIntent = new Intent();
                        selectedIntent.putExtra("custom_refresh_ui", true);
                        getActivity().setResult(-1, selectedIntent);
                        getActivity().finish();
                        return;
                    }
                    return;
                case 2:
                    if (this.mViewHolders != null) {
                        for (int i = 0; i < this.mViewHolders.size(); i++) {
                            CustomContentViewHolder holder = (CustomContentViewHolder) this.mViewHolders.get(this.mViewHolders.keyAt(i));
                            holder.showSelectedView(false);
                            holder.showDeletedView(false);
                        }
                    }
                    this.mState = CustomState.NORMAL;
                    this.mControlBtn.setText(getActivity().getString(R.string.custom_none_selected));
                    return;
                default:
                    return;
            }
        }
    }

    public void onFileListFound(List<String> filePaths) {
        if (filePaths == null || filePaths.size() <= 0) {
            this.mNoContentContainer.setVisibility(0);
        } else {
            this.mNoContentContainer.setVisibility(4);
        }
        if (this.mCustomAction != null) {
            this.mCustomAction.onFileListFound(filePaths);
        }
        initView();
    }

    private void initView() {
        this.itemHeight = getResources().getDimensionPixelOffset(R.dimen.custom_content_item_height);
        this.mViewHolders = new SparseArray();
        this.mAdapter = new CustomAdapter();
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setSpanSizeLookup(new C02341());
        this.mRecycleView.setLayoutManager(layoutManager);
        this.mRecycleView.setItemAnimator(null);
        this.mRecycleView.setAdapter(this.mAdapter);
        this.mAdapter.setOnItemClickLitener(new C02352());
    }
}
