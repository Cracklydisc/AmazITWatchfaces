package com.huami.watch.watchface.custom.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.recyclerview.C0051R;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.huami.watch.indicator.ViewPagerPageIndicator;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.custom.listener.ICustomChangeListener;
import com.huami.watch.watchface.custom.listener.ICustomChangeListener.ScrollDirection;
import com.huami.watch.watchface.custom.model.ConfigInfo;
import com.huami.watch.watchface.custom.model.WatchFaceResType;
import com.huami.watch.watchface.custom.presenter.DataManager;
import com.huami.watch.watchface.custom.presenter.LoadResManager;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.BasePreviewConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetPositionConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.OnLoadCompleteListener;
import java.util.LinkedList;
import java.util.List;

public class WatchFaceSettingsActivity extends FragmentActivity implements OnClickListener, ICustomChangeListener {
    private ImageView mBottomDigitalImg = null;
    private ImageView mBottomImg = null;
    private ImageView mBottomPointImg = null;
    private ImageView mBottomScaleImg = null;
    private List<ImageView> mConfigViews;
    private FrameLayout mCustomContainer = null;
    private float mDefaultMaxScale = 0.1f;
    private ViewPagerPageIndicator mIndicator = null;
    private OnLoadCompleteListener mLoadCompleteListener = new C02434();
    private AnimatorSet mLoadingAnimatorSet;
    private ImageView mLoadingView = null;
    private PageAdapter mPageAdapter = null;
    private List<ConfigInfo> mPageList;
    private ViewPager mPager = null;
    private int mResType = WatchFaceResType.NORMAL;
    private FrameLayout mRootView;
    private View mSaveContainer = null;
    private ScrollDirection mScrollDirection = ScrollDirection.DOWN;
    private WatchFaceConfigTemplate mTemplate;
    private TextView mTitleView = null;
    private ImageView mTopDigitalImg = null;
    private ImageView mTopImg = null;
    private ImageView mTopPointImg = null;
    private ImageView mTopScaleImg = null;
    private ValueAnimator mValueAnim = null;
    private SparseArray<WatchFaceSettingsFragment> mWactchFaceFragmentList;
    private String mWatchFaceName = null;
    private String mWatchFaceResName = null;

    class C02401 implements AnimatorUpdateListener {
        C02401() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            WatchFaceSettingsActivity.this.findViewById(R.id.tips).setAlpha(1.0f - ((Float) animation.getAnimatedValue()).floatValue());
        }
    }

    class C02412 extends AnimatorListenerAdapter {
        C02412() {
        }

        public void onAnimationCancel(Animator animation) {
            super.onAnimationCancel(animation);
            WatchFaceSettingsActivity.this.findViewById(R.id.tips).setVisibility(8);
        }

        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            WatchFaceSettingsActivity.this.findViewById(R.id.tips).setVisibility(8);
        }

        public void onAnimationStart(Animator animation) {
            super.onAnimationStart(animation);
            WatchFaceSettingsActivity.this.findViewById(R.id.tips).setVisibility(0);
        }
    }

    class C02423 implements OnPageChangeListener {
        C02423() {
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
        }

        public void onPageScrollStateChanged(int state) {
            WatchFaceSettingsActivity.this.refreshView(state);
        }
    }

    class C02434 implements OnLoadCompleteListener {
        C02434() {
        }

        public void onLoadComplete(int dataType) {
            switch (dataType) {
                case 0:
                    if (WatchFaceSettingsActivity.this.mLoadingAnimatorSet != null) {
                        WatchFaceSettingsActivity.this.mLoadingAnimatorSet.cancel();
                    }
                    if (DataManager.getInstance(WatchFaceSettingsActivity.this).isShowFirstTips()) {
                        DataManager.getInstance(WatchFaceSettingsActivity.this).hideFirstTips();
                        WatchFaceSettingsActivity.this.findViewById(R.id.tips).setVisibility(0);
                        WatchFaceSettingsActivity.this.initAnim();
                    } else {
                        WatchFaceSettingsActivity.this.findViewById(R.id.tips).setVisibility(8);
                    }
                    WatchFaceSettingsActivity.this.init();
                    WatchFaceSettingsActivity.this.initPageView();
                    break;
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    WatchFaceSettingsActivity.this.mTopScaleImg.setBackground(LoadResManager.getInstance(WatchFaceSettingsActivity.this).getDefaultDrawable(WatchFaceSettingsActivity.this.mTemplate, WatchFaceSettingsActivity.this.mWatchFaceName, new ConfigInfo(2)));
                    WatchFaceSettingsActivity.this.mBottomScaleImg.setBackground(LoadResManager.getInstance(WatchFaceSettingsActivity.this).getDefaultDrawable(WatchFaceSettingsActivity.this.mTemplate, WatchFaceSettingsActivity.this.mWatchFaceName, new ConfigInfo(2)));
                    if (WatchFaceSettingsActivity.this.hasGraduation()) {
                        WatchFaceSettingsActivity.this.mPageList.add(new ConfigInfo(2));
                        break;
                    }
                    break;
                case 2:
                    WatchFaceSettingsActivity.this.mTopPointImg.setBackground(LoadResManager.getInstance(WatchFaceSettingsActivity.this).getDefaultDrawable(WatchFaceSettingsActivity.this.mTemplate, WatchFaceSettingsActivity.this.mWatchFaceName, new ConfigInfo(1)));
                    WatchFaceSettingsActivity.this.mBottomPointImg.setBackground(LoadResManager.getInstance(WatchFaceSettingsActivity.this).getDefaultDrawable(WatchFaceSettingsActivity.this.mTemplate, WatchFaceSettingsActivity.this.mWatchFaceName, new ConfigInfo(1)));
                    if (WatchFaceSettingsActivity.this.hasTimeHander()) {
                        WatchFaceSettingsActivity.this.mPageList.add(new ConfigInfo(1));
                        break;
                    }
                    break;
                case 3:
                    if (WatchFaceSettingsActivity.this.hasDataWidget()) {
                        WatchFaceSettingsActivity.this.addDataWidgetPage();
                        if (WatchFaceSettingsActivity.this.mConfigViews != null) {
                            for (int i = 0; i < WatchFaceSettingsActivity.this.mConfigViews.size(); i++) {
                                ImageView view = (ImageView) WatchFaceSettingsActivity.this.mConfigViews.get(i);
                                Drawable drawable = LoadResManager.getInstance(WatchFaceSettingsActivity.this).getDefaultDrawable(WatchFaceSettingsActivity.this.mTemplate, WatchFaceSettingsActivity.this.mWatchFaceName, new ConfigInfo(3, i));
                                LayoutParams layoutParams = view.getLayoutParams();
                                layoutParams.width = drawable.getIntrinsicWidth();
                                layoutParams.height = drawable.getIntrinsicHeight();
                                view.setLayoutParams(layoutParams);
                                view.setBackground(drawable);
                                view.requestLayout();
                            }
                            break;
                        }
                    }
                    break;
                case 4:
                    WatchFaceSettingsActivity.this.mTopDigitalImg.setBackground(LoadResManager.getInstance(WatchFaceSettingsActivity.this).getDefaultDrawable(WatchFaceSettingsActivity.this.mTemplate, WatchFaceSettingsActivity.this.mWatchFaceName, new ConfigInfo(4)));
                    WatchFaceSettingsActivity.this.mBottomDigitalImg.setBackground(LoadResManager.getInstance(WatchFaceSettingsActivity.this).getDefaultDrawable(WatchFaceSettingsActivity.this.mTemplate, WatchFaceSettingsActivity.this.mWatchFaceName, new ConfigInfo(4)));
                    if (WatchFaceSettingsActivity.this.hasTimeDigital()) {
                        WatchFaceSettingsActivity.this.mPageList.add(new ConfigInfo(4));
                        break;
                    }
                    break;
            }
            WatchFaceSettingsActivity.this.mPageAdapter.notifyDataSetChanged();
            WatchFaceSettingsActivity.this.mPager.setOffscreenPageLimit(WatchFaceSettingsActivity.this.mPageList.size());
        }
    }

    class C02445 implements AnimatorUpdateListener {
        C02445() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            if (WatchFaceSettingsActivity.this.mLoadingView != null) {
                WatchFaceSettingsActivity.this.mLoadingView.setAlpha(((Float) animation.getAnimatedValue()).floatValue());
            }
        }
    }

    class C02456 implements AnimatorUpdateListener {
        C02456() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            if (WatchFaceSettingsActivity.this.mLoadingView != null) {
                WatchFaceSettingsActivity.this.mLoadingView.setRotation((float) ((Integer) animation.getAnimatedValue()).intValue());
            }
        }
    }

    class C02467 implements AnimatorUpdateListener {
        C02467() {
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            if (WatchFaceSettingsActivity.this.mLoadingView != null) {
                WatchFaceSettingsActivity.this.mLoadingView.setAlpha(1.0f - ((Float) animation.getAnimatedValue()).floatValue());
            }
        }
    }

    class C02478 extends AnimatorListenerAdapter {
        C02478() {
        }

        public void onAnimationCancel(Animator animation) {
            super.onAnimationCancel(animation);
            if (WatchFaceSettingsActivity.this.mLoadingView != null) {
                WatchFaceSettingsActivity.this.mLoadingView.setVisibility(8);
                WatchFaceSettingsActivity.this.findViewById(R.id.watch_face_content).setVisibility(0);
            }
        }

        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            if (WatchFaceSettingsActivity.this.mLoadingView != null) {
                WatchFaceSettingsActivity.this.mLoadingView.setVisibility(8);
                WatchFaceSettingsActivity.this.findViewById(R.id.watch_face_content).setVisibility(0);
            }
        }
    }

    private class PageAdapter extends FragmentPagerAdapter {
        public PageAdapter(FragmentManager fm) {
            super(fm);
        }

        public int getCount() {
            return WatchFaceSettingsActivity.this.mPageList.size();
        }

        public Fragment getItem(int position) {
            WatchFaceSettingsFragment fragment = WatchFaceSettingsFragment.getCorrespondingFragment((ConfigInfo) WatchFaceSettingsActivity.this.mPageList.get(position), WatchFaceSettingsActivity.this.mResType == WatchFaceResType.ZIP ? WatchFaceSettingsActivity.this.mWatchFaceResName : WatchFaceSettingsActivity.this.mWatchFaceName, position, WatchFaceSettingsActivity.this.mResType);
            fragment.setPageScrollListener(WatchFaceSettingsActivity.this);
            WatchFaceSettingsActivity.this.mWactchFaceFragmentList.put(position, fragment);
            return fragment;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            return (Fragment) super.instantiateItem(container, position);
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            WatchFaceSettingsActivity.this.mWactchFaceFragmentList.remove(position);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mWatchFaceName = getIntent().getStringExtra("huami.watchface.servicename");
        this.mWatchFaceResName = getIntent().getStringExtra("huami.watchface.resname");
        setContentView(R.layout.watch_face_main);
        this.mRootView = (FrameLayout) findViewById(R.id.scale_container);
        this.mCustomContainer = (FrameLayout) findViewById(R.id.custom_container);
        this.mLoadingView = (ImageView) findViewById(R.id.loading_view);
        initLoadingAnim();
        loadTemplate();
    }

    private void initAnim() {
        this.mValueAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        this.mValueAnim.setDuration(2000);
        this.mValueAnim.setInterpolator(new LinearInterpolator());
        this.mValueAnim.addUpdateListener(new C02401());
        this.mValueAnim.addListener(new C02412());
        this.mValueAnim.setStartDelay(2000);
        this.mValueAnim.start();
    }

    private void loadTemplate() {
        if (this.mPageList == null) {
            this.mPageList = new LinkedList();
        }
        if (this.mTemplate != null) {
            return;
        }
        if (this.mWatchFaceResName != null) {
            this.mResType = WatchFaceResType.ZIP;
            this.mTemplate = WatchFaceConfigTemplate.loadWatchFaceTemplate(this, this.mWatchFaceResName, this.mLoadCompleteListener);
            return;
        }
        this.mTemplate = WatchFaceConfigTemplate.loadWatchFaceTemplate(this, this.mWatchFaceName, this.mLoadCompleteListener);
    }

    public boolean hasBackground() {
        boolean z = true;
        if (this.mTemplate == null) {
            return false;
        }
        if (this.mTemplate.getBackgrounds().size() < 1) {
            z = false;
        }
        return z;
    }

    public boolean hasGraduation() {
        boolean z = true;
        if (this.mTemplate == null) {
            return false;
        }
        if (this.mTemplate.getGraduations().size() <= 1) {
            z = false;
        }
        return z;
    }

    public boolean hasTimeHander() {
        boolean z = true;
        if (this.mTemplate == null) {
            return false;
        }
        if (this.mTemplate.getTimeHands().size() <= 1) {
            z = false;
        }
        return z;
    }

    public boolean hasTimeDigital() {
        boolean z = true;
        if (this.mTemplate == null) {
            return false;
        }
        if (this.mTemplate.getTimeDigitals().size() <= 1) {
            z = false;
        }
        return z;
    }

    public boolean hasDataWidget() {
        if (this.mTemplate != null && this.mTemplate.getDataWidgetPositionConfigs().size() > 0) {
            return true;
        }
        return false;
    }

    private void addDataWidgetPage() {
        if (this.mTemplate != null) {
            List<DataWidgetPositionConfig> list = this.mTemplate.getDataWidgetPositionConfigs();
            if (list != null && !list.isEmpty()) {
                if (this.mConfigViews == null) {
                    this.mConfigViews = new LinkedList();
                }
                for (int i = 0; i < list.size(); i++) {
                    if (((DataWidgetPositionConfig) list.get(i)).getDataWidgetConfigs().size() > 1) {
                        this.mPageList.add(new ConfigInfo(3, i));
                    }
                    ImageView imageView = new ImageView(this);
                    this.mConfigViews.add(imageView);
                    this.mCustomContainer.addView(imageView, new LayoutParams(-2, -2));
                    imageView.setX((float) ((DataWidgetPositionConfig) list.get(i)).getX());
                    imageView.setY((float) ((DataWidgetPositionConfig) list.get(i)).getY());
                }
            }
        }
    }

    private void initPageCount() {
        if (hasBackground()) {
            this.mPageList.add(new ConfigInfo(0));
        }
        this.mRootView.setPivotX(160.0f);
        this.mRootView.setPivotY(160.0f);
        this.mRootView.setScaleX(0.74f);
        this.mRootView.setScaleY(0.74f);
        this.mRootView.requestLayout();
    }

    private void init() {
        initPageCount();
        this.mPager = (ViewPager) findViewById(R.id.view_pager);
        this.mIndicator = (ViewPagerPageIndicator) findViewById(R.id.indicator);
        this.mTopImg = (ImageView) findViewById(R.id.top_img);
        this.mBottomImg = (ImageView) findViewById(R.id.bottom_img);
        this.mSaveContainer = findViewById(R.id.save_btn_container);
        this.mTopPointImg = (ImageView) findViewById(R.id.top_point_img);
        this.mTopScaleImg = (ImageView) findViewById(R.id.top_scale_img);
        this.mBottomPointImg = (ImageView) findViewById(R.id.bottom_point_img);
        this.mBottomScaleImg = (ImageView) findViewById(R.id.bottom_scale_img);
        this.mTopDigitalImg = (ImageView) findViewById(R.id.top_digital_img);
        this.mBottomDigitalImg = (ImageView) findViewById(R.id.bottom_digital_img);
        this.mTitleView = (TextView) findViewById(R.id.title);
        this.mSaveContainer.setOnClickListener(this);
        this.mTopImg.setBackground(LoadResManager.getInstance(this).getDefaultDrawable(this.mTemplate, this.mWatchFaceName, new ConfigInfo(0)));
        this.mBottomImg.setBackground(LoadResManager.getInstance(this).getDefaultDrawable(this.mTemplate, this.mWatchFaceName, new ConfigInfo(0)));
        this.mTopScaleImg.setBackground(LoadResManager.getInstance(this).getDefaultDrawable(this.mTemplate, this.mWatchFaceName, new ConfigInfo(2)));
        this.mBottomScaleImg.setBackground(LoadResManager.getInstance(this).getDefaultDrawable(this.mTemplate, this.mWatchFaceName, new ConfigInfo(2)));
        this.mTopPointImg.setBackground(LoadResManager.getInstance(this).getDefaultDrawable(this.mTemplate, this.mWatchFaceName, new ConfigInfo(1)));
        this.mBottomPointImg.setBackground(LoadResManager.getInstance(this).getDefaultDrawable(this.mTemplate, this.mWatchFaceName, new ConfigInfo(1)));
        this.mTopDigitalImg.setBackground(LoadResManager.getInstance(this).getDefaultDrawable(this.mTemplate, this.mWatchFaceName, new ConfigInfo(4)));
        this.mBottomDigitalImg.setBackground(LoadResManager.getInstance(this).getDefaultDrawable(this.mTemplate, this.mWatchFaceName, new ConfigInfo(4)));
        this.mTitleView.setText(((BasePreviewConfig) this.mTemplate.getBackgrounds().get(0)).getTitle());
        this.mBottomImg.setAlpha(0.0f);
        this.mBottomPointImg.setAlpha(0.0f);
        this.mBottomDigitalImg.setAlpha(0.0f);
        this.mBottomScaleImg.setAlpha(0.0f);
        this.mPager.setOnPageChangeListener(new C02423());
        this.mPager.setOffscreenPageLimit(this.mPageList.size());
        this.mPager.setDefaultGutterSize(0);
    }

    public WatchFaceConfigTemplate getTemplate() {
        return this.mTemplate;
    }

    private void hideBottomView() {
        this.mBottomImg.setAlpha(0.0f);
        this.mBottomScaleImg.setAlpha(0.0f);
        this.mBottomPointImg.setAlpha(0.0f);
        this.mBottomDigitalImg.setAlpha(0.0f);
    }

    private void initTopView() {
        this.mTopImg.setScaleX(1.0f);
        this.mTopImg.setScaleY(1.0f);
        this.mTopImg.setAlpha(1.0f);
        this.mTopPointImg.setScaleX(1.0f);
        this.mTopPointImg.setScaleY(1.0f);
        this.mTopPointImg.setAlpha(1.0f);
        this.mTopDigitalImg.setScaleX(1.0f);
        this.mTopDigitalImg.setScaleY(1.0f);
        this.mTopDigitalImg.setAlpha(1.0f);
        this.mTopScaleImg.setScaleX(1.0f);
        this.mTopScaleImg.setScaleY(1.0f);
        this.mTopScaleImg.setAlpha(1.0f);
        this.mSaveContainer.setAlpha(1.0f);
        this.mSaveContainer.setClickable(true);
        if (this.mConfigViews != null) {
            for (ImageView imageView : this.mConfigViews) {
                imageView.setAlpha(1.0f);
            }
        }
    }

    private void hideAllView() {
        this.mSaveContainer.setClickable(false);
        this.mSaveContainer.setAlpha(0.0f);
        this.mTopPointImg.setAlpha(0.0f);
        this.mTopDigitalImg.setAlpha(0.0f);
        this.mBottomPointImg.setAlpha(0.0f);
        this.mBottomDigitalImg.setAlpha(0.0f);
        this.mTopScaleImg.setAlpha(0.0f);
        this.mBottomScaleImg.setAlpha(0.0f);
        if (this.mConfigViews != null) {
            for (ImageView imageView : this.mConfigViews) {
                imageView.setAlpha(0.0f);
            }
        }
    }

    private void changeBottomImg(int type) {
        switch (type) {
            case 0:
                this.mBottomImg.setBackground(this.mTopImg.getBackground());
                return;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.mBottomPointImg.setBackground(this.mTopPointImg.getBackground());
                return;
            case 2:
                this.mBottomScaleImg.setBackground(this.mTopScaleImg.getBackground());
                return;
            case 4:
                this.mBottomDigitalImg.setBackground(this.mTopDigitalImg.getBackground());
                return;
            default:
                return;
        }
    }

    private void refreshView(int state) {
        if (state == 0) {
            WatchFaceSettingsFragment fragment = (WatchFaceSettingsFragment) this.mWactchFaceFragmentList.get(this.mPager.getCurrentItem());
            this.mTitleView.setText(fragment.getTitle());
            hideBottomView();
            if (fragment.getItemType() != 0 || (fragment.getItemType() == 0 && fragment.getCount() - 1 != fragment.getCurrentItem())) {
                initTopView();
            } else if (fragment.getItemType() == 0 && this.mWactchFaceFragmentList != null && this.mPager != null && WatchFaceSettingsFragment.SHOWN_CUSTOM_USER) {
                if (fragment.getCurrentItem() == fragment.getCount() - 1) {
                    this.mTopImg.setScaleX(1.0f);
                    this.mTopImg.setScaleY(1.0f);
                    this.mTopImg.setAlpha(1.0f);
                    hideAllView();
                } else {
                    initTopView();
                }
            }
            if (((WatchFaceSettingsFragment) this.mWactchFaceFragmentList.get(0)).getCurrentItem() == ((WatchFaceSettingsFragment) this.mWactchFaceFragmentList.get(0)).getCount() - 1) {
                if (fragment.getItemType() != 0) {
                    this.mTopImg.setBackground(((BasePreviewConfig) this.mTemplate.getBackgrounds().get(0)).getPreviewDrawable());
                } else if (fragment.getItemType() == 0) {
                    this.mTopImg.setBackground(((WatchFaceSettingsFragment) this.mWactchFaceFragmentList.get(0)).getCurDrawable());
                }
            }
            changeBottomImg(fragment.getItemType());
            if (fragment.getItemType() == 3) {
                ImageView view = (ImageView) this.mConfigViews.get(fragment.getCurConfig().position);
                LayoutParams params = view.getLayoutParams();
                params.width = fragment.getCurDrawable().getIntrinsicWidth();
                params.height = fragment.getCurDrawable().getIntrinsicHeight();
                view.setLayoutParams(params);
                view.setBackground(fragment.getCurDrawable());
            }
        }
    }

    private void initPageView() {
        this.mWactchFaceFragmentList = new SparseArray();
        this.mPageAdapter = new PageAdapter(getSupportFragmentManager());
        this.mPager.setAdapter(this.mPageAdapter);
        this.mIndicator.setViewPager(this.mPager);
        this.mIndicator.showIndicator(true);
    }

    protected void onDestroy() {
        super.onDestroy();
        if (this.mValueAnim != null && (this.mValueAnim.isRunning() || this.mValueAnim.isStarted())) {
            this.mValueAnim.cancel();
        }
        if (this.mTemplate != null) {
            this.mTemplate.onDestroy();
        }
        this.mTemplate = null;
        if (this.mConfigViews != null) {
            this.mConfigViews.clear();
        }
        if (this.mLoadingAnimatorSet != null && this.mLoadingAnimatorSet.isRunning()) {
            this.mLoadingAnimatorSet.cancel();
        }
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        WatchFaceSettingsFragment fragment = (WatchFaceSettingsFragment) this.mWactchFaceFragmentList.get(this.mPager.getCurrentItem());
        if (fragment.getItemType() == 0) {
            this.mTopImg.setScaleX((this.mDefaultMaxScale * positionOffset) + 1.0f);
            this.mTopImg.setScaleY((this.mDefaultMaxScale * positionOffset) + 1.0f);
            this.mTopImg.setAlpha(1.0f - positionOffset);
            this.mBottomImg.setAlpha(positionOffset);
        } else if (fragment.getItemType() == 1) {
            this.mTopPointImg.setScaleX((this.mDefaultMaxScale * positionOffset) + 1.0f);
            this.mTopPointImg.setScaleY((this.mDefaultMaxScale * positionOffset) + 1.0f);
            this.mTopPointImg.setAlpha(1.0f - positionOffset);
            this.mBottomPointImg.setAlpha(positionOffset);
        } else if (fragment.getItemType() == 2) {
            this.mTopScaleImg.setScaleX((this.mDefaultMaxScale * positionOffset) + 1.0f);
            this.mTopScaleImg.setScaleY((this.mDefaultMaxScale * positionOffset) + 1.0f);
            this.mTopScaleImg.setAlpha(1.0f - positionOffset);
            this.mBottomScaleImg.setAlpha(positionOffset);
        } else if (fragment.getItemType() == 4) {
            this.mTopDigitalImg.setScaleX((this.mDefaultMaxScale * positionOffset) + 1.0f);
            this.mTopDigitalImg.setScaleY((this.mDefaultMaxScale * positionOffset) + 1.0f);
            this.mTopDigitalImg.setAlpha(1.0f - positionOffset);
            this.mBottomDigitalImg.setAlpha(positionOffset);
        }
        if (fragment.getItemType() == 0 && this.mWactchFaceFragmentList != null && this.mPager != null && WatchFaceSettingsFragment.SHOWN_CUSTOM_USER && position == ((WatchFaceSettingsFragment) this.mWactchFaceFragmentList.get(this.mPager.getCurrentItem())).getCount() - 2) {
            this.mSaveContainer.setAlpha(1.0f - positionOffset);
            this.mTopPointImg.setAlpha(1.0f - positionOffset);
            this.mTopDigitalImg.setAlpha(1.0f - positionOffset);
            this.mTopScaleImg.setAlpha(1.0f - positionOffset);
            if (this.mConfigViews != null) {
                for (ImageView view : this.mConfigViews) {
                    view.setAlpha(1.0f - positionOffset);
                }
            }
        }
    }

    public void onPageSelected(int position) {
        this.mTopImg.setScaleX(1.0f);
        this.mTopImg.setScaleY(1.0f);
        this.mTopImg.setAlpha(1.0f);
        this.mBottomImg.setBackground(this.mTopImg.getBackground());
    }

    public void onPageScrollStateChanged(int state) {
        refreshView(state);
    }

    public void onBackgroundChanged(Drawable drawable, boolean isChanged, int position) {
        int curItem = this.mPager.getCurrentItem();
        if (curItem == position) {
            WatchFaceSettingsFragment fragment = (WatchFaceSettingsFragment) this.mWactchFaceFragmentList.get(curItem);
            if (isChanged && this.mScrollDirection == ScrollDirection.DOWN) {
                if (fragment.getItemType() == 0) {
                    this.mBottomImg.setBackground(drawable);
                } else if (fragment.getItemType() == 1) {
                    this.mBottomPointImg.setBackground(drawable);
                } else if (fragment.getItemType() == 2) {
                    this.mBottomScaleImg.setBackground(drawable);
                } else if (fragment.getItemType() == 4) {
                    this.mBottomDigitalImg.setBackground(drawable);
                }
            } else if (fragment.getItemType() == 0) {
                this.mTopImg.setBackground(drawable);
            } else if (fragment.getItemType() == 1) {
                this.mTopPointImg.setBackground(drawable);
            } else if (fragment.getItemType() == 2) {
                this.mTopScaleImg.setBackground(drawable);
            } else if (fragment.getItemType() == 4) {
                this.mTopDigitalImg.setBackground(drawable);
            }
            if ((this.mWactchFaceFragmentList.size() > 0 && fragment.getItemType() != 0 && Float.compare(0.0f, this.mSaveContainer.getAlpha()) == 0) || (fragment.getItemType() == 0 && fragment.getCurrentItem() != fragment.getCount() - 1 && Float.compare(0.0f, this.mSaveContainer.getAlpha()) == 0)) {
                initTopView();
            }
        }
    }

    public void onScrollDirectionChange(ScrollDirection direction) {
        this.mScrollDirection = direction;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.save_btn_container) {
            if (this.mWactchFaceFragmentList != null) {
                for (int i = 0; i < this.mWactchFaceFragmentList.size(); i++) {
                    ((WatchFaceSettingsFragment) this.mWactchFaceFragmentList.get(i)).saveSelectedItem();
                }
                sendBroadcast(new Intent("com.huami.intent.action.WATCHFACE_CONFIG_CHANGED"));
            }
            setResult(-1);
            finish();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 256) {
            boolean isRefresh = false;
            if (data != null) {
                isRefresh = data.getBooleanExtra("custom_refresh_ui", false);
            }
            if (this.mWactchFaceFragmentList != null) {
                ((WatchFaceSettingsFragment) this.mWactchFaceFragmentList.get(this.mPager.getCurrentItem())).refreshUI(isRefresh);
            }
        }
    }

    private void initLoadingAnim() {
        if (this.mLoadingAnimatorSet == null) {
            this.mLoadingAnimatorSet = new AnimatorSet();
        }
        ValueAnimator alphaInAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        ValueAnimator rotateAnim = ValueAnimator.ofInt(new int[]{0, 360});
        final ValueAnimator alphaOutAnim = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        alphaInAnim.setDuration(300);
        alphaOutAnim.setDuration(160);
        rotateAnim.setRepeatCount(-1);
        rotateAnim.setInterpolator(new LinearInterpolator());
        rotateAnim.setDuration(1000);
        alphaInAnim.addUpdateListener(new C02445());
        rotateAnim.addUpdateListener(new C02456());
        alphaOutAnim.addUpdateListener(new C02467());
        alphaOutAnim.addListener(new C02478());
        this.mLoadingAnimatorSet.addListener(new AnimatorListenerAdapter() {
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                if (alphaOutAnim != null) {
                    alphaOutAnim.start();
                }
            }

            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (alphaOutAnim != null) {
                    alphaOutAnim.start();
                }
            }
        });
        this.mLoadingAnimatorSet.playSequentially(new Animator[]{alphaInAnim, rotateAnim});
        this.mLoadingAnimatorSet.start();
    }
}
