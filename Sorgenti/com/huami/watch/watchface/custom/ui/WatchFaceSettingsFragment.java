package com.huami.watch.watchface.custom.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import com.huami.watch.common.widget.VerticalViewPager;
import com.huami.watch.common.widget.VerticalViewPager.OnPageChangeListener;
import com.huami.watch.scrollbar.ArcScrollbarHelper;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.custom.listener.ICustomChangeListener;
import com.huami.watch.watchface.custom.listener.ICustomChangeListener.ScrollDirection;
import com.huami.watch.watchface.custom.model.ConfigInfo;
import com.huami.watch.watchface.custom.model.WatchFaceResType;
import com.huami.watch.watchface.custom.presenter.AbsCustomSelectedItem;
import com.huami.watch.watchface.custom.presenter.CustomBackgroundSelectedItem;
import com.huami.watch.watchface.custom.presenter.CustomDataWidgetSelectedItem;
import com.huami.watch.watchface.custom.presenter.CustomGradutionSelectedItem;
import com.huami.watch.watchface.custom.presenter.CustomTimeDigitalSelectedItem;
import com.huami.watch.watchface.custom.presenter.CustomTimeHanderSelectedItem;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.BasePreviewConfig;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetPositionConfig;
import java.util.LinkedList;
import java.util.List;

public class WatchFaceSettingsFragment extends Fragment {
    public static boolean SHOWN_CUSTOM_USER = true;
    private static final String TAG = WatchFaceSettingsFragment.class.getName();
    private boolean isChangeBackground = false;
    private WatchFaceSettingsActivity mActivity;
    private WatchFaceFragmentAdapter mAdapter = null;
    private int mCurItem = 0;
    private AbsCustomSelectedItem mCustomSelectedItem = null;
    private int mCustomType = -1;
    private ConfigInfo mInfo;
    private ICustomChangeListener mPageScrollListener;
    private VerticalViewPager mPager = null;
    private int mPosition = -1;
    private List<Drawable> mPreviewsList;
    private int mResType = WatchFaceResType.NORMAL;
    private String mWatchFaceName = null;

    class C02491 implements OnPageChangeListener {
        C02491() {
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (WatchFaceSettingsFragment.this.mPageScrollListener == null) {
                return;
            }
            if (positionOffsetPixels != 0) {
                if (WatchFaceSettingsFragment.this.isChangeBackground) {
                    int nextPos = position;
                    if (position == WatchFaceSettingsFragment.this.mCurItem) {
                        nextPos++;
                        if (nextPos > WatchFaceSettingsFragment.this.mAdapter.getCount() - 1) {
                            nextPos = WatchFaceSettingsFragment.this.mAdapter.getCount() - 1;
                        }
                        WatchFaceSettingsFragment.this.mPageScrollListener.onScrollDirectionChange(ScrollDirection.DOWN);
                    } else {
                        WatchFaceSettingsFragment.this.mPageScrollListener.onScrollDirectionChange(ScrollDirection.UP);
                    }
                    WatchFaceSettingsFragment.this.mPageScrollListener.onBackgroundChanged((Drawable) WatchFaceSettingsFragment.this.mPreviewsList.get(nextPos), true, WatchFaceSettingsFragment.this.mPosition);
                    WatchFaceSettingsFragment.this.isChangeBackground = false;
                }
                WatchFaceSettingsFragment.this.mPageScrollListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
                return;
            }
            WatchFaceSettingsFragment.this.mPageScrollListener.onBackgroundChanged((Drawable) WatchFaceSettingsFragment.this.mPreviewsList.get(position), false, WatchFaceSettingsFragment.this.mPosition);
        }

        public void onPageSelected(int position) {
            WatchFaceSettingsFragment.this.mCurItem = position;
        }

        public void onPageScrollStateChanged(int state) {
            if (WatchFaceSettingsFragment.this.mPageScrollListener != null) {
                if (state == 0) {
                    WatchFaceSettingsFragment.this.mPageScrollListener.onBackgroundChanged((Drawable) WatchFaceSettingsFragment.this.mPreviewsList.get(WatchFaceSettingsFragment.this.mPager.getCurrentItem()), false, WatchFaceSettingsFragment.this.mPosition);
                } else if (state == 1) {
                    WatchFaceSettingsFragment.this.mCurItem = WatchFaceSettingsFragment.this.mPager.getCurrentItem();
                    WatchFaceSettingsFragment.this.isChangeBackground = true;
                } else {
                    WatchFaceSettingsFragment.this.isChangeBackground = false;
                }
                WatchFaceSettingsFragment.this.mPageScrollListener.onPageScrollStateChanged(state);
                WatchFaceSettingsFragment.this.forceCanScrollHorizontally();
            }
        }
    }

    private class WatchFaceFragmentAdapter extends PagerAdapter {
        final int mTouchSlop;

        class C02501 implements OnClickListener {
            C02501() {
            }

            public void onClick(View v) {
                Intent intent = new Intent(WatchFaceSettingsFragment.this.getActivity(), WatchFaceCustomActivity.class);
                intent.putExtra("huami.watchface.servicename", WatchFaceSettingsFragment.this.mWatchFaceName);
                intent.putExtra("huami.watchface.customtype", WatchFaceSettingsFragment.this.mCustomType);
                intent.putExtra("watch_face_res_type", WatchFaceSettingsFragment.this.mResType);
                WatchFaceSettingsFragment.this.getActivity().startActivityForResult(intent, 256);
            }
        }

        private WatchFaceFragmentAdapter() {
            this.mTouchSlop = ViewConfiguration.get(WatchFaceSettingsFragment.this.getActivity()).getScaledTouchSlop();
        }

        public int getCount() {
            return WatchFaceSettingsFragment.this.mPreviewsList.size();
        }

        public boolean isViewFromObject(View view, Object object) {
            return view.getTag() == object;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            final View view = LayoutInflater.from(WatchFaceSettingsFragment.this.getActivity()).inflate(R.layout.watch_face_custom_item, null);
            view.setTag(Integer.valueOf(position));
            if (WatchFaceSettingsFragment.this.mCustomType == 0 && position == getCount() - 1 && WatchFaceSettingsFragment.SHOWN_CUSTOM_USER) {
                view.setOnClickListener(new C02501());
                view.setOnTouchListener(new OnTouchListener() {
                    float downX;
                    float downY;

                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case 0:
                                this.downX = event.getX();
                                this.downY = event.getY();
                                break;
                            case 2:
                                if (Math.abs(event.getY() - this.downY) <= ((float) WatchFaceFragmentAdapter.this.mTouchSlop)) {
                                    if (Math.abs(event.getX() - this.downX) > ((float) WatchFaceFragmentAdapter.this.mTouchSlop)) {
                                        view.cancelLongPress();
                                        view.setPressed(false);
                                        break;
                                    }
                                }
                                view.cancelLongPress();
                                view.setPressed(false);
                                break;
                                break;
                        }
                        return false;
                    }
                });
            }
            container.addView(view);
            return Integer.valueOf(position);
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = container.findViewWithTag(Integer.valueOf(position));
            view.setTag(null);
            container.removeView(view);
        }

        public int getItemPosition(Object object) {
            return -2;
        }
    }

    public static WatchFaceSettingsFragment getCorrespondingFragment(ConfigInfo info, String watchFaceName, int position, int resType) {
        WatchFaceSettingsFragment fragment = new WatchFaceSettingsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("custom_info", info);
        bundle.putString("watch_name", watchFaceName);
        bundle.putInt("watch_position", position);
        bundle.putInt("watch_face_res_type", resType);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (WatchFaceSettingsActivity) activity;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            this.mInfo = (ConfigInfo) bundle.getParcelable("custom_info");
            this.mPosition = bundle.getInt("watch_position");
            this.mWatchFaceName = bundle.getString("watch_name");
            this.mResType = bundle.getInt("watch_face_res_type");
            this.mCustomType = this.mInfo.getConfigType();
        }
        if (this.mWatchFaceName == null) {
            throw new IllegalArgumentException("err custom watch face name");
        }
        View rootView = inflater.inflate(R.layout.watch_face_fragment_layout, container, false);
        if (this.mCustomType == 0) {
            this.mCustomSelectedItem = new CustomBackgroundSelectedItem(getActivity(), this.mWatchFaceName, this.mResType);
        } else if (this.mCustomType == 1) {
            this.mCustomSelectedItem = new CustomTimeHanderSelectedItem(getActivity(), this.mWatchFaceName, this.mResType);
        } else if (this.mCustomType == 2) {
            this.mCustomSelectedItem = new CustomGradutionSelectedItem(getActivity(), this.mWatchFaceName, this.mResType);
        } else if (this.mCustomType == 3) {
            this.mCustomSelectedItem = new CustomDataWidgetSelectedItem(getActivity(), this.mWatchFaceName, (DataWidgetPositionConfig) this.mActivity.getTemplate().getDataWidgetPositionConfigs().get(this.mInfo.position), this.mResType);
        } else if (this.mCustomType == 4) {
            this.mCustomSelectedItem = new CustomTimeDigitalSelectedItem(getActivity(), this.mWatchFaceName, this.mResType);
        } else {
            throw new IllegalArgumentException("err custom type");
        }
        this.mCustomSelectedItem.setTemplate(this.mActivity.getTemplate());
        rootView.findViewById(R.id.hide_img).setBackground(this.mCustomSelectedItem.getBgDrawable());
        rootView.findViewById(R.id.selected_img).setBackground(this.mCustomSelectedItem.getFgDrawable());
        this.mPager = (VerticalViewPager) rootView.findViewById(R.id.watch_face_list);
        ArcScrollbarHelper.setArcScrollBarDrawable(this.mPager);
        initPreviewList();
        init();
        return rootView;
    }

    public int getItemType() {
        return this.mCustomType;
    }

    private void initPreviewList() {
        if (this.mPreviewsList == null) {
            this.mPreviewsList = new LinkedList();
        }
        this.mPreviewsList.clear();
        List<? extends BasePreviewConfig> list = this.mCustomSelectedItem.getBackground();
        if (list != null) {
            for (BasePreviewConfig config : list) {
                this.mPreviewsList.add(config.getPreviewDrawable());
            }
        }
        if (this.mCustomType == 0) {
            this.mPreviewsList.add(getActivity().getDrawable(R.drawable.watchface_custom_switch_upload_bg));
        }
    }

    private void init() {
        this.mAdapter = new WatchFaceFragmentAdapter();
        this.mPager.setAdapter(this.mAdapter);
        this.mPager.addOnPageChangeListener(new C02491());
        this.mPager.setCurrentItem(this.mCustomSelectedItem.getSavedIndex());
    }

    private void forceCanScrollHorizontally() {
        if (this.mCustomType == 0 && this.mCurItem == this.mAdapter.getCount() - 1) {
            this.mPager.setForceCanScrollHorizontally(true);
        } else {
            this.mPager.setForceCanScrollHorizontally(false);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void refreshUI(boolean isReLocate) {
        initPreviewList();
        Log.i(TAG, "list size: " + this.mPreviewsList.size());
        if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
        }
        if (isReLocate) {
            int item = this.mAdapter.getCount() >= 2 ? this.mAdapter.getCount() - 2 : this.mPager.getCurrentItem();
            this.mPager.setCurrentItem(item);
            this.mCurItem = item;
        } else {
            this.mCurItem = this.mPreviewsList.size() - 1;
        }
        forceCanScrollHorizontally();
    }

    public void setPageScrollListener(ICustomChangeListener pageScrollListener) {
        this.mPageScrollListener = pageScrollListener;
    }

    public int getCount() {
        if (this.mPreviewsList != null) {
            return this.mPreviewsList.size();
        }
        return 0;
    }

    public int getCurrentItem() {
        if (this.mPager != null) {
            return this.mPager.getCurrentItem();
        }
        return 0;
    }

    public ConfigInfo getCurConfig() {
        return this.mInfo;
    }

    public Drawable getCurDrawable() {
        return (Drawable) this.mPreviewsList.get(this.mCurItem);
    }

    public String getTitle() {
        List<? extends BasePreviewConfig> list = this.mCustomSelectedItem.getBackground();
        if (list == null || this.mCurItem >= list.size()) {
            return getActivity().getString(R.string.custom_title);
        }
        return ((BasePreviewConfig) list.get(this.mCurItem)).getTitle();
    }

    public void saveSelectedItem() {
        if (this.mCustomSelectedItem != null) {
            if (this.mCustomType == 0 && this.mCurItem == this.mPreviewsList.size() - 1) {
                this.mCurItem = 0;
            }
            this.mCustomSelectedItem.saveSelectedItem(this.mCurItem);
        }
    }
}
