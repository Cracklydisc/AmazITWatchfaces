package com.huami.watch.watchface.loader;

import com.huami.watch.watchface.model.WatchFaceModule;
import com.huami.watch.watchface.model.WatchFaceModuleItem;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public abstract class WatchFaceExternalLoader {
    public abstract void close();

    public abstract boolean exists(String str);

    public abstract WatchFaceModule parseWatchFace();

    public abstract List<WatchFaceModuleItem> parseWatchFaceConfigList(String str);

    public abstract WatchFaceZipInfo parseWatchFaceZipInfo();

    public abstract byte[] readFile(String str);

    public abstract InputStream readFileInputStream(String str);

    public static WatchFaceExternalLoader getWatchFaceExternalLoader(String path) {
        if (path == null || path.length() <= 0) {
            return null;
        }
        return getWatchFaceExternalLoader(new File(path));
    }

    public static WatchFaceExternalLoader getWatchFaceExternalLoader(File f) {
        if (f.isFile() && f.getName().endsWith(".wfz")) {
            try {
                return new WatchFaceZipLoader(f);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static final boolean closeInputStream(InputStream in) {
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }

    public static final File findWatchFaceFile(String resName) {
        if (resName != null && resName.endsWith(".wfz")) {
            File pathExternal = new File("/sdcard/WatchFace/" + resName);
            if (pathExternal.isFile()) {
                return pathExternal;
            }
            File pathInternal = new File("/system/watch_face/" + resName);
            if (pathInternal.isFile()) {
                return pathInternal;
            }
        }
        return null;
    }
}
