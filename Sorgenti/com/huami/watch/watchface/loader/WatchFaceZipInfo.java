package com.huami.watch.watchface.loader;

import android.graphics.drawable.Drawable;

public class WatchFaceZipInfo {
    public CharSequence label;
    public Drawable preview;
    public String previewPath;
    public String resName;
    public boolean settings;

    public String toString() {
        return "WatchFaceZipInfo [ label " + this.label + "\nresName: " + this.resName + "\nsettings: " + this.settings + " ]";
    }
}
