package com.huami.watch.watchface.loader;

import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import com.huami.watch.watchface.model.WatchFaceInfoModule;
import com.huami.watch.watchface.model.WatchFaceModule;
import com.huami.watch.watchface.model.WatchFaceModuleItem;
import com.huami.watch.watchface.utils.WatchFaceXmlSaxParser;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class WatchFaceZipLoader extends WatchFaceExternalLoader {
    private File mFile;
    private ZipFile mZipFile;

    protected WatchFaceZipLoader(File f) {
        this.mFile = f;
        try {
            this.mZipFile = new ZipFile(f, 1);
        } catch (IOException e) {
            throw new IllegalArgumentException("can not open the file " + f.getPath() + " message: " + e.getMessage());
        }
    }

    public void close() {
        if (this.mZipFile != null) {
            try {
                this.mZipFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public WatchFaceZipInfo parseWatchFaceZipInfo() {
        ZipEntry descriptionEntry = this.mZipFile.getEntry("description.xml");
        if (descriptionEntry != null) {
            try {
                WatchFaceZipInfo info = new WatchFaceZipInfo();
                InputStream isDescription = this.mZipFile.getInputStream(descriptionEntry);
                WatchFaceInfoModule infoModule = WatchFaceXmlSaxParser.parseWatchFaceInfo(isDescription);
                WatchFaceExternalLoader.closeInputStream(isDescription);
                if (infoModule == null) {
                    return null;
                }
                if (!WfzConstants.isSupportVersion(infoModule.getVersion())) {
                    return null;
                }
                if (TextUtils.isEmpty(infoModule.getTitle())) {
                    int splt = this.mFile.getName().lastIndexOf(".wfz");
                    if (splt > 0) {
                        info.label = this.mFile.getName().substring(0, splt);
                    } else {
                        info.label = this.mFile.getName();
                    }
                } else {
                    info.label = infoModule.getTitle();
                }
                info.previewPath = infoModule.getPreview();
                ZipEntry previewEntry = this.mZipFile.getEntry(infoModule.getPreview());
                if (previewEntry != null) {
                    InputStream isPreview = this.mZipFile.getInputStream(previewEntry);
                    BitmapDrawable preview = new BitmapDrawable(isPreview);
                    WatchFaceExternalLoader.closeInputStream(isPreview);
                    info.preview = preview;
                }
                info.resName = this.mFile.getName();
                info.settings = infoModule.getSettings();
                return info;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public WatchFaceModule parseWatchFace() {
        ZipEntry watchfaceEntry = this.mZipFile.getEntry("watchface.xml");
        if (watchfaceEntry != null) {
            try {
                InputStream is = this.mZipFile.getInputStream(watchfaceEntry);
                WatchFaceModule module = WatchFaceXmlSaxParser.parseWatchFace(is);
                WatchFaceExternalLoader.closeInputStream(is);
                return module;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<WatchFaceModuleItem> parseWatchFaceConfigList(String configFile) {
        ZipEntry watchfaceEntry = this.mZipFile.getEntry(configFile);
        if (watchfaceEntry != null) {
            try {
                InputStream is = this.mZipFile.getInputStream(watchfaceEntry);
                WatchFaceModule module = WatchFaceXmlSaxParser.parseWatchFace(is);
                WatchFaceExternalLoader.closeInputStream(is);
                if (module != null) {
                    return module.getWatchFaceItemList();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public boolean exists(String path) {
        if (this.mZipFile == null || this.mZipFile.getEntry(path) == null) {
            return false;
        }
        return true;
    }

    public byte[] readFile(String path) {
        InputStream in = null;
        int len = -1;
        if (this.mZipFile != null) {
            ZipEntry entry = this.mZipFile.getEntry(path);
            if (entry != null) {
                len = (int) entry.getSize();
                try {
                    in = this.mZipFile.getInputStream(entry);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (in == null) {
            return null;
        }
        if (len <= 0) {
            WatchFaceExternalLoader.closeInputStream(in);
            return null;
        }
        byte[] buf = new byte[len];
        try {
            in.read(buf, 0, buf.length);
        } catch (IOException e2) {
            buf = null;
        }
        WatchFaceExternalLoader.closeInputStream(in);
        return buf;
    }

    public InputStream readFileInputStream(String path) {
        if (this.mZipFile != null) {
            ZipEntry entry = this.mZipFile.getEntry(path);
            if (entry != null) {
                InputStream is = null;
                try {
                    return this.mZipFile.getInputStream(entry);
                } catch (IOException e) {
                    e.printStackTrace();
                    return is;
                }
            }
        }
        return null;
    }
}
