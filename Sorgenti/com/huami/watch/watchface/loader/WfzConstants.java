package com.huami.watch.watchface.loader;

import java.util.HashMap;

public final class WfzConstants {
    private static final HashMap<String, Integer> sItemTypeMap = new HashMap(5);

    static {
        sItemTypeMap.put("background", Integer.valueOf(0));
        sItemTypeMap.put("graduation", Integer.valueOf(1));
        sItemTypeMap.put("timehand", Integer.valueOf(2));
        sItemTypeMap.put("datawidget", Integer.valueOf(3));
        sItemTypeMap.put("timedigital", Integer.valueOf(4));
        sItemTypeMap.put("statusbar", Integer.valueOf(5));
        sItemTypeMap.put("support26w", Integer.valueOf(6));
        sItemTypeMap.put("lowpower", Integer.valueOf(7));
    }

    public static final int parseItemType(String itemType) {
        Integer type = (Integer) sItemTypeMap.get(itemType);
        if (type != null) {
            return type.intValue();
        }
        return -1;
    }

    private static int compareVersion(String version1, String version2) throws Exception {
        if (version1 == null || version2 == null) {
            throw new Exception("compareVersion error:illegal params.");
        }
        String[] versionArray1 = version1.split("\\.");
        String[] versionArray2 = version2.split("\\.");
        int minLength = Math.min(versionArray1.length, versionArray2.length);
        int diff = 0;
        for (int idx = 0; idx < minLength; idx++) {
            diff = versionArray1[idx].length() - versionArray2[idx].length();
            if (diff != 0) {
                break;
            }
            diff = versionArray1[idx].compareTo(versionArray2[idx]);
            if (diff != 0) {
                break;
            }
        }
        if (diff != 0) {
            return diff;
        }
        return versionArray1.length - versionArray2.length;
    }

    public static boolean isSupportVersion(String version) {
        try {
            return compareVersion(version, "1.4") <= 0;
        } catch (Exception e) {
            return false;
        }
    }
}
