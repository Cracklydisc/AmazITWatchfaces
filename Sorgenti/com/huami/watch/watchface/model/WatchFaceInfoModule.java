package com.huami.watch.watchface.model;

import android.support.v7.recyclerview.C0051R;
import java.util.Hashtable;

public class WatchFaceInfoModule {
    private static Hashtable<String, Integer> sKeyMap = new Hashtable();
    private String author;
    private String description;
    private String designer;
    private String id;
    private String preview;
    private boolean settings;
    private String title;
    private String version;

    public String getVersion() {
        return this.version;
    }

    public String getTitle() {
        return this.title;
    }

    public String getPreview() {
        return this.preview;
    }

    public boolean getSettings() {
        return this.settings;
    }

    public String toString() {
        return "WatchFaceInfoModule{version='" + this.version + '\'' + ", id='" + this.id + '\'' + ", author='" + this.author + '\'' + ", designer='" + this.designer + '\'' + ", title='" + this.title + '\'' + ", preview='" + this.preview + '\'' + ", settings='" + this.settings + '\'' + ", description='" + this.description + '\'' + '}';
    }

    static {
        sKeyMap.put("version", Integer.valueOf(1));
        sKeyMap.put("id", Integer.valueOf(2));
        sKeyMap.put("author", Integer.valueOf(3));
        sKeyMap.put("designer", Integer.valueOf(4));
        sKeyMap.put("title", Integer.valueOf(5));
        sKeyMap.put("preview", Integer.valueOf(6));
        sKeyMap.put("settings", Integer.valueOf(7));
        sKeyMap.put("description", Integer.valueOf(8));
    }

    public void setValue(String key, String value) {
        Integer index = (Integer) sKeyMap.get(key);
        if (index != null) {
            switch (index.intValue()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    this.version = value;
                    return;
                case 2:
                    this.id = value;
                    return;
                case 3:
                    this.author = value;
                    return;
                case 4:
                    this.designer = value;
                    return;
                case 5:
                    this.title = value;
                    return;
                case 6:
                    this.preview = value;
                    return;
                case 7:
                    this.settings = Boolean.parseBoolean(value);
                    return;
                case 8:
                    this.description = value;
                    return;
                default:
                    return;
            }
        }
    }
}
