package com.huami.watch.watchface.model;

import android.support.v7.recyclerview.C0051R;
import java.util.Hashtable;
import java.util.List;

public class WatchFaceModule {
    private static Hashtable<String, Integer> sKeyMap = new Hashtable();
    private String version;
    private List<WatchFaceModuleItem> watchFaceItemList;

    public List<WatchFaceModuleItem> getWatchFaceItemList() {
        return this.watchFaceItemList;
    }

    public void setWatchFaceItemList(List<WatchFaceModuleItem> watchFaceItemList) {
        this.watchFaceItemList = watchFaceItemList;
    }

    public String toString() {
        return "WatchFaceModule{, version='" + this.version + '\'' + ", watchFaceItemList=" + this.watchFaceItemList + '}';
    }

    static {
        sKeyMap.put("version", Integer.valueOf(1));
    }

    public void setValue(String key, String value) {
        Integer index = (Integer) sKeyMap.get(key);
        if (index != null) {
            switch (index.intValue()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    this.version = value;
                    return;
                default:
                    return;
            }
        }
    }
}
