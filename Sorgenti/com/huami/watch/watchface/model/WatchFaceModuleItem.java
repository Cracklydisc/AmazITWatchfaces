package com.huami.watch.watchface.model;

import android.support.v7.recyclerview.C0051R;
import java.util.Hashtable;

public class WatchFaceModuleItem {
    private static Hashtable<String, Integer> sKeyMap = new Hashtable();
    private String charset;
    private String color;
    private String config;
    private String configlist;
    private int dataType;
    private String font;
    private int gap;
    private int height;
    private int id = -1;
    private String mask;
    private int model;
    private String preview;
    private String type;
    private int width;
    private int f13x = Integer.MIN_VALUE;
    private int f14y = Integer.MIN_VALUE;

    public String getType() {
        return this.type;
    }

    public String getConfig() {
        return this.config;
    }

    public String getConfiglist() {
        return this.configlist;
    }

    public String getPreview() {
        return this.preview;
    }

    public String getMask() {
        return this.mask;
    }

    public int getId() {
        return this.id;
    }

    public int getX() {
        return this.f13x;
    }

    public int getY() {
        return this.f14y;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public int getDataType() {
        return this.dataType;
    }

    public int getModel() {
        return this.model;
    }

    public String getFontConfig() {
        return this.font;
    }

    public int getGap() {
        return this.gap;
    }

    public String getColor() {
        return this.color;
    }

    public String getCharset() {
        return this.charset;
    }

    public String toString() {
        return "WatchFaceModuleItem{type='" + this.type + '\'' + ", config='" + this.config + '\'' + ", config_list='" + this.configlist + '\'' + ", preview='" + this.preview + '\'' + ", mask='" + this.mask + '\'' + ", x=" + this.f13x + ", y=" + this.f14y + ", width=" + this.width + ", height=" + this.height + ", dataType=" + this.dataType + ", model=" + this.model + '}';
    }

    static {
        sKeyMap.put("type", Integer.valueOf(1));
        sKeyMap.put("config", Integer.valueOf(2));
        sKeyMap.put("configList", Integer.valueOf(3));
        sKeyMap.put("preview", Integer.valueOf(4));
        sKeyMap.put("mask", Integer.valueOf(5));
        sKeyMap.put("id", Integer.valueOf(6));
        sKeyMap.put("x", Integer.valueOf(7));
        sKeyMap.put("y", Integer.valueOf(8));
        sKeyMap.put("width", Integer.valueOf(9));
        sKeyMap.put("height", Integer.valueOf(10));
        sKeyMap.put("dataType", Integer.valueOf(11));
        sKeyMap.put("model", Integer.valueOf(12));
        sKeyMap.put("font", Integer.valueOf(13));
        sKeyMap.put("gap", Integer.valueOf(14));
        sKeyMap.put("color", Integer.valueOf(15));
        sKeyMap.put("charset", Integer.valueOf(16));
    }

    public void setValue(String key, String value) {
        Integer index = (Integer) sKeyMap.get(key);
        if (index != null) {
            switch (index.intValue()) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    this.type = value;
                    return;
                case 2:
                    this.config = value;
                    return;
                case 3:
                    this.configlist = value;
                    return;
                case 4:
                    this.preview = value;
                    return;
                case 5:
                    this.mask = value;
                    return;
                case 6:
                    this.id = Integer.parseInt(value);
                    return;
                case 7:
                    this.f13x = Integer.parseInt(value);
                    return;
                case 8:
                    this.f14y = Integer.parseInt(value);
                    return;
                case 9:
                    this.width = Integer.parseInt(value);
                    return;
                case 10:
                    this.height = Integer.parseInt(value);
                    return;
                case 11:
                    this.dataType = Integer.parseInt(value);
                    return;
                case 12:
                    this.model = Integer.parseInt(value);
                    return;
                case 13:
                    this.font = value;
                    return;
                case 14:
                    this.gap = Integer.parseInt(value);
                    return;
                case 15:
                    this.color = value;
                    return;
                case 16:
                    this.charset = value;
                    return;
                default:
                    return;
            }
        }
    }
}
