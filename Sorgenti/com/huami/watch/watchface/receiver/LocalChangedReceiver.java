package com.huami.watch.watchface.receiver;

import android.app.WallpaperInfo;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import com.huami.watch.watchface.loader.WatchFaceExternalLoader;
import com.huami.watch.watchface.loader.WatchFaceZipInfo;
import com.huami.watch.watchface.util.Util;
import com.huami.watch.watchface.util.WatchFaceSave;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;

public class LocalChangedReceiver extends BroadcastReceiver {
    private static final String PATH_CUSTOM_BG_TMP2 = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/.watchface/tmp/customBg/");

    private class CustomBgTask extends AsyncTask<String, Void, Boolean> {
        private Context mContext;

        class C02531 implements FilenameFilter {
            C02531() {
            }

            public boolean accept(File dir, String filename) {
                if (filename == null || !filename.endsWith(".wfz")) {
                    return false;
                }
                return true;
            }
        }

        private CustomBgTask(Context context) {
            this.mContext = context;
        }

        protected Boolean doInBackground(String... paths) {
            if (paths != null && paths.length > 0) {
                String srcpath = paths[0];
                if (srcpath == null) {
                    return Boolean.valueOf(false);
                }
                Bitmap srcBg = BitmapFactory.decodeFile(srcpath);
                if (srcBg == null) {
                    return Boolean.valueOf(false);
                }
                int width = srcBg.getWidth();
                int height = srcBg.getHeight();
                Log.d("CustomBg", "  CustomBgTask size: [" + width + " x " + height + "]. " + srcBg.getConfig());
                Bitmap srcClipSlptBg = Bitmap.createBitmap(width, height, Config.ARGB_8888);
                Canvas canvas = new Canvas(srcClipSlptBg);
                Paint paint = new Paint(6);
                Path clipPath = new Path();
                clipPath.addCircle(160.0f, 160.0f, 160.0f, Direction.CCW);
                canvas.clipPath(clipPath);
                canvas.drawBitmap(srcBg, 0.0f, 0.0f, paint);
                canvas.setBitmap(null);
                Bitmap dstSlptBg = Bitmap.createBitmap(width, height, Config.ARGB_8888);
                Util.ditherBitmap(srcClipSlptBg, dstSlptBg, 0.7f);
                Log.d("CustomBg", "  Generate slpt bg success. Saving bg...");
                File file = new File(srcpath);
                String srcName = file.getName();
                int prefixStart = srcName.lastIndexOf(".");
                if (prefixStart > 0) {
                    srcName = srcName.substring(0, prefixStart);
                }
                String dstName = srcName + ".png";
                LocalChangedReceiver.this.saveBitmap(srcClipSlptBg, "/sdcard/.watchface/customBg/", dstName);
                LocalChangedReceiver.this.saveBitmap(dstSlptBg, "/sdcard/.watchface/customSlptBg/", dstName);
                file.delete();
                srcBg.recycle();
                srcClipSlptBg.recycle();
                dstSlptBg.recycle();
                Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                intent.setData(Uri.fromFile(new File("/sdcard/.watchface/customBg/" + dstName)));
                this.mContext.sendBroadcast(intent);
                intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                intent.setData(Uri.fromFile(new File("/sdcard/.watchface/customSlptBg/" + dstName)));
                this.mContext.sendBroadcast(intent);
                Log.d("CustomBg", "  Save bg " + dstName + " done.");
                WallpaperInfo current = WallpaperManager.getInstance(this.mContext).getWallpaperInfo();
                if (current != null) {
                    String INTERNAL_PATH = this.mContext.getFilesDir().getAbsolutePath();
                    String INTERNAL_BG_PATH_CUSTOM = "/watchface/customBg/";
                    String INTERNAL_BG_PATH_CUSTOM_SLPT = "/watchface/customSlptBg/";
                    ComponentName currentCmpName = current.getComponent();
                    if (currentCmpName == null) {
                        return Boolean.valueOf(false);
                    }
                    String KEY_BG = "Background";
                    String PREFIX_IMG_CUSTOM = "@customBg/";
                    boolean saved = false;
                    boolean success;
                    if ("com.huami.watch.watchface.ExternalWatchFace".equals(currentCmpName.getClassName())) {
                        File currentPath = new File("/sdcard/.watchface/current/");
                        if (currentPath.isDirectory()) {
                            File[] wfzs = currentPath.listFiles(new C02531());
                            if (wfzs != null && wfzs.length > 0) {
                                WatchFaceExternalLoader loader = WatchFaceExternalLoader.getWatchFaceExternalLoader(wfzs[0]);
                                if (loader != null) {
                                    WatchFaceZipInfo infoTemp = loader.parseWatchFaceZipInfo();
                                    if (infoTemp != null && infoTemp.settings) {
                                        Log.d("CustomBg", "  Current WatchFace support custom background.");
                                        success = Util.copyFile(new StringBuilder().append("/sdcard/.watchface/customBg/").append(dstName).toString(), new StringBuilder().append(INTERNAL_PATH).append("/watchface/customBg/").append(dstName).toString()) && Util.copyFile("/sdcard/.watchface/customSlptBg/" + dstName, INTERNAL_PATH + "/watchface/customSlptBg/" + dstName);
                                        if (success) {
                                            Log.d("CustomBg", "    copy the bg resources to internal path done.");
                                            String wfzPath = wfzs[0].getPath();
                                            Log.d("CustomBg", "    update current watchface saved config: " + wfzPath);
                                            WatchFaceSave preferences = WatchFaceSave.getWatchFaceSave(wfzPath);
                                            preferences.putString("Background", "@customBg/" + dstName);
                                            saved = preferences.commit();
                                        } else {
                                            Log.d("CustomBg", "    copy the bg resources to internal path failure.");
                                            return Boolean.valueOf(false);
                                        }
                                    }
                                }
                            }
                        }
                    } else if (current.getSettingsActivity() != null) {
                        Log.d("CustomBg", "  Current WatchFace support custom background.");
                        success = Util.copyFile(new StringBuilder().append("/sdcard/.watchface/customBg/").append(dstName).toString(), new StringBuilder().append(INTERNAL_PATH).append("/watchface/customBg/").append(dstName).toString()) && Util.copyFile("/sdcard/.watchface/customSlptBg/" + dstName, INTERNAL_PATH + "/watchface/customSlptBg/" + dstName);
                        if (success) {
                            Log.d("CustomBg", "    copy the bg resources to internal path done.");
                            saved = this.mContext.getSharedPreferences(currentCmpName.getClassName(), 0).edit().putString("Background", "@customBg/" + dstName).commit();
                        } else {
                            Log.d("CustomBg", "    copy the bg resources to internal path failure.");
                            return Boolean.valueOf(false);
                        }
                    }
                    return Boolean.valueOf(saved);
                }
            }
            return Boolean.valueOf(false);
        }

        protected void onPostExecute(Boolean saved) {
            if (saved.booleanValue()) {
                Log.d("CustomBg", "    update saved done.");
                LocalChangedReceiver.restartWatchFace(this.mContext);
                return;
            }
            Log.d("CustomBg", "    update saved failure.");
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.LOCALE_CHANGED")) {
            Log.d("LocalChangedReceiver", "LOCALE_CHANGED");
            restartWatchFace(context);
        } else if ("com.huami.intent.action.FILE_ARRIVED".equals(intent.getAction())) {
            String path = intent.getStringExtra("com.huami.intent.extra.FILE_ARRIVED_PATH");
            if (path == null) {
                return;
            }
            if (path.startsWith("/sdcard/.watchface/tmp/customBg/") || path.startsWith(PATH_CUSTOM_BG_TMP2)) {
                Log.d("CustomBg", "Receive custom bg: " + path);
                ((PowerManager) context.getSystemService("power")).newWakeLock(6, "CustomBg").acquire(2000);
                new CustomBgTask(context).execute(new String[]{path});
            }
        } else if ("com.huami.intent.action.WATCHFACE_CONFIG_CHANGED".equals(intent.getAction())) {
            Log.d("WatchFaceConfig", "Config changed.");
            restartWatchFace(context);
        }
    }

    private static void restartWatchFace(Context context) {
        ((PowerManager) context.getSystemService("power")).wakeUp(SystemClock.uptimeMillis());
        WallpaperManager mWallpaperManager = WallpaperManager.getInstance(context);
        WallpaperInfo currentInfo = mWallpaperManager.getWallpaperInfo();
        if (currentInfo != null) {
            ComponentName componentName = currentInfo.getComponent();
            try {
                mWallpaperManager.getIWallpaperManager().setWallpaperComponent(null);
                Log.d("LocalChangedReceiver", " Reset current watchface " + componentName);
                mWallpaperManager.getIWallpaperManager().setWallpaperComponent(componentName);
            } catch (RemoteException e) {
                Log.e("LocalChangedReceiver", "Restart watchface failure. " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void saveBitmap(Bitmap src, String path, String fileName) {
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File f = new File(path, fileName);
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            src.compress(CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
