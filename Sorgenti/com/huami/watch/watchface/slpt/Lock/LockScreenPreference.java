package com.huami.watch.watchface.slpt.Lock;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class LockScreenPreference {
    public static void setTargetStept(Context mContext, int value) {
        mContext.getSharedPreferences("sport_value", 0).edit().putInt("target_step", value).commit();
    }

    public static int getTargetStept(Context mContext) {
        return mContext.getSharedPreferences("sport_value", 0).getInt("target_step", 8000);
    }

    public static void setTodaySportDistance(Context mContext, float value) {
        SharedPreferences pref = mContext.getSharedPreferences("sport_value", 0);
        Editor editor = pref.edit();
        editor.putFloat("current_sport_distance", value).commit();
        pref.getFloat("today_sport_distance", 0.0f);
        editor.putFloat("today_sport_distance", 0.0f + value).commit();
    }

    public static float getTodaySportDistance(Context mContext) {
        return mContext.getSharedPreferences("sport_value", 0).getFloat("today_sport_distance", 0.0f);
    }

    public static void setTotalSportDistance(Context mContext, float value) {
        mContext.getSharedPreferences("sport_value", 0).edit().putFloat("total_sport_distance", value).commit();
    }

    public static void setCurrentHeartrate(Context mContext, int value) {
        mContext.getSharedPreferences("sport_value", 0).edit().putFloat("current_heartrate", (float) value).commit();
    }

    public static float getTotalSportDistance(Context mContext) {
        return mContext.getSharedPreferences("sport_value", 0).getFloat("total_sport_distance", 0.0f);
    }
}
