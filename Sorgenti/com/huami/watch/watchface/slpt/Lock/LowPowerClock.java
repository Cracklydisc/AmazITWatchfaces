package com.huami.watch.watchface.slpt.Lock;

import android.content.Context;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.sport.SlptPowerNumView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class LowPowerClock extends SlptClock {
    private SlptAbsoluteLayout rootLayout = new SlptAbsoluteLayout();

    private void initLayout(Context mContext) {
        SlptPictureView powerIcon = new SlptPictureView();
        powerIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, "guard/lowpower/watchface_icon_low_power.png"));
        powerIcon.setStart(138, 69);
        byte[][] num = new byte[10][];
        for (int i = 0; i < 10; i++) {
            num[i] = SimpleFile.readFileFromAssets(mContext, String.format("guard/lowpower/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        SlptPowerNumView numView = new SlptPowerNumView();
        numView.setImagePictureArray(num);
        SlptLinearLayout powerNumLayout = new SlptLinearLayout();
        SlptPictureView percentView = new SlptPictureView();
        percentView.setImagePicture(SimpleFile.readFileFromAssets(mContext, "guard/lowpower/percent.png"));
        powerNumLayout.add(numView);
        powerNumLayout.add(percentView);
        powerNumLayout.setStart(138, 69);
        powerNumLayout.setRect(44, 74);
        powerNumLayout.alignX = (byte) 2;
        powerNumLayout.alignY = (byte) 2;
        SlptLinearLayout stringLayout = (SlptLinearLayout) SportItemInfoWrapper.getFontStringView(mContext, mContext.getResources().getString(R.string.low_battery_please_charging), 16, -1, (short) 0);
        stringLayout.setStart(0, 207);
        stringLayout.setRect(320, 2147483646);
        stringLayout.alignX = (byte) 2;
        SlptLinearLayout stringLayout2 = (SlptLinearLayout) SportItemInfoWrapper.getFontStringView(mContext, mContext.getResources().getString(R.string.low_battery_protect), 21, -1, (short) 0);
        stringLayout2.setStart(0, 162);
        stringLayout2.setRect(320, 2147483646);
        stringLayout2.alignX = (byte) 2;
        this.rootLayout.background.color = -16777216;
        this.rootLayout.add(powerIcon);
        this.rootLayout.add(powerNumLayout);
        this.rootLayout.add(stringLayout);
        this.rootLayout.add(stringLayout2);
    }

    public LowPowerClock(Context mContext) {
        initLayout(mContext);
        setRootView(this.rootLayout);
    }
}
