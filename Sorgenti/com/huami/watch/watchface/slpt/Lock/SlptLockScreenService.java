package com.huami.watch.watchface.slpt.Lock;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.util.Log;
import com.huami.watch.companion.settings.WatchSettings;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.JSONObject;

public class SlptLockScreenService extends Service {
    public static final Uri CONTENT_HEART_URI = Uri.parse("content://com.huami.watch.health.heartdata");
    public static final Uri MEASUREMENT_UNIT = Uri.parse("content://settings/secure/measurement");
    public static final Uri STEP_URI = Uri.parse("content://com.huami.watch.companion.settings");
    public static final Uri TODAY_DISTANCE_URI = Uri.parse("content://settings/secure/sport_today_distance");
    public static final Uri TOTAL_DISTANCE_URI = Uri.parse("content://settings/secure/sport_total_distance");
    private static int currentHeartRate;
    private static int currentTargetStep;
    private static float currentTodayDistance;
    private static float currentTotalDistance;
    private static Context mContext;
    private static Handler mHandler;
    Callback callback = new C02541();
    private SlptClockClient mSlptClockClient;
    private ContentObserver mlastHeartrateOberver = new lastHeartrateObserver(new Handler());
    private ContentObserver mmeasurementObserver = new measurementObserver(new Handler());
    private ContentObserver mstepOberver = new stepObserver(new Handler());
    private ContentObserver mtodayDistanceOberver = new todayDistanceObserver(new Handler());
    private ContentObserver mtotalDistanceOberver = new totalDistanceObserver(new Handler());

    class C02541 implements Callback {
        C02541() {
        }

        public void onServiceDisconnected() {
        }

        public void onServiceConnected() {
            try {
                SlptLockScreenService.currentTargetStep = LockScreenPreference.getTargetStept(SlptLockScreenService.mContext);
                SlptLockScreenService.currentTotalDistance = Secure.getFloat(SlptLockScreenService.mContext.getContentResolver(), "sport_total_distance");
                SlptLockScreenService.currentTodayDistance = Secure.getFloat(SlptLockScreenService.mContext.getContentResolver(), "sport_today_distance");
                SlptLockScreenService.currentHeartRate = SlptLockScreenService.getHeartRato(SlptLockScreenService.mContext);
                Log.i("HmSlptLockScreen", "request todayDistance = " + SlptLockScreenService.currentTodayDistance + " currentTotalDistance = " + SlptLockScreenService.currentTotalDistance);
            } catch (SettingNotFoundException e) {
                e.printStackTrace();
                SlptLockScreenService.currentTodayDistance = 0.0f;
                SlptLockScreenService.currentTotalDistance = 0.0f;
                SlptLockScreenService.currentHeartRate = 0;
                Log.i("HmSlptLockScreen", "request sport value error get preference todayDistance = " + SlptLockScreenService.currentTodayDistance + " currentTotalDistance = " + SlptLockScreenService.currentTotalDistance);
            }
            int unit = Util.getMeasurement(SlptLockScreenService.mContext);
            if (SlptLockScreenService.currentTargetStep == 0) {
                SlptLockScreenService.this.getTargetStep(SlptLockScreenService.mContext);
            }
            if (Util.getSportfaceValue() != 1) {
                SlptLockScreenService.this.mSlptClockClient.setTargetSportStep(SlptLockScreenService.currentTargetStep);
                SlptLockScreenService.this.mSlptClockClient.setTodayDistance(Util.getDistance(unit, SlptLockScreenService.currentTodayDistance));
                SlptLockScreenService.this.mSlptClockClient.setTotalDistance(Util.getDistance(unit, SlptLockScreenService.currentTotalDistance));
                SlptLockScreenService.this.mSlptClockClient.setLastHeartrate(SlptLockScreenService.currentHeartRate);
                SlptLockScreenService.this.mSlptClockClient.enableSlpt();
            }
        }
    }

    public static class ClearSportReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Log.i("HmSlptLockScreen", "clear today distance to zero");
            if (SlptLockScreenService.mHandler == null) {
                Log.i("HmSlptLockScreen", "clear handler is null");
            } else {
                SlptLockScreenService.mHandler.obtainMessage().sendToTarget();
            }
        }
    }

    private final class lastHeartrateObserver extends ContentObserver {
        public lastHeartrateObserver(Handler handler) {
            super(handler);
        }

        public void onChange(boolean selfChange, Uri uri) {
            Log.d("HmSlptLockScreen", "total distance data has changed");
            int heartrate = SlptLockScreenService.getHeartRato(SlptLockScreenService.mContext);
            if (SlptLockScreenService.currentHeartRate != heartrate) {
                SlptLockScreenService.currentHeartRate = heartrate;
                SlptLockScreenService.this.setHeartrate(SlptLockScreenService.currentHeartRate);
            }
        }
    }

    private final class measurementObserver extends ContentObserver {
        public measurementObserver(Handler handler) {
            super(handler);
        }

        public void onChange(boolean selfChange, Uri uri) {
            Log.d("HmSlptLockScreen", "measure unit change to " + Util.getMeasurement(SlptLockScreenService.mContext));
            SlptLockScreenService.this.getDistanceValue(true);
        }
    }

    private final class stepObserver extends ContentObserver {
        private Runnable stepUpdate = new C02551();

        class C02551 implements Runnable {
            C02551() {
            }

            public void run() {
                SlptLockScreenService.this.getTargetStep(SlptLockScreenService.mContext);
            }
        }

        public stepObserver(Handler handler) {
            super(handler);
        }

        public void onChange(boolean selfChange, Uri uri) {
            Log.i("HmSlptLockScreen", "provider data has changed");
            SlptLockScreenService.mHandler.removeCallbacks(this.stepUpdate);
            SlptLockScreenService.mHandler.post(this.stepUpdate);
        }
    }

    private final class todayDistanceObserver extends ContentObserver {
        private Runnable DistanceUpdate = new C02561();

        class C02561 implements Runnable {
            C02561() {
            }

            public void run() {
                SlptLockScreenService.this.getDistanceValue(false);
            }
        }

        public todayDistanceObserver(Handler handler) {
            super(handler);
        }

        public void onChange(boolean selfChange, Uri uri) {
            Log.d("HmSlptLockScreen", "today distance data has changed");
            SlptLockScreenService.mHandler.removeCallbacks(this.DistanceUpdate);
            SlptLockScreenService.mHandler.post(this.DistanceUpdate);
        }
    }

    private final class totalDistanceObserver extends ContentObserver {
        private Runnable DistanceUpdate = new C02571();

        class C02571 implements Runnable {
            C02571() {
            }

            public void run() {
                SlptLockScreenService.this.getDistanceValue(false);
            }
        }

        public totalDistanceObserver(Handler handler) {
            super(handler);
        }

        public void onChange(boolean selfChange, Uri uri) {
            Log.d("HmSlptLockScreen", "total distance data has changed");
            SlptLockScreenService.mHandler.removeCallbacks(this.DistanceUpdate);
            SlptLockScreenService.mHandler.post(this.DistanceUpdate);
        }
    }

    private class updateSportHandler extends Handler {
        private updateSportHandler() {
        }

        public void handleMessage(Message msg) {
            SlptLockScreenService.createNewAlarm(SlptLockScreenService.mContext);
            SlptLockScreenService.this.setSportValue(0.0f, SlptLockScreenService.currentTotalDistance, true);
        }
    }

    public void onCreate() {
        Log.d("HmSlptLockScreen", "onCreate!");
        mContext = getApplicationContext();
        this.mSlptClockClient = new SlptClockClient();
        mHandler = new updateSportHandler();
        try {
            this.mSlptClockClient.bindService(this, "HmSlptLockScreen", this.callback);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("HmSlptLockScreen", "can't bind service something error");
        }
        getContentResolver().registerContentObserver(STEP_URI, true, this.mstepOberver);
        getContentResolver().registerContentObserver(TOTAL_DISTANCE_URI, true, this.mtotalDistanceOberver);
        getContentResolver().registerContentObserver(TODAY_DISTANCE_URI, true, this.mtodayDistanceOberver);
        getContentResolver().registerContentObserver(CONTENT_HEART_URI, true, this.mlastHeartrateOberver);
        getContentResolver().registerContentObserver(MEASUREMENT_UNIT, true, this.mmeasurementObserver);
        createNewAlarm(mContext);
    }

    public static int getHeartRato(Context c) {
        try {
            Cursor cur = c.getContentResolver().query(CONTENT_HEART_URI, null, null, null, "utc_time DESC LIMIT 1");
            if (cur != null) {
                while (cur.moveToNext()) {
                    int rato = cur.getInt(cur.getColumnIndex("heart_rate"));
                    if (rato < 300 && rato >= 0) {
                        cur.close();
                        return rato;
                    }
                }
            }
            cur.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setSportValue(float todayDistance, float totalDistance, boolean forceUpdate) {
        LockScreenPreference.setTotalSportDistance(mContext, currentTotalDistance);
        LockScreenPreference.setTodaySportDistance(mContext, currentTodayDistance);
        int unit = Util.getMeasurement(mContext);
        if (this.mSlptClockClient.serviceIsConnected()) {
            this.mSlptClockClient.setTargetSportStep(currentTargetStep);
            if (todayDistance != currentTodayDistance || forceUpdate) {
                this.mSlptClockClient.setTodayDistance(Util.getDistance(unit, todayDistance));
                currentTodayDistance = todayDistance;
                Log.i("HmSlptLockScreen", "update currentTodayDistance=" + currentTodayDistance);
            }
            if (totalDistance != currentTotalDistance || forceUpdate) {
                this.mSlptClockClient.setTotalDistance(Util.getDistance(unit, totalDistance));
                currentTotalDistance = totalDistance;
                Log.i("HmSlptLockScreen", "update currentTotalDistance=" + currentTotalDistance);
                return;
            }
            return;
        }
        Log.i("HmSlptLockScreen", "service disconnected , rebind service again");
        try {
            this.mSlptClockClient.bindService(this, "HmSlptLockScreen", this.callback);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("HmSlptLockScreen", "already bind servic!!!");
        }
    }

    public void setHeartrate(int heartrate) {
        LockScreenPreference.setCurrentHeartrate(mContext, currentHeartRate);
        if (this.mSlptClockClient.serviceIsConnected()) {
            this.mSlptClockClient.setLastHeartrate(currentHeartRate);
            Log.d("HmSlptLockScreen", "update currentHeartRate=" + currentHeartRate);
            return;
        }
        Log.i("HmSlptLockScreen", "setHeartrate service disconnected , rebind service again");
        try {
            this.mSlptClockClient.bindService(this, "HmSlptLockScreen", this.callback);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("HmSlptLockScreen", "already bind servic!!!");
        }
    }

    private void getTargetStep(Context mContext) {
        try {
            String configJson = WatchSettings.get(mContext.getContentResolver(), "huami.watch.health.config");
            if (configJson == null) {
                currentTargetStep = 8000;
                Log.i("HmSlptLockScreen", "init current step " + currentTargetStep);
                this.mSlptClockClient.setTargetSportStep(currentTargetStep);
                LockScreenPreference.setTargetStept(mContext, currentTargetStep);
                return;
            }
            int targetStep = new JSONObject(configJson).optInt("step_target", 80000);
            Log.i("HmSlptLockScreen", "get current target step " + currentTargetStep);
            if (this.mSlptClockClient.serviceIsConnected() && targetStep != currentTargetStep) {
                currentTargetStep = targetStep;
                Log.i("HmSlptLockScreen", "set current target step " + currentTargetStep);
                this.mSlptClockClient.setTargetSportStep(currentTargetStep);
                LockScreenPreference.setTargetStept(mContext, currentTargetStep);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getDistanceValue(boolean forceUpdate) {
        try {
            float tempTotalDistance = Secure.getFloat(mContext.getContentResolver(), "sport_total_distance");
            float tempTodayDistance = Secure.getFloat(mContext.getContentResolver(), "sport_today_distance");
            Log.i("HmSlptLockScreen", "get Distance value todayDistance = " + tempTotalDistance + " currentTotalDistance = " + tempTodayDistance);
            setSportValue(tempTodayDistance, tempTotalDistance, forceUpdate);
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
            currentTodayDistance = LockScreenPreference.getTodaySportDistance(mContext);
            currentTotalDistance = LockScreenPreference.getTotalSportDistance(mContext);
            Log.i("HmSlptLockScreen", "request sport value error get preference todayDistance = " + currentTodayDistance + " currentTotalDistance = " + currentTotalDistance);
        }
    }

    private static void createNewAlarm(Context mContext) {
        AlarmManager am = (AlarmManager) mContext.getSystemService("alarm");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(mContext, ClearSportReceiver.class), 0);
        Calendar mCalendar = Calendar.getInstance();
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        mCalendar.set(6, mCalendar.get(6) + 1);
        mCalendar.set(11, 0);
        mCalendar.set(12, 0);
        mCalendar.set(13, 0);
        SimpleDateFormat alarmTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        am.set(0, mCalendar.getTimeInMillis(), pendingIntent);
        Log.d("HmSlptLockScreen", "send to start update broadcase next time  :" + alarmTime.format(mCalendar.getTime()));
    }

    public void onDestroy() {
        Log.d("HmSlptLockScreen", "onDestroy!");
        try {
            this.mSlptClockClient.unbindService(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getContentResolver().unregisterContentObserver(this.mstepOberver);
        getContentResolver().unregisterContentObserver(this.mtotalDistanceOberver);
        getContentResolver().unregisterContentObserver(this.mtodayDistanceOberver);
        getContentResolver().unregisterContentObserver(this.mlastHeartrateOberver);
        getContentResolver().unregisterContentObserver(this.mmeasurementObserver);
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("HmSlptLockScreen", "onStartCommand!");
        return 1;
    }
}
