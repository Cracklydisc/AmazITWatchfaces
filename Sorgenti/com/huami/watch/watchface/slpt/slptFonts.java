package com.huami.watch.watchface.slpt;

import android.util.ArrayMap;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;

public class slptFonts {
    private ArrayMap<Character, byte[]> fontMap = new ArrayMap();

    public void setNumFonts(byte[][] num) {
        if (num.length == 10) {
            for (int i = 0; i < 10; i++) {
                this.fontMap.put(Character.valueOf(Character.forDigit(i, 10)), num[i]);
            }
        }
    }

    public void addChar(char tag, byte[] mem) {
        this.fontMap.put(Character.valueOf(tag), mem);
    }

    public SlptLayout getStringView(String st) {
        SlptLinearLayout layout = new SlptLinearLayout();
        for (int i = 0; i < st.length(); i++) {
            byte[] mem = (byte[]) this.fontMap.get(Character.valueOf(st.charAt(i)));
            if (mem != null) {
                SlptPictureView view = new SlptPictureView();
                view.setImagePicture(mem);
                layout.add(view);
            }
        }
        return layout;
    }
}
