package com.huami.watch.watchface.slpt.sport;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import com.huami.watch.watchface.slpt.sport.layout.CrossContryWatchScreen;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutClock;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutTheme;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import java.util.ArrayList;

public class CrossContryService extends Service {
    Callback callback = new C02602();
    private int clockPeriod;
    private boolean isMixSport;
    private Context mContext;
    private Object mLock = Util.mLock;
    private SlptClockClient mSlptClockClient = new SlptClockClient();
    private SportLayoutClock watchFace;

    class C02581 implements Runnable {
        C02581() {
        }

        public void run() {
            try {
                synchronized (CrossContryService.this.mLock) {
                    CrossContryService.this.watchFace = new CrossContryWatchScreen(CrossContryService.this.mContext, CrossContryService.this.isMixSport);
                    if (CrossContryService.this.mSlptClockClient.lockService()) {
                        CrossContryService.this.createSportClock();
                        CrossContryService.this.mSlptClockClient.unlockService();
                        return;
                    }
                    Log.i("slptCCSview", "lock service error!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C02602 implements Callback {

        class C02591 implements Runnable {
            C02591() {
            }

            public void run() {
                try {
                    synchronized (CrossContryService.this.mLock) {
                        CrossContryService.this.watchFace = new CrossContryWatchScreen(CrossContryService.this.mContext, CrossContryService.this.isMixSport);
                        if (CrossContryService.this.mSlptClockClient.lockService()) {
                            CrossContryService.this.createSportClock();
                            CrossContryService.this.mSlptClockClient.unlockService();
                            return;
                        }
                        Log.i("slptCCSview", "lock service error!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        C02602() {
        }

        public void onServiceDisconnected() {
            Log.d("slptCCSview", "slpt clock service has crashed");
            try {
                CrossContryService.this.mSlptClockClient.bindService(CrossContryService.this.mContext, "slptCCSview", CrossContryService.this.callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceConnected() {
            new Thread(new C02591()).start();
        }
    }

    public void onCreate() {
        Log.d("slptCCSview", "onCreate ---------------!");
        this.mContext = getApplicationContext();
        SportLayoutTheme.initSportTheme(this.mContext);
    }

    public int onStartCommand(Intent intent, int flag, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flag, startId);
        }
        try {
            this.clockPeriod = intent.getIntExtra("clockperiod", 0);
            this.isMixSport = intent.getBooleanExtra("key_multi_sport", false);
            if (this.mSlptClockClient.serviceIsConnected()) {
                new Thread(new C02581()).start();
            } else {
                this.mSlptClockClient.bindService(this.mContext, "slptCCSview", this.callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flag, startId);
    }

    private void createSportClock() {
        Util.acquireWakeLock(this.mContext, 10, "slptCCSview");
        ArrayList<SlptClock> clockList = this.watchFace.getLayoutClock();
        this.mSlptClockClient.clearAllClock();
        if (clockList.size() >= SlptClock.CLOCK_INDEX_OVERFOLLOW) {
            Log.e("slptCCSview", "watch face too many");
            return;
        }
        int i = 0;
        while (i < clockList.size()) {
            this.mSlptClockClient.selectClockIndex(i);
            if (this.mSlptClockClient.enableOneClock((SlptClock) clockList.get(i))) {
                clockList.set(i, null);
                i++;
            } else {
                Log.i("slptCCSview", "enable clock error");
                return;
            }
        }
        this.mSlptClockClient.selectClockIndex(0);
        this.mSlptClockClient.setClockPeriod(this.clockPeriod);
        Log.i("slptCCSview", "set clock period " + this.clockPeriod + " success");
        this.mSlptClockClient.enableSportMode();
        if (this.isMixSport) {
            this.mSlptClockClient.setLongUpKeyStatus(0);
        } else {
            this.mSlptClockClient.setLongUpKeyStatus(1);
        }
        Log.i("slptCCSview", "enable clock format 24 " + DateFormat.is24HourFormat(this.mContext));
        if (DateFormat.is24HourFormat(this.mContext)) {
            this.mSlptClockClient.setHourFormat(1);
        } else {
            this.mSlptClockClient.setHourFormat(0);
        }
        this.mSlptClockClient.setMeasurement(Util.getMeasurement(this.mContext));
        this.mSlptClockClient.enableSlpt();
        Log.i("slptCCSview", "enable slpt success");
    }

    public void onDestroy() {
        Log.d("slptCCSview", "onDestroy ---------------!");
        try {
            synchronized (this.mLock) {
                this.mSlptClockClient.unbindService(this.mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
