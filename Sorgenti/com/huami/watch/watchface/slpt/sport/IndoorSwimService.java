package com.huami.watch.watchface.slpt.sport;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import com.huami.watch.watchface.slpt.sport.layout.IndroorSwimWatchScreen;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutClock;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutTheme;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import java.util.ArrayList;

public class IndoorSwimService extends Service {
    Callback callback = new C02722();
    private int clockPeriod;
    private boolean isMixSport;
    private Context mContext;
    private Object mLock = Util.mLock;
    private SlptClockClient mSlptClockClient = new SlptClockClient();
    private SportLayoutClock watchFace;

    class C02701 implements Runnable {
        C02701() {
        }

        public void run() {
            try {
                synchronized (IndoorSwimService.this.mLock) {
                    IndoorSwimService.this.watchFace = new IndroorSwimWatchScreen(IndoorSwimService.this.mContext, IndoorSwimService.this.isMixSport);
                    if (IndoorSwimService.this.mSlptClockClient.lockService()) {
                        IndoorSwimService.this.createSportClock();
                        IndoorSwimService.this.mSlptClockClient.unlockService();
                        return;
                    }
                    Log.i("IndoorSwimService", "lock service error!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C02722 implements Callback {

        class C02711 implements Runnable {
            C02711() {
            }

            public void run() {
                try {
                    synchronized (IndoorSwimService.this.mLock) {
                        IndoorSwimService.this.watchFace = new IndroorSwimWatchScreen(IndoorSwimService.this.mContext, IndoorSwimService.this.isMixSport);
                        if (IndoorSwimService.this.mSlptClockClient.lockService()) {
                            IndoorSwimService.this.createSportClock();
                            IndoorSwimService.this.mSlptClockClient.unlockService();
                            return;
                        }
                        Log.i("IndoorSwimService", "lock service error!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        C02722() {
        }

        public void onServiceDisconnected() {
            Log.d("IndoorSwimService", "slpt clock service has crashed");
            try {
                IndoorSwimService.this.mSlptClockClient.bindService(IndoorSwimService.this.mContext, "IndoorSwimService", IndoorSwimService.this.callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceConnected() {
            new Thread(new C02711()).start();
        }
    }

    public void onCreate() {
        Log.d("IndoorSwimService", "onCreate ---------------!");
        this.mContext = getApplicationContext();
        SportLayoutTheme.initSportTheme(this.mContext);
    }

    public int onStartCommand(Intent intent, int flag, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flag, startId);
        }
        try {
            this.clockPeriod = intent.getIntExtra("clockperiod", 0);
            this.isMixSport = intent.getBooleanExtra("key_multi_sport", false);
            if (this.mSlptClockClient.serviceIsConnected()) {
                new Thread(new C02701()).start();
            } else {
                this.mSlptClockClient.bindService(this.mContext, "IndoorSwimService", this.callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flag, startId);
    }

    private void createSportClock() {
        Util.acquireWakeLock(this.mContext, 10, "IndoorSwimService");
        ArrayList<SlptClock> clockList = this.watchFace.getLayoutClock();
        this.mSlptClockClient.clearAllClock();
        if (clockList.size() >= SlptClock.CLOCK_INDEX_OVERFOLLOW) {
            Log.e("IndoorSwimService", "watch face too many");
            return;
        }
        int i = 0;
        while (i < clockList.size()) {
            this.mSlptClockClient.selectClockIndex(i);
            if (this.mSlptClockClient.enableOneClock((SlptClock) clockList.get(i))) {
                clockList.set(i, null);
                i++;
            } else {
                Log.i("IndoorSwimService", "enable clock error");
                return;
            }
        }
        this.mSlptClockClient.selectClockIndex(0);
        this.mSlptClockClient.setClockPeriod(this.clockPeriod);
        Log.i("IndoorSwimService", "set clock period " + this.clockPeriod + " success");
        this.mSlptClockClient.enableSportMode();
        if (this.isMixSport) {
            this.mSlptClockClient.setLongUpKeyStatus(0);
        } else {
            this.mSlptClockClient.setLongUpKeyStatus(1);
        }
        Log.i("IndoorSwimService", "enable clock format 24 " + DateFormat.is24HourFormat(this.mContext));
        if (DateFormat.is24HourFormat(this.mContext)) {
            this.mSlptClockClient.setHourFormat(1);
        } else {
            this.mSlptClockClient.setHourFormat(0);
        }
        this.mSlptClockClient.enableSlpt();
        if (!Util.needSwimYardUnit(this.mContext) || this.isMixSport) {
            this.mSlptClockClient.setMeasurement(0);
        } else {
            this.mSlptClockClient.setMeasurement(2);
        }
        Log.i("IndoorSwimService", "enable slpt success");
    }

    public void onDestroy() {
        Log.d("IndoorSwimService", "onDestroy ---------------!");
        try {
            synchronized (this.mLock) {
                this.mSlptClockClient.unbindService(this.mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
