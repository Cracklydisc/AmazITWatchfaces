package com.huami.watch.watchface.slpt.sport;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import com.huami.watch.watchface.slpt.sport.layout.MountainWatchScreen;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutClock;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutTheme;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import java.util.ArrayList;

public class MountainWatchService extends Service {
    Callback callback = new C02752();
    private int clockPeriod;
    private boolean isMixSport;
    private Context mContext;
    private Object mLock = Util.mLock;
    private SlptClockClient mSlptClockClient = new SlptClockClient();
    private SportLayoutClock watchFace;

    class C02731 implements Runnable {
        C02731() {
        }

        public void run() {
            try {
                synchronized (MountainWatchService.this.mLock) {
                    MountainWatchService.this.watchFace = new MountainWatchScreen(MountainWatchService.this.mContext, MountainWatchService.this.isMixSport);
                    if (MountainWatchService.this.mSlptClockClient.lockService()) {
                        MountainWatchService.this.createSportClock();
                        MountainWatchService.this.mSlptClockClient.unlockService();
                        return;
                    }
                    Log.i("MountainWatchService", "lock service error!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C02752 implements Callback {

        class C02741 implements Runnable {
            C02741() {
            }

            public void run() {
                try {
                    synchronized (MountainWatchService.this.mLock) {
                        MountainWatchService.this.watchFace = new MountainWatchScreen(MountainWatchService.this.mContext, MountainWatchService.this.isMixSport);
                        if (MountainWatchService.this.mSlptClockClient.lockService()) {
                            MountainWatchService.this.createSportClock();
                            MountainWatchService.this.mSlptClockClient.unlockService();
                            return;
                        }
                        Log.i("MountainWatchService", "lock service error!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        C02752() {
        }

        public void onServiceDisconnected() {
            Log.d("MountainWatchService", "slpt clock service has crashed");
            try {
                MountainWatchService.this.mSlptClockClient.bindService(MountainWatchService.this.mContext, "MountainWatchService", MountainWatchService.this.callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceConnected() {
            new Thread(new C02741()).start();
        }
    }

    public void onCreate() {
        Log.d("MountainWatchService", "onCreate ---------------!");
        this.mContext = getApplicationContext();
        SportLayoutTheme.initSportTheme(this.mContext);
    }

    public int onStartCommand(Intent intent, int flag, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flag, startId);
        }
        try {
            this.clockPeriod = intent.getIntExtra("clockperiod", 0);
            this.isMixSport = intent.getBooleanExtra("key_multi_sport", false);
            if (this.mSlptClockClient.serviceIsConnected()) {
                new Thread(new C02731()).start();
            } else {
                this.mSlptClockClient.bindService(this.mContext, "MountainWatchService", this.callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flag, startId);
    }

    private void createSportClock() {
        Util.acquireWakeLock(this.mContext, 10, "MountainWatchService");
        ArrayList<SlptClock> clockList = this.watchFace.getLayoutClock();
        this.mSlptClockClient.clearAllClock();
        if (clockList.size() >= SlptClock.CLOCK_INDEX_OVERFOLLOW) {
            Log.e("MountainWatchService", "watch face too many");
            return;
        }
        int i = 0;
        while (i < clockList.size()) {
            this.mSlptClockClient.selectClockIndex(i);
            if (this.mSlptClockClient.enableOneClock((SlptClock) clockList.get(i))) {
                clockList.set(i, null);
                i++;
            } else {
                Log.i("MountainWatchService", "enable clock error");
                return;
            }
        }
        this.mSlptClockClient.selectClockIndex(0);
        this.mSlptClockClient.setClockPeriod(this.clockPeriod);
        Log.i("MountainWatchService", "set clock period " + this.clockPeriod + " success");
        this.mSlptClockClient.enableSportMode();
        if (this.isMixSport) {
            this.mSlptClockClient.setLongUpKeyStatus(0);
        } else {
            this.mSlptClockClient.setLongUpKeyStatus(1);
        }
        Log.i("MountainWatchService", "enable clock format 24 " + DateFormat.is24HourFormat(this.mContext));
        if (DateFormat.is24HourFormat(this.mContext)) {
            this.mSlptClockClient.setHourFormat(1);
        } else {
            this.mSlptClockClient.setHourFormat(0);
        }
        this.mSlptClockClient.enableSlpt();
        this.mSlptClockClient.setMeasurement(Util.getMeasurement(this.mContext));
        Log.i("MountainWatchService", "enable slpt success");
    }

    public void onDestroy() {
        Log.d("MountainWatchService", "onDestroy ---------------!");
        try {
            synchronized (this.mLock) {
                this.mSlptClockClient.unbindService(this.mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
