package com.huami.watch.watchface.slpt.sport;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutClockTrain;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutTheme;
import com.huami.watch.watchface.slpt.sport.layout.SportTrainInfo;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import java.util.ArrayList;
import org.json.JSONObject;

public class RunTrainModeService extends Service {
    Callback callback = new C02872();
    private int clockPeriod;
    SportTrainInfo info = null;
    private Context mContext;
    private Object mLock = Util.mLock;
    private SlptClockClient mSlptClockClient = new SlptClockClient();
    private String trainInfo = null;

    class C02851 implements Runnable {
        C02851() {
        }

        public void run() {
            try {
                synchronized (RunTrainModeService.this.mLock) {
                    if (RunTrainModeService.this.mSlptClockClient.lockService()) {
                        RunTrainModeService.this.createSportClock();
                        RunTrainModeService.this.mSlptClockClient.unlockService();
                        return;
                    }
                    Log.i("RunTrainMode", "lock service error!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C02872 implements Callback {

        class C02861 implements Runnable {
            C02861() {
            }

            public void run() {
                try {
                    synchronized (RunTrainModeService.this.mLock) {
                        if (RunTrainModeService.this.mSlptClockClient.lockService()) {
                            RunTrainModeService.this.createSportClock();
                            RunTrainModeService.this.mSlptClockClient.unlockService();
                            return;
                        }
                        Log.i("RunTrainMode", "lock service error!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        C02872() {
        }

        public void onServiceDisconnected() {
            Log.d("RunTrainMode", "slpt clock service has crashed");
            try {
                RunTrainModeService.this.mSlptClockClient.bindService(RunTrainModeService.this.mContext, "RunTrainMode", RunTrainModeService.this.callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceConnected() {
            new Thread(new C02861()).start();
        }
    }

    public void onCreate() {
        Log.d("RunTrainMode", "onCreate ---------------!");
        this.mContext = getApplicationContext();
    }

    public int onStartCommand(Intent intent, int flag, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flag, startId);
        }
        SportLayoutTheme.initSportTheme(this.mContext);
        try {
            this.clockPeriod = intent.getIntExtra("clockperiod", 0);
            this.trainInfo = intent.getStringExtra("train_unit");
            if (this.trainInfo == null || this.trainInfo.equals("")) {
                Log.i("RunTrainMode", "get Train info error");
                return super.onStartCommand(intent, flag, startId);
            }
            JSONObject jStr = new JSONObject(this.trainInfo);
            this.info = new SportTrainInfo();
            this.info.setArray(SportItemInfoWrapper.parseStringToOrder(jStr.optString("data_item")));
            this.info.setCurrentDistance(jStr.optInt("curtargetDis", 0));
            this.info.setCurrentTime(jStr.optInt("curtargetTime", 0));
            this.info.setTotalDistance(jStr.optInt("totaltargetDis", 0));
            this.info.setTotalTime(jStr.optInt("totaltargetTime", 0));
            this.info.setStartDistance(jStr.optInt("startDis", 0));
            this.info.setStartTime(jStr.optInt("startTime", 0));
            this.info.setTrainMode(jStr.optInt("interval_train_mode", 0));
            Log.d("RunTrainMode", "train info " + this.info.toString());
            if (this.mSlptClockClient.serviceIsConnected()) {
                new Thread(new C02851()).start();
            } else {
                this.mSlptClockClient.bindService(this.mContext, "RunTrainMode", this.callback);
            }
            return super.onStartCommand(intent, flag, startId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createSportClock() {
        Util.acquireWakeLock(this.mContext, 10, "RunTrainMode");
        this.mSlptClockClient.clearAllClock();
        SportLayoutClockTrain layoutClock = new SportLayoutClockTrain(this.mContext, this.info);
        layoutClock.CreateSportClock();
        ArrayList<SlptClock> clockList = layoutClock.getLayoutClock();
        if (clockList.size() > SlptClock.CLOCK_INDEX_OVERFOLLOW) {
            Log.e("RunTrainMode", "watch face too many");
            return;
        }
        Log.i("RunTrainMode", "add clock list size " + clockList.size());
        int i = 0;
        while (i < clockList.size()) {
            this.mSlptClockClient.selectClockIndex(i);
            if (this.mSlptClockClient.enableOneClock((SlptClock) clockList.get(i))) {
                clockList.set(i, null);
                i++;
            } else {
                Log.i("RunTrainMode", "enable clock error");
                return;
            }
        }
        this.mSlptClockClient.selectClockIndex(0);
        this.mSlptClockClient.setClockPeriod(this.clockPeriod);
        Log.i("RunTrainMode", "set clock period " + this.clockPeriod + " success");
        this.mSlptClockClient.enableSportMode();
        this.mSlptClockClient.setLongUpKeyStatus(1);
        Log.i("RunTrainMode", "enable clock format 24 " + DateFormat.is24HourFormat(this.mContext));
        if (DateFormat.is24HourFormat(this.mContext)) {
            this.mSlptClockClient.setHourFormat(1);
        } else {
            this.mSlptClockClient.setHourFormat(0);
        }
        this.mSlptClockClient.enableSlpt();
        this.mSlptClockClient.setMeasurement(Util.getMeasurement(this.mContext));
    }

    public void onDestroy() {
        Log.d("RunTrainMode", "onDestroy ---------------!");
        try {
            synchronized (this.mLock) {
                this.mSlptClockClient.unbindService(this.mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
