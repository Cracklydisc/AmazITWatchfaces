package com.huami.watch.watchface.slpt.sport;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutTheme;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.sport.SlptDHTotalDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptDHTotalDistanceLView;
import com.ingenic.iwds.slpt.view.sport.SlptDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptDistanceLView;
import com.ingenic.iwds.slpt.view.sport.SlptSportHPauseView;
import com.ingenic.iwds.slpt.view.sport.SlptSportMPauseView;
import com.ingenic.iwds.slpt.view.sport.SlptSportSPauseView;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.sport.SlptTennisStrokesView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class SlptSportStopService extends Service {
    private SlptPictureView bgView = new SlptPictureView();
    Callback callback = new C02921();
    private byte[][] contentNum;
    SlptLinearLayout dataLayout = new SlptLinearLayout();
    private SlptSportHPauseView hPauseView = new SlptSportHPauseView();
    private SlptPictureView iconView = new SlptPictureView();
    public SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
    private Context mContext;
    private Object mLock = Util.mLock;
    private SlptSportMPauseView mPauseView = new SlptSportMPauseView();
    private SlptPictureView mSeqView = new SlptPictureView();
    private SlptClock mSlptClock;
    private SlptClockClient mSlptClockClient = new SlptClockClient();
    private SlptLinearLayout mTitle = new SlptLinearLayout();
    private byte[][] pauseNum;
    private SlptSportSPauseView sPauseView = new SlptSportSPauseView();
    private SlptPictureView sSeqView = new SlptPictureView();
    private boolean servicesStarted = false;
    private int sportType = 0;
    private long stopTime;
    private int stopType = 1;
    private String themeDir;
    private SlptLinearLayout timeLayout = new SlptLinearLayout();
    private SlptLinearLayout timeTitleLayout = new SlptLinearLayout();
    private SlptLinearLayout titleLayout = new SlptLinearLayout();
    private byte[][] titleNum;

    class C02921 implements Callback {

        class C02911 implements Runnable {
            C02911() {
            }

            public void run() {
                synchronized (SlptSportStopService.this.mLock) {
                    if (SlptSportStopService.this.servicesStarted) {
                        SlptSportStopService.this.initLayout(SlptSportStopService.this.mContext);
                        SlptSportStopService.this.initDefaultSettings(SlptSportStopService.this.mContext);
                        if (!SlptSportStopService.this.mSlptClockClient.lockService()) {
                            Log.i("slptStopSview", "lock service error!");
                            return;
                        } else if (SlptSportStopService.this.mSlptClockClient.tryEnableClock(SlptSportStopService.this.mSlptClock)) {
                            Log.i("slptStopSview", "enable clock format 24 " + DateFormat.is24HourFormat(SlptSportStopService.this.mContext));
                            if (DateFormat.is24HourFormat(SlptSportStopService.this.mContext)) {
                                SlptSportStopService.this.mSlptClockClient.setHourFormat(1);
                            } else {
                                SlptSportStopService.this.mSlptClockClient.setHourFormat(0);
                            }
                            SlptSportStopService.this.mSlptClockClient.setLongUpKeyStatus(1);
                            SlptSportStopService.this.mSlptClockClient.setClockPeriod(1);
                            SlptSportStopService.this.mSlptClockClient.enableSportMode();
                            SlptSportStopService.this.mSlptClockClient.setSportStopTime(SlptSportStopService.this.stopTime);
                            SlptSportStopService.this.mSlptClockClient.enableSlpt();
                            SlptSportStopService.this.mSlptClockClient.unlockService();
                            SlptSportStopService.this.mSlptClock = null;
                            return;
                        } else {
                            Log.i("slptStopSview", "unable to enable watchface");
                            SlptSportStopService.this.mSlptClockClient.unlockService();
                            return;
                        }
                    }
                    Log.i("slptStopSview", "service has stoped!");
                }
            }
        }

        C02921() {
        }

        public void onServiceDisconnected() {
            Log.d("slptStopSview", "slpt clock service has crashed");
            try {
                SlptSportStopService.this.mSlptClockClient.bindService(SlptSportStopService.this.mContext, "slptStopSview", SlptSportStopService.this.callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceConnected() {
            new Thread(new C02911()).start();
        }
    }

    private void initLayout(Context context) {
        SportLayoutTheme.setViewBgColor(context, this.linearLayout);
        this.contentNum = SportItemInfoWrapper.getContentNum(this.mContext, this.themeDir);
        this.titleNum = SportItemInfoWrapper.getTitleNum(this.mContext, this.themeDir);
        this.pauseNum = SportItemInfoWrapper.getPauseNum(this.mContext, this.themeDir);
        this.iconView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, this.themeDir + "bg/sport_pause_img_8c.png"));
        this.titleLayout = (SlptLinearLayout) SportItemInfoWrapper.getFontStringView(this.mContext, this.mContext.getResources().getString(R.string.sport_has_paused), 21, -16711936, (short) 0);
        this.hPauseView.setImagePictureArray(this.pauseNum);
        this.mPauseView.setImagePictureArray(this.pauseNum);
        this.sPauseView.setImagePictureArray(this.pauseNum);
        byte[] seqMem1 = SimpleFile.readFileFromAssets(this.mContext, this.themeDir + "pause-num/colon.png");
        this.sSeqView.setImagePicture(seqMem1);
        this.mSeqView.setImagePicture(seqMem1);
        this.timeLayout.add(this.hPauseView);
        this.timeLayout.add(this.mSeqView);
        this.timeLayout.add(this.mPauseView);
        this.timeLayout.add(this.sSeqView);
        this.timeLayout.add(this.sPauseView);
        this.timeTitleLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringViewBlack(this.mContext, this.mContext.getResources().getString(R.string.sport_paused_time), (short) 0);
        if (this.stopType != 1) {
            SlptDistanceFView fView;
            SlptPictureView seqView;
            SlptDistanceLView lView;
            if (this.stopType == 2) {
                this.mTitle = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringViewBlack(this.mContext, this.mContext.getResources().getString(R.string.sport_mileage), (short) 0);
                fView = new SlptDistanceFView();
                seqView = new SlptPictureView();
                lView = new SlptDistanceLView();
                fView.setImagePictureArray(this.pauseNum);
                lView.setImagePictureArray(this.pauseNum);
                fView.setUnit(0);
                lView.setUnit(0);
                seqView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, this.themeDir + "pause-num/dot.png"));
                SlptSportUtil.setDistanceSeparatorView(seqView);
                this.dataLayout.add(fView);
                this.dataLayout.add(seqView);
                this.dataLayout.add(lView);
            }
            if (this.stopType == 3) {
                this.mTitle = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringViewBlack(this.mContext, this.mContext.getResources().getString(R.string.sport_distance), (short) 0);
                fView = new SlptDistanceFView();
                seqView = new SlptPictureView();
                lView = new SlptDistanceLView();
                fView.setImagePictureArray(this.pauseNum);
                lView.setImagePictureArray(this.pauseNum);
                fView.setUnit(1);
                lView.setUnit(1);
                seqView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, this.themeDir + "pause-num/dot.png"));
                SlptSportUtil.setDistanceSeparatorView(seqView);
                this.dataLayout.add(fView);
                this.dataLayout.add(seqView);
                this.dataLayout.add(lView);
            }
            if (this.stopType == 4) {
                this.mTitle = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringViewBlack(this.mContext, this.mContext.getResources().getString(R.string.sport_downhill_total_distance), (short) 0);
                SlptDHTotalDistanceFView fView2 = new SlptDHTotalDistanceFView();
                seqView = new SlptPictureView();
                SlptDHTotalDistanceLView lView2 = new SlptDHTotalDistanceLView();
                fView2.setImagePictureArray(this.pauseNum);
                lView2.setImagePictureArray(this.pauseNum);
                seqView.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, this.themeDir + "pause-num/dot.png"));
                SlptSportUtil.setDHTotalDistanceSepView(seqView);
                this.dataLayout.add(fView2);
                this.dataLayout.add(seqView);
                this.dataLayout.add(lView2);
            }
            if (this.stopType == 5) {
                this.mTitle = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringViewBlack(this.mContext, this.mContext.getResources().getString(R.string.sport_tennis_strokes), (short) 0);
                SlptTennisStrokesView fView3 = new SlptTennisStrokesView();
                fView3.setImagePictureArray(this.pauseNum);
                this.dataLayout.add(fView3);
            }
        }
    }

    private void initDefaultSettings(Context mContext) {
        SlptLinearLayout contentLayout;
        if (this.stopType == 1) {
            this.bgView.setImagePicture(SimpleFile.readFileFromAssets(mContext, this.themeDir + "bg/sport_pause_bg_1_8c.png"));
            this.timeLayout.setStart(0, 108);
            this.timeLayout.setRect(320, 33);
            this.timeLayout.alignX = (byte) 2;
            this.timeTitleLayout.setStart(0, 145);
            this.timeTitleLayout.setRect(320, 24);
            this.timeTitleLayout.alignX = (byte) 2;
            this.linearLayout.add(this.bgView);
            this.linearLayout.add(this.timeLayout);
            this.linearLayout.add(this.timeTitleLayout);
        } else {
            this.bgView.setImagePicture(SimpleFile.readFileFromAssets(mContext, this.themeDir + "bg/sport_pause_bg_2_8c.png"));
            this.timeLayout.setStart(15, 105);
            this.timeLayout.setRect(144, 33);
            this.timeLayout.alignX = (byte) 2;
            this.timeTitleLayout.setStart(15, 144);
            this.timeTitleLayout.setRect(144, 24);
            this.timeTitleLayout.alignX = (byte) 2;
            this.dataLayout.setStart(161, 105);
            this.dataLayout.setRect(144, 33);
            this.dataLayout.alignX = (byte) 2;
            this.mTitle.setStart(161, 144);
            this.mTitle.setRect(144, 24);
            this.mTitle.alignX = (byte) 2;
            this.linearLayout.add(this.bgView);
            this.linearLayout.add(this.timeLayout);
            this.linearLayout.add(this.timeTitleLayout);
            this.linearLayout.add(this.dataLayout);
            this.linearLayout.add(this.mTitle);
        }
        this.linearLayout.add(SportItemInfoWrapper.createSystemTimeLayoutBlack(mContext, this.titleNum));
        this.titleLayout.setStart(0, 72);
        this.titleLayout.setRect(320, 26);
        this.titleLayout.alignX = (byte) 2;
        this.linearLayout.add(this.titleLayout);
        SlptPictureView lockIcon = new SlptPictureView();
        lockIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, this.themeDir + "bg/sport_lock.png"));
        if (ModelUtil.isRealModelEverest(mContext)) {
            contentLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_any_key_wakeup), (short) 0);
        } else {
            contentLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_power_key_wakeup), (short) 0);
        }
        if (Util.needEnAssets()) {
            SlptLinearLayout layoutBottom = new SlptLinearLayout();
            layoutBottom.add(lockIcon);
            layoutBottom.add(contentLayout);
            lockIcon.padding.right = (short) 8;
            layoutBottom.setStart(0, 237);
            layoutBottom.setRect(320, 2147483646);
            layoutBottom.alignX = (byte) 2;
            this.linearLayout.add(layoutBottom);
        } else {
            lockIcon.setStart(78, 238);
            contentLayout.setStart(116, 237);
            this.linearLayout.add(lockIcon);
            this.linearLayout.add(contentLayout);
        }
        this.mSlptClock = new SlptClock(this.linearLayout);
    }

    public void onCreate() {
        Log.d("slptStopSview", "onCreate ---------------!");
        this.mContext = getApplicationContext();
        this.themeDir = "sport/black/zh/";
    }

    public int onStartCommand(Intent intent, int flag, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flag, startId);
        }
        try {
            this.sportType = intent.getIntExtra("pause_view_type", 0);
            this.stopTime = intent.getLongExtra("timestamp", 0);
            if (this.sportType == 15 || this.sportType == 14) {
                this.stopType = 3;
            } else if (this.sportType == 12 || this.sportType == 10) {
                this.stopType = 1;
            } else if (this.sportType == 11) {
                this.stopType = 4;
            } else if (this.sportType == 17) {
                this.stopType = 5;
            } else {
                this.stopType = 2;
            }
            Log.d("slptStopSview", "intent sport type " + this.sportType + " stopTime " + this.stopTime + " data type " + this.stopType);
            if (this.servicesStarted) {
                Log.d("slptStopSview", "service already started");
                return startId;
            }
            this.servicesStarted = true;
            this.mSlptClockClient.bindService(this.mContext, "slptStopSview", this.callback);
            return super.onStartCommand(intent, flag, startId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDestroy() {
        Log.d("slptStopSview", "onDestroy ---------------!");
        try {
            synchronized (this.mLock) {
                this.mSlptClockClient.unbindService(this.mContext);
                this.servicesStarted = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
