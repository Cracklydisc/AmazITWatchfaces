package com.huami.watch.watchface.slpt.sport;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutTheme;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class SportChangePauseService extends Service {
    Callback callback = new C02971();
    private int changeType = 0;
    private int clockPeriod;
    private Context mContext;
    private Object mLock = Util.mLock;
    private SlptClock mSlptClock;
    private SlptClockClient mSlptClockClient = new SlptClockClient();
    private long stopTime = 0;
    private String themeDir;

    class C02971 implements Callback {

        class C02961 implements Runnable {
            C02961() {
            }

            public void run() {
                try {
                    synchronized (SportChangePauseService.this.mLock) {
                        if (SportChangePauseService.this.changeType == 1) {
                            SportChangePauseService.this.mSlptClock = SportChangePauseService.this.getChangeLayout(SportChangePauseService.this.themeDir + "bg/sport_fun_icon_running_8c.png", SportChangePauseService.this.mContext.getResources().getString(R.string.please_get_ready_for_runnning));
                        }
                        if (SportChangePauseService.this.changeType == 9) {
                            SportChangePauseService.this.mSlptClock = SportChangePauseService.this.getChangeLayout(SportChangePauseService.this.themeDir + "bg/sport_fun_icon_outdoor_riding_8c.png", SportChangePauseService.this.mContext.getResources().getString(R.string.please_get_ready_for_riding));
                        }
                        if (SportChangePauseService.this.changeType == 15) {
                            SportChangePauseService.this.mSlptClock = SportChangePauseService.this.getChangeLayout(SportChangePauseService.this.themeDir + "bg/sport_fun_icon_outdoor_swimming_8c.png", SportChangePauseService.this.mContext.getResources().getString(R.string.please_get_ready_for_swimming));
                        }
                        SportChangePauseService.this.createSportClock();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        C02971() {
        }

        public void onServiceDisconnected() {
            Log.d("SportChangePauseService", "slpt clock service has crashed");
            try {
                SportChangePauseService.this.mSlptClockClient.bindService(SportChangePauseService.this.mContext, "SportChangePauseService", SportChangePauseService.this.callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceConnected() {
            new Thread(new C02961()).start();
        }
    }

    public void onCreate() {
        Log.d("SportChangePauseService", "onCreate ---------------!");
        this.mContext = getApplicationContext();
        SportLayoutTheme.initSportTheme(this.mContext);
        this.themeDir = SportLayoutTheme.getSlptThemesDir(this.mContext, false);
    }

    public int onStartCommand(Intent intent, int flag, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flag, startId);
        }
        this.clockPeriod = intent.getIntExtra("clockperiod", 0);
        this.stopTime = intent.getLongExtra("timestamp", 0);
        this.changeType = intent.getIntExtra("sport_change_to", 0);
        try {
            this.mSlptClockClient.bindService(this.mContext, "SportChangePauseService", this.callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flag, startId);
    }

    private SlptClock getChangeLayout(String iconPath, String iconName) {
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptPictureView bg = new SlptPictureView();
        bg.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, this.themeDir + "bg/sport_triathlon_bg_2.png"));
        layout.add(bg);
        layout.add(SportItemInfoWrapper.createMixSportTitle(this.mContext, SportItemInfoWrapper.getTitleNum(this.mContext)));
        layout.add(SportItemInfoWrapper.createSportChangeValue(this.mContext, SportItemInfoWrapper.getContentNum(this.mContext)));
        layout.add(SportItemInfoWrapper.createSportChangeBottom(this.mContext, iconPath, iconName));
        return new SlptClock(layout);
    }

    private void createSportClock() {
        Util.acquireWakeLock(this.mContext, 10, "SportChangePauseService");
        if (this.mSlptClockClient.lockService()) {
            this.mSlptClockClient.clearAllClock();
            if (this.mSlptClockClient.tryEnableClock(this.mSlptClock)) {
                this.mSlptClockClient.setClockPeriod(this.clockPeriod);
                Log.i("SportChangePauseService", "set clock period " + this.clockPeriod + " success");
                this.mSlptClockClient.enableSportMode();
                this.mSlptClockClient.setLongUpKeyStatus(1);
                Log.i("SportChangePauseService", "enable clock format 24 " + DateFormat.is24HourFormat(this.mContext));
                if (DateFormat.is24HourFormat(this.mContext)) {
                    this.mSlptClockClient.setHourFormat(1);
                } else {
                    this.mSlptClockClient.setHourFormat(0);
                }
                this.mSlptClockClient.setSportStopTime(this.stopTime);
                this.mSlptClockClient.setMeasurement(Util.getMeasurement(this.mContext));
                this.mSlptClockClient.enableSlpt();
                Log.i("SportChangePauseService", "enable slpt success");
                this.mSlptClockClient.unlockService();
                this.mSlptClock = null;
                return;
            }
            Log.i("SportChangePauseService", "enable clock slpt Failed");
            this.mSlptClockClient.unlockService();
            return;
        }
        Log.i("SportChangePauseService", "lock service error!");
    }

    public void onDestroy() {
        Log.d("SportChangePauseService", "onDestroy ---------------!");
        try {
            synchronized (this.mLock) {
                this.mSlptClockClient.unbindService(this.mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
