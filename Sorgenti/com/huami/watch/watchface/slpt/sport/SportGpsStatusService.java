package com.huami.watch.watchface.slpt.sport;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.SystemProperties;
import android.util.Log;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.Lock.LowPowerClock;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import com.ingenic.iwds.slpt.view.utils.SlptWatchFaceConst;

public class SportGpsStatusService extends Service {
    Callback callback = new C03002();
    private String iconPath = null;
    private Context mContext;
    private Object mLock = Util.mLock;
    private SlptClockClient mSlptClockClient = null;
    private boolean needGPS = false;
    private screenOffReceiver receiver;
    private String title = null;

    class C02981 implements Runnable {
        C02981() {
        }

        public void run() {
            try {
                synchronized (SportGpsStatusService.this.mLock) {
                    if (SportGpsStatusService.this.mSlptClockClient.lockService()) {
                        SportGpsStatusService.this.createSportClock();
                        SportGpsStatusService.this.mSlptClockClient.unlockService();
                        return;
                    }
                    Log.i("SportGpsService", "lock service error!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C03002 implements Callback {

        class C02991 implements Runnable {
            C02991() {
            }

            public void run() {
                try {
                    synchronized (SportGpsStatusService.this.mLock) {
                        if (SportGpsStatusService.this.mSlptClockClient.lockService()) {
                            SportGpsStatusService.this.createSportClock();
                            SportGpsStatusService.this.mSlptClockClient.unlockService();
                            return;
                        }
                        Log.i("SportGpsService", "lock service error!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        C03002() {
        }

        public void onServiceDisconnected() {
            Log.d("SportGpsService", "slpt clock service has crashed");
            try {
                SportGpsStatusService.this.mSlptClockClient.bindService(SportGpsStatusService.this.mContext, "SportGpsService", SportGpsStatusService.this.callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceConnected() {
            new Thread(new C02991()).start();
        }
    }

    public class screenOffReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            int status = Integer.parseInt(SystemProperties.get("sys.watchface.gps.status", "0"));
            Log.i("SportGpsService", "screen off current GPS status " + status);
            if (SportGpsStatusService.this.mSlptClockClient != null && status != 0) {
                if (!SportGpsStatusService.this.mSlptClockClient.serviceIsConnected()) {
                    Log.i("SportGpsService", "screen off when service not bind ");
                } else if (SportGpsStatusService.this.mSlptClockClient.lockService()) {
                    if (status == 1) {
                        SportGpsStatusService.this.mSlptClockClient.selectClockIndex(3);
                    } else if (status == 2) {
                        SportGpsStatusService.this.mSlptClockClient.selectClockIndex(4);
                    } else if (status == 4) {
                        SportGpsStatusService.this.mSlptClockClient.selectClockIndex(5);
                    } else if (status == 3) {
                        SportGpsStatusService.this.mSlptClockClient.selectClockIndex(6);
                    }
                    SportGpsStatusService.this.mSlptClockClient.unlockService();
                } else {
                    Log.i("SportGpsService", "lock service error!");
                }
            }
        }
    }

    public void onCreate() {
        Log.d("SportGpsService", "onCreate ---------------!");
        this.mContext = getApplicationContext();
        this.receiver = new screenOffReceiver();
        this.mSlptClockClient = new SlptClockClient();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.SCREEN_OFF");
        this.mContext.registerReceiver(this.receiver, filter);
        Util.hideWatchFace(this.mContext);
    }

    public int onStartCommand(Intent intent, int flag, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flag, startId);
        }
        int type = intent.getIntExtra("sport_type", 0);
        SetTitleString(type);
        Log.d("SportGpsService", "enable GPS page type " + type + " title  " + this.title);
        try {
            if (this.mSlptClockClient.serviceIsConnected()) {
                new Thread(new C02981()).start();
            } else {
                this.mSlptClockClient.bindService(this.mContext, "SportGpsService", this.callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flag, startId);
    }

    private void createSportClock() {
        Util.acquireWakeLock(this.mContext, 10, "SportGpsService");
        this.mSlptClockClient.clearAllClock();
        if (this.needGPS) {
            this.mSlptClockClient.selectClockIndex(2);
            if (this.mSlptClockClient.enableOneClock(new LowPowerClock(this.mContext))) {
                this.mSlptClockClient.selectClockIndex(3);
                if (this.mSlptClockClient.enableOneClock(SportItemInfoWrapper.createGpsReadyClock(this.mContext, this.title, this.iconPath))) {
                    this.mSlptClockClient.selectClockIndex(4);
                    if (this.mSlptClockClient.enableOneClock(SportItemInfoWrapper.createGpsLostClock(this.mContext, this.title))) {
                        this.mSlptClockClient.selectClockIndex(5);
                        if (this.mSlptClockClient.enableOneClock(SportItemInfoWrapper.createGpsCloseClock(this.mContext, this.title))) {
                            this.mSlptClockClient.selectClockIndex(6);
                            if (this.mSlptClockClient.enableOneClock(SportItemInfoWrapper.createGpsSearchClock(this.mContext, this.title, this.iconPath))) {
                                int status = Integer.parseInt(SystemProperties.get("sys.watchface.gps.status", "0"));
                                Log.i("SportGpsService", "current GPS status " + status);
                                if (status == 1) {
                                    this.mSlptClockClient.selectClockIndex(3);
                                } else if (status == 2) {
                                    this.mSlptClockClient.selectClockIndex(4);
                                } else if (status == 4) {
                                    this.mSlptClockClient.selectClockIndex(5);
                                } else {
                                    this.mSlptClockClient.selectClockIndex(6);
                                }
                            } else {
                                Log.i("SportGpsService", "enable gps lost clock error");
                                return;
                            }
                        }
                        Log.i("SportGpsService", "enable gps lost clock error");
                        return;
                    }
                    Log.i("SportGpsService", "enable gps lost clock error");
                    return;
                }
                Log.i("SportGpsService", "enable gps ready clock error");
                return;
            }
            Log.i("SportGpsService", "enable low battery clock error");
            return;
        }
        this.mSlptClockClient.selectClockIndex(2);
        if (this.mSlptClockClient.enableOneClock(new LowPowerClock(this.mContext))) {
            this.mContext.unregisterReceiver(this.receiver);
            this.mSlptClockClient.selectClockIndex(0);
            if (!this.mSlptClockClient.enableOneClock(SportItemInfoWrapper.createSportStartClock(this.mContext, this.title, this.iconPath))) {
                Log.i("SportGpsService", "enable sport start error");
                return;
            }
        }
        Log.i("SportGpsService", "enable low battery clock error");
        return;
        this.mSlptClockClient.disableSportMode();
        this.mSlptClockClient.setLongUpKeyStatus(1);
        this.mSlptClockClient.setClockPeriod(1);
        this.mSlptClockClient.enableSlpt();
        this.mSlptClockClient.setKeyWakeupStatus(SlptWatchFaceConst.KEY_WAEUP_STATUS_NONE);
        Log.i("SportGpsService", "enable slpt success");
    }

    public void onDestroy() {
        Log.d("SportGpsService", "onDestroy ---------------!");
        super.onDestroy();
        Util.showWatchFace(this.mContext);
        try {
            synchronized (this.mLock) {
                this.mSlptClockClient.unbindService(this.mContext);
            }
            this.mContext.unregisterReceiver(this.receiver);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void SetTitleString(int type) {
        switch (type) {
            case 6:
                this.title = this.mContext.getResources().getString(R.string.waking);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_walk.png";
                return;
            case 7:
                this.title = this.mContext.getResources().getString(R.string.cross_contry);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_trail_running.png";
                return;
            case 8:
                this.title = this.mContext.getResources().getString(R.string.indoor_running);
                this.needGPS = false;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_treadmill.png";
                return;
            case 9:
                this.title = this.mContext.getResources().getString(R.string.outdoor_riding);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_outdoor_riding.png";
                return;
            case 10:
                this.title = this.mContext.getResources().getString(R.string.indoor_riding);
                this.needGPS = false;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_indoor_riding.png";
                return;
            case 11:
                this.title = this.mContext.getResources().getString(R.string.skiing);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_skiing.png";
                return;
            case 12:
                this.title = this.mContext.getResources().getString(R.string.eppitical);
                this.needGPS = false;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_ellipticals.png";
                return;
            case 13:
                this.title = this.mContext.getResources().getString(R.string.mountain);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_climb_mountain.png";
                return;
            case 14:
                this.title = this.mContext.getResources().getString(R.string.indoor_swimming);
                this.needGPS = false;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_indoor_swimming.png";
                return;
            case 15:
                this.title = this.mContext.getResources().getString(R.string.outdoor_swimming);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_outdoor_swimming.png";
                return;
            case 17:
                this.title = this.mContext.getResources().getString(R.string.tennis);
                this.needGPS = false;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_tennis.png";
                return;
            case 18:
                this.title = this.mContext.getResources().getString(R.string.soccer);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_football.png";
                return;
            case 2001:
                this.title = this.mContext.getResources().getString(R.string.triathlon);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_triathlon.png";
                return;
            case 2002:
                this.title = this.mContext.getResources().getString(R.string.complex_sport);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_complex_sport.png";
                return;
            default:
                this.title = this.mContext.getResources().getString(R.string.running);
                this.needGPS = true;
                this.iconPath = "sport/black/zh/bg/sport_history_icon_running.png";
                return;
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
