package com.huami.watch.watchface.slpt.sport;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import com.huami.watch.watchface.slpt.sport.layout.SportItemInfoWrapper;
import com.huami.watch.watchface.slpt.sport.layout.SportLayoutTheme;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.utils.SlptWatchFaceConst;

public class SportLoadingService extends Service {
    Callback callback = new C03032();
    private Context mContext;
    private Object mLock = Util.mLock;
    private SlptClockClient mSlptClockClient = new SlptClockClient();

    class C03011 implements Runnable {
        C03011() {
        }

        public void run() {
            try {
                synchronized (SportLoadingService.this.mLock) {
                    SportLoadingService.this.createSportClock();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C03032 implements Callback {

        class C03021 implements Runnable {
            C03021() {
            }

            public void run() {
                try {
                    synchronized (SportLoadingService.this.mLock) {
                        SportLoadingService.this.createSportClock();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        C03032() {
        }

        public void onServiceDisconnected() {
            Log.d("SportLoadingService", "slpt clock service has crashed");
            try {
                SportLoadingService.this.mSlptClockClient.bindService(SportLoadingService.this.mContext, "SportLoadingService", SportLoadingService.this.callback);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onServiceConnected() {
            Log.i("SportLoadingService", "onServiceConnected");
            new Thread(new C03021()).start();
        }
    }

    public void onCreate() {
        Log.d("SportLoadingService", "onCreate ---------------!");
        this.mContext = getApplicationContext();
        SportLayoutTheme.initSportTheme(this.mContext);
    }

    public int onStartCommand(Intent intent, int flag, int startId) {
        if (intent == null) {
            return super.onStartCommand(intent, flag, startId);
        }
        try {
            if (this.mSlptClockClient.serviceIsConnected()) {
                new Thread(new C03011()).start();
            } else {
                this.mSlptClockClient.bindService(this.mContext, "SportLoadingService", this.callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flag, startId);
    }

    private void createSportClock() {
        Util.acquireWakeLock(this.mContext, 15, "slptLoading");
        if (this.mSlptClockClient.lockService()) {
            this.mSlptClockClient.clearAllClock();
            SlptLayout rootLayout = SportItemInfoWrapper.createLoadingLayout(this.mContext);
            if (rootLayout != null) {
                SlptClock loadingClock = new SlptClock(rootLayout);
                this.mSlptClockClient.selectClockIndex(0);
                this.mSlptClockClient.enableOneClock(loadingClock);
                if (DateFormat.is24HourFormat(this.mContext)) {
                    this.mSlptClockClient.setHourFormat(1);
                } else {
                    this.mSlptClockClient.setHourFormat(0);
                }
                this.mSlptClockClient.setMeasurement(Util.getMeasurement(this.mContext));
                this.mSlptClockClient.enableSportMode();
                this.mSlptClockClient.enableSlpt();
                this.mSlptClockClient.selectClockIndex(SlptClock.CLOCK_INDEX_SHOW);
                Log.i("SportLoadingService", "enable slpt success");
                this.mSlptClockClient.unlockService();
                this.mSlptClockClient.setKeyWakeupStatus(SlptWatchFaceConst.KEY_WAEUP_STATUS_UP);
                stopSelf();
                return;
            }
            Log.i("SportLoadingService", "rootLayout is null");
            return;
        }
        Log.i("SportLoadingService", "lock service error!");
    }

    public void onDestroy() {
        Log.d("SportLoadingService", "onDestroy ---------------!");
        try {
            synchronized (this.mLock) {
                this.mSlptClockClient.unbindService(this.mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
