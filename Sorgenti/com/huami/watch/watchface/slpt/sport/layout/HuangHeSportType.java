package com.huami.watch.watchface.slpt.sport.layout;

public class HuangHeSportType {
    public static int transferSportTypeFromZhuFeng2Huanghe(int sportType) {
        switch (sportType) {
            case 6:
                return 2;
            case 7:
                return 3;
            case 8:
                return 4;
            case 9:
                return 5;
            case 10:
                return 6;
            case 12:
                return 7;
            case 13:
                return 8;
            default:
                return 1;
        }
    }
}
