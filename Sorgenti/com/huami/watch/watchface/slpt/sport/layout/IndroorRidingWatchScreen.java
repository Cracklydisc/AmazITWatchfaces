package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;

public class IndroorRidingWatchScreen extends SportLayoutClock {
    private static int[] diagramList = new int[]{0};
    private static int[] pageList4 = new int[]{3};
    private static int[] pageList6 = new int[]{3};

    public IndroorRidingWatchScreen(Context mContext, boolean isMixSport) {
        super(mContext, 10, SportDataItemInfo.mIndoorRidingInfoOrder, isMixSport);
        CreateSportClock();
    }

    protected int[] getPageList6() {
        return pageList6;
    }

    protected int[] getPageList4() {
        return pageList4;
    }

    protected int[] getDiagramList() {
        return diagramList;
    }

    protected boolean needDistanceDiagram() {
        return false;
    }

    protected int getDefaultDiagram() {
        return 0;
    }
}
