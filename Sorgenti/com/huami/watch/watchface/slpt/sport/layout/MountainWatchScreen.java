package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;

public class MountainWatchScreen extends SportLayoutClock {
    private static int[] diagramList = new int[]{2};
    private static int[] pageList4 = new int[]{4, 4, 3};
    private static int[] pageList6 = new int[]{6, 5};

    public MountainWatchScreen(Context mContext, boolean isMixSport) {
        super(mContext, 13, SportDataItemInfo.mMountaineerInfoOrder, isMixSport);
        CreateSportClock();
    }

    protected int[] getPageList6() {
        return pageList6;
    }

    protected int[] getPageList4() {
        return pageList4;
    }

    protected int[] getDiagramList() {
        return diagramList;
    }

    protected boolean needDistanceDiagram() {
        return true;
    }

    protected int getDefaultDiagram() {
        return 2;
    }
}
