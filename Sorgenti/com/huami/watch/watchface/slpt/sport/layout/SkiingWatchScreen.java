package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;

public class SkiingWatchScreen extends SportLayoutClock {
    private static int[] diagramList = new int[]{0};
    private static int[] pageList4 = new int[]{4, 4, 3};
    private static int[] pageList6 = new int[]{6, 5};

    public SkiingWatchScreen(Context mContext, boolean isMixSport) {
        super(mContext, 11, SportDataItemInfo.mSkiingInfoOrder, isMixSport);
        CreateSportClock();
    }

    protected int[] getPageList6() {
        return pageList6;
    }

    protected int[] getPageList4() {
        return pageList4;
    }

    protected int[] getDiagramList() {
        return diagramList;
    }

    protected boolean needDistanceDiagram() {
        return true;
    }

    protected int getDefaultDiagram() {
        return 4;
    }
}
