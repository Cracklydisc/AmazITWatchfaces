package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.analogyellow.R;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;

public class SportDataItemInfo {
    public static final String[] DATA_ITEM_TILE_ZH = new String[]{"", "用时", "里程", "配速", "平均配速", "心率", "消耗", "当前海拔", "累计上升", "累计下降", "速度", "平均速度", "步频", "累计爬坡", "步数", "踏频", "趟数", "DPS", "划水速率", "平均划水率", "配速", "平均配速", "距离", "TE", "垂直速度"};
    public static int[] mCrossingInfoOrder = new int[]{1, 2, 5, 3, 4, 10, 11, 7, 13, 8, 9, 6};
    public static int[] mEppiticalInfoOrder = new int[]{1, 5, 6};
    public static int[] mIndoorRidingInfoOrder = new int[]{1, 5, 6};
    public static int[] mIndoorRunningInfoOrder = new int[]{1, 2, 3, 5, 4, 6, 10, 11, 12};
    public static final int[] mIndoorSwimingInfoOrder = new int[]{1, 22, 16, 17, 18, 19, 20, 21, 6};
    public static final int[] mMountaineerInfoOrder = new int[]{1, 2, 7, 5, 8, 13, 9, 10, 11, 6, 24};
    public static int[] mOutRidingMixInfoOrder = new int[]{1, 2, 10, 5};
    public static int[] mOutSwimMixInfoOrder = new int[]{1, 22, 20};
    public static int[] mOutdoorRidingInfoOrder = new int[]{1, 2, 5, 10, 11, 7, 13, 8, 9, 6};
    public static final int[] mOutdoorSwimingInfoOrder = new int[]{1, 22, 18, 19, 20, 21, 6};
    public static int[] mRunningInfoOrder = new int[]{1, 2, 5, 3, 4, 12, 10, 11, 7, 13, 8, 9, 6, 23};
    public static int[] mRunningMixInfoOrder = new int[]{1, 2, 3, 5};
    public static final int[] mSkiingInfoOrder = new int[]{1, 10, 26, 30, 27, 28, 29, 7, 5, 6, 25};
    public static final int[] mSoccerInfoOrder = new int[]{1, 2, 6, 5};
    public static final int[] mTennisInfoOrder = new int[]{1, 31, 6, 5};
    public static int[] mWalkingInfoOrder = new int[]{14, 1, 2, 5, 6, 12, 3, 4, 10, 11};

    public static String[] getDateItemArray(Context mContext) {
        return mContext.getResources().getStringArray(R.array.sport_data_item_array);
    }

    public static String[] getTrainDateItemArray(Context mContext) {
        return mContext.getResources().getStringArray(R.array.sport_train_item);
    }

    public static SlptViewComponent getItemView(Context mContext, int id, Typeface sportItemTypeface) {
        return getItemView(mContext, id, sportItemTypeface, null);
    }

    public static SlptViewComponent getItemView(Context mContext, int id, Typeface sportItemTypeface, SportTrainInfo info) {
        switch (id) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                return SportItemInfoWrapper.createSportTimeLayout(mContext, sportItemTypeface);
            case 2:
                return SportItemInfoWrapper.createDistanceView(mContext, sportItemTypeface);
            case 3:
                return SportItemInfoWrapper.createPaceView(mContext, sportItemTypeface);
            case 4:
                return SportItemInfoWrapper.createAvgPaceView(mContext, sportItemTypeface);
            case 5:
                return SportItemInfoWrapper.createHeartrateView(mContext, sportItemTypeface);
            case 6:
                return SportItemInfoWrapper.createCalarieView(mContext, sportItemTypeface);
            case 7:
                return SportItemInfoWrapper.createAltitudeView(mContext, sportItemTypeface);
            case 8:
                return SportItemInfoWrapper.createAscendMeterView(mContext, sportItemTypeface);
            case 9:
                return SportItemInfoWrapper.createDescendMeterView(mContext, sportItemTypeface);
            case 10:
                return SportItemInfoWrapper.createSpeedView(mContext, sportItemTypeface);
            case 11:
                return SportItemInfoWrapper.createAvgSpeedView(mContext, sportItemTypeface);
            case 12:
                return SportItemInfoWrapper.createStepFreqView(mContext, sportItemTypeface);
            case 13:
                return SportItemInfoWrapper.createAscendDistanceView(mContext, sportItemTypeface);
            case 14:
                return SportItemInfoWrapper.createSportStepView(mContext, sportItemTypeface);
            case 16:
                return SportItemInfoWrapper.createSwimTripsView(mContext, sportItemTypeface);
            case 17:
                return SportItemInfoWrapper.createSwimDPSView(mContext, sportItemTypeface);
            case 18:
                return SportItemInfoWrapper.createSwimStrokeSpeedView(mContext, sportItemTypeface);
            case 19:
                return SportItemInfoWrapper.createSwimAvgStrokeSpeedView(mContext, sportItemTypeface);
            case 20:
                return SportItemInfoWrapper.createSwimPaceView(mContext, sportItemTypeface);
            case 21:
                return SportItemInfoWrapper.createSwimAvgPaceView(mContext, sportItemTypeface);
            case 22:
                return SportItemInfoWrapper.createSwimDistanceView(mContext, sportItemTypeface);
            case 23:
                return SportItemInfoWrapper.createTEView(mContext, sportItemTypeface);
            case 24:
                return SportItemInfoWrapper.createVerticalSpeedView(mContext, sportItemTypeface);
            case 25:
                return SportItemInfoWrapper.createDHAvgSpeedView(mContext, sportItemTypeface);
            case 26:
                return SportItemInfoWrapper.createDHMaxSpeedView(mContext, sportItemTypeface);
            case 27:
                return SportItemInfoWrapper.createDHnumsView(mContext, sportItemTypeface);
            case 28:
                return SportItemInfoWrapper.createDHSingleDistanceView(mContext, sportItemTypeface);
            case 29:
                return SportItemInfoWrapper.createDHTotalDistanceView(mContext, sportItemTypeface);
            case 30:
                return SportItemInfoWrapper.createDHSingleElevLossView(mContext, sportItemTypeface);
            case 31:
                return SportItemInfoWrapper.createTennisStrokesView(mContext, sportItemTypeface);
            case 250:
                return SportItemInfoWrapper.createTrainSurplusDistanceView(mContext, SportItemInfoWrapper.getDiagramNum(mContext), info);
            case 251:
                return SportItemInfoWrapper.createTrainTotalDistanceView(mContext, sportItemTypeface, info);
            case 252:
                return SportItemInfoWrapper.createTrainTotalTimeView(mContext, sportItemTypeface, info);
            case 253:
                return SportItemInfoWrapper.createTrainSurplusTimeView(mContext, SportItemInfoWrapper.getDiagramNum(mContext), info);
            case 254:
                return SportItemInfoWrapper.createKeyPressDistanceLayout(mContext, SportItemInfoWrapper.getDiagramNum(mContext));
            default:
                return null;
        }
    }
}
