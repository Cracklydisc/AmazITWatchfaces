package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;
import android.provider.Settings.System;

public class SportGraphUtils {
    public static int getSportDisplayType(Context context, int sportType, int defaultDiagram) {
        return System.getInt(context.getContentResolver(), "sport_display_graph_type_" + sportType, defaultDiagram);
    }
}
