package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;
import android.graphics.Typeface;
import android.provider.Settings.System;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import com.huami.watch.extendsapi.ModelUtil;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.slpt.slptFonts;
import com.huami.watch.watchface.util.TypefaceManager;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptBatteryView;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.sport.SlptAltitudeView;
import com.ingenic.iwds.slpt.view.sport.SlptAscendDistanceView;
import com.ingenic.iwds.slpt.view.sport.SlptAscendMeterView;
import com.ingenic.iwds.slpt.view.sport.SlptAvgPaceMView;
import com.ingenic.iwds.slpt.view.sport.SlptAvgPaceSView;
import com.ingenic.iwds.slpt.view.sport.SlptAvgSpeedMView;
import com.ingenic.iwds.slpt.view.sport.SlptAvgSpeedSView;
import com.ingenic.iwds.slpt.view.sport.SlptCaloriesView;
import com.ingenic.iwds.slpt.view.sport.SlptDHAvgSpeedFView;
import com.ingenic.iwds.slpt.view.sport.SlptDHAvgSpeedLView;
import com.ingenic.iwds.slpt.view.sport.SlptDHMaxSpeedFView;
import com.ingenic.iwds.slpt.view.sport.SlptDHMaxSpeedLView;
import com.ingenic.iwds.slpt.view.sport.SlptDHNumsView;
import com.ingenic.iwds.slpt.view.sport.SlptDHSingleDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptDHSingleDistanceLView;
import com.ingenic.iwds.slpt.view.sport.SlptDHSingleElevLossView;
import com.ingenic.iwds.slpt.view.sport.SlptDHTotalDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptDHTotalDistanceLView;
import com.ingenic.iwds.slpt.view.sport.SlptDescendMeterView;
import com.ingenic.iwds.slpt.view.sport.SlptDistanceFView;
import com.ingenic.iwds.slpt.view.sport.SlptDistanceLView;
import com.ingenic.iwds.slpt.view.sport.SlptFlashPicGroupView;
import com.ingenic.iwds.slpt.view.sport.SlptHeartRateSview;
import com.ingenic.iwds.slpt.view.sport.SlptLatitudeDegreeView;
import com.ingenic.iwds.slpt.view.sport.SlptLatitudeMinuteView;
import com.ingenic.iwds.slpt.view.sport.SlptLatitudeSecondView;
import com.ingenic.iwds.slpt.view.sport.SlptLongitudeDegreeView;
import com.ingenic.iwds.slpt.view.sport.SlptLongitudeMinuteView;
import com.ingenic.iwds.slpt.view.sport.SlptLongitudeSecondView;
import com.ingenic.iwds.slpt.view.sport.SlptPaceMView;
import com.ingenic.iwds.slpt.view.sport.SlptPaceSView;
import com.ingenic.iwds.slpt.view.sport.SlptSpeedMView;
import com.ingenic.iwds.slpt.view.sport.SlptSpeedSView;
import com.ingenic.iwds.slpt.view.sport.SlptSportHPauseView;
import com.ingenic.iwds.slpt.view.sport.SlptSportHTotalView;
import com.ingenic.iwds.slpt.view.sport.SlptSportHView;
import com.ingenic.iwds.slpt.view.sport.SlptSportMPauseView;
import com.ingenic.iwds.slpt.view.sport.SlptSportMTotalView;
import com.ingenic.iwds.slpt.view.sport.SlptSportMView;
import com.ingenic.iwds.slpt.view.sport.SlptSportSPauseView;
import com.ingenic.iwds.slpt.view.sport.SlptSportSTotalView;
import com.ingenic.iwds.slpt.view.sport.SlptSportSView;
import com.ingenic.iwds.slpt.view.sport.SlptSportStepView;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.sport.SlptStepFreqView;
import com.ingenic.iwds.slpt.view.sport.SlptSwimAvgStrokeSpeedView;
import com.ingenic.iwds.slpt.view.sport.SlptSwimDPSFView;
import com.ingenic.iwds.slpt.view.sport.SlptSwimDPSLView;
import com.ingenic.iwds.slpt.view.sport.SlptSwimStrokeSpeedView;
import com.ingenic.iwds.slpt.view.sport.SlptSwimTripsView;
import com.ingenic.iwds.slpt.view.sport.SlptTEFView;
import com.ingenic.iwds.slpt.view.sport.SlptTELView;
import com.ingenic.iwds.slpt.view.sport.SlptTennisStrokesView;
import com.ingenic.iwds.slpt.view.sport.SlptVerticalSpeedFView;
import com.ingenic.iwds.slpt.view.sport.SlptVerticalSpeedLView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.util.Arrays;

public class SportItemInfoWrapper {
    private static final String[] digitalNum = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    public static int getSlptSportItemCount(Context mContext) {
        return System.getInt(mContext.getContentResolver(), "key_sport_lock_source", 6);
    }

    public static int[] parseStringToOrder(String orderStr) {
        if (orderStr == null || orderStr.isEmpty()) {
            Log.e("HmSportListUtil", "order null ");
            return null;
        }
        try {
            String[] orders = orderStr.split(",");
            int[] result = new int[orders.length];
            for (int i = 0; i < orders.length; i++) {
                result[i] = Integer.parseInt(orders[i].trim());
            }
            Log.e("HmSportListUtil", "order string " + orderStr);
            return result;
        } catch (Exception e) {
            Log.e("HmSportListUtil", "parse order string " + orderStr + " failed, return order as null");
            return null;
        }
    }

    public static String getSportItemOrder(Context mContext, int sportType) {
        if (!ModelUtil.isModelHuanghe(mContext)) {
            return System.getString(mContext.getContentResolver(), "sport_item_order" + sportType);
        }
        if (System.getInt(mContext.getContentResolver(), "sport_item_order_version" + sportType, 0) > 0) {
            return System.getString(mContext.getContentResolver(), "sport_item_order" + sportType);
        }
        int huangheSportType = HuangHeSportType.transferSportTypeFromZhuFeng2Huanghe(sportType);
        if (System.getInt(mContext.getContentResolver(), "sport_item_order_version" + huangheSportType, 0) == 1) {
            Log.i("HmSportListUtil", "get sport item order, the setting has been covered by new order");
            return null;
        }
        int[] transferOrders = HuangHeSportInfoOrderManagerWrapper.getTransferSportOrder(sportType, parseStringToOrder(System.getString(mContext.getContentResolver(), "sport_item_order" + huangheSportType)));
        Log.d("HmSportListUtil", "get sport item order_0 " + Arrays.toString(transferOrders) + " for sport type " + sportType + ", " + huangheSportType);
        return Arrays.toString(transferOrders).replaceAll("[\\[\\]]", "");
    }

    public static Typeface getSportRegularTypeface(Context mContext) {
        return TypefaceManager.get().createFromAsset("typeface/huamisport-Regular.otf");
    }

    public static Typeface getSportRegular42Typeface(Context mContext) {
        return TypefaceManager.get().createFromAsset("typeface/huamisport42-Regular.otf");
    }

    public static byte[][] getTitleNum(Context mContext) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        byte[][] num = new byte[10][];
        for (int i = 0; i < 10; i++) {
            num[i] = SimpleFile.readFileFromAssets(mContext, String.format(themeDir + "title-number/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        return num;
    }

    public static byte[][] getTitleNum(Context mContext, String themeDir) {
        byte[][] num = new byte[10][];
        for (int i = 0; i < 10; i++) {
            num[i] = SimpleFile.readFileFromAssets(mContext, String.format(themeDir + "title-number/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        return num;
    }

    public static byte[][] getPauseNum(Context mContext, String themeDir) {
        byte[][] num = new byte[10][];
        for (int i = 0; i < 10; i++) {
            num[i] = SimpleFile.readFileFromAssets(mContext, String.format(themeDir + "pause-num/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        return num;
    }

    public static byte[][] getContentNum(Context mContext) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        byte[][] num = new byte[10][];
        for (int i = 0; i < 10; i++) {
            num[i] = SimpleFile.readFileFromAssets(mContext, String.format(themeDir + "main-number/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        return num;
    }

    public static byte[][] getContentNum(Context mContext, String themeDir) {
        byte[][] num = new byte[10][];
        for (int i = 0; i < 10; i++) {
            num[i] = SimpleFile.readFileFromAssets(mContext, String.format(themeDir + "main-number/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        return num;
    }

    public static byte[][] getDiagramNum(Context mContext) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        byte[][] num = new byte[10][];
        for (int i = 0; i < 10; i++) {
            num[i] = SimpleFile.readFileFromAssets(mContext, String.format(themeDir + "big-number/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        return num;
    }

    public static byte[][] getTrainNum(Context mContext) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        byte[][] num = new byte[10][];
        for (int i = 0; i < 10; i++) {
            num[i] = SimpleFile.readFileFromAssets(mContext, String.format(themeDir + "train_small_num/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        return num;
    }

    public static SlptViewComponent getFontStringView(Context mContext, String title, int fontsize, int color, short space) {
        Typeface font;
        SlptLinearLayout layout = new SlptLinearLayout();
        TypefaceManager typefaceManager = TypefaceManager.get();
        if (fontsize == 21) {
            font = typefaceManager.createFromAsset("typeface/font8c-21.ttf");
        } else if (fontsize == 28) {
            font = typefaceManager.createFromAsset("typeface/font8c-28.ttf");
        } else if (fontsize == 16) {
            font = typefaceManager.createFromAsset("typeface/font8c-16.ttf");
        } else if (fontsize == 14) {
            font = typefaceManager.createFromAsset("typeface/font8c-14.ttf");
        } else {
            Log.d("HmSportListUtil", "error fontsize " + fontsize);
            return null;
        }
        SlptPictureView view = new SlptPictureView();
        view.setStringPicture(title);
        view.setTextAttr((float) fontsize, color, font);
        layout.add(view);
        return layout;
    }

    public static SlptViewComponent getFont16StringView(Context mContext, String title, short space, boolean needBlack) {
        SlptLinearLayout layout = new SlptLinearLayout();
        Typeface font = TypefaceManager.get().createFromAsset("typeface/font8c-16.ttf");
        SlptPictureView view = new SlptPictureView();
        view.setStringPicture(title);
        if (needBlack) {
            view.setTextAttr(16.0f, -16777216, font);
        } else {
            view.setTextAttr(16.0f, -1, font);
        }
        layout.add(view);
        return layout;
    }

    public static SlptViewComponent getFont16StringView(Context mContext, String title, short space) {
        int color;
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            color = -1;
        } else {
            color = -16777216;
        }
        return (SlptLinearLayout) getFontStringView(mContext, title, 16, color, space);
    }

    public static SlptViewComponent getFont16StringViewBlack(Context mContext, String title, short space) {
        return (SlptLinearLayout) getFontStringView(mContext, title, 16, -1, space);
    }

    public static SlptViewComponent getFont21StringView(Context mContext, String title, short space) {
        int color;
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            color = -1;
        } else {
            color = -16777216;
        }
        return (SlptLinearLayout) getFontStringView(mContext, title, 21, color, space);
    }

    public static SlptViewComponent getFont21StringViewBlack(Context mContext, String title, short space) {
        return (SlptLinearLayout) getFontStringView(mContext, title, 21, -1, space);
    }

    public static SlptViewComponent getFont21StringViewGreen(Context mContext, String title, short space) {
        int color;
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            color = -16711936;
        } else {
            color = -65536;
        }
        return (SlptLinearLayout) getFontStringView(mContext, title, 21, color, space);
    }

    public static SlptViewComponent getFont28StringViewBlack(Context mContext, String title, short space) {
        return (SlptLinearLayout) getFontStringView(mContext, title, 28, -1, space);
    }

    public static SlptViewComponent getFont28StringView(Context mContext, String title, short space) {
        int color;
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            color = -1;
        } else {
            color = -16777216;
        }
        return (SlptLinearLayout) getFontStringView(mContext, title, 28, color, space);
    }

    public static SlptViewComponent createDistanceView(Context mContext, Typeface font) {
        SlptDistanceFView distanceFView = new SlptDistanceFView();
        SlptDistanceLView distanceLView = new SlptDistanceLView();
        SlptPictureView distanceSepView = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(distanceFView);
        layout.add(distanceSepView);
        layout.add(distanceLView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        distanceSepView.setStringPicture(".");
        SlptSportUtil.setDistanceSeparatorView(distanceSepView);
        return layout;
    }

    public static SlptViewComponent createDHSingleDistanceView(Context mContext, Typeface font) {
        SlptDHSingleDistanceFView distanceFView = new SlptDHSingleDistanceFView();
        SlptDHSingleDistanceLView distanceLView = new SlptDHSingleDistanceLView();
        SlptPictureView distanceSepView = new SlptPictureView();
        SlptPictureView down = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(distanceFView);
        layout.add(distanceSepView);
        layout.add(distanceLView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        distanceSepView.setStringPicture(".");
        SlptSportUtil.setDHSingleDistanceSepView(distanceSepView);
        down.setImagePicture(SimpleFile.readFileFromAssets(mContext, SportLayoutTheme.getSlptThemesDir(mContext, false) + "bg/down.png"));
        layout.add(down);
        down.alignY = (byte) 1;
        return layout;
    }

    public static SlptViewComponent createDHTotalDistanceView(Context mContext, Typeface font) {
        SlptDHTotalDistanceFView distanceFView = new SlptDHTotalDistanceFView();
        SlptDHTotalDistanceLView distanceLView = new SlptDHTotalDistanceLView();
        SlptPictureView distanceSepView = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(distanceFView);
        layout.add(distanceSepView);
        layout.add(distanceLView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        distanceSepView.setStringPicture(".");
        SlptSportUtil.setDHTotalDistanceSepView(distanceSepView);
        return layout;
    }

    public static SlptViewComponent createKeyPressDistanceLayout(Context mContext, byte[][] num) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptDistanceFView distanceFView = new SlptDistanceFView();
        SlptDistanceLView distanceLView = new SlptDistanceLView();
        SlptPictureView distanceSepView = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(distanceFView);
        layout.add(distanceSepView);
        layout.add(distanceLView);
        distanceFView.setImagePictureArray(num);
        distanceLView.setImagePictureArray(num);
        distanceSepView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/dot.png"));
        SlptSportUtil.setDistanceSeparatorView(distanceSepView);
        return layout;
    }

    public static SlptViewComponent createTrainSurplusDistanceView(Context mContext, byte[][] num, SportTrainInfo info) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptDistanceFView distanceFView = new SlptDistanceFView();
        SlptDistanceLView distanceLView = new SlptDistanceLView();
        SlptPictureView distanceSepView = new SlptPictureView();
        distanceFView.setTrain_distance(info.getCurrentDistance());
        distanceLView.setTrain_distance(info.getCurrentDistance());
        distanceFView.setStart_distance(info.getStartDistance());
        distanceLView.setStart_distance(info.getStartDistance());
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(distanceFView);
        layout.add(distanceSepView);
        layout.add(distanceLView);
        distanceFView.setImagePictureArray(num);
        distanceLView.setImagePictureArray(num);
        distanceSepView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/dot.png"));
        SlptSportUtil.setDistanceSeparatorView(distanceSepView);
        return layout;
    }

    public static SlptViewComponent createTrainTotalDistanceView(Context mContext, Typeface font, SportTrainInfo info) {
        String unit;
        SlptDistanceFView distanceFView = new SlptDistanceFView();
        SlptDistanceLView distanceLView = new SlptDistanceLView();
        SlptPictureView distanceSepView = new SlptPictureView();
        SlptPictureView splashView = new SlptPictureView();
        SlptPictureView nullView = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(distanceFView);
        layout.add(distanceSepView);
        layout.add(distanceLView);
        layout.add(splashView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -16711936, font);
        } else {
            layout.setTextAttrForAll(100.0f, -65536, font);
        }
        Log.d("HmSportListUtil", "format distance " + SportTrainInfo.formatDistance(Util.getMeasurement(mContext), info.getTotalDistance()));
        nullView.setStringPicture(" ");
        distanceSepView.setStringPicture(".");
        splashView.setStringPicture("/" + SportTrainInfo.formatDistance(Util.getMeasurement(mContext), info.getTotalDistance()) + " ");
        splashView.padding.left = (short) 7;
        nullView.padding.right = (short) mContext.getResources().getInteger(R.integer.train_total_distance_null_pading_right);
        SlptSportUtil.setDistanceSeparatorView(distanceSepView);
        if (Util.getMeasurement(mContext) == 1) {
            unit = mContext.getResources().getString(R.string.mile);
        } else {
            unit = mContext.getResources().getString(R.string.kilometre);
        }
        SlptLinearLayout stringLayout = (SlptLinearLayout) getFont16StringView(mContext, unit, (short) 0);
        layout.add(stringLayout);
        stringLayout.padding.top = (short) mContext.getResources().getInteger(R.integer.train_total_unit_pading_top);
        return layout;
    }

    public static SlptViewComponent createSwimDistanceView(Context mContext, Typeface font) {
        SlptDistanceFView distanceFView = new SlptDistanceFView();
        SlptDistanceLView distanceLView = new SlptDistanceLView();
        SlptPictureView distanceSepView = new SlptPictureView();
        distanceFView.setUnit(1);
        distanceLView.setUnit(1);
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(distanceFView);
        layout.add(distanceSepView);
        layout.add(distanceLView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        distanceSepView.setStringPicture(".");
        SlptSportUtil.setDistanceSeparatorView(distanceSepView);
        return layout;
    }

    public static SlptViewComponent createTEView(Context mContext, Typeface font) {
        SlptTEFView tefView = new SlptTEFView();
        SlptTELView telView = new SlptTELView();
        SlptPictureView SepView = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(tefView);
        layout.add(SepView);
        layout.add(telView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        SepView.setStringPicture(".");
        return layout;
    }

    public static SlptViewComponent createPaceView(Context mContext, Typeface font) {
        SlptPaceMView fView = new SlptPaceMView();
        SlptPaceSView lView = new SlptPaceSView();
        SlptPictureView seqView1 = new SlptPictureView();
        SlptPictureView seqView2 = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        layout.add(seqView1);
        layout.add(lView);
        layout.add(seqView2);
        SlptPictureView defaultView = new SlptPictureView();
        layout.show = false;
        defaultView.show = true;
        SlptSportUtil.setPaceLayoutView(layout);
        SlptSportUtil.setPaceDefaultView(defaultView);
        SlptAbsoluteLayout layoutView = new SlptAbsoluteLayout();
        layoutView.add(defaultView);
        layoutView.add(layout);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layoutView.setTextAttrForAll(100.0f, -1, font);
        } else {
            layoutView.setTextAttrForAll(100.0f, -16777216, font);
        }
        seqView1.setStringPicture("'");
        seqView2.setStringPicture("\"");
        defaultView.setStringPicture("--");
        return layoutView;
    }

    public static SlptViewComponent createSwimPaceView(Context mContext, Typeface font) {
        SlptPaceMView fView = new SlptPaceMView();
        SlptPaceSView lView = new SlptPaceSView();
        SlptPictureView seqView1 = new SlptPictureView();
        SlptPictureView seqView2 = new SlptPictureView();
        fView.setUnit(1);
        lView.setUnit(1);
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        layout.add(seqView1);
        layout.add(lView);
        layout.add(seqView2);
        SlptPictureView defaultView = new SlptPictureView();
        layout.show = false;
        defaultView.show = true;
        SlptSportUtil.setPaceLayoutView(layout);
        SlptSportUtil.setPaceDefaultView(defaultView);
        SlptAbsoluteLayout layoutView = new SlptAbsoluteLayout();
        layoutView.add(layout);
        layoutView.add(defaultView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layoutView.setTextAttrForAll(100.0f, -1, font);
        } else {
            layoutView.setTextAttrForAll(100.0f, -16777216, font);
        }
        seqView1.setStringPicture("'");
        seqView2.setStringPicture("\"");
        defaultView.setStringPicture("--");
        return layoutView;
    }

    public static SlptViewComponent createAvgPaceView(Context mContext, Typeface font) {
        SlptAvgPaceMView fView = new SlptAvgPaceMView();
        SlptAvgPaceSView lView = new SlptAvgPaceSView();
        SlptPictureView seqView1 = new SlptPictureView();
        SlptPictureView seqView2 = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        layout.add(seqView1);
        layout.add(lView);
        layout.add(seqView2);
        SlptPictureView defaultView = new SlptPictureView();
        layout.show = false;
        defaultView.show = true;
        SlptSportUtil.setAvgPaceLayoutView(layout);
        SlptSportUtil.setAvgPaceDefaultView(defaultView);
        SlptAbsoluteLayout layoutView = new SlptAbsoluteLayout();
        layoutView.add(defaultView);
        layoutView.add(layout);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layoutView.setTextAttrForAll(100.0f, -1, font);
        } else {
            layoutView.setTextAttrForAll(100.0f, -16777216, font);
        }
        seqView1.setStringPicture("'");
        seqView2.setStringPicture("\"");
        defaultView.setStringPicture("--");
        return layoutView;
    }

    public static SlptViewComponent createSwimAvgPaceView(Context mContext, Typeface font) {
        SlptAvgPaceMView fView = new SlptAvgPaceMView();
        SlptAvgPaceSView lView = new SlptAvgPaceSView();
        SlptPictureView seqView1 = new SlptPictureView();
        SlptPictureView seqView2 = new SlptPictureView();
        fView.setUnit(1);
        lView.setUnit(1);
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        layout.add(seqView1);
        layout.add(lView);
        layout.add(seqView2);
        SlptPictureView defaultView = new SlptPictureView();
        layout.show = false;
        defaultView.show = true;
        SlptSportUtil.setAvgPaceLayoutView(layout);
        SlptSportUtil.setAvgPaceDefaultView(defaultView);
        SlptAbsoluteLayout layoutView = new SlptAbsoluteLayout();
        layoutView.add(defaultView);
        layoutView.add(layout);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layoutView.setTextAttrForAll(100.0f, -1, font);
        } else {
            layoutView.setTextAttrForAll(100.0f, -16777216, font);
        }
        seqView1.setStringPicture("'");
        seqView2.setStringPicture("\"");
        defaultView.setStringPicture("--");
        return layoutView;
    }

    public static SlptViewComponent createSpeedView(Context mContext, Typeface font) {
        SlptSpeedMView fView = new SlptSpeedMView();
        SlptSpeedSView lView = new SlptSpeedSView();
        SlptPictureView seqView1 = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        layout.add(seqView1);
        layout.add(lView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        seqView1.setStringPicture(".");
        SlptSportUtil.setSpeedSeparatorView(seqView1);
        return layout;
    }

    public static SlptViewComponent createVerticalSpeedView(Context mContext, Typeface font) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptVerticalSpeedFView fView = new SlptVerticalSpeedFView();
        SlptVerticalSpeedLView lView = new SlptVerticalSpeedLView();
        SlptPictureView seqView1 = new SlptPictureView();
        SlptPictureView up = new SlptPictureView();
        SlptPictureView down = new SlptPictureView();
        up.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "bg/up.png"));
        SlptSportUtil.setVerticalSpeedUp(up);
        down.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "bg/down.png"));
        SlptSportUtil.setVerticalSpeedDown(down);
        SlptLinearLayout layout = new SlptLinearLayout();
        down.padding.right = (short) 2;
        layout.add(fView);
        layout.add(seqView1);
        layout.add(lView);
        layout.add(up);
        layout.add(down);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        seqView1.setStringPicture(".");
        SlptSportUtil.setVerticalSpeedSeparatorView(seqView1);
        return layout;
    }

    public static SlptViewComponent createAvgSpeedView(Context mContext, Typeface font) {
        SlptAvgSpeedMView fView = new SlptAvgSpeedMView();
        SlptAvgSpeedSView lView = new SlptAvgSpeedSView();
        SlptPictureView seqView1 = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        layout.add(seqView1);
        layout.add(lView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        seqView1.setStringPicture(".");
        SlptSportUtil.setAvgSpeedSeparatorView(seqView1);
        return layout;
    }

    public static SlptViewComponent createDHAvgSpeedView(Context mContext, Typeface font) {
        SlptDHAvgSpeedFView fView = new SlptDHAvgSpeedFView();
        SlptDHAvgSpeedLView lView = new SlptDHAvgSpeedLView();
        SlptPictureView seqView1 = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        layout.add(seqView1);
        layout.add(lView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        seqView1.setStringPicture(".");
        SlptSportUtil.setDHAvgSpeedSepView(seqView1);
        return layout;
    }

    public static SlptViewComponent createDHMaxSpeedView(Context mContext, Typeface font) {
        SlptDHMaxSpeedFView fView = new SlptDHMaxSpeedFView();
        SlptDHMaxSpeedLView lView = new SlptDHMaxSpeedLView();
        SlptPictureView seqView1 = new SlptPictureView();
        SlptPictureView down = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        layout.add(seqView1);
        layout.add(lView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        seqView1.setStringPicture(".");
        SlptSportUtil.setDHMaxSpeedSepView(seqView1);
        down.setImagePicture(SimpleFile.readFileFromAssets(mContext, SportLayoutTheme.getSlptThemesDir(mContext, false) + "bg/down.png"));
        layout.add(down);
        down.alignY = (byte) 1;
        return layout;
    }

    public static SlptViewComponent createStepFreqView(Context mContext, Typeface font) {
        SlptStepFreqView fView = new SlptStepFreqView();
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            fView.setTextAttr(100.0f, -1, font);
        } else {
            fView.setTextAttr(100.0f, -16777216, font);
        }
        return fView;
    }

    public static SlptViewComponent createSportStepView(Context mContext, Typeface font) {
        SlptSportStepView fView = new SlptSportStepView();
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            fView.setTextAttr(100.0f, -1, font);
        } else {
            fView.setTextAttr(100.0f, -16777216, font);
        }
        return fView;
    }

    public static SlptViewComponent createSwimTripsView(Context mContext, Typeface font) {
        SlptSwimTripsView fView = new SlptSwimTripsView();
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            fView.setTextAttr(100.0f, -1, font);
        } else {
            fView.setTextAttr(100.0f, -16777216, font);
        }
        return fView;
    }

    public static SlptViewComponent createSwimStrokeSpeedView(Context mContext, Typeface font) {
        SlptSwimStrokeSpeedView fView = new SlptSwimStrokeSpeedView();
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            fView.setTextAttr(100.0f, -1, font);
        } else {
            fView.setTextAttr(100.0f, -16777216, font);
        }
        return fView;
    }

    public static SlptViewComponent createSwimAvgStrokeSpeedView(Context mContext, Typeface font) {
        SlptSwimAvgStrokeSpeedView fView = new SlptSwimAvgStrokeSpeedView();
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            fView.setTextAttr(100.0f, -1, font);
        } else {
            fView.setTextAttr(100.0f, -16777216, font);
        }
        return fView;
    }

    public static SlptViewComponent createSwimDPSView(Context mContext, Typeface font) {
        SlptSwimDPSFView dpsfView = new SlptSwimDPSFView();
        SlptSwimDPSLView dpslView = new SlptSwimDPSLView();
        SlptPictureView distanceSepView = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(dpsfView);
        layout.add(distanceSepView);
        layout.add(dpslView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        distanceSepView.setStringPicture(".");
        return layout;
    }

    public static SlptViewComponent createHeartrateView(Context mContext, Typeface font) {
        SlptLinearLayout layout = new SlptLinearLayout();
        SlptHeartRateSview fView = new SlptHeartRateSview();
        SlptPictureView defaultView = new SlptPictureView();
        SlptSportUtil.setHeartRateInvalidView(defaultView);
        fView.show = false;
        layout.add(fView);
        layout.add(defaultView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        defaultView.setStringPicture("--");
        return layout;
    }

    public static SlptViewComponent createCalarieView(Context mContext, Typeface font) {
        SlptCaloriesView fView = new SlptCaloriesView();
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            fView.setTextAttr(100.0f, -1, font);
        } else {
            fView.setTextAttr(100.0f, -16777216, font);
        }
        return fView;
    }

    public static SlptViewComponent createTennisStrokesView(Context mContext, Typeface font) {
        SlptTennisStrokesView fView = new SlptTennisStrokesView();
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            fView.setTextAttr(100.0f, -1, font);
        } else {
            fView.setTextAttr(100.0f, -16777216, font);
        }
        return fView;
    }

    public static SlptViewComponent createDHnumsView(Context mContext, Typeface font) {
        SlptDHNumsView fView = new SlptDHNumsView();
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            fView.setTextAttr(100.0f, -1, font);
        } else {
            fView.setTextAttr(100.0f, -16777216, font);
        }
        return fView;
    }

    public static SlptViewComponent createDHSingleElevLossView(Context mContext, Typeface font) {
        SlptLinearLayout layout = new SlptLinearLayout();
        SlptDHSingleElevLossView fView = new SlptDHSingleElevLossView();
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            fView.setTextAttr(100.0f, -1, font);
        } else {
            fView.setTextAttr(100.0f, -16777216, font);
        }
        SlptPictureView down = new SlptPictureView();
        down.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "bg/down.png"));
        layout.add(fView);
        layout.add(down);
        down.alignY = (byte) 1;
        return layout;
    }

    public static SlptViewComponent createAltitudeView(Context mContext, Typeface font) {
        SlptAltitudeView fView = new SlptAltitudeView();
        SlptPictureView negativeView = new SlptPictureView();
        SlptPictureView invalidView = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(negativeView);
        layout.add(invalidView);
        layout.add(fView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        negativeView.setStringPicture("-");
        SlptSportUtil.setAltitudeNegativeView(negativeView);
        invalidView.setStringPicture("--");
        SlptSportUtil.setAltitudeInvalidView(invalidView);
        return layout;
    }

    public static SlptViewComponent createAscendMeterView(Context mContext, Typeface font) {
        SlptAscendMeterView fView = new SlptAscendMeterView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        return layout;
    }

    public static SlptViewComponent createAscendDistanceView(Context mContext, Typeface font) {
        SlptAscendDistanceView fView = new SlptAscendDistanceView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        return layout;
    }

    public static SlptViewComponent createDescendMeterView(Context mContext, Typeface font) {
        SlptDescendMeterView fView = new SlptDescendMeterView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(fView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        return layout;
    }

    public static SlptViewComponent createBackGroudLayout(Context mContext, int size, boolean isMixed) {
        SlptPictureView slptBgView = new SlptPictureView();
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        byte[] slptBgMem;
        switch (size) {
            case 2:
                slptBgView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "bg/2.png"));
                return slptBgView;
            case 3:
                slptBgView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "bg/3.png"));
                return slptBgView;
            case 4:
                if (isMixed) {
                    slptBgMem = SimpleFile.readFileFromAssets(mContext, themeDir + "bg/sport_triathlon_bg_4.png");
                } else {
                    slptBgMem = SimpleFile.readFileFromAssets(mContext, themeDir + "bg/4.png");
                }
                slptBgView.setImagePicture(slptBgMem);
                return slptBgView;
            case 5:
                if (isMixed) {
                    slptBgMem = SimpleFile.readFileFromAssets(mContext, themeDir + "bg/sport_triathlon_bg_5.png");
                } else {
                    slptBgMem = SimpleFile.readFileFromAssets(mContext, themeDir + "bg/5.png");
                }
                slptBgView.setImagePicture(slptBgMem);
                return slptBgView;
            case 6:
                slptBgView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "bg/6.png"));
                return slptBgView;
            default:
                return null;
        }
    }

    public static SlptViewComponent SystemTimeLayout(Context mContext, byte[][] num, boolean needLockView, boolean autoTheme) {
        String themeDir;
        SlptPictureView lockView = new SlptPictureView();
        SlptPictureView amView = new SlptPictureView();
        SlptPictureView pmView = new SlptPictureView();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptViewComponent timeLayout = new SlptLinearLayout();
        SlptPictureView timeSepView = new SlptPictureView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        if (autoTheme) {
            themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        } else {
            themeDir = "sport/black/zh/";
        }
        SlptViewComponent batteryView = getBatteryView(mContext, themeDir);
        timeSepView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "title-number/colon.png"));
        if (needLockView) {
            lockView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "bg/sport_lock.png"));
        }
        amView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "title-number/am.png"));
        pmView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "title-number/pm.png"));
        hourHView.setImagePictureArray(num);
        hourLView.setImagePictureArray(num);
        minuteHView.setImagePictureArray(num);
        minuteLView.setImagePictureArray(num);
        timeLayout.add(hourHView);
        timeLayout.add(hourLView);
        timeLayout.add(timeSepView);
        timeLayout.add(minuteHView);
        timeLayout.add(minuteLView);
        SlptSportUtil.setAmBgView(amView);
        SlptSportUtil.setPmBgView(pmView);
        lockView.setStart(80, 31);
        amView.setStart(111, 30);
        pmView.setStart(111, 30);
        timeLayout.setStart(135, 31);
        batteryView.setStart(214, 35);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        if (needLockView) {
            layout.add(lockView);
        }
        layout.add(amView);
        layout.add(pmView);
        layout.add(timeLayout);
        layout.add(batteryView);
        return layout;
    }

    public static SlptViewComponent createSystemTimeLayout(Context mContext, byte[][] num) {
        return SystemTimeLayout(mContext, num, true, true);
    }

    public static SlptViewComponent createSystemTimeLayoutBlack(Context mContext, byte[][] num) {
        return SystemTimeLayout(mContext, num, true, false);
    }

    public static SlptLayout createSportTrainTips(Context mContext, int id, byte[][] num, SportTrainInfo info) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        slptFonts font = new slptFonts();
        font.addChar('.', SimpleFile.readFileFromAssets(mContext, themeDir + "train_small_num/point.png"));
        font.addChar(':', SimpleFile.readFileFromAssets(mContext, themeDir + "train_small_num/colon.png"));
        font.setNumFonts(getTrainNum(mContext));
        SlptLinearLayout layout = new SlptLinearLayout();
        SlptLinearLayout value;
        if (id == 250) {
            SlptLinearLayout StringLayout2;
            SlptLinearLayout StringLayout1 = (SlptLinearLayout) getFont21StringViewGreen(mContext, mContext.getResources().getString(R.string.need_run_kilometer), (short) 0);
            value = (SlptLinearLayout) font.getStringView(SportTrainInfo.formatDistance(Util.getMeasurement(mContext), info.getCurrentDistance()));
            if (Util.getMeasurement(mContext) == 1) {
                StringLayout2 = (SlptLinearLayout) getFont21StringViewGreen(mContext, mContext.getResources().getString(R.string.mile), (short) 0);
            } else {
                StringLayout2 = (SlptLinearLayout) getFont21StringViewGreen(mContext, mContext.getResources().getString(R.string.kilometre), (short) 0);
            }
            layout.add(StringLayout1);
            layout.add(value);
            layout.add(StringLayout2);
        } else if (id == 253) {
            value = (SlptLinearLayout) font.getStringView(SportTrainInfo.formatCurrentTime(info.getCurrentTime()));
            layout.add((SlptLinearLayout) getFont21StringViewGreen(mContext, mContext.getResources().getString(R.string.need_run_time), (short) 0));
            layout.add(value);
        } else {
            layout.add((SlptLinearLayout) getFont21StringViewGreen(mContext, mContext.getResources().getString(R.string.press_key_finish_train), (short) 0));
        }
        return layout;
    }

    public static SlptLayout createMixSportTitle(Context mContext, byte[][] num) {
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptViewComponent titleLayout = new SlptLinearLayout();
        SlptPictureView iconView = new SlptPictureView();
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptViewComponent batteryView = getBatteryView(mContext, themeDir);
        SlptSportHTotalView sportHView = new SlptSportHTotalView();
        SlptSportMTotalView sportMView = new SlptSportMTotalView();
        SlptSportSTotalView sportSView = new SlptSportSTotalView();
        SlptPictureView timeSeqView1 = new SlptPictureView();
        SlptPictureView timeSeqView2 = new SlptPictureView();
        SlptLinearLayout sportTimelayout = new SlptLinearLayout();
        titleLayout.add(getFont16StringView(mContext, mContext.getResources().getString(R.string.sport_total_time), (short) 0));
        titleLayout.setStart(0, 13);
        titleLayout.setRect(320, 24);
        titleLayout.alignX = (byte) 2;
        iconView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "bg/sport_lock.png"));
        timeSeqView1.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "title-number/colon.png"));
        timeSeqView2.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "title-number/colon.png"));
        sportHView.setImagePictureArray(num);
        sportMView.setImagePictureArray(num);
        sportSView.setImagePictureArray(num);
        sportTimelayout.add(sportHView);
        sportTimelayout.add(timeSeqView1);
        sportTimelayout.add(sportMView);
        sportTimelayout.add(timeSeqView2);
        sportTimelayout.add(sportSView);
        iconView.setStart(70, 34);
        sportTimelayout.setStart(121, 31);
        sportTimelayout.setRect(79, 27);
        batteryView.setStart(214, 31);
        layout.add(batteryView);
        layout.add(titleLayout);
        layout.add(iconView);
        layout.add(sportTimelayout);
        return layout;
    }

    public static SlptLayout createSportChangeValue(Context mContext, byte[][] num) {
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptLinearLayout stringLayout = new SlptLinearLayout();
        SlptLinearLayout stringLayout1 = new SlptLinearLayout();
        SlptPictureView icon = new SlptPictureView();
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptSportHPauseView sportHView = new SlptSportHPauseView();
        SlptSportMPauseView sportMView = new SlptSportMPauseView();
        SlptSportSPauseView sportSView = new SlptSportSPauseView();
        SlptPictureView timeSeqView1 = new SlptPictureView();
        SlptPictureView timeSeqView2 = new SlptPictureView();
        SlptLinearLayout sportTimelayout = new SlptLinearLayout();
        byte[] seqMem1 = SimpleFile.readFileFromAssets(mContext, themeDir + "main-number/colon.png");
        timeSeqView1.setImagePicture(seqMem1);
        timeSeqView2.setImagePicture(seqMem1);
        sportHView.setImagePictureArray(num);
        sportMView.setImagePictureArray(num);
        sportSView.setImagePictureArray(num);
        sportTimelayout.add(sportHView);
        sportTimelayout.add(timeSeqView1);
        sportTimelayout.add(sportMView);
        sportTimelayout.add(timeSeqView2);
        sportTimelayout.add(sportSView);
        sportTimelayout.setStart(0, 135);
        sportTimelayout.setRect(320, 2147483646);
        sportTimelayout.alignX = (byte) 2;
        if (Util.needEnAssets()) {
            stringLayout.add(getFont16StringView(mContext, mContext.getResources().getString(R.string.sport_change), (short) 0));
            stringLayout.setStart(140, 79);
            stringLayout.setStart(0, 79);
            stringLayout.setRect(320, 2147483646);
            stringLayout.alignX = (byte) 2;
        } else {
            stringLayout.add(getFont21StringView(mContext, mContext.getResources().getString(R.string.sport_change), (short) 0));
            stringLayout.setStart(140, 79);
        }
        stringLayout1.add(getFont16StringView(mContext, mContext.getResources().getString(R.string.sport_change_time), (short) 0));
        stringLayout1.setStart(0, 187);
        stringLayout1.setRect(320, 21);
        stringLayout1.alignX = (byte) 2;
        icon.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "bg/sport_8c_icon_right_red.png"));
        icon.setStart(272, 79);
        layout.add(sportTimelayout);
        layout.add(stringLayout);
        layout.add(stringLayout1);
        layout.add(icon);
        return layout;
    }

    public static SlptLayout createSportChangeBottom(Context mContext, String iconPath, String flagString) {
        SlptPictureView iconView = new SlptPictureView();
        SlptLinearLayout stringLayout = new SlptLinearLayout();
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        iconView.setImagePicture(SimpleFile.readFileFromAssets(mContext, iconPath));
        iconView.setStart(213, 61);
        stringLayout.add(getFont28StringView(mContext, flagString, (short) 0));
        stringLayout.setStart(0, 230);
        stringLayout.setRect(320, 32);
        stringLayout.alignX = (byte) 2;
        layout.add(iconView);
        layout.add(stringLayout);
        return layout;
    }

    public static SlptLayout createMixSportFlag(Context mContext, String iconPath, String flagString) {
        SlptPictureView iconView = new SlptPictureView();
        SlptLinearLayout stringLayout = new SlptLinearLayout();
        SlptLinearLayout layout = new SlptLinearLayout();
        iconView.setImagePicture(SimpleFile.readFileFromAssets(mContext, iconPath));
        stringLayout.add(getFont21StringView(mContext, flagString, (short) 0));
        layout.setStart(0, 230);
        layout.setRect(320, 61);
        layout.alignX = (byte) 2;
        layout.alignY = (byte) 2;
        layout.add(iconView);
        layout.add(stringLayout);
        return layout;
    }

    public static SlptViewComponent createSportTimeLayout(Context mContext, Typeface font) {
        SlptSportHView sportHView = new SlptSportHView();
        SlptSportMView sportMView = new SlptSportMView();
        SlptSportSView sportSView = new SlptSportSView();
        SlptPictureView timeSeqView1 = new SlptPictureView();
        SlptPictureView timeSeqView2 = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(sportHView);
        layout.add(timeSeqView1);
        layout.add(sportMView);
        layout.add(timeSeqView2);
        layout.add(sportSView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layout.setTextAttrForAll(100.0f, -16777216, font);
        }
        timeSeqView1.setStringPicture(":");
        timeSeqView2.setStringPicture(":");
        return layout;
    }

    public static SlptViewComponent createTrainSurplusTimeView(Context mContext, byte[][] num, SportTrainInfo info) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptSportMView sportMView = new SlptSportMView();
        SlptSportSView sportSView = new SlptSportSView();
        SlptPictureView timeSeqView2 = new SlptPictureView();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(sportMView);
        layout.add(timeSeqView2);
        layout.add(sportSView);
        sportMView.setTrain_time(info.getCurrentTime());
        sportSView.setTrain_time(info.getCurrentTime());
        sportMView.setStart_time(info.getStartTime());
        sportSView.setStart_time(info.getStartTime());
        sportMView.setImagePictureArray(num);
        sportSView.setImagePictureArray(num);
        timeSeqView2.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/colon.png"));
        return layout;
    }

    public static SlptViewComponent createTrainTotalTimeView(Context mContext, Typeface font, SportTrainInfo info) {
        SlptSportMView sportMView = new SlptSportMView();
        SlptSportSView sportSView = new SlptSportSView();
        SlptPictureView splashView = new SlptPictureView();
        SlptPictureView nullView = new SlptPictureView();
        SlptLinearLayout timeSeqView1 = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.minute) + "   ", (short) 0);
        SlptLinearLayout timeSeqView2 = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.second) + "   ", (short) 0);
        sportMView.setMinute_with_hour();
        SlptLinearLayout layout = new SlptLinearLayout();
        layout.add(sportMView);
        layout.add(timeSeqView1);
        layout.add(sportSView);
        layout.add(timeSeqView2);
        layout.add(splashView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            sportMView.setTextAttr(100.0f, -16711936, font);
            sportSView.setTextAttr(100.0f, -16711936, font);
            splashView.setTextAttr(100.0f, -16711936, font);
        } else {
            sportMView.setTextAttr(100.0f, -65536, font);
            sportSView.setTextAttr(100.0f, -65536, font);
            splashView.setTextAttr(100.0f, -65536, font);
        }
        nullView.setStringPicture(" ");
        splashView.setStringPicture("/ " + SportTrainInfo.formatTotalTime(info.getTotalTime()) + " ");
        nullView.padding.right = (short) mContext.getResources().getInteger(R.integer.train_total_time_null_pading_right);
        SlptLinearLayout stringLayout = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.minute_clock), (short) 0);
        layout.add(stringLayout);
        timeSeqView1.padding.top = (short) mContext.getResources().getInteger(R.integer.train_total_unit_pading_top);
        timeSeqView2.padding.top = (short) mContext.getResources().getInteger(R.integer.train_total_unit_pading_top);
        stringLayout.padding.top = (short) mContext.getResources().getInteger(R.integer.train_total_unit_pading_top);
        return layout;
    }

    public static SlptViewComponent getBatteryView(Context mContext, String themeDir) {
        byte[][] battery_num = new byte[14][];
        SlptBatteryView view = new SlptBatteryView(14);
        for (int i = 0; i < 14; i++) {
            battery_num[i] = SimpleFile.readFileFromAssets(mContext, String.format(themeDir + "battery/widget_battery_%d.png", new Object[]{Integer.valueOf(i)}));
        }
        view.setImagePictureArray(battery_num);
        return view;
    }

    public static SlptClock createGpsReadyClock(Context mContext, String title, String IconPath) {
        SlptLinearLayout contentLayout2;
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptPictureView bg = new SlptPictureView();
        bg.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_8c_bg.png"));
        SlptViewComponent titleLayout = (SlptLinearLayout) getFont28StringViewBlack(mContext, title, (short) 0);
        titleLayout.setStart(0, 31);
        titleLayout.setRect(320, 32);
        titleLayout.alignX = (byte) 2;
        SlptPictureView icon = new SlptPictureView();
        icon.setImagePicture(SimpleFile.readFileFromAssets(mContext, IconPath));
        icon.setStart(120, 110);
        SlptPictureView gpsView = new SlptPictureView();
        gpsView.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_gps_ready_8c.png"));
        SlptPictureView lockIcon = new SlptPictureView();
        lockIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_lock.png"));
        SlptLinearLayout contentLayout = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.gps_ready), (short) 0);
        if (ModelUtil.isRealModelEverest(mContext)) {
            contentLayout2 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_any_key_wakeup), (short) 0);
        } else {
            contentLayout2 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_power_key_wakeup), (short) 0);
        }
        layout.add(bg);
        layout.add(titleLayout);
        layout.add(icon);
        if (Util.needEnAssets()) {
            SlptLinearLayout layoutBottom = new SlptLinearLayout();
            layoutBottom.add(lockIcon);
            layoutBottom.add(contentLayout2);
            lockIcon.padding.right = (short) 8;
            layoutBottom.setStart(0, 237);
            layoutBottom.setRect(320, 2147483646);
            layoutBottom.alignX = (byte) 2;
            layout.add(layoutBottom);
            SlptLinearLayout layoutGps = new SlptLinearLayout();
            layoutGps.add(gpsView);
            layoutGps.add(contentLayout);
            gpsView.padding.right = (short) 8;
            layoutGps.setStart(0, 66);
            layoutGps.setRect(320, 35);
            layoutGps.alignX = (byte) 2;
            layoutGps.alignY = (byte) 2;
            layout.add(layoutGps);
        } else {
            gpsView.setStart(84, 66);
            contentLayout.setStart(126, 71);
            lockIcon.setStart(78, 238);
            contentLayout2.setStart(116, 237);
            layout.add(contentLayout);
            layout.add(gpsView);
            layout.add(lockIcon);
            layout.add(contentLayout2);
        }
        return new SlptClock(layout);
    }

    public static SlptClock createGpsSearchClock(Context mContext, String title, String IconPath) {
        SlptLinearLayout contentLayout2;
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptPictureView bg = new SlptPictureView();
        bg.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_8c_bg.png"));
        SlptViewComponent titleLayout = (SlptLinearLayout) getFont28StringViewBlack(mContext, title, (short) 0);
        titleLayout.setStart(0, 31);
        titleLayout.setRect(320, 32);
        titleLayout.alignX = (byte) 2;
        SlptPictureView icon = new SlptPictureView();
        icon.setImagePicture(SimpleFile.readFileFromAssets(mContext, IconPath));
        icon.setStart(120, 110);
        SlptFlashPicGroupView gpsView = new SlptFlashPicGroupView(3);
        byte[] mem2 = SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_gps_search_1_8c.png");
        byte[] mem3 = SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_gps_search_2_8c.png");
        byte[] mem4 = SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_gps_search_3_8c.png");
        gpsView.setImagePicture(0, mem2);
        gpsView.setImagePicture(1, mem3);
        gpsView.setImagePicture(2, mem4);
        gpsView.setStart(84, 66);
        SlptPictureView lockIcon = new SlptPictureView();
        lockIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_lock.png"));
        SlptLinearLayout contentLayout = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.gps_wait), (short) 0);
        contentLayout.setStart(126, 71);
        if (ModelUtil.isRealModelEverest(mContext)) {
            contentLayout2 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_any_key_wakeup), (short) 0);
        } else {
            contentLayout2 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_power_key_wakeup), (short) 0);
        }
        layout.add(bg);
        layout.add(titleLayout);
        layout.add(icon);
        layout.add(gpsView);
        layout.add(contentLayout);
        if (Util.needEnAssets()) {
            SlptLinearLayout layoutBottom = new SlptLinearLayout();
            layoutBottom.add(lockIcon);
            layoutBottom.add(contentLayout2);
            lockIcon.padding.right = (short) 8;
            layoutBottom.setStart(0, 237);
            layoutBottom.setRect(320, 2147483646);
            layoutBottom.alignX = (byte) 2;
            layout.add(layoutBottom);
        } else {
            lockIcon.setStart(78, 238);
            contentLayout2.setStart(116, 237);
            layout.add(lockIcon);
            layout.add(contentLayout2);
        }
        return new SlptClock(layout);
    }

    public static SlptClock createGpsLostClock(Context mContext, String title) {
        SlptLinearLayout contentLayout4;
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptPictureView bg = new SlptPictureView();
        bg.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_8c_bg.png"));
        SlptPictureView icon = new SlptPictureView();
        icon.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_gps_lost_8c.png"));
        icon.setStart(0, 32);
        icon.setRect(320, 2147483646);
        icon.alignX = (byte) 2;
        SlptPictureView lockIcon = new SlptPictureView();
        lockIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_lock.png"));
        SlptLinearLayout contentLayout = (SlptLinearLayout) getFont28StringViewBlack(mContext, mContext.getResources().getString(R.string.gps_fail), (short) 0);
        contentLayout.setStart(0, 101);
        contentLayout.setRect(320, 2147483646);
        contentLayout.alignX = (byte) 2;
        SlptLinearLayout contentLayout2 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.gps_singal_lost), (short) 0);
        contentLayout2.setStart(0, 161);
        contentLayout2.setRect(320, 24);
        contentLayout2.alignX = (byte) 2;
        if (ModelUtil.isRealModelEverest(mContext)) {
            contentLayout4 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_any_key_wakeup), (short) 0);
        } else {
            contentLayout4 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_power_key_wakeup), (short) 0);
        }
        layout.add(bg);
        layout.add(contentLayout);
        layout.add(contentLayout2);
        layout.add(icon);
        if (Util.needEnAssets()) {
            SlptLinearLayout layoutBottom = new SlptLinearLayout();
            layoutBottom.add(lockIcon);
            layoutBottom.add(contentLayout4);
            lockIcon.padding.right = (short) 8;
            layoutBottom.setStart(0, 237);
            layoutBottom.setRect(320, 2147483646);
            layoutBottom.alignX = (byte) 2;
            layout.add(layoutBottom);
        } else {
            lockIcon.setStart(78, 238);
            contentLayout4.setStart(116, 237);
            layout.add(lockIcon);
            layout.add(contentLayout4);
        }
        return new SlptClock(layout);
    }

    public static SlptClock createGpsCloseClock(Context mContext, String title) {
        SlptLinearLayout contentLayout4;
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptPictureView bg = new SlptPictureView();
        bg.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_8c_bg.png"));
        SlptPictureView icon = new SlptPictureView();
        icon.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_gps_close_8c.png"));
        icon.setStart(131, 32);
        SlptPictureView lockIcon = new SlptPictureView();
        lockIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_lock.png"));
        SlptLinearLayout contentLayout2 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.gps_close_1), (short) 0);
        contentLayout2.setStart(0, 127);
        contentLayout2.setRect(320, 24);
        contentLayout2.alignX = (byte) 2;
        SlptLinearLayout contentLayout3 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.gps_close_2), (short) 0);
        contentLayout3.setStart(0, 155);
        contentLayout3.setRect(320, 24);
        contentLayout3.alignX = (byte) 2;
        if (ModelUtil.isRealModelEverest(mContext)) {
            contentLayout4 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_any_key_wakeup), (short) 0);
        } else {
            contentLayout4 = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_power_key_wakeup), (short) 0);
        }
        layout.add(bg);
        layout.add(contentLayout2);
        layout.add(contentLayout3);
        layout.add(icon);
        if (Util.needEnAssets()) {
            SlptLinearLayout layoutBottom = new SlptLinearLayout();
            layoutBottom.add(lockIcon);
            layoutBottom.add(contentLayout4);
            lockIcon.padding.right = (short) 8;
            layoutBottom.setStart(0, 237);
            layoutBottom.setRect(320, 2147483646);
            layoutBottom.alignX = (byte) 2;
            layout.add(layoutBottom);
        } else {
            lockIcon.setStart(78, 238);
            contentLayout4.setStart(116, 237);
            layout.add(lockIcon);
            layout.add(contentLayout4);
        }
        return new SlptClock(layout);
    }

    public static SlptClock createSportStartClock(Context mContext, String title, String IconPath) {
        SlptLinearLayout contentLayout;
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptPictureView bg = new SlptPictureView();
        bg.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_8c_bg.png"));
        SlptLinearLayout titleLayout = (SlptLinearLayout) getFont28StringViewBlack(mContext, title, (short) 0);
        titleLayout.setStart(0, 41);
        titleLayout.setRect(320, 32);
        titleLayout.alignX = (byte) 2;
        SlptPictureView icon = new SlptPictureView();
        icon.setImagePicture(SimpleFile.readFileFromAssets(mContext, IconPath));
        icon.setStart(120, 97);
        SlptPictureView lockIcon = new SlptPictureView();
        lockIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_lock.png"));
        if (ModelUtil.isRealModelEverest(mContext)) {
            contentLayout = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_any_key_wakeup), (short) 0);
        } else {
            contentLayout = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_power_key_wakeup), (short) 0);
        }
        layout.add(bg);
        layout.add(titleLayout);
        layout.add(icon);
        if (Util.needEnAssets()) {
            SlptLinearLayout layoutBottom = new SlptLinearLayout();
            layoutBottom.add(lockIcon);
            layoutBottom.add(contentLayout);
            lockIcon.padding.right = (short) 8;
            layoutBottom.setStart(0, 237);
            layoutBottom.setRect(320, 2147483646);
            layoutBottom.alignX = (byte) 2;
            layout.add(layoutBottom);
        } else {
            lockIcon.setStart(78, 238);
            contentLayout.setStart(116, 237);
            layout.add(lockIcon);
            layout.add(contentLayout);
        }
        return new SlptClock(layout);
    }

    public static SlptLayout createDiagramHeartRate(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptAbsoluteLayout layoutHeartIcon = new SlptAbsoluteLayout();
        SlptAbsoluteLayout layoutHeartTitle = new SlptAbsoluteLayout();
        SlptLinearLayout layoutData = new SlptLinearLayout();
        SlptViewComponent layoutValue = new SlptLinearLayout();
        SlptViewComponent normalIconView = new SlptPictureView();
        normalIconView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_hr_diagram_0_8c.png"));
        SlptViewComponent normalTitleView = (SlptLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.heartrate), (short) 0);
        SlptSportUtil.setBgHeartrateNormal(normalIconView);
        SlptSportUtil.setBgHeartrateNormal(normalTitleView);
        SlptViewComponent warmUpIconView = new SlptPictureView();
        warmUpIconView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_hr_diagram_1_8c.png"));
        SlptViewComponent warmUpTitleView = (SlptLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.warmup), (short) 0);
        SlptSportUtil.setBgHeartrateWarmUp(warmUpIconView);
        SlptSportUtil.setBgHeartrateWarmUp(warmUpTitleView);
        SlptPictureView fatBurnIconView = new SlptPictureView();
        fatBurnIconView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_hr_diagram_2_8c.png"));
        SlptLayout fatBurnTitleView = (SlptLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.fatburn), (short) 0);
        SlptSportUtil.setBgHeartrateFatBurn(fatBurnIconView);
        SlptSportUtil.setBgHeartrateFatBurn(fatBurnTitleView);
        SlptPictureView heartLungIconView = new SlptPictureView();
        heartLungIconView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_hr_diagram_3_8c.png"));
        SlptLayout heartLungTitleView = (SlptLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.heartlung), (short) 0);
        SlptSportUtil.setBgHeartrateHeartLung(heartLungIconView);
        SlptSportUtil.setBgHeartrateHeartLung(heartLungTitleView);
        SlptViewComponent strengthIconView = new SlptPictureView();
        strengthIconView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_hr_diagram_4_8c.png"));
        SlptViewComponent strengthTitleView = (SlptLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.strength), (short) 0);
        SlptSportUtil.setHeartrateStrength(strengthIconView);
        SlptSportUtil.setHeartrateStrength(strengthTitleView);
        SlptPictureView anaerobicIconView = new SlptPictureView();
        anaerobicIconView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_hr_diagram_5_8c.png"));
        SlptLayout anaerobicTitleView = (SlptLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.anaerobic), (short) 0);
        SlptSportUtil.setBgHeartrateAnaerobic(anaerobicIconView);
        SlptSportUtil.setBgHeartrateAnaerobic(anaerobicTitleView);
        layoutHeartIcon.add(normalIconView);
        layoutHeartIcon.add(warmUpIconView);
        layoutHeartIcon.add(fatBurnIconView);
        layoutHeartIcon.add(heartLungIconView);
        layoutHeartIcon.add(strengthIconView);
        layoutHeartIcon.add(anaerobicIconView);
        layoutHeartIcon.setStart(0, 22);
        layoutHeartIcon.setRect(320, 108);
        layoutHeartIcon.alignX = (byte) 2;
        layoutHeartTitle.add(normalTitleView);
        layoutHeartTitle.add(warmUpTitleView);
        layoutHeartTitle.add(fatBurnTitleView);
        layoutHeartTitle.add(heartLungTitleView);
        layoutHeartTitle.add(strengthTitleView);
        layoutHeartTitle.add(anaerobicTitleView);
        layoutHeartTitle.setStart(0, 59);
        layoutHeartTitle.setRect(320, 2147483646);
        layoutHeartTitle.alignX = (byte) 2;
        SlptPictureView heartrateInvalidView = new SlptPictureView();
        heartrateInvalidView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/default.png"));
        SlptSportUtil.setHeartRateInvalidView(heartrateInvalidView);
        SlptHeartRateSview heartRateSview = new SlptHeartRateSview();
        heartRateSview.setImagePictureArray(num);
        layoutData.add(heartRateSview);
        layoutData.add(heartrateInvalidView);
        SlptViewComponent unitLayout = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.bpm), (short) 0);
        unitLayout.padding.left = (short) 4;
        unitLayout.alignParentY = (byte) 1;
        byte[] mem15 = SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/unit_null.png");
        SlptViewComponent nullView = new SlptPictureView();
        nullView.setImagePicture(mem15);
        nullView.padding.right = (short) 24;
        SlptPictureView diagramIcon = new SlptPictureView();
        diagramIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_diagram_heart_icon_8c.png"));
        diagramIcon.setStart(136, 235);
        layoutValue.add(nullView);
        layoutValue.add(layoutData);
        layoutValue.add(unitLayout);
        layoutValue.setStart(0, 128);
        layoutValue.setRect(320, 64);
        layoutValue.alignX = (byte) 2;
        layout.add(layoutHeartIcon);
        layout.add(layoutHeartTitle);
        layout.add(layoutValue);
        layout.add(diagramIcon);
        if (pageTitleView != null) {
            layout.add(pageTitleView);
        }
        SportLayoutTheme.setViewBgColor(mContext, layout);
        return layout;
    }

    public static SlptLayout createDiagramAltitude(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        String unit;
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptLinearLayout layoutValue = new SlptLinearLayout();
        SlptLinearLayout layoutData = new SlptLinearLayout();
        SlptPictureView defaultView = new SlptPictureView();
        defaultView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/default.png"));
        SlptSportUtil.setAltitudeInvalidView(defaultView);
        SlptPictureView negativeView = new SlptPictureView();
        negativeView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/minus.png"));
        SlptSportUtil.setAltitudeNegativeView(negativeView);
        SlptAltitudeView altitudeView = new SlptAltitudeView();
        altitudeView.setImagePictureArray(num);
        if (Util.getMeasurement(mContext) == 0) {
            unit = mContext.getResources().getString(R.string.meter);
        } else {
            unit = mContext.getResources().getString(R.string.feet);
        }
        SlptViewComponent unitLayout = (SlptLinearLayout) getFont16StringView(mContext, unit, (short) 0);
        unitLayout.padding.left = (short) 4;
        unitLayout.alignParentY = (byte) 1;
        byte[] mem15 = SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/unit_null.png");
        SlptPictureView nullView = new SlptPictureView();
        nullView.setImagePicture(mem15);
        nullView.padding.right = (short) 6;
        layoutData.add(defaultView);
        layoutData.add(negativeView);
        layoutData.add(altitudeView);
        layoutValue.add(nullView);
        layoutValue.add(layoutData);
        layoutValue.add(unitLayout);
        layoutValue.setStart(0, 128);
        layoutValue.setRect(320, 64);
        layoutValue.alignX = (byte) 2;
        SlptPictureView diagramIcon = new SlptPictureView();
        diagramIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_diagram_altitude_icon_8c.png"));
        diagramIcon.setStart(136, 235);
        SlptViewComponent titleString = (SlptLinearLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.current_altitude), (short) 0);
        titleString.setStart(0, 60);
        titleString.setRect(320, 2147483646);
        titleString.alignX = (byte) 2;
        layout.add(titleString);
        layout.add(diagramIcon);
        layout.add(layoutValue);
        if (pageTitleView != null) {
            layout.add(pageTitleView);
        }
        SportLayoutTheme.setViewBgColor(mContext, layout);
        return layout;
    }

    public static SlptLayout createDiagramSpeed(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        String unit;
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptLinearLayout layoutData = new SlptLinearLayout();
        SlptSpeedMView fView = new SlptSpeedMView();
        SlptSpeedSView lView = new SlptSpeedSView();
        SlptPictureView seqView1 = new SlptPictureView();
        fView.setImagePictureArray(num);
        lView.setImagePictureArray(num);
        seqView1.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/dot.png"));
        SlptSportUtil.setSpeedSeparatorView(seqView1);
        SlptPictureView diagramIcon = new SlptPictureView();
        diagramIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_diagram_speed_icon_8c.png"));
        diagramIcon.setStart(136, 235);
        if (Util.getMeasurement(mContext) == 0) {
            unit = mContext.getResources().getString(R.string.km_per_hour);
        } else {
            unit = mContext.getResources().getString(R.string.mile_per_hour);
        }
        SlptViewComponent unitLayout = (SlptLinearLayout) getFont16StringView(mContext, unit, (short) 0);
        unitLayout.alignParentY = (byte) 1;
        unitLayout.padding.left = (short) 4;
        byte[] mem2 = SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/unit_null.png");
        SlptPictureView nullView = new SlptPictureView();
        nullView.setImagePicture(mem2);
        nullView.padding.right = (short) 34;
        SlptLinearLayout titleString = (SlptLinearLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.speed), (short) 0);
        titleString.setStart(0, 60);
        titleString.setRect(320, 2147483646);
        titleString.alignX = (byte) 2;
        layoutData.add(nullView);
        layoutData.add(fView);
        layoutData.add(seqView1);
        layoutData.add(lView);
        layoutData.add(unitLayout);
        layoutData.setStart(0, 128);
        layoutData.setRect(320, 64);
        layoutData.alignX = (byte) 2;
        layout.add(titleString);
        layout.add(diagramIcon);
        layout.add(layoutData);
        if (pageTitleView != null) {
            layout.add(pageTitleView);
        }
        SportLayoutTheme.setViewBgColor(mContext, layout);
        return layout;
    }

    public static SlptLayout createDiagramStrokeSpeed(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptLinearLayout layoutData = new SlptLinearLayout();
        SlptSwimStrokeSpeedView speedView = new SlptSwimStrokeSpeedView();
        speedView.setImagePictureArray(num);
        SlptLinearLayout unitLayout = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.times_per_minute), (short) 0);
        unitLayout.alignParentY = (byte) 1;
        unitLayout.padding.left = (short) 4;
        byte[] mem15 = SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/unit_null.png");
        SlptPictureView nullView = new SlptPictureView();
        nullView.setImagePicture(mem15);
        nullView.padding.right = (short) 26;
        layoutData.add(nullView);
        layoutData.add(speedView);
        layoutData.add(unitLayout);
        layoutData.setStart(0, 128);
        layoutData.setRect(320, 64);
        layoutData.alignX = (byte) 2;
        SlptPictureView diagramIcon = new SlptPictureView();
        diagramIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_diagram_chart_icon_8c.png"));
        diagramIcon.setStart(136, 235);
        SlptLinearLayout titleString = (SlptLinearLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.stroke_speed), (short) 0);
        titleString.setStart(0, 60);
        titleString.setRect(320, 2147483646);
        titleString.alignX = (byte) 2;
        layout.add(titleString);
        layout.add(diagramIcon);
        layout.add(layoutData);
        if (pageTitleView != null) {
            layout.add(pageTitleView);
        }
        SportLayoutTheme.setViewBgColor(mContext, layout);
        return layout;
    }

    public static SlptLayout createDiagramPace(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        String unit;
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptLinearLayout layoutData = new SlptLinearLayout();
        SlptPaceMView fView = new SlptPaceMView();
        SlptPaceSView lView = new SlptPaceSView();
        SlptViewComponent seqView1 = new SlptPictureView();
        SlptViewComponent seqView2 = new SlptPictureView();
        fView.setImagePictureArray(num);
        lView.setImagePictureArray(num);
        fView.setUnit(0);
        lView.setUnit(0);
        seqView1.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/minute.png"));
        seqView2.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/second.png"));
        SlptPictureView diagramIcon = new SlptPictureView();
        diagramIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_diagram_pace_icon_8c.png"));
        diagramIcon.setStart(136, 235);
        SlptPictureView defaultView = new SlptPictureView();
        defaultView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/default.png"));
        if (Util.getMeasurement(mContext) == 0) {
            unit = mContext.getResources().getString(R.string.per_km);
        } else {
            unit = mContext.getResources().getString(R.string.per_mile);
        }
        SlptViewComponent unitLayout = (SlptLinearLayout) getFont16StringView(mContext, unit, (short) 0);
        unitLayout.alignParentY = (byte) 1;
        unitLayout.padding.left = (short) 4;
        byte[] mem5 = SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/unit_null.png");
        SlptPictureView nullView = new SlptPictureView();
        nullView.setImagePicture(mem5);
        nullView.padding.right = (short) 26;
        SlptViewComponent titleString = (SlptLinearLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.pace), (short) 0);
        titleString.setStart(0, 60);
        titleString.setRect(320, 2147483646);
        titleString.alignX = (byte) 2;
        SlptLinearLayout layoutValue = new SlptLinearLayout();
        layoutValue.add(fView);
        layoutValue.add(seqView1);
        layoutValue.add(lView);
        layoutValue.add(seqView2);
        layoutData.add(nullView);
        layoutData.add(defaultView);
        layoutData.add(layoutValue);
        layoutData.add(unitLayout);
        SlptSportUtil.setPaceLayoutView(layoutValue);
        SlptSportUtil.setPaceDefaultView(defaultView);
        layoutData.setStart(0, 128);
        layoutData.setRect(320, 64);
        layoutData.alignX = (byte) 2;
        layout.add(titleString);
        layout.add(diagramIcon);
        layout.add(layoutData);
        if (pageTitleView != null) {
            layout.add(pageTitleView);
        }
        SportLayoutTheme.setViewBgColor(mContext, layout);
        return layout;
    }

    public static SlptLayout createDiagramSwimPace(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        String unit;
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptLinearLayout layoutData = new SlptLinearLayout();
        SlptPaceMView fView = new SlptPaceMView();
        SlptPaceSView lView = new SlptPaceSView();
        SlptViewComponent seqView1 = new SlptPictureView();
        SlptViewComponent seqView2 = new SlptPictureView();
        fView.setImagePictureArray(num);
        lView.setImagePictureArray(num);
        fView.setUnit(1);
        lView.setUnit(1);
        seqView1.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/minute.png"));
        seqView2.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/second.png"));
        SlptPictureView diagramIcon = new SlptPictureView();
        diagramIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_diagram_pace_icon_8c.png"));
        diagramIcon.setStart(136, 235);
        SlptPictureView defaultView = new SlptPictureView();
        defaultView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/default.png"));
        defaultView.alignX = (byte) 2;
        byte[] mem5 = SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/unit_null.png");
        SlptPictureView nullView = new SlptPictureView();
        nullView.setImagePicture(mem5);
        if (Util.needSwimYardUnit(mContext)) {
            unit = mContext.getResources().getString(R.string.per_100Yard);
            nullView.padding.right = (short) 60;
        } else {
            unit = mContext.getResources().getString(R.string.per_100m);
            nullView.padding.right = (short) 49;
        }
        SlptViewComponent unitLayout = (SlptLinearLayout) getFont16StringView(mContext, unit, (short) 0);
        unitLayout.alignParentY = (byte) 1;
        unitLayout.padding.left = (short) 4;
        SlptViewComponent titleString = (SlptLinearLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.pace), (short) 0);
        titleString.setStart(0, 60);
        titleString.setRect(320, 2147483646);
        titleString.alignX = (byte) 2;
        SlptLinearLayout layoutValue = new SlptLinearLayout();
        layoutValue.add(fView);
        layoutValue.add(seqView1);
        layoutValue.add(lView);
        layoutValue.add(seqView2);
        layoutData.add(nullView);
        layoutData.add(defaultView);
        layoutData.add(layoutValue);
        layoutData.add(unitLayout);
        SlptSportUtil.setPaceLayoutView(layoutValue);
        SlptSportUtil.setPaceDefaultView(defaultView);
        layoutData.setStart(0, 128);
        layoutData.setRect(320, 64);
        layoutData.alignX = (byte) 2;
        layout.add(titleString);
        layout.add(diagramIcon);
        layout.add(layoutData);
        if (pageTitleView != null) {
            layout.add(pageTitleView);
        }
        SportLayoutTheme.setViewBgColor(mContext, layout);
        return layout;
    }

    public static SlptLayout createDiagramDistanceKm(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        return createDiagramDistance(mContext, num, pageTitleView, 0);
    }

    public static SlptLayout createDiagramDistanceM(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        return createDiagramDistance(mContext, num, pageTitleView, 1);
    }

    public static SlptLayout createDiagramDistanceMi(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        return createDiagramDistance(mContext, num, pageTitleView, 2);
    }

    public static SlptLayout createDiagramDistanceYard(Context mContext, byte[][] num, SlptViewComponent pageTitleView) {
        return createDiagramDistance(mContext, num, pageTitleView, 4);
    }

    public static SlptLayout createDiagramDistance(Context mContext, byte[][] num, SlptViewComponent pageTitleView, int distanceType) {
        SlptLinearLayout unitString;
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptLinearLayout layoutData = new SlptLinearLayout();
        SlptDistanceFView fView = new SlptDistanceFView();
        SlptDistanceLView lView = new SlptDistanceLView();
        SlptPictureView seqView = new SlptPictureView();
        fView.setImagePictureArray(num);
        lView.setImagePictureArray(num);
        SlptSportUtil.setDistanceSeparatorView(seqView);
        seqView.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/dot.png"));
        SlptPictureView diagramIcon = new SlptPictureView();
        diagramIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_diagram_distance_icon_8c.png"));
        diagramIcon.setStart(139, 239);
        byte[] mem15 = SimpleFile.readFileFromAssets(mContext, themeDir + "big-number/unit_null.png");
        SlptPictureView nullView = new SlptPictureView();
        nullView.setImagePicture(mem15);
        SlptLinearLayout titleString = (SlptLinearLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.mileage), (short) 0);
        titleString.setStart(0, 60);
        titleString.setRect(320, 2147483646);
        titleString.alignX = (byte) 2;
        switch (distanceType) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                fView.setUnit(1);
                lView.setUnit(1);
                unitString = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.meter), (short) 0);
                nullView.padding.right = (short) 18;
                break;
            case 2:
                fView.setUnit(0);
                lView.setUnit(0);
                unitString = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.mile), (short) 0);
                nullView.padding.right = (short) 18;
                break;
            case 3:
                fView.setUnit(1);
                lView.setUnit(1);
                unitString = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.feet), (short) 0);
                nullView.padding.right = (short) 36;
                break;
            case 4:
                fView.setUnit(1);
                lView.setUnit(1);
                unitString = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.yard), (short) 0);
                nullView.padding.right = (short) 36;
                break;
            default:
                fView.setUnit(0);
                lView.setUnit(0);
                unitString = (SlptLinearLayout) getFont16StringView(mContext, mContext.getResources().getString(R.string.kilometre), (short) 0);
                nullView.padding.right = (short) 18;
                break;
        }
        layoutData.add(nullView);
        layoutData.add(fView);
        layoutData.add(seqView);
        layoutData.add(lView);
        layoutData.add(unitString);
        layoutData.setStart(0, 131);
        layoutData.setRect(320, 72);
        layoutData.alignX = (byte) 2;
        layoutData.alignY = (byte) 1;
        layout.add(titleString);
        layout.add(diagramIcon);
        layout.add(layoutData);
        if (pageTitleView != null) {
            layout.add(pageTitleView);
        }
        SportLayoutTheme.setViewBgColor(mContext, layout);
        return layout;
    }

    public static SlptLayout createDiagramSkiingDistance(Context mContext, Typeface font, SlptViewComponent pageTitleView) {
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        SlptViewComponent layout = new SlptAbsoluteLayout();
        SlptAltitudeView altitudeView = new SlptAltitudeView();
        SlptLinearLayout layoutAltiude = new SlptLinearLayout();
        SlptLinearLayout layoutAltiudevalue = new SlptLinearLayout();
        SlptViewComponent negtiveView = new SlptPictureView();
        SlptPictureView defaultAltitude = new SlptPictureView();
        String unitString = "";
        SlptViewComponent longitudeLayout = new SlptAbsoluteLayout();
        SlptViewComponent longitudeValueLayout = new SlptLinearLayout();
        SlptPictureView eastView = new SlptPictureView();
        SlptViewComponent westView = new SlptPictureView();
        SlptViewComponent longitudeDefault = new SlptPictureView();
        SlptViewComponent longitudeDegreeView = new SlptLongitudeDegreeView();
        SlptViewComponent longitudeDegreeSep = new SlptPictureView();
        SlptViewComponent longitudeMinuteView = new SlptLongitudeMinuteView();
        SlptViewComponent longitudeMinuteSep = new SlptPictureView();
        SlptViewComponent longitudeSecondView = new SlptLongitudeSecondView();
        SlptViewComponent longitudeSecondSep = new SlptPictureView();
        SlptAbsoluteLayout latitudeLayout = new SlptAbsoluteLayout();
        SlptLinearLayout latitudeValueLayout = new SlptLinearLayout();
        SlptViewComponent latitudedeDefault = new SlptPictureView();
        SlptViewComponent northView = new SlptPictureView();
        SlptViewComponent southView = new SlptPictureView();
        SlptLatitudeDegreeView latitudeDegreeView = new SlptLatitudeDegreeView();
        SlptPictureView latitudeDegreeSep = new SlptPictureView();
        SlptLatitudeMinuteView latitudeMinuteView = new SlptLatitudeMinuteView();
        SlptPictureView latitudeMinuteSep = new SlptPictureView();
        SlptLatitudeSecondView latitudeSecondView = new SlptLatitudeSecondView();
        SlptPictureView latitudeSecondSep = new SlptPictureView();
        SlptPictureView diagramIcon = new SlptPictureView();
        diagramIcon.setImagePicture(SimpleFile.readFileFromAssets(mContext, themeDir + "diagram/sport_diagram_location_icon_8c.png"));
        diagramIcon.setStart(140, 234);
        SlptViewComponent stringLayout = (SlptLinearLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.altitude), (short) 0);
        stringLayout.setStart(0, 69);
        stringLayout.setRect(mContext.getResources().getInteger(R.integer.skill_diagram_content_rect_width), 2147483646);
        stringLayout.alignX = (byte) 1;
        SlptViewComponent stringLayout1 = (SlptLinearLayout) getFont21StringView(mContext, mContext.getResources().getString(R.string.longitude_and_latitude), (short) 0);
        stringLayout1.setStart(0, 132);
        stringLayout1.setRect(mContext.getResources().getInteger(R.integer.skill_diagram_content_rect_width), 2147483646);
        stringLayout1.alignX = (byte) 1;
        if (Util.getMeasurement(mContext) == 0) {
            unitString = mContext.getResources().getString(R.string.meter);
        } else {
            unitString = mContext.getResources().getString(R.string.feet);
        }
        SlptViewComponent unitLayout = (SlptLinearLayout) getFont16StringView(mContext, unitString, (short) 0);
        layoutAltiudevalue.add(defaultAltitude);
        layoutAltiudevalue.add(negtiveView);
        layoutAltiudevalue.add(altitudeView);
        layoutAltiude.add(layoutAltiudevalue);
        layoutAltiude.add(unitLayout);
        SlptSportUtil.setAltitudeInvalidView(defaultAltitude);
        SlptSportUtil.setAltitudeNegativeView(negtiveView);
        negtiveView.setStringPicture("-");
        defaultAltitude.setStringPicture("--");
        layoutAltiude.setStart(mContext.getResources().getInteger(R.integer.skill_diagram_altitude_left), 63);
        layoutAltiude.setRect(160, 38);
        layoutAltiude.alignX = (byte) 0;
        layoutAltiude.alignY = (byte) 1;
        longitudeValueLayout.add(eastView);
        longitudeValueLayout.add(westView);
        longitudeValueLayout.add(longitudeDegreeView);
        longitudeValueLayout.add(longitudeDegreeSep);
        longitudeValueLayout.add(longitudeMinuteView);
        longitudeValueLayout.add(longitudeMinuteSep);
        longitudeValueLayout.add(longitudeSecondView);
        longitudeValueLayout.add(longitudeSecondSep);
        longitudeLayout.add(longitudeDefault);
        longitudeLayout.add(longitudeValueLayout);
        longitudeLayout.setStart(mContext.getResources().getInteger(R.integer.skill_diagram_longtitude_left), 163);
        longitudeLayout.setRect(160, 53);
        longitudeLayout.alignX = (byte) 0;
        longitudeLayout.alignY = (byte) 2;
        longitudeDegreeSep.setStringPicture("°");
        longitudeMinuteSep.setStringPicture("'");
        longitudeSecondSep.setStringPicture("\"");
        longitudeDefault.setStringPicture("--");
        eastView.setStringPicture("E");
        westView.setStringPicture("W");
        SlptSportUtil.setBgLongitudeLayoutView(longitudeValueLayout);
        SlptSportUtil.setLongitudeEastView(eastView);
        SlptSportUtil.setLongitudeWestView(westView);
        latitudeValueLayout.add(northView);
        latitudeValueLayout.add(southView);
        latitudeValueLayout.add(latitudeDegreeView);
        latitudeValueLayout.add(latitudeDegreeSep);
        latitudeValueLayout.add(latitudeMinuteView);
        latitudeValueLayout.add(latitudeMinuteSep);
        latitudeValueLayout.add(latitudeSecondView);
        latitudeValueLayout.add(latitudeSecondSep);
        latitudeLayout.add(latitudedeDefault);
        latitudeLayout.add(latitudeValueLayout);
        latitudeLayout.setStart(mContext.getResources().getInteger(R.integer.skill_diagram_longtitude_left), 114);
        latitudeLayout.setRect(160, 53);
        latitudeLayout.alignX = (byte) 0;
        latitudeLayout.alignY = (byte) 2;
        latitudeDegreeSep.setStringPicture("°");
        latitudeMinuteSep.setStringPicture("'");
        latitudeSecondSep.setStringPicture("\"");
        latitudedeDefault.setStringPicture("--");
        northView.setStringPicture("N");
        southView.setStringPicture("S");
        SlptSportUtil.setBgLatitudeLayout(latitudeValueLayout);
        SlptSportUtil.setLatitudeNorthView(northView);
        SlptSportUtil.setLatitudeSouthView(southView);
        if (SportLayoutTheme.getSlptThemesId(mContext) == 0) {
            layoutAltiudevalue.setTextAttrForAll(100.0f, -1, font);
            longitudeLayout.setTextAttrForAll(100.0f, -1, font);
            latitudeLayout.setTextAttrForAll(100.0f, -1, font);
        } else {
            layoutAltiudevalue.setTextAttrForAll(100.0f, -16777216, font);
            longitudeLayout.setTextAttrForAll(100.0f, -16777216, font);
            latitudeLayout.setTextAttrForAll(100.0f, -16777216, font);
        }
        layout.add(layoutAltiude);
        layout.add(longitudeLayout);
        layout.add(latitudeLayout);
        layout.add(diagramIcon);
        layout.add(stringLayout);
        layout.add(stringLayout1);
        if (pageTitleView != null) {
            layout.add(pageTitleView);
        }
        SportLayoutTheme.setViewBgColor(mContext, layout);
        SportLayoutTheme.setViewBgColor(mContext, latitudeValueLayout);
        SportLayoutTheme.setViewBgColor(mContext, longitudeValueLayout);
        return layout;
    }

    public static SlptClock createMixSportLayout(Context mContext, int type) {
        int[] order;
        String iconName;
        String iconPath;
        int itemSize;
        String themeDir = SportLayoutTheme.getSlptThemesDir(mContext, false);
        switch (type) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                order = SportDataItemInfo.mRunningMixInfoOrder;
                iconName = mContext.getResources().getString(R.string.running);
                iconPath = themeDir + "bg/sport_fun_icon_running_8c.png";
                itemSize = 5;
                break;
            case 9:
                order = SportDataItemInfo.mOutRidingMixInfoOrder;
                iconPath = themeDir + "bg/sport_fun_icon_outdoor_riding_8c.png";
                iconName = mContext.getResources().getString(R.string.riding);
                itemSize = 5;
                break;
            case 15:
                order = SportDataItemInfo.mOutSwimMixInfoOrder;
                iconPath = themeDir + "bg/sport_fun_icon_outdoor_swimming_8c.png";
                iconName = mContext.getResources().getString(R.string.swimming);
                itemSize = 4;
                break;
            default:
                return null;
        }
        SportLayoutItem clockItem = new SportLayoutItem(mContext, itemSize, true);
        String[] itemNameArray = SportDataItemInfo.getDateItemArray(mContext);
        clockItem.addView(createMixSportTitle(mContext, getTitleNum(mContext)));
        for (int i = 0; i < itemSize - 1; i++) {
            int currentLayoutIndex = clockItem.getCurrentLayoutIndex();
            clockItem.addItem(SportDataItemInfo.getItemView(mContext, order[i], getSportRegularTypeface(mContext)), itemNameArray[order[i]]);
            if (order[i] == 5) {
                clockItem.addHeartRateViewBg(currentLayoutIndex);
                Log.i("HmSportListUtil", "add heart inex" + currentLayoutIndex);
            }
        }
        clockItem.addView(createMixSportFlag(mContext, iconPath, iconName));
        return clockItem.getSlptClock();
    }

    public static SlptLayout createLoadingLayout(Context mContext) {
        SlptAbsoluteLayout rootLayout = new SlptAbsoluteLayout();
        SlptPictureView bg = new SlptPictureView();
        bg.setImagePicture(SimpleFile.readFileFromAssets(mContext, "sport/black/zh/bg/sport_triathlon_bg_2.png"));
        rootLayout.add(bg);
        SlptLinearLayout stringLayout = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.enter_sport_lock_status), (short) 0);
        stringLayout.setStart(0, 170);
        stringLayout.setRect(320, 2147483646);
        stringLayout.alignX = (byte) 2;
        rootLayout.add(stringLayout);
        rootLayout.add(createSystemTimeLayoutBlack(mContext, getTitleNum(mContext)));
        return rootLayout;
    }

    public static SlptViewComponent createWakeUpLockView(Context mContext) {
        SlptLinearLayout layout = new SlptLinearLayout();
        SlptLinearLayout stringLayout = (SlptLinearLayout) getFont21StringViewBlack(mContext, mContext.getResources().getString(R.string.press_up_key_wake_up), (short) 0);
        stringLayout.setRect(2147483646, mContext.getResources().getInteger(R.integer.unlock_watchface_title_hight));
        stringLayout.background.color = -16777216;
        stringLayout.alignY = (byte) 2;
        SlptPictureView headView = new SlptPictureView();
        headView.setImagePicture(SimpleFile.readFileFromAssets(mContext, "guard/lan_watchface_icon_unlock_left.png"));
        SlptPictureView tailView = new SlptPictureView();
        tailView.setImagePicture(SimpleFile.readFileFromAssets(mContext, "guard/lan_watchface_icon_unlock_right.png"));
        layout.add(headView);
        layout.add(stringLayout);
        layout.add(tailView);
        layout.setStart(0, 136);
        layout.setRect(320, mContext.getResources().getInteger(R.integer.unlock_watchface_title_hight));
        layout.alignX = (byte) 2;
        SlptSportUtil.setWatchfaceLockView(layout);
        return layout;
    }

    public static SlptViewComponent createLowBatteryView(Context mContext, int Iconcolor, int bgColor) {
        int fontColor;
        SlptLinearLayout layout = new SlptLinearLayout();
        SlptPictureView headView = new SlptPictureView();
        SlptLinearLayout layoutHead = new SlptLinearLayout();
        switch (Iconcolor) {
            case -16711936:
                fontColor = -1;
                headView.setImagePicture(SimpleFile.readFileFromAssets(mContext, "guard/watchface_low_power_green.png"));
                break;
            case -65536:
                fontColor = -1;
                headView.setImagePicture(SimpleFile.readFileFromAssets(mContext, "guard/watchface_low_power_red.png"));
                break;
            case -1:
                fontColor = -1;
                headView.setImagePicture(SimpleFile.readFileFromAssets(mContext, "guard/watchface_low_power_white.png"));
                break;
            default:
                fontColor = -16777216;
                headView.setImagePicture(SimpleFile.readFileFromAssets(mContext, "guard/watchface_low_power_black.png"));
                break;
        }
        layoutHead.add(headView);
        SlptLinearLayout stringLayout = (SlptLinearLayout) getFontStringView(mContext, mContext.getResources().getString(R.string.low_battery_tips), 14, fontColor, (short) 0);
        layout.add(layoutHead);
        layout.add(stringLayout);
        layoutHead.setRect(mContext.getResources().getInteger(R.integer.low_battery_icon_width), mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
        stringLayout.setRect(2147483646, mContext.getResources().getInteger(R.integer.low_battery_tips_hight));
        if (bgColor != 0) {
            stringLayout.background.color = bgColor;
            layoutHead.background.color = bgColor;
        }
        stringLayout.alignY = (byte) 2;
        layoutHead.alignY = (byte) 2;
        layout.alignX = (byte) 2;
        layout.alignY = (byte) 2;
        layout.show = false;
        SlptSportUtil.setLowBatteryIconView(layout);
        return layout;
    }
}
