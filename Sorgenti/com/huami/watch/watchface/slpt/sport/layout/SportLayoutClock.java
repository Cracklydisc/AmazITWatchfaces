package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import java.util.ArrayList;

public abstract class SportLayoutClock {
    private static final int[][] array4;
    private static final int[][] array6;
    private ArrayList<SlptClock> clockList = new ArrayList();
    private byte[][] content_num;
    private int currentFaceIndex = 0;
    private int currentItemIndex = 0;
    private int currentSportId = 0;
    private int[] defOrder;
    private int[] diagramList;
    private byte[][] diagram_num;
    private boolean isMixSport = false;
    private String[] itemNameArray;
    private Context mContext;
    private int[] order;
    private int[] pageList;
    private Typeface sportItemTypeface;
    private byte[][] title_num;

    protected abstract int getDefaultDiagram();

    protected abstract int[] getDiagramList();

    protected abstract int[] getPageList4();

    protected abstract int[] getPageList6();

    protected abstract boolean needDistanceDiagram();

    static {
        r0 = new int[14][];
        r0[0] = new int[]{2};
        r0[1] = new int[]{2};
        r0[2] = new int[]{3};
        r0[3] = new int[]{4};
        r0[4] = new int[]{4, 2};
        r0[5] = new int[]{4, 2};
        r0[6] = new int[]{4, 3};
        r0[7] = new int[]{4, 4};
        r0[8] = new int[]{4, 3, 2};
        r0[9] = new int[]{4, 4, 2};
        r0[10] = new int[]{4, 4, 3};
        r0[11] = new int[]{4, 4, 4};
        r0[12] = new int[]{4, 5, 4};
        r0[13] = new int[]{4, 5, 5};
        array4 = r0;
        r0 = new int[14][];
        r0[0] = new int[]{2};
        r0[1] = new int[]{2};
        r0[2] = new int[]{3};
        r0[3] = new int[]{4};
        r0[4] = new int[]{5};
        r0[5] = new int[]{6};
        r0[6] = new int[]{6, 2};
        r0[7] = new int[]{6, 2};
        r0[8] = new int[]{6, 3};
        r0[9] = new int[]{6, 4};
        r0[10] = new int[]{6, 5};
        r0[11] = new int[]{6, 6};
        r0[12] = new int[]{6, 4, 3};
        r0[13] = new int[]{6, 4, 4};
        array6 = r0;
    }

    public SportLayoutClock(Context mContext, int sportId, int[] defOrder, boolean isMixSport) {
        this.mContext = mContext;
        if (getFirstPageSize() == 6) {
            this.pageList = getPageList6();
        } else {
            this.pageList = getPageList4();
        }
        this.isMixSport = isMixSport;
        this.currentSportId = sportId;
        this.diagramList = getDiagramList();
        this.title_num = SportItemInfoWrapper.getTitleNum(mContext);
        this.content_num = SportItemInfoWrapper.getContentNum(mContext);
        this.diagram_num = SportItemInfoWrapper.getDiagramNum(mContext);
        this.defOrder = defOrder;
        this.sportItemTypeface = SportItemInfoWrapper.getSportRegularTypeface(mContext);
        this.itemNameArray = SportDataItemInfo.getDateItemArray(mContext);
        String currentItemOrder = Util.getSportItemOrder();
        String remoteOrder;
        if (currentItemOrder.equals("")) {
            remoteOrder = SportItemInfoWrapper.getSportItemOrder(mContext, this.currentSportId);
            Log.i("LayoutClock", "get first itemOrder " + currentItemOrder);
            this.order = filter_order(SportItemInfoWrapper.parseStringToOrder(remoteOrder), defOrder);
            if (this.order == null) {
                setOrder(defOrder);
                return;
            }
            setOrder(this.order);
            Util.setSportItemOrder(remoteOrder);
            this.pageList = caclulate_page_list(this.order, getFirstPageSize());
            return;
        }
        remoteOrder = currentItemOrder;
        Log.i("LayoutClock", "get exist itemOrder " + currentItemOrder);
        this.order = filter_order(SportItemInfoWrapper.parseStringToOrder(remoteOrder), defOrder);
        if (this.order == null) {
            setOrder(defOrder);
            return;
        }
        setOrder(this.order);
        this.pageList = caclulate_page_list(this.order, getFirstPageSize());
    }

    public void CreateSportClock() {
        int[] clockItemList = new int[6];
        this.currentItemIndex = 0;
        this.currentFaceIndex = 0;
        int diagramId = getDiagram();
        if (this.isMixSport) {
            this.clockList.add(SportItemInfoWrapper.createMixSportLayout(this.mContext, this.currentSportId));
            return;
        }
        for (int clockItemSize : this.pageList) {
            SportLayoutItem clockItem = new SportLayoutItem(this.mContext, clockItemSize);
            clockItem.getLayout().add(SportItemInfoWrapper.createSystemTimeLayout(this.mContext, getTitle_num()));
            int j = 0;
            while (j < clockItemSize && this.currentItemIndex < this.order.length) {
                if (notSupportItem(this.order[this.currentItemIndex])) {
                    j--;
                } else {
                    clockItemList[j] = this.order[this.currentItemIndex];
                    if (clockItemList[j] == 1) {
                        clockItem.addItem(SportDataItemInfo.getItemView(this.mContext, clockItemList[j], this.sportItemTypeface), getItemTitle(clockItemList[j]));
                    }
                }
                j++;
                this.currentItemIndex++;
            }
            for (j = 0; j < clockItemSize; j++) {
                if (clockItemList[j] != 1) {
                    SlptViewComponent view = SportDataItemInfo.getItemView(this.mContext, clockItemList[j], this.sportItemTypeface);
                    int currentLayoutIndex = clockItem.getCurrentLayoutIndex();
                    clockItem.addItem(view, getItemTitle(clockItemList[j]));
                    if (clockItemList[j] == 5) {
                        clockItem.addHeartRateViewBg(currentLayoutIndex);
                    }
                    clockItemList[j] = 0;
                }
            }
            this.clockList.add(clockItem.getSlptClock());
        }
        switch (diagramId) {
            case 0:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramHeartRate(this.mContext, getDiagram_num(), null)));
                break;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramPace(this.mContext, getDiagram_num(), null)));
                break;
            case 2:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramAltitude(this.mContext, getDiagram_num(), null)));
                break;
            case 3:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramStrokeSpeed(this.mContext, getDiagram_num(), null)));
                break;
            case 4:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramSpeed(this.mContext, getDiagram_num(), null)));
                break;
            case 5:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramSwimPace(this.mContext, getDiagram_num(), null)));
                break;
        }
        if (!needDistanceDiagram()) {
            return;
        }
        if (this.currentSportId == 15) {
            if (Util.needSwimYardUnit(this.mContext)) {
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramDistanceYard(this.mContext, getDiagram_num(), null)));
            } else {
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramDistanceM(this.mContext, getDiagram_num(), null)));
            }
        } else if (this.currentSportId == 11) {
            this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramSkiingDistance(this.mContext, SportItemInfoWrapper.getSportRegular42Typeface(this.mContext), null)));
        } else if (Util.getMeasurement(this.mContext) == 0) {
            this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramDistanceKm(this.mContext, getDiagram_num(), null)));
        } else {
            this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramDistanceMi(this.mContext, getDiagram_num(), null)));
        }
    }

    private String getItemTitle(int id) {
        return this.itemNameArray[id];
    }

    private int getDiagram() {
        return SportGraphUtils.getSportDisplayType(this.mContext, this.currentSportId, getDefaultDiagram());
    }

    public boolean notSupportItem(int id) {
        if (id == 15) {
            return true;
        }
        for (int i : this.defOrder) {
            if (id == i) {
                return false;
            }
        }
        return true;
    }

    public int[] filter_order(int[] srcOrder, int[] defOrder) {
        int filter_count = 0;
        if (srcOrder == null || srcOrder.length == 0) {
            return null;
        }
        int i;
        for (i = 0; i < srcOrder.length; i++) {
            boolean found = false;
            for (int i2 : defOrder) {
                if (srcOrder[i] == i2) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                filter_count++;
                srcOrder[i] = -1;
            }
        }
        int[] array = new int[(srcOrder.length - filter_count)];
        int j = 0;
        for (i = 0; i < srcOrder.length; i++) {
            if (srcOrder[i] != -1) {
                int j2 = j + 1;
                array[j] = srcOrder[i];
                j = j2;
            }
        }
        return array;
    }

    private int[] caclulate_page_list(int[] order, int firstPageSize) {
        if (firstPageSize == 6) {
            if (order.length > 15) {
                return array6[14];
            }
            return array6[order.length - 1];
        } else if (order.length > 15) {
            return array4[14];
        } else {
            return array4[order.length - 1];
        }
    }

    public byte[][] getDiagram_num() {
        return this.diagram_num;
    }

    public byte[][] getTitle_num() {
        return this.title_num;
    }

    public void setOrder(int[] order) {
        this.order = order;
    }

    public ArrayList<SlptClock> getLayoutClock() {
        return this.clockList;
    }

    private int getFirstPageSize() {
        return SportItemInfoWrapper.getSlptSportItemCount(this.mContext) == 4 ? 4 : 6;
    }
}
