package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.util.ArrayList;

public class SportLayoutClockTrain {
    private ArrayList<SlptClock> clockList = new ArrayList();
    private byte[][] diagram_num;
    private SportTrainInfo info;
    private String[] itemNameArray;
    private String[] itemTrainNameArray;
    private Context mContext;
    private int[] order;
    private Typeface sportItemTypeface;
    private byte[][] title_num;
    private byte[][] train_num;

    private class positionInfo {
        byte alignY;
        int height;
        int width;
        int f15x;
        int f16y;

        private void setInfo(int x, int y, int width, int height) {
            this.f15x = x;
            this.f16y = y;
            this.width = width;
            this.height = height;
        }
    }

    public SportLayoutClockTrain(Context mContext, SportTrainInfo info) {
        this.mContext = mContext;
        this.title_num = SportItemInfoWrapper.getTitleNum(mContext);
        this.train_num = SportItemInfoWrapper.getTrainNum(mContext);
        this.sportItemTypeface = SportItemInfoWrapper.getSportRegularTypeface(mContext);
        this.info = info;
        this.itemNameArray = SportDataItemInfo.getDateItemArray(mContext);
        this.itemTrainNameArray = SportDataItemInfo.getTrainDateItemArray(mContext);
        this.diagram_num = SportItemInfoWrapper.getDiagramNum(mContext);
        Log.d("LayoutClockTrain", "order string " + info.getArray().toString());
        setOrder(info.getArray());
    }

    public void CreateSportClock() {
        String themeDir = SportLayoutTheme.getSlptThemesDir(this.mContext, false);
        SlptAbsoluteLayout layout1 = new SlptAbsoluteLayout();
        SlptAbsoluteLayout layout2 = new SlptAbsoluteLayout();
        if (this.order == null || this.order.length < 4) {
            Log.e("LayoutClockTrain", "sport train info order error!");
            return;
        }
        SlptPictureView bg1 = new SlptPictureView();
        if (this.info.getTrainMode() == 0) {
            bg1.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, themeDir + "bg/sport_train_bg_2.png"));
        } else {
            bg1.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, themeDir + "bg/2.png"));
        }
        layout1.add(bg1);
        layout1.add(SportItemInfoWrapper.createSystemTimeLayout(this.mContext, getTitle_num()));
        int currentId = this.order[0];
        if (this.order[0] == 250 || this.order[0] == 253 || this.order[0] == 254) {
            SlptViewComponent tipsLayout = SportItemInfoWrapper.createSportTrainTips(this.mContext, this.order[0], this.train_num, this.info);
            tipsLayout.setStart(0, 82);
            tipsLayout.setRect(320, 40);
            tipsLayout.alignX = (byte) 2;
            layout1.add(tipsLayout);
        }
        layout1.add(getItem(currentId, 0, 0, 2));
        layout1.add(getItem(this.order[1], 0, 1, 2));
        int clockSize = this.order.length - 2;
        SlptPictureView bg2 = new SlptPictureView();
        if (clockSize >= 3) {
            clockSize = 3;
            bg2.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, themeDir + "bg/3.png"));
        } else {
            bg2.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, themeDir + "bg/2.png"));
        }
        layout2.add(bg2);
        layout2.add(SportItemInfoWrapper.createSystemTimeLayout(this.mContext, getTitle_num()));
        layout2.add(getItem(this.order[2], 1, 0, clockSize));
        layout2.add(getItem(this.order[3], 1, 1, clockSize));
        if (clockSize == 3) {
            layout2.add(getItem(this.order[4], 1, 2, clockSize));
        }
        this.clockList.add(new SlptClock(layout1));
        this.clockList.add(new SlptClock(layout2));
        switch (getDiagram()) {
            case 0:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramHeartRate(this.mContext, this.diagram_num, null)));
                break;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramPace(this.mContext, this.diagram_num, null)));
                break;
            case 2:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramAltitude(this.mContext, this.diagram_num, null)));
                break;
            case 3:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramStrokeSpeed(this.mContext, this.diagram_num, null)));
                break;
            case 4:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramSpeed(this.mContext, this.diagram_num, null)));
                break;
            case 5:
                this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramSwimPace(this.mContext, this.diagram_num, null)));
                break;
        }
        if (Util.getMeasurement(this.mContext) == 0) {
            this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramDistanceKm(this.mContext, this.diagram_num, null)));
        } else {
            this.clockList.add(new SlptClock(SportItemInfoWrapper.createDiagramDistanceMi(this.mContext, this.diagram_num, null)));
        }
    }

    public void addHeartRateViewBg(SlptLayout linearLayout, int clockindex, int itemIndex, int clockSize) {
        String str;
        SlptViewComponent warmUpLayout;
        SlptLinearLayout fatBrunLayout;
        SlptLinearLayout heartLungLayout;
        SlptViewComponent strengthLayout;
        SlptLinearLayout anaboicLayout;
        String themeDir = SportLayoutTheme.getSlptThemesDir(this.mContext, false);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptViewComponent warmUpBg = new SlptPictureView();
        SlptPictureView fatBrunBg = new SlptPictureView();
        SlptPictureView heartLungBg = new SlptPictureView();
        SlptViewComponent strengthBg = new SlptPictureView();
        SlptPictureView anaboicBg = new SlptPictureView();
        byte[] mem1 = SimpleFile.readFileFromAssets(this.mContext, 1 != null ? themeDir + "bg/sport_slpt_heart_warm_l.png" : themeDir + "bg/sport_slpt_heart_warm.png");
        byte[] mem2 = SimpleFile.readFileFromAssets(this.mContext, 1 != null ? themeDir + "bg/sport_slpt_heart_burn_l.png" : themeDir + "bg/sport_slpt_heart_burn.png");
        byte[] mem3 = SimpleFile.readFileFromAssets(this.mContext, 1 != null ? themeDir + "bg/sport_slpt_heart_lung_l.png" : themeDir + "bg/sport_slpt_heart_lung.png");
        byte[] mem4 = SimpleFile.readFileFromAssets(this.mContext, 1 != null ? themeDir + "bg/sport_slpt_heart_strength_l.png" : themeDir + "bg/sport_slpt_heart_strength.png");
        Context context = this.mContext;
        if (1 != null) {
            str = themeDir + "bg/sport_slpt_heart_anaboic_l.png";
        } else {
            str = themeDir + "bg/sport_slpt_heart_anaboic.png";
        }
        byte[] mem5 = SimpleFile.readFileFromAssets(context, str);
        warmUpBg.setImagePicture(mem1);
        fatBrunBg.setImagePicture(mem2);
        heartLungBg.setImagePicture(mem3);
        strengthBg.setImagePicture(mem4);
        anaboicBg.setImagePicture(mem5);
        String warnUp = this.mContext.getResources().getString(R.string.warmup);
        String fatburn = this.mContext.getResources().getString(R.string.fatburn);
        String heartlung = this.mContext.getResources().getString(R.string.heartlung);
        String strength = this.mContext.getResources().getString(R.string.strength);
        String anaerobic = this.mContext.getResources().getString(R.string.anaerobic);
        if (SportLayoutTheme.getSlptThemesId(this.mContext) == 0) {
            warmUpLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, warnUp, (short) 0, false);
            fatBrunLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, fatburn, (short) 0, true);
            heartLungLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, heartlung, (short) 0, true);
            strengthLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, strength, (short) 0, true);
            anaboicLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, anaerobic, (short) 0, false);
        } else {
            SlptLinearLayout warmUpLayout2 = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, warnUp, (short) 0, false);
            fatBrunLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, fatburn, (short) 0, false);
            heartLungLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, heartlung, (short) 0, true);
            SlptLinearLayout strengthLayout2 = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, strength, (short) 0, true);
            anaboicLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, anaerobic, (short) 0, false);
        }
        SlptSportUtil.setBgHeartrateWarmUp(warmUpBg);
        SlptSportUtil.setBgHeartrateWarmUp(warmUpLayout);
        SlptSportUtil.setBgHeartrateFatBurn(fatBrunBg);
        SlptSportUtil.setBgHeartrateFatBurn(fatBrunLayout);
        SlptSportUtil.setBgHeartrateHeartLung(heartLungBg);
        SlptSportUtil.setBgHeartrateHeartLung(heartLungLayout);
        SlptSportUtil.setHeartrateStrength(strengthBg);
        SlptSportUtil.setHeartrateStrength(strengthLayout);
        SlptSportUtil.setBgHeartrateAnaerobic(anaboicBg);
        SlptSportUtil.setBgHeartrateAnaerobic(anaboicLayout);
        layout.add(warmUpBg);
        layout.add(fatBrunBg);
        layout.add(heartLungBg);
        layout.add(strengthBg);
        layout.add(anaboicBg);
        layout.add(warmUpLayout);
        layout.add(fatBrunLayout);
        layout.add(heartLungLayout);
        layout.add(strengthLayout);
        layout.add(anaboicLayout);
        if (clockindex == 0) {
            setClockLayout0(null, warmUpBg, itemIndex);
            setClockLayout0(null, fatBrunBg, itemIndex);
            setClockLayout0(null, heartLungBg, itemIndex);
            setClockLayout0(null, strengthBg, itemIndex);
            setClockLayout0(null, anaboicBg, itemIndex);
            setClockLayout0(null, warmUpLayout, itemIndex);
            setClockLayout0(null, fatBrunLayout, itemIndex);
            setClockLayout0(null, heartLungLayout, itemIndex);
            setClockLayout0(null, strengthLayout, itemIndex);
            setClockLayout0(null, anaboicLayout, itemIndex);
        } else {
            setClockLayout1(null, warmUpBg, itemIndex, clockSize);
            setClockLayout1(null, fatBrunBg, itemIndex, clockSize);
            setClockLayout1(null, heartLungBg, itemIndex, clockSize);
            setClockLayout1(null, strengthBg, itemIndex, clockSize);
            setClockLayout1(null, anaboicBg, itemIndex, clockSize);
            setClockLayout1(null, warmUpLayout, itemIndex, clockSize);
            setClockLayout1(null, fatBrunLayout, itemIndex, clockSize);
            setClockLayout1(null, heartLungLayout, itemIndex, clockSize);
            setClockLayout1(null, strengthLayout, itemIndex, clockSize);
            setClockLayout1(null, anaboicLayout, itemIndex, clockSize);
        }
        linearLayout.add(layout);
    }

    SlptLayout getItem(int id, int clockIndex, int itemIndex, int clockSize) {
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptViewComponent value = SportDataItemInfo.getItemView(this.mContext, id, this.sportItemTypeface, this.info);
        SlptViewComponent title = SportItemInfoWrapper.getFont16StringView(this.mContext, getItemTitle(id), (short) 0);
        if (clockIndex == 0) {
            setClockLayout0(value, title, itemIndex);
        } else {
            setClockLayout1(value, title, itemIndex, clockSize);
        }
        layout.add(value);
        layout.add(title);
        if (id == 5) {
            addHeartRateViewBg(layout, clockIndex, itemIndex, clockSize);
        }
        return layout;
    }

    void setClockLayout0(SlptViewComponent value, SlptViewComponent title, int itemIndex) {
        positionInfo titleInfo = new positionInfo();
        positionInfo valueInfo = new positionInfo();
        if (itemIndex == 0) {
            if (this.info.getTrainMode() == 0) {
                valueInfo.setInfo(0, 123, 320, 72);
                titleInfo.setInfo(0, 195, 320, 24);
                valueInfo.alignY = (byte) 0;
            } else {
                valueInfo.setInfo(0, 77, 320, 58);
                titleInfo.setInfo(0, 153, 320, 24);
                valueInfo.alignY = (byte) 1;
            }
        } else if (this.info.getTrainMode() == 0) {
            valueInfo.setInfo(0, 222, 320, 54);
            titleInfo.setInfo(0, 273, 320, 24);
            valueInfo.alignY = (byte) 1;
        } else {
            valueInfo.setInfo(0, 198, 320, 58);
            titleInfo.setInfo(0, 273, 320, 24);
            valueInfo.alignY = (byte) 1;
        }
        if (title != null) {
            title.setStart(titleInfo.f15x, titleInfo.f16y);
            title.setRect(titleInfo.width, titleInfo.height);
            title.alignX = (byte) 2;
            title.alignY = (byte) 1;
        }
        if (value != null) {
            value.setStart(valueInfo.f15x, valueInfo.f16y);
            value.setRect(valueInfo.width, valueInfo.height);
            value.alignX = (byte) 2;
            value.alignY = valueInfo.alignY;
        }
    }

    void setClockLayout1(SlptViewComponent value, SlptViewComponent title, int itemIndex, int clockSize) {
        positionInfo titleInfo = new positionInfo();
        positionInfo valueInfo = new positionInfo();
        if (clockSize == 2) {
            if (itemIndex == 0) {
                valueInfo.setInfo(0, 77, 320, 58);
                titleInfo.setInfo(0, 153, 320, 24);
            } else {
                valueInfo.setInfo(0, 198, 320, 58);
                titleInfo.setInfo(0, 271, 320, 24);
            }
        } else if (itemIndex == 0) {
            valueInfo.setInfo(0, 57, 320, 58);
            titleInfo.setInfo(0, 118, 320, 24);
        } else if (itemIndex == 1) {
            valueInfo.setInfo(0, 139, 320, 58);
            titleInfo.setInfo(0, 198, 320, 24);
        } else {
            valueInfo.setInfo(0, 219, 320, 58);
            titleInfo.setInfo(0, 276, 320, 24);
        }
        if (title != null) {
            title.setStart(titleInfo.f15x, titleInfo.f16y);
            title.setRect(titleInfo.width, titleInfo.height);
            title.alignX = (byte) 2;
            title.alignY = (byte) 2;
        }
        if (value != null) {
            value.setStart(valueInfo.f15x, valueInfo.f16y);
            value.setRect(valueInfo.width, valueInfo.height);
            value.alignX = (byte) 2;
        }
    }

    private String getItemTitle(int id) {
        if (id >= 250) {
            return this.itemTrainNameArray[id - 250];
        }
        return this.itemNameArray[id];
    }

    private int getDiagram() {
        return SportGraphUtils.getSportDisplayType(this.mContext, 1, 0);
    }

    public byte[][] getTitle_num() {
        return this.title_num;
    }

    public void setOrder(int[] order) {
        this.order = order;
    }

    public ArrayList<SlptClock> getLayoutClock() {
        return this.clockList;
    }
}
