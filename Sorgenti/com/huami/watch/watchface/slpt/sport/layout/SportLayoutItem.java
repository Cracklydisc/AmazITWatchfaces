package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class SportLayoutItem {
    private int ItemSize = 0;
    private int currentIndex = 0;
    public SlptAbsoluteLayout linearLayout = new SlptAbsoluteLayout();
    private Context mContext;

    private class positionInfo {
        int height;
        int width;
        int f17x;
        int f18y;

        private void setInfo(int x, int y, int width, int height) {
            this.f17x = x;
            this.f18y = y;
            this.width = width;
            this.height = height;
        }
    }

    public SportLayoutItem(Context mContext, int size, boolean isMixMode) {
        this.mContext = mContext;
        this.ItemSize = size;
        this.currentIndex = 1;
        this.linearLayout.add(SportItemInfoWrapper.createBackGroudLayout(mContext, size, isMixMode));
    }

    public SportLayoutItem(Context mContext, int size) {
        this.mContext = mContext;
        this.ItemSize = size;
        this.currentIndex = 1;
        this.linearLayout.add(SportItemInfoWrapper.createBackGroudLayout(mContext, size, false));
    }

    public SlptLayout getLayout() {
        return this.linearLayout;
    }

    private void setLayoutPosition6(SlptViewComponent layout, SlptViewComponent titleView, int index) {
        positionInfo layoutInfo = new positionInfo();
        positionInfo titleInfo = new positionInfo();
        int size = 0;
        if (titleView instanceof SlptLayout) {
            size = ((SlptLayout) titleView).size();
        }
        switch (index) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                layoutInfo.setInfo(0, 62, 320, 54);
                titleInfo.setInfo(0, 118, 320, 24);
                break;
            case 2:
                layoutInfo.setInfo(0, 142, 105, 54);
                titleInfo.setInfo(0, 198, 105, 24);
                break;
            case 3:
                layoutInfo.setInfo(107, 142, 105, 54);
                titleInfo.setInfo(107, 198, 105, 24);
                break;
            case 4:
                layoutInfo.setInfo(215, 142, 105, 54);
                titleInfo.setInfo(215, 198, 105, 24);
                break;
            case 5:
                layoutInfo.setInfo(54, 222, 105, 54);
                if (size >= 4 && !Util.needEnAssets()) {
                    titleInfo.setInfo(64, 276, 105, 24);
                    break;
                } else {
                    titleInfo.setInfo(54, 276, 105, 24);
                    break;
                }
            case 6:
                layoutInfo.setInfo(161, 222, 105, 54);
                if (size >= 4 && !Util.needEnAssets()) {
                    titleInfo.setInfo(151, 276, 105, 24);
                    break;
                } else {
                    titleInfo.setInfo(161, 276, 105, 24);
                    break;
                }
                break;
        }
        if (layout != null) {
            layout.setStart(layoutInfo.f17x, layoutInfo.f18y);
            layout.setRect(layoutInfo.width, layoutInfo.height);
            layout.alignX = (byte) 2;
            layout.alignY = (byte) 1;
        }
        if (titleView != null) {
            titleView.setStart(titleInfo.f17x, titleInfo.f18y);
            titleView.setRect(titleInfo.width, titleInfo.height);
            titleView.alignX = (byte) 2;
            titleView.alignY = (byte) 2;
        }
    }

    private void setLayoutPosition5(SlptViewComponent layout, SlptViewComponent titleView, int index) {
        positionInfo layoutInfo = new positionInfo();
        positionInfo titleInfo = new positionInfo();
        switch (index) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                layoutInfo.setInfo(0, 62, 320, 54);
                titleInfo.setInfo(0, 118, 320, 24);
                break;
            case 2:
                layoutInfo.setInfo(0, 142, 105, 54);
                titleInfo.setInfo(0, 198, 105, 24);
                break;
            case 3:
                layoutInfo.setInfo(107, 142, 106, 54);
                titleInfo.setInfo(107, 198, 106, 24);
                break;
            case 4:
                layoutInfo.setInfo(215, 142, 105, 54);
                titleInfo.setInfo(215, 198, 105, 24);
                break;
            case 5:
                layoutInfo.setInfo(0, 222, 320, 54);
                titleInfo.setInfo(0, 276, 320, 24);
                break;
        }
        if (layout != null) {
            layout.setStart(layoutInfo.f17x, layoutInfo.f18y);
            layout.setRect(layoutInfo.width, layoutInfo.height);
            layout.alignX = (byte) 2;
            layout.alignY = (byte) 1;
        }
        if (titleView != null) {
            titleView.setStart(titleInfo.f17x, titleInfo.f18y);
            titleView.setRect(titleInfo.width, titleInfo.height);
            titleView.alignX = (byte) 2;
            titleView.alignY = (byte) 2;
        }
    }

    private void setLayoutPosition4(SlptViewComponent layout, SlptViewComponent titleView, int index) {
        positionInfo layoutInfo = new positionInfo();
        positionInfo titleInfo = new positionInfo();
        switch (index) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                layoutInfo.setInfo(0, 62, 320, 54);
                titleInfo.setInfo(0, 118, 320, 24);
                break;
            case 2:
                layoutInfo.setInfo(0, 142, 159, 54);
                titleInfo.setInfo(0, 198, 159, 24);
                break;
            case 3:
                layoutInfo.setInfo(161, 142, 159, 54);
                titleInfo.setInfo(161, 198, 159, 24);
                break;
            case 4:
                layoutInfo.setInfo(0, 222, 320, 54);
                titleInfo.setInfo(0, 276, 320, 24);
                break;
        }
        if (layout != null) {
            layout.setStart(layoutInfo.f17x, layoutInfo.f18y);
            layout.setRect(layoutInfo.width, layoutInfo.height);
            layout.alignX = (byte) 2;
            layout.alignY = (byte) 1;
        }
        if (titleView != null) {
            titleView.setStart(titleInfo.f17x, titleInfo.f18y);
            titleView.setRect(titleInfo.width, titleInfo.height);
            titleView.alignX = (byte) 2;
            titleView.alignY = (byte) 2;
        }
    }

    private void setLayoutPosition3(SlptViewComponent layout, SlptViewComponent titleView, int index) {
        positionInfo layoutInfo = new positionInfo();
        positionInfo titleInfo = new positionInfo();
        switch (index) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                layoutInfo.setInfo(0, 62, 320, 54);
                titleInfo.setInfo(0, 118, 320, 24);
                break;
            case 2:
                layoutInfo.setInfo(0, 142, 320, 54);
                titleInfo.setInfo(0, 198, 320, 24);
                break;
            case 3:
                layoutInfo.setInfo(0, 222, 320, 54);
                titleInfo.setInfo(0, 276, 320, 24);
                break;
        }
        if (layout != null) {
            layout.setStart(layoutInfo.f17x, layoutInfo.f18y);
            layout.setRect(layoutInfo.width, layoutInfo.height);
            layout.alignX = (byte) 2;
            layout.alignY = (byte) 1;
        }
        if (titleView != null) {
            titleView.setStart(titleInfo.f17x, titleInfo.f18y);
            titleView.setRect(titleInfo.width, titleInfo.height);
            titleView.alignX = (byte) 2;
            titleView.alignY = (byte) 2;
        }
    }

    private void setLayoutPosition2(SlptViewComponent layout, SlptViewComponent titleView, int index) {
        positionInfo layoutInfo = new positionInfo();
        positionInfo titleInfo = new positionInfo();
        switch (index) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                layoutInfo.setInfo(0, 87, 320, 48);
                titleInfo.setInfo(0, 157, 320, 24);
                break;
            case 2:
                layoutInfo.setInfo(0, 208, 320, 48);
                titleInfo.setInfo(0, 274, 320, 24);
                break;
        }
        if (layout != null) {
            layout.setStart(layoutInfo.f17x, layoutInfo.f18y);
            layout.setRect(layoutInfo.width, layoutInfo.height);
            layout.alignX = (byte) 2;
            layout.alignY = (byte) 1;
        }
        if (titleView != null) {
            titleView.setStart(titleInfo.f17x, titleInfo.f18y);
            titleView.setRect(titleInfo.width, titleInfo.height);
            titleView.alignX = (byte) 2;
            titleView.alignY = (byte) 2;
        }
    }

    private void setLayoutPosition(SlptViewComponent layout, SlptViewComponent titleView, int size, int index) {
        switch (size) {
            case 2:
                setLayoutPosition2(layout, titleView, index);
                return;
            case 3:
                setLayoutPosition3(layout, titleView, index);
                return;
            case 4:
                setLayoutPosition4(layout, titleView, index);
                return;
            case 5:
                setLayoutPosition5(layout, titleView, index);
                return;
            case 6:
                setLayoutPosition6(layout, titleView, index);
                return;
            default:
                return;
        }
    }

    public void addHeartRateViewBg(int layoutIndex) {
        String str;
        SlptViewComponent warmUpLayout;
        SlptLinearLayout fatBrunLayout;
        SlptLinearLayout heartLungLayout;
        SlptViewComponent strengthLayout;
        SlptLinearLayout anaboicLayout;
        String themeDir = SportLayoutTheme.getSlptThemesDir(this.mContext, false);
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptViewComponent warmUpBg = new SlptPictureView();
        SlptPictureView fatBrunBg = new SlptPictureView();
        SlptPictureView heartLungBg = new SlptPictureView();
        SlptViewComponent strengthBg = new SlptPictureView();
        SlptPictureView anaboicBg = new SlptPictureView();
        boolean bg_length = true;
        if (this.ItemSize == 4 && (this.currentIndex == 2 || this.currentIndex == 3)) {
            bg_length = false;
        }
        if (this.ItemSize == 6 && (this.currentIndex == 2 || this.currentIndex == 3 || this.currentIndex == 4 || this.currentIndex == 5 || this.currentIndex == 6)) {
            bg_length = false;
        }
        byte[] mem1 = SimpleFile.readFileFromAssets(this.mContext, bg_length ? themeDir + "bg/sport_slpt_heart_warm_l.png" : themeDir + "bg/sport_slpt_heart_warm.png");
        byte[] mem2 = SimpleFile.readFileFromAssets(this.mContext, bg_length ? themeDir + "bg/sport_slpt_heart_burn_l.png" : themeDir + "bg/sport_slpt_heart_burn.png");
        byte[] mem3 = SimpleFile.readFileFromAssets(this.mContext, bg_length ? themeDir + "bg/sport_slpt_heart_lung_l.png" : themeDir + "bg/sport_slpt_heart_lung.png");
        byte[] mem4 = SimpleFile.readFileFromAssets(this.mContext, bg_length ? themeDir + "bg/sport_slpt_heart_strength_l.png" : themeDir + "bg/sport_slpt_heart_strength.png");
        Context context = this.mContext;
        if (bg_length) {
            str = themeDir + "bg/sport_slpt_heart_anaboic_l.png";
        } else {
            str = themeDir + "bg/sport_slpt_heart_anaboic.png";
        }
        byte[] mem5 = SimpleFile.readFileFromAssets(context, str);
        warmUpBg.setImagePicture(mem1);
        fatBrunBg.setImagePicture(mem2);
        heartLungBg.setImagePicture(mem3);
        strengthBg.setImagePicture(mem4);
        anaboicBg.setImagePicture(mem5);
        String warnUp = this.mContext.getResources().getString(R.string.warmup);
        String fatburn = this.mContext.getResources().getString(R.string.fatburn);
        String heartlung = this.mContext.getResources().getString(R.string.heartlung);
        String strength = this.mContext.getResources().getString(R.string.strength);
        String anaerobic = this.mContext.getResources().getString(R.string.anaerobic);
        if (SportLayoutTheme.getSlptThemesId(this.mContext) == 0) {
            warmUpLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, warnUp, (short) 0, false);
            fatBrunLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, fatburn, (short) 0, true);
            heartLungLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, heartlung, (short) 0, true);
            strengthLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, strength, (short) 0, true);
            anaboicLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, anaerobic, (short) 0, false);
        } else {
            SlptLinearLayout warmUpLayout2 = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, warnUp, (short) 0, false);
            fatBrunLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, fatburn, (short) 0, false);
            heartLungLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, heartlung, (short) 0, true);
            SlptLinearLayout strengthLayout2 = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, strength, (short) 0, true);
            anaboicLayout = (SlptLinearLayout) SportItemInfoWrapper.getFont16StringView(this.mContext, anaerobic, (short) 0, false);
        }
        SlptSportUtil.setBgHeartrateWarmUp(warmUpBg);
        SlptSportUtil.setBgHeartrateWarmUp(warmUpLayout);
        SlptSportUtil.setBgHeartrateFatBurn(fatBrunBg);
        SlptSportUtil.setBgHeartrateFatBurn(fatBrunLayout);
        SlptSportUtil.setBgHeartrateHeartLung(heartLungBg);
        SlptSportUtil.setBgHeartrateHeartLung(heartLungLayout);
        SlptSportUtil.setHeartrateStrength(strengthBg);
        SlptSportUtil.setHeartrateStrength(strengthLayout);
        SlptSportUtil.setBgHeartrateAnaerobic(anaboicBg);
        SlptSportUtil.setBgHeartrateAnaerobic(anaboicLayout);
        layout.add(warmUpBg);
        layout.add(fatBrunBg);
        layout.add(heartLungBg);
        layout.add(strengthBg);
        layout.add(anaboicBg);
        layout.add(warmUpLayout);
        layout.add(fatBrunLayout);
        layout.add(heartLungLayout);
        layout.add(strengthLayout);
        layout.add(anaboicLayout);
        setLayoutPosition(null, warmUpBg, this.ItemSize, layoutIndex);
        setLayoutPosition(null, fatBrunBg, this.ItemSize, layoutIndex);
        setLayoutPosition(null, heartLungBg, this.ItemSize, layoutIndex);
        setLayoutPosition(null, strengthBg, this.ItemSize, layoutIndex);
        setLayoutPosition(null, anaboicBg, this.ItemSize, layoutIndex);
        setLayoutPosition(null, warmUpLayout, this.ItemSize, layoutIndex);
        setLayoutPosition(null, fatBrunLayout, this.ItemSize, layoutIndex);
        setLayoutPosition(null, heartLungLayout, this.ItemSize, layoutIndex);
        setLayoutPosition(null, strengthLayout, this.ItemSize, layoutIndex);
        setLayoutPosition(null, anaboicLayout, this.ItemSize, layoutIndex);
        this.linearLayout.add(layout);
    }

    public int getCurrentLayoutIndex() {
        return this.currentIndex;
    }

    public void addItem(SlptViewComponent view, String title) {
        if (view != null) {
            SlptViewComponent titleView = SportItemInfoWrapper.getFont16StringView(this.mContext, title, (short) 0);
            this.linearLayout.add(view);
            this.linearLayout.add(titleView);
            setLayoutPosition(view, titleView, this.ItemSize, this.currentIndex);
            this.currentIndex++;
        }
    }

    public void addView(SlptViewComponent view) {
        if (view != null) {
            this.linearLayout.add(view);
        }
    }

    public SlptClock getSlptClock() {
        return new SlptClock(this.linearLayout);
    }
}
