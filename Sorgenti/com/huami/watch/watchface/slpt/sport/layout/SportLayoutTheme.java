package com.huami.watch.watchface.slpt.sport.layout;

import android.content.Context;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import com.huami.watch.watchface.WatchfaceApplication;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;

public class SportLayoutTheme {
    public static void setSlptThemes(Context mContext, int value) {
        if (value == 0 || value == 1) {
            ((WatchfaceApplication) mContext.getApplicationContext()).setSlptThemes(value);
        }
    }

    public static String getSlptThemesDir(Context mContext, boolean needEnRes) {
        return ((WatchfaceApplication) mContext.getApplicationContext()).getSlptThemesDir(needEnRes);
    }

    public static int getSlptThemesId(Context mContext) {
        return ((WatchfaceApplication) mContext.getApplicationContext()).getSlptThems();
    }

    public static void setViewBgColor(Context mContext, SlptViewComponent view) {
        int themeId = getSlptThemesId(mContext);
        if (themeId == 1) {
            view.background.color = -328966;
        }
        if (themeId == 0) {
            view.background.color = -16777216;
        }
    }

    public static int getSelectTheme(Context mContext) {
        try {
            return Secure.getInt(mContext.getContentResolver(), "sport_background_mode");
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void initSportTheme(Context mContext) {
        setSlptThemes(mContext, getSelectTheme(mContext));
    }
}
