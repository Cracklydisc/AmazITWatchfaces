package com.huami.watch.watchface.slpt.sport.layout;

import com.huami.watch.watchface.util.Util;
import java.math.BigDecimal;

public class SportTrainInfo {
    private int[] array;
    private int currentDistance;
    private int currentTime;
    private int startDistance;
    private int startTime;
    private int totalDistance;
    private int totalTime;
    private int trainMode;

    public int[] getArray() {
        return this.array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public int getCurrentDistance() {
        return this.currentDistance;
    }

    public void setCurrentDistance(int currentDistance) {
        this.currentDistance = currentDistance;
    }

    public int getTotalDistance() {
        return this.totalDistance;
    }

    public void setTotalDistance(int totalDistance) {
        this.totalDistance = totalDistance;
    }

    public int getCurrentTime() {
        return this.currentTime;
    }

    public void setCurrentTime(int currentTime) {
        this.currentTime = currentTime;
    }

    public int getTotalTime() {
        return this.totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getStartDistance() {
        return this.startDistance;
    }

    public void setStartDistance(int startDistance) {
        this.startDistance = startDistance;
    }

    public int getStartTime() {
        return this.startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getTrainMode() {
        return this.trainMode;
    }

    public void setTrainMode(int trainMode) {
        this.trainMode = trainMode;
    }

    public static String formatDistance(int unit, int meter) {
        float distance = Util.getDistance(unit, ((float) meter) / 1000.0f);
        BigDecimal num = new BigDecimal((double) distance);
        float count;
        if (distance < 100.0f) {
            count = num.setScale(2, 1).floatValue();
            return String.format("%.2f", new Object[]{Float.valueOf(count)});
        } else if (distance > 999.0f) {
            count = num.setScale(0, 1).floatValue();
            return String.format("%d", new Object[]{Integer.valueOf((int) Math.ceil((double) count))});
        } else {
            count = num.setScale(1, 1).floatValue();
            return String.format("%.1f", new Object[]{Float.valueOf(count)});
        }
    }

    public static String formatCurrentTime(int seconds) {
        int second = seconds % 60;
        return String.format("%02d:%02d", new Object[]{Integer.valueOf(seconds / 60), Integer.valueOf(second)}).toString();
    }

    public static String formatTotalTime(int seconds) {
        return String.format("%d", new Object[]{Integer.valueOf(seconds / 60)}).toString();
    }

    public String toString() {
        String str;
        StringBuilder builder = new StringBuilder();
        StringBuilder append = builder.append("<").append("item array:");
        if (this.array == null) {
            str = null;
        } else {
            str = this.array.toString();
        }
        append.append(str).append("\ncurrent Distance:").append(this.currentDistance).append("\ncurrent time: ").append(this.currentTime).append("\ntotal Distance : ").append(this.totalDistance).append("\ntotal time : ").append(this.totalTime).append("\nstart distance : ").append(this.startDistance).append("\nstart time : ").append(this.startTime).append("\ntrain mode : ").append(this.trainMode).append(">");
        return builder.toString();
    }
}
