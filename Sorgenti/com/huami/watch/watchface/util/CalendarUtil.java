package com.huami.watch.watchface.util;

import android.content.res.Resources;
import android.text.TextUtils;
import com.huami.watch.watchface.analogyellow.R;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.json.JSONException;
import org.json.JSONObject;

public class CalendarUtil {
    private final long[] LUNAR_INFO = new long[]{19416, 19168, 42352, 21717, 53856, 55632, 91476, 22176, 39632, 21970, 19168, 42422, 42192, 53840, 119381, 46400, 54944, 44450, 38320, 84343, 18800, 42160, 46261, 27216, 27968, 109396, 11104, 38256, 21234, 18800, 25958, 54432, 59984, 28309, 23248, 11104, 100067, 37600, 116951, 51536, 54432, 120998, 46416, 22176, 107956, 9680, 37584, 53938, 43344, 46423, 27808, 46416, 86869, 19872, 42448, 83315, 21200, 43432, 59728, 27296, 44710, 43856, 19296, 43748, 42352, 21088, 62051, 55632, 23383, 22176, 38608, 19925, 19152, 42192, 54484, 53840, 54616, 46400, 46496, 103846, 38320, 18864, 43380, 42160, 45690, 27216, 27968, 44870, 43872, 38256, 19189, 18800, 25776, 29859, 59984, 27480, 21952, 43872, 38613, 37600, 51552, 55636, 54432, 55888, 30034, 22176, 43959, 9680, 37584, 51893, 43344, 46240, 47780, 44368, 21977, 19360, 42416, 86390, 21168, 43312, 31060, 27296, 44368, 23378, 19296, 42726, 42208, 53856, 60005, 54576, 23200, 30371, 38608, 19415, 19152, 42192, 118966, 53840, 54560, 56645, 46496, 22224, 21938, 18864, 42359, 42160, 43600, 111189, 27936, 44448};
    private Date baseDate = null;
    private String[] chineseMonthNumberStringArray;
    private String[] chineseNumberStringArray;
    private boolean isLoap;
    private String leafString;
    private final Calendar mCurrenCalendar = Calendar.getInstance();
    private JSONObject mHolidayJSONObject;
    private int mLuchDay;
    private int mLuchMonth;
    private int mLuchYear;
    private Resources mResources;
    private final SolarTermsUtil mSolarTermsUtil;
    private String monthString;

    private int yearDays(int year) {
        int sum = 348;
        for (int i = 32768; i > 8; i >>= 1) {
            if ((this.LUNAR_INFO[year - 1900] & ((long) i)) != 0) {
                sum++;
            }
        }
        return leapDays(year) + sum;
    }

    private int leapDays(int year) {
        if (leapMonth(year) == 0) {
            return 0;
        }
        if ((this.LUNAR_INFO[year - 1900] & 65536) != 0) {
            return 30;
        }
        return 29;
    }

    private int leapMonth(int year) {
        return (int) (this.LUNAR_INFO[year - 1900] & 15);
    }

    private int monthDays(int year, int month) {
        if ((this.LUNAR_INFO[year - 1900] & ((long) (65536 >> month))) == 0) {
            return 29;
        }
        return 30;
    }

    public CalendarUtil(Resources resources) {
        this.mResources = resources;
        this.mSolarTermsUtil = new SolarTermsUtil(resources);
        getHolidayJson(resources);
        SimpleDateFormat chineseDateFormat = new SimpleDateFormat(resources.getString(R.string.quick_settings_date_chinese));
        this.chineseNumberStringArray = resources.getStringArray(R.array.quick_settings_status_chineseNumber);
        this.chineseMonthNumberStringArray = resources.getStringArray(R.array.quick_settings_status_chineseMonthNumber);
        this.leafString = resources.getString(R.string.quick_settings_status_leaf);
        this.monthString = resources.getString(R.string.quick_settings_status_month);
        try {
            this.baseDate = chineseDateFormat.parse(resources.getString(R.string.quick_settings_date_format));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void getHolidayJson(Resources resources) {
        JSONObject jsonObject = null;
        try {
            InputStream is = resources.getAssets().open("holiday.json");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            String jsonString = new String(buffer, "utf-8");
            is.close();
            try {
                jsonObject = new JSONObject(jsonString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.mHolidayJSONObject = jsonObject;
    }

    private String getChinaDayString(int day) throws JSONException {
        String[] chineseTen = this.mResources.getStringArray(R.array.quick_settings_status_chineseTen);
        int n = day % 10 == 0 ? 9 : (day % 10) - 1;
        if (day > 30) {
            return "";
        }
        if (day == 10) {
            return chineseTen[0] + chineseTen[1];
        }
        if (day % 10 == 0) {
            return this.chineseNumberStringArray[(day / 10) - 1] + chineseTen[1];
        }
        return chineseTen[day / 10] + this.chineseNumberStringArray[n];
    }

    public String getHolidayString() {
        this.mCurrenCalendar.setTimeInMillis(System.currentTimeMillis());
        int offset = (int) ((this.mCurrenCalendar.getTimeInMillis() - this.baseDate.getTime()) / 86400000);
        int daysOfYear = 0;
        int iYear = 1900;
        while (iYear < 2050 && offset > 0) {
            daysOfYear = yearDays(iYear);
            offset -= daysOfYear;
            iYear++;
        }
        if (offset < 0) {
            offset += daysOfYear;
            iYear--;
        }
        this.mLuchYear = iYear;
        int leapMonth = leapMonth(iYear);
        this.isLoap = false;
        int daysOfMonth = 0;
        int iMonth = 1;
        while (iMonth < 13 && offset > 0) {
            if (leapMonth <= 0 || iMonth != leapMonth + 1 || this.isLoap) {
                daysOfMonth = monthDays(this.mLuchYear, iMonth);
            } else {
                iMonth--;
                this.isLoap = true;
                daysOfMonth = leapDays(this.mLuchYear);
            }
            offset -= daysOfMonth;
            if (this.isLoap && iMonth == leapMonth + 1) {
                this.isLoap = false;
            }
            iMonth++;
        }
        if (offset == 0 && leapMonth > 0 && iMonth == leapMonth + 1) {
            if (this.isLoap) {
                this.isLoap = false;
            } else {
                this.isLoap = true;
                iMonth--;
            }
        }
        if (offset < 0) {
            offset += daysOfMonth;
            iMonth--;
        }
        this.mLuchMonth = iMonth;
        this.mLuchDay = offset + 1;
        String message = getLunarHoliday();
        if (!TextUtils.isEmpty(message)) {
            return message;
        }
        String solarTermMsg = this.mSolarTermsUtil.getSolartermsMsg(this.mCurrenCalendar.get(1), this.mCurrenCalendar.get(2), this.mCurrenCalendar.get(5));
        if (!TextUtils.isEmpty(solarTermMsg)) {
            return solarTermMsg;
        }
        String solarMsg = getSolarHoliday(this.mCurrenCalendar);
        if (!TextUtils.isEmpty(solarMsg)) {
            return solarMsg;
        }
        StringBuffer sb = new StringBuffer();
        if (this.isLoap) {
            sb.append(this.leafString);
        }
        sb.append(this.chineseMonthNumberStringArray[this.mLuchMonth - 1]).append(this.monthString);
        try {
            sb.append(getChinaDayString(this.mLuchDay));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private String getSolarHoliday(Calendar calendar) {
        String mString;
        int month = calendar.get(2) + 1;
        int day = calendar.get(5);
        int week = calendar.get(8);
        GregorianCalendar nextDate = new GregorianCalendar();
        nextDate.add(5, 7);
        if (month - 1 != nextDate.get(2)) {
        }
        if (month < 10) {
            try {
                mString = "0" + month;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        mString = "" + month;
        return new JSONObject(this.mHolidayJSONObject.getString("solarHoliday")).optString(mString + (day < 10 ? "0" + day : "" + day), "");
    }

    private String getLunarHoliday() {
        String mDayString = null;
        try {
            mDayString = new JSONObject(this.mHolidayJSONObject.getString("lunarHoliday")).optString((this.mLuchMonth < 10 ? "0" + this.mLuchMonth : "" + this.mLuchMonth) + (this.mLuchDay < 10 ? "0" + this.mLuchDay : "" + this.mLuchDay), "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (this.mLuchMonth != 12) {
            return mDayString;
        }
        if ((monthDays(this.mLuchYear, this.mLuchMonth) == 29 && this.mLuchDay == 29) || (monthDays(this.mLuchYear, this.mLuchMonth) == 30 && this.mLuchDay == 30)) {
            return this.mResources.getString(R.string.quick_settings_date_yeareve);
        }
        return mDayString;
    }
}
