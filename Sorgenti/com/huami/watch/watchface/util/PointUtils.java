package com.huami.watch.watchface.util;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;

public class PointUtils {
    public static final String TAG = PointUtils.class.getName();

    public static String getApplicationVersion(Context context) {
        if (context.getApplicationContext() == null) {
            return "NULL";
        }
        try {
            return String.valueOf(context.getApplicationContext().getPackageManager().getPackageInfo(context.getPackageName(), 16384).versionCode);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "NULL";
        }
    }
}
