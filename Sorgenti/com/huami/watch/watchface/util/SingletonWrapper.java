package com.huami.watch.watchface.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Settings.Secure;
import com.huami.watch.sensor.HmSensorHubConfigManager;

public class SingletonWrapper {
    public static final Uri CONTENT_HEART_URI = Uri.parse("content://com.huami.watch.health.heartdata");
    private static volatile SingletonWrapper INSTANCE = null;
    private final Context mContext;
    private HmSensorHubConfigManager mSensorHubConfigManager = null;

    private SingletonWrapper(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public static SingletonWrapper getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (SingletonWrapper.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SingletonWrapper(context);
                }
            }
        }
        return INSTANCE;
    }

    public static final boolean isSupportDataType(int dataType) {
        return dataType >= 0 && dataType <= 12;
    }

    public static int getLastHeartRato(Context c) {
        try {
            Cursor cur = c.getContentResolver().query(CONTENT_HEART_URI, null, null, null, "utc_time DESC LIMIT 1");
            if (cur != null) {
                while (cur.moveToNext()) {
                    int rato = cur.getInt(cur.getColumnIndex("heart_rate"));
                    if (rato < 300 && rato >= 0) {
                        cur.close();
                        return rato;
                    }
                }
            }
            cur.close();
        } catch (Exception e) {
        }
        return 0;
    }

    public int getHeartRato() {
        if (this.mSensorHubConfigManager == null) {
            this.mSensorHubConfigManager = new HmSensorHubConfigManager(this.mContext);
        }
        return this.mSensorHubConfigManager.getHeartRate();
    }

    public float getSportCalories() {
        if (this.mSensorHubConfigManager == null) {
            this.mSensorHubConfigManager = new HmSensorHubConfigManager(this.mContext);
        }
        return this.mSensorHubConfigManager.getSportCalories();
    }

    public double getTodayDistance() {
        double d = 0.0d;
        String value = Secure.getString(this.mContext.getContentResolver(), "sport_today_distance");
        if (value != null) {
            try {
                d = Double.parseDouble(value);
            } catch (NumberFormatException e) {
            }
        }
        return d;
    }

    public double getTotalDistance() {
        double d = 0.0d;
        String value = Secure.getString(this.mContext.getContentResolver(), "sport_total_distance");
        if (value != null) {
            try {
                d = Double.parseDouble(value);
            } catch (NumberFormatException e) {
            }
        }
        return d;
    }

    public int getFloorCount() {
        if (this.mSensorHubConfigManager == null) {
            this.mSensorHubConfigManager = new HmSensorHubConfigManager(this.mContext);
        }
        return this.mSensorHubConfigManager.getFloorCount();
    }
}
