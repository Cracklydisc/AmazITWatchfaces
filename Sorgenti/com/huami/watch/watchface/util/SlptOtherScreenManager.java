package com.huami.watch.watchface.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import com.ingenic.iwds.slpt.SlptClock;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.SlptClockClient.Callback;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.io.ByteArrayOutputStream;

public class SlptOtherScreenManager {
    private static SlptOtherScreenManager sInstance = null;
    Callback callback = new C03132();
    private Context mContext;
    private final Object mLock = Util.mLock;
    private SlptClock mSlptClock = null;
    private SlptClockClient mSlptClockClient = new SlptClockClient();
    private boolean serviceBinded = false;

    class C03111 implements Runnable {
        C03111() {
        }

        public void run() {
            synchronized (SlptOtherScreenManager.this.mLock) {
                if (SlptOtherScreenManager.this.mSlptClock == null) {
                } else if (!SlptOtherScreenManager.this.mSlptClockClient.lockService()) {
                    Log.i("SlptOtherScreenManager", "lock service error!");
                } else if (SlptOtherScreenManager.this.mSlptClockClient.tryEnableClock(SlptOtherScreenManager.this.mSlptClock)) {
                    SlptOtherScreenManager.this.mSlptClockClient.enableSlpt();
                    if (SlptOtherScreenManager.this.mSlptClockClient.unlockService()) {
                        return;
                    }
                    Log.i("SlptOtherScreenManager", "lock service error!");
                } else {
                    Log.i("SlptOtherScreenManager", "unable to enable watchface");
                    SlptOtherScreenManager.this.mSlptClockClient.unlockService();
                }
            }
        }
    }

    class C03132 implements Callback {

        class C03121 implements Runnable {
            C03121() {
            }

            public void run() {
                synchronized (SlptOtherScreenManager.this.mLock) {
                    if (SlptOtherScreenManager.this.mSlptClock == null) {
                    } else if (!SlptOtherScreenManager.this.mSlptClockClient.lockService()) {
                        Log.i("SlptOtherScreenManager", "lock service error!");
                    } else if (SlptOtherScreenManager.this.mSlptClockClient.tryEnableClock(SlptOtherScreenManager.this.mSlptClock)) {
                        SlptOtherScreenManager.this.mSlptClockClient.enableSlpt();
                        if (SlptOtherScreenManager.this.mSlptClockClient.unlockService()) {
                            return;
                        }
                        Log.i("SlptOtherScreenManager", "lock service error!");
                    } else {
                        Log.i("SlptOtherScreenManager", "unable to enable watchface");
                        SlptOtherScreenManager.this.mSlptClockClient.unlockService();
                    }
                }
            }
        }

        C03132() {
        }

        public void onServiceDisconnected() {
            Log.d("SlptOtherScreenManager", "slpt clock service has crashed");
            SlptOtherScreenManager.this.serviceBinded = false;
        }

        public void onServiceConnected() {
            SlptOtherScreenManager.this.serviceBinded = true;
            new Thread(new C03121()).start();
        }
    }

    private SlptOtherScreenManager(Context context) {
        this.mContext = context;
    }

    public static SlptOtherScreenManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SlptOtherScreenManager(context);
        }
        return sInstance;
    }

    public int createScreen(Intent intent) {
        switch (intent.getIntExtra("type", 0)) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.mSlptClock = createNegativeOneScreen((Bitmap) intent.getParcelableExtra("screenShot"));
                return 0;
            default:
                return -1;
        }
    }

    public void showScreen() {
        try {
            if (this.serviceBinded) {
                new Thread(new C03111()).start();
            } else {
                this.mSlptClockClient.bindService(this.mContext, "SlptOtherScreenManager", this.callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideScreen() {
        try {
            synchronized (this.mLock) {
                if (this.serviceBinded) {
                    this.mSlptClockClient.unbindService(this.mContext);
                    this.serviceBinded = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    SlptClock createNegativeOneScreen(Bitmap mBackground) {
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptPictureView bg = new SlptPictureView();
        SlptLinearLayout timelayout = new SlptLinearLayout();
        SlptHourHView hourHView = new SlptHourHView();
        SlptHourLView hourLView = new SlptHourLView();
        SlptMinuteHView minuteHView = new SlptMinuteHView();
        SlptMinuteLView minuteLView = new SlptMinuteLView();
        SlptPictureView timeSeq = new SlptPictureView();
        byte[][] num = new byte[10][];
        for (int i = 0; i < 10; i++) {
            num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("thirdScreen/negativeOne/%d.png", new Object[]{Integer.valueOf(i)}));
        }
        timeSeq.setImagePicture(SimpleFile.readFileFromAssets(this.mContext, "thirdScreen/negativeOne/colon.png"));
        timelayout.add(hourHView);
        timelayout.add(hourLView);
        timelayout.add(timeSeq);
        timelayout.add(minuteHView);
        timelayout.add(minuteLView);
        timelayout.setImagePictureArrayForAll(num);
        timelayout.setStart(0, 22);
        timelayout.setRect(320, 2147483646);
        timelayout.alignX = (byte) 2;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        mBackground.compress(CompressFormat.JPEG, 100, baos);
        bg.setImagePicture(baos.toByteArray());
        layout.add(bg);
        layout.add(timelayout);
        return new SlptClock(layout);
    }
}
