package com.huami.watch.watchface.util;

import android.content.res.Resources;
import com.huami.watch.watchface.analogyellow.R;

public class SolarTermsUtil {
    private int baseChineseDate = 11;
    private int baseChineseMonth = 11;
    private int baseChineseYear = 4597;
    private int baseDate = 1;
    private int baseIndex = 0;
    private int baseMonth = 1;
    private int baseYear = 1901;
    private int[] bigLeapMonthYears = new int[]{6, 14, 19, 25, 33, 36, 38, 41, 44, 52, 55, 79, 117, 136, 147, 150, 155, 158, 185, 193};
    private int chineseDate;
    private int chineseMonth;
    private char[] chineseMonths = new char[]{'\u0000', '\u0004', '­', '\b', 'Z', '\u0001', 'Õ', 'T', '´', '\t', 'd', '\u0005', 'Y', 'E', '', '\n', '¦', '\u0004', 'U', '$', '­', '\b', 'Z', 'b', 'Ú', '\u0004', '´', '\u0005', '´', 'U', 'R', '\r', '', '\n', 'J', '*', 'V', '\u0002', 'm', 'q', 'm', '\u0001', 'Ú', '\u0002', 'Ò', 'R', '©', '\u0005', 'I', '\r', '*', 'E', '+', '\t', 'V', '\u0001', 'µ', ' ', 'm', '\u0001', 'Y', 'i', 'Ô', '\n', '¨', '\u0005', '©', 'V', '¥', '\u0004', '+', '\t', '', '8', '¶', '\b', 'ì', 't', 'l', '\u0005', 'Ô', '\n', 'ä', 'j', 'R', '\u0005', '', '\n', 'Z', 'B', '[', '\u0004', '¶', '\u0004', '´', '\"', 'j', '\u0005', 'R', 'u', 'É', '\n', 'R', '\u0005', '5', 'U', 'M', '\n', 'Z', '\u0002', ']', '1', 'µ', '\u0002', 'j', '', 'h', '\u0005', '©', '\n', '', 'j', '*', '\u0005', '-', '\t', 'ª', 'H', 'Z', '\u0001', 'µ', '\t', '°', '9', 'd', '\u0005', '%', 'u', '', '\n', '', '\u0004', 'M', 'T', '­', '\u0004', 'Ú', '\u0004', 'Ô', 'D', '´', '\u0005', 'T', '', 'R', '\r', '', '\n', 'V', 'j', 'V', '\u0002', 'm', '\u0002', 'j', 'A', 'Ú', '\u0002', '²', '¡', '©', '\u0005', 'I', '\r', '\n', 'm', '*', '\t', 'V', '\u0001', '­', 'P', 'm', '\u0001', 'Ù', '\u0002', 'Ñ', ':', '¨', '\u0005', ')', '', '¥', '\f', '*', '\t', '', 'T', '¶', '\b', 'l', '\t', 'd', 'E', 'Ô', '\n', '¤', '\u0005', 'Q', '%', '', '\n', '*', 'r', '[', '\u0004', '¶', '\u0004', '¬', 'R', 'j', '\u0005', 'Ò', '\n', '¢', 'J', 'J', '\u0005', 'U', '', '-', '\n', 'Z', '\u0002', 'u', 'a', 'µ', '\u0002', 'j', '\u0003', 'a', 'E', '©', '\n', 'J', '\u0005', '%', '%', '-', '\t', '', 'h', 'Ú', '\b', '´', '\t', '¨', 'Y', 'T', '\u0003', '¥', '\n', '', ':', '', '\u0004', '­', '°', '­', '\u0004', 'Ú', '\u0004', 'ô', 'b', '´', '\u0005', 'T', '\u000b', 'D', ']', 'R', '\n', '', '\u0004', 'U', '\"', 'm', '\u0002', 'Z', 'q', 'Ú', '\u0002', 'ª', '\u0005', '²', 'U', 'I', '\u000b', 'J', '\n', '-', '9', '6', '\u0001', 'm', '', 'm', '\u0001', 'Ù', '\u0002', 'é', 'j', '¨', '\u0005', ')', '\u000b', '', 'L', 'ª', '\b', '¶', '\b', '´', '8', 'l', '\t', 'T', 'u', 'Ô', '\n', '¤', '\u0005', 'E', 'U', '', '\n', '', '\u0004', 'U', 'D', 'µ', '\u0004', 'j', '', 'j', '\u0005', 'Ò', '\n', '', 'j', 'J', '\u0005', 'U', '\n', '*', 'J', 'Z', '\u0002', 'µ', '\u0002', '²', '1', 'i', '\u0003', '1', 's', '©', '\n', 'J', '\u0005', '-', 'U', '-', '\t', 'Z', '\u0001', 'Õ', 'H', '´', '\t', 'h', '', 'T', '\u000b', '¤', '\n', '¥', 'j', '', '\u0004', '­', '\b', 'j', 'D', 'Ú', '\u0004', 't', '\u0005', '°', '%', 'T', '\u0003'};
    private int chineseYear;
    private char[] daysInGregorianMonth = new char[]{'\u001f', '\u001c', '\u001f', '\u001e', '\u001f', '\u001e', '\u001f', '\u001f', '\u001e', '\u001f', '\u001e', '\u001f'};
    private int gregorianDate;
    private int gregorianMonth;
    private int gregorianYear;
    private int principleTerm;
    private char[][] principleTermMap = new char[][]{new char[]{'\u0015', '\u0015', '\u0015', '\u0015', '\u0015', '\u0014', '\u0015', '\u0015', '\u0015', '\u0014', '\u0014', '\u0015', '\u0015', '\u0014', '\u0014', '\u0014', '\u0014', '\u0014', '\u0014', '\u0014', '\u0014', '\u0013', '\u0014', '\u0014', '\u0014', '\u0013', '\u0013', '\u0014'}, new char[]{'\u0014', '\u0013', '\u0013', '\u0014', '\u0014', '\u0013', '\u0013', '\u0013', '\u0013', '\u0013', '\u0013', '\u0013', '\u0013', '\u0012', '\u0013', '\u0013', '\u0013', '\u0012', '\u0012', '\u0013', '\u0013', '\u0012', '\u0012', '\u0012', '\u0012', '\u0012', '\u0012', '\u0012'}, new char[]{'\u0015', '\u0015', '\u0015', '\u0016', '\u0015', '\u0015', '\u0015', '\u0015', '\u0014', '\u0015', '\u0015', '\u0015', '\u0014', '\u0014', '\u0015', '\u0015', '\u0014', '\u0014', '\u0014', '\u0015', '\u0014', '\u0014', '\u0014', '\u0014', '\u0013', '\u0014', '\u0014', '\u0014', '\u0014'}, new char[]{'\u0014', '\u0015', '\u0015', '\u0015', '\u0014', '\u0014', '\u0015', '\u0015', '\u0014', '\u0014', '\u0014', '\u0015', '\u0014', '\u0014', '\u0014', '\u0014', '\u0013', '\u0014', '\u0014', '\u0014', '\u0013', '\u0013', '\u0014', '\u0014', '\u0013', '\u0013', '\u0013', '\u0014', '\u0014'}, new char[]{'\u0015', '\u0016', '\u0016', '\u0016', '\u0015', '\u0015', '\u0016', '\u0016', '\u0015', '\u0015', '\u0015', '\u0016', '\u0015', '\u0015', '\u0015', '\u0015', '\u0014', '\u0015', '\u0015', '\u0015', '\u0014', '\u0014', '\u0015', '\u0015', '\u0014', '\u0014', '\u0014', '\u0015', '\u0015'}, new char[]{'\u0016', '\u0016', '\u0016', '\u0016', '\u0015', '\u0016', '\u0016', '\u0016', '\u0015', '\u0015', '\u0016', '\u0016', '\u0015', '\u0015', '\u0015', '\u0016', '\u0015', '\u0015', '\u0015', '\u0015', '\u0014', '\u0015', '\u0015', '\u0015', '\u0014', '\u0014', '\u0015', '\u0015', '\u0015'}, new char[]{'\u0017', '\u0017', '\u0018', '\u0018', '\u0017', '\u0017', '\u0017', '\u0018', '\u0017', '\u0017', '\u0017', '\u0017', '\u0016', '\u0017', '\u0017', '\u0017', '\u0016', '\u0016', '\u0017', '\u0017', '\u0016', '\u0016', '\u0016', '\u0017', '\u0016', '\u0016', '\u0016', '\u0016', '\u0017'}, new char[]{'\u0017', '\u0018', '\u0018', '\u0018', '\u0017', '\u0017', '\u0018', '\u0018', '\u0017', '\u0017', '\u0017', '\u0018', '\u0017', '\u0017', '\u0017', '\u0017', '\u0016', '\u0017', '\u0017', '\u0017', '\u0016', '\u0016', '\u0017', '\u0017', '\u0016', '\u0016', '\u0016', '\u0017', '\u0017'}, new char[]{'\u0017', '\u0018', '\u0018', '\u0018', '\u0017', '\u0017', '\u0018', '\u0018', '\u0017', '\u0017', '\u0017', '\u0018', '\u0017', '\u0017', '\u0017', '\u0017', '\u0016', '\u0017', '\u0017', '\u0017', '\u0016', '\u0016', '\u0017', '\u0017', '\u0016', '\u0016', '\u0016', '\u0017', '\u0017'}, new char[]{'\u0018', '\u0018', '\u0018', '\u0018', '\u0017', '\u0018', '\u0018', '\u0018', '\u0017', '\u0017', '\u0018', '\u0018', '\u0017', '\u0017', '\u0017', '\u0018', '\u0017', '\u0017', '\u0017', '\u0017', '\u0016', '\u0017', '\u0017', '\u0017', '\u0016', '\u0016', '\u0017', '\u0017', '\u0017'}, new char[]{'\u0017', '\u0017', '\u0017', '\u0017', '\u0016', '\u0017', '\u0017', '\u0017', '\u0016', '\u0016', '\u0017', '\u0017', '\u0016', '\u0016', '\u0016', '\u0017', '\u0016', '\u0016', '\u0016', '\u0016', '\u0015', '\u0016', '\u0016', '\u0016', '\u0015', '\u0015', '\u0016', '\u0016', '\u0016'}, new char[]{'\u0016', '\u0016', '\u0017', '\u0017', '\u0016', '\u0016', '\u0016', '\u0017', '\u0016', '\u0016', '\u0016', '\u0016', '\u0015', '\u0016', '\u0016', '\u0016', '\u0015', '\u0015', '\u0016', '\u0016', '\u0015', '\u0015', '\u0015', '\u0016', '\u0015', '\u0015', '\u0015', '\u0015', '\u0016'}};
    private String[] principleTermNames;
    private char[][] principleTermYear = new char[][]{new char[]{'\r', '-', 'Q', 'q', '', '¹', 'É'}, new char[]{'\u0015', '9', ']', '}', '¡', 'Á', 'É'}, new char[]{'\u0015', '8', 'X', 'x', '', '¼', 'È', 'É'}, new char[]{'\u0015', '1', 'Q', 't', '', '°', 'È', 'É'}, new char[]{'\u0011', '1', 'M', 'p', '', '¨', 'È', 'É'}, new char[]{'\u001c', '<', 'X', 't', '', '´', 'È', 'É'}, new char[]{'\u0019', '5', 'T', 'p', '', '¬', 'È', 'É'}, new char[]{'\u001d', '9', 'Y', 'x', '', '´', 'È', 'É'}, new char[]{'\u0011', '-', 'I', 'l', '', '¨', 'È', 'É'}, new char[]{'\u001c', '<', '\\', '|', ' ', 'À', 'È', 'É'}, new char[]{'\u0010', ',', 'P', 'p', '', '´', 'È', 'É'}, new char[]{'\u0011', '5', 'X', 'x', '', '¼', 'È', 'É'}};
    private int sectionalTerm;
    private char[][] sectionalTermMap = new char[][]{new char[]{'\u0007', '\u0006', '\u0006', '\u0006', '\u0006', '\u0006', '\u0006', '\u0006', '\u0006', '\u0005', '\u0006', '\u0006', '\u0006', '\u0005', '\u0005', '\u0006', '\u0006', '\u0005', '\u0005', '\u0005', '\u0005', '\u0005', '\u0005', '\u0005', '\u0005', '\u0004', '\u0005', '\u0005'}, new char[]{'\u0005', '\u0004', '\u0005', '\u0005', '\u0005', '\u0004', '\u0004', '\u0005', '\u0005', '\u0004', '\u0004', '\u0004', '\u0004', '\u0004', '\u0004', '\u0004', '\u0004', '\u0003', '\u0004', '\u0004', '\u0004', '\u0003', '\u0003', '\u0004', '\u0004', '\u0003', '\u0003', '\u0003'}, new char[]{'\u0006', '\u0006', '\u0006', '\u0007', '\u0006', '\u0006', '\u0006', '\u0006', '\u0005', '\u0006', '\u0006', '\u0006', '\u0005', '\u0005', '\u0006', '\u0006', '\u0005', '\u0005', '\u0005', '\u0006', '\u0005', '\u0005', '\u0005', '\u0005', '\u0004', '\u0005', '\u0005', '\u0005', '\u0005'}, new char[]{'\u0005', '\u0005', '\u0006', '\u0006', '\u0005', '\u0005', '\u0005', '\u0006', '\u0005', '\u0005', '\u0005', '\u0005', '\u0004', '\u0005', '\u0005', '\u0005', '\u0004', '\u0004', '\u0005', '\u0005', '\u0004', '\u0004', '\u0004', '\u0005', '\u0004', '\u0004', '\u0004', '\u0004', '\u0005'}, new char[]{'\u0006', '\u0006', '\u0006', '\u0007', '\u0006', '\u0006', '\u0006', '\u0006', '\u0005', '\u0006', '\u0006', '\u0006', '\u0005', '\u0005', '\u0006', '\u0006', '\u0005', '\u0005', '\u0005', '\u0006', '\u0005', '\u0005', '\u0005', '\u0005', '\u0004', '\u0005', '\u0005', '\u0005', '\u0005'}, new char[]{'\u0006', '\u0006', '\u0007', '\u0007', '\u0006', '\u0006', '\u0006', '\u0007', '\u0006', '\u0006', '\u0006', '\u0006', '\u0005', '\u0006', '\u0006', '\u0006', '\u0005', '\u0005', '\u0006', '\u0006', '\u0005', '\u0005', '\u0005', '\u0006', '\u0005', '\u0005', '\u0005', '\u0005', '\u0004', '\u0005', '\u0005', '\u0005', '\u0005'}, new char[]{'\u0007', '\b', '\b', '\b', '\u0007', '\u0007', '\b', '\b', '\u0007', '\u0007', '\u0007', '\b', '\u0007', '\u0007', '\u0007', '\u0007', '\u0006', '\u0007', '\u0007', '\u0007', '\u0006', '\u0006', '\u0007', '\u0007', '\u0006', '\u0006', '\u0006', '\u0007', '\u0007'}, new char[]{'\b', '\b', '\b', '\t', '\b', '\b', '\b', '\b', '\u0007', '\b', '\b', '\b', '\u0007', '\u0007', '\b', '\b', '\u0007', '\u0007', '\u0007', '\b', '\u0007', '\u0007', '\u0007', '\u0007', '\u0006', '\u0007', '\u0007', '\u0007', '\u0006', '\u0006', '\u0007', '\u0007', '\u0007'}, new char[]{'\b', '\b', '\b', '\t', '\b', '\b', '\b', '\b', '\u0007', '\b', '\b', '\b', '\u0007', '\u0007', '\b', '\b', '\u0007', '\u0007', '\u0007', '\b', '\u0007', '\u0007', '\u0007', '\u0007', '\u0006', '\u0007', '\u0007', '\u0007', '\u0007'}, new char[]{'\t', '\t', '\t', '\t', '\b', '\t', '\t', '\t', '\b', '\b', '\t', '\t', '\b', '\b', '\b', '\t', '\b', '\b', '\b', '\b', '\u0007', '\b', '\b', '\b', '\u0007', '\u0007', '\b', '\b', '\b'}, new char[]{'\b', '\b', '\b', '\b', '\u0007', '\b', '\b', '\b', '\u0007', '\u0007', '\b', '\b', '\u0007', '\u0007', '\u0007', '\b', '\u0007', '\u0007', '\u0007', '\u0007', '\u0006', '\u0007', '\u0007', '\u0007', '\u0006', '\u0006', '\u0007', '\u0007', '\u0007'}, new char[]{'\u0007', '\b', '\b', '\b', '\u0007', '\u0007', '\b', '\b', '\u0007', '\u0007', '\u0007', '\b', '\u0007', '\u0007', '\u0007', '\u0007', '\u0006', '\u0007', '\u0007', '\u0007', '\u0006', '\u0006', '\u0007', '\u0007', '\u0006', '\u0006', '\u0006', '\u0007', '\u0007'}};
    private String[] sectionalTermNames;
    private char[][] sectionalTermYear = new char[][]{new char[]{'\r', '1', 'U', 'u', '', '¹', 'É', 'ú', 'ú'}, new char[]{'\r', '-', 'Q', 'u', '', '¹', 'É', 'ú', 'ú'}, new char[]{'\r', '0', 'T', 'p', '', '¸', 'È', 'É', 'ú'}, new char[]{'\r', '-', 'L', 'l', '', '¬', 'È', 'É', 'ú'}, new char[]{'\r', ',', 'H', 'h', '', '¨', 'È', 'É', 'ú'}, new char[]{'\u0005', '!', 'D', '`', '|', '', '¼', 'È', 'É'}, new char[]{'\u001d', '9', 'U', 'x', '', '°', 'È', 'É', 'ú'}, new char[]{'\r', '0', 'L', 'h', '', '¨', 'Ä', 'È', 'É'}, new char[]{'\u0019', '<', 'X', 'x', '', '¸', 'È', 'É', 'ú'}, new char[]{'\u0010', ',', 'L', 'l', '', '¬', 'È', 'É', 'ú'}, new char[]{'\u001c', '<', '\\', '|', ' ', 'À', 'È', 'É', 'ú'}, new char[]{'\u0011', '5', 'U', '|', '', '¼', 'È', 'É', 'ú'}};

    public SolarTermsUtil(Resources resources) {
        this.principleTermNames = resources.getStringArray(R.array.quick_settings_status_principleTerm);
        this.sectionalTermNames = resources.getStringArray(R.array.quick_settings_status_sectionalTerm);
    }

    private int computeChineseFields() {
        if (this.gregorianYear < 1901 || this.gregorianYear > 2100) {
            return 1;
        }
        int i;
        int startYear = this.baseYear;
        int startMonth = this.baseMonth;
        int startDate = this.baseDate;
        this.chineseYear = this.baseChineseYear;
        this.chineseMonth = this.baseChineseMonth;
        this.chineseDate = this.baseChineseDate;
        if (this.gregorianYear >= 2000) {
            startYear = this.baseYear + 99;
            startMonth = 1;
            startDate = 1;
            this.chineseYear = this.baseChineseYear + 99;
            this.chineseMonth = 11;
            this.chineseDate = 25;
        }
        int daysDiff = 0;
        for (i = startYear; i < this.gregorianYear; i++) {
            daysDiff += 365;
            if (isGregorianLeapYear(i)) {
                daysDiff++;
            }
        }
        for (i = startMonth; i < this.gregorianMonth; i++) {
            daysDiff += daysInGregorianMonth(this.gregorianYear, i);
        }
        this.chineseDate += daysDiff + (this.gregorianDate - startDate);
        int lastDate = daysInChineseMonth(this.chineseYear, this.chineseMonth);
        int nextMonth = nextChineseMonth(this.chineseYear, this.chineseMonth);
        while (this.chineseDate > lastDate) {
            if (Math.abs(nextMonth) < Math.abs(this.chineseMonth)) {
                this.chineseYear++;
            }
            this.chineseMonth = nextMonth;
            this.chineseDate -= lastDate;
            lastDate = daysInChineseMonth(this.chineseYear, this.chineseMonth);
            nextMonth = nextChineseMonth(this.chineseYear, this.chineseMonth);
        }
        return 0;
    }

    private int computeSolarTerms() {
        if (this.gregorianYear < 1901 || this.gregorianYear > 2100) {
            return 1;
        }
        this.sectionalTerm = sectionalTerm(this.gregorianYear, this.gregorianMonth);
        this.principleTerm = principleTerm(this.gregorianYear, this.gregorianMonth);
        return 0;
    }

    private int sectionalTerm(int y, int m) {
        if (y < 1901 || y > 2100) {
            return 0;
        }
        int index = 0;
        char ry = (y - this.baseYear) + 1;
        while (ry >= this.sectionalTermYear[m - 1][index]) {
            index++;
        }
        int term = this.sectionalTermMap[m - 1][(index * 4) + (ry % 4)];
        if (ry == 'y' && m == 4) {
            term = 5;
        }
        if (ry == '' && m == 4) {
            term = 5;
        }
        if (ry == 'Â' && m == 6) {
            return 6;
        }
        return term;
    }

    private int principleTerm(int y, int m) {
        if (y < 1901 || y > 2100) {
            return 0;
        }
        int index = 0;
        char ry = (y - this.baseYear) + 1;
        while (ry >= this.principleTermYear[m - 1][index]) {
            index++;
        }
        int term = this.principleTermMap[m - 1][(index * 4) + (ry % 4)];
        if (ry == '«' && m == 3) {
            term = 21;
        }
        if (ry == 'µ' && m == 5) {
            return 21;
        }
        return term;
    }

    private boolean isGregorianLeapYear(int year) {
        boolean isLeap = false;
        if (year % 4 == 0) {
            isLeap = true;
        }
        if (year % 100 == 0) {
            isLeap = false;
        }
        if (year % 400 == 0) {
            return true;
        }
        return isLeap;
    }

    private int daysInGregorianMonth(int y, int m) {
        int d = this.daysInGregorianMonth[m - 1];
        if (m == 2 && isGregorianLeapYear(y)) {
            return d + 1;
        }
        return d;
    }

    private int daysInChineseMonth(int y, int m) {
        int index = (y - this.baseChineseYear) + this.baseIndex;
        if (1 > m || m > 8) {
            if (9 > m || m > 12) {
                if (((this.chineseMonths[(index * 2) + 1] >> 4) & 15) != Math.abs(m)) {
                    return 0;
                }
                for (int i : this.bigLeapMonthYears) {
                    if (i == index) {
                        return 30;
                    }
                }
                return 29;
            } else if (((this.chineseMonths[(index * 2) + 1] >> (m - 9)) & 1) == 1) {
                return 29;
            } else {
                return 30;
            }
        } else if (((this.chineseMonths[index * 2] >> (m - 1)) & 1) == 1) {
            return 29;
        } else {
            return 30;
        }
    }

    private int nextChineseMonth(int y, int m) {
        int n = Math.abs(m) + 1;
        if (m > 0) {
            if (((this.chineseMonths[(((y - this.baseChineseYear) + this.baseIndex) * 2) + 1] >> 4) & 15) == m) {
                n = -m;
            }
        }
        if (n == 13) {
            return 1;
        }
        return n;
    }

    public String getSolartermsMsg(int year, int month, int day) {
        this.gregorianYear = year;
        this.gregorianMonth = month + 1;
        this.gregorianDate = day;
        computeChineseFields();
        computeSolarTerms();
        String str = "";
        String gm = String.valueOf(this.gregorianMonth);
        if (gm.length() == 1) {
            gm = ' ' + gm;
        }
        String cm = String.valueOf(Math.abs(this.chineseMonth));
        if (cm.length() == 1) {
            cm = ' ' + cm;
        }
        String gd = String.valueOf(this.gregorianDate);
        if (gd.length() == 1) {
            gd = ' ' + gd;
        }
        String cd = String.valueOf(this.chineseDate);
        if (cd.length() == 1) {
            cd = ' ' + cd;
        }
        if (this.gregorianDate == this.sectionalTerm) {
            return " " + this.sectionalTermNames[this.gregorianMonth - 1];
        }
        if (this.gregorianDate == this.principleTerm) {
            return " " + this.principleTermNames[this.gregorianMonth - 1];
        }
        return str;
    }
}
