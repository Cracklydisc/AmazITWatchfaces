package com.huami.watch.watchface.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;
import java.util.HashMap;

public class TypefaceManager {
    private static TypefaceManager sInstance;
    private AssetManager mAssetManager;
    private HashMap<String, Typeface> mCacheMap = new HashMap();
    private final Object mLock = new Object();

    public static void init(Context context) {
        if (sInstance == null) {
            sInstance = new TypefaceManager(context);
        }
    }

    public static TypefaceManager get() {
        return sInstance;
    }

    private TypefaceManager(Context context) {
        this.mAssetManager = context.getAssets();
    }

    public Typeface createFromAsset(String path) {
        if (path == null) {
            throw new IllegalArgumentException("path can not be null.");
        }
        Typeface t;
        Log.d("TypefaceManager", "createFromAsset " + path);
        synchronized (this.mLock) {
            t = (Typeface) this.mCacheMap.get(path);
            if (t == null) {
                t = Typeface.createFromAsset(this.mAssetManager, path);
                Log.d("TypefaceManager", " init typeface.");
                this.mCacheMap.put(path, t);
            } else {
                Log.d("TypefaceManager", " found cache.");
            }
        }
        return t;
    }
}
