package com.huami.watch.watchface.util;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.PowerManager;
import android.os.SystemProperties;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.support.v7.recyclerview.C0051R;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.TextUtils;
import android.util.Log;
import com.huami.watch.watchface.WatchfaceApplication;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptNumView;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Locale;

public class Util {
    public static final boolean DEBUG = Log.isLoggable("watchface", 2);
    public static final boolean IS_OVERSEA = TextUtils.equals(SystemProperties.get("ro.build.oversea", "0"), "1");
    private static final Locale LOCALE_DEFAULT = Locale.US;
    public static final Object mLock = new Object();
    private static int[][] palette = new int[][]{new int[]{0, 0, 0}, new int[]{0, 0, 255}, new int[]{0, 255, 0}, new int[]{0, 255, 255}, new int[]{255, 0, 0}, new int[]{255, 0, 255}, new int[]{255, 255, 0}, new int[]{255, 255, 255}};
    public static int sHasWatchFaceStarted;

    private static class C3 {
        public static void initC(int color, int[] store) {
            store[0] = (color >> 16) & 255;
            store[1] = (color >> 8) & 255;
            store[2] = color & 255;
        }

        public static void add(int[] src, int[] dst) {
            src[0] = src[0] + dst[0];
            src[1] = src[1] + dst[1];
            src[2] = src[2] + dst[2];
        }

        public static void sub(int[] src, int[] dst, int[] store) {
            store[0] = src[0] - dst[0];
            store[1] = src[1] - dst[1];
            store[2] = src[2] - dst[2];
        }

        public static void mul(int[] src, double d, int[] store) {
            store[0] = (int) (((double) src[0]) * d);
            store[1] = (int) (((double) src[1]) * d);
            store[2] = (int) (((double) src[2]) * d);
        }

        public static int diff(int[] src, int[] dst) {
            return (Math.abs(src[0] - dst[0]) + Math.abs(src[1] - dst[1])) + Math.abs(src[2] - dst[2]);
        }

        public static int toRGB(int[] src) {
            return ((-16777216 | (clamp(src[0]) << 16)) | (clamp(src[1]) << 8)) | clamp(src[2]);
        }

        private static int clamp(int c) {
            return Math.max(0, Math.min(255, c));
        }
    }

    public static final int getMeasurement(Context c) {
        int m = 0;
        try {
            m = Secure.getInt(c.getContentResolver(), "measurement");
        } catch (SettingNotFoundException e) {
        }
        return m;
    }

    public static final Boolean needSlptRefreshSecond(Context mContext) {
        boolean flag = false;
        try {
            if (Global.getInt(mContext.getContentResolver(), "slpt_refresh_in_sec") == 1) {
                flag = true;
            }
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
        }
        return Boolean.valueOf(flag);
    }

    public static final float getDistance(int measurem, float distance) {
        return measurem == 0 ? distance : distance * 0.6213712f;
    }

    public static void setCurrentSlpt(Context mContext, String name) {
        System.putString(mContext.getContentResolver(), "slptName", name);
    }

    public static String getCurrentSlpt(Context mContext) {
        return System.getString(mContext.getContentResolver(), "slptName");
    }

    public static void notifyStatusBarPositionUpdated(Context context, float posY) {
        notifyStatusBarPositionUpdated(context, -1.0f, posY);
    }

    public static void notifyStatusBarPositionUpdated(Context context, float posX, float posY) {
        Log.d("DDDD", "Notify StatusBar Position Updated : " + posY);
        Intent intent = new Intent("com.huami.watch.watchface.action.STATUSBAR_POSITION_UPDATED");
        if (posX >= 0.0f) {
            intent.putExtra("com.huami.watch.watchface.extra.STATUSBAR_POSITION_X", posX);
        }
        if (posY >= 0.0f) {
            intent.putExtra("com.huami.watch.watchface.extra.STATUSBAR_POSITION_Y", posY);
        }
        context.sendBroadcast(intent);
        if (posX >= 0.0f) {
            System.putFloat(context.getContentResolver(), "com.huami.watch.watchface.extra.STATUSBAR_POSITION_X", posX);
        } else {
            System.putFloat(context.getContentResolver(), "com.huami.watch.watchface.extra.STATUSBAR_POSITION_X", -1.0f);
        }
        if (posY >= 0.0f) {
            System.putFloat(context.getContentResolver(), "com.huami.watch.watchface.extra.STATUSBAR_POSITION_Y", posY);
        } else {
            System.putFloat(context.getContentResolver(), "com.huami.watch.watchface.extra.STATUSBAR_POSITION_Y", -1.0f);
        }
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        Bitmap bitmap = Bitmap.createBitmap(w, h, drawable.getOpacity() != -1 ? Config.ARGB_8888 : Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);
        drawable.draw(canvas);
        return bitmap;
    }

    public static boolean needEnAssets() {
        Locale locale = Locale.getDefault();
        String id = SystemProperties.get("ro.build.oversea", "0");
        String id2 = SystemProperties.get("user.language", "en");
        String cty = locale.getCountry();
        if (!id.equals("1")) {
            return false;
        }
        if (id2.equals("zh") && cty.equals("CN")) {
            return false;
        }
        return true;
    }

    public static Bitmap decodeImage(Resources resources, String fileName) {
        Bitmap image = null;
        try {
            InputStream is = resources.getAssets().open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
            return image;
        } catch (IOException e) {
            e.printStackTrace();
            return image;
        }
    }

    public static BitmapDrawable decodeBitmapDrawableFromAssets(Resources resources, String fileName) {
        try {
            InputStream is = resources.getAssets().open(fileName);
            Bitmap b = BitmapFactory.decodeStream(is);
            is.close();
            if (b != null) {
                return new BitmapDrawable(resources, b);
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean copyFile(String sourcefile, String targetFile) {
        try {
            FileInputStream input = new FileInputStream(new File(sourcefile));
            BufferedInputStream inbuff = new BufferedInputStream(input);
            File target = new File(targetFile);
            target.mkdirs();
            if (target.exists()) {
                target.delete();
            }
            FileOutputStream out = new FileOutputStream(target);
            BufferedOutputStream outbuff = new BufferedOutputStream(out);
            byte[] b = new byte[5120];
            while (true) {
                int len = inbuff.read(b);
                if (len != -1) {
                    outbuff.write(b, 0, len);
                } else {
                    outbuff.flush();
                    inbuff.close();
                    outbuff.close();
                    out.close();
                    input.close();
                    return true;
                }
            }
        } catch (Exception ioException) {
            ioException.printStackTrace();
            return false;
        }
    }

    public static final boolean isSlptSupportColor(int color) {
        int r = (color >> 16) & 255;
        int g = (color >> 8) & 255;
        int b = color & 255;
        if (((color >> 32) & 255) % 255 == 0 && r % 255 == 0 && g % 255 == 0 && b % 255 == 0) {
            return true;
        }
        return false;
    }

    public static final String formatTime(int n) {
        String v = Integer.toString(n);
        if (v.length() < 2) {
            return "0" + v;
        }
        return v;
    }

    public static final CharSequence formatDate(CharSequence inFormat, int year, int month, int day) {
        SpannableStringBuilder s = new SpannableStringBuilder(inFormat);
        int len = inFormat.length();
        int i = 0;
        while (i < len) {
            String replacement;
            int count = 1;
            char c = s.charAt(i);
            while (i + count < len && s.charAt(i + count) == c) {
                count++;
            }
            switch (c) {
                case 'M':
                    replacement = formatTime(month);
                    break;
                case 'd':
                    replacement = formatTime(day);
                    break;
                case 'y':
                    replacement = String.valueOf(year);
                    break;
                default:
                    replacement = null;
                    break;
            }
            if (replacement != null) {
                s.replace(i, i + count, replacement);
                count = replacement.length();
                len = s.length();
            }
            i += count;
        }
        if (inFormat instanceof Spanned) {
            return new SpannedString(s);
        }
        return s.toString();
    }

    public static String getFormatDistance(double distance) {
        if (distance < 0.0d) {
            return "--";
        }
        if (Double.isNaN(distance) || Double.isInfinite(distance)) {
            return "--";
        }
        BigDecimal decimal = new BigDecimal(String.valueOf(distance));
        if (distance < 100.0d) {
            return decimal.setScale(2, 3).toString();
        }
        if (distance < 1000.0d) {
            return decimal.setScale(1, RoundingMode.FLOOR).toString();
        }
        return "" + ((int) distance);
    }

    public static final double convertKmToMi(double distance) {
        return 0.6213712d * distance;
    }

    public static void ditherBitmap(Bitmap src, Bitmap dst, float dither) {
        createDitherBitmap(src, dst, dither);
    }

    private static void createDitherBitmap(Bitmap src, Bitmap dst, float dither) {
        if (dither <= 0.0f || dither > 1.0f) {
            throw new IllegalArgumentException("input must be in (0, 1)");
        } else if (src != null && dst != null) {
            int i;
            int w = src.getWidth();
            int h = src.getHeight();
            float D1 = (7.0f * dither) / 16.0f;
            float D2 = (3.0f * dither) / 16.0f;
            float D3 = (5.0f * dither) / 16.0f;
            float D4 = (1.0f * dither) / 16.0f;
            int[] colors = new int[(w * h)];
            int[][] tmps = (int[][]) Array.newInstance(Integer.TYPE, new int[]{w * h, 3});
            src.getPixels(colors, 0, w, 0, 0, w, h);
            for (i = 0; i < colors.length; i++) {
                int[] tmp = new int[3];
                C3.initC(colors[i], tmp);
                tmps[i] = tmp;
            }
            int[] c0 = new int[3];
            for (i = 0; i < tmps.length; i++) {
                int[] c1 = tmps[i];
                findClosestPaletteColor(c1, c0);
                colors[i] = C3.toRGB(c0);
                C3.sub(c1, c0, c0);
                int x = i % w;
                int y = i / w;
                if (x + 1 < w) {
                    C3.mul(c0, (double) D1, c1);
                    C3.add(tmps[i + 1], c1);
                }
                if (x - 1 >= 0 && y + 1 < h) {
                    C3.mul(c0, (double) D2, c1);
                    C3.add(tmps[(i + w) - 1], c1);
                }
                if (y + 1 < h) {
                    C3.mul(c0, (double) D3, c1);
                    C3.add(tmps[i + w], c1);
                }
                if (x + 1 < w && y + 1 < h) {
                    C3.mul(c0, (double) D4, c1);
                    C3.add(tmps[(i + w) + 1], c1);
                }
            }
            dst.setPixels(colors, 0, w, 0, 0, w, h);
        }
    }

    private static void findClosestPaletteColor(int[] c, int[] store) {
        int[] closest = palette[0];
        for (int[] n : palette) {
            if (C3.diff(n, c) < C3.diff(closest, c)) {
                closest = n;
            }
        }
        System.arraycopy(closest, 0, store, 0, 3);
    }

    public static SlptViewComponent getCharView(Context mContext, String path) {
        byte[] mem = SimpleFile.readFileFromAssets(mContext, path);
        SlptPictureView view = new SlptPictureView();
        view.setImagePicture(mem);
        return view;
    }

    public static SlptViewComponent getUnicodeStringView(Context mContext, String path, String title, short space) {
        SlptLinearLayout layout = new SlptLinearLayout();
        ArrayList list = chinaToUnicode(title);
        for (int i = 0; i < list.size(); i++) {
            byte[] mem = SimpleFile.readFileFromAssets(mContext, path + list.get(i).toString() + ".png");
            SlptPictureView view = new SlptPictureView();
            view.setImagePicture(mem);
            view.padding.right = space;
            layout.add(view);
        }
        return layout;
    }

    public static SlptNumView getDefaultNumView(Context mContext, String value) {
        SlptNumView numView = new SlptNumView();
        int defaultValue = Integer.parseInt(value);
        if (defaultValue < 0 || defaultValue > 9) {
            numView.num = 0;
        } else {
            numView.num = defaultValue;
        }
        return numView;
    }

    public static ArrayList chinaToUnicode(String str) {
        StringBuffer unicode = new StringBuffer();
        ArrayList list = new ArrayList();
        for (int i = 0; i < str.length(); i++) {
            unicode.setLength(0);
            String value = Integer.toHexString(str.charAt(i));
            if (value.length() <= 2) {
                unicode.append("u00" + value);
            } else {
                unicode.append("u" + value);
            }
            list.add(unicode.toString());
        }
        return list;
    }

    public static void acquireWakeLock(Context mContext, int second, String tag) {
        ((PowerManager) mContext.getSystemService("power")).newWakeLock(1, tag).acquire((long) (second * 1000));
    }

    public static char getWeekNumberZh(int number) {
        switch (number) {
            case 0:
                return '日';
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                return '一';
            case 2:
                return '二';
            case 3:
                return '三';
            case 4:
                return '四';
            case 5:
                return '五';
            case 6:
                return '六';
            default:
                return ' ';
        }
    }

    public static void hideWatchFace(Context mContext) {
        ((WatchfaceApplication) mContext.getApplicationContext()).setNeedWatchFace(false);
    }

    public static void showWatchFace(Context mContext) {
        ((WatchfaceApplication) mContext.getApplicationContext()).setNeedWatchFace(true);
    }

    public static boolean checkWatchFace(Context mContext) {
        return ((WatchfaceApplication) mContext.getApplicationContext()).getNeedWatchFace();
    }

    public static int getSportfaceValue() {
        return Integer.parseInt(SystemProperties.get("sys.watchface.sport", "0"));
    }

    public static boolean needSwimYardUnit(Context mContext) {
        if (SystemProperties.get("sys.watchface.android.unit", "0").equals("0")) {
            return false;
        }
        return true;
    }

    public static void setSportItemOrder(String value) {
        SystemProperties.set("sys.watchface.sport.order", value);
    }

    public static void cleanSportItemOrder() {
        SystemProperties.set("sys.watchface.sport.order", "");
    }

    public static String getSportItemOrder() {
        return SystemProperties.get("sys.watchface.sport.order", "");
    }
}
