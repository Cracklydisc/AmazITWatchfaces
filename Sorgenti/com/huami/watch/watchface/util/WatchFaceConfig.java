package com.huami.watch.watchface.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import android.util.SparseArray;
import com.huami.watch.watchface.loader.WatchFaceExternalLoader;
import com.huami.watch.watchface.loader.WfzConstants;
import com.huami.watch.watchface.model.WatchFaceModule;
import com.huami.watch.watchface.model.WatchFaceModuleItem;
import com.huami.watch.watchface.util.WatchFaceConfigTemplate.DataWidgetConfig;
import com.huami.watch.watchface.widget.ImageFont;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class WatchFaceConfig {
    private String INTERNAL_PATH;
    private Drawable mBgDrawable;
    private String mBgPathSlpt;
    private String mBgPathSlpt26W;
    private int mBgType = -1;
    private Context mContext;
    private WatchFaceExternalLoader mExternalLoader;
    private Drawable mGraduation;
    private String mGraduationSlptPath;
    private String mGraduationSlptPath26W;
    private int mGraduationType = -1;
    private String mLowPowerIconSlptPath;
    private int mLowPowerIconType = -1;
    private int mLowPowerX = Integer.MIN_VALUE;
    private int mLowPowerY = Integer.MIN_VALUE;
    private int mStatusBarPosX = Integer.MIN_VALUE;
    private int mStatusBarPosY = Integer.MIN_VALUE;
    private boolean mSupport26W = false;
    private TimeDigital mTimeDigital;
    private TimeHand mTimeHand;
    private String mTimeHandHourSlpt26WPath;
    private String mTimeHandHourSlptPath;
    private String mTimeHandMinuteSlpt26WPath;
    private String mTimeHandMinuteSlptPath;
    private String mTimeHandSecondsSlpt26WPath;
    private String mTimeHandSecondsSlptPath;
    private int mTimeHandType = -1;
    private String mWatchFaceName;
    private ArrayList<DataWidgetConfig> mWidgets = new ArrayList();

    static class C03141 implements FilenameFilter {
        C03141() {
        }

        public boolean accept(File dir, String filename) {
            Log.d("WatchFaceConfig", "FilenameFilter filename: " + filename);
            if (filename == null || !filename.endsWith(".wfz")) {
                return false;
            }
            return true;
        }
    }

    public class ImageFontInfo {
        private Canvas canvas;
        private PorterDuffColorFilter colorFilter;
        private String configs;
        private SparseArray<String> fontConfigs;
        private ImageFont imageFont;
        private Paint paint;

        private ImageFontInfo(String configs) {
            this.fontConfigs = new SparseArray();
            this.colorFilter = new PorterDuffColorFilter(0, Mode.SRC_ATOP);
            this.configs = configs;
            this.paint = new Paint(6);
            this.canvas = new Canvas();
        }

        private void putChar(char c, String config) {
            this.fontConfigs.put(c, config);
        }

        public byte[] parseCharData(char c) {
            String config = (String) this.fontConfigs.get(c);
            if (config == null) {
                return null;
            }
            String path;
            if (config.startsWith("@assets/")) {
                String filePath = config.substring("@assets/".length());
                path = WatchFaceConfig.this.getParent(filePath);
                String fileName = WatchFaceConfig.getFileName(filePath);
                String slptPath = path + "8c";
                if (WatchFaceConfig.isAssetsFileExist(WatchFaceConfig.this.mContext, slptPath, fileName)) {
                    return SimpleFile.readFileFromAssets(WatchFaceConfig.this.mContext, slptPath + File.separator + fileName);
                }
                if (WatchFaceConfig.isAssetsFileExist(WatchFaceConfig.this.mContext, path, fileName)) {
                    return SimpleFile.readFileFromAssets(WatchFaceConfig.this.mContext, filePath);
                }
                Log.d("WatchFaceConfig", "parseCharData failure. No file found " + config);
                return null;
            } else if (!config.startsWith("@wfz/")) {
                return null;
            } else {
                path = config.substring("@wfz/".length());
                String pathSlpt = new StringBuffer(path).insert(path.lastIndexOf(File.separator) + 1, "8c/").toString();
                if (WatchFaceConfig.this.mExternalLoader == null) {
                    Log.d("WatchFaceConfig", "parseCharData failure. ExternalLoader not init.");
                    return null;
                } else if (WatchFaceConfig.this.mExternalLoader.exists(pathSlpt)) {
                    return WatchFaceConfig.this.parseFile(6, pathSlpt);
                } else {
                    if (WatchFaceConfig.this.mExternalLoader.exists(path)) {
                        return WatchFaceConfig.this.parseFile(6, path);
                    }
                    Log.d("WatchFaceConfig", "parseCharData failure. No file found " + config);
                    return null;
                }
            }
        }

        public byte[] parseCharData(char c, int color) {
            byte[] imgdata = parseCharData(c);
            if (color == 0 || imgdata == null) {
                return imgdata;
            }
            Bitmap src = BitmapFactory.decodeByteArray(imgdata, 0, imgdata.length);
            if (src == null) {
                return imgdata;
            }
            this.colorFilter.setColor(color);
            this.paint.setColorFilter(this.colorFilter);
            Bitmap out = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
            this.canvas.setBitmap(out);
            this.canvas.drawBitmap(src, 0.0f, 0.0f, this.paint);
            this.canvas.setBitmap(null);
            if (Util.isSlptSupportColor(color)) {
                return bitmap2Bytes(out);
            }
            Bitmap dither = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
            Util.ditherBitmap(out, dither, 0.7f);
            return bitmap2Bytes(dither);
        }

        private byte[] bitmap2Bytes(Bitmap bm) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(CompressFormat.PNG, 100, baos);
            return baos.toByteArray();
        }

        public ImageFont parseImageFont() {
            if (this.imageFont != null) {
                return this.imageFont;
            }
            this.imageFont = new ImageFont(this.configs);
            for (int i = 0; i < this.fontConfigs.size(); i++) {
                String config = (String) this.fontConfigs.valueAt(i);
                char c = (char) this.fontConfigs.keyAt(i);
                Log.d("WatchFaceConfig", "parseImageFont found char '" + c + "' config: " + config);
                if (config != null) {
                    Bitmap b = null;
                    if (config.startsWith("@assets/")) {
                        b = WatchFaceConfig.this.parseBitmap(2, config.substring("@assets/".length()));
                    } else if (config.startsWith("@drawable/")) {
                        b = WatchFaceConfig.this.parseBitmap(1, config.substring("@drawable/".length()));
                    } else if (config.startsWith("@wfz/")) {
                        b = WatchFaceConfig.this.parseBitmap(6, config.substring("@wfz/".length()));
                    }
                    if (b != null) {
                        this.imageFont.addChar(c, b);
                        Log.d("WatchFaceConfig", "  parse done.");
                    }
                }
            }
            return this.imageFont;
        }
    }

    public class TimeDigital {
        int color = 0;
        String config;
        String fontConfig;
        ImageFontInfo fontInfo;
        int gap;
        int height;
        int model;
        int width;
        int f19x;
        int f20y;

        public TimeDigital(int model, int x, int y, int width, int height, String fontConfig, int gap, String color) {
            this.model = model;
            this.f19x = x;
            this.f20y = y;
            this.width = width;
            this.height = height;
            this.fontConfig = fontConfig;
            this.gap = gap;
            if (color != null) {
                try {
                    this.color = Color.parseColor(color);
                } catch (Exception e) {
                }
            }
            encodeConfig();
        }

        public TimeDigital(String config) throws JSONException {
            this.config = config;
            decodeConfig();
        }

        private void parseImageFontInfo() {
            if (this.fontConfig != null && this.fontConfig.startsWith("@wfz/")) {
                List<WatchFaceModuleItem> chars = WatchFaceConfig.this.mExternalLoader.parseWatchFaceConfigList(this.fontConfig.substring("@wfz/".length()) + "/font.xml");
                if (chars != null) {
                    this.fontInfo = new ImageFontInfo(this.fontConfig);
                    for (int i = 0; i < chars.size(); i++) {
                        WatchFaceModuleItem item = (WatchFaceModuleItem) chars.get(i);
                        if ("font".equals(item.getType())) {
                            String charset = item.getCharset();
                            if (charset == null || charset.length() != 1) {
                                Log.d("WatchFaceConfig", "parseImageFontInfo invalid charset: " + charset);
                            } else {
                                this.fontInfo.putChar(charset.charAt(0), item.getConfig());
                            }
                        }
                    }
                }
            }
        }

        public ImageFontInfo getImageFontInfo() {
            return this.fontInfo;
        }

        private void encodeConfig() {
            try {
                JSONObject obj = new JSONObject();
                obj.put("model", this.model);
                obj.put("x", this.f19x);
                obj.put("y", this.f20y);
                obj.put("width", this.width);
                obj.put("height", this.height);
                obj.put("fontConfig", this.fontConfig);
                obj.put("gap", this.gap);
                obj.put("color", this.color);
                this.config = obj.toString();
                Log.d("WatchFaceConfig", "TimeDigital.encodeConfig: " + this.config);
            } catch (JSONException e) {
                Log.d("WatchFaceConfig", "TimeDigital.encodeConfig Error.");
                e.printStackTrace();
            }
        }

        private void decodeConfig() throws JSONException {
            JSONObject obj = new JSONObject(this.config);
            this.model = obj.getInt("model");
            this.f19x = obj.getInt("x");
            this.f20y = obj.getInt("y");
            this.width = obj.getInt("width");
            this.height = obj.getInt("height");
            this.fontConfig = obj.getString("fontConfig");
            this.gap = obj.getInt("gap");
            this.color = obj.getInt("color");
            Log.d("WatchFaceConfig", "TimeDigital.decodeConfig: " + this);
        }

        public int getX() {
            return this.f19x;
        }

        public int getY() {
            return this.f20y;
        }

        public int getWidth() {
            return this.width;
        }

        public int getHeight() {
            return this.height;
        }

        public int getGap() {
            return this.gap;
        }

        public int getModel() {
            return this.model;
        }

        public int getColor() {
            return this.color;
        }

        public String toString() {
            return "TimeDigital: " + this.config;
        }
    }

    public static class TimeHand {
        Drawable mHours;
        Drawable mMinute;
        Drawable mSeconds;

        public Drawable getHour() {
            return this.mHours;
        }

        public Drawable getMinute() {
            return this.mMinute;
        }

        public Drawable getSeconds() {
            return this.mSeconds;
        }
    }

    private WatchFaceConfig() {
    }

    public static WatchFaceConfig getWatchFaceConfig(Context context, String watchfaceName) {
        WatchFaceConfig config = new WatchFaceConfig();
        config.mContext = context;
        config.INTERNAL_PATH = context.getFilesDir().getAbsolutePath();
        config.mWatchFaceName = watchfaceName;
        Log.d("WatchFaceConfig", "WatchFaceConfig watchfaceName: " + watchfaceName);
        int i;
        if (watchfaceName.equals("external_watchface")) {
            File currentPath = new File("/sdcard/.watchface/current/");
            String wfzPath = null;
            if (currentPath.exists() && currentPath.isDirectory()) {
                File[] wfzs = currentPath.listFiles(new C03141());
                if (wfzs != null && wfzs.length > 0) {
                    wfzPath = wfzs[0].getPath();
                    Log.d("WatchFaceConfig", "current wfz: " + wfzPath);
                    config.mExternalLoader = WatchFaceExternalLoader.getWatchFaceExternalLoader(wfzPath);
                }
            }
            if (config.mExternalLoader != null) {
                WatchFaceModule watchfaceModule = config.mExternalLoader.parseWatchFace();
                WatchFaceSave preferences = WatchFaceSave.getWatchFaceSave(wfzPath);
                if (watchfaceModule != null) {
                    List<WatchFaceModuleItem> items = watchfaceModule.getWatchFaceItemList();
                    if (items != null && items.size() > 0) {
                        for (i = 0; i < items.size(); i++) {
                            WatchFaceModuleItem item = (WatchFaceModuleItem) items.get(i);
                            switch (WfzConstants.parseItemType(item.getType())) {
                                case -1:
                                    Log.d("WatchFaceConfig", "Not support " + item);
                                    break;
                                case 0:
                                    if (preferences != null) {
                                        if (preferences.contains("Background")) {
                                            config.parseBg(context, preferences.getString("Background", null));
                                        }
                                    }
                                    if (config.mBgDrawable != null) {
                                        break;
                                    }
                                    config.parseBg(context, item.getConfig());
                                    break;
                                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                                    if (preferences != null) {
                                        if (preferences.contains("Graduation")) {
                                            config.parseGraduation(context, preferences.getString("Graduation", null));
                                        }
                                    }
                                    if (config.mGraduation != null) {
                                        break;
                                    }
                                    config.parseGraduation(context, item.getConfig());
                                    break;
                                case 2:
                                    if (preferences != null) {
                                        if (preferences.contains("TimeHand")) {
                                            config.parseTimeHand(context, preferences.getString("TimeHand", null));
                                        }
                                    }
                                    if (config.mTimeHand != null) {
                                        break;
                                    }
                                    config.parseTimeHand(context, item.getConfig());
                                    break;
                                case 3:
                                    boolean parseSuccess = false;
                                    if (preferences != null) {
                                        if (preferences.contains("DataWidget" + item.getId())) {
                                            parseSuccess = config.parseDataWidget(preferences.getString("DataWidget" + item.getId(), null));
                                        }
                                    }
                                    if (!parseSuccess) {
                                        config.mWidgets.add(new DataWidgetConfig(item.getId(), item.getDataType(), item.getX(), item.getY(), item.getModel(), null, null));
                                        break;
                                    }
                                    break;
                                case 4:
                                    if (preferences != null) {
                                        if (preferences.contains("TimeDigital")) {
                                            config.parseTimeDigital(context, preferences.getString("TimeDigital", null));
                                        }
                                    }
                                    if (config.mTimeDigital != null) {
                                        break;
                                    }
                                    config.parseTimeDigital(item);
                                    break;
                                case 5:
                                    config.mStatusBarPosX = item.getX();
                                    config.mStatusBarPosY = item.getY();
                                    break;
                                case 6:
                                    config.mSupport26W = Boolean.parseBoolean(item.getConfig());
                                    break;
                                case 7:
                                    config.mLowPowerX = item.getX();
                                    config.mLowPowerY = item.getY();
                                    config.parseLowPower(context, item.getConfig());
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        } else {
            SharedPreferences preferences2 = context.getSharedPreferences(watchfaceName, 0);
            if (preferences2.contains("Background")) {
                config.parseBg(context, preferences2.getString("Background", null));
            }
            if (preferences2.contains("TimeHand")) {
                config.parseTimeHand(context, preferences2.getString("TimeHand", null));
            }
            if (preferences2.contains("Graduation")) {
                config.parseGraduation(context, preferences2.getString("Graduation", null));
            }
            for (i = 0; i < 10; i++) {
                if (preferences2.contains("DataWidget" + i)) {
                    config.parseDataWidget(preferences2.getString("DataWidget" + i, null));
                }
            }
        }
        return config;
    }

    private void parseBg(Context context, String value) {
        Log.d("WatchFaceConfig", "parseBg " + value);
        if (value != null && value.length() > 2) {
            if (value.startsWith("@")) {
                if (value.startsWith("@drawable/")) {
                    String resName = value.substring("@drawable/".length());
                    this.mBgDrawable = getDrawableFromResName(context, resName);
                    if (this.mBgDrawable != null) {
                        this.mBgPathSlpt = "guard/" + this.mWatchFaceName + File.separator + resName + ".png";
                        if (this.mSupport26W) {
                            this.mBgPathSlpt26W = resName;
                        }
                        this.mBgType = 1;
                        Log.d("WatchFaceConfig", "BgType: IMG_TYPE_DRAWABLE SlptBgPath: " + this.mBgPathSlpt);
                    }
                } else if (value.startsWith("@customBg/")) {
                    fileName = value.substring("@customBg/".length());
                    String bgPath = this.INTERNAL_PATH + "/watchface/customBg/" + fileName;
                    if (new File(bgPath).exists()) {
                        this.mBgDrawable = new BitmapDrawable(context.getResources(), bgPath);
                        this.mBgPathSlpt = this.INTERNAL_PATH + "/watchface/customSlptBg/" + fileName;
                        if (this.mSupport26W) {
                            this.mBgPathSlpt26W = bgPath;
                        }
                        this.mBgType = 4;
                        Log.d("WatchFaceConfig", "BgType: IMG_TYPE_FILE  BgPathSlpt: " + this.mBgPathSlpt);
                    }
                } else if (value.startsWith("@wfz/")) {
                    fileName = value.substring("@wfz/".length());
                    if (fileName != null && fileName.length() > 0) {
                        this.mBgDrawable = parseDrawable(6, fileName);
                        if (this.mBgDrawable != null) {
                            this.mBgPathSlpt = new StringBuffer(fileName).insert(fileName.lastIndexOf(File.separator) + 1, "8c/").toString();
                            if (this.mSupport26W) {
                                this.mBgPathSlpt26W = fileName;
                            }
                            this.mBgType = 6;
                        }
                    }
                }
            } else if (value.startsWith("#")) {
                this.mBgType = 5;
            }
        }
    }

    private void parseTimeHand(Context context, String value) {
        String handPath;
        Drawable hour;
        Drawable minute;
        Drawable seconds;
        if (value.startsWith("@assets/")) {
            handPath = value.substring("@assets/".length());
            hour = parseDrawable(2, handPath + File.separator + "hour.png");
            if (hour != null) {
                minute = parseDrawable(2, handPath + File.separator + "minute.png");
                if (minute != null) {
                    seconds = parseDrawable(2, handPath + File.separator + "seconds.png");
                    this.mTimeHand = new TimeHand();
                    this.mTimeHand.mHours = hour;
                    this.mTimeHand.mMinute = minute;
                    this.mTimeHand.mSeconds = seconds;
                    this.mTimeHandType = 2;
                    this.mTimeHandHourSlptPath = handPath + "/8c/hour/%d.png.color_map";
                    this.mTimeHandMinuteSlptPath = handPath + "/8c/minute/%d.png.color_map";
                    if (isAssetsFileExist(this.mContext, handPath + "/8c/seconds", "0.png.color_map")) {
                        this.mTimeHandSecondsSlptPath = handPath + "/8c/seconds/%d.png.color_map";
                    }
                    if (this.mSupport26W) {
                        if (isAssetsFileExist(this.mContext, handPath + "/26w/hour", "0.png.color_map")) {
                            this.mTimeHandHourSlpt26WPath = handPath + "/26w/hour/%d.png.color_map";
                        }
                        if (isAssetsFileExist(this.mContext, handPath + "/26w/minute", "0.png.color_map")) {
                            this.mTimeHandMinuteSlpt26WPath = handPath + "/26w/minute/%d.png.color_map";
                        }
                        if (isAssetsFileExist(this.mContext, handPath + "/26w/seconds", "0.png.color_map")) {
                            this.mTimeHandSecondsSlpt26WPath = handPath + "/26w/seconds/%d.png.color_map";
                        }
                    }
                }
            }
        } else if (value.startsWith("@wfz/")) {
            handPath = value.substring("@wfz/".length());
            hour = parseDrawable(6, handPath + File.separator + "hour.png");
            if (hour != null) {
                minute = parseDrawable(6, handPath + File.separator + "minute.png");
                if (minute != null) {
                    seconds = parseDrawable(6, handPath + File.separator + "seconds.png");
                    this.mTimeHand = new TimeHand();
                    this.mTimeHand.mHours = hour;
                    this.mTimeHand.mMinute = minute;
                    this.mTimeHand.mSeconds = seconds;
                    this.mTimeHandType = 6;
                    if (this.mExternalLoader != null) {
                        if (this.mExternalLoader.exists(handPath + "/8c/hour.png")) {
                            this.mTimeHandHourSlptPath = handPath + "/8c/hour.png";
                        } else if (this.mExternalLoader.exists(handPath + "/8c/hour/0.png.color_map")) {
                            this.mTimeHandHourSlptPath = handPath + "/8c/hour/%d.png.color_map";
                        }
                        if (this.mExternalLoader.exists(handPath + "/8c/minute.png")) {
                            this.mTimeHandMinuteSlptPath = handPath + "/8c/minute.png";
                        } else if (this.mExternalLoader.exists(handPath + "/8c/minute/0.png.color_map")) {
                            this.mTimeHandMinuteSlptPath = handPath + "/8c/minute/%d.png.color_map";
                        }
                        if (this.mExternalLoader.exists(handPath + "/8c/seconds.png")) {
                            this.mTimeHandSecondsSlptPath = handPath + "/8c/seconds.png";
                        } else if (this.mExternalLoader.exists(handPath + "/8c/seconds/0.png.color_map")) {
                            this.mTimeHandSecondsSlptPath = handPath + "/8c/seconds/%d.png.color_map";
                        }
                        if (this.mSupport26W) {
                            if (this.mExternalLoader.exists(handPath + "/26w/" + "hour.png")) {
                                this.mTimeHandHourSlpt26WPath = handPath + "/26w/" + "hour.png";
                            } else if (this.mExternalLoader.exists(handPath + "/26w/hour/0.png.color_map")) {
                                this.mTimeHandHourSlpt26WPath = handPath + "/26w/hour/%d.png.color_map";
                            }
                            if (this.mExternalLoader.exists(handPath + "/26w/" + "minute.png")) {
                                this.mTimeHandMinuteSlpt26WPath = handPath + "/26w/" + "minute.png";
                            } else if (this.mExternalLoader.exists(handPath + "/26w/minute/0.png.color_map")) {
                                this.mTimeHandMinuteSlpt26WPath = handPath + "/26w/minute/%d.png.color_map";
                            }
                            if (this.mExternalLoader.exists(handPath + "/26w/" + "seconds.png")) {
                                this.mTimeHandSecondsSlpt26WPath = handPath + "/26w/" + "seconds.png";
                            } else if (this.mExternalLoader.exists(handPath + "/26w/seconds/0.png.color_map")) {
                                this.mTimeHandSecondsSlpt26WPath = handPath + "/26w/seconds/%d.png.color_map";
                            }
                        }
                    }
                }
            }
        }
    }

    private void parseTimeDigital(WatchFaceModuleItem item) {
        this.mTimeDigital = new TimeDigital(item.getModel(), item.getX(), item.getY(), item.getWidth(), item.getHeight(), item.getFontConfig(), item.getGap(), item.getColor());
        this.mTimeDigital.parseImageFontInfo();
    }

    private void parseTimeDigital(Context context, String value) {
        if (value != null && value.length() > 2) {
            try {
                TimeDigital config = new TimeDigital(value);
                config.parseImageFontInfo();
                Log.d("WatchFaceConfig", "parseTimeDigital success: " + config);
                this.mTimeDigital = config;
            } catch (JSONException e) {
                Log.d("WatchFaceConfig", "parseTimeDigital failure: " + value);
            }
        }
    }

    private void parseGraduation(Context context, String value) {
        String graduationPath;
        if (value.startsWith("@assets/")) {
            graduationPath = value.substring("@assets/".length());
            this.mGraduation = parseDrawable(2, graduationPath + File.separator + "graduation.png");
            if (this.mGraduation != null) {
                this.mGraduationSlptPath = graduationPath + "/8c/graduation.png";
                this.mGraduationType = 2;
                if (this.mSupport26W) {
                    String[] list26w = null;
                    try {
                        list26w = this.mContext.getAssets().list(graduationPath + "/26w/");
                    } catch (IOException e) {
                    }
                    if (contantsInList(list26w, "graduation.png")) {
                        this.mGraduationSlptPath26W = graduationPath + "/26w/" + "graduation.png";
                    }
                }
            }
        } else if (value.startsWith("@drawable/")) {
            this.mGraduation = parseDrawable(1, value.substring("@drawable/".length()));
            if (this.mGraduation != null) {
                this.mGraduationType = 1;
                if (!this.mSupport26W) {
                }
            }
        } else if (value.startsWith("@wfz/")) {
            graduationPath = value.substring("@wfz/".length());
            if (this.mExternalLoader != null) {
                this.mGraduation = parseDrawable(6, graduationPath + File.separator + "graduation.png");
                if (this.mGraduation != null) {
                    this.mGraduationSlptPath = graduationPath + "/8c/graduation.png";
                    this.mGraduationType = 6;
                    if (this.mSupport26W && this.mExternalLoader.exists(graduationPath + "/26w/" + "graduation.png")) {
                        this.mGraduationSlptPath26W = graduationPath + "/26w/" + "graduation.png";
                    }
                }
            }
        }
    }

    private boolean parseDataWidget(String value) {
        if (value != null && value.length() > 2) {
            try {
                DataWidgetConfig config = new DataWidgetConfig(value);
                Log.d("WatchFaceConfig", "parseDataWidget success: " + config);
                this.mWidgets.add(config);
                return true;
            } catch (JSONException e) {
            }
        }
        return false;
    }

    private void parseLowPower(Context context, String value) {
        if (value == null) {
            return;
        }
        String lowPowerIconPath;
        if (value.startsWith("@assets/")) {
            lowPowerIconPath = value.substring("@assets/".length());
            if (isAssetsFileExist(this.mContext, getParent(lowPowerIconPath), getFileName(lowPowerIconPath))) {
                this.mLowPowerIconSlptPath = lowPowerIconPath;
                this.mLowPowerIconType = 2;
            }
        } else if (value.startsWith("@wfz/")) {
            lowPowerIconPath = value.substring("@wfz/".length());
            if (this.mExternalLoader != null && this.mExternalLoader.exists(lowPowerIconPath)) {
                this.mLowPowerIconSlptPath = lowPowerIconPath;
                this.mLowPowerIconType = 6;
            }
        }
    }

    public int getBgType() {
        return this.mBgType;
    }

    public Drawable getBgDrawable() {
        return this.mBgDrawable;
    }

    public String getBgPathSlpt() {
        return this.mBgPathSlpt;
    }

    public String getBgPathSlpt26W() {
        return this.mBgPathSlpt26W;
    }

    public TimeHand getTimeHand() {
        return this.mTimeHand;
    }

    public int getTimeHandType() {
        return this.mTimeHandType;
    }

    public String getHourPathSlpt() {
        return this.mTimeHandHourSlptPath;
    }

    public String getMinutePathSlpt() {
        return this.mTimeHandMinuteSlptPath;
    }

    public String getSecondsPathSlpt() {
        return this.mTimeHandSecondsSlptPath;
    }

    public String getHourPathSlpt26W() {
        return this.mTimeHandHourSlpt26WPath;
    }

    public String getMinutePathSlpt26W() {
        return this.mTimeHandMinuteSlpt26WPath;
    }

    public String getSecondsPathSlpt26W() {
        return this.mTimeHandSecondsSlpt26WPath;
    }

    public Drawable getGraduation() {
        return this.mGraduation;
    }

    public int getGraduationType() {
        return this.mGraduationType;
    }

    public String getGraduationPathSlpt() {
        return this.mGraduationSlptPath;
    }

    public String getGraduationPathSlpt26W() {
        return this.mGraduationSlptPath26W;
    }

    public ArrayList<DataWidgetConfig> getDataWidgets() {
        return this.mWidgets;
    }

    public TimeDigital getTimeDigital() {
        return this.mTimeDigital;
    }

    public int getStatusBarPosX() {
        return this.mStatusBarPosX;
    }

    public int getStatusBarPosY() {
        return this.mStatusBarPosY;
    }

    public boolean isSupport26W() {
        return this.mSupport26W;
    }

    public int getLowPowerX() {
        return this.mLowPowerX;
    }

    public int getLowPowerY() {
        return this.mLowPowerY;
    }

    public int getLowPowerIconType() {
        return this.mLowPowerIconType;
    }

    public String getLowPowerIconPath() {
        return this.mLowPowerIconSlptPath;
    }

    private Drawable getDrawableFromResName(Context context, String drawableName) {
        if (drawableName != null && drawableName.length() > 0) {
            Resources res = context.getResources();
            int resId = res.getIdentifier(drawableName, "drawable", context.getPackageName());
            if (resId > 0) {
                try {
                    return res.getDrawable(resId);
                } catch (NotFoundException e) {
                    Log.d("WatchFaceConfig", "getDrawableFromResName: Unable to find drawable " + drawableName);
                }
            } else {
                Log.d("WatchFaceConfig", "getDrawableFromResName: Unable to find drawable " + drawableName);
            }
        }
        return null;
    }

    private Bitmap getBitmapFromResName(Context context, String drawableName) {
        if (drawableName != null && drawableName.length() > 0) {
            Resources res = context.getResources();
            int resId = res.getIdentifier(drawableName, "drawable", context.getPackageName());
            if (resId > 0) {
                try {
                    return BitmapFactory.decodeResource(res, resId);
                } catch (IllegalArgumentException e) {
                    Log.d("WatchFaceConfig", "getBitmapFromResName: Unable to find drawable " + drawableName);
                }
            } else {
                Log.d("WatchFaceConfig", "getBitmapFromResName: Unable to find drawable " + drawableName);
            }
        }
        return null;
    }

    private Drawable parseDrawable(int type, String value) {
        if (value != null && value.length() > 2) {
            switch (type) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    return getDrawableFromResName(this.mContext, value);
                case 2:
                    return Util.decodeBitmapDrawableFromAssets(this.mContext.getResources(), value);
                case 4:
                    String bgPath = this.INTERNAL_PATH + "/watchface/customBg/" + value;
                    if (new File(bgPath).exists()) {
                        return new BitmapDrawable(this.mContext.getResources(), bgPath);
                    }
                    break;
                case 6:
                    if (this.mExternalLoader != null) {
                        InputStream is = this.mExternalLoader.readFileInputStream(value);
                        Bitmap b = BitmapFactory.decodeStream(is);
                        WatchFaceExternalLoader.closeInputStream(is);
                        if (b != null) {
                            return new BitmapDrawable(this.mContext.getResources(), b);
                        }
                    }
                    break;
            }
        }
        return null;
    }

    private Bitmap parseBitmap(int type, String value) {
        Log.d("WatchFaceConfig", "parseBitmap type:" + type + " path=" + value);
        if (value != null && value.length() > 2) {
            switch (type) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    return getBitmapFromResName(this.mContext, value);
                case 2:
                    return Util.decodeImage(this.mContext.getResources(), value);
                case 4:
                    String bgPath = this.INTERNAL_PATH + "/watchface/customBg/" + value;
                    if (new File(bgPath).exists()) {
                        return BitmapFactory.decodeFile(bgPath);
                    }
                    break;
                case 6:
                    if (this.mExternalLoader != null) {
                        InputStream is = this.mExternalLoader.readFileInputStream(value);
                        Bitmap b = BitmapFactory.decodeStream(is);
                        WatchFaceExternalLoader.closeInputStream(is);
                        return b;
                    }
                    break;
            }
        }
        return null;
    }

    public byte[] parseFile(int type, String path) {
        if (path != null && path.length() > 0) {
            switch (type) {
                case 6:
                    if (this.mExternalLoader != null) {
                        return this.mExternalLoader.readFile(path);
                    }
                    break;
            }
        }
        return null;
    }

    private static boolean contantsInList(String[] list, String str) {
        if (list != null) {
            for (String string : list) {
                if (string != null && string.equals(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean isAssetsFileExist(Context context, String path, String name) {
        String[] list = null;
        if (path == null) {
            try {
                path = "";
            } catch (IOException e) {
            }
        }
        list = context.getAssets().list(path);
        return contantsInList(list, name);
    }

    public static String getFileName(String path) {
        int separatorIndex = path.lastIndexOf(File.separator);
        return separatorIndex < 0 ? path : path.substring(separatorIndex + 1, path.length());
    }

    public String getParent(String path) {
        int length = path.length();
        int index = path.lastIndexOf(File.separatorChar);
        if (index == -1 && null > null) {
            index = 2;
        }
        if (index == -1 || path.charAt(length - 1) == File.separatorChar) {
            return null;
        }
        if (path.indexOf(File.separatorChar) == index && path.charAt(0) == File.separatorChar) {
            return path.substring(0, index + 1);
        }
        return path.substring(0, index);
    }
}
