package com.huami.watch.watchface.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.recyclerview.C0051R;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.loader.WatchFaceExternalLoader;
import com.huami.watch.watchface.loader.WfzConstants;
import com.huami.watch.watchface.model.WatchFaceModule;
import com.huami.watch.watchface.model.WatchFaceModuleItem;
import com.huami.watch.watchface.widget.ImageFont;
import com.huami.watch.watchface.widget.TimeDigitalWidget;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class WatchFaceConfigTemplate {
    private static final int[][] SUPPORT_DATA_TYPE = new int[][]{new int[]{1, 0}, new int[]{2, 0}, new int[]{3, 0}, new int[]{10, 0}, new int[]{5, 0}, new int[]{4, 0}, new int[]{8, 0}, new int[]{12, 0}, new int[]{0, 0}};
    private static final int[][] SUPPORT_DATE_TYPE = new int[][]{new int[]{6, 0}, new int[]{6, 1}, new int[]{6, 2}, new int[]{6, 3}, new int[]{0, 0}};
    private String INTERNAL_PATH;
    private boolean destroyed = false;
    private SelectMask mBackgroundMask;
    private ArrayList<Background> mBackgrounds = new ArrayList();
    private OnLoadCompleteListener mCompleteListener;
    private Context mContext;
    private ArrayList<DataWidgetPositionConfig> mDataWidgetPositionConfigs = new ArrayList();
    private int mDefaultBackgroundIndex;
    private int mDefaultGraduationIndex;
    private int mDefaultTimeDigitalIndex;
    private int mDefaultTimeHandIndex;
    private WatchFaceExternalLoader mExternalLoader;
    private SelectMask mGraduationMask;
    private ArrayList<Graduation> mGraduations = new ArrayList();
    private Handler mLoadEventHandler = new C03151();
    private LoadWfzFileTask mLoadWfzFileTask;
    private HashMap<String, Drawable> mLoadedPreviews = new HashMap();
    private HashMap<String, Bitmap> mLoadedPreviewsBmp = new HashMap();
    private SharedPreferences mPreferences;
    private String mSavedBackgroundConfig;
    private int mSavedBackgroundIndex;
    private String mSavedGraduationConfig;
    private int mSavedGraduationIndex;
    private String mSavedTimeDigitalConfig;
    private int mSavedTimeDigitalIndex;
    private String mSavedTimeHandConfig;
    private int mSavedTimeHandIndex;
    private SelectMask mTimeDigitalMask;
    private ArrayList<TimeDigital> mTimeDigitals = new ArrayList();
    private SelectMask mTimeHandMask;
    private ArrayList<TimeHand> mTimeHands = new ArrayList();
    private String mWatchFaceName;

    public interface OnLoadCompleteListener {
        void onLoadComplete(int i);
    }

    class C03151 extends Handler {
        C03151() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 100:
                    int dataType = msg.arg1;
                    Log.d("WatchFaceConfigTemplate", "onLoadCompleted " + dataType);
                    if (WatchFaceConfigTemplate.this.mCompleteListener != null) {
                        WatchFaceConfigTemplate.this.mCompleteListener.onLoadComplete(dataType);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public static abstract class BasePreviewConfig {
        protected String config;
        protected Drawable preview;
        protected String title;

        public abstract String getConfig();

        public abstract Drawable getPreviewDrawable();

        public abstract String getTitle();

        public boolean equals(Object o) {
            if (!(o instanceof BasePreviewConfig) || this.config == null) {
                return false;
            }
            return this.config.equals(((BasePreviewConfig) o).config);
        }
    }

    public static class Background extends BasePreviewConfig {
        public String getConfig() {
            return this.config;
        }

        public Drawable getPreviewDrawable() {
            return this.preview;
        }

        public String getTitle() {
            return this.title;
        }
    }

    public static class DataWidgetConfig extends BasePreviewConfig {
        public String KEY = "DataWidget";
        private int dataType;
        private int id;
        private int model;
        private int f21x;
        private int f22y;

        public DataWidgetConfig(int id, int dataType, int x, int y, int model, String title, Drawable preview) {
            this.KEY = "DataWidget" + id;
            this.id = id;
            this.dataType = dataType;
            this.f21x = x;
            this.f22y = y;
            this.model = model;
            this.title = title;
            this.preview = preview;
            encodeConfig();
        }

        public DataWidgetConfig(String config) throws JSONException {
            this.config = config;
            decodeConfig();
        }

        public String getConfig() {
            return this.config;
        }

        public Drawable getPreviewDrawable() {
            return this.preview;
        }

        public String getTitle() {
            return this.title;
        }

        public int getDataType() {
            return this.dataType;
        }

        public int getX() {
            return this.f21x;
        }

        public int getY() {
            return this.f22y;
        }

        public int getModel() {
            return this.model;
        }

        public String toString() {
            return this.KEY + " dataType:" + this.dataType + " model:" + this.model + " x:" + this.f21x + " y:" + this.f22y;
        }

        private void encodeConfig() {
            try {
                JSONObject obj = new JSONObject();
                obj.put("id", this.id);
                obj.put("dataType", this.dataType);
                obj.put("model", this.model);
                obj.put("x", this.f21x);
                obj.put("y", this.f22y);
                this.config = obj.toString();
            } catch (JSONException e) {
                Log.d("WatchFaceConfigTemplate", "DataWidgetConfig.encodeConfig Error.");
                e.printStackTrace();
            }
        }

        private void decodeConfig() throws JSONException {
            JSONObject obj = new JSONObject(this.config);
            this.id = obj.getInt("id");
            this.dataType = obj.getInt("dataType");
            this.model = obj.getInt("model");
            this.f21x = obj.getInt("x");
            this.f22y = obj.getInt("y");
            this.KEY = "DataWidget" + this.id;
        }
    }

    public static class DataWidgetPositionConfig {
        public String KEY;
        private int id;
        private int mDefaultDataType;
        private int mDefaultModel;
        private String mSavedConfig;
        private int mSavedIndex;
        private SelectMask mask;
        private ArrayList<DataWidgetConfig> widgets;
        private int f23x;
        private int f24y;

        private DataWidgetPositionConfig(int id, int x, int y, String savedConfig, int defaultDataType, int defaultDataModel) {
            this.KEY = "DataWidget";
            this.widgets = new ArrayList();
            this.KEY = "DataWidget" + id;
            this.id = id;
            this.f23x = x;
            this.f24y = y;
            this.mSavedConfig = savedConfig;
            this.mDefaultDataType = defaultDataType;
            this.mDefaultModel = defaultDataModel;
        }

        public int getX() {
            return this.f23x;
        }

        public int getY() {
            return this.f24y;
        }

        public ArrayList<? extends BasePreviewConfig> getDataWidgetConfigs() {
            return this.widgets;
        }

        public SelectMask getDataWidgetMask() {
            return this.mask;
        }

        public void addDataWidget(int dataType, int model, String title, Drawable preview) {
            if (contains(dataType, model)) {
                Log.d("WatchFaceConfigTemplate", "addDataWidget Failed. dataType/model is already exists. dataType: " + dataType + " model: " + model + " pos:[" + this.f23x + ", " + this.f24y + "].");
                return;
            }
            DataWidgetConfig config = new DataWidgetConfig(this.id, dataType, this.f23x, this.f24y, model, title, preview);
            this.widgets.add(config);
            if (this.mSavedConfig != null) {
                if (config.config.equals(this.mSavedConfig)) {
                    this.mSavedIndex = this.widgets.size() - 1;
                }
            } else if (this.mDefaultDataType == config.dataType && this.mDefaultModel == config.model) {
                this.mSavedConfig = config.getConfig();
                this.mSavedIndex = this.widgets.size() - 1;
            }
        }

        private boolean contains(int dataType, int model) {
            for (int i = 0; i < this.widgets.size(); i++) {
                DataWidgetConfig item = (DataWidgetConfig) this.widgets.get(i);
                if (item.dataType == dataType && item.model == model) {
                    return true;
                }
            }
            return false;
        }

        public int getSavedDataWidgetIndex() {
            return this.mSavedIndex;
        }

        public boolean equals(Object o) {
            if ((o instanceof DataWidgetPositionConfig) && this.id == ((DataWidgetPositionConfig) o).id) {
                return true;
            }
            return false;
        }
    }

    public static class Graduation extends BasePreviewConfig {
        public String getConfig() {
            return this.config;
        }

        public Drawable getPreviewDrawable() {
            return this.preview;
        }

        public String getTitle() {
            return this.title;
        }
    }

    public class ImageFontInfo {
        private String configs;
        private SparseArray<String> fontConfigs;
        private ImageFont imageFont;

        private ImageFontInfo(String configs) {
            this.fontConfigs = new SparseArray();
            this.configs = configs;
        }

        private void putChar(char c, String config) {
            this.fontConfigs.put(c, config);
        }

        public ImageFont parseImageFont() {
            if (this.imageFont != null) {
                return this.imageFont;
            }
            this.imageFont = new ImageFont(this.configs);
            for (int i = 0; i < this.fontConfigs.size(); i++) {
                String config = (String) this.fontConfigs.valueAt(i);
                char c = (char) this.fontConfigs.keyAt(i);
                Log.d("WatchFaceConfigTemplate", "parseImageFont found char '" + c + "' config: " + config);
                if (config != null) {
                    Bitmap b = null;
                    if (config.startsWith("@assets/")) {
                        b = WatchFaceConfigTemplate.this.parseBitmap(2, config.substring("@assets/".length()));
                    } else if (config.startsWith("@drawable/")) {
                        b = WatchFaceConfigTemplate.this.parseBitmap(1, config.substring("@drawable/".length()));
                    } else if (config.startsWith("@wfz/")) {
                        b = WatchFaceConfigTemplate.this.parseBitmap(6, config.substring("@wfz/".length()));
                    }
                    if (b != null) {
                        this.imageFont.addChar(c, b);
                        Log.d("WatchFaceConfigTemplate", "  parse done.");
                    }
                }
            }
            return this.imageFont;
        }
    }

    private class LoadWfzFileTask extends AsyncTask<File, Void, Void> {
        private LoadWfzFileTask() {
        }

        protected Void doInBackground(File... params) {
            WatchFaceConfigTemplate.this.loadWfzFile(params[0]);
            return null;
        }

        protected void onCancelled() {
            if (WatchFaceConfigTemplate.this.destroyed) {
                Log.d("WatchFaceConfigTemplate", "Load task finished, destroy again.");
                WatchFaceConfigTemplate.this.onDestroy();
            }
        }
    }

    public static class SelectMask {
        private Drawable mBg;
        private Drawable mFg;

        public Drawable getFgDrawable() {
            return this.mFg;
        }

        public Drawable getBgDrawable() {
            return this.mBg;
        }

        private SelectMask() {
        }
    }

    public static class TimeDigital extends BasePreviewConfig {
        int color = -1;
        String fontConfig;
        int gap;
        int height;
        int model;
        int width;
        int f25x;
        int f26y;

        public TimeDigital(int model, int x, int y, int width, int height, String fontConfig, int gap, String color) {
            this.model = model;
            this.f25x = x;
            this.f26y = y;
            this.width = width;
            this.height = height;
            this.fontConfig = fontConfig;
            this.gap = gap;
            if (color != null) {
                try {
                    this.color = Color.parseColor(color);
                } catch (Exception e) {
                }
            }
            encodeConfig();
        }

        public String getConfig() {
            return this.config;
        }

        public Drawable getPreviewDrawable() {
            return this.preview;
        }

        public String getTitle() {
            return this.title;
        }

        private void encodeConfig() {
            try {
                JSONObject obj = new JSONObject();
                obj.put("model", this.model);
                obj.put("x", this.f25x);
                obj.put("y", this.f26y);
                obj.put("width", this.width);
                obj.put("height", this.height);
                obj.put("fontConfig", this.fontConfig);
                obj.put("gap", this.gap);
                obj.put("color", this.color);
                this.config = obj.toString();
                Log.d("WatchFaceConfigTemplate", "TimeDigital.encodeConfig: " + this.config);
            } catch (JSONException e) {
                Log.d("WatchFaceConfigTemplate", "TimeDigital.encodeConfig Error.");
                e.printStackTrace();
            }
        }
    }

    public static class TimeHand extends BasePreviewConfig {
        public String getConfig() {
            return this.config;
        }

        public Drawable getPreviewDrawable() {
            return this.preview;
        }

        public String getTitle() {
            return this.title;
        }
    }

    public ArrayList<DataWidgetPositionConfig> getDataWidgetPositionConfigs() {
        return this.mDataWidgetPositionConfigs;
    }

    private void addDataWidgetPositionConfig(int id, int x, int y, String maskFgName, String maskBgName, int defaultDataType, int[]... supportDataTypes) {
        DataWidgetPositionConfig configs = new DataWidgetPositionConfig(id, x, y, this.mPreferences.getString("DataWidget" + id, null), defaultDataType, 0);
        SelectMask mask = new SelectMask();
        mask.mFg = parseDrawable(this.mContext, maskFgName);
        mask.mBg = parseDrawable(this.mContext, maskBgName);
        configs.mask = mask;
        for (int i = 0; i < supportDataTypes.length; i++) {
            int dataType = supportDataTypes[i][0];
            int model = supportDataTypes[i][1];
            Drawable preview = getDataWidgetPreview(dataType, model);
            if (preview != null) {
                configs.addDataWidget(dataType, model, getDataWidgetTitle(dataType), preview);
            }
        }
        this.mDataWidgetPositionConfigs.add(configs);
    }

    private Drawable getDataWidgetPreview(int dataType, int model) {
        if (dataType != 6) {
            return parseDrawable(this.mContext, String.format("@assets/datawidget/preview_%d_%d.png", new Object[]{Integer.valueOf(dataType), Integer.valueOf(model)}));
        } else if (Util.needEnAssets() && model == 3) {
            return null;
        } else {
            return parseDrawable(this.mContext, String.format(this.mContext.getString(R.string.custom_widget_preview_date), new Object[]{Integer.valueOf(model)}));
        }
    }

    private String getDataWidgetTitle(int dataType) {
        switch (dataType) {
            case 0:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_nothing);
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_step);
            case 2:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_today_dis);
            case 3:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_total_dis);
            case 4:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_calorie);
            case 5:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_heart_rate);
            case 6:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_date);
            case 8:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_weather);
            case 9:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_aq);
            case 10:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_battery);
            case 11:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_msg);
            case 12:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_floor);
            default:
                return this.mContext.getResources().getString(R.string.custom_data_widget_title_default);
        }
    }

    private WatchFaceConfigTemplate(Context context, String watchFaceName, int type, OnLoadCompleteListener listener) {
        this.mContext = context;
        this.INTERNAL_PATH = context.getFilesDir().getAbsolutePath();
        this.mWatchFaceName = watchFaceName;
        this.mPreferences = context.getSharedPreferences(watchFaceName, 0);
        updateSavedBackgroundConfig();
        updateSavedGraduationConfig();
        updateSavedTimeHandConfig();
        this.mCompleteListener = listener;
    }

    private WatchFaceConfigTemplate(Context context, File wfzFile, int type, OnLoadCompleteListener listener) {
        this.mContext = context;
        this.INTERNAL_PATH = context.getFilesDir().getAbsolutePath();
        this.mCompleteListener = listener;
        this.mLoadWfzFileTask = new LoadWfzFileTask();
        this.mLoadWfzFileTask.execute(new File[]{wfzFile});
    }

    private void notifyLoadCompleted(int dataType) {
        this.mLoadEventHandler.obtainMessage(100, dataType, -1).sendToTarget();
    }

    private void loadWfzFile(File wfzFile) {
        this.mWatchFaceName = wfzFile.getName();
        this.mExternalLoader = WatchFaceExternalLoader.getWatchFaceExternalLoader(wfzFile);
        if (this.mExternalLoader != null) {
            WatchFaceModule watchfaceModule = this.mExternalLoader.parseWatchFace();
            WatchFaceSave preferences = WatchFaceSave.getWatchFaceSave("/sdcard/.watchface/current/" + this.mWatchFaceName);
            if (watchfaceModule != null) {
                List<WatchFaceModuleItem> items = watchfaceModule.getWatchFaceItemList();
                if (items != null && items.size() > 0) {
                    for (int i = 0; i < items.size(); i++) {
                        WatchFaceModuleItem item = (WatchFaceModuleItem) items.get(i);
                        int j;
                        String maskFg;
                        switch (WfzConstants.parseItemType(item.getType())) {
                            case 0:
                                if (preferences != null) {
                                    if (preferences.contains("Background")) {
                                        this.mSavedBackgroundConfig = preferences.getString("Background", null);
                                    }
                                }
                                addBackground(item.getConfig());
                                if (item.getMask() != null) {
                                    setBackgroundMask(item.getMask(), null);
                                } else {
                                    setBackgroundMask("@drawable/watchface_custom_switch_focus_bg", null);
                                }
                                String bgConfigs = item.getConfiglist();
                                if (bgConfigs != null && bgConfigs.startsWith("@wfz/")) {
                                    List<WatchFaceModuleItem> bgs = this.mExternalLoader.parseWatchFaceConfigList(bgConfigs.substring("@wfz/".length()));
                                    if (bgs != null) {
                                        for (j = 0; j < bgs.size(); j++) {
                                            WatchFaceModuleItem bg = (WatchFaceModuleItem) bgs.get(j);
                                            if (WfzConstants.parseItemType(bg.getType()) == 0) {
                                                addBackground(bg.getConfig());
                                            }
                                        }
                                    }
                                }
                                notifyLoadCompleted(0);
                                break;
                            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                                if (preferences != null) {
                                    if (preferences.contains("Graduation")) {
                                        this.mSavedGraduationConfig = preferences.getString("Graduation", null);
                                    }
                                }
                                addGraduation(item.getConfig(), item.getConfig() + "/graduation.png");
                                if (item.getMask() != null) {
                                    setGraduationMask(item.getMask(), null);
                                } else {
                                    setGraduationMask("@drawable/watchface_custom_switch_focus_graduation_fg", null);
                                }
                                String graduationConfigs = item.getConfiglist();
                                if (graduationConfigs != null) {
                                    if (graduationConfigs.startsWith("@wfz/")) {
                                        List<WatchFaceModuleItem> graduations = this.mExternalLoader.parseWatchFaceConfigList(graduationConfigs.substring("@wfz/".length()));
                                        if (graduations != null) {
                                            for (j = 0; j < graduations.size(); j++) {
                                                WatchFaceModuleItem graduation = (WatchFaceModuleItem) graduations.get(j);
                                                if (WfzConstants.parseItemType(graduation.getType()) == 1) {
                                                    addGraduation(graduation.getConfig(), graduation.getConfig() + "/graduation.png");
                                                }
                                            }
                                        }
                                    }
                                }
                                notifyLoadCompleted(1);
                                break;
                            case 2:
                                if (preferences != null) {
                                    if (preferences.contains("TimeHand")) {
                                        this.mSavedTimeHandConfig = preferences.getString("TimeHand", null);
                                    }
                                }
                                addTimeHand(item.getConfig(), item.getConfig() + "/preview.png");
                                if (item.getMask() != null) {
                                    maskFg = item.getMask();
                                    setTimeHandMask(maskFg, maskFg.replace("_fg", "_bg"));
                                }
                                String timeHandConfigs = item.getConfiglist();
                                if (timeHandConfigs != null) {
                                    if (timeHandConfigs.startsWith("@wfz/")) {
                                        List<WatchFaceModuleItem> timeHands = this.mExternalLoader.parseWatchFaceConfigList(timeHandConfigs.substring("@wfz/".length()));
                                        if (timeHands != null) {
                                            for (j = 0; j < timeHands.size(); j++) {
                                                WatchFaceModuleItem timeHand = (WatchFaceModuleItem) timeHands.get(j);
                                                if (WfzConstants.parseItemType(timeHand.getType()) == 2) {
                                                    addTimeHand(timeHand.getConfig(), timeHand.getConfig() + "/preview.png");
                                                }
                                            }
                                        }
                                    }
                                }
                                notifyLoadCompleted(2);
                                break;
                            case 3:
                                if (item.getId() < 0) {
                                    break;
                                }
                                String saved = null;
                                if (preferences != null) {
                                    if (preferences.contains("DataWidget" + item.getId())) {
                                        saved = preferences.getString("DataWidget" + item.getId(), null);
                                    }
                                }
                                DataWidgetPositionConfig configs = new DataWidgetPositionConfig(item.getId(), item.getX(), item.getY(), saved, item.getDataType(), item.getModel());
                                if (this.mDataWidgetPositionConfigs.contains(configs)) {
                                    break;
                                }
                                Drawable preview;
                                if (item.getMask() != null) {
                                    SelectMask selectMask = new SelectMask();
                                    maskFg = item.getMask();
                                    String maskBg = maskFg.replace("_fg", "_bg");
                                    selectMask.mFg = parseDrawable(this.mContext, maskFg);
                                    selectMask.mBg = parseDrawable(this.mContext, maskBg);
                                    configs.mask = selectMask;
                                }
                                String widgetConfigs = item.getConfiglist();
                                if (widgetConfigs != null) {
                                    if (widgetConfigs.startsWith("@wfz/")) {
                                        List<WatchFaceModuleItem> widgets = this.mExternalLoader.parseWatchFaceConfigList(widgetConfigs.substring("@wfz/".length()));
                                        if (widgets != null) {
                                            for (j = 0; j < widgets.size(); j++) {
                                                WatchFaceModuleItem widget = (WatchFaceModuleItem) widgets.get(j);
                                                if (WfzConstants.parseItemType(widget.getType()) == 3 && SingletonWrapper.isSupportDataType(widget.getDataType())) {
                                                    preview = getDataWidgetPreview(widget.getDataType(), widget.getModel());
                                                    if (preview != null) {
                                                        configs.addDataWidget(widget.getDataType(), widget.getModel(), getDataWidgetTitle(widget.getDataType()), preview);
                                                    }
                                                }
                                            }
                                        }
                                        if (configs.widgets.size() <= 0) {
                                            break;
                                        }
                                        this.mDataWidgetPositionConfigs.add(configs);
                                        break;
                                    }
                                }
                                if (SingletonWrapper.isSupportDataType(item.getDataType())) {
                                    preview = getDataWidgetPreview(item.getDataType(), item.getModel());
                                    if (preview != null) {
                                        configs.addDataWidget(item.getDataType(), item.getModel(), getDataWidgetTitle(item.getDataType()), preview);
                                    }
                                }
                                if (configs.widgets.size() <= 0) {
                                    this.mDataWidgetPositionConfigs.add(configs);
                                }
                            case 4:
                                if (preferences != null) {
                                    if (preferences.contains("TimeDigital")) {
                                        this.mSavedTimeDigitalConfig = preferences.getString("TimeDigital", null);
                                    }
                                }
                                addTimeDigital(item.getModel(), item.getX(), item.getY(), item.getWidth(), item.getHeight(), item.getFontConfig(), item.getGap(), item.getColor(), item.getPreview());
                                if (item.getMask() != null) {
                                    maskFg = item.getMask();
                                    setTimeDigitalMask(maskFg, maskFg.replace("_fg", "_bg"));
                                }
                                String timeDigitalConfigs = item.getConfiglist();
                                if (timeDigitalConfigs != null) {
                                    if (timeDigitalConfigs.startsWith("@wfz/")) {
                                        List<WatchFaceModuleItem> timeDigitals = this.mExternalLoader.parseWatchFaceConfigList(timeDigitalConfigs.substring("@wfz/".length()));
                                        if (timeDigitals != null) {
                                            for (j = 0; j < timeDigitals.size(); j++) {
                                                WatchFaceModuleItem timeDigital = (WatchFaceModuleItem) timeDigitals.get(j);
                                                if (WfzConstants.parseItemType(timeDigital.getType()) == 4) {
                                                    addTimeDigital(timeDigital.getModel(), timeDigital.getX(), timeDigital.getY(), timeDigital.getWidth(), timeDigital.getHeight(), timeDigital.getFontConfig(), timeDigital.getGap(), timeDigital.getColor(), timeDigital.getPreview());
                                                }
                                            }
                                        }
                                    }
                                }
                                notifyLoadCompleted(4);
                                break;
                            default:
                                break;
                        }
                    }
                    if (this.mDataWidgetPositionConfigs.size() > 0) {
                        notifyLoadCompleted(3);
                    }
                }
            }
            this.mExternalLoader.close();
        }
    }

    public static WatchFaceConfigTemplate loadWatchFaceTemplate(Context context, String watchfaceName, OnLoadCompleteListener listener) {
        Log.d("WatchFaceConfigTemplate", "loadWatchFaceTemplate " + watchfaceName);
        if (watchfaceName == null || watchfaceName.length() <= 0) {
            Log.d("WatchFaceConfigTemplate", "Invalid serviceName: " + watchfaceName);
        } else if (watchfaceName.endsWith(".wfz")) {
            File wfzFile = WatchFaceExternalLoader.findWatchFaceFile(watchfaceName);
            if (wfzFile != null) {
                return new WatchFaceConfigTemplate(context, wfzFile, 2, listener);
            }
        } else if ("com.huami.watch.watchface.AnalogWatchFaceThirteen".equals(watchfaceName)) {
            template = new WatchFaceConfigTemplate(context, watchfaceName, 1, listener);
            template.setDefaultBackgroundIndex(0);
            template.addBackground("@drawable/watchface_bg_analog_thirteen_0");
            template.addBackground("@drawable/watchface_bg_analog_thirteen_1");
            template.addBackground("@drawable/watchface_bg_analog_thirteen_2");
            template.addBackground("@drawable/watchface_bg_analog_thirteen_4");
            template.addBackground("@drawable/watchface_bg_analog_thirteen_5");
            template.addBackground("@drawable/watchface_bg_analog_thirteen_6");
            template.addBackground("@drawable/watchface_bg_analog_thirteen_7");
            template.addBackground("@drawable/watchface_bg_analog_thirteen_8");
            template.loadCurrentBg();
            template.setBackgroundMask("@drawable/watchface_custom_switch_focus_bg", null);
            template.addGraduation("@drawable/watchface_default_thirteen_graduation", "@drawable/watchface_default_thirteen_graduation");
            template.addTimeHand("@drawable/watchface_default_thirteen_", "@drawable/watchface_default_thirteen_timehand_preview");
            template.addDataWidgetPositionConfig(3, 100, 94, "@assets/datawidget/mask_3_fg.png", "@assets/datawidget/mask_3_bg.png", 6, SUPPORT_DATE_TYPE);
            template.notifyLoadCompleted(0);
            template.notifyLoadCompleted(1);
            template.notifyLoadCompleted(2);
            template.notifyLoadCompleted(3);
            return template;
        } else if ("com.huami.watch.watchface.AnalogWatchFaceEleven".equals(watchfaceName)) {
            template = new WatchFaceConfigTemplate(context, watchfaceName, 1, listener);
            template.setDefaultBackgroundIndex(0);
            template.addBackground("@drawable/watchface_default_eleven_bg");
            template.addBackground("@drawable/watchface_default_eleven_bg_1");
            template.addBackground("@drawable/watchface_default_eleven_bg_2");
            template.addBackground("@drawable/watchface_default_eleven_bg_3");
            template.loadCurrentBg();
            template.setBackgroundMask("@drawable/watchface_custom_switch_focus_bg", null);
            template.addGraduation("@drawable/watchface_default_eleven_graduation", "@drawable/watchface_default_eleven_graduation_preview");
            template.addTimeHand("@drawable/watchface_default_eleven_", "@drawable/watchface_default_eleven_timehand_preview");
            template.addDataWidgetPositionConfig(3, 100, 94, "@assets/datawidget/mask_3_fg.png", "@assets/datawidget/mask_3_bg.png", 6, SUPPORT_DATE_TYPE);
            template.notifyLoadCompleted(0);
            template.notifyLoadCompleted(1);
            template.notifyLoadCompleted(2);
            template.notifyLoadCompleted(3);
            return template;
        } else if ("com.huami.watch.watchface.AnalogWatchFaceFourteen".equals(watchfaceName)) {
            template = new WatchFaceConfigTemplate(context, watchfaceName, 1, listener);
            template.setDefaultBackgroundIndex(0);
            template.addBackground("@drawable/watchface_default_fourteen_bg");
            template.addBackground("@drawable/watchface_default_fourteen_bg_1");
            template.addBackground("@drawable/watchface_default_fourteen_bg_2");
            template.addBackground("@drawable/watchface_default_fourteen_bg_3");
            template.addBackground("@drawable/watchface_default_fourteen_bg_4");
            template.loadCurrentBg();
            template.setBackgroundMask("@drawable/watchface_custom_switch_focus_bg", null);
            template.setDefaultGraduationIndex(0);
            template.addGraduation("@assets/graduation/01", "@assets/graduation/01/graduation.png");
            template.addGraduation("@assets/graduation/02", "@assets/graduation/02/graduation.png");
            template.addGraduation("@assets/graduation/03", "@assets/graduation/03/graduation.png");
            template.addGraduation("@assets/graduation/04", "@assets/graduation/04/graduation.png");
            template.addGraduation("@assets/graduation/05", "@assets/graduation/05/graduation.png");
            template.addGraduation("@assets/graduation/00", "@assets/graduation/00/graduation.png");
            template.setGraduationMask("@drawable/watchface_custom_switch_focus_graduation_fg", null);
            template.setDefaultTimeHandIndex(0);
            template.addTimeHand("@assets/timehand/01", "@assets/timehand/01/preview.png");
            template.addTimeHand("@assets/timehand/02", "@assets/timehand/02/preview.png");
            template.addTimeHand("@assets/timehand/03", "@assets/timehand/03/preview.png");
            template.setTimeHandMask("@assets/timehand/watchface_custom_select_timehand_fg_0.png", "@assets/timehand/watchface_custom_select_timehand_bg_0.png");
            template.addDataWidgetPositionConfig(0, 52, 118, "@assets/datawidget/mask_0_fg.png", "@assets/datawidget/mask_0_bg.png", 10, SUPPORT_DATA_TYPE);
            template.addDataWidgetPositionConfig(1, 119, 187, "@assets/datawidget/mask_1_fg.png", "@assets/datawidget/mask_1_bg.png", 1, SUPPORT_DATA_TYPE);
            template.addDataWidgetPositionConfig(2, 184, 118, "@assets/datawidget/mask_2_fg.png", "@assets/datawidget/mask_2_bg.png", 2, SUPPORT_DATA_TYPE);
            template.addDataWidgetPositionConfig(3, 100, 94, "@assets/datawidget/mask_3_fg.png", "@assets/datawidget/mask_3_bg.png", 6, SUPPORT_DATE_TYPE);
            template.notifyLoadCompleted(0);
            template.notifyLoadCompleted(1);
            template.notifyLoadCompleted(2);
            template.notifyLoadCompleted(3);
            return template;
        } else if ("com.huami.watch.watchface.AnalogWatchFaceFifteen".equals(watchfaceName)) {
            template = new WatchFaceConfigTemplate(context, watchfaceName, 1, listener);
            template.setDefaultBackgroundIndex(0);
            template.addBackground("@drawable/watchface_bg_analog_fifteen_0");
            template.addBackground("@drawable/watchface_bg_analog_fifteen_1");
            template.addBackground("@drawable/watchface_bg_analog_fifteen_2");
            template.loadCurrentBg();
            template.setBackgroundMask("@drawable/watchface_custom_switch_focus_bg", null);
            template.addGraduation("@assets/graduation/06", "@assets/graduation/06/graduation.png");
            template.setGraduationMask("@drawable/watchface_custom_switch_focus_graduation_fg", null);
            template.addTimeHand("@assets/timehand/04", "@assets/timehand/04/preview.png");
            template.setTimeHandMask("@assets/timehand/watchface_custom_select_timehand_fg_0.png", "@assets/timehand/watchface_custom_select_timehand_bg_0.png");
            template.notifyLoadCompleted(0);
            template.notifyLoadCompleted(1);
            template.notifyLoadCompleted(2);
            return template;
        }
        return null;
    }

    public void refresh() {
        if (this.mBackgrounds.size() > 0 && ((Background) this.mBackgrounds.get(this.mBackgrounds.size() - 1)).config.startsWith("@customBg/")) {
            this.mBackgrounds.remove(this.mBackgrounds.size() - 1);
        }
        loadCurrentBg();
    }

    private void addBackground(String config) {
        Background bg = new Background();
        bg.config = config;
        bg.title = this.mContext.getResources().getString(R.string.custom_title_bg);
        if (!this.mBackgrounds.contains(bg)) {
            Drawable d = parseDrawable(this.mContext, config);
            if (d != null) {
                bg.preview = d;
                this.mBackgrounds.add(bg);
                if (this.mSavedBackgroundConfig != null) {
                    if (bg.config.equals(this.mSavedBackgroundConfig)) {
                        this.mSavedBackgroundIndex = this.mBackgrounds.size() - 1;
                    }
                } else if (this.mDefaultBackgroundIndex == this.mBackgrounds.size() - 1) {
                    this.mSavedBackgroundIndex = this.mDefaultBackgroundIndex;
                    this.mSavedBackgroundConfig = bg.getConfig();
                }
            }
        }
    }

    private void setDefaultBackgroundIndex(int defaultIndex) {
        this.mDefaultBackgroundIndex = defaultIndex;
    }

    public ArrayList<? extends BasePreviewConfig> getBackgrounds() {
        return this.mBackgrounds;
    }

    private void setBackgroundMask(String fgName, String bgName) {
        this.mBackgroundMask = new SelectMask();
        this.mBackgroundMask.mFg = parseDrawable(this.mContext, fgName);
        this.mBackgroundMask.mBg = parseDrawable(this.mContext, bgName);
    }

    public SelectMask getBackgroundMask() {
        return this.mBackgroundMask;
    }

    private void updateSavedBackgroundConfig() {
        this.mSavedBackgroundConfig = this.mPreferences.getString("Background", null);
    }

    public int getSavedBackgroundIndex() {
        return this.mSavedBackgroundIndex;
    }

    private void addGraduation(String config, String drawableName) {
        Graduation item = new Graduation();
        item.config = config;
        if (!this.mGraduations.contains(item)) {
            Drawable d = parseDrawable(this.mContext, drawableName);
            if (d != null) {
                item.title = this.mContext.getResources().getString(R.string.custom_title_graduation);
                item.preview = d;
                this.mGraduations.add(item);
                if (this.mSavedGraduationConfig != null) {
                    if (item.config.equals(this.mSavedGraduationConfig)) {
                        this.mSavedGraduationIndex = this.mGraduations.size() - 1;
                    }
                } else if (this.mDefaultGraduationIndex == this.mGraduations.size() - 1) {
                    this.mSavedGraduationIndex = this.mDefaultGraduationIndex;
                    this.mSavedGraduationConfig = item.getConfig();
                }
            }
        }
    }

    private void setDefaultGraduationIndex(int defaultIndex) {
        this.mDefaultGraduationIndex = defaultIndex;
    }

    public ArrayList<? extends BasePreviewConfig> getGraduations() {
        return this.mGraduations;
    }

    private void setGraduationMask(String fgName, String bgName) {
        this.mGraduationMask = new SelectMask();
        this.mGraduationMask.mFg = parseDrawable(this.mContext, fgName);
        this.mGraduationMask.mBg = parseDrawable(this.mContext, bgName);
    }

    public SelectMask getGraduationMask() {
        return this.mGraduationMask;
    }

    private void updateSavedGraduationConfig() {
        this.mSavedGraduationConfig = this.mPreferences.getString("Graduation", null);
    }

    public int getSavedGraduationIndex() {
        return this.mSavedGraduationIndex;
    }

    private void addTimeHand(String config, String drawableName) {
        TimeHand item = new TimeHand();
        item.config = config;
        if (!this.mTimeHands.contains(item)) {
            Drawable d = parseDrawable(this.mContext, drawableName);
            if (d != null) {
                item.title = this.mContext.getResources().getString(R.string.custom_title_timehand);
                item.preview = d;
                this.mTimeHands.add(item);
                if (this.mSavedTimeHandConfig != null) {
                    if (item.config.equals(this.mSavedTimeHandConfig)) {
                        this.mSavedTimeHandIndex = this.mTimeHands.size() - 1;
                    }
                } else if (this.mDefaultTimeHandIndex == this.mTimeHands.size() - 1) {
                    this.mSavedTimeHandIndex = this.mDefaultTimeHandIndex;
                    this.mSavedTimeHandConfig = item.getConfig();
                }
            }
        }
    }

    private void setDefaultTimeHandIndex(int defaultIndex) {
        this.mDefaultTimeHandIndex = defaultIndex;
    }

    public ArrayList<? extends BasePreviewConfig> getTimeHands() {
        return this.mTimeHands;
    }

    private void setTimeHandMask(String fgName, String bgName) {
        this.mTimeHandMask = new SelectMask();
        this.mTimeHandMask.mFg = parseDrawable(this.mContext, fgName);
        this.mTimeHandMask.mBg = parseDrawable(this.mContext, bgName);
    }

    public SelectMask getTimeHandMask() {
        return this.mTimeHandMask;
    }

    private void updateSavedTimeHandConfig() {
        this.mSavedTimeHandConfig = this.mPreferences.getString("TimeHand", null);
    }

    public int getSavedTimeHandIndex() {
        return this.mSavedTimeHandIndex;
    }

    private void addTimeDigital(int model, int x, int y, int width, int height, String fontConfig, int gap, String color, String drawableName) {
        TimeDigital item = new TimeDigital(model, x, y, width, height, fontConfig, gap, color);
        if (!this.mTimeDigitals.contains(item)) {
            Drawable d = parseDrawable(this.mContext, drawableName);
            if (d == null && fontConfig != null) {
                try {
                    if (fontConfig.startsWith("@wfz/")) {
                        List<WatchFaceModuleItem> chars = this.mExternalLoader.parseWatchFaceConfigList(fontConfig.substring("@wfz/".length()) + "/font.xml");
                        if (chars != null) {
                            ImageFontInfo imageFontInfo = new ImageFontInfo(fontConfig);
                            for (int i = 0; i < chars.size(); i++) {
                                WatchFaceModuleItem fontitem = (WatchFaceModuleItem) chars.get(i);
                                if ("font".equals(fontitem.getType())) {
                                    String charset = fontitem.getCharset();
                                    if (charset == null || charset.length() != 1) {
                                        Log.d("WatchFaceConfigTemplate", "parseImageFontInfo invalid charset: " + charset);
                                    } else {
                                        imageFontInfo.putChar(charset.charAt(0), fontitem.getConfig());
                                    }
                                }
                            }
                            ImageFont font = imageFontInfo.parseImageFont();
                            int colorInt = 0;
                            if (color != null) {
                                try {
                                    colorInt = Color.parseColor(color);
                                } catch (Exception e) {
                                }
                            }
                            TimeDigitalWidget preview = new TimeDigitalWidget(model, x, y, width, height, font, colorInt, gap);
                            Bitmap out = Bitmap.createBitmap(320, 320, Config.ARGB_8888);
                            Canvas canvas = new Canvas(out);
                            canvas.translate((float) x, (float) y);
                            preview.onDraw(canvas);
                            canvas.setBitmap(null);
                            d = new BitmapDrawable(this.mContext.getResources(), out);
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (d != null) {
                item.title = this.mContext.getResources().getString(R.string.custom_title_timedigital);
                item.preview = d;
                this.mTimeDigitals.add(item);
                if (this.mSavedTimeDigitalConfig != null) {
                    if (item.config.equals(this.mSavedTimeDigitalConfig)) {
                        this.mSavedTimeDigitalIndex = this.mTimeDigitals.size() - 1;
                    }
                } else if (this.mDefaultTimeDigitalIndex == this.mTimeDigitals.size() - 1) {
                    this.mSavedTimeDigitalIndex = this.mDefaultTimeDigitalIndex;
                    this.mSavedTimeDigitalConfig = item.getConfig();
                }
            }
        }
    }

    public ArrayList<? extends BasePreviewConfig> getTimeDigitals() {
        return this.mTimeDigitals;
    }

    private void setTimeDigitalMask(String fgName, String bgName) {
        this.mTimeDigitalMask = new SelectMask();
        this.mTimeDigitalMask.mFg = parseDrawable(this.mContext, fgName);
        this.mTimeDigitalMask.mBg = parseDrawable(this.mContext, bgName);
    }

    public SelectMask getTimeDigitalMask() {
        return this.mTimeDigitalMask;
    }

    public int getSavedTimeDigitalIndex() {
        return this.mSavedTimeDigitalIndex;
    }

    private Drawable getDrawableFromResName(Context context, String name) {
        if (name != null && name.length() > 0) {
            Resources res = context.getResources();
            int resId = res.getIdentifier(name, "drawable", "com.huami.watch.watchface.analogyellow");
            if (resId > 0) {
                try {
                    return res.getDrawable(resId);
                } catch (NotFoundException e) {
                    Log.d("WatchFaceConfigTemplate", "Unable to find drawable " + name);
                }
            } else {
                Log.d("WatchFaceConfigTemplate", "Unable to find drawable " + name);
            }
        }
        return null;
    }

    private void recycleLoadedPreviews() {
        for (String config : this.mLoadedPreviews.keySet()) {
            if (!(config == null || config.startsWith("@drawable/"))) {
                BitmapDrawable d = (BitmapDrawable) this.mLoadedPreviews.get(config);
                if (!(d.getBitmap() == null || d.getBitmap().isRecycled())) {
                    Log.d("WatchFaceConfigTemplate", "recycle " + config);
                    d.getBitmap().recycle();
                }
            }
        }
        this.mLoadedPreviews.clear();
        for (String config2 : this.mLoadedPreviewsBmp.keySet()) {
            if (!(config2 == null || config2.startsWith(Integer.toString(1)))) {
                Bitmap b = (Bitmap) this.mLoadedPreviewsBmp.get(config2);
                if (!(b == null || b.isRecycled())) {
                    Log.d("WatchFaceConfigTemplate", "recycle " + config2);
                    b.recycle();
                }
            }
        }
        this.mLoadedPreviewsBmp.clear();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable parseDrawable(android.content.Context r11, java.lang.String r12) {
        /*
        r10 = this;
        r7 = "WatchFaceConfigTemplate";
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "parseBg ";
        r8 = r8.append(r9);
        r8 = r8.append(r12);
        r8 = r8.toString();
        android.util.Log.d(r7, r8);
        if (r12 == 0) goto L_0x00fb;
    L_0x001a:
        r7 = r12.length();
        r8 = 2;
        if (r7 <= r8) goto L_0x00fb;
    L_0x0021:
        r7 = r10.mLoadedPreviews;
        r2 = r7.get(r12);
        r2 = (android.graphics.drawable.Drawable) r2;
        if (r2 == 0) goto L_0x002c;
    L_0x002b:
        return r2;
    L_0x002c:
        r7 = "@";
        r7 = r12.startsWith(r7);
        if (r7 == 0) goto L_0x00f3;
    L_0x0034:
        r7 = "@drawable/";
        r7 = r12.startsWith(r7);
        if (r7 == 0) goto L_0x0052;
    L_0x003c:
        r7 = "@drawable/";
        r7 = r7.length();
        r6 = r12.substring(r7);
        r2 = r10.getDrawableFromResName(r11, r6);
        if (r2 == 0) goto L_0x002b;
    L_0x004c:
        r7 = r10.mLoadedPreviews;
        r7.put(r12, r2);
        goto L_0x002b;
    L_0x0052:
        r7 = "@customBg/";
        r7 = r12.startsWith(r7);
        if (r7 == 0) goto L_0x0097;
    L_0x005a:
        r7 = "@customBg/";
        r7 = r7.length();
        r4 = r12.substring(r7);
        r7 = new java.lang.StringBuilder;
        r7.<init>();
        r8 = r10.INTERNAL_PATH;
        r7 = r7.append(r8);
        r8 = "/watchface/customBg/";
        r7 = r7.append(r8);
        r7 = r7.append(r4);
        r1 = r7.toString();
        r3 = new java.io.File;
        r3.<init>(r1);
        r7 = r3.exists();
        if (r7 == 0) goto L_0x00fb;
    L_0x0088:
        r2 = new android.graphics.drawable.BitmapDrawable;
        r7 = r11.getResources();
        r2.<init>(r7, r1);
        r7 = r10.mLoadedPreviews;
        r7.put(r12, r2);
        goto L_0x002b;
    L_0x0097:
        r7 = "@assets/";
        r7 = r12.startsWith(r7);
        if (r7 == 0) goto L_0x00ba;
    L_0x009f:
        r7 = "@assets/";
        r7 = r7.length();
        r4 = r12.substring(r7);
        r7 = r11.getResources();
        r2 = com.huami.watch.watchface.util.Util.decodeBitmapDrawableFromAssets(r7, r4);
        if (r2 == 0) goto L_0x002b;
    L_0x00b3:
        r7 = r10.mLoadedPreviews;
        r7.put(r12, r2);
        goto L_0x002b;
    L_0x00ba:
        r7 = "@wfz/";
        r7 = r12.startsWith(r7);
        if (r7 == 0) goto L_0x00fb;
    L_0x00c2:
        r7 = "@wfz/";
        r7 = r7.length();
        r4 = r12.substring(r7);
        r7 = r10.mExternalLoader;
        if (r7 == 0) goto L_0x00fb;
    L_0x00d0:
        r7 = r10.mExternalLoader;
        r5 = r7.readFileInputStream(r4);
        r0 = android.graphics.BitmapFactory.decodeStream(r5);
        com.huami.watch.watchface.loader.WatchFaceExternalLoader.closeInputStream(r5);
        if (r0 == 0) goto L_0x00fb;
    L_0x00df:
        r2 = new android.graphics.drawable.BitmapDrawable;
        r7 = r10.mContext;
        r7 = r7.getResources();
        r2.<init>(r7, r0);
        if (r2 == 0) goto L_0x002b;
    L_0x00ec:
        r7 = r10.mLoadedPreviews;
        r7.put(r12, r2);
        goto L_0x002b;
    L_0x00f3:
        r7 = "#";
        r7 = r12.startsWith(r7);
        if (r7 == 0) goto L_0x00fb;
    L_0x00fb:
        r2 = 0;
        goto L_0x002b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huami.watch.watchface.util.WatchFaceConfigTemplate.parseDrawable(android.content.Context, java.lang.String):android.graphics.drawable.Drawable");
    }

    private Bitmap parseBitmap(int type, String value) {
        Log.d("WatchFaceConfigTemplate", "parseBitmap type:" + type + " path=" + value);
        if (value != null && value.length() > 2) {
            String key = type + value;
            Bitmap cache = (Bitmap) this.mLoadedPreviewsBmp.get(key);
            if (cache == null) {
                switch (type) {
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        cache = getBitmapFromResName(this.mContext, value);
                        if (cache == null) {
                            return cache;
                        }
                        this.mLoadedPreviewsBmp.put(key, cache);
                        return cache;
                    case 2:
                        cache = Util.decodeImage(this.mContext.getResources(), value);
                        if (cache == null) {
                            return cache;
                        }
                        this.mLoadedPreviewsBmp.put(key, cache);
                        return cache;
                    case 4:
                        String bgPath = this.INTERNAL_PATH + "/watchface/customBg/" + value;
                        if (new File(bgPath).exists()) {
                            cache = BitmapFactory.decodeFile(bgPath);
                            if (cache == null) {
                                return cache;
                            }
                            this.mLoadedPreviewsBmp.put(key, cache);
                            return cache;
                        }
                        break;
                    case 6:
                        if (this.mExternalLoader != null) {
                            InputStream is = this.mExternalLoader.readFileInputStream(value);
                            cache = BitmapFactory.decodeStream(is);
                            WatchFaceExternalLoader.closeInputStream(is);
                            if (cache == null) {
                                return cache;
                            }
                            this.mLoadedPreviewsBmp.put(key, cache);
                            return cache;
                        }
                        break;
                }
            }
            Log.d("WatchFaceConfigTemplate", "  reuse cache.");
            return cache;
        }
        return null;
    }

    private Bitmap getBitmapFromResName(Context context, String drawableName) {
        if (drawableName != null && drawableName.length() > 0) {
            Resources res = context.getResources();
            int resId = res.getIdentifier(drawableName, "drawable", context.getPackageName());
            if (resId > 0) {
                try {
                    return BitmapFactory.decodeResource(res, resId);
                } catch (IllegalArgumentException e) {
                    Log.d("WatchFaceConfigTemplate", "getBitmapFromResName: Unable to find drawable " + drawableName);
                }
            } else {
                Log.d("WatchFaceConfigTemplate", "getBitmapFromResName: Unable to find drawable " + drawableName);
            }
        }
        return null;
    }

    private void loadCurrentBg() {
        int oldsize;
        if (this.mWatchFaceName.endsWith(".wfz")) {
            WatchFaceSave preferences = WatchFaceSave.getWatchFaceSave("/sdcard/.watchface/current/" + this.mWatchFaceName);
            if (preferences.contains("Background")) {
                this.mSavedBackgroundConfig = preferences.getString("Background", null);
                if (!TextUtils.isEmpty(this.mSavedBackgroundConfig)) {
                    oldsize = this.mBackgrounds.size();
                    addBackground(this.mSavedBackgroundConfig);
                    if (this.mBackgrounds.size() > oldsize) {
                        this.mSavedBackgroundIndex = this.mBackgrounds.size() - 1;
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        SharedPreferences preferences2 = this.mContext.getSharedPreferences(this.mWatchFaceName, 0);
        if (preferences2.contains("Background")) {
            this.mSavedBackgroundConfig = preferences2.getString("Background", null);
            if (!TextUtils.isEmpty(this.mSavedBackgroundConfig)) {
                oldsize = this.mBackgrounds.size();
                addBackground(this.mSavedBackgroundConfig);
                if (this.mBackgrounds.size() > oldsize) {
                    this.mSavedBackgroundIndex = this.mBackgrounds.size() - 1;
                }
            }
        }
    }

    public void onDestroy() {
        Log.d("WatchFaceConfigTemplate", "onDestroy");
        this.destroyed = true;
        this.mCompleteListener = null;
        if (!(this.mLoadWfzFileTask == null || this.mLoadWfzFileTask.isCancelled())) {
            this.mLoadWfzFileTask.cancel(true);
        }
        this.mLoadEventHandler.removeMessages(100);
        recycleLoadedPreviews();
    }
}
