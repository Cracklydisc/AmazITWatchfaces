package com.huami.watch.watchface.util;

import android.util.Log;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.json.JSONException;
import org.json.JSONObject;

public class WatchFaceSave {
    private JSONObject mSave;
    private File mSaveFile;

    private WatchFaceSave(File f) {
        this.mSaveFile = f;
        if (f.exists() && f.isFile()) {
            String saveString = load(f);
            if (saveString != null) {
                try {
                    this.mSave = new JSONObject(saveString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            File parent = f.getParentFile();
            if (parent != null) {
                parent.mkdirs();
            }
        }
        if (this.mSave == null) {
            this.mSave = new JSONObject();
        }
    }

    public static final WatchFaceSave getWatchFaceSave(String wfzPath) {
        if (wfzPath == null || !wfzPath.endsWith(".wfz")) {
            Log.d("WatchFaceSave", "not support watchface. " + wfzPath);
            return null;
        }
        String savePath = new StringBuffer(wfzPath).delete(wfzPath.length() - ".wfz".length(), wfzPath.length()).append(".save").toString();
        File f = new File(savePath);
        if (!f.exists()) {
            Log.d("WatchFaceSave", "no save file found. " + savePath);
        }
        return new WatchFaceSave(f);
    }

    private String load(File f) {
        FileNotFoundException e;
        Throwable th;
        IOException e2;
        InputStream is = null;
        DataInputStream dis = null;
        String save = null;
        try {
            InputStream is2 = new FileInputStream(f);
            try {
                DataInputStream dis2 = new DataInputStream(is2);
                try {
                    save = dis2.readUTF();
                    Log.d("WatchFaceSave", "  load success.");
                    if (is2 != null) {
                        try {
                            is2.close();
                        } catch (IOException e3) {
                        }
                    }
                    if (dis2 != null) {
                        try {
                            dis2.close();
                        } catch (IOException e4) {
                            dis = dis2;
                            is = is2;
                        }
                    }
                    dis = dis2;
                    is = is2;
                } catch (FileNotFoundException e5) {
                    e = e5;
                    dis = dis2;
                    is = is2;
                    try {
                        e.printStackTrace();
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e6) {
                            }
                        }
                        if (dis != null) {
                            try {
                                dis.close();
                            } catch (IOException e7) {
                            }
                        }
                        return save;
                    } catch (Throwable th2) {
                        th = th2;
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e8) {
                            }
                        }
                        if (dis != null) {
                            try {
                                dis.close();
                            } catch (IOException e9) {
                            }
                        }
                        throw th;
                    }
                } catch (IOException e10) {
                    e2 = e10;
                    dis = dis2;
                    is = is2;
                    e2.printStackTrace();
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e11) {
                        }
                    }
                    if (dis != null) {
                        try {
                            dis.close();
                        } catch (IOException e12) {
                        }
                    }
                    return save;
                } catch (Throwable th3) {
                    th = th3;
                    dis = dis2;
                    is = is2;
                    if (is != null) {
                        is.close();
                    }
                    if (dis != null) {
                        dis.close();
                    }
                    throw th;
                }
            } catch (FileNotFoundException e13) {
                e = e13;
                is = is2;
                e.printStackTrace();
                if (is != null) {
                    is.close();
                }
                if (dis != null) {
                    dis.close();
                }
                return save;
            } catch (IOException e14) {
                e2 = e14;
                is = is2;
                e2.printStackTrace();
                if (is != null) {
                    is.close();
                }
                if (dis != null) {
                    dis.close();
                }
                return save;
            } catch (Throwable th4) {
                th = th4;
                is = is2;
                if (is != null) {
                    is.close();
                }
                if (dis != null) {
                    dis.close();
                }
                throw th;
            }
        } catch (FileNotFoundException e15) {
            e = e15;
            e.printStackTrace();
            if (is != null) {
                is.close();
            }
            if (dis != null) {
                dis.close();
            }
            return save;
        } catch (IOException e16) {
            e2 = e16;
            e2.printStackTrace();
            if (is != null) {
                is.close();
            }
            if (dis != null) {
                dis.close();
            }
            return save;
        }
        return save;
    }

    public boolean commit() {
        OutputStream out;
        DataOutputStream dos;
        FileNotFoundException e;
        Throwable th;
        IOException e2;
        if (this.mSaveFile != null) {
            OutputStream out2 = null;
            DataOutputStream dos2 = null;
            try {
                out = new FileOutputStream(this.mSaveFile);
                try {
                    dos = new DataOutputStream(out);
                } catch (FileNotFoundException e3) {
                    e = e3;
                    out2 = out;
                    try {
                        e.printStackTrace();
                        if (out2 != null) {
                            try {
                                out2.close();
                            } catch (IOException e4) {
                            }
                        }
                        if (dos2 != null) {
                            try {
                                dos2.close();
                            } catch (IOException e5) {
                            }
                        }
                        Log.d("WatchFaceSave", "  save failure.");
                        return false;
                    } catch (Throwable th2) {
                        th = th2;
                        if (out2 != null) {
                            try {
                                out2.close();
                            } catch (IOException e6) {
                            }
                        }
                        if (dos2 != null) {
                            try {
                                dos2.close();
                            } catch (IOException e7) {
                            }
                        }
                        throw th;
                    }
                } catch (IOException e8) {
                    e2 = e8;
                    out2 = out;
                    e2.printStackTrace();
                    if (out2 != null) {
                        try {
                            out2.close();
                        } catch (IOException e9) {
                        }
                    }
                    if (dos2 != null) {
                        try {
                            dos2.close();
                        } catch (IOException e10) {
                        }
                    }
                    Log.d("WatchFaceSave", "  save failure.");
                    return false;
                } catch (Throwable th3) {
                    th = th3;
                    out2 = out;
                    if (out2 != null) {
                        out2.close();
                    }
                    if (dos2 != null) {
                        dos2.close();
                    }
                    throw th;
                }
            } catch (FileNotFoundException e11) {
                e = e11;
                e.printStackTrace();
                if (out2 != null) {
                    out2.close();
                }
                if (dos2 != null) {
                    dos2.close();
                }
                Log.d("WatchFaceSave", "  save failure.");
                return false;
            } catch (IOException e12) {
                e2 = e12;
                e2.printStackTrace();
                if (out2 != null) {
                    out2.close();
                }
                if (dos2 != null) {
                    dos2.close();
                }
                Log.d("WatchFaceSave", "  save failure.");
                return false;
            }
            try {
                dos.writeUTF(this.mSave.toString());
                dos.flush();
                Log.d("WatchFaceSave", "  save success.");
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e13) {
                    }
                }
                if (dos == null) {
                    return true;
                }
                try {
                    dos.close();
                    return true;
                } catch (IOException e14) {
                    return true;
                }
            } catch (FileNotFoundException e15) {
                e = e15;
                dos2 = dos;
                out2 = out;
                e.printStackTrace();
                if (out2 != null) {
                    out2.close();
                }
                if (dos2 != null) {
                    dos2.close();
                }
                Log.d("WatchFaceSave", "  save failure.");
                return false;
            } catch (IOException e16) {
                e2 = e16;
                dos2 = dos;
                out2 = out;
                e2.printStackTrace();
                if (out2 != null) {
                    out2.close();
                }
                if (dos2 != null) {
                    dos2.close();
                }
                Log.d("WatchFaceSave", "  save failure.");
                return false;
            } catch (Throwable th4) {
                th = th4;
                dos2 = dos;
                out2 = out;
                if (out2 != null) {
                    out2.close();
                }
                if (dos2 != null) {
                    dos2.close();
                }
                throw th;
            }
        }
        Log.d("WatchFaceSave", "  save failure.");
        return false;
    }

    public boolean contains(String key) {
        return this.mSave.has(key);
    }

    public String getString(String key, String defValue) {
        try {
            String value = this.mSave.getString(key);
            if (value != null) {
                return value;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return defValue;
    }

    public WatchFaceSave putString(String key, String value) {
        try {
            this.mSave.put(key, value);
            Log.d("WatchFaceSave", "  putString key = " + key + " value = " + value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return this;
    }
}
