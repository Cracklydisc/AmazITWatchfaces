package com.huami.watch.watchface.utils;

import com.huami.watch.watchface.model.WatchFaceInfoModule;
import com.huami.watch.watchface.model.WatchFaceModule;
import com.huami.watch.watchface.model.WatchFaceModuleItem;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class WatchFaceXmlSaxParser {

    private static class WatchFaceHandler extends DefaultHandler {
        private List<WatchFaceModuleItem> watchFaceItemList;
        private WatchFaceModule watchface;

        private WatchFaceHandler() {
        }

        private WatchFaceModule getWatchFace() {
            return this.watchface;
        }

        public void startDocument() throws SAXException {
            this.watchface = new WatchFaceModule();
            this.watchFaceItemList = new ArrayList();
        }

        public void endDocument() throws SAXException {
            this.watchface.setWatchFaceItemList(this.watchFaceItemList);
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            int i;
            if ("WatchFaceItem".equals(localName)) {
                if (attributes != null) {
                    WatchFaceModuleItem item = new WatchFaceModuleItem();
                    for (i = 0; i < attributes.getLength(); i++) {
                        item.setValue(attributes.getLocalName(i), attributes.getValue(i));
                    }
                    this.watchFaceItemList.add(item);
                }
            } else if ("WatchFace".equals(localName) && attributes != null) {
                for (i = 0; i < attributes.getLength(); i++) {
                    this.watchface.setValue(attributes.getLocalName(i), attributes.getValue(i));
                }
            }
        }
    }

    private static class WatchFaceInfoHandler extends DefaultHandler {
        private WatchFaceInfoModule info;
        private Stack<String> mKeyStack;

        private WatchFaceInfoHandler() {
            this.mKeyStack = new Stack();
        }

        private WatchFaceInfoModule getWatchFaceInfo() {
            return this.info;
        }

        public void startDocument() throws SAXException {
            this.info = new WatchFaceInfoModule();
            this.mKeyStack.clear();
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            this.info.setValue((String) this.mKeyStack.peek(), new String(ch, start, length).trim());
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            this.mKeyStack.push(localName);
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            this.mKeyStack.pop();
        }
    }

    public static WatchFaceInfoModule parseWatchFaceInfo(InputStream inputStream) {
        WatchFaceInfoModule watchFaceInfoModule = null;
        if (inputStream != null) {
            try {
                SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
                WatchFaceInfoHandler handler = new WatchFaceInfoHandler();
                parser.parse(inputStream, handler);
                watchFaceInfoModule = handler.getWatchFaceInfo();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return watchFaceInfoModule;
    }

    public static WatchFaceModule parseWatchFace(InputStream inputStream) {
        WatchFaceModule watchFaceModule = null;
        if (inputStream != null) {
            try {
                SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
                WatchFaceHandler handler = new WatchFaceHandler();
                parser.parse(inputStream, handler);
                watchFaceModule = handler.getWatchFace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return watchFaceModule;
    }
}
