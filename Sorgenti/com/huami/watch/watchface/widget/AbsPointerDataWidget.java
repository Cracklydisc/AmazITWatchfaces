package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.util.SingletonWrapper;

public abstract class AbsPointerDataWidget extends AbsWatchFaceDataWidget {
    private RectF mBound;
    private Path mClip;
    private int mDataType;
    private Paint mGPaint;
    private int mHalfH;
    private int mHalfW;
    private int mHeight = 108;
    private Drawable mPointer;
    private int mWidth = 108;
    private int mX;
    private int mY;
    protected Resources resources;

    protected abstract float getProgress();

    public AbsPointerDataWidget(Resources resources, int x, int y, int width, int height, int dataType) {
        if (SingletonWrapper.isSupportDataType(dataType)) {
            this.mDataType = dataType;
            this.resources = resources;
            this.mX = x;
            this.mY = y;
            this.mWidth = width;
            this.mHeight = height;
            this.mHalfW = this.mWidth / 2;
            this.mHalfH = this.mHeight / 2;
            this.mGPaint = new Paint(5);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setColor(-65536);
            this.mGPaint.setStrokeCap(Cap.ROUND);
            this.mGPaint.setStrokeWidth(2.0f);
            initLayout();
            return;
        }
        throw new IllegalArgumentException("Not support dataType: " + dataType);
    }

    private void initLayout() {
        this.mClip = new Path();
        this.mBound = new RectF(-2.0f, -2.0f, (float) (this.mWidth + 2), (float) (this.mHeight + 2));
        this.mClip.addRect(this.mBound, Direction.CCW);
    }

    public final int getDataType() {
        return this.mDataType;
    }

    public final int getX() {
        return this.mX;
    }

    public final int getY() {
        return this.mY;
    }

    public final void onDraw(Canvas canvas) {
        canvas.save(2);
        canvas.clipPath(this.mClip);
        onDrawInternal(canvas);
        canvas.drawArc(0.0f, 0.0f, (float) this.mWidth, (float) this.mHeight, -90.0f, getProgress() * 360.0f, false, this.mGPaint);
        canvas.save(1);
        canvas.translate((float) this.mHalfW, (float) this.mHalfH);
        canvas.rotate(getProgress() * 360.0f);
        this.mPointer.draw(canvas);
        canvas.restore();
        canvas.restore();
    }

    protected void onDrawInternal(Canvas canvas) {
    }

    public void setPointerDrawable(Drawable pointer) {
        this.mPointer = pointer;
        if (this.mPointer != null) {
            int width = this.mPointer.getIntrinsicWidth();
            int height = this.mPointer.getIntrinsicHeight();
            int x = (-width) / 2;
            int y = (-height) / 2;
            this.mPointer.setBounds(x, y, x + width, y + height);
        }
    }
}
