package com.huami.watch.watchface.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;

public abstract class AbsTextWidget extends AbsWatchFaceDataWidget {
    private ImageFont fonts;
    private Paint mGPaint = new Paint(3);
    private int mTextWidth;
    private int f27x;
    private int f28y;

    protected abstract String getText();

    public AbsTextWidget(int x, int y, ImageFont fonts) {
        this.f27x = x;
        this.f28y = y;
        this.fonts = fonts;
    }

    public void setTextAlign(Align align) {
        this.mGPaint.setTextAlign(align);
    }

    public int getX() {
        return this.f27x;
    }

    public int getY() {
        return this.f28y;
    }

    protected int getTextWidth() {
        return this.mTextWidth;
    }

    public void onDraw(Canvas canvas) {
        String text = getText();
        if (this.fonts != null && text != null) {
            this.mTextWidth = this.fonts.getFontWidth(text);
            this.fonts.drawText(canvas, text, 0, 0, this.mGPaint);
        }
    }
}
