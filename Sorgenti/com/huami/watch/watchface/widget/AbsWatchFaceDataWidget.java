package com.huami.watch.watchface.widget;

import android.graphics.Canvas;
import com.huami.watch.watchface.WatchDataListener;

public abstract class AbsWatchFaceDataWidget implements WatchDataListener {
    public abstract int getX();

    public abstract int getY();

    public abstract void onDraw(Canvas canvas);

    public void onDestroy() {
    }
}
