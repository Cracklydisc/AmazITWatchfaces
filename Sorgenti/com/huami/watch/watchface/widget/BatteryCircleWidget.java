package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;

public class BatteryCircleWidget extends AbsWatchFaceDataWidget {
    private Bitmap[] mBatterys;
    private RectF mBound;
    private int mCenterX;
    private int mCenterY;
    private Path mClipPathBig = new Path();
    private Paint mGPaint;
    private int mHeight;
    private float mLeft;
    private float mMaxDegree;
    private int mMaxProgress;
    private Bitmap mProgress;
    private int mProgressBattery;
    private float mProgressDegreeBattery;
    private float mStartDegree;
    private float mTop;
    private int mWidth;

    public BatteryCircleWidget(Resources resources, int centerX, int centerY, Bitmap[] batterys, Bitmap progress, float startDegree, float maxDegree) {
        this.mCenterX = centerX;
        this.mCenterY = centerY;
        this.mBatterys = batterys;
        this.mStartDegree = startDegree;
        this.mMaxDegree = maxDegree;
        if (this.mBatterys != null && this.mBatterys.length > 0) {
            this.mMaxProgress = this.mBatterys.length - 1;
        }
        this.mProgress = progress;
        if (this.mProgress != null) {
            this.mWidth = this.mProgress.getWidth();
            this.mHeight = this.mProgress.getHeight();
            float hW = ((float) this.mWidth) * 0.5f;
            float hH = ((float) this.mHeight) * 0.5f;
            this.mLeft = -hW;
            this.mTop = -hH;
            this.mBound = new RectF(-hW, -hH, hW, hH);
        }
        this.mGPaint = new Paint(3);
        this.mGPaint.setStyle(Style.STROKE);
        this.mGPaint.setStrokeWidth(17.0f);
    }

    public int getDataType() {
        return 10;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 10) {
            updateBatteryLevel(((Integer) values[0]).intValue(), ((Integer) values[1]).intValue());
        }
    }

    private void updateBatteryLevel(int batteryLevel, int batteryMax) {
        if (batteryMax > 0) {
            float scale = Math.min((((float) batteryLevel) * 1.0f) / ((float) batteryMax), 1.0f);
            this.mProgressBattery = (int) (((float) this.mMaxProgress) * scale);
            this.mProgressDegreeBattery = this.mMaxDegree * scale;
        } else {
            this.mProgressBattery = 0;
            this.mProgressDegreeBattery = 0.0f;
        }
        this.mClipPathBig.rewind();
        this.mClipPathBig.addArc(this.mBound, this.mStartDegree, this.mProgressDegreeBattery);
        this.mClipPathBig.lineTo(0.0f, 0.0f);
        this.mClipPathBig.close();
    }

    public int getX() {
        return this.mCenterX;
    }

    public int getY() {
        return this.mCenterY;
    }

    public void onDraw(Canvas canvas) {
        if (!(this.mProgress == null || this.mProgress.isRecycled())) {
            canvas.save(2);
            canvas.clipPath(this.mClipPathBig);
            canvas.drawBitmap(this.mProgress, this.mLeft, this.mTop, this.mGPaint);
            canvas.restore();
        }
        if (this.mBatterys != null && this.mBatterys.length > 0) {
            Bitmap battery = this.mBatterys[this.mProgressBattery];
            if (battery != null && !battery.isRecycled()) {
                canvas.drawBitmap(battery, ((float) battery.getWidth()) * -0.5f, ((float) battery.getHeight()) * -0.5f, this.mGPaint);
            }
        }
    }
}
