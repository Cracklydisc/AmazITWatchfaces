package com.huami.watch.watchface.widget;

import android.graphics.Bitmap;

public class BatteryLevelWidget extends LevelWidget {
    private int mX;
    private int mY;

    public BatteryLevelWidget(int x, int y, Bitmap[] levels) {
        super(levels);
        this.mX = x;
        this.mY = y;
    }

    public int getDataType() {
        return 10;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 10) {
            int batteryLevel = ((Integer) values[0]).intValue();
            int batteryMax = ((Integer) values[1]).intValue();
            float progress = 0.0f;
            if (batteryMax > 0) {
                progress = (((float) batteryLevel) * 1.0f) / ((float) batteryMax);
            }
            super.onDataUpdate(dataType, Float.valueOf(progress));
        }
    }

    public int getX() {
        return this.mX;
    }

    public int getY() {
        return this.mY;
    }
}
