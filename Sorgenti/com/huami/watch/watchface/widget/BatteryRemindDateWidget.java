package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;

public class BatteryRemindDateWidget extends DefaultWatchFaceDataWidget {
    private int MAX_LEVEL;
    private String mBattery;
    private int mBatteryLevel;
    private float mBatteryPercent;
    private Drawable[] mLevels;

    public BatteryRemindDateWidget(Resources resources, int x, int y, int model) {
        this(resources, x, y, model, null);
    }

    public BatteryRemindDateWidget(Resources resources, int x, int y, int model, ImageFont fonts) {
        this(resources, x, y, -1, -1, model, fonts);
    }

    public BatteryRemindDateWidget(Resources resources, int x, int y, int width, int height, int model, ImageFont fonts) {
        super(resources, x, y, width, height, 10, model, fonts);
        this.MAX_LEVEL = 10;
        this.mBattery = "0%";
        this.mBatteryPercent = 1.0f;
        this.mBatteryLevel = 0;
        this.mLevels = loadLevelDrawables(model);
        this.MAX_LEVEL = this.mLevels.length;
    }

    protected Drawable[] loadLevelDrawables(int model) {
        int maxLevel;
        String ICON_PATH;
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                maxLevel = 11;
                ICON_PATH = "datawidget/slpt/watchface_custom_fun_icon_slpt_battery1_%d.png";
                break;
            case 2:
                setProgressBitmap(((BitmapDrawable) this.resources.getDrawable(R.drawable.watchface_custom_fun_icon_circle_r_2, null)).getBitmap());
                maxLevel = 10;
                ICON_PATH = "datawidget/slpt/watchface_custom_fun_icon_slpt_battery2_%d.png";
                break;
            case 3:
                setProgressBitmap(Util.decodeImage(this.resources, "datawidget/model_3/icon/watchface_custom_fun_icon_circle_r.png"));
                maxLevel = 10;
                ICON_PATH = "datawidget/model_3/8C/icon/watchface_custom_fun_icon_slpt_battery_%d.png";
                break;
            case 4:
                maxLevel = 10;
                ICON_PATH = "datawidget/model_4/icon/watchface_custom_fun_icon_slpt_battery%d.png";
                break;
            default:
                setProgressBitmap(((BitmapDrawable) this.resources.getDrawable(R.drawable.watchface_custom_fun_icon_circle_r, null)).getBitmap());
                maxLevel = 10;
                ICON_PATH = "datawidget/slpt/watchface_custom_fun_icon_slpt_battery%d.png";
                break;
        }
        Drawable[] levels = new Drawable[maxLevel];
        for (int i = 0; i < maxLevel; i++) {
            levels[i] = Util.decodeBitmapDrawableFromAssets(this.resources, String.format(ICON_PATH, new Object[]{Integer.valueOf(i + 1)}));
        }
        return levels;
    }

    protected boolean isSupportProgress() {
        return true;
    }

    protected float getProgress() {
        return this.mBatteryPercent;
    }

    protected Drawable getIconDrawable(Resources resources) {
        return this.mLevels[this.mBatteryLevel];
    }

    protected String getText() {
        return this.mBattery;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (10 == dataType && values != null && values[0] != null) {
            int battery = ((Integer) values[0]).intValue();
            this.mBatteryPercent = (((float) battery) * 1.0f) / ((float) ((Integer) values[1]).intValue());
            this.mBattery = String.valueOf(battery) + "%";
            this.mBatteryLevel = Math.min((int) (this.mBatteryPercent * ((float) (this.MAX_LEVEL - 1))), this.MAX_LEVEL - 1);
        }
    }
}
