package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.SingletonWrapper;
import com.huami.watch.watchface.util.Util;

public abstract class DefaultWatchFaceDataWidget extends AbsWatchFaceDataWidget {
    private RectF mBound;
    private Path mClip;
    private Path mClipProgressPath;
    private int mDataType;
    private ImageFont mFonts;
    private Paint mGPaint;
    private int mGravity;
    private int mHeight;
    private int mLeftIcon;
    private int mLeftText;
    private float mMaxDegree;
    private int mModel;
    private int mPaddingToDrawable;
    private boolean mProcessBitmapRecyled;
    private Bitmap mProgressBackground;
    private boolean mProgressBgRecyled;
    private Bitmap mProgressBitmap;
    private float mStartDegree;
    private int mTopIcon;
    private int mTopText;
    private int mWidth;
    private int mX;
    private int mY;
    protected Resources resources;

    protected abstract Drawable getIconDrawable(Resources resources);

    protected abstract float getProgress();

    protected abstract String getText();

    protected abstract boolean isSupportProgress();

    public DefaultWatchFaceDataWidget(Resources resources, int x, int y, int dataType, int model) {
        this(resources, x, y, dataType, model, null);
    }

    public DefaultWatchFaceDataWidget(Resources resources, int x, int y, int dataType, int model, ImageFont fonts) {
        this(resources, x, y, -1, -1, dataType, model, fonts);
    }

    public DefaultWatchFaceDataWidget(Resources resources, int x, int y, int width, int height, int dataType, int model, ImageFont fonts) {
        this.mWidth = 84;
        this.mHeight = 84;
        this.mModel = 0;
        this.mGravity = 0;
        this.mClipProgressPath = new Path();
        this.mStartDegree = -90.0f;
        this.mMaxDegree = 360.0f;
        if (SingletonWrapper.isSupportDataType(dataType)) {
            this.mDataType = dataType;
            this.resources = resources;
            this.mX = x;
            this.mY = y;
            this.mModel = model;
            this.mWidth = width;
            this.mHeight = height;
            this.mGPaint = new Paint(3);
            if (fonts != null) {
                this.mFonts = fonts;
            } else {
                this.mFonts = new ImageFont("default");
            }
            initLayout();
            return;
        }
        throw new IllegalArgumentException("Not support dataType: " + dataType);
    }

    private void initLayout() {
        switch (this.mModel) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                if (this.mWidth < 0) {
                    this.mWidth = 120;
                }
                if (this.mHeight < 0) {
                    this.mHeight = 21;
                }
                this.mPaddingToDrawable = 7;
                this.mGPaint.setTextAlign(Align.LEFT);
                this.mGravity = 1;
                if (this.mFonts.size() == 0) {
                    this.mFonts.addChar('0', Util.decodeImage(this.resources, "datawidget/font/02/default_0.png"));
                    this.mFonts.addChar('1', Util.decodeImage(this.resources, "datawidget/font/02/default_1.png"));
                    this.mFonts.addChar('2', Util.decodeImage(this.resources, "datawidget/font/02/default_2.png"));
                    this.mFonts.addChar('3', Util.decodeImage(this.resources, "datawidget/font/02/default_3.png"));
                    this.mFonts.addChar('4', Util.decodeImage(this.resources, "datawidget/font/02/default_4.png"));
                    this.mFonts.addChar('5', Util.decodeImage(this.resources, "datawidget/font/02/default_5.png"));
                    this.mFonts.addChar('6', Util.decodeImage(this.resources, "datawidget/font/02/default_6.png"));
                    this.mFonts.addChar('7', Util.decodeImage(this.resources, "datawidget/font/02/default_7.png"));
                    this.mFonts.addChar('8', Util.decodeImage(this.resources, "datawidget/font/02/default_8.png"));
                    this.mFonts.addChar('9', Util.decodeImage(this.resources, "datawidget/font/02/default_9.png"));
                    this.mFonts.addChar('.', Util.decodeImage(this.resources, "datawidget/font/02/dot.png"));
                    this.mFonts.addChar('%', Util.decodeImage(this.resources, "datawidget/font/02/percent_icon.png"));
                    this.mFonts.addChar('-', Util.decodeImage(this.resources, "datawidget/font/02/minus.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(this.resources, "datawidget/font/02/space.png"));
                    this.mFonts.addChar('/', Util.decodeImage(this.resources, "datawidget/font/02/separator.png"));
                    this.mFonts.addChar('℃', Util.decodeImage(this.resources, "datawidget/font/02/celsius.png"));
                    this.mFonts.addChar('℉', Util.decodeImage(this.resources, "datawidget/font/02/fahrenheit.png"));
                }
                this.mTopText = (this.mHeight - this.mFonts.getCharHeight('0')) / 2;
                break;
            case 2:
                if (this.mWidth < 0) {
                    this.mWidth = 84;
                }
                if (this.mHeight < 0) {
                    this.mHeight = 84;
                }
                this.mGPaint.setTextAlign(Align.CENTER);
                this.mLeftText = this.mWidth / 2;
                this.mTopIcon = this.resources.getDimensionPixelSize(R.dimen.custom_widget_icon_top_2);
                this.mPaddingToDrawable = this.resources.getDimensionPixelSize(R.dimen.custom_widget_text_padding_to_drawable_2);
                if (this.mFonts.size() == 0) {
                    this.mFonts.addChar('0', Util.decodeImage(this.resources, "datawidget/font/03/0.png"));
                    this.mFonts.addChar('1', Util.decodeImage(this.resources, "datawidget/font/03/1.png"));
                    this.mFonts.addChar('2', Util.decodeImage(this.resources, "datawidget/font/03/2.png"));
                    this.mFonts.addChar('3', Util.decodeImage(this.resources, "datawidget/font/03/3.png"));
                    this.mFonts.addChar('4', Util.decodeImage(this.resources, "datawidget/font/03/4.png"));
                    this.mFonts.addChar('5', Util.decodeImage(this.resources, "datawidget/font/03/5.png"));
                    this.mFonts.addChar('6', Util.decodeImage(this.resources, "datawidget/font/03/6.png"));
                    this.mFonts.addChar('7', Util.decodeImage(this.resources, "datawidget/font/03/7.png"));
                    this.mFonts.addChar('8', Util.decodeImage(this.resources, "datawidget/font/03/8.png"));
                    this.mFonts.addChar('9', Util.decodeImage(this.resources, "datawidget/font/03/9.png"));
                    this.mFonts.addChar('.', Util.decodeImage(this.resources, "datawidget/font/03/dot.png"));
                    this.mFonts.addChar('%', Util.decodeImage(this.resources, "datawidget/font/03/percent_icon.png"));
                    this.mFonts.addChar('-', Util.decodeImage(this.resources, "datawidget/font/03/minus.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(this.resources, "datawidget/font/03/space.png"));
                    this.mFonts.addChar('/', Util.decodeImage(this.resources, "datawidget/font/03/separator.png"));
                    this.mFonts.addChar('℃', Util.decodeImage(this.resources, "datawidget/font/03/celsius.png"));
                    this.mFonts.addChar('℉', Util.decodeImage(this.resources, "datawidget/font/03/fahrenheit.png"));
                    break;
                }
                break;
            case 3:
                if (this.mWidth < 0) {
                    this.mWidth = 100;
                }
                if (this.mHeight < 0) {
                    this.mHeight = 100;
                }
                this.mGPaint.setTextAlign(Align.CENTER);
                this.mLeftText = this.mWidth / 2;
                this.mTopIcon = this.resources.getDimensionPixelSize(R.dimen.custom_widget_icon_top_3);
                this.mPaddingToDrawable = 0;
                if (this.mFonts.size() == 0) {
                    this.mFonts.addChar('0', Util.decodeImage(this.resources, "datawidget/model_3/font/0.png"));
                    this.mFonts.addChar('1', Util.decodeImage(this.resources, "datawidget/model_3/font/1.png"));
                    this.mFonts.addChar('2', Util.decodeImage(this.resources, "datawidget/model_3/font/2.png"));
                    this.mFonts.addChar('3', Util.decodeImage(this.resources, "datawidget/model_3/font/3.png"));
                    this.mFonts.addChar('4', Util.decodeImage(this.resources, "datawidget/model_3/font/4.png"));
                    this.mFonts.addChar('5', Util.decodeImage(this.resources, "datawidget/model_3/font/5.png"));
                    this.mFonts.addChar('6', Util.decodeImage(this.resources, "datawidget/model_3/font/6.png"));
                    this.mFonts.addChar('7', Util.decodeImage(this.resources, "datawidget/model_3/font/7.png"));
                    this.mFonts.addChar('8', Util.decodeImage(this.resources, "datawidget/model_3/font/8.png"));
                    this.mFonts.addChar('9', Util.decodeImage(this.resources, "datawidget/model_3/font/9.png"));
                    this.mFonts.addChar('.', Util.decodeImage(this.resources, "datawidget/model_3/font/dot.png"));
                    this.mFonts.addChar('%', Util.decodeImage(this.resources, "datawidget/model_3/font/percent_icon.png"));
                    this.mFonts.addChar('-', Util.decodeImage(this.resources, "datawidget/model_3/font/minus.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(this.resources, "datawidget/model_3/font/space.png"));
                    this.mFonts.addChar('/', Util.decodeImage(this.resources, "datawidget/model_3/font/separator.png"));
                    this.mFonts.addChar('℃', Util.decodeImage(this.resources, "datawidget/model_3/font/celsius.png"));
                    this.mFonts.addChar('℉', Util.decodeImage(this.resources, "datawidget/model_3/font/fahrenheit.png"));
                    break;
                }
                break;
            case 4:
                if (this.mWidth < 0) {
                    this.mWidth = 100;
                }
                if (this.mHeight < 0) {
                    this.mHeight = 24;
                }
                this.mPaddingToDrawable = 5;
                this.mGPaint.setTextAlign(Align.LEFT);
                if (this.mFonts.size() == 0) {
                    this.mFonts.addChar('0', Util.decodeImage(this.resources, "datawidget/model_4/font/0.png"));
                    this.mFonts.addChar('1', Util.decodeImage(this.resources, "datawidget/model_4/font/1.png"));
                    this.mFonts.addChar('2', Util.decodeImage(this.resources, "datawidget/model_4/font/2.png"));
                    this.mFonts.addChar('3', Util.decodeImage(this.resources, "datawidget/model_4/font/3.png"));
                    this.mFonts.addChar('4', Util.decodeImage(this.resources, "datawidget/model_4/font/4.png"));
                    this.mFonts.addChar('5', Util.decodeImage(this.resources, "datawidget/model_4/font/5.png"));
                    this.mFonts.addChar('6', Util.decodeImage(this.resources, "datawidget/model_4/font/6.png"));
                    this.mFonts.addChar('7', Util.decodeImage(this.resources, "datawidget/model_4/font/7.png"));
                    this.mFonts.addChar('8', Util.decodeImage(this.resources, "datawidget/model_4/font/8.png"));
                    this.mFonts.addChar('9', Util.decodeImage(this.resources, "datawidget/model_4/font/9.png"));
                    this.mFonts.addChar('.', Util.decodeImage(this.resources, "datawidget/model_4/font/dot.png"));
                    this.mFonts.addChar('%', Util.decodeImage(this.resources, "datawidget/model_4/font/percent_icon.png"));
                    this.mFonts.addChar('-', Util.decodeImage(this.resources, "datawidget/model_4/font/minus.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(this.resources, "datawidget/model_4/font/space.png"));
                    this.mFonts.addChar('/', Util.decodeImage(this.resources, "datawidget/model_4/font/separator.png"));
                    this.mFonts.addChar('℃', Util.decodeImage(this.resources, "datawidget/model_4/font/celsius.png"));
                    this.mFonts.addChar('℉', Util.decodeImage(this.resources, "datawidget/model_4/font/fahrenheit.png"));
                    break;
                }
                break;
            case 17:
                if (this.mWidth < 0) {
                    this.mWidth = 100;
                }
                if (this.mHeight < 0) {
                    this.mHeight = 100;
                }
                this.mTopText = 0;
                this.mGPaint.setTextAlign(Align.CENTER);
                this.mLeftText = this.mWidth / 2;
                if (this.mFonts.size() == 0) {
                    this.mFonts.addChar('0', Util.decodeImage(this.resources, "datawidget/font/17_1/0.png"));
                    this.mFonts.addChar('1', Util.decodeImage(this.resources, "datawidget/font/17_1/1.png"));
                    this.mFonts.addChar('2', Util.decodeImage(this.resources, "datawidget/font/17_1/2.png"));
                    this.mFonts.addChar('3', Util.decodeImage(this.resources, "datawidget/font/17_1/3.png"));
                    this.mFonts.addChar('4', Util.decodeImage(this.resources, "datawidget/font/17_1/4.png"));
                    this.mFonts.addChar('5', Util.decodeImage(this.resources, "datawidget/font/17_1/5.png"));
                    this.mFonts.addChar('6', Util.decodeImage(this.resources, "datawidget/font/17_1/6.png"));
                    this.mFonts.addChar('7', Util.decodeImage(this.resources, "datawidget/font/17_1/7.png"));
                    this.mFonts.addChar('8', Util.decodeImage(this.resources, "datawidget/font/17_1/8.png"));
                    this.mFonts.addChar('9', Util.decodeImage(this.resources, "datawidget/font/17_1/9.png"));
                    this.mFonts.addChar('.', Util.decodeImage(this.resources, "datawidget/font/17_1/point.png"));
                    this.mFonts.addChar('%', Util.decodeImage(this.resources, "datawidget/font/17_1/percent.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(this.resources, "datawidget/font/01/space.png"));
                    this.mFonts.addChar('℃', Util.decodeImage(this.resources, "datawidget/font/01/celsius.png"));
                    this.mFonts.addChar('℉', Util.decodeImage(this.resources, "datawidget/font/01/fahrenheit.png"));
                    break;
                }
                break;
            default:
                if (this.mWidth < 0) {
                    this.mWidth = 84;
                }
                if (this.mHeight < 0) {
                    this.mHeight = 84;
                }
                this.mGPaint.setTextAlign(Align.CENTER);
                this.mLeftText = this.mWidth / 2;
                this.mTopIcon = this.resources.getDimensionPixelSize(R.dimen.custom_widget_icon_top);
                this.mPaddingToDrawable = this.resources.getDimensionPixelSize(R.dimen.custom_widget_text_padding_to_drawable);
                setProgressBackground(((BitmapDrawable) this.resources.getDrawable(R.drawable.watchface_custom_fun_icon_circle_w)).getBitmap());
                if (this.mFonts.size() == 0) {
                    this.mFonts.addChar('0', Util.decodeImage(this.resources, "datawidget/font/01/default_0.png"));
                    this.mFonts.addChar('1', Util.decodeImage(this.resources, "datawidget/font/01/default_1.png"));
                    this.mFonts.addChar('2', Util.decodeImage(this.resources, "datawidget/font/01/default_2.png"));
                    this.mFonts.addChar('3', Util.decodeImage(this.resources, "datawidget/font/01/default_3.png"));
                    this.mFonts.addChar('4', Util.decodeImage(this.resources, "datawidget/font/01/default_4.png"));
                    this.mFonts.addChar('5', Util.decodeImage(this.resources, "datawidget/font/01/default_5.png"));
                    this.mFonts.addChar('6', Util.decodeImage(this.resources, "datawidget/font/01/default_6.png"));
                    this.mFonts.addChar('7', Util.decodeImage(this.resources, "datawidget/font/01/default_7.png"));
                    this.mFonts.addChar('8', Util.decodeImage(this.resources, "datawidget/font/01/default_8.png"));
                    this.mFonts.addChar('9', Util.decodeImage(this.resources, "datawidget/font/01/default_9.png"));
                    this.mFonts.addChar('.', Util.decodeImage(this.resources, "datawidget/font/01/dot.png"));
                    this.mFonts.addChar('%', Util.decodeImage(this.resources, "datawidget/font/01/percent_icon.png"));
                    this.mFonts.addChar('-', Util.decodeImage(this.resources, "datawidget/font/01/minus.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(this.resources, "datawidget/font/01/space.png"));
                    this.mFonts.addChar('/', Util.decodeImage(this.resources, "datawidget/font/01/separator.png"));
                    this.mFonts.addChar('℃', Util.decodeImage(this.resources, "datawidget/font/01/celsius.png"));
                    this.mFonts.addChar('℉', Util.decodeImage(this.resources, "datawidget/font/01/fahrenheit.png"));
                    break;
                }
                break;
        }
        this.mClip = new Path();
        this.mBound = new RectF(-2.0f, -2.0f, (float) (this.mWidth + 2), (float) (this.mHeight + 2));
        this.mClip.addRect(this.mBound, Direction.CCW);
    }

    public void setPaddingToDrawable(int paddingToDrawable) {
        this.mPaddingToDrawable = paddingToDrawable;
    }

    public void setIconTop(int iconTop) {
        this.mTopIcon = iconTop;
    }

    public void setGravity(int gravity) {
        this.mGravity = gravity;
    }

    public final int getDataType() {
        return this.mDataType;
    }

    public final int getX() {
        return this.mX;
    }

    public final int getY() {
        return this.mY;
    }

    protected int getModel() {
        return this.mModel;
    }

    public final void setProgressBitmap(Bitmap bitmap) {
        setProgressBitmap(bitmap, false);
    }

    public final void setProgressBitmap(Bitmap bitmap, boolean recyled) {
        if (bitmap != this.mProgressBitmap) {
            if (this.mProcessBitmapRecyled && this.mProgressBitmap != null) {
                this.mProgressBitmap.recycle();
            }
            this.mProcessBitmapRecyled = recyled;
            this.mProgressBitmap = bitmap;
        }
    }

    public final void setProgressBackground(Bitmap bitmap) {
        setProgressBackground(bitmap, false);
    }

    public final void setProgressBackground(Bitmap bitmap, boolean recyled) {
        if (bitmap != this.mProgressBackground) {
            if (this.mProgressBgRecyled && this.mProgressBackground != null) {
                this.mProgressBackground.recycle();
            }
            this.mProgressBgRecyled = recyled;
            this.mProgressBackground = bitmap;
        }
    }

    public final void onDraw(Canvas canvas) {
        canvas.save(2);
        canvas.clipPath(this.mClip);
        Drawable mIcon = getIconDrawable(this.resources);
        String text = getText();
        switch (this.mModel) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
            case 4:
                int textwidth = this.mFonts.getFontWidth(text);
                int hgrav = this.mGravity & 7;
                if (mIcon == null) {
                    switch (hgrav) {
                        case 3:
                            this.mLeftIcon = 0;
                            break;
                        case 5:
                            this.mLeftIcon = this.mWidth - textwidth;
                            break;
                        default:
                            this.mLeftText = (this.mWidth - textwidth) / 2;
                            break;
                    }
                }
                int iconwidth = mIcon.getIntrinsicWidth();
                int iconheight = mIcon.getIntrinsicHeight();
                int totalwidth = (this.mPaddingToDrawable + iconwidth) + textwidth;
                switch (hgrav) {
                    case 3:
                        this.mLeftIcon = 0;
                        break;
                    case 5:
                        this.mLeftIcon = this.mWidth - totalwidth;
                        break;
                    default:
                        this.mLeftIcon = (this.mWidth - totalwidth) / 2;
                        break;
                }
                this.mTopIcon = (this.mHeight - iconheight) / 2;
                mIcon.setBounds(this.mLeftIcon, this.mTopIcon, this.mLeftIcon + iconwidth, this.mTopIcon + mIcon.getIntrinsicHeight());
                this.mLeftText = (this.mLeftIcon + iconwidth) + this.mPaddingToDrawable;
                this.mTopText = (this.mHeight - this.mFonts.getCharHeight('0')) / 2;
                mIcon.draw(canvas);
                if (text != null) {
                    drawText(canvas, text);
                    break;
                }
                break;
            case 17:
                if (text != null) {
                    drawText(canvas, text);
                    break;
                }
                break;
            default:
                if (!(this.mProgressBackground == null || this.mProgressBackground.isRecycled())) {
                    calculateProgress(1.0f);
                    canvas.save(2);
                    canvas.clipPath(this.mClipProgressPath);
                    canvas.drawBitmap(this.mProgressBackground, 0.0f, 0.0f, this.mGPaint);
                    canvas.restore();
                }
                if (!(!isSupportProgress() || this.mProgressBitmap == null || this.mProgressBitmap.isRecycled())) {
                    calculateProgress(getProgress());
                    canvas.save(2);
                    canvas.clipPath(this.mClipProgressPath);
                    canvas.drawBitmap(this.mProgressBitmap, 0.0f, 0.0f, this.mGPaint);
                    canvas.restore();
                }
                if (mIcon != null) {
                    this.mLeftIcon = (this.mWidth - mIcon.getIntrinsicWidth()) / 2;
                    mIcon.setBounds(this.mLeftIcon, this.mTopIcon, this.mLeftIcon + mIcon.getIntrinsicWidth(), this.mTopIcon + mIcon.getIntrinsicHeight());
                    this.mTopText = (this.mTopIcon + mIcon.getIntrinsicHeight()) + this.mPaddingToDrawable;
                    mIcon.draw(canvas);
                } else {
                    this.mTopText = this.mHeight / 2;
                }
                if (text != null) {
                    drawText(canvas, text);
                    break;
                }
                break;
        }
        canvas.restore();
    }

    protected void drawText(Canvas canvas, String text) {
        this.mFonts.drawText(canvas, text, this.mLeftText, this.mTopText, this.mGPaint);
    }

    private void calculateProgress(float percent) {
        float progress = Math.max(0.0f, Math.min(1.0f, percent));
        this.mClipProgressPath.rewind();
        this.mClipProgressPath.addArc(this.mBound, this.mStartDegree, this.mMaxDegree * progress);
        this.mClipProgressPath.lineTo((float) (this.mWidth / 2), (float) (this.mHeight / 2));
        this.mClipProgressPath.close();
    }

    protected boolean isWithUnit() {
        return false;
    }
}
