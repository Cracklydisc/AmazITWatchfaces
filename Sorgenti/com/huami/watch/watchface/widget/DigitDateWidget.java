package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.CalendarUtil;
import com.huami.watch.watchface.util.Util;

public class DigitDateWidget extends AbsWatchFaceDataWidget {
    private Bitmap[] WEEKS;
    private CalendarUtil calendarUtil;
    private String date;
    private int day;
    private int leftDate;
    private Paint mBitmapPaint;
    private ImageFont mFonts;
    private int mHeight;
    private int mModel;
    private int mWidth;
    private int month;
    private int topDate;
    private int week;
    private int f29x;
    private int f30y;
    private int year;

    public DigitDateWidget(Resources resources, int x, int y, int model) {
        this(resources, x, y, 0, 0, model, -1, null);
    }

    public DigitDateWidget(Resources resources, int x, int y, int width, int height, int model, int rsidPath, ImageFont fonts) {
        this.mModel = 0;
        this.year = 2015;
        this.month = 12;
        this.day = 12;
        this.week = 7;
        this.date = "12-12";
        this.WEEKS = new Bitmap[7];
        this.f29x = x;
        this.f30y = y;
        this.mModel = model;
        if (fonts != null) {
            this.mFonts = fonts;
        } else {
            this.mFonts = new ImageFont("default");
        }
        this.mBitmapPaint = new Paint(7);
        String weekPath;
        int i;
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                if (this.mWidth <= 0) {
                    this.mWidth = 120;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 21;
                }
                this.topDate = 1;
                if (this.mFonts.size() == 0) {
                    this.mFonts.addChar('0', Util.decodeImage(resources, "datawidget/font/02/default_0.png"));
                    this.mFonts.addChar('1', Util.decodeImage(resources, "datawidget/font/02/default_1.png"));
                    this.mFonts.addChar('2', Util.decodeImage(resources, "datawidget/font/02/default_2.png"));
                    this.mFonts.addChar('3', Util.decodeImage(resources, "datawidget/font/02/default_3.png"));
                    this.mFonts.addChar('4', Util.decodeImage(resources, "datawidget/font/02/default_4.png"));
                    this.mFonts.addChar('5', Util.decodeImage(resources, "datawidget/font/02/default_5.png"));
                    this.mFonts.addChar('6', Util.decodeImage(resources, "datawidget/font/02/default_6.png"));
                    this.mFonts.addChar('7', Util.decodeImage(resources, "datawidget/font/02/default_7.png"));
                    this.mFonts.addChar('8', Util.decodeImage(resources, "datawidget/font/02/default_8.png"));
                    this.mFonts.addChar('9', Util.decodeImage(resources, "datawidget/font/02/default_9.png"));
                    this.mFonts.addChar('-', Util.decodeImage(resources, "datawidget/font/02/minus.png"));
                    return;
                }
                return;
            case 2:
                if (this.mWidth <= 0) {
                    this.mWidth = 120;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 21;
                }
                this.topDate = 1;
                if (rsidPath > 0) {
                    weekPath = resources.getString(rsidPath);
                } else {
                    weekPath = resources.getString(R.string.custom_data_widget_date_2);
                }
                for (i = 0; i < 7; i++) {
                    this.WEEKS[i] = Util.decodeImage(resources, String.format(weekPath, new Object[]{Integer.valueOf(i)}));
                }
                return;
            case 3:
                this.calendarUtil = new CalendarUtil(resources);
                if (this.mWidth <= 0) {
                    this.mWidth = 120;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 21;
                }
                this.topDate = 1;
                if (this.mFonts.size() == 0) {
                    this.mFonts.addChar('一', Util.decodeImage(resources, "datawidget/font/lunar/u4e00.png"));
                    this.mFonts.addChar('九', Util.decodeImage(resources, "datawidget/font/lunar/u4e5d.png"));
                    this.mFonts.addChar('八', Util.decodeImage(resources, "datawidget/font/lunar/u516b.png"));
                    this.mFonts.addChar('初', Util.decodeImage(resources, "datawidget/font/lunar/u521d.png"));
                    this.mFonts.addChar('四', Util.decodeImage(resources, "datawidget/font/lunar/u56db.png"));
                    this.mFonts.addChar('夕', Util.decodeImage(resources, "datawidget/font/lunar/u5915.png"));
                    this.mFonts.addChar('年', Util.decodeImage(resources, "datawidget/font/lunar/u5e74.png"));
                    this.mFonts.addChar('日', Util.decodeImage(resources, "datawidget/font/lunar/u65e5.png"));
                    this.mFonts.addChar('春', Util.decodeImage(resources, "datawidget/font/lunar/u6625.png"));
                    this.mFonts.addChar('正', Util.decodeImage(resources, "datawidget/font/lunar/u6b63.png"));
                    this.mFonts.addChar('白', Util.decodeImage(resources, "datawidget/font/lunar/u767d.png"));
                    this.mFonts.addChar('腊', Util.decodeImage(resources, "datawidget/font/lunar/u814a.png"));
                    this.mFonts.addChar('谷', Util.decodeImage(resources, "datawidget/font/lunar/u8c37.png"));
                    this.mFonts.addChar('降', Util.decodeImage(resources, "datawidget/font/lunar/u964d.png"));
                    this.mFonts.addChar('霜', Util.decodeImage(resources, "datawidget/font/lunar/u971c.png"));
                    this.mFonts.addChar('七', Util.decodeImage(resources, "datawidget/font/lunar/u4e03.png"));
                    this.mFonts.addChar('二', Util.decodeImage(resources, "datawidget/font/lunar/u4e8c.png"));
                    this.mFonts.addChar('六', Util.decodeImage(resources, "datawidget/font/lunar/u516d.png"));
                    this.mFonts.addChar('十', Util.decodeImage(resources, "datawidget/font/lunar/u5341.png"));
                    this.mFonts.addChar('国', Util.decodeImage(resources, "datawidget/font/lunar/u56fd.png"));
                    this.mFonts.addChar('大', Util.decodeImage(resources, "datawidget/font/lunar/u5927.png"));
                    this.mFonts.addChar('庆', Util.decodeImage(resources, "datawidget/font/lunar/u5e86.png"));
                    this.mFonts.addChar('旦', Util.decodeImage(resources, "datawidget/font/lunar/u65e6.png"));
                    this.mFonts.addChar('暑', Util.decodeImage(resources, "datawidget/font/lunar/u6691.png"));
                    this.mFonts.addChar('水', Util.decodeImage(resources, "datawidget/font/lunar/u6c34.png"));
                    this.mFonts.addChar('秋', Util.decodeImage(resources, "datawidget/font/lunar/u79cb.png"));
                    this.mFonts.addChar('至', Util.decodeImage(resources, "datawidget/font/lunar/u81f3.png"));
                    this.mFonts.addChar('重', Util.decodeImage(resources, "datawidget/font/lunar/u91cd.png"));
                    this.mFonts.addChar('除', Util.decodeImage(resources, "datawidget/font/lunar/u9664.png"));
                    this.mFonts.addChar('露', Util.decodeImage(resources, "datawidget/font/lunar/u9732.png"));
                    this.mFonts.addChar('三', Util.decodeImage(resources, "datawidget/font/lunar/u4e09.png"));
                    this.mFonts.addChar('五', Util.decodeImage(resources, "datawidget/font/lunar/u4e94.png"));
                    this.mFonts.addChar('冬', Util.decodeImage(resources, "datawidget/font/lunar/u51ac.png"));
                    this.mFonts.addChar('卅', Util.decodeImage(resources, "datawidget/font/lunar/u5345.png"));
                    this.mFonts.addChar('处', Util.decodeImage(resources, "datawidget/font/lunar/u5904.png"));
                    this.mFonts.addChar('寒', Util.decodeImage(resources, "datawidget/font/lunar/u5bd2.png"));
                    this.mFonts.addChar('廿', Util.decodeImage(resources, "datawidget/font/lunar/u5eff.png"));
                    this.mFonts.addChar('明', Util.decodeImage(resources, "datawidget/font/lunar/u660e.png"));
                    this.mFonts.addChar('月', Util.decodeImage(resources, "datawidget/font/lunar/u6708.png"));
                    this.mFonts.addChar('清', Util.decodeImage(resources, "datawidget/font/lunar/u6e05.png"));
                    this.mFonts.addChar('立', Util.decodeImage(resources, "datawidget/font/lunar/u7acb.png"));
                    this.mFonts.addChar('节', Util.decodeImage(resources, "datawidget/font/lunar/u8282.png"));
                    this.mFonts.addChar('闰', Util.decodeImage(resources, "datawidget/font/lunar/u95f0.png"));
                    this.mFonts.addChar('雨', Util.decodeImage(resources, "datawidget/font/lunar/u96e8.png"));
                    this.mFonts.addChar('中', Util.decodeImage(resources, "datawidget/font/lunar/u4e2d.png"));
                    this.mFonts.addChar('元', Util.decodeImage(resources, "datawidget/font/lunar/u5143.png"));
                    this.mFonts.addChar('分', Util.decodeImage(resources, "datawidget/font/lunar/u5206.png"));
                    this.mFonts.addChar('午', Util.decodeImage(resources, "datawidget/font/lunar/u5348.png"));
                    this.mFonts.addChar('夏', Util.decodeImage(resources, "datawidget/font/lunar/u590f.png"));
                    this.mFonts.addChar('小', Util.decodeImage(resources, "datawidget/font/lunar/u5c0f.png"));
                    this.mFonts.addChar('惊', Util.decodeImage(resources, "datawidget/font/lunar/u60ca.png"));
                    this.mFonts.addChar('星', Util.decodeImage(resources, "datawidget/font/lunar/u661f.png"));
                    this.mFonts.addChar('期', Util.decodeImage(resources, "datawidget/font/lunar/u671f.png"));
                    this.mFonts.addChar('满', Util.decodeImage(resources, "datawidget/font/lunar/u6ee1.png"));
                    this.mFonts.addChar('端', Util.decodeImage(resources, "datawidget/font/lunar/u7aef.png"));
                    this.mFonts.addChar('蛰', Util.decodeImage(resources, "datawidget/font/lunar/u86f0.png"));
                    this.mFonts.addChar('阳', Util.decodeImage(resources, "datawidget/font/lunar/u9633.png"));
                    this.mFonts.addChar('雪', Util.decodeImage(resources, "datawidget/font/lunar/u96ea.png"));
                    this.mFonts.addChar('种', Util.decodeImage(resources, "datawidget/font/lunar/u79cd.png"));
                    this.mFonts.addChar('芒', Util.decodeImage(resources, "datawidget/font/lunar/u8292.png"));
                    return;
                }
                return;
            case 4:
                if (this.mWidth <= 0) {
                    this.mWidth = 120;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 21;
                }
                this.topDate = 0;
                if (this.mFonts.size() == 0) {
                    for (i = 0; i < 10; i++) {
                        this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/font/17_1/%d.png", new Object[]{Integer.valueOf(i)})));
                    }
                    this.mFonts.addChar('.', Util.decodeImage(resources, "datawidget/font/17_1/point.png"));
                    return;
                }
                return;
            case 5:
                if (this.mWidth <= 0) {
                    this.mWidth = 32;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 32;
                }
                this.topDate = 7;
                if (this.mFonts.size() == 0) {
                    for (i = 0; i < 10; i++) {
                        this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/font/data5/%d.png", new Object[]{Integer.valueOf(i)})));
                    }
                    return;
                }
                return;
            case 6:
                if (this.mWidth <= 0) {
                    this.mWidth = 160;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 30;
                }
                this.topDate = 0;
                this.mBitmapPaint.setTextAlign(Align.RIGHT);
                if (this.mFonts.size() == 0) {
                    for (i = 0; i < 10; i++) {
                        this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/model_3/font/%d.png", new Object[]{Integer.valueOf(i)})));
                    }
                    this.mFonts.addChar('.', Util.decodeImage(resources, "datawidget/model_3/font/dot.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(resources, "datawidget/model_3/font/space.png"));
                }
                for (i = 0; i < 7; i++) {
                    this.WEEKS[i] = Util.decodeImage(resources, String.format("datawidget/model_3/date/en/week_%d.png", new Object[]{Integer.valueOf(i)}));
                }
                return;
            case 7:
                if (this.mWidth <= 0) {
                    this.mWidth = 160;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 30;
                }
                this.topDate = 0;
                if (this.mFonts.size() == 0) {
                    for (i = 0; i < 10; i++) {
                        this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/model_3/font/%d.png", new Object[]{Integer.valueOf(i)})));
                    }
                    this.mFonts.addChar('.', Util.decodeImage(resources, "datawidget/model_3/font/dot.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(resources, "datawidget/model_3/font/space.png"));
                    this.mFonts.addChar('一', Util.decodeImage(resources, "datawidget/model_3/date/u4e00.png"));
                    this.mFonts.addChar('二', Util.decodeImage(resources, "datawidget/model_3/date/u4e8c.png"));
                    this.mFonts.addChar('三', Util.decodeImage(resources, "datawidget/model_3/date/u4e09.png"));
                    this.mFonts.addChar('四', Util.decodeImage(resources, "datawidget/model_3/date/u56db.png"));
                    this.mFonts.addChar('五', Util.decodeImage(resources, "datawidget/model_3/date/u4e94.png"));
                    this.mFonts.addChar('六', Util.decodeImage(resources, "datawidget/model_3/date/u516d.png"));
                    this.mFonts.addChar('日', Util.decodeImage(resources, "datawidget/model_3/date/u65e5.png"));
                    this.mFonts.addChar('星', Util.decodeImage(resources, "datawidget/model_3/date/u661f.png"));
                    this.mFonts.addChar('期', Util.decodeImage(resources, "datawidget/model_3/date/u671f.png"));
                    return;
                }
                return;
            case 8:
                if (this.mWidth <= 0) {
                    this.mWidth = 180;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 18;
                }
                this.topDate = 0;
                if (this.mFonts.size() == 0) {
                    for (i = 0; i < 10; i++) {
                        this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/model_4/font/%d.png", new Object[]{Integer.valueOf(i)})));
                    }
                    this.mFonts.addChar('.', Util.decodeImage(resources, "datawidget/model_4/font/dot.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(resources, "datawidget/model_4/font/space.png"));
                    this.mFonts.addChar('一', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e00.png"));
                    this.mFonts.addChar('二', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e8c.png"));
                    this.mFonts.addChar('三', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e09.png"));
                    this.mFonts.addChar('四', Util.decodeImage(resources, "datawidget/model_4/week/cn/u56db.png"));
                    this.mFonts.addChar('五', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e94.png"));
                    this.mFonts.addChar('六', Util.decodeImage(resources, "datawidget/model_4/week/cn/u516d.png"));
                    this.mFonts.addChar('日', Util.decodeImage(resources, "datawidget/model_4/week/cn/u65e5.png"));
                    this.mFonts.addChar('星', Util.decodeImage(resources, "datawidget/model_4/week/cn/u661f.png"));
                    this.mFonts.addChar('期', Util.decodeImage(resources, "datawidget/model_4/week/cn/u671f.png"));
                    return;
                }
                return;
            case 9:
                if (this.mWidth <= 0) {
                    this.mWidth = 180;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 18;
                }
                this.topDate = 0;
                if (this.mFonts.size() == 0) {
                    for (i = 0; i < 10; i++) {
                        this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/model_4/font/%d.png", new Object[]{Integer.valueOf(i)})));
                    }
                    this.mFonts.addChar('.', Util.decodeImage(resources, "datawidget/model_4/font/dot.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(resources, "datawidget/model_4/font/space.png"));
                    return;
                }
                return;
            case 10:
                this.calendarUtil = new CalendarUtil(resources);
                if (this.mWidth <= 0) {
                    this.mWidth = 180;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 18;
                }
                this.topDate = 0;
                if (this.mFonts.size() == 0) {
                    this.mFonts.addChar('一', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e00.png"));
                    this.mFonts.addChar('九', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e5d.png"));
                    this.mFonts.addChar('八', Util.decodeImage(resources, "datawidget/model_4/week/cn/u516b.png"));
                    this.mFonts.addChar('初', Util.decodeImage(resources, "datawidget/model_4/week/cn/u521d.png"));
                    this.mFonts.addChar('四', Util.decodeImage(resources, "datawidget/model_4/week/cn/u56db.png"));
                    this.mFonts.addChar('夕', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5915.png"));
                    this.mFonts.addChar('年', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5e74.png"));
                    this.mFonts.addChar('日', Util.decodeImage(resources, "datawidget/model_4/week/cn/u65e5.png"));
                    this.mFonts.addChar('春', Util.decodeImage(resources, "datawidget/model_4/week/cn/u6625.png"));
                    this.mFonts.addChar('正', Util.decodeImage(resources, "datawidget/model_4/week/cn/u6b63.png"));
                    this.mFonts.addChar('白', Util.decodeImage(resources, "datawidget/model_4/week/cn/u767d.png"));
                    this.mFonts.addChar('腊', Util.decodeImage(resources, "datawidget/model_4/week/cn/u814a.png"));
                    this.mFonts.addChar('谷', Util.decodeImage(resources, "datawidget/model_4/week/cn/u8c37.png"));
                    this.mFonts.addChar('降', Util.decodeImage(resources, "datawidget/model_4/week/cn/u964d.png"));
                    this.mFonts.addChar('霜', Util.decodeImage(resources, "datawidget/model_4/week/cn/u971c.png"));
                    this.mFonts.addChar('七', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e03.png"));
                    this.mFonts.addChar('二', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e8c.png"));
                    this.mFonts.addChar('六', Util.decodeImage(resources, "datawidget/model_4/week/cn/u516d.png"));
                    this.mFonts.addChar('十', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5341.png"));
                    this.mFonts.addChar('国', Util.decodeImage(resources, "datawidget/model_4/week/cn/u56fd.png"));
                    this.mFonts.addChar('大', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5927.png"));
                    this.mFonts.addChar('庆', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5e86.png"));
                    this.mFonts.addChar('旦', Util.decodeImage(resources, "datawidget/model_4/week/cn/u65e6.png"));
                    this.mFonts.addChar('暑', Util.decodeImage(resources, "datawidget/model_4/week/cn/u6691.png"));
                    this.mFonts.addChar('水', Util.decodeImage(resources, "datawidget/model_4/week/cn/u6c34.png"));
                    this.mFonts.addChar('秋', Util.decodeImage(resources, "datawidget/model_4/week/cn/u79cb.png"));
                    this.mFonts.addChar('至', Util.decodeImage(resources, "datawidget/model_4/week/cn/u81f3.png"));
                    this.mFonts.addChar('重', Util.decodeImage(resources, "datawidget/model_4/week/cn/u91cd.png"));
                    this.mFonts.addChar('除', Util.decodeImage(resources, "datawidget/model_4/week/cn/u9664.png"));
                    this.mFonts.addChar('露', Util.decodeImage(resources, "datawidget/model_4/week/cn/u9732.png"));
                    this.mFonts.addChar('三', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e09.png"));
                    this.mFonts.addChar('五', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e94.png"));
                    this.mFonts.addChar('冬', Util.decodeImage(resources, "datawidget/model_4/week/cn/u51ac.png"));
                    this.mFonts.addChar('卅', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5345.png"));
                    this.mFonts.addChar('处', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5904.png"));
                    this.mFonts.addChar('寒', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5bd2.png"));
                    this.mFonts.addChar('廿', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5eff.png"));
                    this.mFonts.addChar('明', Util.decodeImage(resources, "datawidget/model_4/week/cn/u660e.png"));
                    this.mFonts.addChar('月', Util.decodeImage(resources, "datawidget/model_4/week/cn/u6708.png"));
                    this.mFonts.addChar('清', Util.decodeImage(resources, "datawidget/model_4/week/cn/u6e05.png"));
                    this.mFonts.addChar('立', Util.decodeImage(resources, "datawidget/model_4/week/cn/u7acb.png"));
                    this.mFonts.addChar('节', Util.decodeImage(resources, "datawidget/model_4/week/cn/u8282.png"));
                    this.mFonts.addChar('闰', Util.decodeImage(resources, "datawidget/model_4/week/cn/u95f0.png"));
                    this.mFonts.addChar('雨', Util.decodeImage(resources, "datawidget/model_4/week/cn/u96e8.png"));
                    this.mFonts.addChar('中', Util.decodeImage(resources, "datawidget/model_4/week/cn/u4e2d.png"));
                    this.mFonts.addChar('元', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5143.png"));
                    this.mFonts.addChar('分', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5206.png"));
                    this.mFonts.addChar('午', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5348.png"));
                    this.mFonts.addChar('夏', Util.decodeImage(resources, "datawidget/model_4/week/cn/u590f.png"));
                    this.mFonts.addChar('小', Util.decodeImage(resources, "datawidget/model_4/week/cn/u5c0f.png"));
                    this.mFonts.addChar('惊', Util.decodeImage(resources, "datawidget/model_4/week/cn/u60ca.png"));
                    this.mFonts.addChar('星', Util.decodeImage(resources, "datawidget/model_4/week/cn/u661f.png"));
                    this.mFonts.addChar('期', Util.decodeImage(resources, "datawidget/model_4/week/cn/u671f.png"));
                    this.mFonts.addChar('满', Util.decodeImage(resources, "datawidget/model_4/week/cn/u6ee1.png"));
                    this.mFonts.addChar('端', Util.decodeImage(resources, "datawidget/model_4/week/cn/u7aef.png"));
                    this.mFonts.addChar('蛰', Util.decodeImage(resources, "datawidget/model_4/week/cn/u86f0.png"));
                    this.mFonts.addChar('阳', Util.decodeImage(resources, "datawidget/model_4/week/cn/u9633.png"));
                    this.mFonts.addChar('雪', Util.decodeImage(resources, "datawidget/model_4/week/cn/u96ea.png"));
                    this.mFonts.addChar('种', Util.decodeImage(resources, "datawidget/model_4/week/cn/u79cd.png"));
                    this.mFonts.addChar('芒', Util.decodeImage(resources, "datawidget/model_4/week/cn/u8292.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(resources, "datawidget/model_4/font/space.png"));
                    return;
                }
                return;
            case 11:
                if (this.mWidth <= 0) {
                    this.mWidth = 180;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 18;
                }
                this.topDate = 0;
                this.mBitmapPaint.setTextAlign(Align.RIGHT);
                if (this.mFonts.size() == 0) {
                    for (i = 0; i < 10; i++) {
                        this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/model_4/font/%d.png", new Object[]{Integer.valueOf(i)})));
                    }
                    this.mFonts.addChar('.', Util.decodeImage(resources, "datawidget/model_4/font/dot.png"));
                    this.mFonts.addChar(' ', Util.decodeImage(resources, "datawidget/model_4/font/space.png"));
                }
                for (i = 0; i < 7; i++) {
                    this.WEEKS[i] = Util.decodeImage(resources, String.format("datawidget/model_4/week/w%d.png", new Object[]{Integer.valueOf(i)}));
                }
                return;
            default:
                if (this.mWidth <= 0) {
                    this.mWidth = 120;
                }
                if (this.mHeight <= 0) {
                    this.mHeight = 21;
                }
                this.topDate = 0;
                if (this.mFonts.size() == 0) {
                    for (i = 0; i < 10; i++) {
                        this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/android/date_%d.png", new Object[]{Integer.valueOf(i)})));
                    }
                    this.mFonts.addChar('-', Util.decodeImage(resources, "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/android/minus.png"));
                }
                weekPath = resources.getString(R.string.analog_thirteen_week_path);
                for (i = 0; i < 7; i++) {
                    this.WEEKS[i] = Util.decodeImage(resources, String.format(weekPath, new Object[]{Integer.valueOf(i)}));
                }
                return;
        }
    }

    public int getDataType() {
        return 6;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 6 && values != null && values.length >= 4) {
            this.year = ((Integer) values[0]).intValue();
            this.day = ((Integer) values[1]).intValue();
            this.month = ((Integer) values[2]).intValue();
            this.week = ((Integer) values[3]).intValue();
            switch (this.mModel) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    this.date = Util.formatTime(this.month) + "-" + Util.formatTime(this.day);
                    this.leftDate = (this.mWidth - this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                case 2:
                    this.leftDate = (this.mWidth - this.WEEKS[this.week - 1].getWidth()) / 2;
                    return;
                case 3:
                    this.date = this.calendarUtil.getHolidayString();
                    this.leftDate = (this.mWidth - this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                case 4:
                    this.date = this.year + "." + Util.formatTime(this.month) + "." + Util.formatTime(this.day);
                    this.leftDate = (this.mWidth - this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                case 5:
                    this.date = Util.formatTime(this.day);
                    this.leftDate = (this.mWidth - this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                case 6:
                    this.date = Util.formatDate("MM.dd ", this.year, this.month, this.day).toString();
                    this.leftDate = ((this.mWidth - this.WEEKS[this.week - 1].getWidth()) + this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                case 7:
                    this.date = new StringBuffer(Util.formatDate("MM.dd 星期", this.year, this.month, this.day)).append(Util.getWeekNumberZh(this.week - 1)).toString();
                    this.leftDate = (this.mWidth - this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                case 8:
                    this.date = new StringBuffer(Util.formatDate("MM.dd.yyyy 星期", this.year, this.month, this.day)).append(Util.getWeekNumberZh(this.week - 1)).toString();
                    this.leftDate = (this.mWidth - this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                case 9:
                    this.date = Util.formatDate("MM.dd.yyyy ", this.year, this.month, this.day).toString();
                    this.leftDate = (this.mWidth - this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                case 10:
                    this.date = this.calendarUtil.getHolidayString();
                    this.date = new StringBuffer(this.date).append(" 星期").append(Util.getWeekNumberZh(this.week - 1)).toString();
                    this.leftDate = (this.mWidth - this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                case 11:
                    this.date = Util.formatDate("MM.dd.yyyy ", this.year, this.month, this.day).toString();
                    this.leftDate = ((this.mWidth - this.WEEKS[this.week - 1].getWidth()) + this.mFonts.getFontWidth(this.date)) / 2;
                    return;
                default:
                    this.date = Util.formatTime(this.month) + "-" + Util.formatTime(this.day);
                    this.leftDate = ((this.mWidth - 48) - this.WEEKS[this.week - 1].getWidth()) / 2;
                    return;
            }
        }
    }

    public int getX() {
        return this.f29x;
    }

    public int getY() {
        return this.f30y;
    }

    public void onDraw(Canvas canvas) {
        switch (this.mModel) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
            case 3:
            case 4:
            case 5:
            case 7:
            case 8:
            case 9:
            case 10:
                break;
            case 2:
                canvas.drawBitmap(this.WEEKS[this.week - 1], (float) this.leftDate, (float) this.topDate, this.mBitmapPaint);
                return;
            case 6:
            case 11:
                this.mFonts.drawText(canvas, this.date, this.leftDate, this.topDate, this.mBitmapPaint);
                canvas.drawBitmap(this.WEEKS[this.week - 1], (float) this.leftDate, (float) this.topDate, this.mBitmapPaint);
                return;
            default:
                canvas.drawBitmap(this.WEEKS[this.week - 1], (float) (this.leftDate + 48), (float) this.topDate, this.mBitmapPaint);
                break;
        }
        this.mFonts.drawText(canvas, this.date, this.leftDate, this.topDate, this.mBitmapPaint);
    }
}
