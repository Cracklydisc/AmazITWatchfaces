package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.huami.watch.watchface.util.Util;

public class EverestDateWidget extends AbsWatchFaceDataWidget {
    private Bitmap[] WEEKS = new Bitmap[7];
    private String date = "12.12";
    private int day = 12;
    private Paint mBitmapPaint;
    private ImageFont mFonts;
    private int mHeight = 11;
    private int mWidth = 75;
    private int month = 12;
    private int week = 7;
    private int f31x;
    private int f32y;

    public EverestDateWidget(Resources resources, int x, int y) {
        int i;
        this.f31x = x;
        this.f32y = y;
        this.mFonts = new ImageFont("everestDate");
        this.mBitmapPaint = new Paint(7);
        for (i = 0; i < 10; i++) {
            this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/date_everest/%d.png", new Object[]{Integer.valueOf(i)})));
        }
        this.mFonts.addChar('.', Util.decodeImage(resources, "datawidget/date_everest/point.png"));
        for (i = 0; i < 7; i++) {
            this.WEEKS[i] = Util.decodeImage(resources, String.format("datawidget/date_everest/w%d.png", new Object[]{Integer.valueOf(i)}));
        }
    }

    public int getDataType() {
        return 6;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 6 && values != null && values.length >= 4) {
            this.month = ((Integer) values[1]).intValue();
            this.day = ((Integer) values[2]).intValue();
            this.week = ((Integer) values[3]).intValue();
            this.date = Util.formatTime(this.month) + "." + Util.formatTime(this.day);
        }
    }

    public int getX() {
        return this.f31x;
    }

    public int getY() {
        return this.f32y;
    }

    public void onDraw(Canvas canvas) {
        this.mFonts.drawText(canvas, this.date, 0, 0, this.mBitmapPaint);
        canvas.drawBitmap(this.WEEKS[this.week - 1], 42.0f, 0.0f, this.mBitmapPaint);
    }
}
