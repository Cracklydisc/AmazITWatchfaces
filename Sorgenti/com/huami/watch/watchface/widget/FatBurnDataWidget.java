package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.util.Util;

public class FatBurnDataWidget extends DefaultWatchFaceDataWidget {
    private Drawable icon;
    private String mCalories;

    public FatBurnDataWidget(Resources resources, int x, int y, int model) {
        this(resources, x, y, model, null);
    }

    public FatBurnDataWidget(Resources resources, int x, int y, int model, ImageFont fonts) {
        super(resources, x, y, 4, model, fonts);
        this.mCalories = "--";
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_calories_1.png");
                return;
            case 2:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/icon_android/watchface_custom_fun_icon_calories_2.png");
                return;
            case 3:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_3/icon/watchface_custom_fun_icon_calories.png");
                return;
            case 4:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_4/icon/watchface_custom_fun_icon_slpt_calories.png");
                return;
            default:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_calories.png");
                return;
        }
    }

    protected boolean isSupportProgress() {
        return false;
    }

    protected float getProgress() {
        return 0.7f;
    }

    protected Drawable getIconDrawable(Resources resources) {
        return this.icon;
    }

    protected String getText() {
        return this.mCalories;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (4 == dataType && values != null && values[0] != null) {
            float calories = ((Float) values[0]).floatValue();
            if (isWithUnit()) {
                this.mCalories = String.valueOf((int) calories) + "KCAL";
            } else {
                this.mCalories = String.valueOf((int) calories);
            }
        }
    }
}
