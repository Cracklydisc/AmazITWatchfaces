package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.analogyellow.R;

public class FatBurnTextWidget extends AbsTextWidget {
    private String mText = "--";
    private Drawable mUnit;

    public FatBurnTextWidget(Resources resources, int x, int y, ImageFont fonts) {
        super(x, y, fonts);
        this.mUnit = resources.getDrawable(R.drawable.unit_kcal, null);
        this.mUnit.setBounds(0, 0, this.mUnit.getIntrinsicWidth(), this.mUnit.getIntrinsicHeight());
    }

    protected String getText() {
        return this.mText;
    }

    public int getDataType() {
        return 4;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (4 == dataType && values != null && values[0] != null) {
            this.mText = String.valueOf((int) ((Float) values[0]).floatValue());
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save(1);
        canvas.translate((float) (getTextWidth() + 4), 12.0f);
        this.mUnit.draw(canvas);
        canvas.restore();
    }
}
