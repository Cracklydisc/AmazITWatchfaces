package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.util.Util;

public class HeartRateDateWidget extends DefaultWatchFaceDataWidget {
    private Drawable icon;
    private String mHeartRate;

    public HeartRateDateWidget(Resources resources, int x, int y, int model) {
        this(resources, x, y, model, null);
    }

    public HeartRateDateWidget(Resources resources, int x, int y, int model, ImageFont fonts) {
        super(resources, x, y, 5, model, fonts);
        this.mHeartRate = "--";
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_heart_rate_1.png");
                return;
            case 2:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/icon_android/watchface_custom_fun_icon_heart_rate_2.png");
                return;
            case 3:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_3/icon/watchface_custom_fun_icon_heart_rate.png");
                return;
            case 4:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_4/icon/watchface_custom_fun_icon_heart_slpt_rate.png");
                return;
            default:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_heart_rate.png");
                return;
        }
    }

    protected boolean isSupportProgress() {
        return false;
    }

    protected float getProgress() {
        return 0.0f;
    }

    protected Drawable getIconDrawable(Resources resources) {
        return this.icon;
    }

    protected String getText() {
        return this.mHeartRate;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 5 && values[0] != null) {
            int heartrate = ((Integer) values[0]).intValue();
            if (heartrate > 0) {
                this.mHeartRate = String.valueOf(heartrate);
            } else {
                this.mHeartRate = "--";
            }
            if (isWithUnit()) {
                this.mHeartRate += "BPM";
            }
        }
    }
}
