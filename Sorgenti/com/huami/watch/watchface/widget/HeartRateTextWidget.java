package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import com.huami.watch.watchface.analogyellow.R;

public class HeartRateTextWidget extends AbsTextWidget {
    private String mText = "--";
    private Drawable mUnit;

    public HeartRateTextWidget(Resources resources, int x, int y, ImageFont fonts) {
        super(x, y, fonts);
        this.mUnit = resources.getDrawable(R.drawable.unit_bpm, null);
        this.mUnit.setBounds(0, 0, this.mUnit.getIntrinsicWidth(), this.mUnit.getIntrinsicHeight());
    }

    protected String getText() {
        return this.mText;
    }

    public int getDataType() {
        return 5;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (5 == dataType && values[0] != null) {
            int heartrate = ((Integer) values[0]).intValue();
            if (heartrate > 0) {
                this.mText = String.valueOf(heartrate);
            } else {
                this.mText = "--";
            }
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save(1);
        canvas.translate((float) (getTextWidth() + 4), 12.0f);
        this.mUnit.draw(canvas);
        canvas.restore();
    }
}
