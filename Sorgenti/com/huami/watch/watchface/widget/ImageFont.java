package com.huami.watch.watchface.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.support.v7.recyclerview.C0051R;
import android.util.SparseArray;

public class ImageFont {
    private SparseArray<Bitmap> mFontBitmap = new SparseArray();
    private String name;

    static /* synthetic */ class C03171 {
        static final /* synthetic */ int[] $SwitchMap$android$graphics$Paint$Align = new int[Align.values().length];

        static {
            try {
                $SwitchMap$android$graphics$Paint$Align[Align.RIGHT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$android$graphics$Paint$Align[Align.CENTER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$android$graphics$Paint$Align[Align.LEFT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public ImageFont(String name) {
        this.name = name;
    }

    public int size() {
        return this.mFontBitmap.size();
    }

    public void addChar(char c, Bitmap b) {
        this.mFontBitmap.put(c, b);
    }

    public Bitmap getChar(char c) {
        return (Bitmap) this.mFontBitmap.get(c);
    }

    public int getFontWidth(String text) {
        int width = 0;
        if (text != null) {
            for (int i = 0; i < text.length(); i++) {
                width += getCharWidth(text.charAt(i));
            }
        }
        return width;
    }

    public int getCharWidth(char c) {
        Bitmap b = (Bitmap) this.mFontBitmap.get(c);
        if (b != null) {
            return b.getWidth();
        }
        return 0;
    }

    public int getCharHeight(char c) {
        Bitmap b = (Bitmap) this.mFontBitmap.get(c);
        if (b != null) {
            return b.getHeight();
        }
        return 0;
    }

    public void drawText(Canvas canvas, String text, int x, int y, Paint p) {
        if (text != null) {
            int startX;
            switch (C03171.$SwitchMap$android$graphics$Paint$Align[p.getTextAlign().ordinal()]) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    startX = x - getFontWidth(text);
                    break;
                case 2:
                    startX = x - (getFontWidth(text) / 2);
                    break;
                default:
                    startX = x;
                    break;
            }
            for (int i = 0; i < text.length(); i++) {
                Bitmap b = getChar(text.charAt(i));
                if (!(b == null || b.isRecycled())) {
                    canvas.drawBitmap(b, (float) startX, (float) y, p);
                    startX += b.getWidth();
                }
            }
        }
    }

    public void destroy() {
        int nsize = this.mFontBitmap.size();
        for (int i = 0; i < nsize; i++) {
            Bitmap b = (Bitmap) this.mFontBitmap.valueAt(i);
            if (!(b == null || b.isRecycled())) {
                b.recycle();
            }
        }
        this.mFontBitmap.clear();
    }
}
