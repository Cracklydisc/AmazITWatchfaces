package com.huami.watch.watchface.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class LevelWidget extends AbsWatchFaceDataWidget {
    private Paint mGPaint;
    private Bitmap[] mLevels;
    private int mMaxProgress;
    private int mProgressLevel;

    public LevelWidget(Bitmap[] levels) {
        this.mLevels = levels;
        if (this.mLevels != null && this.mLevels.length > 0) {
            this.mMaxProgress = this.mLevels.length - 1;
        }
        this.mGPaint = new Paint(3);
        this.mGPaint.setStyle(Style.STROKE);
        this.mGPaint.setStrokeWidth(17.0f);
    }

    public int getDataType() {
        return 0;
    }

    public void onDataUpdate(int dataType, Object... values) {
        updateBatteryLevel(((Float) values[0]).floatValue());
    }

    private void updateBatteryLevel(float progress) {
        if (progress > 0.0f) {
            this.mProgressLevel = (int) (((float) this.mMaxProgress) * Math.min(progress, 1.0f));
            return;
        }
        this.mProgressLevel = 0;
    }

    public int getX() {
        return 0;
    }

    public int getY() {
        return 0;
    }

    public void onDraw(Canvas canvas) {
        if (this.mLevels != null && this.mLevels.length > 0) {
            Bitmap level = this.mLevels[this.mProgressLevel];
            if (level != null && !level.isRecycled()) {
                canvas.drawBitmap(level, 0.0f, 0.0f, this.mGPaint);
            }
        }
    }
}
