package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.util.Util;

public class MileageDateWidget extends DefaultWatchFaceDataWidget {
    private Drawable icon;
    private String mMileage;

    public MileageDateWidget(Resources resources, int x, int y, int model) {
        this(resources, x, y, model, null);
    }

    public MileageDateWidget(Resources resources, int x, int y, int model, ImageFont fonts) {
        super(resources, x, y, 3, model, fonts);
        this.mMileage = "0";
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_mileage_1.png");
                return;
            case 2:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/icon_android/watchface_custom_fun_icon_mileage_2.png");
                return;
            case 3:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_3/icon/watchface_custom_fun_icon_mileage.png");
                return;
            case 4:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_4/icon/watchface_custom_fun_icon_slpt_mileage.png");
                return;
            default:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_mileage.png");
                return;
        }
    }

    protected boolean isSupportProgress() {
        return false;
    }

    protected float getProgress() {
        return 0.8f;
    }

    protected Drawable getIconDrawable(Resources resources) {
        return this.icon;
    }

    protected String getText() {
        return this.mMileage;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (3 == dataType && values != null && values[0] != null) {
            double mileage = ((Double) values[0]).doubleValue();
            if (isWithUnit()) {
                switch (((Integer) values[1]).intValue()) {
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        this.mMileage = Util.getFormatDistance(mileage) + "MI";
                        return;
                    default:
                        this.mMileage = Util.getFormatDistance(mileage) + "KM";
                        return;
                }
            }
            this.mMileage = Util.getFormatDistance(mileage);
        }
    }
}
