package com.huami.watch.watchface.widget;

import android.content.res.Resources;

public class PointerBatteryWidget extends AbsPointerDataWidget {
    private float mProgress;

    public PointerBatteryWidget(Resources resources, int x, int y, int width, int height) {
        super(resources, x, y, width, height, 10);
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 10) {
            updateBatteryLevel(((Integer) values[0]).intValue(), ((Integer) values[1]).intValue());
        }
    }

    private void updateBatteryLevel(int batteryLevel, int batteryMax) {
        if (batteryMax > 0) {
            this.mProgress = Math.min((((float) batteryLevel) * 1.0f) / ((float) batteryMax), 1.0f);
        } else {
            this.mProgress = 0.0f;
        }
    }

    protected float getProgress() {
        return this.mProgress;
    }
}
