package com.huami.watch.watchface.widget;

import android.content.res.Resources;

public class PointerTodayDistanceWidget extends AbsPointerDataWidget {
    private float mProgress;

    public PointerTodayDistanceWidget(Resources resources, int x, int y, int width, int height) {
        super(resources, x, y, width, height, 2);
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 2) {
            updateSportTodayDistance(((Double) values[0]).doubleValue());
        }
    }

    private void updateSportTodayDistance(double distance) {
        if (distance > 0.0d) {
            this.mProgress = (float) Math.min(distance / 3.0d, 1.0d);
        } else {
            this.mProgress = 0.0f;
        }
    }

    protected float getProgress() {
        return this.mProgress;
    }
}
