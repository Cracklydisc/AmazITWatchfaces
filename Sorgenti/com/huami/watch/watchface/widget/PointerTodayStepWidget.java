package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import com.huami.watch.watchface.util.Util;

public class PointerTodayStepWidget extends AbsPointerDataWidget {
    private Paint mBitmapPaint = new Paint(7);
    private ImageFont mFonts;
    private Kedu[] mKedus = new Kedu[3];
    private float mProgress;
    private int mTotalStepTarget = 0;

    private class Kedu {
        Align align;
        String text;
        int f33x;
        int f34y;

        public Kedu(int x, int y, Align align) {
            this.f33x = x;
            this.f34y = y;
            this.align = align;
        }
    }

    public PointerTodayStepWidget(Resources resources, int x, int y, int width, int height) {
        super(resources, x, y, width, height, 1);
        this.mKedus[0] = new Kedu(94, 50, Align.RIGHT);
        this.mKedus[1] = new Kedu(54, 87, Align.CENTER);
        this.mKedus[2] = new Kedu(14, 50, Align.LEFT);
        this.mFonts = new ImageFont("everestStepKedu");
        for (int i = 0; i < 10; i++) {
            this.mFonts.addChar(Character.forDigit(i, 10), Util.decodeImage(resources, String.format("datawidget/step_everest/%d.png", new Object[]{Integer.valueOf(i)})));
        }
        this.mFonts.addChar('.', Util.decodeImage(resources, "datawidget/step_everest/point.png"));
        this.mFonts.addChar('k', Util.decodeImage(resources, "datawidget/step_everest/k.png"));
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 1) {
            updateStepProgress(((Integer) values[0]).intValue(), ((Integer) values[1]).intValue());
        }
    }

    private void updateStepProgress(int curStepCount, int totalStepTarget) {
        if (totalStepTarget > 0) {
            updateTotalStep(totalStepTarget);
        } else {
            updateTotalStep(8000);
        }
        if (curStepCount > 0) {
            this.mProgress = (float) Math.min((double) ((((float) curStepCount) * 1.0f) / ((float) this.mTotalStepTarget)), 1.0d);
        } else {
            this.mProgress = 0.0f;
        }
    }

    private void updateTotalStep(int target) {
        if (this.mTotalStepTarget != target) {
            this.mTotalStepTarget = target;
            int delta = this.mTotalStepTarget / 4;
            for (int i = 0; i < this.mKedus.length; i++) {
                int k = delta * (i + 1);
                if (k < 1000) {
                    this.mKedus[i].text = String.valueOf(k);
                } else if (k % 1000 == 0) {
                    this.mKedus[i].text = (k / 1000) + "k";
                } else {
                    this.mKedus[i].text = (((float) ((k + 50) / 100)) / 10.0f) + "k";
                }
            }
        }
    }

    protected float getProgress() {
        return this.mProgress;
    }

    protected void onDrawInternal(Canvas canvas) {
        for (Kedu kedu : this.mKedus) {
            this.mBitmapPaint.setTextAlign(kedu.align);
            this.mFonts.drawText(canvas, kedu.text, kedu.f33x, kedu.f34y, this.mBitmapPaint);
        }
    }
}
