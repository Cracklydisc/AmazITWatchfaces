package com.huami.watch.watchface.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;

public class ProgressWidget extends AbsWatchFaceDataWidget {
    private RectF mBound;
    private int mCenterX;
    private int mCenterY;
    private Path mClipPathBig = new Path();
    private Paint mGPaint;
    private int mHeight;
    private float mLeft;
    private float mMaxDegree;
    private Bitmap mProgress;
    private float mProgressDegreeToday;
    private float mStartDegree;
    private int mStep = 0;
    private float mStepDegree;
    private float mTop;
    private int mWidth;

    public ProgressWidget(Bitmap progress, float startDegree, float maxDegree, int step) {
        this.mStartDegree = startDegree;
        this.mMaxDegree = maxDegree;
        if (step > 0) {
            this.mStep = step;
            this.mStepDegree = this.mMaxDegree / ((float) step);
        }
        this.mProgress = progress;
        if (this.mProgress != null) {
            this.mWidth = this.mProgress.getWidth();
            this.mHeight = this.mProgress.getHeight();
            float hW = ((float) this.mWidth) * 0.5f;
            float hH = ((float) this.mHeight) * 0.5f;
            this.mLeft = -hW;
            this.mTop = -hH;
            this.mBound = new RectF(-hW, -hH, hW, hH);
        }
        this.mGPaint = new Paint(3);
        this.mGPaint.setStyle(Style.STROKE);
        this.mGPaint.setStrokeWidth(17.0f);
    }

    public int getDataType() {
        return 0;
    }

    public void onDataUpdate(int dataType, Object... values) {
        updateSportTodayDistance(((Float) values[0]).floatValue());
    }

    private void updateSportTodayDistance(float progress) {
        if (progress <= 0.0f) {
            this.mProgressDegreeToday = 0.0f;
        } else if (this.mStep > 0) {
            this.mProgressDegreeToday = this.mStepDegree * ((float) ((int) (Math.min(progress, 1.0f) * ((float) this.mStep))));
        } else {
            this.mProgressDegreeToday = Math.min(progress, 1.0f) * this.mMaxDegree;
        }
        this.mClipPathBig.rewind();
        this.mClipPathBig.addArc(this.mBound, this.mStartDegree, this.mProgressDegreeToday);
        this.mClipPathBig.lineTo(0.0f, 0.0f);
        this.mClipPathBig.close();
    }

    public int getX() {
        return this.mCenterX;
    }

    public int getY() {
        return this.mCenterY;
    }

    public void onDraw(Canvas canvas) {
        if (this.mProgress != null && !this.mProgress.isRecycled()) {
            canvas.save(2);
            canvas.clipPath(this.mClipPathBig);
            canvas.drawBitmap(this.mProgress, this.mLeft, this.mTop, this.mGPaint);
            canvas.restore();
        }
    }
}
