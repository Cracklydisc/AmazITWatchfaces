package com.huami.watch.watchface.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class StepProgressTextWidget extends AbsWatchFaceDataWidget {
    private int mCenterX;
    private int mCenterY;
    private ProgressWidget mProgressWidget;
    private int mStepCount = 0;
    private TextWidget mTextWidget;
    private int mTextX;
    private int mTextY;
    private int mTotalStepTarget = 8000;

    public StepProgressTextWidget(int centerX, int centerY, int textCenterX, int textTopY, Bitmap[] fontNums, Bitmap progress, float startDegree, float maxDegree, int step) {
        this.mCenterX = centerX;
        this.mCenterY = centerY;
        this.mTextX = textCenterX;
        this.mTextY = textTopY;
        this.mProgressWidget = new ProgressWidget(progress, startDegree, maxDegree, step);
        this.mTextWidget = new TextWidget(fontNums);
    }

    public void setTotalStepTarget(int target) {
        this.mTotalStepTarget = target;
    }

    public int getDataType() {
        return 1;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 1) {
            this.mStepCount = ((Integer) values[0]).intValue();
            this.mTextWidget.onDataUpdate(0, Integer.valueOf(this.mStepCount));
            updateProgress();
        }
    }

    private void updateProgress() {
        if (this.mTotalStepTarget > 0) {
            this.mProgressWidget.onDataUpdate(0, Float.valueOf((((float) this.mStepCount) * 1.0f) / ((float) this.mTotalStepTarget)));
        }
    }

    public int getX() {
        return this.mCenterX;
    }

    public int getY() {
        return this.mCenterY;
    }

    public void onDraw(Canvas canvas) {
        if (this.mProgressWidget != null) {
            this.mProgressWidget.onDraw(canvas);
        }
        if (this.mTextWidget != null) {
            canvas.translate((float) this.mTextX, (float) this.mTextY);
            this.mTextWidget.onDraw(canvas);
            canvas.translate((float) (-this.mTextX), (float) (-this.mTextY));
        }
    }
}
