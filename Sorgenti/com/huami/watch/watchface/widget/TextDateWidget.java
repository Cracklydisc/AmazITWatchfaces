package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import com.huami.watch.watchface.util.Util;

public class TextDateWidget extends AbsWatchFaceDataWidget {
    private Bitmap[] WEEKS;
    private String date = "12.12";
    private int dateX;
    private int dateY;
    private int day = 12;
    private Paint mBitmapPaint;
    private ImageFont mFonts;
    private String mFormat = "d";
    private int mHeight;
    private int mWidth;
    private int month = 12;
    private int week = 7;
    private int weekX;
    private int weekY;
    private int f35x;
    private int f36y;
    private int year = 2008;

    public TextDateWidget(Resources resources, int x, int y, int width, int height, ImageFont font, Bitmap[] weeks) {
        this.f35x = x;
        this.f36y = y;
        this.mWidth = width;
        this.mHeight = height;
        this.mFonts = font;
        this.mBitmapPaint = new Paint(7);
        this.WEEKS = weeks;
    }

    public int getDataType() {
        return 6;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 6 && values != null && values.length >= 4) {
            this.year = ((Integer) values[0]).intValue();
            this.month = ((Integer) values[1]).intValue();
            this.day = ((Integer) values[2]).intValue();
            this.week = ((Integer) values[3]).intValue();
            if (this.mFormat != null) {
                this.date = Util.formatDate(this.mFormat, this.year, this.month, this.day).toString();
            }
        }
    }

    public int getX() {
        return this.f35x;
    }

    public int getY() {
        return this.f36y;
    }

    public void setDatePosition(int x, int y) {
        this.dateX = x;
        this.dateY = y;
    }

    public void setWeekPosition(int x, int y) {
        this.weekX = x;
        this.weekY = y;
    }

    public void setTextAlign(Align align) {
        this.mBitmapPaint.setTextAlign(align);
    }

    public void setDateFormat(String format) {
        this.mFormat = format;
        if (this.mFormat != null) {
            this.date = Util.formatDate(this.mFormat, this.year, this.month, this.day).toString();
        }
    }

    public void onDraw(Canvas canvas) {
        if (this.mFonts != null) {
            this.mFonts.drawText(canvas, this.date, this.dateX, this.dateY, this.mBitmapPaint);
        }
        if (this.WEEKS != null && this.WEEKS.length > this.week - 1) {
            Bitmap weekBmp = this.WEEKS[this.week - 1];
            if (weekBmp != null && !weekBmp.isRecycled()) {
                canvas.drawBitmap(weekBmp, (float) this.weekX, (float) this.weekY, this.mBitmapPaint);
            }
        }
    }

    public void onDestroy() {
        this.mFonts.destroy();
        this.mFonts = null;
        if (this.WEEKS != null) {
            for (Bitmap weekBmp : this.WEEKS) {
                if (!(weekBmp == null || weekBmp.isRecycled())) {
                    weekBmp.recycle();
                }
            }
        }
        this.WEEKS = null;
    }
}
