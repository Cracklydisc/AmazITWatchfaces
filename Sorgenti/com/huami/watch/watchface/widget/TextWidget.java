package com.huami.watch.watchface.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class TextWidget extends AbsWatchFaceDataWidget {
    private Bitmap[] NUMS;
    private int mFontWidth = 0;
    private Paint mGPaint;
    private int mStepCount;

    public TextWidget(Bitmap[] fontNums) {
        this.NUMS = fontNums;
        if (!(this.NUMS == null || this.NUMS.length != 10 || this.NUMS[0] == null)) {
            this.mFontWidth = this.NUMS[0].getWidth();
        }
        this.mGPaint = new Paint(3);
        this.mGPaint.setStyle(Style.STROKE);
        this.mGPaint.setStrokeWidth(17.0f);
    }

    public int getDataType() {
        return 0;
    }

    public void onDataUpdate(int dataType, Object... values) {
        this.mStepCount = ((Integer) values[0]).intValue();
    }

    public int getX() {
        return 0;
    }

    public int getY() {
        return 0;
    }

    public void onDraw(Canvas canvas) {
        if (this.mFontWidth > 0) {
            String stepString = String.valueOf(this.mStepCount);
            int stepLength = stepString.length();
            int stepX = (-(stepLength * this.mFontWidth)) / 2;
            for (int i = 0; i < stepLength; i++) {
                canvas.drawBitmap(this.NUMS[stepString.charAt(i) - 48], (float) ((this.mFontWidth * i) + stepX), 0.0f, this.mGPaint);
            }
        }
    }
}
