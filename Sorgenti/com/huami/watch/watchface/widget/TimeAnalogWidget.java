package com.huami.watch.watchface.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class TimeAnalogWidget extends AbsWatchFaceDataWidget {
    private float hHalfH;
    private float hHalfW;
    private float hrRot;
    private int mCenterX;
    private int mCenterY;
    private Paint mGPaint;
    private float mHalfH;
    private float mHalfW;
    private Bitmap mHourBitmap;
    private Bitmap mMinBitmap;
    private Bitmap mSecBitmap;
    private float minRot;
    private float sHalfH;
    private float sHalfW;
    private float secRot;

    public TimeAnalogWidget(int centerX, int centerY, Paint paint, Bitmap hourBitmap, Bitmap minBitmap, Bitmap secBitmap) {
        this.mCenterX = centerX;
        this.mCenterY = centerY;
        if (paint != null) {
            this.mGPaint = paint;
        } else {
            this.mGPaint = new Paint(3);
            this.mGPaint.setStyle(Style.STROKE);
            this.mGPaint.setStrokeWidth(17.0f);
        }
        this.mHourBitmap = hourBitmap;
        if (this.mHourBitmap != null) {
            this.hHalfW = ((float) (-this.mHourBitmap.getWidth())) * 0.5f;
            this.hHalfH = ((float) (-this.mHourBitmap.getHeight())) * 0.5f;
        }
        this.mMinBitmap = minBitmap;
        if (this.mMinBitmap != null) {
            this.mHalfW = ((float) (-this.mMinBitmap.getWidth())) * 0.5f;
            this.mHalfH = ((float) (-this.mMinBitmap.getHeight())) * 0.5f;
        }
        this.mSecBitmap = secBitmap;
        if (this.mSecBitmap != null) {
            this.sHalfW = ((float) (-this.mSecBitmap.getWidth())) * 0.5f;
            this.sHalfH = ((float) (-this.mSecBitmap.getHeight())) * 0.5f;
        }
    }

    public int getDataType() {
        return 7;
    }

    public void onDataUpdate(int dataType, Object... values) {
        updateDegree(((Integer) values[0]).intValue(), ((Integer) values[1]).intValue(), ((Integer) values[2]).intValue());
    }

    private void updateDegree(int seconds, int minutes, int hours) {
        this.secRot = (float) (seconds * 6);
        this.minRot = (float) (minutes * 6);
        this.hrRot = (float) (((hours * 5) * 6) + ((minutes / 12) * 6));
    }

    public int getX() {
        return this.mCenterX;
    }

    public int getY() {
        return this.mCenterY;
    }

    public void onDraw(Canvas canvas) {
        if (!(this.mHourBitmap == null || this.mHourBitmap.isRecycled())) {
            canvas.save(1);
            canvas.rotate(this.hrRot);
            canvas.translate(this.hHalfW, this.hHalfH);
            canvas.drawBitmap(this.mHourBitmap, 0.0f, 0.0f, this.mGPaint);
            canvas.restore();
        }
        if (!(this.mMinBitmap == null || this.mMinBitmap.isRecycled())) {
            canvas.save(1);
            canvas.rotate(this.minRot);
            canvas.translate(this.mHalfW, this.mHalfH);
            canvas.drawBitmap(this.mMinBitmap, 0.0f, 0.0f, this.mGPaint);
            canvas.restore();
        }
        if (this.mMinBitmap != null && !this.mMinBitmap.isRecycled()) {
            canvas.save(1);
            canvas.rotate(this.secRot);
            canvas.translate(this.sHalfW, this.sHalfH);
            canvas.drawBitmap(this.mSecBitmap, 0.0f, 0.0f, this.mGPaint);
            canvas.restore();
        }
    }
}
