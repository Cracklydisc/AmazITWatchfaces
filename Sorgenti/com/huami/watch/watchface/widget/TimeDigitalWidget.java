package com.huami.watch.watchface.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import com.huami.watch.watchface.util.Util;

public class TimeDigitalWidget extends AbsWatchFaceDataWidget {
    private Align alignAM;
    private Align alignHour;
    private Align alignIndicator;
    private Align alignMinutes;
    private Align alignSeconds;
    private int ampm;
    private int color;
    private int gap;
    private int hCenter;
    private int height;
    private int hours;
    private String hoursText;
    private PorterDuffColorFilter mAMPMColorFilter;
    private PorterDuffColorFilter mColonColorFilter;
    private PorterDuffColorFilter mDefaultColorFilter;
    private boolean mDrawSeconds;
    private boolean mDrawTimeIndicator;
    private boolean mEnableIndicator;
    private PorterDuffColorFilter mHourColorFilter;
    private ImageFont mImageFont;
    private PorterDuffColorFilter mMinuteColorFilter;
    private int mModel;
    private int mPaddingToIndicator;
    private Paint mPaint;
    private PorterDuffColorFilter mSecondsColorFilter;
    private int minutes;
    private String minutesText;
    private int seconds;
    private String secondsText;
    private int width;
    private int f37x;
    private int xAm;
    private int xHour;
    private int xIndicator;
    private int xMinutes;
    private int xSeconds;
    private int f38y;
    private int yAm;
    private int yHour;
    private int yIndicator;
    private int yMinutes;
    private int ySeconds;

    public TimeDigitalWidget(int model, int x, int y, int width, int height, ImageFont font, int color, int gap) {
        this(model, x, y, width, height, font, color, gap, -1, -1);
    }

    public TimeDigitalWidget(int model, int x, int y, int width, int height, ImageFont font, int color, int gap, int xAm, int yAm) {
        this.mModel = 0;
        this.color = 0;
        this.seconds = 30;
        this.minutes = 9;
        this.hours = 10;
        this.ampm = 0;
        this.mEnableIndicator = true;
        this.mDrawTimeIndicator = true;
        this.mDrawSeconds = false;
        this.mPaddingToIndicator = 0;
        this.mModel = model;
        this.f37x = x;
        this.f38y = y;
        this.width = width;
        this.hCenter = this.width >>> 1;
        this.height = height;
        this.color = color;
        this.gap = gap;
        Log.d("TimeDigitalWidget", "TimeDigitalWidget x = " + x + " y = " + y + " size: " + width + " x " + height + " color:" + Integer.toHexString(color) + " gap: " + gap);
        this.mImageFont = font;
        this.mPaint = new Paint(6);
        if (this.color != 0) {
            this.mDefaultColorFilter = new PorterDuffColorFilter(this.color, Mode.SRC_ATOP);
        }
        initLayout();
        this.xAm = xAm;
        this.yAm = yAm;
        this.alignAM = Align.LEFT;
        if (this.mDrawSeconds) {
            this.secondsText = Util.formatTime(this.seconds);
        }
        this.minutesText = Util.formatTime(this.minutes);
        this.hoursText = Util.formatTime(this.hours);
    }

    private void initLayout() {
        int i = this.mModel;
        this.yMinutes = 0;
        this.yIndicator = 0;
        this.yHour = 0;
        this.mEnableIndicator = true;
        this.xIndicator = this.hCenter;
        this.alignIndicator = Align.CENTER;
        int wIndicator = this.mImageFont.getCharWidth(':');
        this.xHour = (this.hCenter - (wIndicator / 2)) - this.mPaddingToIndicator;
        this.alignHour = Align.RIGHT;
        this.xMinutes = (this.hCenter + (wIndicator / 2)) + this.mPaddingToIndicator;
        this.alignMinutes = Align.LEFT;
        this.mDrawSeconds = false;
    }

    public int getDataType() {
        return 7;
    }

    public void onDataUpdate(int dataType, Object... values) {
        this.seconds = ((Integer) values[0]).intValue();
        if (this.mDrawSeconds) {
            this.secondsText = Util.formatTime(this.seconds);
        }
        this.minutes = ((Integer) values[1]).intValue();
        this.minutesText = Util.formatTime(this.minutes);
        this.hours = ((Integer) values[2]).intValue();
        this.hoursText = Util.formatTime(this.hours);
        this.ampm = ((Integer) values[3]).intValue();
        if (this.xAm <= 0) {
            this.xAm = this.xMinutes + this.mImageFont.getFontWidth(this.minutesText);
        }
        if (this.yAm <= 0) {
            this.yAm = this.yMinutes;
        }
    }

    public int getX() {
        return this.f37x;
    }

    public int getY() {
        return this.f38y;
    }

    public void setMinuteColor(int color) {
        this.mMinuteColorFilter = new PorterDuffColorFilter(color, Mode.SRC_ATOP);
    }

    public void setPaddingToIndicator(int padding) {
        this.mPaddingToIndicator = padding;
    }

    public void onDraw(Canvas canvas) {
        if (this.mEnableIndicator && this.seconds % 2 == 0) {
            this.mPaint.setTextAlign(this.alignIndicator);
            if (this.mColonColorFilter != null) {
                this.mPaint.setColorFilter(this.mColonColorFilter);
            } else {
                this.mPaint.setColorFilter(this.mDefaultColorFilter);
            }
            this.mImageFont.drawText(canvas, ":", this.xIndicator, this.yIndicator, this.mPaint);
        }
        this.mPaint.setTextAlign(this.alignHour);
        if (this.mHourColorFilter != null) {
            this.mPaint.setColorFilter(this.mHourColorFilter);
        } else {
            this.mPaint.setColorFilter(this.mDefaultColorFilter);
        }
        this.mImageFont.drawText(canvas, this.hoursText, this.xHour, this.yHour, this.mPaint);
        this.mPaint.setTextAlign(this.alignMinutes);
        if (this.mMinuteColorFilter != null) {
            this.mPaint.setColorFilter(this.mMinuteColorFilter);
        } else {
            this.mPaint.setColorFilter(this.mDefaultColorFilter);
        }
        this.mImageFont.drawText(canvas, this.minutesText, this.xMinutes, this.yMinutes, this.mPaint);
        if (this.mDrawSeconds) {
            this.mPaint.setTextAlign(this.alignSeconds);
            if (this.mSecondsColorFilter != null) {
                this.mPaint.setColorFilter(this.mSecondsColorFilter);
            } else {
                this.mPaint.setColorFilter(this.mDefaultColorFilter);
            }
            this.mImageFont.drawText(canvas, this.secondsText, this.xSeconds, this.ySeconds, this.mPaint);
        }
        this.mPaint.setTextAlign(this.alignAM);
        if (this.mAMPMColorFilter != null) {
            this.mPaint.setColorFilter(this.mSecondsColorFilter);
        } else {
            this.mPaint.setColorFilter(this.mAMPMColorFilter);
        }
        switch (this.ampm) {
            case 0:
                this.mImageFont.drawText(canvas, "AM", this.xAm, this.yAm, this.mPaint);
                return;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.mImageFont.drawText(canvas, "PM", this.xAm, this.yAm, this.mPaint);
                return;
            default:
                return;
        }
    }
}
