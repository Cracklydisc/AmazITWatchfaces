package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;

public class TodayDistanceDateWidget extends DefaultWatchFaceDataWidget {
    private Drawable icon;
    private float mProgress;
    private String mTodayDistance;

    public TodayDistanceDateWidget(Resources resources, int x, int y, int model) {
        this(resources, x, y, model, null);
    }

    public TodayDistanceDateWidget(Resources resources, int x, int y, int model, ImageFont fonts) {
        super(resources, x, y, 2, model, fonts);
        this.mTodayDistance = "0";
        this.mProgress = 0.0f;
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_run_1.png");
                return;
            case 2:
                setProgressBitmap(((BitmapDrawable) resources.getDrawable(R.drawable.watchface_custom_fun_icon_circle_r_2, null)).getBitmap());
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/icon_android/watchface_custom_fun_icon_run_2.png");
                return;
            case 3:
                setProgressBitmap(Util.decodeImage(resources, "datawidget/model_3/icon/watchface_custom_fun_icon_circle_r.png"));
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_3/icon/watchface_custom_fun_icon_run.png");
                return;
            case 4:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_4/icon/watchface_custom_fun_icon_slpt_run.png");
                return;
            default:
                setProgressBitmap(((BitmapDrawable) resources.getDrawable(R.drawable.watchface_custom_fun_icon_circle_r, null)).getBitmap());
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_run.png");
                return;
        }
    }

    protected boolean isSupportProgress() {
        return true;
    }

    protected float getProgress() {
        return this.mProgress;
    }

    protected Drawable getIconDrawable(Resources resources) {
        return this.icon;
    }

    protected String getText() {
        return this.mTodayDistance;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (2 == dataType && values != null && values[0] != null) {
            double todayDistance = ((Double) values[0]).doubleValue();
            if (isWithUnit()) {
                switch (((Integer) values[1]).intValue()) {
                    case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                        this.mTodayDistance = Util.getFormatDistance(todayDistance) + "MI";
                        break;
                    default:
                        this.mTodayDistance = Util.getFormatDistance(todayDistance) + "KM";
                        break;
                }
            }
            this.mTodayDistance = Util.getFormatDistance(todayDistance);
            if (todayDistance > 0.0d) {
                this.mProgress = (float) Math.min(todayDistance / 3.0d, 1.0d);
            } else {
                this.mProgress = 0.0f;
            }
        }
    }
}
