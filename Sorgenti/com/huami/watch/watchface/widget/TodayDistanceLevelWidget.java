package com.huami.watch.watchface.widget;

import android.graphics.Bitmap;

public class TodayDistanceLevelWidget extends LevelWidget {
    private int mX;
    private int mY;

    public TodayDistanceLevelWidget(int x, int y, Bitmap[] levels) {
        super(levels);
        this.mX = x;
        this.mY = y;
    }

    public int getDataType() {
        return 2;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (2 == dataType && values != null && values[0] != null) {
            float progress;
            double todayDistance = ((Double) values[0]).doubleValue();
            if (todayDistance > 0.0d) {
                progress = (float) Math.min(todayDistance / 3.0d, 1.0d);
            } else {
                progress = 0.0f;
            }
            super.onDataUpdate(dataType, Float.valueOf(progress));
        }
    }

    public int getX() {
        return this.mX;
    }

    public int getY() {
        return this.mY;
    }
}
