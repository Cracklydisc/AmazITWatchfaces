package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;

public class TodayDistanceProgressWidget extends AbsWatchFaceDataWidget {
    private RectF mBound;
    private int mCenterX;
    private int mCenterY;
    private Path mClipPathBig = new Path();
    private Paint mGPaint;
    private int mHeight;
    private float mLeft;
    private float mMaxDegree;
    private Bitmap mProgress;
    private float mProgressDegreeToday;
    private float mStartDegree;
    private float mTop;
    private int mWidth;

    public TodayDistanceProgressWidget(Resources resources, int centerX, int centerY, Bitmap progress, float startDegree, float maxDegree) {
        this.mCenterX = centerX;
        this.mCenterY = centerY;
        this.mStartDegree = startDegree;
        this.mMaxDegree = maxDegree;
        this.mProgress = progress;
        if (this.mProgress != null) {
            this.mWidth = this.mProgress.getWidth();
            this.mHeight = this.mProgress.getHeight();
            float hW = ((float) this.mWidth) * 0.5f;
            float hH = ((float) this.mHeight) * 0.5f;
            this.mLeft = -hW;
            this.mTop = -hH;
            this.mBound = new RectF(-hW, -hH, hW, hH);
        }
        this.mGPaint = new Paint(3);
        this.mGPaint.setStyle(Style.STROKE);
        this.mGPaint.setStrokeWidth(17.0f);
    }

    public int getDataType() {
        return 2;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 2) {
            updateSportTodayDistance(((Double) values[0]).doubleValue());
        }
    }

    private void updateSportTodayDistance(double distance) {
        if (distance > 0.0d) {
            this.mProgressDegreeToday = (float) (Math.min(distance / 3.0d, 1.0d) * ((double) this.mMaxDegree));
        } else {
            this.mProgressDegreeToday = 0.0f;
        }
        this.mClipPathBig.rewind();
        this.mClipPathBig.addArc(this.mBound, this.mStartDegree, this.mProgressDegreeToday);
        this.mClipPathBig.lineTo(0.0f, 0.0f);
        this.mClipPathBig.close();
    }

    public int getX() {
        return this.mCenterX;
    }

    public int getY() {
        return this.mCenterY;
    }

    public void onDraw(Canvas canvas) {
        if (this.mProgress != null && !this.mProgress.isRecycled()) {
            canvas.save(2);
            canvas.clipPath(this.mClipPathBig);
            canvas.drawBitmap(this.mProgress, this.mLeft, this.mTop, this.mGPaint);
            canvas.restore();
        }
    }
}
