package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.util.Util;

public class TodayFloorDateWidget extends DefaultWatchFaceDataWidget {
    private Drawable icon;
    private String mFloorData = "--";

    public TodayFloorDateWidget(Resources resources, int x, int y, int model) {
        super(resources, x, y, 12, model);
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_stairs_1.png");
                return;
            case 2:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/icon_android/watchface_custom_fun_icon_stairs_2.png");
                return;
            case 3:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_3/icon/watchface_custom_fun_icon_stairs.png");
                return;
            case 4:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_4/icon/watchface_custom_fun_icon_stairs_8c.png");
                return;
            default:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_stairs.png");
                return;
        }
    }

    protected boolean isSupportProgress() {
        return false;
    }

    protected float getProgress() {
        return 0.0f;
    }

    protected Drawable getIconDrawable(Resources resources) {
        return this.icon;
    }

    protected String getText() {
        return this.mFloorData;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 12 && values[0] != null) {
            int floordata = ((Integer) values[0]).intValue();
            if (floordata > 0) {
                this.mFloorData = String.valueOf(floordata);
            } else {
                this.mFloorData = "--";
            }
        }
    }
}
