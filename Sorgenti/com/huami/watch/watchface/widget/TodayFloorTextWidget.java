package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Paint.Align;

public class TodayFloorTextWidget extends AbsTextWidget {
    private String mText = "--";

    public TodayFloorTextWidget(Resources resources, int x, int y, ImageFont fonts) {
        super(x, y, fonts);
        setTextAlign(Align.RIGHT);
    }

    protected String getText() {
        return this.mText;
    }

    public int getDataType() {
        return 12;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 12 && values[0] != null) {
            int floordata = ((Integer) values[0]).intValue();
            if (floordata > 0) {
                this.mText = String.valueOf(floordata);
            } else {
                this.mText = "--";
            }
        }
    }
}
