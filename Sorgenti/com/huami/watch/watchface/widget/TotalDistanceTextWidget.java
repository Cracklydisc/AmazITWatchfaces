package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint.Align;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;

public class TotalDistanceTextWidget extends AbsTextWidget {
    private String mText = "--";
    private Drawable mUnit;
    private int measurement = -1;
    private Resources resources;

    public TotalDistanceTextWidget(Resources resources, int x, int y, ImageFont fonts) {
        super(x, y, fonts);
        this.resources = resources;
        setTextAlign(Align.RIGHT);
    }

    private void updateUnit() {
        switch (this.measurement) {
            case 0:
                this.mUnit = this.resources.getDrawable(R.drawable.unit_km, null);
                this.mUnit.setBounds(0, 0, this.mUnit.getIntrinsicWidth(), this.mUnit.getIntrinsicHeight());
                return;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.mUnit = this.resources.getDrawable(R.drawable.unit_mi, null);
                this.mUnit.setBounds(0, 0, this.mUnit.getIntrinsicWidth(), this.mUnit.getIntrinsicHeight());
                return;
            default:
                return;
        }
    }

    protected String getText() {
        return this.mText;
    }

    public int getDataType() {
        return 3;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 3 && values != null && values[0] != null && values[1] != null) {
            this.mText = Util.getFormatDistance(((Double) values[0]).doubleValue());
            int measurement = ((Integer) values[1]).intValue();
            if (this.measurement != measurement) {
                this.measurement = measurement;
                updateUnit();
            }
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save(1);
        canvas.translate(4.0f, 12.0f);
        if (this.mUnit != null) {
            this.mUnit.draw(canvas);
        }
        canvas.restore();
    }
}
