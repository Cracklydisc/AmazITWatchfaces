package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.analogyellow.R;
import com.huami.watch.watchface.util.Util;

public class WalkDateWidget extends DefaultWatchFaceDataWidget {
    private Drawable icon;
    private float mProgress;
    private String mWalk;

    public WalkDateWidget(Resources resources, int x, int y, int model) {
        this(resources, x, y, model, null);
    }

    public WalkDateWidget(Resources resources, int x, int y, int model, ImageFont fonts) {
        super(resources, x, y, 1, model, fonts);
        this.mWalk = "0";
        this.mProgress = 0.0f;
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_walk_1.png");
                return;
            case 2:
                setProgressBitmap(((BitmapDrawable) resources.getDrawable(R.drawable.watchface_custom_fun_icon_circle_r_2, null)).getBitmap());
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/icon_android/watchface_custom_fun_icon_walk_2.png");
                return;
            case 3:
                setProgressBitmap(Util.decodeImage(resources, "datawidget/model_3/icon/watchface_custom_fun_icon_circle_r.png"));
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_3/icon/watchface_custom_fun_icon_walk.png");
                return;
            case 4:
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/model_4/icon/watchface_custom_fun_icon_slpt_walk.png");
                return;
            default:
                setProgressBitmap(((BitmapDrawable) resources.getDrawable(R.drawable.watchface_custom_fun_icon_circle_r, null)).getBitmap());
                this.icon = Util.decodeBitmapDrawableFromAssets(resources, "datawidget/slpt/watchface_custom_fun_icon_slpt_walk.png");
                return;
        }
    }

    protected boolean isSupportProgress() {
        return true;
    }

    protected float getProgress() {
        return this.mProgress;
    }

    protected Drawable getIconDrawable(Resources resources) {
        return this.icon;
    }

    protected String getText() {
        return this.mWalk;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (1 == dataType && values != null && values[0] != null) {
            int walk = ((Integer) values[0]).intValue();
            int target = ((Integer) values[1]).intValue();
            this.mWalk = String.valueOf(walk);
            if (target > 0) {
                this.mProgress = (((float) walk) * 1.0f) / ((float) target);
            }
        }
    }
}
