package com.huami.watch.watchface.widget;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.recyclerview.C0051R;
import android.text.TextUtils;
import com.huami.watch.watchface.util.Util;

public class WeatherDateWidget extends DefaultWatchFaceDataWidget {
    private BitmapDrawable icon;
    private String mIconPath;
    private int mLastIconIndex;
    private String mTempString;
    private String tempFlag;
    private String tempString;
    private final String[] weatherIconNames;
    private int weatherType;

    public WeatherDateWidget(Resources resources, int x, int y, int model) {
        this(resources, x, y, model, null);
    }

    public WeatherDateWidget(Resources resources, int x, int y, int model, ImageFont fonts) {
        this(resources, x, y, -1, -1, model, fonts);
    }

    public WeatherDateWidget(Resources resources, int x, int y, int width, int height, int model, ImageFont fonts) {
        super(resources, x, y, 8, model, fonts);
        this.weatherIconNames = new String[]{"wf_weather_sunny.png", "wf_weather_cloudy.png", "wf_weather_overcast.png", "wf_weather_fog.png", "wf_weather_smog.png", "wf_weather_shower.png", "wf_weather_thunder_shower.png", "wf_weather_light_rain.png", "wf_weather_moderate_rain.png", "wf_weather_heavy_rain.png", "wf_weather_rainstorm.png", "wf_weather_torrential_rain.png", "wf_weather_sleet.png", "wf_weather_freezing_rain.png", "wf_weather_hail.png", "wf_weather_light_snow.png", "wf_weather_moderate_snow.png", "wf_weather_heavy_snow.png", "wf_weather_snowstorm.png", "wf_weather_dust.png", "wf_weather_blowing_sand.png", "wf_weather_sand_storm.png", "wf_weather_unknown.png"};
        this.weatherType = -1;
        this.mTempString = "--";
        this.mLastIconIndex = -1;
        switch (model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.mIconPath = "datawidget/weather/01/";
                return;
            case 2:
            case 3:
                this.mIconPath = "datawidget/weather/02/";
                return;
            case 4:
                this.mIconPath = "datawidget/model_4/weather/";
                return;
            case 19:
                this.mIconPath = "datawidget/weather/03/";
                return;
            default:
                this.mIconPath = "datawidget/weather/00/";
                return;
        }
    }

    private void setWeatherIcon(int index) {
        if (index != this.mLastIconIndex) {
            this.mLastIconIndex = index;
            BitmapDrawable d = Util.decodeBitmapDrawableFromAssets(this.resources, this.mIconPath + this.weatherIconNames[index]);
            BitmapDrawable old = this.icon;
            this.icon = d;
            if (old != null) {
                Bitmap b = old.getBitmap();
                if (b != null && !b.isRecycled()) {
                    b.recycle();
                }
            }
        }
    }

    public void setWeatherIconPath(String iconPath) {
        this.mIconPath = iconPath;
    }

    protected boolean isAlwaysShowUnit() {
        return false;
    }

    protected boolean isSupportProgress() {
        return false;
    }

    protected float getProgress() {
        return 0.0f;
    }

    protected Drawable getIconDrawable(Resources resources) {
        return this.icon;
    }

    protected String getText() {
        return this.mTempString;
    }

    public void onDataUpdate(int dataType, Object... values) {
        if (dataType == 8) {
            this.tempFlag = (String) values[0];
            this.tempString = (String) values[1];
            this.weatherType = ((Integer) values[2]).intValue();
            if (TextUtils.isEmpty(this.tempString)) {
                this.mTempString = "--";
                setWeatherIcon(this.weatherIconNames.length - 1);
                return;
            }
            String unit;
            if (getModel() == 0 && this.tempString.indexOf(47) > 0 && !isAlwaysShowUnit()) {
                unit = null;
            } else if (TextUtils.isEmpty(this.tempFlag) || this.tempFlag.equals("1")) {
                unit = "℃";
            } else {
                unit = "℉";
            }
            this.mTempString = this.tempString + unit;
            if (this.weatherType < 0 || this.weatherType >= this.weatherIconNames.length) {
                setWeatherIcon(this.weatherIconNames.length - 1);
            } else {
                setWeatherIcon(this.weatherType);
            }
        }
    }
}
