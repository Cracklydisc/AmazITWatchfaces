package com.huami.watch.watchface.widget.slpt;

import android.content.Context;
import android.support.v7.recyclerview.C0051R;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.sport.SlptTodayCaloriesView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class SlptCalorieWidget extends SlptDefaultWidget {
    private Context mContext;

    public SlptCalorieWidget(Context context, int x, int y, int width, int height, int mode, boolean need26WC) {
        super(context, x, y, width, height, mode, need26WC);
        this.mContext = context;
    }

    public void setIconPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.iconPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }

    public void setDataPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.dataPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }

    public SlptViewComponent getDataView() {
        SlptTodayCaloriesView caloriesView = new SlptTodayCaloriesView();
        caloriesView.setImagePictureArray(getDefaultNumMem());
        return caloriesView;
    }

    public SlptViewComponent getIconView() {
        byte[] mem;
        SlptPictureView iconView = new SlptPictureView();
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_calories_1.png");
                break;
            case 2:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_calories_2.png");
                break;
            case 3:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/8C/icon/watchface_custom_fun_icon_slpt_calories.png");
                break;
            case 4:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_4/icon/watchface_custom_fun_icon_slpt_calories.png");
                break;
            default:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_calories.png");
                break;
        }
        iconView.setImagePicture(mem);
        return iconView;
    }

    public SlptViewComponent getProgressView() {
        byte[] Bgmem;
        switch (getModel()) {
            case 2:
            case 3:
            case 4:
                Bgmem = null;
                break;
            default:
                Bgmem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_circle_bg.png");
                break;
        }
        if (Bgmem == null) {
            return null;
        }
        SlptViewComponent pictureView = new SlptPictureView();
        pictureView.setImagePicture(Bgmem);
        pictureView.setStart(getX(), getY());
        pictureView.setRect(getWidth(), getHeight());
        return pictureView;
    }

    public SlptViewComponent getWidgetView() {
        SlptViewComponent dataView = getDataView();
        SlptViewComponent iconView = getIconView();
        SlptViewComponent defaultCircleView = getProgressView();
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
            case 4:
                SlptViewComponent layout1 = new SlptLinearLayout();
                layout1.add(iconView);
                layout1.add(dataView);
                dataView.setPadding(this.dataPadLeft, 0, 0, 0);
                layout1.setStart(getX(), getY());
                layout1.setRect(getWidth(), getHeight());
                layout1.alignX = (byte) 2;
                layout1.alignY = (byte) 2;
                return layout1;
            case 2:
            case 3:
                SlptViewComponent layout2 = new SlptAbsoluteLayout();
                setIconPosition(iconView);
                setDataPosition(dataView);
                layout2.add(dataView);
                layout2.add(iconView);
                return layout2;
            default:
                SlptViewComponent layout = new SlptAbsoluteLayout();
                setIconPosition(iconView);
                setDataPosition(dataView);
                layout.add(dataView);
                layout.add(iconView);
                layout.add(defaultCircleView);
                return layout;
        }
    }
}
