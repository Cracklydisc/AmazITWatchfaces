package com.huami.watch.watchface.widget.slpt;

import android.content.Context;
import android.support.v7.recyclerview.C0051R;
import android.util.Log;
import com.huami.watch.watchface.util.CalendarUtil;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptDayHView;
import com.ingenic.iwds.slpt.view.digital.SlptDayLView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthHView;
import com.ingenic.iwds.slpt.view.digital.SlptMonthLView;
import com.ingenic.iwds.slpt.view.digital.SlptWeekView;
import com.ingenic.iwds.slpt.view.digital.SlptYear0View;
import com.ingenic.iwds.slpt.view.digital.SlptYear1View;
import com.ingenic.iwds.slpt.view.digital.SlptYear2View;
import com.ingenic.iwds.slpt.view.digital.SlptYear3View;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import java.util.ArrayList;

public class SlptDateWidget extends SlptDefaultWidget {
    SlptLinearLayout dateLayout = new SlptLinearLayout();
    private Context mContext;

    public SlptDateWidget(Context context, int x, int y, int width, int height, int mode, boolean need26WC) {
        super(context, x, y, width, height, mode, need26WC);
        initDefaultWeekMem();
        initDefaultDateMem();
        this.mContext = context;
    }

    public SlptViewComponent getYearView() {
        SlptLinearLayout yearLayout = new SlptLinearLayout();
        SlptYear0View year0View = new SlptYear0View();
        SlptYear1View year1View = new SlptYear1View();
        SlptYear2View year2View = new SlptYear2View();
        yearLayout.add(new SlptYear3View());
        yearLayout.add(year2View);
        yearLayout.add(year1View);
        yearLayout.add(year0View);
        yearLayout.setImagePictureArrayForAll(getDefaultDateMem());
        return yearLayout;
    }

    public SlptViewComponent getMonthView() {
        SlptLinearLayout dateLayout = new SlptLinearLayout();
        SlptMonthHView monthHView = new SlptMonthHView();
        SlptMonthLView monthLView = new SlptMonthLView();
        dateLayout.add(monthHView);
        dateLayout.add(monthLView);
        dateLayout.setImagePictureArrayForAll(getDefaultDateMem());
        return dateLayout;
    }

    public SlptViewComponent getDayView() {
        SlptLinearLayout dateLayout = new SlptLinearLayout();
        SlptDayHView dayHView = new SlptDayHView();
        SlptDayLView dayLView = new SlptDayLView();
        dateLayout.add(dayHView);
        dateLayout.add(dayLView);
        dateLayout.setImagePictureArrayForAll(getDefaultDateMem());
        return dateLayout;
    }

    public SlptPictureView getSeqView() {
        switch (getModel()) {
            case 6:
            case 7:
                if (checkNeed26WC()) {
                    return getPathView("datawidget/model_3/font/dot.png");
                }
                return getPathView("datawidget/model_3/8C/num/dot.png");
            case 8:
            case 9:
            case 10:
            case 11:
                return getPathView("datawidget/model_4/font/dot.png");
            default:
                return getMinusView();
        }
    }

    public SlptViewComponent getWeekView() {
        SlptWeekView weekView = new SlptWeekView();
        weekView.setImagePictureArray(getDefaultWeekMem());
        return weekView;
    }

    public SlptViewComponent getWidgetView() {
        SlptViewComponent yearView = getYearView();
        SlptViewComponent monthView = getDayView();
        SlptViewComponent dayView = getMonthView();
        SlptViewComponent weekView = getWeekView();
        SlptLinearLayout holidayView = getHolidayView(this.mContext);
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.dateLayout.add(monthView);
                this.dateLayout.add(getSeqView());
                this.dateLayout.add(dayView);
                break;
            case 2:
                this.dateLayout.add(weekView);
                break;
            case 3:
                if (holidayView != null) {
                    this.dateLayout.add(holidayView);
                    break;
                }
                break;
            case 4:
            case 9:
                this.dateLayout.add(yearView);
                this.dateLayout.add(getSeqView());
                this.dateLayout.add(monthView);
                this.dateLayout.add(getSeqView());
                this.dateLayout.add(dayView);
                break;
            case 5:
                this.dateLayout.add(dayView);
                break;
            case 8:
            case 11:
                this.dateLayout.add(yearView);
                this.dateLayout.add(getSeqView());
                this.dateLayout.add(monthView);
                this.dateLayout.add(getSeqView());
                this.dateLayout.add(dayView);
                this.dateLayout.add(weekView);
                weekView.setPadding(this.weekPadingLeft, 0, 0, 0);
                break;
            case 10:
                this.dateLayout.add(holidayView);
                this.dateLayout.add(weekView);
                weekView.setPadding(this.weekPadingLeft, 0, 0, 0);
                break;
            default:
                this.dateLayout.add(monthView);
                this.dateLayout.add(getSeqView());
                this.dateLayout.add(dayView);
                this.dateLayout.add(weekView);
                weekView.setPadding(this.weekPadingLeft, 0, 0, 0);
                break;
        }
        this.dateLayout.setStart(getX(), getY());
        this.dateLayout.setRect(getWidth(), getHeight());
        this.dateLayout.alignX = (byte) 2;
        this.dateLayout.alignY = (byte) 2;
        return this.dateLayout;
    }

    private SlptLinearLayout getHolidayView(Context context) {
        String path;
        String title = new CalendarUtil(context.getResources()).getHolidayString();
        SlptLinearLayout layout = new SlptLinearLayout();
        ArrayList list = Util.chinaToUnicode(title);
        Log.d(getClass().getCanonicalName(), "holiday string" + title);
        switch (getModel()) {
            case 3:
                path = "datawidget/font/lunar/";
                break;
            case 10:
                path = "datawidget/model_4/week/cn/";
                break;
            default:
                return null;
        }
        for (int i = 0; i < list.size(); i++) {
            try {
                byte[] mem = SimpleFile.readFileFromAssets(context, path + list.get(i).toString() + ".png");
                SlptPictureView view = new SlptPictureView();
                view.setImagePicture(mem);
                layout.add(view);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return layout;
    }
}
