package com.huami.watch.watchface.widget.slpt;

import android.content.Context;
import android.support.v7.recyclerview.C0051R;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public abstract class SlptDefaultWidget {
    public static int POWER_LEVEL_MODE_11 = 11;
    public static int POWER_LEVLE_MODE_10 = 10;
    public int dataPadLeft;
    public int dataPadTop;
    public byte[][] default_date;
    public byte[][] default_num;
    public byte[][] default_week;
    private int height;
    public int iconPadTop;
    private Context mContext;
    private int model;
    private boolean need26WC;
    private String numPath;
    public int weekPadingLeft;
    private int width;
    private int f39x;
    private int f40y;

    public abstract SlptViewComponent getWidgetView();

    public SlptDefaultWidget(Context context, int x, int y, int width, int height, int model) {
        this.model = 0;
        this.numPath = "";
        this.iconPadTop = 14;
        this.dataPadTop = 42;
        this.dataPadLeft = 5;
        this.weekPadingLeft = 7;
        this.default_num = new byte[10][];
        this.default_date = new byte[10][];
        this.default_week = new byte[7][];
        this.mContext = context;
        this.f39x = x;
        this.f40y = y;
        this.model = model;
        this.width = width;
        this.height = height;
        this.need26WC = false;
        initDefaultNumMem();
    }

    public SlptDefaultWidget(Context context, int x, int y, int width, int height, int model, boolean need26WC) {
        this.model = 0;
        this.numPath = "";
        this.iconPadTop = 14;
        this.dataPadTop = 42;
        this.dataPadLeft = 5;
        this.weekPadingLeft = 7;
        this.default_num = new byte[10][];
        this.default_date = new byte[10][];
        this.default_week = new byte[7][];
        this.mContext = context;
        this.f39x = x;
        this.f40y = y;
        this.model = model;
        this.width = width;
        this.height = height;
        this.need26WC = need26WC;
        initDefaultNumMem();
    }

    public SlptDefaultWidget(Context context, int x, int y, int width, int height, int model, String numPath) {
        this.model = 0;
        this.numPath = "";
        this.iconPadTop = 14;
        this.dataPadTop = 42;
        this.dataPadLeft = 5;
        this.weekPadingLeft = 7;
        this.default_num = new byte[10][];
        this.default_date = new byte[10][];
        this.default_week = new byte[7][];
        this.mContext = context;
        this.f39x = x;
        this.f40y = y;
        this.model = model;
        this.width = width;
        this.height = height;
        this.numPath = numPath;
        this.need26WC = false;
        initDefaultNumMem();
    }

    public int getX() {
        return this.f39x;
    }

    public int getY() {
        return this.f40y;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public int getModel() {
        return this.model;
    }

    public boolean checkNeed26WC() {
        return this.need26WC;
    }

    private void initDefaultNumMem() {
        String path;
        switch (this.model) {
            case -1:
                path = new String(this.numPath + "%d.png");
                break;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                path = new String("datawidget/font/02/default_%d.png");
                break;
            case 2:
                path = new String("datawidget/font/03/8c/%d.png");
                this.iconPadTop = 16;
                this.dataPadTop = 44;
                break;
            case 3:
                if (checkNeed26WC()) {
                    path = new String("datawidget/model_3/font/%d.png");
                } else {
                    path = new String("datawidget/model_3/8C/num/%d.png");
                }
                this.iconPadTop = 23;
                this.dataPadTop = 47;
                break;
            case 4:
                path = new String("datawidget/model_4/font/%d.png");
                break;
            default:
                path = new String("datawidget/font/01/default_%d.png");
                break;
        }
        for (int i = 0; i < 10; i++) {
            this.default_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
    }

    public byte[][] getDefaultNumMem() {
        return this.default_num;
    }

    public void initDefaultDateMem() {
        String path;
        switch (this.model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                path = new String("datawidget/font/02/default_%d.png");
                break;
            case 2:
            case 5:
                path = new String("datawidget/font/data5/8c/%d.png");
                break;
            case 6:
            case 7:
                if (checkNeed26WC()) {
                    path = new String("datawidget/model_3/font/%d.png");
                } else {
                    path = new String("datawidget/model_3/8C/num/%d.png");
                }
                this.weekPadingLeft = 14;
                break;
            case 8:
            case 9:
            case 10:
            case 11:
                path = new String("datawidget/model_4/font/%d.png");
                break;
            default:
                path = new String("guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/android/date_%d.png");
                break;
        }
        for (int i = 0; i < 10; i++) {
            this.default_date[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
    }

    public byte[][] getDefaultDateMem() {
        return this.default_date;
    }

    public void initDefaultWeekMem() {
        String path;
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                if (!Util.needEnAssets()) {
                    path = new String("datawidget/date_2/week_%d.png");
                    break;
                } else {
                    path = new String("datawidget/date_2/en/week_%d.png");
                    break;
                }
            case 6:
                path = new String("datawidget/model_3/8C/num/week_en_%d.png");
                break;
            case 7:
                path = new String("datawidget/model_3/8C/num/week_zh_%d.png");
                break;
            case 8:
            case 9:
            case 10:
                path = new String("datawidget/model_4/week/cn/w%d.png");
                break;
            case 11:
                path = new String("datawidget/model_4/week/w%d.png");
                break;
            default:
                if (!Util.needEnAssets()) {
                    path = new String("guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/week_%d.png");
                    break;
                } else {
                    path = new String("guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/en/week_%d.png");
                    break;
                }
        }
        for (int i = 0; i < 7; i++) {
            this.default_week[i] = SimpleFile.readFileFromAssets(this.mContext, String.format(path.toString(), new Object[]{Integer.valueOf(i)}));
        }
    }

    public byte[][] getDefaultWeekMem() {
        return this.default_week;
    }

    public SlptPictureView getDotView() {
        byte[] mem;
        SlptPictureView view = new SlptPictureView();
        switch (this.model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/font/02/dot.png");
                break;
            case 2:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/font/03/8c/dot.png");
                break;
            case 3:
                if (!checkNeed26WC()) {
                    mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/8C/num/dot.png");
                    break;
                }
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/font/dot.png");
                break;
            case 4:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_4/font/dot.png");
                break;
            default:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/font/01/dot.png");
                break;
        }
        view.setImagePicture(mem);
        return view;
    }

    public SlptPictureView getPathView(String path) {
        SlptPictureView view = new SlptPictureView();
        byte[] mem = SimpleFile.readFileFromAssets(this.mContext, path);
        if (mem == null) {
            return null;
        }
        view.setImagePicture(mem);
        return view;
    }

    public SlptPictureView getMinusView() {
        byte[] mem;
        SlptPictureView view = new SlptPictureView();
        switch (this.model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/font/02/minus.png");
                break;
            case 2:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/font/03/8c/minus.png");
                break;
            case 3:
                if (!checkNeed26WC()) {
                    mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/8C/num/minus.png");
                    break;
                }
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/font/minus.png");
                break;
            case 4:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_4/font/minus.png");
                break;
            default:
                mem = SimpleFile.readFileFromAssets(this.mContext, "guard/com.huami.watch.watchface.AnalogWatchFaceThirteen/android/minus.png");
                break;
        }
        view.setImagePicture(mem);
        return view;
    }

    public SlptPictureView getPercentView() {
        byte[] mem;
        SlptPictureView view = new SlptPictureView();
        switch (this.model) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/font/02/percent_icon.png");
                break;
            case 2:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/font/03/8c/percent_icon.png");
                break;
            case 3:
                if (!checkNeed26WC()) {
                    mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/8C/num/percent_icon.png");
                    break;
                }
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/font/percent_icon.png");
                break;
            case 4:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_4/font/percent_icon.png");
                break;
            default:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/font/01/percent_icon.png");
                break;
        }
        view.setImagePicture(mem);
        return view;
    }
}
