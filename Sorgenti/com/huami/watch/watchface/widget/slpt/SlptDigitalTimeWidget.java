package com.huami.watch.watchface.widget.slpt;

import android.content.Context;
import android.text.format.DateFormat;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.digital.SlptHourHView;
import com.ingenic.iwds.slpt.view.digital.SlptHourLView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteHView;
import com.ingenic.iwds.slpt.view.digital.SlptMinuteLView;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;

public class SlptDigitalTimeWidget extends SlptDefaultWidget {
    SlptPictureView aView;
    SlptPictureView colonView = new SlptPictureView();
    SlptHourHView hourHView = new SlptHourHView();
    SlptHourLView hourLView = new SlptHourLView();
    SlptLinearLayout layout = new SlptLinearLayout();
    private Context mContext;
    SlptPictureView mView;
    SlptMinuteHView minuteHView = new SlptMinuteHView();
    SlptMinuteLView minuteLView = new SlptMinuteLView();
    SlptPictureView pView;

    public SlptDigitalTimeWidget(Context context, int x, int y, int width, int height, int mode) {
        super(context, x, y, width, height, mode);
        this.mContext = context;
    }

    public void setTimeView(byte[][] num) {
        this.hourHView.setImagePictureArray(num);
        this.hourLView.setImagePictureArray(num);
        this.minuteHView.setImagePictureArray(num);
        this.minuteLView.setImagePictureArray(num);
    }

    public void setColonView(byte[] num) {
        this.colonView.setImagePicture(num);
    }

    public void setAPMview(byte[] num_A, byte[] num_P, byte[] num_M) {
        if (num_A == null) {
            this.aView = null;
        } else {
            this.aView = new SlptPictureView();
            this.aView.setImagePicture(num_A);
            SlptSportUtil.setAmBgView(this.aView);
        }
        if (num_P == null) {
            this.pView = null;
        } else {
            this.pView = new SlptPictureView();
            this.pView.setImagePicture(num_P);
            SlptSportUtil.setPmBgView(this.pView);
        }
        if (num_M == null) {
            this.mView = null;
            return;
        }
        this.mView = new SlptPictureView();
        this.mView.setImagePicture(num_M);
    }

    public SlptViewComponent getWidgetView() {
        this.layout.add(this.hourHView);
        this.layout.add(this.hourLView);
        this.layout.add(this.colonView);
        this.layout.add(this.minuteHView);
        this.layout.add(this.minuteLView);
        if (!DateFormat.is24HourFormat(this.mContext)) {
            if (this.aView != null) {
                this.layout.add(this.aView);
                this.aView.alignY = (byte) 0;
            }
            if (this.pView != null) {
                this.layout.add(this.pView);
                this.pView.alignY = (byte) 0;
            }
            if (this.mView != null) {
                this.layout.add(this.mView);
                this.mView.alignY = (byte) 0;
            }
        }
        this.layout.setStart(getX(), getY());
        return this.layout;
    }
}
