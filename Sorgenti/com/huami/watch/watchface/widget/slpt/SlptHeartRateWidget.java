package com.huami.watch.watchface.widget.slpt;

import android.content.Context;
import android.support.v7.recyclerview.C0051R;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.sport.SlptLastHeartRateView;
import com.ingenic.iwds.slpt.view.sport.SlptSportUtil;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class SlptHeartRateWidget extends SlptDefaultWidget {
    private SlptPictureView invalidView = new SlptPictureView();
    private Context mContext;

    public SlptHeartRateWidget(Context context, int x, int y, int width, int height, int model, boolean need26WC) {
        super(context, x, y, width, height, model, need26WC);
        this.mContext = context;
    }

    public void setIconPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.iconPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }

    public void setDataPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.dataPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }

    public SlptViewComponent getDataView() {
        byte[] mem;
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        SlptLastHeartRateView heartRateView = new SlptLastHeartRateView();
        heartRateView.setImagePictureArray(getDefaultNumMem());
        switch (getModel()) {
            case 3:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/8C/num/--.png");
                break;
            case 4:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_4/font/--.png");
                break;
            default:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/--.png");
                break;
        }
        this.invalidView.setImagePicture(mem);
        SlptSportUtil.setLastHeartRateInvalidView(this.invalidView);
        layout.add(heartRateView);
        layout.add(this.invalidView);
        return layout;
    }

    public SlptViewComponent getIconView() {
        byte[] mem;
        SlptPictureView iconView = new SlptPictureView();
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_heart_rate_1.png");
                break;
            case 2:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_heart_rate_2.png");
                break;
            case 3:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/8C/icon/watchface_custom_fun_icon_slpt_heart_rate.png");
                break;
            case 4:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_4/icon/watchface_custom_fun_icon_slpt_heart_rate.png");
                break;
            default:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_heart_rate.png");
                break;
        }
        iconView.setImagePicture(mem);
        return iconView;
    }

    public SlptViewComponent getProgressView() {
        byte[] circleByte = null;
        switch (getModel()) {
            case 2:
            case 3:
            case 4:
                break;
            default:
                circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_circle_bg.png");
                break;
        }
        if (circleByte == null) {
            return null;
        }
        SlptViewComponent pictureView = new SlptPictureView();
        pictureView.setImagePicture(circleByte);
        pictureView.setStart(getX(), getY());
        pictureView.setRect(getWidth(), getHeight());
        return pictureView;
    }

    public SlptViewComponent getWidgetView() {
        SlptViewComponent dataView = getDataView();
        SlptViewComponent iconView = getIconView();
        SlptViewComponent defaultCircleView = getProgressView();
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
            case 4:
                SlptViewComponent layout1 = new SlptLinearLayout();
                layout1.add(iconView);
                layout1.add(dataView);
                dataView.setPadding(this.dataPadLeft, 0, 0, 0);
                layout1.setStart(getX(), getY());
                layout1.setRect(getWidth(), getHeight());
                layout1.alignX = (byte) 2;
                layout1.alignY = (byte) 2;
                return layout1;
            default:
                SlptViewComponent layout = new SlptAbsoluteLayout();
                setIconPosition(iconView);
                setDataPosition(dataView);
                if (dataView != null) {
                    layout.add(dataView);
                }
                if (iconView != null) {
                    layout.add(iconView);
                }
                if (defaultCircleView != null) {
                    layout.add(defaultCircleView);
                }
                return layout;
        }
    }
}
