package com.huami.watch.watchface.widget.slpt;

import android.content.Context;
import android.support.v7.recyclerview.C0051R;
import com.ingenic.iwds.slpt.view.arc.SlptPowerArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptBatteryView;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.sport.SlptPowerNumView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class SlptPowerWidget extends SlptDefaultWidget {
    private int BATTERY_LEVEL;
    private Context mContext;

    public SlptPowerWidget(Context context, int x, int y, int width, int height, int model, boolean need26WC) {
        super(context, x, y, width, height, model, need26WC);
        this.mContext = context;
    }

    public void setIconPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.iconPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }

    public void setDataPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.dataPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }

    public SlptViewComponent getDataView() {
        SlptLinearLayout layout = new SlptLinearLayout();
        SlptPowerNumView powerNumView = new SlptPowerNumView();
        powerNumView.setImagePictureArray(getDefaultNumMem());
        layout.add(powerNumView);
        layout.add(getPercentView());
        return layout;
    }

    public SlptViewComponent getIconView() {
        byte[][] battery_num;
        int i;
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                this.BATTERY_LEVEL = POWER_LEVEL_MODE_11;
                battery_num = new byte[this.BATTERY_LEVEL][];
                for (i = 0; i < this.BATTERY_LEVEL; i++) {
                    battery_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("datawidget/slpt/watchface_custom_fun_icon_slpt_battery1_%d.png", new Object[]{Integer.valueOf(i + 1)}));
                }
                break;
            case 2:
                this.BATTERY_LEVEL = POWER_LEVLE_MODE_10;
                battery_num = new byte[this.BATTERY_LEVEL][];
                for (i = 0; i < this.BATTERY_LEVEL; i++) {
                    battery_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("datawidget/slpt/watchface_custom_fun_icon_slpt_battery2_%d.png", new Object[]{Integer.valueOf(i + 1)}));
                }
                break;
            case 3:
                this.BATTERY_LEVEL = POWER_LEVLE_MODE_10;
                battery_num = new byte[this.BATTERY_LEVEL][];
                for (i = 0; i < this.BATTERY_LEVEL; i++) {
                    battery_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("datawidget/model_3/8C/icon/watchface_custom_fun_icon_slpt_battery_%d.png", new Object[]{Integer.valueOf(i + 1)}));
                }
                break;
            case 4:
                this.BATTERY_LEVEL = POWER_LEVLE_MODE_10;
                battery_num = new byte[this.BATTERY_LEVEL][];
                for (i = 0; i < this.BATTERY_LEVEL; i++) {
                    battery_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("datawidget/model_4/icon/watchface_custom_fun_icon_slpt_battery%d.png", new Object[]{Integer.valueOf(i + 1)}));
                }
                break;
            default:
                this.BATTERY_LEVEL = POWER_LEVLE_MODE_10;
                battery_num = new byte[this.BATTERY_LEVEL][];
                for (i = 0; i < this.BATTERY_LEVEL; i++) {
                    battery_num[i] = SimpleFile.readFileFromAssets(this.mContext, String.format("datawidget/slpt/watchface_custom_fun_icon_slpt_battery%d.png", new Object[]{Integer.valueOf(i + 1)}));
                }
                break;
        }
        SlptBatteryView batteryView = new SlptBatteryView(this.BATTERY_LEVEL);
        batteryView.setImagePictureArray(battery_num);
        return batteryView;
    }

    public SlptViewComponent getProgressView() {
        byte[] circleByte;
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        byte[] circleBgByte = null;
        switch (getModel()) {
            case 2:
                circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_circle_r_2.png");
                break;
            case 3:
                if (!checkNeed26WC()) {
                    circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/8C/icon/watchface_custom_fun_icon_slpt_circle_r.png");
                    break;
                }
                circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/icon/watchface_custom_fun_icon_circle_r.png");
                break;
            default:
                circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_circle_r.png");
                circleBgByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_circle_bg.png");
                break;
        }
        if (circleBgByte != null) {
            SlptPictureView pictureView = new SlptPictureView();
            pictureView.setImagePicture(circleBgByte);
            pictureView.setStart(getX(), getY());
            pictureView.setRect(getWidth(), getHeight());
            layout.add(pictureView);
        }
        if (circleByte != null) {
            SlptPowerArcAnglePicView anglePicView = new SlptPowerArcAnglePicView();
            anglePicView.setImagePicture(circleByte);
            anglePicView.setStart(getX(), getY());
            anglePicView.setRect(getWidth(), getHeight());
            anglePicView.start_angle = 0;
            anglePicView.full_angle = 360;
            anglePicView.len_angle = 0;
            layout.add(anglePicView);
        }
        return layout;
    }

    public SlptViewComponent getWidgetView() {
        SlptViewComponent dataView = getDataView();
        SlptViewComponent iconView = getIconView();
        SlptViewComponent defaultCircleView = getProgressView();
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
            case 4:
                SlptViewComponent layout1 = new SlptLinearLayout();
                layout1.add(iconView);
                layout1.add(dataView);
                dataView.setPadding(this.dataPadLeft, 0, 0, 0);
                layout1.setStart(getX(), getY());
                layout1.setRect(getWidth(), getHeight());
                layout1.alignX = (byte) 2;
                layout1.alignY = (byte) 2;
                return layout1;
            default:
                SlptViewComponent layout = new SlptAbsoluteLayout();
                setIconPosition(iconView);
                setDataPosition(dataView);
                layout.add(dataView);
                layout.add(iconView);
                layout.add(defaultCircleView);
                return layout;
        }
    }
}
