package com.huami.watch.watchface.widget.slpt;

import android.content.Context;
import android.support.v7.recyclerview.C0051R;
import com.ingenic.iwds.slpt.view.arc.SlptTodayStepArcAnglePicView;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.sport.SlptTodayStepNumView;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;

public class SlptStepWidget extends SlptDefaultWidget {
    private Context mContext;

    public SlptStepWidget(Context context, int x, int y, int width, int height, int mode, boolean need26WC) {
        super(context, x, y, width, height, mode, need26WC);
        this.mContext = context;
    }

    public SlptViewComponent getDataView() {
        SlptTodayStepNumView stepView = new SlptTodayStepNumView();
        stepView.setImagePictureArray(getDefaultNumMem());
        return stepView;
    }

    public SlptViewComponent getIconView() {
        byte[] mem;
        SlptPictureView iconView = new SlptPictureView();
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_walk_1.png");
                break;
            case 2:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_walk_2.png");
                break;
            case 3:
                if (!checkNeed26WC()) {
                    mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/8C/icon/watchface_custom_fun_icon_slpt_walk.png");
                    break;
                }
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/icon/watchface_custom_fun_icon_walk.png");
                break;
            case 4:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_4/icon/watchface_custom_fun_icon_slpt_walk.png");
                break;
            default:
                mem = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_walk.png");
                break;
        }
        iconView.setImagePicture(mem);
        return iconView;
    }

    public SlptViewComponent getProgressView() {
        byte[] circleByte;
        SlptAbsoluteLayout layout = new SlptAbsoluteLayout();
        byte[] circleBgByte = null;
        switch (getModel()) {
            case 2:
                circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_circle_r_2.png");
                break;
            case 3:
                if (!checkNeed26WC()) {
                    circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/8C/icon/watchface_custom_fun_icon_slpt_circle_r.png");
                    break;
                }
                circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/model_3/icon/watchface_custom_fun_icon_circle_r.png");
                break;
            default:
                circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_circle_r.png");
                circleBgByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_circle_bg.png");
                break;
        }
        if (circleBgByte != null) {
            SlptPictureView pictureView = new SlptPictureView();
            pictureView.setImagePicture(circleBgByte);
            pictureView.setStart(getX(), getY());
            pictureView.setRect(getWidth(), getHeight());
            layout.add(pictureView);
        }
        if (circleByte != null) {
            SlptTodayStepArcAnglePicView anglePicView = new SlptTodayStepArcAnglePicView();
            anglePicView.setImagePicture(circleByte);
            anglePicView.setStart(getX(), getY());
            anglePicView.setRect(getWidth(), getHeight());
            anglePicView.start_angle = 0;
            anglePicView.full_angle = 360;
            anglePicView.len_angle = 0;
            layout.add(anglePicView);
        }
        return layout;
    }

    public SlptViewComponent getWidgetView() {
        SlptViewComponent dataView = getDataView();
        SlptViewComponent iconView = getIconView();
        SlptViewComponent defaultCircleView = getProgressView();
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
            case 4:
                SlptViewComponent layout1 = new SlptLinearLayout();
                layout1.add(iconView);
                layout1.add(dataView);
                dataView.setPadding(this.dataPadLeft, 0, 0, 0);
                layout1.setStart(getX(), getY());
                layout1.setRect(getWidth(), getHeight());
                layout1.alignX = (byte) 2;
                return layout1;
            default:
                SlptViewComponent layout = new SlptAbsoluteLayout();
                setIconPosition(iconView);
                setDataPosition(dataView);
                layout.add(dataView);
                layout.add(iconView);
                layout.add(defaultCircleView);
                return layout;
        }
    }

    public void setIconPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.iconPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }

    public void setDataPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.dataPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }
}
