package com.huami.watch.watchface.widget.slpt;

import android.content.Context;
import android.provider.Settings.System;
import android.support.v7.recyclerview.C0051R;
import android.text.TextUtils;
import android.util.Log;
import com.huami.watch.watchface.util.Util;
import com.ingenic.iwds.slpt.view.core.SlptAbsoluteLayout;
import com.ingenic.iwds.slpt.view.core.SlptLinearLayout;
import com.ingenic.iwds.slpt.view.core.SlptNumView;
import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import org.json.JSONObject;

public class SlptWeatherWidget extends SlptDefaultWidget {
    private String iconPath;
    private Context mContext;
    private String numPath;
    private String tempFlag;
    private String tempString;
    private final String[] weatherIconNames;
    private int weatherType;

    public SlptWeatherWidget(Context context, int x, int y, int width, int height, int model, boolean need26WC) {
        super(context, x, y, width, height, model, need26WC);
        this.weatherIconNames = new String[]{"wf_weather_sunny.png", "wf_weather_cloudy.png", "wf_weather_overcast.png", "wf_weather_fog.png", "wf_weather_smog.png", "wf_weather_shower.png", "wf_weather_thunder_shower.png", "wf_weather_light_rain.png", "wf_weather_moderate_rain.png", "wf_weather_heavy_rain.png", "wf_weather_rainstorm.png", "wf_weather_torrential_rain.png", "wf_weather_sleet.png", "wf_weather_freezing_rain.png", "wf_weather_hail.png", "wf_weather_light_snow.png", "wf_weather_moderate_snow.png", "wf_weather_heavy_snow.png", "wf_weather_snowstorm.png", "wf_weather_dust.png", "wf_weather_blowing_sand.png", "wf_weather_sand_storm.png", "wf_weather_unknown.png"};
        this.tempString = "--";
        this.weatherType = this.weatherIconNames.length - 1;
        this.iconPath = "";
        this.numPath = "";
        this.mContext = context;
        getWeatherData();
    }

    public SlptWeatherWidget(Context context, String iconPath, String numPath) {
        super(context, 0, 0, 0, 0, -1, numPath);
        this.weatherIconNames = new String[]{"wf_weather_sunny.png", "wf_weather_cloudy.png", "wf_weather_overcast.png", "wf_weather_fog.png", "wf_weather_smog.png", "wf_weather_shower.png", "wf_weather_thunder_shower.png", "wf_weather_light_rain.png", "wf_weather_moderate_rain.png", "wf_weather_heavy_rain.png", "wf_weather_rainstorm.png", "wf_weather_torrential_rain.png", "wf_weather_sleet.png", "wf_weather_freezing_rain.png", "wf_weather_hail.png", "wf_weather_light_snow.png", "wf_weather_moderate_snow.png", "wf_weather_heavy_snow.png", "wf_weather_snowstorm.png", "wf_weather_dust.png", "wf_weather_blowing_sand.png", "wf_weather_sand_storm.png", "wf_weather_unknown.png"};
        this.tempString = "--";
        this.weatherType = this.weatherIconNames.length - 1;
        this.iconPath = "";
        this.numPath = "";
        this.mContext = context;
        this.iconPath = iconPath;
        this.numPath = numPath;
        getWeatherData();
    }

    public void setIconPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.iconPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }

    public void setDataPosition(SlptViewComponent view) {
        view.setStart(getX(), getY() + this.dataPadTop);
        view.setRect(getWidth(), getHeight());
        view.alignX = (byte) 2;
    }

    public SlptViewComponent getDataView() {
        String mIconPath;
        SlptLinearLayout layout = new SlptLinearLayout();
        switch (getModel()) {
            case -1:
                mIconPath = this.numPath;
                break;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                mIconPath = "datawidget/font/02/";
                break;
            case 2:
                mIconPath = "datawidget/font/03/8c/";
                break;
            case 3:
                mIconPath = "datawidget/model_3/8C/num/";
                break;
            case 4:
                mIconPath = "datawidget/model_4/font/";
                break;
            default:
                mIconPath = "datawidget/font/01/";
                break;
        }
        for (char c : this.tempString.toCharArray()) {
            if ("-".equals(String.valueOf(c))) {
                layout.add(Util.getCharView(this.mContext, mIconPath + "minus.png"));
            } else if ("F".equals(String.valueOf(c))) {
                layout.add(Util.getCharView(this.mContext, mIconPath + "fahrenheit.png"));
            } else if ("C".equals(String.valueOf(c))) {
                layout.add(Util.getCharView(this.mContext, mIconPath + "celsius.png"));
            } else if ("/".equals(String.valueOf(c))) {
                layout.add(Util.getCharView(this.mContext, mIconPath + "separator.png"));
            } else if ("0123456789".contains(String.valueOf(c))) {
                SlptNumView numView = Util.getDefaultNumView(this.mContext, String.valueOf(c));
                numView.setImagePictureArray(getDefaultNumMem());
                layout.add(numView);
            }
        }
        return layout;
    }

    public SlptViewComponent getIconView() {
        String mIconPath;
        switch (getModel()) {
            case -1:
                mIconPath = this.iconPath;
                break;
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                mIconPath = "datawidget/weather/01/8C/";
                break;
            case 2:
                mIconPath = "datawidget/weather/02/8C/";
                break;
            case 3:
                mIconPath = "datawidget/weather/03/8C/";
                break;
            case 4:
                mIconPath = "datawidget/model_4/weather/8C/";
                break;
            default:
                mIconPath = "datawidget/weather/00/8C/";
                break;
        }
        byte[] iconMem = SimpleFile.readFileFromAssets(this.mContext, mIconPath + this.weatherIconNames[this.weatherType]);
        SlptPictureView view = new SlptPictureView();
        view.setImagePicture(iconMem);
        return view;
    }

    public SlptViewComponent getProgressView() {
        byte[] circleByte = null;
        switch (getModel()) {
            case 2:
            case 3:
            case 4:
                break;
            default:
                circleByte = SimpleFile.readFileFromAssets(this.mContext, "datawidget/slpt/watchface_custom_fun_icon_slpt_circle_bg.png");
                break;
        }
        if (circleByte == null) {
            return null;
        }
        SlptViewComponent pictureView = new SlptPictureView();
        pictureView.setImagePicture(circleByte);
        pictureView.setStart(getX(), getY());
        pictureView.setRect(getWidth(), getHeight());
        return pictureView;
    }

    public SlptViewComponent getWidgetView() {
        SlptViewComponent dataView = getDataView();
        SlptViewComponent iconView = getIconView();
        SlptViewComponent processView = getIconView();
        SlptViewComponent defaultCircleView = getProgressView();
        switch (getModel()) {
            case C0051R.styleable.RecyclerView_layoutManager /*1*/:
            case 4:
                SlptViewComponent layout1 = new SlptLinearLayout();
                layout1.add(iconView);
                layout1.add(dataView);
                dataView.setPadding(this.dataPadLeft, 0, 0, 0);
                layout1.setStart(getX(), getY());
                layout1.setRect(getWidth(), getHeight());
                layout1.alignX = (byte) 2;
                layout1.alignY = (byte) 2;
                return layout1;
            default:
                SlptViewComponent layout = new SlptAbsoluteLayout();
                setIconPosition(iconView);
                setDataPosition(dataView);
                if (dataView != null) {
                    layout.add(dataView);
                }
                if (iconView != null) {
                    layout.add(iconView);
                }
                if (defaultCircleView != null) {
                    layout.add(defaultCircleView);
                }
                return layout;
        }
    }

    private void getWeatherData() {
        String weatherData = System.getString(this.mContext.getContentResolver(), "WeatherCheckedSummary");
        if (weatherData != null) {
            try {
                JSONObject weatherJson = new JSONObject(weatherData);
                String tempFlag = weatherJson.getString("tempUnit");
                String tempString = weatherJson.getString("temp");
                int weatherType = weatherJson.getInt("weatherCodeFrom");
                this.tempFlag = tempFlag;
                this.tempString = tempString;
                this.weatherType = weatherType;
                if (this.weatherType < 0) {
                    this.weatherType = this.weatherIconNames.length - 1;
                }
                if (TextUtils.isEmpty(tempString)) {
                    this.tempString = "--";
                    this.weatherType = this.weatherIconNames.length - 1;
                } else {
                    String unit;
                    if (getModel() == 0 && tempString.indexOf(47) > 0) {
                        unit = "";
                    } else if (TextUtils.isEmpty(tempFlag) || tempFlag.equals("1")) {
                        unit = "C";
                    } else {
                        unit = "F";
                    }
                    this.tempString = tempString + unit;
                }
                Log.i("slptWeatherWidget", "  handle weather change " + this.tempString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
