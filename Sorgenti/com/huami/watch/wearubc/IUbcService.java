package com.huami.watch.wearubc;

import android.content.ContentValues;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.v7.recyclerview.C0051R;

public interface IUbcService extends IInterface {

    public static abstract class Stub extends Binder implements IUbcService {

        private static class Proxy implements IUbcService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void startSync(int count) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.wearubc.IUbcService");
                    _data.writeInt(count);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void insert(ContentValues values) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.huami.watch.wearubc.IUbcService");
                    if (values != null) {
                        _data.writeInt(1);
                        values.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public static IUbcService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.huami.watch.wearubc.IUbcService");
            if (iin == null || !(iin instanceof IUbcService)) {
                return new Proxy(obj);
            }
            return (IUbcService) iin;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    data.enforceInterface("com.huami.watch.wearubc.IUbcService");
                    startSync(data.readInt());
                    reply.writeNoException();
                    return true;
                case 2:
                    ContentValues _arg0;
                    data.enforceInterface("com.huami.watch.wearubc.IUbcService");
                    if (data.readInt() != 0) {
                        _arg0 = (ContentValues) ContentValues.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    insert(_arg0);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.huami.watch.wearubc.IUbcService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void insert(ContentValues contentValues) throws RemoteException;

    void startSync(int i) throws RemoteException;
}
