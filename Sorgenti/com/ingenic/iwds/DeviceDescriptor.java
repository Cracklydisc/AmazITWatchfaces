package com.ingenic.iwds;

import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.ArrayList;
import java.util.HashMap;

public class DeviceDescriptor implements Parcelable {
    public static final Creator<DeviceDescriptor> CREATOR = new C03191();
    private static HashMap<Integer, ArrayList<Integer>> f41a = new HashMap();
    public int androidApiLevel;
    public String devAddress;
    public int devClass;
    public int devSubClass;
    public String displayID;
    public String extraInfo;
    public int iwdsVersion;
    public String lcdExterior;
    public String lcdSize;
    public String linkTag;
    public String manufacture;
    public String model;
    public String serialNo;

    static class C03191 implements Creator<DeviceDescriptor> {
        C03191() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m0a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m1a(i);
        }

        public DeviceDescriptor m0a(Parcel parcel) {
            DeviceDescriptor deviceDescriptor = new DeviceDescriptor(parcel.readString(), parcel.readString(), parcel.readInt(), parcel.readInt());
            deviceDescriptor.model = parcel.readString();
            deviceDescriptor.manufacture = parcel.readString();
            deviceDescriptor.serialNo = parcel.readString();
            deviceDescriptor.displayID = parcel.readString();
            deviceDescriptor.androidApiLevel = parcel.readInt();
            deviceDescriptor.iwdsVersion = parcel.readInt();
            deviceDescriptor.lcdExterior = parcel.readString();
            deviceDescriptor.lcdSize = parcel.readString();
            deviceDescriptor.extraInfo = parcel.readString();
            return deviceDescriptor;
        }

        public DeviceDescriptor[] m1a(int i) {
            return new DeviceDescriptor[i];
        }
    }

    static {
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf(1));
        arrayList.add(Integer.valueOf(2));
        f41a.put(Integer.valueOf(0), arrayList);
        f41a.put(Integer.valueOf(1), new ArrayList());
        arrayList = new ArrayList();
        arrayList.add(Integer.valueOf(1));
        f41a.put(Integer.valueOf(2), arrayList);
    }

    public DeviceDescriptor(String deviceAddress, String devicelinkTag, int deviceClass, int deviceSubClass) {
        boolean z = true;
        boolean z2 = deviceAddress == null || deviceAddress.isEmpty();
        IwdsAssert.dieIf((Object) this, z2, "Device devAddress is null or empty.");
        if (f41a.containsKey(Integer.valueOf(deviceClass))) {
            z2 = false;
        } else {
            z2 = true;
        }
        IwdsAssert.dieIf((Object) this, z2, "Unknown device class, class code: " + deviceClass);
        if (((ArrayList) f41a.get(Integer.valueOf(deviceClass))).contains(Integer.valueOf(deviceSubClass))) {
            z = false;
        }
        IwdsAssert.dieIf((Object) this, z, "Unknown sub device class, subclass code: " + deviceSubClass);
        this.lcdExterior = HardwareList.getHardwareValue("lcd.exterior", "unknown");
        this.lcdSize = HardwareList.getHardwareValue("lcd.size", "unknown");
        this.devAddress = deviceAddress;
        this.linkTag = devicelinkTag;
        this.devClass = deviceClass;
        this.devSubClass = deviceSubClass;
        this.model = BuildOptions.MODEL;
        this.manufacture = BuildOptions.MANUFACTURER;
        this.serialNo = BuildOptions.SERIAL;
        this.displayID = BuildOptions.DISPLAY;
        this.androidApiLevel = VERSION.SDK_INT;
        this.iwdsVersion = 10001;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.devAddress);
        dest.writeString(this.linkTag);
        dest.writeInt(this.devClass);
        dest.writeInt(this.devSubClass);
        dest.writeString(this.model);
        dest.writeString(this.manufacture);
        dest.writeString(this.serialNo);
        dest.writeString(this.displayID);
        dest.writeInt(this.androidApiLevel);
        dest.writeInt(this.iwdsVersion);
        dest.writeString(this.lcdExterior);
        dest.writeString(this.lcdSize);
        dest.writeString(this.extraInfo);
    }

    public String toString() {
        return "DeviceDescriptor [devAddress=" + this.devAddress + ", linkTag=" + this.linkTag + ", devClass=" + this.devClass + ", devSubClass=" + this.devSubClass + ", model=" + this.model + ", manufacture=" + this.manufacture + ", serialNo=" + this.serialNo + ", androidApiLevel=" + this.androidApiLevel + ", iwdsVersion=" + this.iwdsVersion + ", displayid=" + this.displayID + ", extraInfo : " + this.extraInfo + "]";
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.serialNo == null ? 0 : this.serialNo.hashCode()) + (((this.model == null ? 0 : this.model.hashCode()) + (((this.manufacture == null ? 0 : this.manufacture.hashCode()) + (((this.linkTag == null ? 0 : this.linkTag.hashCode()) + (((((((((this.devAddress == null ? 0 : this.devAddress.hashCode()) + ((this.androidApiLevel + 31) * 31)) * 31) + this.devClass) * 31) + this.devSubClass) * 31) + this.iwdsVersion) * 31)) * 31)) * 31)) * 31)) * 31;
        if (this.displayID != null) {
            i = this.displayID.hashCode();
        }
        return hashCode + i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof DeviceDescriptor)) {
            return false;
        }
        DeviceDescriptor deviceDescriptor = (DeviceDescriptor) obj;
        if (this.androidApiLevel != deviceDescriptor.androidApiLevel) {
            return false;
        }
        if (this.devAddress == null) {
            if (deviceDescriptor.devAddress != null) {
                return false;
            }
        } else if (!this.devAddress.equals(deviceDescriptor.devAddress)) {
            return false;
        }
        if (this.devClass != deviceDescriptor.devClass) {
            return false;
        }
        if (this.devSubClass != deviceDescriptor.devSubClass) {
            return false;
        }
        if (this.iwdsVersion != deviceDescriptor.iwdsVersion) {
            return false;
        }
        if (this.linkTag == null) {
            if (deviceDescriptor.linkTag != null) {
                return false;
            }
        } else if (!this.linkTag.equals(deviceDescriptor.linkTag)) {
            return false;
        }
        if (this.manufacture == null) {
            if (deviceDescriptor.manufacture != null) {
                return false;
            }
        } else if (!this.manufacture.equals(deviceDescriptor.manufacture)) {
            return false;
        }
        if (this.model == null) {
            if (deviceDescriptor.model != null) {
                return false;
            }
        } else if (!this.model.equals(deviceDescriptor.model)) {
            return false;
        }
        if (this.serialNo == null) {
            if (deviceDescriptor.serialNo != null) {
                return false;
            }
        } else if (!this.serialNo.equals(deviceDescriptor.serialNo)) {
            return false;
        }
        if (this.displayID == null) {
            if (deviceDescriptor.displayID != null) {
                return false;
            }
            return true;
        } else if (this.displayID.equals(deviceDescriptor.displayID)) {
            return true;
        } else {
            return false;
        }
    }
}
