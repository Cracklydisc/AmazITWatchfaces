package com.ingenic.iwds.content;

import android.content.ClipData;
import android.content.ComponentName;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.os.RemoteBundle;
import java.util.HashSet;
import java.util.Set;

public class RemoteIntent implements Parcelable, Cloneable {
    public static final Creator<RemoteIntent> CREATOR = new C03211();
    private String f45a;
    private Uri f46b;
    private String f47c;
    private int f48d;
    private String f49e;
    private ComponentName f50f;
    private Rect f51g;
    private Set<String> f52h;
    private RemoteIntent f53i;
    private ClipData f54j;
    private RemoteBundle f55k;

    static class C03211 implements Creator<RemoteIntent> {
        C03211() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m4a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m5a(i);
        }

        public RemoteIntent m4a(Parcel parcel) {
            return new RemoteIntent(parcel);
        }

        public RemoteIntent[] m5a(int i) {
            return new RemoteIntent[i];
        }
    }

    public RemoteIntent(RemoteIntent in) {
        this.f45a = in.f45a;
        this.f46b = in.f46b;
        this.f47c = in.f47c;
        this.f48d = in.f48d;
        this.f49e = in.f49e;
        this.f50f = in.f50f;
        if (in.f51g != null) {
            this.f51g = new Rect(in.f51g);
        }
        if (in.f52h != null) {
            this.f52h = new HashSet(in.f52h);
        }
        if (in.f53i != null) {
            this.f53i = new RemoteIntent(in.f53i);
        }
        if (in.f54j != null) {
            this.f54j = new ClipData(in.f54j);
        }
        if (in.f55k != null) {
            this.f55k = new RemoteBundle(in.f55k);
        }
    }

    protected RemoteIntent(Parcel source) {
        this.f45a = source.readString();
        this.f46b = (Uri) Uri.CREATOR.createFromParcel(source);
        this.f47c = source.readString();
        this.f48d = source.readInt();
        this.f49e = source.readString();
        this.f50f = ComponentName.readFromParcel(source);
        if (source.readInt() != 0) {
            this.f51g = (Rect) Rect.CREATOR.createFromParcel(source);
        }
        int readInt = source.readInt();
        if (readInt > 0) {
            this.f52h = new HashSet();
            for (int i = 0; i < readInt; i++) {
                this.f52h.add(source.readString().intern());
            }
        } else {
            this.f52h = null;
        }
        if (source.readInt() != 0) {
            this.f53i = (RemoteIntent) CREATOR.createFromParcel(source);
        }
        if (source.readInt() != 0) {
            this.f54j = (ClipData) ClipData.CREATOR.createFromParcel(source);
        }
        if (source.readInt() != 0) {
            this.f55k = (RemoteBundle) RemoteBundle.CREATOR.createFromParcel(source);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.f45a);
        Uri.writeToParcel(dest, this.f46b);
        dest.writeString(this.f47c);
        dest.writeInt(this.f48d);
        dest.writeString(this.f49e);
        ComponentName.writeToParcel(this.f50f, dest);
        if (this.f51g != null) {
            dest.writeInt(1);
            this.f51g.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        if (this.f52h != null) {
            dest.writeInt(this.f52h.size());
            for (String writeString : this.f52h) {
                dest.writeString(writeString);
            }
        } else {
            dest.writeInt(0);
        }
        if (this.f53i != null) {
            dest.writeInt(1);
            this.f53i.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        if (this.f54j != null) {
            dest.writeInt(1);
            this.f54j.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        if (this.f55k != null) {
            dest.writeInt(1);
            this.f55k.writeToParcel(dest, flags);
            return;
        }
        dest.writeInt(0);
    }

    public Object clone() {
        return new RemoteIntent(this);
    }
}
