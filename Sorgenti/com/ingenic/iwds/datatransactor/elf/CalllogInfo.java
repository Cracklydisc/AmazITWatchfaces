package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class CalllogInfo implements Parcelable {
    public static final Creator<CalllogInfo> CREATOR = new C03221();
    private int _id;
    private long date;
    private int duration;
    private int is_read;
    private String name;
    private int news;
    private String number;
    private int type;

    static class C03221 implements Creator<CalllogInfo> {
        C03221() {
        }

        public CalllogInfo createFromParcel(Parcel source) {
            CalllogInfo calllogInfo = new CalllogInfo();
            calllogInfo._id = source.readInt();
            calllogInfo.number = source.readString();
            calllogInfo.date = source.readLong();
            calllogInfo.duration = source.readInt();
            calllogInfo.type = source.readInt();
            calllogInfo.news = source.readInt();
            calllogInfo.name = source.readString();
            calllogInfo.is_read = source.readInt();
            return calllogInfo;
        }

        public CalllogInfo[] newArray(int size) {
            return new CalllogInfo[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this._id);
        dest.writeString(this.number);
        dest.writeLong(this.date);
        dest.writeInt(this.duration);
        dest.writeInt(this.type);
        dest.writeInt(this.news);
        dest.writeString(this.name);
        dest.writeInt(this.is_read);
    }

    public String toString() {
        return "Calllog info _id:" + this._id + ",number:" + this.number + ",date:" + this.date + ",duration:" + this.duration + ",type:" + this.type + ",new:" + this.news + ",name:" + this.name + ",is_read:" + this.is_read;
    }
}
