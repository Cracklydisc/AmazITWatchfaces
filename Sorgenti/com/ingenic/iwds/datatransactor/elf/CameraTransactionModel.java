package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class CameraTransactionModel {

    public static class TakePictureDone implements Parcelable {
        public static final Creator<TakePictureDone> CREATOR = new C03241();
        public CameraFrameInfo frame;

        static class C03241 implements Creator<TakePictureDone> {
            C03241() {
            }

            public TakePictureDone createFromParcel(Parcel source) {
                TakePictureDone takePictureDone = new TakePictureDone();
                takePictureDone.frame = (CameraFrameInfo) CameraFrameInfo.CREATOR.createFromParcel(source);
                return takePictureDone;
            }

            public TakePictureDone[] newArray(int size) {
                return new TakePictureDone[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            this.frame.writeToParcel(dest, flags);
        }
    }
}
