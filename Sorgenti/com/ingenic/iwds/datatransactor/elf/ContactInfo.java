package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class ContactInfo implements Parcelable {
    public static final Creator<ContactInfo> CREATOR = new C03251();
    public List<EmailInfo> email = new ArrayList();
    public String name = "";
    public int operation = -1;
    public List<PhoneInfo> phoneList = new ArrayList();
    public String raw_id = "";

    static class C03251 implements Creator<ContactInfo> {
        C03251() {
        }

        public ContactInfo createFromParcel(Parcel source) {
            ContactInfo contactInfo = new ContactInfo();
            contactInfo.operation = source.readInt();
            contactInfo.raw_id = source.readString();
            contactInfo.name = source.readString();
            contactInfo.phoneList = source.readArrayList(PhoneInfo.class.getClassLoader());
            contactInfo.email = source.readArrayList(EmailInfo.class.getClassLoader());
            return contactInfo;
        }

        public ContactInfo[] newArray(int size) {
            return new ContactInfo[size];
        }
    }

    public static class EmailInfo implements Parcelable {
        public static final Creator<EmailInfo> CREATOR = new C03261();
        public String email;
        public int type;

        static class C03261 implements Creator<EmailInfo> {
            C03261() {
            }

            public EmailInfo createFromParcel(Parcel source) {
                EmailInfo emailInfo = new EmailInfo();
                emailInfo.type = source.readInt();
                emailInfo.email = source.readString();
                return emailInfo;
            }

            public EmailInfo[] newArray(int size) {
                return new EmailInfo[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.type);
            dest.writeString(this.email);
        }
    }

    public static class PhoneInfo implements Parcelable {
        public static final Creator<PhoneInfo> CREATOR = new C03271();
        public String number;
        public int type;

        static class C03271 implements Creator<PhoneInfo> {
            C03271() {
            }

            public PhoneInfo createFromParcel(Parcel source) {
                PhoneInfo phoneInfo = new PhoneInfo();
                phoneInfo.type = source.readInt();
                phoneInfo.number = source.readString();
                return phoneInfo;
            }

            public PhoneInfo[] newArray(int size) {
                return new PhoneInfo[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.type);
            dest.writeString(this.number);
        }
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "{operation: " + this.operation + " raw_id: " + this.raw_id + " name: " + this.name + ", phoneList: " + this.phoneList + ", email: " + this.email + "}";
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.operation);
        dest.writeString(this.raw_id);
        dest.writeString(this.name);
        dest.writeList(this.phoneList);
        dest.writeList(this.email);
    }
}
