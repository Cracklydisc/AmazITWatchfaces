package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.Arrays;

public class HealthInfo implements Parcelable {
    public static final Creator<HealthInfo> CREATOR = new C03281();
    public String altitude;
    public String[] days;
    public long deepSleepTime;
    public int humidity;
    public long lightSleepTime;
    public long nowDate;
    public int pressure;
    public String rates;
    public Record[] record;
    public long recordDate;
    public int sleepQuality;
    public int sleepRecordCount;
    public long sleepTime;
    public int temp;
    public int uitravioletIntensity;
    public String[] weeks;

    static class C03281 implements Creator<HealthInfo> {
        C03281() {
        }

        public HealthInfo createFromParcel(Parcel source) {
            int readInt = source.readInt();
            HealthInfo healthInfo = new HealthInfo(readInt);
            source.readStringArray(healthInfo.days);
            source.readStringArray(healthInfo.weeks);
            healthInfo.rates = source.readString();
            healthInfo.nowDate = source.readLong();
            healthInfo.temp = source.readInt();
            healthInfo.humidity = source.readInt();
            healthInfo.pressure = source.readInt();
            healthInfo.recordDate = source.readLong();
            healthInfo.sleepTime = source.readLong();
            healthInfo.deepSleepTime = source.readLong();
            healthInfo.lightSleepTime = source.readLong();
            healthInfo.sleepQuality = source.readInt();
            healthInfo.sleepRecordCount = readInt;
            if (healthInfo.record != null) {
                source.readTypedArray(healthInfo.record, Record.CREATOR);
            }
            healthInfo.uitravioletIntensity = source.readInt();
            healthInfo.altitude = source.readString();
            return healthInfo;
        }

        public HealthInfo[] newArray(int size) {
            return new HealthInfo[size];
        }
    }

    public static class Record implements Parcelable {
        public static final Creator<Record> CREATOR = new C03291();
        public long endTime;
        public long startTime;
        public int type;

        static class C03291 implements Creator<Record> {
            C03291() {
            }

            public Record createFromParcel(Parcel source) {
                Record record = new Record();
                record.startTime = source.readLong();
                record.endTime = source.readLong();
                record.type = source.readInt();
                return record;
            }

            public Record[] newArray(int size) {
                return new Record[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.startTime);
            dest.writeLong(this.endTime);
            dest.writeInt(this.type);
        }

        public String toString() {
            return "Record [startTime=" + this.startTime + ", endTime=" + this.endTime + ", type=" + this.type + "]";
        }
    }

    public HealthInfo() {
        this.sleepRecordCount = 0;
        this.days = new String[13];
        this.weeks = new String[7];
    }

    public HealthInfo(int count) {
        boolean z = false;
        this.sleepRecordCount = 0;
        if (count < 0) {
            z = true;
        }
        IwdsAssert.dieIf((Object) this, z, "monitor size < 0.");
        this.sleepRecordCount = count;
        if (count != 0) {
            this.record = new Record[count];
        }
        this.days = new String[13];
        this.weeks = new String[7];
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        if (this.sleepRecordCount > 0) {
            dest.writeInt(this.sleepRecordCount);
        } else {
            dest.writeInt(0);
        }
        dest.writeStringArray(this.days);
        dest.writeStringArray(this.weeks);
        dest.writeString(this.rates);
        dest.writeLong(this.nowDate);
        dest.writeInt(this.temp);
        dest.writeInt(this.humidity);
        dest.writeInt(this.pressure);
        dest.writeLong(this.recordDate);
        dest.writeLong(this.sleepTime);
        dest.writeLong(this.deepSleepTime);
        dest.writeLong(this.lightSleepTime);
        dest.writeInt(this.sleepQuality);
        if (this.record != null) {
            dest.writeTypedArray(this.record, flags);
        }
        dest.writeInt(this.uitravioletIntensity);
        dest.writeString(this.altitude);
    }

    public String toString() {
        return "HealthInfo [days=" + Arrays.toString(this.days) + ", weeks=" + Arrays.toString(this.weeks) + ", rates=[" + this.rates + "], nowDate =" + this.nowDate + ", temp=" + this.temp + ", humidity=" + this.humidity + ", pressure=" + this.pressure + ", recordDate=" + this.recordDate + ", sleepTime=" + this.sleepTime + ", deepSleepTime=" + this.deepSleepTime + ", lightSleepTime =" + this.lightSleepTime + ", sleepQuality=" + this.sleepQuality + ", sleepRecordCount=" + this.sleepRecordCount + ", record=" + Arrays.toString(this.record) + "]";
    }
}
