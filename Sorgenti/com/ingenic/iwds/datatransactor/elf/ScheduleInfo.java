package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.Arrays;

public class ScheduleInfo implements Parcelable {
    public static final Creator<ScheduleInfo> CREATOR = new C03331();
    public Event[] event;
    public int eventCount;

    static class C03331 implements Creator<ScheduleInfo> {
        C03331() {
        }

        public ScheduleInfo createFromParcel(Parcel source) {
            int readInt = source.readInt();
            ScheduleInfo scheduleInfo = new ScheduleInfo(readInt);
            scheduleInfo.eventCount = readInt;
            if (scheduleInfo.event != null) {
                source.readTypedArray(scheduleInfo.event, Event.CREATOR);
            }
            return scheduleInfo;
        }

        public ScheduleInfo[] newArray(int size) {
            return new ScheduleInfo[size];
        }
    }

    public static class Event implements Parcelable {
        public static final Creator<Event> CREATOR = new C03341();
        public int allDay = 0;
        public String description = null;
        public long dtEnd = 0;
        public long dtStart = 0;
        public String duration = null;
        public String eventLocation = null;
        public String eventTimezone = null;
        public int hasAlarm = 0;
        public long id = 0;
        public Reminder reminder = null;
        public String rrule = null;
        public String title = null;

        static class C03341 implements Creator<Event> {
            C03341() {
            }

            public Event createFromParcel(Parcel source) {
                Event event = new Event();
                event.id = source.readLong();
                event.title = source.readString();
                event.eventLocation = source.readString();
                event.description = source.readString();
                event.dtStart = source.readLong();
                event.dtEnd = source.readLong();
                event.eventTimezone = source.readString();
                event.duration = source.readString();
                event.allDay = source.readInt();
                event.hasAlarm = source.readInt();
                event.rrule = source.readString();
                event.reminder = (Reminder) source.readParcelable(Reminder.class.getClassLoader());
                return event;
            }

            public Event[] newArray(int size) {
                return new Event[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.id);
            dest.writeString(this.title);
            dest.writeString(this.eventLocation);
            dest.writeString(this.description);
            dest.writeLong(this.dtStart);
            dest.writeLong(this.dtEnd);
            dest.writeString(this.eventTimezone);
            dest.writeString(this.duration);
            dest.writeInt(this.allDay);
            dest.writeInt(this.hasAlarm);
            dest.writeString(this.rrule);
            dest.writeParcelable(this.reminder, flags);
        }

        public String toString() {
            return "Event [id=" + this.id + ", title=" + this.title + ", eventLocation=" + this.eventLocation + ", description=" + this.description + ", dtStart=" + this.dtStart + ", dtEnd=" + this.dtEnd + ", eventTimezone=" + this.eventTimezone + ", duration=" + this.duration + ", allDay=" + this.allDay + ", hasAlarm=" + this.hasAlarm + ", rrule=" + this.rrule + ", reminder=" + this.reminder + "]";
        }
    }

    public static class Reminder implements Parcelable {
        public static final Creator<Reminder> CREATOR = new C03351();
        public long eventId;
        public long id;
        public long method;
        public long minutes;

        static class C03351 implements Creator<Reminder> {
            C03351() {
            }

            public Reminder createFromParcel(Parcel source) {
                Reminder reminder = new Reminder();
                reminder.id = source.readLong();
                reminder.eventId = source.readLong();
                reminder.minutes = source.readLong();
                reminder.method = source.readLong();
                return reminder;
            }

            public Reminder[] newArray(int size) {
                return new Reminder[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.id);
            dest.writeLong(this.eventId);
            dest.writeLong(this.minutes);
            dest.writeLong(this.method);
        }

        public String toString() {
            return "Reminder [id=" + this.id + ", eventId=" + this.eventId + ", minutes=" + this.minutes + ", method=" + this.method + "]";
        }
    }

    public ScheduleInfo(int count) {
        IwdsAssert.dieIf((Object) this, count < 0, "Event count < 0.");
        this.eventCount = count;
        if (count != 0) {
            this.event = new Event[this.eventCount];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.eventCount);
        if (this.event != null) {
            dest.writeTypedArray(this.event, flags);
        }
    }

    public String toString() {
        return "ScheduleInfo [event=" + Arrays.toString(this.event) + "]";
    }
}
