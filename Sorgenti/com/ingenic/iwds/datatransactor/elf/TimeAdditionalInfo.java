package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class TimeAdditionalInfo implements Parcelable {
    public static final Creator<TimeAdditionalInfo> CREATOR = new C03381();
    public String sunrise;
    public String sunset;

    static class C03381 implements Creator<TimeAdditionalInfo> {
        C03381() {
        }

        public TimeAdditionalInfo createFromParcel(Parcel source) {
            TimeAdditionalInfo timeAdditionalInfo = new TimeAdditionalInfo();
            timeAdditionalInfo.sunrise = source.readString();
            timeAdditionalInfo.sunset = source.readString();
            return timeAdditionalInfo;
        }

        public TimeAdditionalInfo[] newArray(int size) {
            return new TimeAdditionalInfo[size];
        }
    }

    private TimeAdditionalInfo() {
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sunrise);
        dest.writeString(this.sunset);
    }

    public String toString() {
        return "TimeAdditionalInfo [sunrise = " + this.sunrise + " sunset = " + this.sunset + "]";
    }

    public int describeContents() {
        return 0;
    }
}
