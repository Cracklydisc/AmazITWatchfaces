package com.ingenic.iwds.datatransactor.elf;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.Arrays;

public class WatchFaceInfo implements Parcelable {
    public static final Creator<WatchFaceInfo> CREATOR = new C03391();
    public AnalogStyleWatchFace analogWatchFace;
    public Background background;
    public DigitalStyleWatchFace digitalWatchFace;
    public String name;

    static class C03391 implements Creator<WatchFaceInfo> {
        C03391() {
        }

        public WatchFaceInfo createFromParcel(Parcel source) {
            WatchFaceInfo watchFaceInfo = new WatchFaceInfo();
            watchFaceInfo.name = source.readString();
            watchFaceInfo.background = (Background) source.readParcelable(Background.class.getClassLoader());
            watchFaceInfo.digitalWatchFace = (DigitalStyleWatchFace) source.readParcelable(DigitalStyleWatchFace.class.getClassLoader());
            watchFaceInfo.analogWatchFace = (AnalogStyleWatchFace) source.readParcelable(AnalogStyleWatchFace.class.getClassLoader());
            return watchFaceInfo;
        }

        public WatchFaceInfo[] newArray(int size) {
            return new WatchFaceInfo[size];
        }
    }

    public static class AnalogStyleWatchFace implements Parcelable {
        public static final Creator<AnalogStyleWatchFace> CREATOR = new C03401();
        public InnerWatchFace ampm;
        public TypefaceProfile dateDisplayProfile;
        public Picture hourPointer;
        public Picture minutePointer;
        public InnerWatchFace month;
        public Picture secondPointer;
        public int updateInterval = 1000;
        public InnerWatchFace week;

        static class C03401 implements Creator<AnalogStyleWatchFace> {
            C03401() {
            }

            public AnalogStyleWatchFace createFromParcel(Parcel source) {
                AnalogStyleWatchFace analogStyleWatchFace = new AnalogStyleWatchFace();
                analogStyleWatchFace.updateInterval = source.readInt();
                analogStyleWatchFace.hourPointer = (Picture) source.readParcelable(Picture.class.getClassLoader());
                analogStyleWatchFace.minutePointer = (Picture) source.readParcelable(Picture.class.getClassLoader());
                analogStyleWatchFace.secondPointer = (Picture) source.readParcelable(Picture.class.getClassLoader());
                analogStyleWatchFace.month = (InnerWatchFace) source.readParcelable(InnerWatchFace.class.getClassLoader());
                analogStyleWatchFace.week = (InnerWatchFace) source.readParcelable(InnerWatchFace.class.getClassLoader());
                analogStyleWatchFace.ampm = (InnerWatchFace) source.readParcelable(InnerWatchFace.class.getClassLoader());
                analogStyleWatchFace.dateDisplayProfile = (TypefaceProfile) source.readParcelable(TypefaceProfile.class.getClassLoader());
                return analogStyleWatchFace;
            }

            public AnalogStyleWatchFace[] newArray(int size) {
                return new AnalogStyleWatchFace[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.updateInterval);
            dest.writeParcelable(this.hourPointer, flags);
            dest.writeParcelable(this.minutePointer, flags);
            dest.writeParcelable(this.secondPointer, flags);
            dest.writeParcelable(this.month, flags);
            dest.writeParcelable(this.week, flags);
            dest.writeParcelable(this.ampm, flags);
            dest.writeParcelable(this.dateDisplayProfile, flags);
        }

        public String toString() {
            return "AnalogStyleWatchFace [updateInterval=" + this.updateInterval + ", hourPointer=" + this.hourPointer + ", minutePointer=" + this.minutePointer + ", secondPointer=" + this.secondPointer + ", month=" + this.month + ", week=" + this.week + ", ampm=" + this.ampm + ", dateDisplayProfile=" + this.dateDisplayProfile + "]";
        }
    }

    public static class Background implements Parcelable {
        public static final Creator<Background> CREATOR = new C03411();
        public int color = -1;
        public Picture picture;

        static class C03411 implements Creator<Background> {
            C03411() {
            }

            public Background createFromParcel(Parcel source) {
                Background background = new Background();
                background.color = source.readInt();
                background.picture = (Picture) source.readParcelable(Picture.class.getClassLoader());
                return background;
            }

            public Background[] newArray(int size) {
                return new Background[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.color);
            dest.writeParcelable(this.picture, flags);
        }

        public String toString() {
            return "Background [color=" + this.color + ", picture=" + this.picture + "]";
        }
    }

    public static class DigitalStyleWatchFace implements Parcelable {
        public static final Creator<DigitalStyleWatchFace> CREATOR = new C03421();
        public TextDisplayProfile dateDateDisplayProfile;
        public TextDisplayProfile hourDisplayProfile;
        public TextDisplayProfile minuteDisplayProfile;
        public TextDisplayProfile separatorDisplayProfile;
        public TextDisplayProfile traditionalChineseCalenderDisplayProfile;

        static class C03421 implements Creator<DigitalStyleWatchFace> {
            C03421() {
            }

            public DigitalStyleWatchFace createFromParcel(Parcel source) {
                DigitalStyleWatchFace digitalStyleWatchFace = new DigitalStyleWatchFace();
                digitalStyleWatchFace.hourDisplayProfile = (TextDisplayProfile) source.readParcelable(TextDisplayProfile.class.getClassLoader());
                digitalStyleWatchFace.minuteDisplayProfile = (TextDisplayProfile) source.readParcelable(TextDisplayProfile.class.getClassLoader());
                digitalStyleWatchFace.separatorDisplayProfile = (TextDisplayProfile) source.readParcelable(TextDisplayProfile.class.getClassLoader());
                digitalStyleWatchFace.traditionalChineseCalenderDisplayProfile = (TextDisplayProfile) source.readParcelable(TextDisplayProfile.class.getClassLoader());
                digitalStyleWatchFace.dateDateDisplayProfile = (TextDisplayProfile) source.readParcelable(TextDisplayProfile.class.getClassLoader());
                return digitalStyleWatchFace;
            }

            public DigitalStyleWatchFace[] newArray(int size) {
                return new DigitalStyleWatchFace[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.hourDisplayProfile, flags);
            dest.writeParcelable(this.minuteDisplayProfile, flags);
            dest.writeParcelable(this.separatorDisplayProfile, flags);
            dest.writeParcelable(this.traditionalChineseCalenderDisplayProfile, flags);
            dest.writeParcelable(this.dateDateDisplayProfile, flags);
        }

        public String toString() {
            return "DigitalStyleWatchFace [hourDisplayProfile=" + this.hourDisplayProfile + ", minuteDisplayProfile=" + this.minuteDisplayProfile + ", separatorDisplayProfile=" + this.separatorDisplayProfile + ", traditionalChineseCalenderDisplayProfile=" + this.traditionalChineseCalenderDisplayProfile + ", dataDateDisplayProfile=" + this.dateDateDisplayProfile + "]";
        }
    }

    public static class InnerWatchFace implements Parcelable {
        public static final Creator<InnerWatchFace> CREATOR = new C03431();
        public Background background;
        public TypefaceProfile fontDisplayProfile;
        public Picture pointer;
        public int posX;
        public int posY;

        static class C03431 implements Creator<InnerWatchFace> {
            C03431() {
            }

            public InnerWatchFace createFromParcel(Parcel source) {
                InnerWatchFace innerWatchFace = new InnerWatchFace();
                innerWatchFace.background = (Background) source.readParcelable(Background.class.getClassLoader());
                innerWatchFace.pointer = (Picture) source.readParcelable(Picture.class.getClassLoader());
                innerWatchFace.posX = source.readInt();
                innerWatchFace.posY = source.readInt();
                innerWatchFace.fontDisplayProfile = (TypefaceProfile) source.readParcelable(TypefaceProfile.class.getClassLoader());
                return innerWatchFace;
            }

            public InnerWatchFace[] newArray(int size) {
                return new InnerWatchFace[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.background, flags);
            dest.writeParcelable(this.pointer, flags);
            dest.writeInt(this.posX);
            dest.writeInt(this.posY);
            dest.writeParcelable(this.fontDisplayProfile, flags);
        }

        public String toString() {
            return "InnerWatchFace [background=" + this.background + ", pointer=" + this.pointer + ", posX=" + this.posX + ", posY=" + this.posY + ", textDisplayProfile=" + this.fontDisplayProfile + "]";
        }
    }

    public static class Picture implements Parcelable {
        public static final Creator<Picture> CREATOR = new C03441();
        private byte[] m_data;
        private int m_height;
        private int m_width;

        static class C03441 implements Creator<Picture> {
            C03441() {
            }

            public Picture createFromParcel(Parcel source) {
                byte[] bArr = new byte[source.readInt()];
                source.readByteArray(bArr);
                return new Picture(bArr, source.readInt(), source.readInt());
            }

            public Picture[] newArray(int size) {
                return new Picture[size];
            }
        }

        public Picture(byte[] data, int width, int height) {
            boolean z = false;
            boolean z2 = data == null || data.length == 0;
            IwdsAssert.dieIf((Object) this, z2, "Data is null or empty.");
            if (width <= 0 || height <= 0) {
                z = true;
            }
            IwdsAssert.dieIf((Object) this, z, "Width <= 0 or Heigth <= 0.");
            this.m_data = data;
            this.m_width = width;
            this.m_height = height;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.m_data.length);
            dest.writeByteArray(this.m_data);
            dest.writeInt(this.m_width);
            dest.writeInt(this.m_height);
        }

        public String toString() {
            return "Picture [m_data=" + Arrays.toString(this.m_data) + ", m_width=" + this.m_width + ", m_height=" + this.m_height + "]";
        }
    }

    public static class TextDisplayProfile implements Parcelable {
        public static final Creator<TextDisplayProfile> CREATOR = new C03451();
        public int posX;
        public int posY;
        public TypefaceProfile typefaceProfile;

        static class C03451 implements Creator<TextDisplayProfile> {
            C03451() {
            }

            public TextDisplayProfile createFromParcel(Parcel source) {
                TextDisplayProfile textDisplayProfile = new TextDisplayProfile();
                textDisplayProfile.posX = source.readInt();
                textDisplayProfile.posY = source.readInt();
                textDisplayProfile.typefaceProfile = (TypefaceProfile) source.readParcelable(TypefaceProfile.class.getClassLoader());
                return textDisplayProfile;
            }

            public TextDisplayProfile[] newArray(int size) {
                return new TextDisplayProfile[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.posX);
            dest.writeInt(this.posY);
            dest.writeParcelable(this.typefaceProfile, flags);
        }

        public String toString() {
            return "TextDisplayProfile [posX=" + this.posX + ", posY=" + this.posY + ", typefaceProfile=" + this.typefaceProfile + "]";
        }
    }

    public static class TypefaceProfile implements Parcelable {
        public static final Creator<TypefaceProfile> CREATOR = new C03461();
        public Background background;
        public int color;
        public float size;
        public String typefaceName;
        public int unit;

        static class C03461 implements Creator<TypefaceProfile> {
            C03461() {
            }

            public TypefaceProfile createFromParcel(Parcel source) {
                TypefaceProfile typefaceProfile = new TypefaceProfile();
                typefaceProfile.typefaceName = source.readString();
                typefaceProfile.color = source.readInt();
                typefaceProfile.size = source.readFloat();
                typefaceProfile.background = (Background) source.readParcelable(Background.class.getClassLoader());
                return typefaceProfile;
            }

            public TypefaceProfile[] newArray(int size) {
                return new TypefaceProfile[size];
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.typefaceName);
            dest.writeInt(this.color);
            dest.writeInt(this.unit);
            dest.writeFloat(this.size);
            dest.writeParcelable(this.background, flags);
        }

        public String toString() {
            return "TypefaceProfile [typefaceName=" + this.typefaceName + ", color=" + this.color + ", unit=" + this.unit + ", size=" + this.size + ", background=" + this.background + "]";
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeParcelable(this.background, flags);
        dest.writeParcelable(this.digitalWatchFace, flags);
        dest.writeParcelable(this.analogWatchFace, flags);
    }
}
