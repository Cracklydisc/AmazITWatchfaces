package com.ingenic.iwds.os;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.HashMap;
import java.util.Map;

public class RemoteBundle implements Parcelable, Cloneable {
    public static final Creator<RemoteBundle> CREATOR = new C03491();
    private Map<String, Object> f56a;

    static class C03491 implements Creator<RemoteBundle> {
        C03491() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m6a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m7a(i);
        }

        public RemoteBundle m6a(Parcel parcel) {
            return new RemoteBundle(parcel);
        }

        public RemoteBundle[] m7a(int i) {
            return new RemoteBundle[i];
        }
    }

    public RemoteBundle(RemoteBundle in) {
        if (in.f56a != null) {
            this.f56a = new HashMap(in.f56a);
        }
    }

    protected RemoteBundle(Parcel source) {
        int readInt = source.readInt();
        if (readInt > 0) {
            this.f56a = new HashMap(readInt);
            source.readMap(this.f56a, getClass().getClassLoader());
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        if (this.f56a != null) {
            dest.writeInt(this.f56a.size());
            dest.writeMap(this.f56a);
            return;
        }
        dest.writeInt(0);
    }

    public Object clone() {
        return new RemoteBundle(this);
    }
}
