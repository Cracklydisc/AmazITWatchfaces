package com.ingenic.iwds.remotebroadcast;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.content.RemoteIntent;

class RemoteBroadcast implements Parcelable {
    public static final Creator<RemoteBroadcast> CREATOR = new C03501();
    private int f57a;
    private RemoteIntent f58b;
    private String f59c;
    private boolean f60d;
    private boolean f61e;

    static class C03501 implements Creator<RemoteBroadcast> {
        C03501() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m8a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m9a(i);
        }

        public RemoteBroadcast m8a(Parcel parcel) {
            return new RemoteBroadcast(parcel);
        }

        public RemoteBroadcast[] m9a(int i) {
            return new RemoteBroadcast[i];
        }
    }

    protected RemoteBroadcast(Parcel in) {
        readFromParcel(in);
    }

    public int describeContents() {
        return this.f58b != null ? this.f58b.describeContents() : 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i;
        int i2 = 1;
        dest.writeInt(this.f57a);
        if (this.f58b != null) {
            dest.writeInt(1);
            this.f58b.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        dest.writeString(this.f59c);
        if (this.f60d) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeInt(i);
        if (!this.f61e) {
            i2 = 0;
        }
        dest.writeInt(i2);
    }

    public void readFromParcel(Parcel in) {
        boolean z;
        boolean z2 = true;
        this.f57a = in.readInt();
        if (in.readInt() != 0) {
            this.f58b = (RemoteIntent) RemoteIntent.CREATOR.createFromParcel(in);
        }
        this.f59c = in.readString();
        if (in.readInt() != 0) {
            z = true;
        } else {
            z = false;
        }
        this.f60d = z;
        if (in.readInt() == 0) {
            z2 = false;
        }
        this.f61e = z2;
    }
}
