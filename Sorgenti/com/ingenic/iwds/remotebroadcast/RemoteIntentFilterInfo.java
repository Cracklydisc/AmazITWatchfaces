package com.ingenic.iwds.remotebroadcast;

import android.content.IntentFilter;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteBroadcastInfo */
class RemoteIntentFilterInfo extends C0352a implements Parcelable {
    public static final Creator<RemoteIntentFilterInfo> CREATOR = new C03511();
    private IntentFilter f64c;
    private String f65d;

    /* compiled from: RemoteBroadcastInfo */
    static class C03511 implements Creator<RemoteIntentFilterInfo> {
        C03511() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m10a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m11a(i);
        }

        public RemoteIntentFilterInfo m10a(Parcel parcel) {
            return new RemoteIntentFilterInfo(parcel);
        }

        public RemoteIntentFilterInfo[] m11a(int i) {
            return new RemoteIntentFilterInfo[i];
        }
    }

    protected RemoteIntentFilterInfo(Parcel in) {
        readFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
        if (this.f64c != null) {
            dest.writeInt(1);
            this.f64c.writeToParcel(dest, flags);
        } else {
            dest.writeInt(0);
        }
        dest.writeString(this.f65d);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
        if (in.readInt() != 0) {
            this.f64c = (IntentFilter) IntentFilter.CREATOR.createFromParcel(in);
        }
        this.f65d = in.readString();
    }

    public String toString() {
        return "RemoteIntentFilterInfo[id:" + this.b + "-IntentFilter:" + this.f64c + "-permission:" + this.f65d + "]";
    }
}
