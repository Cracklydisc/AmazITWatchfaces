package com.ingenic.iwds.remotebroadcast;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.content.RemoteIntent;

/* compiled from: RemoteBroadcastInfo */
class RemoteIntentInfo extends C0352a implements Parcelable {
    public static final Creator<RemoteIntentInfo> CREATOR = new C03531();
    private RemoteIntent f66c;

    /* compiled from: RemoteBroadcastInfo */
    static class C03531 implements Creator<RemoteIntentInfo> {
        C03531() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m12a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m13a(i);
        }

        public RemoteIntentInfo m12a(Parcel parcel) {
            return new RemoteIntentInfo(parcel);
        }

        public RemoteIntentInfo[] m13a(int i) {
            return new RemoteIntentInfo[i];
        }
    }

    protected RemoteIntentInfo(Parcel in) {
        readFromParcel(in);
    }

    public int describeContents() {
        return this.f66c != null ? this.f66c.describeContents() : 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
        if (this.f66c != null) {
            dest.writeInt(1);
            this.f66c.writeToParcel(dest, flags);
            return;
        }
        dest.writeInt(0);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
        if (in.readInt() != 0) {
            this.f66c = (RemoteIntent) RemoteIntent.CREATOR.createFromParcel(in);
        }
    }

    public String toString() {
        return "RemoteIntentInfo[-id" + this.b + "-Intent:" + this.f66c + "]";
    }
}
