package com.ingenic.iwds.remotebroadcast;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteBroadcastInfo */
class UnregisterInfo extends C0352a implements Parcelable {
    public static final Creator<UnregisterInfo> CREATOR = new C03541();

    /* compiled from: RemoteBroadcastInfo */
    static class C03541 implements Creator<UnregisterInfo> {
        C03541() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m14a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m15a(i);
        }

        public UnregisterInfo m14a(Parcel parcel) {
            return new UnregisterInfo(parcel);
        }

        public UnregisterInfo[] m15a(int i) {
            return new UnregisterInfo[i];
        }
    }

    protected UnregisterInfo(Parcel in) {
        readFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
    }
}
