package com.ingenic.iwds.remotedevice;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

class RemoteApplicationInfoList implements Parcelable {
    public static final Creator<RemoteApplicationInfoList> CREATOR = new C03561();
    private List<RemoteApplicationInfo> remoteAppInfoList;

    static class C03561 implements Creator<RemoteApplicationInfoList> {
        C03561() {
        }

        public RemoteApplicationInfoList createFromParcel(Parcel source) {
            return new RemoteApplicationInfoList(source);
        }

        public RemoteApplicationInfoList[] newArray(int size) {
            return new RemoteApplicationInfoList[size];
        }
    }

    private RemoteApplicationInfoList(Parcel in) {
        this.remoteAppInfoList = new ArrayList();
        in.readList(this.remoteAppInfoList, RemoteApplicationInfo.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.remoteAppInfoList);
    }
}
