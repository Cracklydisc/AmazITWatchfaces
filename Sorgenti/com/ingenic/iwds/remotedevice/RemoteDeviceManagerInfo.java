package com.ingenic.iwds.remotedevice;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class RemoteDeviceManagerInfo {

    static class RemoteResponse implements Parcelable {
        public static final Creator<RemoteResponse> CREATOR = new C03621();
        int returnCode;
        int type;

        static class C03621 implements Creator<RemoteResponse> {
            C03621() {
            }

            public RemoteResponse createFromParcel(Parcel source) {
                return new RemoteResponse(source);
            }

            public RemoteResponse[] newArray(int size) {
                return new RemoteResponse[size];
            }
        }

        public RemoteResponse(Parcel source) {
            readFromParcel(source);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.type);
            dest.writeInt(this.returnCode);
        }

        public void readFromParcel(Parcel source) {
            this.type = source.readInt();
            this.returnCode = source.readInt();
        }
    }

    static class AppListResponse extends RemoteResponse {
        public static final Creator<AppListResponse> CREATOR = new C03571();
        RemoteApplicationInfoList appList;

        static class C03571 implements Creator<AppListResponse> {
            C03571() {
            }

            public AppListResponse createFromParcel(Parcel source) {
                return new AppListResponse(source);
            }

            public AppListResponse[] newArray(int size) {
                return new AppListResponse[size];
            }
        }

        public AppListResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeParcelable(this.appList, flags);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.appList = (RemoteApplicationInfoList) source.readParcelable(AppListResponse.class.getClassLoader());
        }
    }

    static class ResponseWithName extends RemoteResponse {
        public static final Creator<ResponseWithName> CREATOR = new C03631();
        String packageName;

        static class C03631 implements Creator<ResponseWithName> {
            C03631() {
            }

            public ResponseWithName createFromParcel(Parcel source) {
                return new ResponseWithName(source);
            }

            public ResponseWithName[] newArray(int size) {
                return new ResponseWithName[size];
            }
        }

        public ResponseWithName(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.packageName);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.packageName = source.readString();
        }
    }

    static class ClearAllAppDataCacheResponse extends ResponseWithName {
        public static final Creator<ClearAllAppDataCacheResponse> CREATOR = new C03581();
        int index;
        int totalCount;
        int typeOfIndex;

        static class C03581 implements Creator<ClearAllAppDataCacheResponse> {
            C03581() {
            }

            public ClearAllAppDataCacheResponse createFromParcel(Parcel source) {
                return new ClearAllAppDataCacheResponse(source);
            }

            public ClearAllAppDataCacheResponse[] newArray(int size) {
                return new ClearAllAppDataCacheResponse[size];
            }
        }

        public ClearAllAppDataCacheResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.packageName);
            dest.writeInt(this.totalCount);
            dest.writeInt(this.index);
            dest.writeInt(this.typeOfIndex);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.packageName = source.readString();
            this.totalCount = source.readInt();
            this.index = source.readInt();
            this.typeOfIndex = source.readInt();
        }
    }

    static class ConfirmInstallResponse extends ResponseWithName {
        public static final Creator<ConfirmInstallResponse> CREATOR = new C03591();
        String apkFilePath;

        static class C03591 implements Creator<ConfirmInstallResponse> {
            C03591() {
            }

            public ConfirmInstallResponse createFromParcel(Parcel source) {
                return new ConfirmInstallResponse(source);
            }

            public ConfirmInstallResponse[] newArray(int size) {
                return new ConfirmInstallResponse[size];
            }
        }

        public ConfirmInstallResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.apkFilePath);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.apkFilePath = source.readString();
        }
    }

    static class PkgInfoResponse extends RemoteResponse {
        public static final Creator<PkgInfoResponse> CREATOR = new C03601();
        String packageName;
        RemotePackageStats pkgStats;

        static class C03601 implements Creator<PkgInfoResponse> {
            C03601() {
            }

            public PkgInfoResponse createFromParcel(Parcel source) {
                return new PkgInfoResponse(source);
            }

            public PkgInfoResponse[] newArray(int size) {
                return new PkgInfoResponse[size];
            }
        }

        public PkgInfoResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.packageName);
            dest.writeParcelable(this.pkgStats, flags);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.packageName = source.readString();
            this.pkgStats = (RemotePackageStats) source.readParcelable(PkgInfoResponse.class.getClassLoader());
        }
    }

    static class ProcessInfoResponse extends RemoteResponse {
        public static final Creator<ProcessInfoResponse> CREATOR = new C03611();
        RemoteProcessInfoList processList;

        static class C03611 implements Creator<ProcessInfoResponse> {
            C03611() {
            }

            public ProcessInfoResponse createFromParcel(Parcel source) {
                return new ProcessInfoResponse(source);
            }

            public ProcessInfoResponse[] newArray(int size) {
                return new ProcessInfoResponse[size];
            }
        }

        public ProcessInfoResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeParcelable(this.processList, flags);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.processList = (RemoteProcessInfoList) source.readParcelable(ProcessInfoResponse.class.getClassLoader());
        }
    }

    static class SettingResponse extends RemoteResponse {
        public static final Creator<SettingResponse> CREATOR = new C03641();
        int subType;

        static class C03641 implements Creator<SettingResponse> {
            C03641() {
            }

            public SettingResponse createFromParcel(Parcel source) {
                return new SettingResponse(source);
            }

            public SettingResponse[] newArray(int size) {
                return new SettingResponse[size];
            }
        }

        public SettingResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(this.subType);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.subType = source.readInt();
        }
    }

    static class StorageInfoResponse extends RemoteResponse {
        public static final Creator<StorageInfoResponse> CREATOR = new C03651();
        RemoteStorageInfo storageInfo;

        static class C03651 implements Creator<StorageInfoResponse> {
            C03651() {
            }

            public StorageInfoResponse createFromParcel(Parcel source) {
                return new StorageInfoResponse(source);
            }

            public StorageInfoResponse[] newArray(int size) {
                return new StorageInfoResponse[size];
            }
        }

        public StorageInfoResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeParcelable(this.storageInfo, flags);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.storageInfo = (RemoteStorageInfo) source.readParcelable(StorageInfoResponse.class.getClassLoader());
        }
    }

    static class SysMemResponse extends RemoteResponse {
        public static final Creator<SysMemResponse> CREATOR = new C03661();
        long availSysMemSize;
        long totalSysMemSize;

        static class C03661 implements Creator<SysMemResponse> {
            C03661() {
            }

            public SysMemResponse createFromParcel(Parcel source) {
                return new SysMemResponse(source);
            }

            public SysMemResponse[] newArray(int size) {
                return new SysMemResponse[size];
            }
        }

        public SysMemResponse(Parcel source) {
            super(source);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeLong(this.availSysMemSize);
            dest.writeLong(this.totalSysMemSize);
        }

        public void readFromParcel(Parcel source) {
            super.readFromParcel(source);
            this.availSysMemSize = source.readLong();
            this.totalSysMemSize = source.readLong();
        }
    }
}
