package com.ingenic.iwds.remotedevice;

import android.content.pm.PackageStats;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

class RemotePackageStats implements Parcelable {
    public static final Creator<RemotePackageStats> CREATOR = new C03671();
    public long cacheSize;
    public long codeSize;
    public long dataSize;
    public long externalCacheSize;
    public long externalCodeSize;
    public long externalDataSize;
    public long externalMediaSize;
    public long externalObbSize;
    public String packageName;
    private PackageStats packageStats = new PackageStats(this.packageName);

    static class C03671 implements Creator<RemotePackageStats> {
        C03671() {
        }

        public RemotePackageStats createFromParcel(Parcel in) {
            return new RemotePackageStats(in);
        }

        public RemotePackageStats[] newArray(int size) {
            return new RemotePackageStats[size];
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("PackageStats{");
        stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
        stringBuilder.append(" ");
        stringBuilder.append(this.packageName);
        if (this.codeSize != 0) {
            stringBuilder.append(" code=");
            stringBuilder.append(this.codeSize);
        }
        if (this.dataSize != 0) {
            stringBuilder.append(" data=");
            stringBuilder.append(this.dataSize);
        }
        if (this.cacheSize != 0) {
            stringBuilder.append(" cache=");
            stringBuilder.append(this.cacheSize);
        }
        if (this.externalCodeSize != 0) {
            stringBuilder.append(" extCode=");
            stringBuilder.append(this.externalCodeSize);
        }
        if (this.externalDataSize != 0) {
            stringBuilder.append(" extData=");
            stringBuilder.append(this.externalDataSize);
        }
        if (this.externalCacheSize != 0) {
            stringBuilder.append(" extCache=");
            stringBuilder.append(this.externalCacheSize);
        }
        if (this.externalMediaSize != 0) {
            stringBuilder.append(" media=");
            stringBuilder.append(this.externalMediaSize);
        }
        if (this.externalObbSize != 0) {
            stringBuilder.append(" obb=");
            stringBuilder.append(this.externalObbSize);
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    public RemotePackageStats(Parcel source) {
        this.packageName = source.readString();
        PackageStats packageStats = this.packageStats;
        long readLong = source.readLong();
        this.codeSize = readLong;
        packageStats.codeSize = readLong;
        packageStats = this.packageStats;
        readLong = source.readLong();
        this.dataSize = readLong;
        packageStats.dataSize = readLong;
        packageStats = this.packageStats;
        readLong = source.readLong();
        this.cacheSize = readLong;
        packageStats.cacheSize = readLong;
        packageStats = this.packageStats;
        readLong = source.readLong();
        this.externalCodeSize = readLong;
        packageStats.externalCodeSize = readLong;
        packageStats = this.packageStats;
        readLong = source.readLong();
        this.externalDataSize = readLong;
        packageStats.externalDataSize = readLong;
        packageStats = this.packageStats;
        readLong = source.readLong();
        this.externalCacheSize = readLong;
        packageStats.externalCacheSize = readLong;
        packageStats = this.packageStats;
        readLong = source.readLong();
        this.externalMediaSize = readLong;
        packageStats.externalMediaSize = readLong;
        packageStats = this.packageStats;
        readLong = source.readLong();
        this.externalObbSize = readLong;
        packageStats.externalObbSize = readLong;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int parcelableFlags) {
        dest.writeString(this.packageName);
        dest.writeLong(this.codeSize);
        dest.writeLong(this.dataSize);
        dest.writeLong(this.cacheSize);
        dest.writeLong(this.externalCodeSize);
        dest.writeLong(this.externalDataSize);
        dest.writeLong(this.externalCacheSize);
        dest.writeLong(this.externalMediaSize);
        dest.writeLong(this.externalObbSize);
    }
}
