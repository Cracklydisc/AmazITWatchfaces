package com.ingenic.iwds.remotedevice;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

/* compiled from: RemoteProcessInfo */
class RemoteProcessInfoList implements Parcelable {
    public static final Creator<RemoteProcessInfoList> CREATOR = new C03691();
    List<RemoteProcessInfo> processInfoList;

    /* compiled from: RemoteProcessInfo */
    static class C03691 implements Creator<RemoteProcessInfoList> {
        C03691() {
        }

        public RemoteProcessInfoList createFromParcel(Parcel source) {
            return new RemoteProcessInfoList(source);
        }

        public RemoteProcessInfoList[] newArray(int size) {
            return new RemoteProcessInfoList[size];
        }
    }

    public RemoteProcessInfoList() {
        this.processInfoList = new ArrayList();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.processInfoList);
    }

    private RemoteProcessInfoList(Parcel source) {
        this.processInfoList = new ArrayList();
        source.readList(this.processInfoList, RemoteProcessInfo.class.getClassLoader());
    }
}
