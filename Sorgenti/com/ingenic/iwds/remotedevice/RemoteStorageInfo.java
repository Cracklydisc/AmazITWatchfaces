package com.ingenic.iwds.remotedevice;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class RemoteStorageInfo implements Parcelable {
    public static final Creator<RemoteStorageInfo> CREATOR = new C03701();
    public long availExternalSize;
    public long availInternalSize;
    public boolean hasExternalStorage;
    public long totalExternalSize;
    public long totalInternalSize;

    static class C03701 implements Creator<RemoteStorageInfo> {
        C03701() {
        }

        public RemoteStorageInfo createFromParcel(Parcel source) {
            return new RemoteStorageInfo(source);
        }

        public RemoteStorageInfo[] newArray(int size) {
            return new RemoteStorageInfo[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.availInternalSize);
        dest.writeLong(this.totalInternalSize);
        dest.writeInt(this.hasExternalStorage ? 1 : 0);
        dest.writeLong(this.availExternalSize);
        dest.writeLong(this.totalExternalSize);
    }

    private RemoteStorageInfo(Parcel source) {
        boolean z = true;
        this.availInternalSize = source.readLong();
        this.totalInternalSize = source.readLong();
        if (source.readInt() != 1) {
            z = false;
        }
        this.hasExternalStorage = z;
        this.availExternalSize = source.readLong();
        this.totalExternalSize = source.readLong();
    }
}
