package com.ingenic.iwds.remotewakelock;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteWakeLockInfo */
class CMDInfo extends C0372a implements Parcelable {
    public static final Creator<CMDInfo> CREATOR = new C03711();
    private int f69c = 0;
    private long f70d = -1;

    /* compiled from: RemoteWakeLockInfo */
    static class C03711 implements Creator<CMDInfo> {
        C03711() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m16a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m17a(i);
        }

        public CMDInfo m16a(Parcel parcel) {
            return new CMDInfo(parcel);
        }

        public CMDInfo[] m17a(int i) {
            return new CMDInfo[i];
        }
    }

    protected CMDInfo(Parcel in) {
        readFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
        dest.writeInt(this.f69c);
        dest.writeLong(this.f70d);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
        this.f69c = in.readInt();
        this.f70d = in.readLong();
    }
}
