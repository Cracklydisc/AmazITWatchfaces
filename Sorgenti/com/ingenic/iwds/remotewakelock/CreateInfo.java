package com.ingenic.iwds.remotewakelock;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteWakeLockInfo */
class CreateInfo extends C0372a implements Parcelable {
    public static final Creator<CreateInfo> CREATOR = new C03731();
    private int f71c;
    private String f72d;

    /* compiled from: RemoteWakeLockInfo */
    static class C03731 implements Creator<CreateInfo> {
        C03731() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m18a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m19a(i);
        }

        public CreateInfo m18a(Parcel parcel) {
            return new CreateInfo(parcel);
        }

        public CreateInfo[] m19a(int i) {
            return new CreateInfo[i];
        }
    }

    protected CreateInfo(Parcel in) {
        readFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
        dest.writeInt(this.f71c);
        dest.writeString(this.f72d);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
        this.f71c = in.readInt();
        this.f72d = in.readString();
    }
}
