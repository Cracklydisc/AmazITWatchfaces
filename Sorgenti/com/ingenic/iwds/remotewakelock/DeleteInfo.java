package com.ingenic.iwds.remotewakelock;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

/* compiled from: RemoteWakeLockInfo */
class DeleteInfo extends C0372a implements Parcelable {
    public static final Creator<DeleteInfo> CREATOR = new C03741();

    /* compiled from: RemoteWakeLockInfo */
    static class C03741 implements Creator<DeleteInfo> {
        C03741() {
        }

        public /* synthetic */ Object createFromParcel(Parcel parcel) {
            return m20a(parcel);
        }

        public /* synthetic */ Object[] newArray(int i) {
            return m21a(i);
        }

        public DeleteInfo m20a(Parcel parcel) {
            return new DeleteInfo(parcel);
        }

        public DeleteInfo[] m21a(int i) {
            return new DeleteInfo[i];
        }
    }

    protected DeleteInfo(Parcel in) {
        readFromParcel(in);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.a);
        dest.writeInt(this.b);
    }

    public void readFromParcel(Parcel in) {
        this.a = in.readInt();
        this.b = in.readInt();
    }
}
