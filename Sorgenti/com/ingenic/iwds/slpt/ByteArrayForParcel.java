package com.ingenic.iwds.slpt;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ByteArrayForParcel implements Parcelable {
    public static Creator<ByteArrayForParcel> CREATOR = new C03751();
    byte[] array = null;
    int length = 0;
    int offset = 0;

    static class C03751 implements Creator<ByteArrayForParcel> {
        C03751() {
        }

        public ByteArrayForParcel[] newArray(int size) {
            return new ByteArrayForParcel[size];
        }

        public ByteArrayForParcel createFromParcel(Parcel source) {
            return new ByteArrayForParcel(source);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.array == null ? 0 : this.array.length);
        if (this.array != null) {
            dest.writeByteArray(this.array, this.offset, this.length);
        }
    }

    public ByteArrayForParcel(Parcel source) {
        if (source.readInt() != 0) {
            this.array = source.createByteArray();
        }
    }
}
