package com.ingenic.iwds.slpt;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;

public class GroupData implements Parcelable {
    public static final Creator<GroupData> CREATOR = new C03761();
    public String groupName = "";
    public ArrayList<PictureData> pictureList = new ArrayList();
    public int pictureNum = 0;

    static class C03761 implements Creator<GroupData> {
        C03761() {
        }

        public GroupData createFromParcel(Parcel source) {
            GroupData groupData = new GroupData();
            groupData.pictureNum = source.readInt();
            groupData.groupName = source.readString();
            int size = source.readInt();
            for (int i = 0; i < size; i++) {
                groupData.pictureList.add(PictureData.CREATOR.createFromParcel(source));
            }
            return groupData;
        }

        public GroupData[] newArray(int size) {
            return new GroupData[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.pictureNum);
        dest.writeString(this.groupName);
        dest.writeInt(this.pictureList.size());
        for (int i = 0; i < this.pictureList.size(); i++) {
            ((PictureData) this.pictureList.get(i)).writeToParcel(dest, flags);
        }
    }
}
