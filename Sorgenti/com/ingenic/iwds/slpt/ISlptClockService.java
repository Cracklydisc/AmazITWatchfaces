package com.ingenic.iwds.slpt;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.v7.recyclerview.C0051R;

public interface ISlptClockService extends IInterface {

    public static abstract class Stub extends Binder implements ISlptClockService {

        private static class Proxy implements ISlptClockService {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void isAlive() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void enableSlpt() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void disableSlpt() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    this.mRemote.transact(3, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean slptIsEnabled() throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    this.mRemote.transact(4, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setClockPeriod(int period) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeInt(period);
                    this.mRemote.transact(5, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setTargetSportStep(int step) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeInt(step);
                    this.mRemote.transact(6, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setTodayDistance(float distance) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeFloat(distance);
                    this.mRemote.transact(7, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setTotalDistance(float distance) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeFloat(distance);
                    this.mRemote.transact(8, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setLastHeartrate(int heartrate) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeInt(heartrate);
                    this.mRemote.transact(9, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setSafeHeartrate(int heartrate) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeInt(heartrate);
                    this.mRemote.transact(10, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setMeasurement(int key) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeInt(key);
                    this.mRemote.transact(11, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setHourFormat(int key) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeInt(key);
                    this.mRemote.transact(12, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setSportStopTime(long time) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeLong(time);
                    this.mRemote.transact(13, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setLongUpKeyStatus(int enable) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeInt(enable);
                    this.mRemote.transact(14, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setKeyWakeupStatus(int enable) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeInt(enable);
                    this.mRemote.transact(15, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void setWakeUpVibratorStatus(int enable) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeInt(enable);
                    this.mRemote.transact(16, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void enableSportMode() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    this.mRemote.transact(17, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void disableSportMode() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    this.mRemote.transact(18, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void enableLowBattery() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    this.mRemote.transact(19, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void disableLowBattery() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    this.mRemote.transact(20, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean registerCallback(String uuid, ISlptClockServiceCallback callback) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    _data.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(21, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unregisterCallback(String uuid, ISlptClockServiceCallback callback) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    _data.writeStrongBinder(callback != null ? callback.asBinder() : null);
                    this.mRemote.transact(22, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean lockService(String uuid) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    this.mRemote.transact(23, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void clearAllClock(String uuid) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    this.mRemote.transact(24, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void selectClockIndex(String uuid, int index) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    _data.writeInt(index);
                    this.mRemote.transact(25, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void writeSview(String uuid, byte[] mem) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    _data.writeByteArray(mem);
                    this.mRemote.transact(26, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void clearPictureGroup(String uuid) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    this.mRemote.transact(27, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void writePictureGroup(String uuid, String groupName) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    _data.writeString(groupName);
                    this.mRemote.transact(28, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void writePictureRle(String uuid, PictureInfo[] infoList, int[] mem) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    _data.writeTypedArray(infoList, 0);
                    _data.writeIntArray(mem);
                    this.mRemote.transact(29, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void writePreDrawedPicture(String uuid, PreDrawedPictureInfo info) throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    if (info != null) {
                        _data.writeInt(1);
                        info.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    this.mRemote.transact(30, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public boolean unlockService(String uuid) throws RemoteException {
                boolean _result = false;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockService");
                    _data.writeString(uuid);
                    this.mRemote.transact(31, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = true;
                    }
                    _reply.recycle();
                    _data.recycle();
                    return _result;
                } catch (Throwable th) {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public static ISlptClockService asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.ingenic.iwds.slpt.ISlptClockService");
            if (iin == null || !(iin instanceof ISlptClockService)) {
                return new Proxy(obj);
            }
            return (ISlptClockService) iin;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            int i = 0;
            boolean _result;
            switch (code) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    isAlive();
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    enableSlpt();
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    disableSlpt();
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    _result = slptIsEnabled();
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 5:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setClockPeriod(data.readInt());
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setTargetSportStep(data.readInt());
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setTodayDistance(data.readFloat());
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setTotalDistance(data.readFloat());
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setLastHeartrate(data.readInt());
                    reply.writeNoException();
                    return true;
                case 10:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setSafeHeartrate(data.readInt());
                    reply.writeNoException();
                    return true;
                case 11:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setMeasurement(data.readInt());
                    reply.writeNoException();
                    return true;
                case 12:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setHourFormat(data.readInt());
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setSportStopTime(data.readLong());
                    reply.writeNoException();
                    return true;
                case 14:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setLongUpKeyStatus(data.readInt());
                    reply.writeNoException();
                    return true;
                case 15:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setKeyWakeupStatus(data.readInt());
                    reply.writeNoException();
                    return true;
                case 16:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    setWakeUpVibratorStatus(data.readInt());
                    reply.writeNoException();
                    return true;
                case 17:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    enableSportMode();
                    reply.writeNoException();
                    return true;
                case 18:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    disableSportMode();
                    reply.writeNoException();
                    return true;
                case 19:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    enableLowBattery();
                    reply.writeNoException();
                    return true;
                case 20:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    disableLowBattery();
                    reply.writeNoException();
                    return true;
                case 21:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    _result = registerCallback(data.readString(), com.ingenic.iwds.slpt.ISlptClockServiceCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 22:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    _result = unregisterCallback(data.readString(), com.ingenic.iwds.slpt.ISlptClockServiceCallback.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 23:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    _result = lockService(data.readString());
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 24:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    clearAllClock(data.readString());
                    reply.writeNoException();
                    return true;
                case 25:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    selectClockIndex(data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                case 26:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    writeSview(data.readString(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 27:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    clearPictureGroup(data.readString());
                    reply.writeNoException();
                    return true;
                case 28:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    writePictureGroup(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 29:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    writePictureRle(data.readString(), (PictureInfo[]) data.createTypedArray(PictureInfo.CREATOR), data.createIntArray());
                    reply.writeNoException();
                    return true;
                case 30:
                    PreDrawedPictureInfo _arg1;
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    String _arg0 = data.readString();
                    if (data.readInt() != 0) {
                        _arg1 = (PreDrawedPictureInfo) PreDrawedPictureInfo.CREATOR.createFromParcel(data);
                    } else {
                        _arg1 = null;
                    }
                    writePreDrawedPicture(_arg0, _arg1);
                    reply.writeNoException();
                    return true;
                case 31:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockService");
                    _result = unlockService(data.readString());
                    reply.writeNoException();
                    if (_result) {
                        i = 1;
                    }
                    reply.writeInt(i);
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.slpt.ISlptClockService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void clearAllClock(String str) throws RemoteException;

    void clearPictureGroup(String str) throws RemoteException;

    void disableLowBattery() throws RemoteException;

    void disableSlpt() throws RemoteException;

    void disableSportMode() throws RemoteException;

    void enableLowBattery() throws RemoteException;

    void enableSlpt() throws RemoteException;

    void enableSportMode() throws RemoteException;

    void isAlive() throws RemoteException;

    boolean lockService(String str) throws RemoteException;

    boolean registerCallback(String str, ISlptClockServiceCallback iSlptClockServiceCallback) throws RemoteException;

    void selectClockIndex(String str, int i) throws RemoteException;

    void setClockPeriod(int i) throws RemoteException;

    void setHourFormat(int i) throws RemoteException;

    void setKeyWakeupStatus(int i) throws RemoteException;

    void setLastHeartrate(int i) throws RemoteException;

    void setLongUpKeyStatus(int i) throws RemoteException;

    void setMeasurement(int i) throws RemoteException;

    void setSafeHeartrate(int i) throws RemoteException;

    void setSportStopTime(long j) throws RemoteException;

    void setTargetSportStep(int i) throws RemoteException;

    void setTodayDistance(float f) throws RemoteException;

    void setTotalDistance(float f) throws RemoteException;

    void setWakeUpVibratorStatus(int i) throws RemoteException;

    boolean slptIsEnabled() throws RemoteException;

    boolean unlockService(String str) throws RemoteException;

    boolean unregisterCallback(String str, ISlptClockServiceCallback iSlptClockServiceCallback) throws RemoteException;

    void writePictureGroup(String str, String str2) throws RemoteException;

    void writePictureRle(String str, PictureInfo[] pictureInfoArr, int[] iArr) throws RemoteException;

    void writePreDrawedPicture(String str, PreDrawedPictureInfo preDrawedPictureInfo) throws RemoteException;

    void writeSview(String str, byte[] bArr) throws RemoteException;
}
