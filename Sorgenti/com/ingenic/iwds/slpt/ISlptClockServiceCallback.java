package com.ingenic.iwds.slpt;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.v7.recyclerview.C0051R;

public interface ISlptClockServiceCallback extends IInterface {

    public static abstract class Stub extends Binder implements ISlptClockServiceCallback {

        private static class Proxy implements ISlptClockServiceCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void isAlive() throws RemoteException {
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("com.ingenic.iwds.slpt.ISlptClockServiceCallback");
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, "com.ingenic.iwds.slpt.ISlptClockServiceCallback");
        }

        public static ISlptClockServiceCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("com.ingenic.iwds.slpt.ISlptClockServiceCallback");
            if (iin == null || !(iin instanceof ISlptClockServiceCallback)) {
                return new Proxy(obj);
            }
            return (ISlptClockServiceCallback) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    data.enforceInterface("com.ingenic.iwds.slpt.ISlptClockServiceCallback");
                    isAlive();
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.ingenic.iwds.slpt.ISlptClockServiceCallback");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void isAlive() throws RemoteException;
}
