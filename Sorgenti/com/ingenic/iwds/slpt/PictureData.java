package com.ingenic.iwds.slpt;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PictureData implements Parcelable {
    public static final Creator<PictureData> CREATOR = new C03771();
    public int backgroundColor;
    public int[] bitmapBuffer;
    public int height;
    public int pictureIndex;
    public int pictureSize;
    public int width;

    static class C03771 implements Creator<PictureData> {
        C03771() {
        }

        public PictureData createFromParcel(Parcel source) {
            PictureData pictureData = new PictureData();
            pictureData.pictureSize = source.readInt();
            pictureData.pictureIndex = source.readInt();
            pictureData.width = source.readInt();
            pictureData.height = source.readInt();
            pictureData.backgroundColor = source.readInt();
            return pictureData;
        }

        public PictureData[] newArray(int size) {
            return new PictureData[size];
        }
    }

    public PictureData() {
        this.bitmapBuffer = null;
        this.pictureSize = 0;
        this.pictureIndex = 0;
        this.width = 0;
        this.height = 0;
        this.backgroundColor = 0;
        this.bitmapBuffer = null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.bitmapBuffer.length);
        dest.writeInt(this.pictureIndex);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
        dest.writeInt(this.backgroundColor);
    }
}
