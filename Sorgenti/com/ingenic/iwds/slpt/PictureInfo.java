package com.ingenic.iwds.slpt;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PictureInfo implements Parcelable {
    public static final Creator<PictureInfo> CREATOR = new C03781();
    public int backgroundColor;
    public int height;
    public int[] mem = null;
    public String pictureName;
    public int width;

    static class C03781 implements Creator<PictureInfo> {
        C03781() {
        }

        public PictureInfo[] newArray(int size) {
            return new PictureInfo[size];
        }

        public PictureInfo createFromParcel(Parcel source) {
            return new PictureInfo(source);
        }
    }

    public PictureInfo(Parcel source) {
        this.pictureName = source.readString();
        this.width = source.readInt();
        this.height = source.readInt();
        this.backgroundColor = source.readInt();
    }

    public PictureInfo(String pictureName, int width, int height, int backgroundColor) {
        this.pictureName = pictureName;
        this.width = width;
        this.height = height;
        this.backgroundColor = backgroundColor;
    }

    public String toString() {
        return "[picture: " + this.pictureName + " " + this.width + " " + this.height + "]";
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.pictureName);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
        dest.writeInt(this.backgroundColor);
    }
}
