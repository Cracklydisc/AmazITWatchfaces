package com.ingenic.iwds.slpt;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.v7.recyclerview.C0051R;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup.PreDrawedPicture;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.ArrayList;

public class PreDrawedPictureInfo implements Parcelable {
    public static Creator<PreDrawedPictureInfo> CREATOR = new C03791();
    private PreDrawedPictureGroup group = null;
    private ArrayList<byte[]> memList = new ArrayList();
    private int picIndex = 0;
    private int picOffset = 0;
    private PreDrawedPictureGroup rgroup = null;
    private int rpicIndex = 0;
    private int rpicOffset = 0;

    static class C03791 implements Creator<PreDrawedPictureInfo> {
        C03791() {
        }

        public PreDrawedPictureInfo[] newArray(int size) {
            return new PreDrawedPictureInfo[size];
        }

        public PreDrawedPictureInfo createFromParcel(Parcel source) {
            return new PreDrawedPictureInfo(source);
        }
    }

    public int describeContents() {
        return 0;
    }

    private void mergeMemListToPicture(ArrayList<byte[]> memList) {
        boolean z;
        String str = "PreDrawedPictureInfo";
        if (memList.size() > this.rgroup.size() - this.rpicIndex) {
            z = true;
        } else {
            z = false;
        }
        IwdsAssert.dieIf(str, z, "error: too many byte array");
        int i = 0;
        while (i < memList.size()) {
            byte[] mem = (byte[]) memList.get(i);
            int length = this.rpicOffset + mem.length;
            PreDrawedPicture picture = this.rgroup.get(this.rpicIndex);
            str = "PreDrawedPictureInfo";
            if (length > picture.memLen) {
                z = true;
            } else {
                z = false;
            }
            IwdsAssert.dieIf(str, z, "error: too many bytes " + mem.length);
            str = "PreDrawedPictureInfo";
            if (length >= picture.memLen || i == memList.size() - 1) {
                z = false;
            } else {
                z = true;
            }
            IwdsAssert.dieIf(str, z, "error: too many byte array for a picture " + mem.length);
            if (length < picture.memLen) {
                if (this.rpicOffset == 0) {
                    picture.mem = new byte[picture.memLen];
                }
                System.arraycopy(mem, 0, picture.mem, this.rpicOffset, mem.length);
                this.rpicOffset += mem.length;
                return;
            }
            if (this.rpicOffset == 0) {
                picture.mem = mem;
            } else {
                System.arraycopy(mem, 0, picture.mem, this.rpicOffset, mem.length);
            }
            this.rpicIndex++;
            this.rpicOffset = 0;
            i++;
        }
    }

    public PreDrawedPictureInfo(Parcel source) {
        boolean goOut;
        do {
            int type = source.readInt();
            goOut = false;
            switch (type) {
                case 0:
                    boolean z;
                    String str = "PreDrawedPictureInfo";
                    if (this.rgroup != null) {
                        z = true;
                    } else {
                        z = false;
                    }
                    IwdsAssert.dieIf(str, z, "error: too many group");
                    String name = source.readString();
                    int size = source.readInt();
                    this.rgroup = new PreDrawedPictureGroup();
                    this.rgroup.setName(name);
                    for (int i = 0; i < size; i++) {
                        PreDrawedPicture picture = new PreDrawedPicture();
                        this.rgroup.add(picture);
                        picture.index = source.readInt();
                        picture.width = source.readInt();
                        picture.height = source.readInt();
                        picture.data_count = source.readInt();
                        picture.memLen = source.readInt();
                    }
                    continue;
                case C0051R.styleable.RecyclerView_layoutManager /*1*/:
                    this.memList.add(source.createByteArray());
                    continue;
                case 2:
                    goOut = true;
                    continue;
                default:
                    IwdsAssert.dieIf("PreDrawedPictureInfo", true, "unknow type " + type);
                    continue;
            }
        } while (!goOut);
        if (this.rgroup != null) {
            mergeMemListToPicture(this.memList);
        }
    }

    public void setSendGroup(PreDrawedPictureGroup group) {
        this.group = group;
        this.picIndex = 0;
        this.picOffset = 0;
    }

    public boolean isSendOk() {
        return this.group == null || this.group.size() == this.picIndex;
    }

    public void writeToParcel(Parcel dest, int flags) {
        if (this.picIndex == 0 && this.picOffset == 0) {
            dest.writeInt(0);
            dest.writeString(this.group.getName());
            dest.writeInt(this.group.size());
            for (int i = 0; i < this.group.size(); i++) {
                dest.writeInt(this.group.get(i).index);
                dest.writeInt(this.group.get(i).width);
                dest.writeInt(this.group.get(i).height);
                dest.writeInt(this.group.get(i).data_count);
                dest.writeInt(this.group.get(i).mem.length);
            }
        }
        int size = 0;
        boolean goOut = false;
        while (this.picIndex < this.group.size()) {
            int length;
            byte[] mem = this.group.get(this.picIndex).mem;
            if ((mem.length - this.picOffset) + size <= 30720) {
                length = mem.length - this.picOffset;
            } else {
                length = 30720 - size;
                goOut = true;
            }
            dest.writeInt(1);
            dest.writeByteArray(mem, this.picOffset, length);
            if (goOut) {
                this.picOffset += length;
                break;
            }
            size += length;
            this.picIndex++;
            this.picOffset = 0;
        }
        dest.writeInt(2);
    }
}
