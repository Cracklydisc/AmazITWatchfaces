package com.ingenic.iwds.slpt;

import java.util.Arrays;

public class RleBuffer {
    private int[] rleBuf = new int[10000];
    private int rleOffset = 0;

    public int add(int[] mem, int offset, int length) {
        int i = 0;
        int count = 0;
        if (mem.length > offset && this.rleBuf.length > this.rleOffset) {
            int[] iArr;
            int i2;
            i = offset;
            int val = mem[offset];
            while (i < length) {
                if (mem[i] == val) {
                    count++;
                } else {
                    iArr = this.rleBuf;
                    i2 = this.rleOffset;
                    this.rleOffset = i2 + 1;
                    iArr[i2] = val;
                    iArr = this.rleBuf;
                    i2 = this.rleOffset;
                    this.rleOffset = i2 + 1;
                    iArr[i2] = count;
                    if (this.rleOffset >= this.rleBuf.length) {
                        break;
                    }
                    val = mem[i];
                    count = 1;
                }
                i++;
            }
            if (this.rleOffset < this.rleBuf.length) {
                iArr = this.rleBuf;
                i2 = this.rleOffset;
                this.rleOffset = i2 + 1;
                iArr[i2] = val;
                iArr = this.rleBuf;
                i2 = this.rleOffset;
                this.rleOffset = i2 + 1;
                iArr[i2] = count;
            }
        }
        return i;
    }

    public int[] getBuffer() {
        int[] buf;
        if (this.rleOffset == 0) {
            buf = null;
        } else if (this.rleOffset == this.rleBuf.length) {
            buf = this.rleBuf;
        } else {
            buf = Arrays.copyOfRange(this.rleBuf, 0, this.rleOffset);
        }
        clear();
        return buf;
    }

    public void clear() {
        this.rleOffset = 0;
    }

    public int getOffset() {
        return this.rleOffset;
    }
}
