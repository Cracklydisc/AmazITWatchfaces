package com.ingenic.iwds.slpt;

import com.ingenic.iwds.slpt.view.core.Picture.PictureContainer;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup.PreDrawedPictureContainer;
import com.ingenic.iwds.slpt.view.core.SlptLayout;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent.RegisterPictureParam;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;
import com.ingenic.iwds.utils.IwdsAssert;

public class SlptClock {
    public static int CLOCK_INDEX_0 = 0;
    public static int CLOCK_INDEX_1 = 1;
    public static int CLOCK_INDEX_2 = 2;
    public static int CLOCK_INDEX_3 = 3;
    public static int CLOCK_INDEX_4 = 4;
    public static int CLOCK_INDEX_OVERFOLLOW = 7;
    public static int CLOCK_INDEX_SHOW = 999;
    private static String clock_last_heartrate = "/sys/slpt/apps/slpt-app/res/clock-last-heartrate/data";
    private static String clock_period_path = "/sys/slpt/apps/slpt-app/res/clock-period/data";
    private static String clock_safe_heartrate = "/sys/slpt/apps/slpt-app/res/clock-safe-heartrate/data";
    private static String clock_target_sport_step = "/sys/slpt/apps/slpt-app/res/clock-target-sport-step/data";
    private static String clock_today_sport_distance_f = "/sys/slpt/apps/slpt-app/res/clock-today-sport-distance-f/data";
    private static String clock_today_sport_distance_l = "/sys/slpt/apps/slpt-app/res/clock-today-sport-distance-l/data";
    private static String clock_total_sport_distance_f = "/sys/slpt/apps/slpt-app/res/clock-total-sport-distance-f/data";
    private static String clock_total_sport_distance_l = "/sys/slpt/apps/slpt-app/res/clock-total-sport-distance-l/data";
    private static boolean nativeIsInitialized = false;
    PreDrawedPictureContainer prePictureContainer;
    SlptLayout rootView;

    public SlptClock(SlptLayout rootView) {
        this.prePictureContainer = new PreDrawedPictureContainer();
        setRootView(rootView);
    }

    public SlptClock() {
        this.prePictureContainer = new PreDrawedPictureContainer();
        this.rootView = null;
    }

    public void setRootView(SlptLayout rootView) {
        IwdsAssert.dieIf("SlptClock", rootView == null, "rootView can not be null!");
        this.rootView = rootView;
    }

    public SlptLayout getRootView() {
        return this.rootView;
    }

    public void addPreDrawedPicture(PreDrawedPictureGroup group) {
        this.prePictureContainer.add(group);
    }

    public boolean writeToSlptService(SlptClockClient mClockClient) {
        KeyWriter writer = new KeyWriter();
        PictureContainer container = new PictureContainer();
        RegisterPictureParam param = new RegisterPictureParam();
        param.backgroundColor = 16777215;
        this.rootView.registerPicture(container, param);
        container.writeToSlptService(mClockClient);
        this.prePictureContainer.writeToSlptService(mClockClient);
        this.rootView.writeConfigure(writer);
        byte[] buffer = new byte[writer.getSize()];
        writer.setRawBytes(buffer, writer.getSize());
        mClockClient.writeSview(buffer);
        writer.recycle();
        return true;
    }
}
