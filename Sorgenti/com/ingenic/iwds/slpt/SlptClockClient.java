package com.ingenic.iwds.slpt;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.util.Log;
import com.ingenic.iwds.slpt.ISlptClockServiceCallback.Stub;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.ArrayList;

public class SlptClockClient {
    private boolean debugIsEnable = false;
    private ArrayList<PictureInfo> infoList = new ArrayList();
    private Callback m_callback;
    private final ServiceConnection m_connection = new C03801();
    private ISlptClockService m_service = null;
    private String m_uuid;
    private PowerManager pm = null;
    private RleBuffer rleBuffer = null;
    private final Stub serviceCallback = new C03812();
    private boolean service_is_bind = false;
    private WakeLock wakeLock = null;

    public interface Callback {
        void onServiceConnected();

        void onServiceDisconnected();
    }

    class C03801 implements ServiceConnection {
        C03801() {
        }

        public void onServiceDisconnected(ComponentName name) {
            SlptClockClient.this.debug("onServiceDisconnected ---------------!");
            SlptClockClient.this.service_is_bind = false;
            SlptClockClient.this.m_service = null;
            if (SlptClockClient.this.m_callback != null) {
                SlptClockClient.this.m_callback.onServiceDisconnected();
            }
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            SlptClockClient.this.debug("onServiceConnected ---------------!");
            Log.d("SlptClockClient", " onServiceConnected ");
            SlptClockClient.this.m_service = ISlptClockService.Stub.asInterface(service);
            if (SlptClockClient.this.connectionIsAlive()) {
                if (!SlptClockClient.this.registerCallback()) {
                    SlptClockClient.this.warning("callback already register");
                }
                if (SlptClockClient.this.m_callback != null) {
                    SlptClockClient.this.m_callback.onServiceConnected();
                    return;
                }
                return;
            }
            SlptClockClient.this.warning("connection to service is down");
        }
    }

    class C03812 extends Stub {
        C03812() {
        }

        public void isAlive() throws RemoteException {
        }
    }

    private void assertService() {
        IwdsAssert.dieIf("SlptClockClient", this.m_service == null, "service is not connected yet!");
    }

    public boolean serviceIsConnected() {
        return this.m_service != null;
    }

    public void bindService(Context context, String uuid, Callback callback) {
        IwdsAssert.dieIf("SlptClockClient", this.service_is_bind, "Do not bindService more than once!");
        Log.d("SlptClockClient", " service_is_bind = " + this.service_is_bind);
        this.service_is_bind = true;
        this.m_callback = callback;
        this.m_uuid = uuid;
        Intent intent = new Intent("com.huami.watchface.SlptClockService");
        intent.setPackage("com.huami.watch.wearservices");
        context.startService(intent);
        context.bindService(intent, this.m_connection, 1);
        this.pm = (PowerManager) context.getSystemService("power");
        this.wakeLock = this.pm.newWakeLock(1, "SlptClockClient");
    }

    public void unbindService(Context context) {
        if (this.service_is_bind) {
            this.service_is_bind = false;
            if (this.m_service != null) {
                unregisterCallback();
            }
            context.unbindService(this.m_connection);
            this.m_service = null;
            return;
        }
        warning("service is already unbinded!");
    }

    public boolean connectionIsAlive() {
        try {
            this.m_service.isAlive();
            return true;
        } catch (RemoteException e) {
            return false;
        }
    }

    public boolean enableSlpt() {
        assertService();
        try {
            this.m_service.enableSlpt();
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setClockPeriod(int periodSeconds) {
        if (periodSeconds <= 0) {
            periodSeconds = 900000;
        }
        assertService();
        try {
            this.m_service.setClockPeriod(periodSeconds);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setTargetSportStep(int step) {
        assertService();
        try {
            this.m_service.setTargetSportStep(step);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setTodayDistance(float distance) {
        assertService();
        try {
            this.m_service.setTodayDistance(distance);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setTotalDistance(float distance) {
        assertService();
        try {
            this.m_service.setTotalDistance(distance);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setLastHeartrate(int heartrate) {
        assertService();
        try {
            this.m_service.setLastHeartrate(heartrate);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setMeasurement(int key) {
        assertService();
        try {
            this.m_service.setMeasurement(key);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setHourFormat(int key) {
        assertService();
        try {
            this.m_service.setHourFormat(key);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setSportStopTime(long time) {
        assertService();
        try {
            this.m_service.setSportStopTime(time);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setLongUpKeyStatus(int enable) {
        assertService();
        try {
            this.m_service.setLongUpKeyStatus(enable);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setKeyWakeupStatus(int enable) {
        assertService();
        try {
            this.m_service.setKeyWakeupStatus(enable);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean enableSportMode() {
        assertService();
        try {
            this.m_service.enableSportMode();
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean disableSportMode() {
        assertService();
        try {
            this.m_service.disableSportMode();
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean lockService() {
        assertService();
        try {
            Log.d("SlptClockClient", "lockService");
            return this.m_service.lockService(this.m_uuid);
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean unlockService() {
        assertService();
        try {
            Log.d("SlptClockClient", "unlockService");
            return this.m_service.unlockService(this.m_uuid);
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean clearAllClock() {
        assertService();
        acquireWakeLock();
        try {
            this.m_service.clearAllClock(this.m_uuid);
            releaseWakeLock();
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            releaseWakeLock();
            return false;
        }
    }

    public boolean selectClockIndex(int index) {
        assertService();
        acquireWakeLock();
        try {
            this.m_service.selectClockIndex(this.m_uuid, index);
            releaseWakeLock();
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            releaseWakeLock();
            return false;
        }
    }

    public boolean enableOneClock(SlptClock clock) {
        assertService();
        IwdsAssert.dieIf("SlptClockClient", clock == null, "clock can not be null!");
        acquireWakeLock();
        if (this.rleBuffer == null) {
            this.rleBuffer = new RleBuffer();
        }
        boolean ret = clock.writeToSlptService(this);
        releaseWakeLock();
        return ret;
    }

    public boolean tryEnableClock(SlptClock clock) {
        boolean z;
        assertService();
        String str = "SlptClockClient";
        if (clock == null) {
            z = true;
        } else {
            z = false;
        }
        IwdsAssert.dieIf(str, z, "clock can not be null!");
        clearAllClock();
        selectClockIndex(0);
        acquireWakeLock();
        if (this.rleBuffer == null) {
            this.rleBuffer = new RleBuffer();
        }
        boolean ret = clock.writeToSlptService(this);
        disableSportMode();
        setHourFormat(1);
        releaseWakeLock();
        return ret;
    }

    private void acquireWakeLock() {
        this.wakeLock.acquire();
    }

    private void releaseWakeLock() {
        this.wakeLock.release();
    }

    private void writePictureInner() {
        PictureInfo[] infos = null;
        if (this.infoList.size() != 0) {
            infos = new PictureInfo[this.infoList.size()];
            this.infoList.toArray(infos);
        }
        this.infoList.clear();
        writePictureRle(infos, this.rleBuffer.getBuffer());
    }

    public void writePicture(String pictureName, int width, int height, int[] mem, int backgroundColor) {
        int offset = 0;
        int length = width * height;
        this.infoList.add(new PictureInfo(pictureName, width, height, backgroundColor));
        while (mem != null) {
            offset = this.rleBuffer.add(mem, offset, length);
            debug(pictureName + " size " + length + " compress " + this.rleBuffer.getOffset());
            if (offset != length) {
                writePictureInner();
            } else {
                return;
            }
        }
    }

    public boolean flushPicture() {
        writePictureInner();
        return true;
    }

    public boolean writeSview(byte[] array) {
        try {
            this.m_service.writeSview(this.m_uuid, array);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean clearPictureGroup() {
        try {
            this.m_service.clearPictureGroup(this.m_uuid);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean writePictureGroup(String groupName) {
        try {
            this.m_service.writePictureGroup(this.m_uuid, groupName);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean writePictureRle(PictureInfo[] infoList, int[] array) {
        try {
            this.m_service.writePictureRle(this.m_uuid, infoList, array);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean writePreDrawedPicture(PreDrawedPictureInfo info) {
        try {
            this.m_service.writePreDrawedPicture(this.m_uuid, info);
            return true;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean registerCallback() {
        try {
            return this.m_service.registerCallback(this.m_uuid, this.serviceCallback);
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean unregisterCallback() {
        try {
            return this.m_service.unregisterCallback(this.m_uuid, this.serviceCallback);
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void debug(String str) {
        if (this.debugIsEnable) {
            Log.d("SlptClockClient", str);
        }
    }

    private void warning(String str) {
        Log.e("SlptClockClient", "------------------------------------------------------------------------");
        Log.e("SlptClockClient", str);
        Log.e("SlptClockClient", "------------------------------------------------------------------------");
        Thread.dumpStack();
    }
}
