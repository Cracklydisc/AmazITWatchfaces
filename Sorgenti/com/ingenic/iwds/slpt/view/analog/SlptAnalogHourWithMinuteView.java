package com.ingenic.iwds.slpt.view.analog;

public class SlptAnalogHourWithMinuteView extends SlptAnalogTimeView {
    protected short initType() {
        return SVIEW_ANALOG_HOUR_WITH_MINUTE;
    }
}
