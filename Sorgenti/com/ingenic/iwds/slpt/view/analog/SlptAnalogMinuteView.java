package com.ingenic.iwds.slpt.view.analog;

public class SlptAnalogMinuteView extends SlptAnalogTimeView {
    protected short initType() {
        return SVIEW_ANALOG_MINUTE;
    }
}
