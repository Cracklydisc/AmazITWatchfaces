package com.ingenic.iwds.slpt.view.analog;

public class SlptAnalogSecondView extends SlptAnalogTimeView {
    protected short initType() {
        return SVIEW_ANALOG_SECOND;
    }
}
