package com.ingenic.iwds.slpt.view.analog;

import com.ingenic.iwds.slpt.view.core.SlptPictureView;

public class SlptRotatePictureView extends SlptPictureView {
    protected short initType() {
        return SVIEW_ROTATE_PIC;
    }
}
