package com.ingenic.iwds.slpt.view.analog;

import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptRotateView extends SlptPictureView {
    public int draw_clockwise = 1;
    public int full_angle = 0;
    public int start_angle = 0;

    protected short initType() {
        return SVIEW_ROTATE_VIEW;
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeInt(this.start_angle);
        writer.writeInt(this.full_angle);
        writer.writeInt(this.draw_clockwise);
    }
}
