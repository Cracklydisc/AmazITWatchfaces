package com.ingenic.iwds.slpt.view.arc;

import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptArcAnglePicView extends SlptPictureView {
    public int draw_clockwise = 1;
    public int full_angle = 0;
    public int len_angle = 0;
    public int start_angle = 0;

    protected short initType() {
        return SVIEW_ARC_ANGLE_PIC;
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeInt(this.start_angle);
        writer.writeInt(this.len_angle);
        writer.writeInt(this.full_angle);
        writer.writeInt(this.draw_clockwise);
    }
}
