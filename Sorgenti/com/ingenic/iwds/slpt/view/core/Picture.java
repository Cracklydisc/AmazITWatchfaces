package com.ingenic.iwds.slpt.view.core;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.view.utils.BmpCreator;
import java.util.ArrayList;

public abstract class Picture {
    int backgroundColor = 16777215;
    Bitmap bitmap = null;
    PictureGroup group = null;
    Picture picture = null;
    int pictureIndex;

    public static class ImagePicture extends Picture {
        Bitmap bitmap = null;
        byte[] mem = null;
        String path = null;

        public ImagePicture(byte[] mem) {
            this.mem = mem;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (!(o instanceof ImagePicture)) {
                return false;
            }
            ImagePicture picture = (ImagePicture) o;
            if (picture.mem == null && this.mem == null) {
                if (picture.path == null && this.path == null) {
                    if ((picture.bitmap == null && this.bitmap == null) || picture.bitmap == this.bitmap) {
                        return true;
                    }
                    return false;
                } else if (picture.path != this.path) {
                    return false;
                } else {
                    return true;
                }
            } else if (picture.mem != this.mem) {
                return false;
            } else {
                return true;
            }
        }

        public void decodeBitmap() {
            if (this.mem != null) {
                this.bitmap = BitmapFactory.decodeByteArray(this.mem, 0, this.mem.length);
            } else if (this.path == null || this.path.length() == 0) {
                this.bitmap = this.bitmap;
            } else {
                this.bitmap = BitmapFactory.decodeFile(this.path);
            }
        }

        public void recycle() {
            if (this.bitmap != null) {
                if (!this.bitmap.isRecycled()) {
                    this.bitmap.recycle();
                }
                this.bitmap = null;
            }
            this.bitmap = null;
        }
    }

    public static class PictureContainer {
        private static int[] bitmapBuffer = new int[102400];
        ArrayList<PictureGroup> list = new ArrayList();
        PictureGroup miscGroup = new PictureGroup(0);

        public boolean findGroup(PictureGroup group) {
            if (group == null) {
                return false;
            }
            for (int i = 0; i < this.list.size(); i++) {
                if (group == this.list.get(i)) {
                    return true;
                }
            }
            return false;
        }

        public boolean findPicture(Picture picture) {
            if (picture == null) {
                return false;
            }
            for (int i = 0; i < this.list.size(); i++) {
                if (((PictureGroup) this.list.get(i)).findPicture(picture)) {
                    return true;
                }
            }
            return false;
        }

        public boolean add(PictureGroup group) {
            if (group == null) {
                return false;
            }
            if (findGroup(group)) {
                return true;
            }
            int i;
            for (i = 0; i < this.list.size(); i++) {
                PictureGroup g = (PictureGroup) this.list.get(i);
                if (group.isSubsetOf(g)) {
                    group.group = g;
                    return true;
                }
            }
            for (i = 0; i < this.list.size(); i++) {
                g = (PictureGroup) this.list.get(i);
                if (g.isSubsetOf(group)) {
                    g.group = group;
                    this.list.set(i, group);
                    group.group = null;
                    return true;
                }
            }
            this.list.add(group);
            group.group = null;
            return true;
        }

        public boolean add(Picture picture) {
            if (picture == null) {
                return false;
            }
            if (this.miscGroup.findPicture(picture)) {
                return true;
            }
            if (findPicture(picture)) {
                return true;
            }
            Picture p = this.miscGroup.findSamePicture(picture);
            if (p != null) {
                picture.picture = p;
                picture.group = this.miscGroup;
                return true;
            }
            for (int i = 0; i < this.list.size(); i++) {
                p = ((PictureGroup) this.list.get(i)).findSamePicture(picture);
                if (p != null) {
                    picture.picture = p;
                    picture.group = (PictureGroup) this.list.get(i);
                    return true;
                }
            }
            this.miscGroup.add(picture);
            picture.picture = null;
            picture.group = this.miscGroup;
            return true;
        }

        private static int[] requestBuffer(Bitmap bitmap) {
            int len = bitmap.getWidth() * bitmap.getHeight();
            if (len > bitmapBuffer.length) {
                bitmapBuffer = new int[len];
            }
            return bitmapBuffer;
        }

        private void writePictureToSlptService(SlptClockClient mClockClient, String pictureName, int width, int height, int[] mem, int backgroundColor) {
            mClockClient.writePicture(pictureName, width, height, mem, backgroundColor);
        }

        private void writeGroupToSlptService(SlptClockClient mClockClient, PictureGroup group) {
            mClockClient.writePictureGroup(group.getName());
            for (int j = 0; j < group.list.size(); j++) {
                Picture picture = (Picture) group.list.get(j);
                picture.pictureIndex = j;
                picture.decodeBitmap();
                if (picture.bitmap == null) {
                    writePictureToSlptService(mClockClient, "" + picture.pictureIndex, 0, 0, null, 16777215);
                } else {
                    requestBuffer(picture.bitmap);
                    int width = picture.bitmap.getWidth();
                    int height = picture.bitmap.getHeight();
                    picture.bitmap.getPixels(bitmapBuffer, 0, width, 0, 0, width, height);
                    writePictureToSlptService(mClockClient, "" + picture.pictureIndex, width, height, bitmapBuffer, picture.backgroundColor);
                    picture.recycle();
                    picture.bitmap = null;
                }
            }
            mClockClient.flushPicture();
        }

        public void writeToSlptService(SlptClockClient mClockClient) {
            mClockClient.clearPictureGroup();
            writeGroupToSlptService(mClockClient, this.miscGroup);
            for (int i = 0; i < this.list.size(); i++) {
                PictureGroup group = (PictureGroup) this.list.get(i);
                group.groupIndex = i;
                writeGroupToSlptService(mClockClient, group);
            }
        }
    }

    public static class PictureGroup {
        public int capacity;
        PictureGroup group = null;
        int groupIndex;
        ArrayList<Picture> list = new ArrayList();
        String name = null;
        Picture nullPicture = new ImagePicture((byte[]) null);

        public PictureGroup(int capacity) {
            this.list.ensureCapacity(capacity);
            for (int i = 0; i < capacity; i++) {
                this.list.add(this.nullPicture);
            }
            this.capacity = capacity;
        }

        public boolean isSubsetOf(PictureGroup group) {
            if (group == null) {
                return false;
            }
            if (this.list.size() > group.list.size()) {
                return false;
            }
            for (int i = 0; i < this.list.size(); i++) {
                if (!((Picture) this.list.get(i)).equals(group.list.get(i))) {
                    return false;
                }
            }
            return true;
        }

        public boolean findPicture(Picture picture) {
            if (picture == null) {
                return false;
            }
            for (int i = 0; i < this.list.size(); i++) {
                if (this.list.get(i) == picture) {
                    return true;
                }
            }
            return false;
        }

        public Picture findSamePicture(Picture picture) {
            if (picture == null) {
                return null;
            }
            int index = this.list.indexOf(picture);
            if (index >= 0) {
                return (Picture) this.list.get(index);
            }
            return null;
        }

        public boolean set(int index, Picture picture) {
            if (picture == null || index < 0 || index >= this.capacity) {
                return false;
            }
            Picture old = (Picture) this.list.get(index);
            if (old != null) {
                old.recycle();
            }
            this.list.set(index, picture);
            return true;
        }

        public boolean add(Picture picture) {
            if (picture == null) {
                return false;
            }
            this.list.add(picture);
            this.capacity = this.list.size();
            return true;
        }

        public String getName() {
            if (this.group != null) {
                return this.group.getName();
            }
            if (this.name != null) {
                return this.name;
            }
            return "" + this.groupIndex;
        }

        public void setBackgroundColorForAll(int backgroundColor) {
            for (int i = 0; i < this.list.size(); i++) {
                ((Picture) this.list.get(i)).setBackgroundColor(backgroundColor);
            }
        }
    }

    public static class StringPicture extends Picture {
        BmpCreator creator = new BmpCreator();
        String str = null;
        int textColor = -16777216;
        float textSize = 10.0f;
        Typeface typeface = Typeface.DEFAULT;

        public StringPicture(String str) {
            this.str = new String(str);
        }

        public void setTypeFace(Typeface typeface) {
            if (typeface == null) {
                typeface = Typeface.DEFAULT;
            }
            this.typeface = typeface;
        }

        public void setTextSize(float textSize) {
            this.textSize = textSize;
        }

        public void setTextColor(int textColor) {
            this.textColor = textColor;
        }

        public void setBackgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (!(o instanceof StringPicture)) {
                return false;
            }
            StringPicture picture = (StringPicture) o;
            if (this.str != null) {
                if (this.str.equals(picture.str) && this.typeface.equals(picture.typeface) && this.textSize == picture.textSize && this.textColor == picture.textColor && this.backgroundColor == picture.backgroundColor) {
                    return true;
                }
                return false;
            } else if (this.str != picture.str) {
                return false;
            } else {
                return true;
            }
        }

        public void decodeBitmap() {
            this.creator.setTypeface(this.typeface);
            this.creator.setTextSize(this.textSize);
            this.creator.setColor(this.textColor);
            this.creator.setBackgroundColor(this.backgroundColor);
            this.creator.setAntiAlias(this.backgroundColor != 16777215);
            if (this.str == null || this.str.length() == 0) {
                this.bitmap = null;
                return;
            }
            if (this.str.length() == 1) {
                this.creator.decodeChar(this.str.charAt(0));
            } else {
                this.creator.decodeString(this.str);
            }
            this.bitmap = this.creator.getBitmapNoCopy();
        }

        public void recycle() {
            this.creator.recycle();
            if (this.bitmap != null) {
                if (!this.bitmap.isRecycled()) {
                    this.bitmap.recycle();
                }
                this.bitmap = null;
            }
        }
    }

    public abstract void decodeBitmap();

    public abstract void recycle();

    public void setBackgroundColor(int backgroundColor) {
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Picture)) {
            return false;
        }
        if (((Picture) o).bitmap != this.bitmap) {
            return false;
        }
        return true;
    }

    public String getName() {
        if (this.picture != null) {
            return this.picture.getName();
        }
        return "" + this.group.getName() + "/" + this.pictureIndex;
    }
}
