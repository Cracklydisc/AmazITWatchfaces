package com.ingenic.iwds.slpt.view.core;

import android.content.Context;
import android.util.Log;
import com.ingenic.iwds.slpt.PreDrawedPictureInfo;
import com.ingenic.iwds.slpt.SlptClockClient;
import com.ingenic.iwds.slpt.view.utils.SimpleFile;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.ArrayList;

public class PreDrawedPictureGroup {
    public static final PreDrawedPictureGroup emptyGroup = new PreDrawedPictureGroup();
    private int index;
    private ArrayList<PreDrawedPicture> list;
    public String name;

    public static class PreDrawedPicture {
        private static final byte[] COLOR_MAP = "colormap".getBytes();
        private static final int HEIGHT_OFFSET = (WIDTH_OFFSET + 4);
        private static final int WIDTH_OFFSET = COLOR_MAP.length;
        private static byte[] color_map_no_data = new byte[(((COLOR_MAP.length + 8) + 12) + 4)];
        public int data_count = 0;
        public int height = 0;
        public int index = 0;
        public byte[] mem = color_map_no_data;
        public int memLen = 0;
        public int width = 0;

        static {
            System.arraycopy(COLOR_MAP, 0, color_map_no_data, 0, COLOR_MAP.length);
            setInt(color_map_no_data, WIDTH_OFFSET, 0);
            setInt(color_map_no_data, HEIGHT_OFFSET, 0);
            int offset = color_map_no_data.length - 16;
            for (int i = 0; i < 12; i++) {
                color_map_no_data[offset + i] = (byte) -1;
            }
            setInt(color_map_no_data, color_map_no_data.length - 4, 0);
        }

        private boolean checkColormap(byte[] mem) {
            if (mem.length < color_map_no_data.length) {
                return false;
            }
            int i;
            for (i = 0; i < COLOR_MAP.length; i++) {
                if (mem[i] != COLOR_MAP[i]) {
                    return false;
                }
            }
            int offset = mem.length - 16;
            for (i = 0; i < 12; i++) {
                if (mem[offset + i] != (byte) -1) {
                    return false;
                }
            }
            return true;
        }

        private static void setInt(byte[] mem, int offset, int val) {
            mem[offset + 0] = (byte) ((val >> 0) & 255);
            mem[offset + 1] = (byte) ((val >> 8) & 255);
            mem[offset + 2] = (byte) ((val >> 16) & 255);
            mem[offset + 3] = (byte) ((val >> 24) & 255);
        }

        private static int getInt(byte[] mem, int offset) {
            return ((((mem[offset + 0] & 255) << 0) | ((mem[offset + 1] & 255) << 8)) | ((mem[offset + 2] & 255) << 16)) | ((mem[offset + 3] & 255) << 24);
        }

        private void setConfig(byte[] mem) {
            this.mem = mem;
            this.width = getInt(mem, WIDTH_OFFSET);
            this.height = getInt(mem, HEIGHT_OFFSET);
            this.data_count = getInt(mem, mem.length - 4);
        }

        public PreDrawedPicture(byte[] mem) {
            if (checkColormap(mem)) {
                setConfig(mem);
            } else {
                Log.e("PreDrawedPictureGroup", "error: failed to check color map");
            }
        }

        public PreDrawedPicture(Context context, String assets_path) {
            byte[] mem = SimpleFile.readFileFromAssets(context, assets_path);
            if (mem == null) {
                Log.e("PreDrawedPictureGroup", "error: failed to open file :" + assets_path);
            } else if (checkColormap(mem)) {
                setConfig(mem);
            } else {
                Log.e("PreDrawedPictureGroup", "error: failed to check file: " + assets_path);
            }
        }
    }

    public static class PreDrawedPictureContainer {
        private ArrayList<PreDrawedPictureGroup> list = new ArrayList();

        public void add(PreDrawedPictureGroup group) {
            this.list.add(group);
        }

        private void writeGroupToSlptService(SlptClockClient mClockClient, PreDrawedPictureGroup group) {
            PreDrawedPictureInfo info = new PreDrawedPictureInfo();
            info.setSendGroup(group);
            do {
                mClockClient.writePreDrawedPicture(info);
            } while (!info.isSendOk());
        }

        public boolean writeToSlptService(SlptClockClient mClockClient) {
            for (int i = 0; i < this.list.size(); i++) {
                PreDrawedPictureGroup group = (PreDrawedPictureGroup) this.list.get(i);
                group.setName("pre_drawed_" + i);
                writeGroupToSlptService(mClockClient, group);
            }
            return true;
        }
    }

    public PreDrawedPictureGroup(Context context, String[] assets_path) {
        this.list = new ArrayList();
        this.index = 0;
        this.name = null;
        for (String preDrawedPicture : assets_path) {
            add(new PreDrawedPicture(context, preDrawedPicture));
        }
    }

    public PreDrawedPictureGroup(byte[][] mem) {
        this.list = new ArrayList();
        this.index = 0;
        this.name = null;
        for (byte[] preDrawedPicture : mem) {
            add(new PreDrawedPicture(preDrawedPicture));
        }
    }

    public PreDrawedPictureGroup() {
        this.list = new ArrayList();
        this.index = 0;
        this.name = null;
        this.name = "pre_drawed_empty";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        IwdsAssert.dieIf("PreDrawedPictureGroup", this.name == null, "must register PreDrawedPictureGroup to slptclock");
        return this.name;
    }

    public void add(PreDrawedPicture picture) {
        this.list.add(picture);
        int i = this.index;
        this.index = i + 1;
        picture.index = i;
    }

    public int size() {
        return this.list.size();
    }

    public PreDrawedPicture get(int index) {
        return (PreDrawedPicture) this.list.get(index);
    }
}
