package com.ingenic.iwds.slpt.view.core;

import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptAbsoluteLayout extends SlptLayout {
    public byte positionOfStartPointX = (byte) 0;
    public byte positionOfStartPointY = (byte) 0;

    protected short initType() {
        return SVIEW_ABSOLUTE_LAYOUT;
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeByte(this.positionOfStartPointX);
        writer.writeByte(this.positionOfStartPointY);
    }
}
