package com.ingenic.iwds.slpt.view.core;

public class SlptBatteryView extends SlptPictureGroupView {
    private int groupSize;

    public SlptBatteryView(int size) {
        this.groupSize = size;
        newPictureGroup();
    }

    public SlptBatteryView() {
        this.groupSize = 10;
        newPictureGroup();
    }

    protected int initCapacity() {
        return this.groupSize;
    }

    protected short initType() {
        return SVIEW_BATTERY;
    }
}
