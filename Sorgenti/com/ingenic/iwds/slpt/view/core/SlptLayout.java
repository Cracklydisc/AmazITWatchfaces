package com.ingenic.iwds.slpt.view.core;

import android.graphics.Typeface;
import com.ingenic.iwds.slpt.view.core.Picture.PictureContainer;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent.RegisterPictureParam;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;
import com.ingenic.iwds.utils.IwdsAssert;
import java.util.ArrayList;

public class SlptLayout extends SlptViewComponent {
    ArrayList<SlptViewComponent> list = new ArrayList();

    public void setTextAttrForAll(float textSize, int textColor, Typeface typeface) {
        for (int i = 0; i < this.list.size(); i++) {
            SlptViewComponent view = (SlptViewComponent) this.list.get(i);
            if (view instanceof SlptLayout) {
                ((SlptLayout) view).setTextAttrForAll(textSize, textColor, typeface);
            } else {
                view.setTextAttr(textSize, textColor, typeface);
            }
        }
    }

    public void setStringPictureArrayForAll(String[] array) {
        for (int i = 0; i < this.list.size(); i++) {
            SlptViewComponent view = (SlptViewComponent) this.list.get(i);
            if (view instanceof SlptNumView) {
                ((SlptNumView) view).setStringPictureArray(array);
            }
        }
    }

    public void setImagePictureArrayForAll(byte[][] array) {
        for (int i = 0; i < this.list.size(); i++) {
            SlptViewComponent view = (SlptViewComponent) this.list.get(i);
            if (view instanceof SlptNumView) {
                ((SlptNumView) view).setImagePictureArray(array);
            }
        }
    }

    public boolean search(SlptViewComponent child) {
        if (child == null) {
            return false;
        }
        for (int i = 0; i < this.list.size(); i++) {
            SlptViewComponent view = (SlptViewComponent) this.list.get(i);
            if (child == view) {
                return true;
            }
            if ((view instanceof SlptLayout) && ((SlptLayout) view).search(child)) {
                return true;
            }
        }
        return false;
    }

    public int add(SlptViewComponent child) {
        IwdsAssert.dieIf("SlptLayout", child == null, "child can not be null");
        IwdsAssert.dieIf("SlptLayout", search(child), "child already be added");
        if (child instanceof SlptLayout) {
            IwdsAssert.dieIf("SlptLayout", ((SlptLayout) child).search(this), "can not add parent to a child");
        }
        this.list.add(child);
        child.parent = this;
        return this.list.size() - 1;
    }

    public void clear() {
        this.list.clear();
    }

    public int size() {
        return this.list.size();
    }

    protected short initType() {
        return (short) -1;
    }

    public void registerPicture(PictureContainer container, RegisterPictureParam param) {
        RegisterPictureParam mParam = param.clone();
        if (this.background.picture != null) {
            mParam.backgroundColor = 16777215;
        } else if (this.background.color != 16777215) {
            mParam.backgroundColor = this.background.color;
        }
        super.registerPicture(container, mParam);
        for (int i = 0; i < this.list.size(); i++) {
            ((SlptViewComponent) this.list.get(i)).registerPicture(container, mParam);
        }
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeInt(this.list.size());
        for (int i = 0; i < this.list.size(); i++) {
            ((SlptViewComponent) this.list.get(i)).writeConfigure(writer);
        }
    }
}
