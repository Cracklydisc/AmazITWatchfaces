package com.ingenic.iwds.slpt.view.core;

import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptLinearLayout extends SlptLayout {
    public byte orientation = (byte) 0;

    protected short initType() {
        return SVIEW_LINEAR_LAYOUT;
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeByte(this.orientation);
    }
}
