package com.ingenic.iwds.slpt.view.core;

import android.graphics.Typeface;
import com.ingenic.iwds.slpt.view.core.Picture.ImagePicture;
import com.ingenic.iwds.slpt.view.core.Picture.PictureContainer;
import com.ingenic.iwds.slpt.view.core.Picture.PictureGroup;
import com.ingenic.iwds.slpt.view.core.Picture.StringPicture;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent.RegisterPictureParam;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptPictureGroupView extends SlptViewComponent {
    PictureGroup group;
    public int num;
    String[] strArray;

    public SlptPictureGroupView() {
        newPictureGroup();
    }

    public void newPictureGroup() {
        this.group = new PictureGroup(initCapacity());
        this.strArray = new String[initCapacity()];
    }

    public boolean setStringPicture(int index, String str) {
        this.strArray[index] = str;
        StringPicture picture = new StringPicture(str);
        picture.setTextSize(this.textSize);
        picture.setTypeFace(this.typeface);
        picture.setTextColor(this.textColor);
        return this.group.set(index, picture);
    }

    public boolean setImagePicture(int index, byte[] mem) {
        return this.group.set(index, new ImagePicture(mem));
    }

    public void setTextAttr(float textSize, int textColor, Typeface typeface) {
        super.setTextAttr(textSize, textColor, typeface);
        for (int i = 0; i < this.strArray.length; i++) {
            if (this.strArray[i] != null) {
                setStringPicture(i, this.strArray[i]);
            }
        }
    }

    public boolean setImagePictureArray(byte[][] array) {
        int i;
        int length = array.length < this.group.capacity ? array.length : this.group.capacity;
        for (i = 0; i < length; i++) {
            if (array[i] == null) {
                break;
            }
        }
        for (i = 0; i < length; i++) {
            setImagePicture(i, array[i]);
        }
        return false;
    }

    protected int initCapacity() {
        return 10;
    }

    protected short initType() {
        return SVIEW_PICTURE_GROUP;
    }

    void registerPicture(PictureContainer container, RegisterPictureParam param) {
        RegisterPictureParam mParam = param.clone();
        if (this.background.picture != null) {
            mParam.backgroundColor = 16777215;
        } else if (this.background.color != 16777215) {
            mParam.backgroundColor = this.background.color;
        }
        super.registerPicture(container, mParam);
        this.group.setBackgroundColorForAll(mParam.backgroundColor);
        container.add(this.group);
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeInt(this.num);
        writer.writeString(this.group.getName());
        writer.writeInt(initCapacity());
    }
}
