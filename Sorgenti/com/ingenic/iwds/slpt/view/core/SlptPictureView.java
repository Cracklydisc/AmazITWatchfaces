package com.ingenic.iwds.slpt.view.core;

import android.graphics.Typeface;
import com.ingenic.iwds.slpt.view.core.Picture.ImagePicture;
import com.ingenic.iwds.slpt.view.core.Picture.PictureContainer;
import com.ingenic.iwds.slpt.view.core.Picture.StringPicture;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent.RegisterPictureParam;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptPictureView extends SlptViewComponent {
    Picture picture = null;
    String str = null;

    public void setStringPicture(String str) {
        if (this.picture != null) {
            this.picture.recycle();
        }
        this.str = str;
        StringPicture picture = new StringPicture(str);
        picture.setTextSize(this.textSize);
        picture.setTypeFace(this.typeface);
        picture.setTextColor(this.textColor);
        this.picture = picture;
    }

    public void setStringPicture(char ch) {
        if (this.picture != null) {
            this.picture.recycle();
        }
        this.str = "" + ch;
        StringPicture picture = new StringPicture("" + ch);
        picture.setTextSize(this.textSize);
        picture.setTypeFace(this.typeface);
        picture.setTextColor(this.textColor);
        this.picture = picture;
    }

    public void setTextAttr(float textSize, int textColor, Typeface typeface) {
        super.setTextAttr(textSize, textColor, typeface);
        if (this.str != null) {
            setStringPicture(this.str);
        }
    }

    public void setImagePicture(byte[] mem) {
        if (this.picture != null) {
            this.picture.recycle();
        }
        this.picture = new ImagePicture(mem);
    }

    protected short initType() {
        return SVIEW_PIC;
    }

    void registerPicture(PictureContainer container, RegisterPictureParam param) {
        RegisterPictureParam mParam = param.clone();
        if (this.background.picture != null) {
            mParam.backgroundColor = 16777215;
        } else if (this.background.color != 16777215) {
            mParam.backgroundColor = this.background.color;
        }
        super.registerPicture(container, mParam);
        this.picture.setBackgroundColor(mParam.backgroundColor);
        container.add(this.picture);
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        SlptViewComponent.writePicture(writer, this.picture);
    }
}
