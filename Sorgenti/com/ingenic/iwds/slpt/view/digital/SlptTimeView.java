package com.ingenic.iwds.slpt.view.digital;

import com.ingenic.iwds.slpt.view.core.SlptNumView;

public class SlptTimeView extends SlptNumView {
    public static final String[] digital_nums = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    public static final String[] week_nums = new String[]{"Sun", "Mon", "Tues", "Wed", "Thur", "Fri", "Sat"};

    private void setDefaultString() {
        if (this.type != SVIEW_WEEK) {
            setStringPictureArray(digital_nums);
        } else {
            setStringPictureArray(week_nums);
        }
    }

    public SlptTimeView() {
        setDefaultString();
    }

    protected short initType() {
        return SVIEW_TIME_NUM;
    }

    protected int initCapacity() {
        return 10;
    }
}
