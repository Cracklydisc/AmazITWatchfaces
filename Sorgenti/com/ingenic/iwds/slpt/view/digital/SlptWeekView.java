package com.ingenic.iwds.slpt.view.digital;

public class SlptWeekView extends SlptTimeView {
    protected short initType() {
        return SVIEW_WEEK;
    }

    protected int initCapacity() {
        return 7;
    }
}
