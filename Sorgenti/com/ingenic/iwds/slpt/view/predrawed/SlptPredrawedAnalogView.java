package com.ingenic.iwds.slpt.view.predrawed;

public class SlptPredrawedAnalogView extends SlptPredrawedNumView {
    protected short initType() {
        return SVIEW_PRE_DRAWED_ANALOG;
    }
}
