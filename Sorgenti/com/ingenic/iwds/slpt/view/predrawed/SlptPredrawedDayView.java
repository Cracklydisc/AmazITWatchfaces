package com.ingenic.iwds.slpt.view.predrawed;

public class SlptPredrawedDayView extends SlptPredrawedTimeView {
    protected short initType() {
        return SVIEW_PRE_DRAWED_DAY;
    }
}
