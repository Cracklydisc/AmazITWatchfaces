package com.ingenic.iwds.slpt.view.predrawed;

public class SlptPredrawedHourWithMinuteView extends SlptPredrawedTimeView {
    protected short initType() {
        return SVIEW_PRE_DRAWED_HOUR_WITH_MINUTE;
    }
}
