package com.ingenic.iwds.slpt.view.predrawed;

public class SlptPredrawedMinuteView extends SlptPredrawedTimeView {
    protected short initType() {
        return SVIEW_PRE_DRAWED_MINUTE;
    }
}
