package com.ingenic.iwds.slpt.view.predrawed;

import com.ingenic.iwds.slpt.view.core.PreDrawedPictureGroup;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptPredrawedNumView extends SlptViewComponent {
    PreDrawedPictureGroup group = PreDrawedPictureGroup.emptyGroup;
    int num = 0;
    int quad = 0;

    public void setPreDrawedPicture(PreDrawedPictureGroup group) {
        this.group = group;
    }

    protected short initType() {
        return SVIEW_PRE_DRAWED_NUM;
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeInt(this.num);
        writer.writeInt(this.quad);
        writer.writeString(this.group.getName());
    }
}
