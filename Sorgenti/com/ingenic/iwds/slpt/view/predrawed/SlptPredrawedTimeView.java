package com.ingenic.iwds.slpt.view.predrawed;

public class SlptPredrawedTimeView extends SlptPredrawedAnalogView {
    protected short initType() {
        return SVIEW_PRE_DRAWED_TIME;
    }
}
