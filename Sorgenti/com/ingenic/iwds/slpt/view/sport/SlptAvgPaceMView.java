package com.ingenic.iwds.slpt.view.sport;

import com.ingenic.iwds.slpt.view.digital.SlptTimeView;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptAvgPaceMView extends SlptTimeView {
    public int unit = 0;

    public void setUnit(int unit) {
        this.unit = unit;
    }

    protected int initCapacity() {
        return 10;
    }

    protected short initType() {
        return SVIEW_SPEEDFM;
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeInt(this.unit);
    }
}
