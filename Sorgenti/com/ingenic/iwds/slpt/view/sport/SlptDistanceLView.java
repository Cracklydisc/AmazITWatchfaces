package com.ingenic.iwds.slpt.view.sport;

import com.ingenic.iwds.slpt.view.digital.SlptTimeView;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptDistanceLView extends SlptTimeView {
    public int start_distance = 0;
    public int train_distance = 0;
    public int unit = 0;

    public void setStart_distance(int start_distance) {
        this.start_distance = start_distance;
    }

    public void setTrain_distance(int train_distance) {
        this.train_distance = train_distance;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    protected int initCapacity() {
        return 10;
    }

    protected short initType() {
        return SVIEW_DISTANCEL;
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeInt(this.unit);
        writer.writeInt(this.train_distance);
        writer.writeInt(this.start_distance);
    }
}
