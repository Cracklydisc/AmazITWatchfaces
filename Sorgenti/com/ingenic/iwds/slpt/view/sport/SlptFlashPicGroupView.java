package com.ingenic.iwds.slpt.view.sport;

import com.ingenic.iwds.slpt.view.core.SlptPictureGroupView;

public class SlptFlashPicGroupView extends SlptPictureGroupView {
    private int groupSize;

    public SlptFlashPicGroupView(int size) {
        this.groupSize = size;
        newPictureGroup();
    }

    public SlptFlashPicGroupView() {
        this.groupSize = 10;
        newPictureGroup();
    }

    protected int initCapacity() {
        return this.groupSize;
    }

    protected short initType() {
        return SVIEW_PICTURE_GROUP_FLASH;
    }
}
