package com.ingenic.iwds.slpt.view.sport;

import com.ingenic.iwds.slpt.view.digital.SlptTimeView;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptSportHView extends SlptTimeView {
    int start_time = 0;
    int train_time = 0;

    protected int initCapacity() {
        return 10;
    }

    protected short initType() {
        return SVIEW_SPORTH;
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeInt(this.train_time);
        writer.writeInt(this.start_time);
    }
}
