package com.ingenic.iwds.slpt.view.sport;

import com.ingenic.iwds.slpt.view.digital.SlptTimeView;
import com.ingenic.iwds.slpt.view.utils.KeyWriter;

public class SlptSportMView extends SlptTimeView {
    boolean show_hour_time = false;
    int start_time = 0;
    int train_time = 0;

    public void setStart_time(int start_time) {
        this.start_time = start_time;
    }

    public void setMinute_with_hour() {
        this.show_hour_time = true;
    }

    public void setTrain_time(int train_time) {
        this.train_time = train_time;
    }

    protected int initCapacity() {
        return 10;
    }

    protected short initType() {
        return SVIEW_SPORTM;
    }

    public void writeConfigure(KeyWriter writer) {
        super.writeConfigure(writer);
        writer.writeInt(this.train_time);
        writer.writeInt(this.start_time);
        if (this.show_hour_time) {
            writer.writeInt(1);
        } else {
            writer.writeInt(0);
        }
    }
}
