package com.ingenic.iwds.slpt.view.sport;

import com.ingenic.iwds.slpt.view.core.SlptPictureView;
import com.ingenic.iwds.slpt.view.core.SlptViewComponent;

public class SlptSportUtil {
    protected static final short BG_ALTITUDE_INVALID;
    protected static final short BG_ALTITUDE_NEGATIVE;
    protected static final short BG_AVG_PACE_INVISABLE;
    protected static final short BG_AVG_PACE_LAYOUT;
    protected static final short BG_AVG_PACE_SEP;
    protected static final short BG_AVG_SPEED_SEP;
    protected static final short BG_CLOCK_SECOND;
    protected static final short BG_DATE_VIEW;
    protected static final short BG_DISTANCE_SEP;
    protected static final short BG_DISTANCE_START_POINT;
    protected static final short BG_DOWNHILL_AVG_SPEED_SEP;
    protected static final short BG_DOWNHILL_MAX_SPEED_SEP;
    protected static final short BG_DOWNHILL_SINGLE_DISTANCE_SEP;
    protected static final short BG_DOWNHILL_TOTAL_DISTANCE_SEP;
    protected static final short BG_FLOOR_INVALID;
    protected static final short BG_HEARTRATE_ANAEROBIC;
    protected static final short BG_HEARTRATE_FAT_BURN;
    protected static final short BG_HEARTRATE_HEART_LUNG;
    protected static final short BG_HEARTRATE_INVALID;
    protected static final short BG_HEARTRATE_NORMAL;
    protected static final short BG_HEARTRATE_STRENGTH;
    protected static final short BG_HEARTRATE_WARM_UP;
    protected static final short BG_HOUR_AM;
    protected static final short BG_HOUR_PM;
    protected static final short BG_KM_ICON_VIEW;
    protected static final short BG_LASTHEART_INVALID;
    protected static final short BG_LATITUDE_LAYOUT;
    protected static final short BG_LATITUDE_NORTH;
    protected static final short BG_LATITUDE_SOUTH;
    protected static final short BG_LONGITUDE_EAST;
    protected static final short BG_LONGITUDE_LAYOUT;
    protected static final short BG_LONGITUDE_WEST;
    protected static final short BG_LOW_BATTERY_ICON_VIEW;
    protected static final short BG_LOW_BATTERY_TEXT_VIEW;
    protected static final short BG_MESSAGE_VIEW;
    protected static final short BG_NORMAL;
    protected static final short BG_PACE_INVISABLE;
    protected static final short BG_PACE_LAYOUT;
    protected static final short BG_PACE_SEP;
    protected static final short BG_SPEED_SEP;
    protected static final short BG_TODAY_SPORT_DISTACNE;
    protected static final short BG_TOTALDISTANCE_SEP;
    protected static final short BG_TOTALDISTANCE_VIEW;
    protected static final short BG_VERTICALSEEPD_NEGATIVE;
    protected static final short BG_VERTICALSPEED_SEP;
    protected static final short BG_VERTICAL_SPEED_DOWN;
    protected static final short BG_VERTICAL_SPEED_UP;
    protected static final short BG_WAKEUP_LOCK_VIEW;
    protected static final short BG_WEEK_VIEW;
    protected static short f76I;

    static {
        f76I = (short) 0;
        short s = f76I;
        f76I = (short) (s + 1);
        BG_NORMAL = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_HEARTRATE_INVALID = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_HEARTRATE_WARM_UP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_HEARTRATE_FAT_BURN = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_SPEED_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_AVG_SPEED_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_DISTANCE_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_MESSAGE_VIEW = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_VERTICALSPEED_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_PACE_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_AVG_PACE_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_TOTALDISTANCE_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_ALTITUDE_NEGATIVE = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_TOTALDISTANCE_VIEW = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_KM_ICON_VIEW = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_DATE_VIEW = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_WEEK_VIEW = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_LOW_BATTERY_TEXT_VIEW = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_LOW_BATTERY_ICON_VIEW = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_ALTITUDE_INVALID = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_TODAY_SPORT_DISTACNE = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_LASTHEART_INVALID = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_DISTANCE_START_POINT = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_VERTICALSEEPD_NEGATIVE = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_HEARTRATE_HEART_LUNG = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_HEARTRATE_STRENGTH = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_HOUR_AM = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_HOUR_PM = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_FLOOR_INVALID = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_HEARTRATE_NORMAL = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_HEARTRATE_ANAEROBIC = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_PACE_INVISABLE = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_AVG_PACE_INVISABLE = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_PACE_LAYOUT = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_AVG_PACE_LAYOUT = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_CLOCK_SECOND = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_VERTICAL_SPEED_UP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_VERTICAL_SPEED_DOWN = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_WAKEUP_LOCK_VIEW = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_DOWNHILL_AVG_SPEED_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_DOWNHILL_MAX_SPEED_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_DOWNHILL_SINGLE_DISTANCE_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_DOWNHILL_TOTAL_DISTANCE_SEP = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_LONGITUDE_LAYOUT = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_LATITUDE_LAYOUT = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_LONGITUDE_EAST = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_LONGITUDE_WEST = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_LATITUDE_NORTH = s;
        s = f76I;
        f76I = (short) (s + 1);
        BG_LATITUDE_SOUTH = s;
    }

    public static void setAltitudeNegativeView(SlptPictureView view) {
        view.id = BG_ALTITUDE_NEGATIVE;
        view.show = false;
    }

    public static void setVerticalSpeedSeparatorView(SlptPictureView view) {
        view.id = BG_VERTICALSPEED_SEP;
    }

    public static void setAltitudeInvalidView(SlptPictureView view) {
        view.id = BG_ALTITUDE_INVALID;
        view.show = false;
    }

    public static void setAvgSpeedSeparatorView(SlptPictureView view) {
        view.id = BG_AVG_SPEED_SEP;
    }

    public static void setDistanceSeparatorView(SlptPictureView view) {
        view.id = BG_DISTANCE_SEP;
    }

    public static void setSpeedSeparatorView(SlptPictureView view) {
        view.id = BG_SPEED_SEP;
    }

    public static void setBgHeartrateNormal(SlptViewComponent view) {
        view.id = BG_HEARTRATE_NORMAL;
        view.show = true;
    }

    public static void setBgHeartrateWarmUp(SlptViewComponent view) {
        view.id = BG_HEARTRATE_WARM_UP;
        view.show = false;
    }

    public static void setBgHeartrateFatBurn(SlptViewComponent view) {
        view.id = BG_HEARTRATE_FAT_BURN;
        view.show = false;
    }

    public static void setHeartRateInvalidView(SlptViewComponent view) {
        view.id = BG_HEARTRATE_INVALID;
        view.show = false;
    }

    public static void setLastHeartRateInvalidView(SlptViewComponent view) {
        view.id = BG_LASTHEART_INVALID;
        view.show = false;
    }

    public static void setHeartrateStrength(SlptViewComponent view) {
        view.id = BG_HEARTRATE_STRENGTH;
        view.show = false;
    }

    public static void setBgHeartrateHeartLung(SlptViewComponent view) {
        view.id = BG_HEARTRATE_HEART_LUNG;
        view.show = false;
    }

    public static void setBgHeartrateAnaerobic(SlptViewComponent view) {
        view.id = BG_HEARTRATE_ANAEROBIC;
        view.show = false;
    }

    public static void setAmBgView(SlptViewComponent view) {
        view.id = BG_HOUR_AM;
        view.show = false;
    }

    public static void setPmBgView(SlptViewComponent view) {
        view.id = BG_HOUR_PM;
        view.show = false;
    }

    public static void setTodayDistanceDotView(SlptViewComponent view) {
        view.id = BG_TODAY_SPORT_DISTACNE;
        view.show = true;
    }

    public static void setTotalDistanceDotView(SlptViewComponent view) {
        view.id = BG_TOTALDISTANCE_SEP;
        view.show = true;
    }

    public static void setTodayFloorInvalidView(SlptViewComponent view) {
        view.id = BG_FLOOR_INVALID;
        view.show = false;
    }

    public static void setPaceDefaultView(SlptViewComponent view) {
        view.id = BG_PACE_INVISABLE;
        view.show = false;
    }

    public static void setAvgPaceDefaultView(SlptViewComponent view) {
        view.id = BG_AVG_PACE_INVISABLE;
        view.show = false;
    }

    public static void setPaceLayoutView(SlptViewComponent view) {
        view.id = BG_PACE_LAYOUT;
        view.show = false;
    }

    public static void setAvgPaceLayoutView(SlptViewComponent view) {
        view.id = BG_AVG_PACE_LAYOUT;
        view.show = false;
    }

    public static void setVerticalSpeedUp(SlptViewComponent view) {
        view.id = BG_VERTICAL_SPEED_UP;
        view.show = false;
    }

    public static void setVerticalSpeedDown(SlptViewComponent view) {
        view.id = BG_VERTICAL_SPEED_DOWN;
        view.show = false;
    }

    public static void setLowBatteryIconView(SlptViewComponent view) {
        view.id = BG_LOW_BATTERY_ICON_VIEW;
        view.show = false;
    }

    public static void setWatchfaceLockView(SlptViewComponent view) {
        view.id = BG_WAKEUP_LOCK_VIEW;
        view.show = false;
    }

    public static void setDHAvgSpeedSepView(SlptViewComponent view) {
        view.id = BG_DOWNHILL_AVG_SPEED_SEP;
        view.show = true;
    }

    public static void setDHMaxSpeedSepView(SlptViewComponent view) {
        view.id = BG_DOWNHILL_MAX_SPEED_SEP;
        view.show = true;
    }

    public static void setDHSingleDistanceSepView(SlptViewComponent view) {
        view.id = BG_DOWNHILL_SINGLE_DISTANCE_SEP;
        view.show = true;
    }

    public static void setDHTotalDistanceSepView(SlptViewComponent view) {
        view.id = BG_DOWNHILL_TOTAL_DISTANCE_SEP;
        view.show = true;
    }

    public static void setBgLongitudeLayoutView(SlptViewComponent view) {
        view.id = BG_LONGITUDE_LAYOUT;
        view.show = true;
    }

    public static void setBgLatitudeLayout(SlptViewComponent view) {
        view.id = BG_LATITUDE_LAYOUT;
        view.show = true;
    }

    public static void setLongitudeEastView(SlptViewComponent view) {
        view.id = BG_LONGITUDE_EAST;
        view.show = false;
    }

    public static void setLongitudeWestView(SlptViewComponent view) {
        view.id = BG_LONGITUDE_WEST;
        view.show = false;
    }

    public static void setLatitudeNorthView(SlptViewComponent view) {
        view.id = BG_LATITUDE_NORTH;
        view.show = false;
    }

    public static void setLatitudeSouthView(SlptViewComponent view) {
        view.id = BG_LATITUDE_SOUTH;
        view.show = false;
    }
}
