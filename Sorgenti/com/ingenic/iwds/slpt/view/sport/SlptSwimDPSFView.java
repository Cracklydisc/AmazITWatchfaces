package com.ingenic.iwds.slpt.view.sport;

import com.ingenic.iwds.slpt.view.digital.SlptTimeView;

public class SlptSwimDPSFView extends SlptTimeView {
    protected int initCapacity() {
        return 10;
    }

    protected short initType() {
        return SVIEW_SWIM_DPSF;
    }
}
