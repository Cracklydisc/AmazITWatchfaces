package com.ingenic.iwds.slpt.view.sport;

import com.ingenic.iwds.slpt.view.digital.SlptTimeView;

public class SlptTEFView extends SlptTimeView {
    protected int initCapacity() {
        return 10;
    }

    protected short initType() {
        return SVIEW_TEF;
    }
}
