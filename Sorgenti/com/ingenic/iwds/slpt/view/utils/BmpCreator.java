package com.ingenic.iwds.slpt.view.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;
import android.graphics.Typeface;

public class BmpCreator extends Paint {
    private int backgroundColor = 16777215;
    private Bitmap bitmap = null;
    private Rect bounds = new Rect();
    private Canvas canvas;
    private Paint paint = this;

    public Bitmap getBitmapNoCopy() {
        return this.bitmap;
    }

    public void setBackgroundColor(int color) {
        this.backgroundColor = color;
    }

    public void recycle() {
        if (!(this.bitmap == null || this.bitmap.isRecycled())) {
            this.bitmap.recycle();
        }
        this.bitmap = null;
    }

    int getWidth(int width) {
        Align align = this.paint.getTextAlign();
        if (align == Align.LEFT) {
            return 0;
        }
        if (align == Align.CENTER) {
            return width / 2;
        }
        if (align != Align.RIGHT) {
            return 0;
        }
        return width;
    }

    void createBitmap(int width, int height) {
        if (this.canvas == null) {
            this.canvas = new Canvas();
        }
        if (!(this.bitmap == null || this.bitmap.isRecycled())) {
            this.bitmap.recycle();
        }
        this.bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        this.canvas.setBitmap(this.bitmap);
    }

    public BmpCreator() {
        this.paint.setTextSize(100.0f);
        this.paint.setTypeface(Typeface.DEFAULT);
        this.paint.setTextAlign(Align.LEFT);
        this.paint.setColor(-16777216);
        this.paint.setAntiAlias(true);
    }

    public void decodeString(String str) {
        this.paint.getTextBounds(str, 0, str.length(), this.bounds);
        int width = this.bounds.right;
        FontMetrics fontMetrics = this.paint.getFontMetrics();
        int height = (int) (((double) (fontMetrics.descent - fontMetrics.ascent)) + 0.5d);
        int x = getWidth(width);
        int y = (int) (((float) height) - fontMetrics.descent);
        createBitmap(width, height);
        this.canvas.drawColor(this.backgroundColor);
        this.canvas.drawText(str, (float) x, (float) y, this.paint);
    }

    public void decodeChar(char ch) {
        this.paint.getTextBounds("" + ch, 0, 1, this.bounds);
        int width1 = this.bounds.right;
        this.paint.getTextBounds("" + ch + ch, 0, 2, this.bounds);
        int width = this.bounds.right - width1;
        FontMetrics fontMetrics = this.paint.getFontMetrics();
        int height = (int) (((double) (fontMetrics.descent - fontMetrics.ascent)) + 0.5d);
        int x = getWidth(width);
        int y = (int) (((float) height) - fontMetrics.descent);
        createBitmap(width, height);
        this.canvas.drawColor(this.backgroundColor);
        this.canvas.drawText("" + ch, (float) x, (float) y, this.paint);
    }
}
