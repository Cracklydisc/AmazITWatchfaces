package com.ingenic.iwds.slpt.view.utils;

import android.util.Log;
import com.ingenic.iwds.utils.IwdsAssert;

public class KeyWriter {
    public static String TAG = "KeyWriter";
    private long jniPrivate = initialize_native();
    private boolean nativeIsInitialized = true;

    private native int getSize(long j);

    private native long initialize_native();

    private native void recycle(long j);

    private native void setBytesArray(long j, byte[] bArr, int i);

    private native void writeBoolean(long j, byte b);

    private native void writeByte(long j, byte b);

    private native void writeInt(long j, int i);

    private native void writeShort(long j, short s);

    private native void writeString(long j, String str);

    static {
        System.loadLibrary("key-writer");
        Log.d(TAG, "load key-writer success");
    }

    public void recycle() {
        if (this.nativeIsInitialized) {
            recycle(this.jniPrivate);
            this.jniPrivate = 0;
            this.nativeIsInitialized = false;
        }
    }

    public void setRawBytes(byte[] buffer, int len) {
        checkInitialization();
        setBytesArray(this.jniPrivate, buffer, len);
    }

    public int getSize() {
        checkInitialization();
        return getSize(this.jniPrivate);
    }

    public void writeByte(byte val) {
        checkInitialization();
        writeByte(this.jniPrivate, val);
    }

    public void writeBoolean(boolean val) {
        checkInitialization();
        writeBoolean(this.jniPrivate, (byte) (val ? 1 : 0));
    }

    public void writeShort(short val) {
        checkInitialization();
        writeShort(this.jniPrivate, val);
    }

    public void writeInt(int val) {
        checkInitialization();
        writeInt(this.jniPrivate, val);
    }

    public void writeString(String str) {
        checkInitialization();
        IwdsAssert.dieIf(TAG, str == null, "String is null!");
        writeString(this.jniPrivate, str);
    }

    private void checkInitialization() {
        IwdsAssert.dieIf(TAG, !this.nativeIsInitialized, "KeyWriter is recycled!");
    }
}
