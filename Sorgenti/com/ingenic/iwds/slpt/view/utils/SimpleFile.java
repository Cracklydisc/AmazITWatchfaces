package com.ingenic.iwds.slpt.view.utils;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class SimpleFile {
    public static FileInputStream getInputStream(String path) {
        File file = new File(path);
        FileInputStream in = null;
        if (!file.exists()) {
            Log.d("simpleFile", "file not exist[" + path + "]");
            return null;
        } else if (file.canRead()) {
            try {
                in = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                Log.d("simpleFile", "Failed to get a file read stream[" + path + "]");
            }
            return in;
        } else {
            Log.d("simpleFile", "file can not be read, permission denied[" + path + "]");
            return null;
        }
    }

    public static Boolean closeInputStream(FileInputStream in) {
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
                Log.d("simpleFile", "Failed to close file");
                return Boolean.valueOf(false);
            }
        }
        return Boolean.valueOf(true);
    }

    public static byte[] readFile(String path) {
        int len = -1;
        FileInputStream in = getInputStream(path);
        if (in == null) {
            return null;
        }
        try {
            len = in.available();
        } catch (IOException e) {
            Log.d("simpleFile", "failed to get file len");
        }
        if (len <= 0) {
            closeInputStream(in);
            return null;
        }
        byte[] buf = new byte[len];
        try {
            in.read(buf, 0, buf.length);
        } catch (IOException e2) {
            Log.d("simpleFile", "Failed to read file[" + path + "]");
            buf = null;
        }
        closeInputStream(in);
        return buf;
    }

    public static byte[] readFileFromAssets(Context context, String path) {
        InputStream in = null;
        int len = -1;
        try {
            in = context.getAssets().open(path);
        } catch (Exception e) {
            Log.d("simpleFile", "Failed to open assets file[" + path + "]");
        }
        if (in == null) {
            return null;
        }
        try {
            len = in.available();
        } catch (IOException e2) {
            Log.d("simpleFile", "failed to get file len");
        }
        if (len <= 0) {
            try {
                in.close();
                return null;
            } catch (IOException e3) {
                Log.d("simpleFile", "Failed to close assets file[" + path + "]");
                return null;
            }
        }
        byte[] buf = new byte[len];
        try {
            in.read(buf, 0, buf.length);
        } catch (IOException e4) {
            Log.d("simpleFile", "Failed to read file[" + path + "]");
            buf = null;
        }
        try {
            in.close();
        } catch (IOException e5) {
            Log.d("simpleFile", "Failed to close assets file[" + path + "]");
        }
        return buf;
    }
}
