package com.ingenic.iwds.slpt.view.utils;

public class SlptWatchFaceConst {
    public static int KEY_WAEUP_STATUS_ALL = 1;
    public static int KEY_WAEUP_STATUS_NONE = 0;
    public static int KEY_WAEUP_STATUS_UP = 2;
    public static Object WatchFaceLock = new Object();
}
