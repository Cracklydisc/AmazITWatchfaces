package com.ingenic.iwds.smartsense;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.ingenic.iwds.utils.IwdsAssert;

public class RemoteAuthInfo implements Parcelable {
    public static final Creator<RemoteAuthInfo> CREATOR = new C03821();
    public String manufacture;
    public String model;
    public String packageName;
    public String serialNo;

    static class C03821 implements Creator<RemoteAuthInfo> {
        C03821() {
        }

        public RemoteAuthInfo createFromParcel(Parcel source) {
            RemoteAuthInfo remoteAuthInfo = new RemoteAuthInfo(source.readString());
            remoteAuthInfo.model = source.readString();
            remoteAuthInfo.manufacture = source.readString();
            remoteAuthInfo.serialNo = source.readString();
            return remoteAuthInfo;
        }

        public RemoteAuthInfo[] newArray(int size) {
            return new RemoteAuthInfo[size];
        }
    }

    public RemoteAuthInfo(String packageName) {
        IwdsAssert.dieIf((Object) this, packageName == null, "Application package name is null");
        this.model = Build.MODEL;
        this.manufacture = Build.MANUFACTURER;
        this.serialNo = Build.SERIAL;
        this.packageName = packageName;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.packageName);
        dest.writeString(this.model);
        dest.writeString(this.manufacture);
        dest.writeString(this.serialNo);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.serialNo == null ? 0 : this.serialNo.hashCode()) + (((this.model == null ? 0 : this.model.hashCode()) + (((this.manufacture == null ? 0 : this.manufacture.hashCode()) + 31) * 31)) * 31)) * 31;
        if (this.packageName != null) {
            i = this.packageName.hashCode();
        }
        return hashCode + i;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof RemoteAuthInfo)) {
            return false;
        }
        RemoteAuthInfo remoteAuthInfo = (RemoteAuthInfo) o;
        if (this.packageName == null) {
            if (remoteAuthInfo.packageName != null) {
                return false;
            }
        } else if (!this.packageName.equals(remoteAuthInfo.packageName)) {
            return false;
        }
        if (this.model == null) {
            if (remoteAuthInfo.model != null) {
                return false;
            }
        } else if (!this.model.equals(remoteAuthInfo.model)) {
            return false;
        }
        if (this.manufacture == null) {
            if (remoteAuthInfo.manufacture != null) {
                return false;
            }
        } else if (!this.manufacture.equals(remoteAuthInfo.manufacture)) {
            return false;
        }
        if (this.serialNo == null) {
            if (remoteAuthInfo.serialNo != null) {
                return false;
            }
            return true;
        } else if (this.serialNo.equals(remoteAuthInfo.serialNo)) {
            return true;
        } else {
            return false;
        }
    }

    public String toString() {
        return "RemoteDeivceAuthInfo [model=" + this.model + ", manufacture=" + this.manufacture + ", serialNo=" + this.serialNo + ", packageName=" + this.packageName + "]";
    }
}
