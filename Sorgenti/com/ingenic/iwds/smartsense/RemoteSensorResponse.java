package com.ingenic.iwds.smartsense;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;

public class RemoteSensorResponse implements Parcelable {
    public static final Creator<RemoteSensorResponse> CREATOR = new C03841();
    public int accuracy;
    public int result;
    public Sensor sensor;
    public SensorEvent sensorEvent;
    public ArrayList<Sensor> sensorList;
    public int type;

    static class C03841 implements Creator<RemoteSensorResponse> {
        C03841() {
        }

        public RemoteSensorResponse createFromParcel(Parcel source) {
            RemoteSensorResponse remoteSensorResponse = new RemoteSensorResponse();
            remoteSensorResponse.type = source.readInt();
            remoteSensorResponse.result = source.readInt();
            remoteSensorResponse.accuracy = source.readInt();
            if (source.readInt() != 0) {
                remoteSensorResponse.sensor = (Sensor) source.readParcelable(Sensor.class.getClassLoader());
            }
            if (source.readInt() != 0) {
                remoteSensorResponse.sensorEvent = (SensorEvent) source.readParcelable(SensorEvent.class.getClassLoader());
            }
            if (source.readInt() != 0) {
                remoteSensorResponse.sensorList = source.readArrayList(Sensor.class.getClassLoader());
            }
            return remoteSensorResponse;
        }

        public RemoteSensorResponse[] newArray(int size) {
            return new RemoteSensorResponse[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeInt(this.result);
        dest.writeInt(this.accuracy);
        if (this.sensor != null) {
            dest.writeInt(1);
            dest.writeParcelable(this.sensor, flags);
        } else {
            dest.writeInt(0);
        }
        if (this.sensorEvent != null) {
            dest.writeInt(1);
            dest.writeParcelable(this.sensorEvent, flags);
        } else {
            dest.writeInt(0);
        }
        if (this.sensorList != null) {
            dest.writeInt(1);
            dest.writeList(this.sensorList);
            return;
        }
        dest.writeInt(0);
    }
}
