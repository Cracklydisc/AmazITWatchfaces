package com.ingenic.iwds.utils;

public class IwdsAssert {
    public static void dieIf(Object who, boolean condition, String message) {
        if (condition) {
            IwdsLog.m22e(who, "============= IWDS Assert Failed ============");
            IwdsLog.m22e(who, "Message: " + message);
            IwdsLog.m22e(who, "=============================================");
            Thread.dumpStack();
            IwdsLog.m22e(who, "============== IWDS Assert End ==============");
            ((IwdsAssert) null).die();
        }
    }

    public static void dieIf(String tag, boolean condition, String message) {
        if (condition) {
            IwdsLog.m23e(tag, "============= IWDS Assert Failed ============");
            IwdsLog.m23e(tag, "Message: " + message);
            IwdsLog.m23e(tag, "=============================================");
            Thread.dumpStack();
            IwdsLog.m23e(tag, "============== IWDS Assert End ==============");
            ((IwdsAssert) null).die();
        }
    }

    private void die() {
    }
}
