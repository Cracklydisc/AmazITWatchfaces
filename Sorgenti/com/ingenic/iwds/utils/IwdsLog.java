package com.ingenic.iwds.utils;

import android.util.Log;

public class IwdsLog {
    public static void m22e(Object who, String message) {
        Log.e("IWDS---" + who.getClass().getSimpleName(), message);
    }

    public static void m23e(String tag, String message) {
        Log.e("IWDS---" + tag, message);
    }
}
